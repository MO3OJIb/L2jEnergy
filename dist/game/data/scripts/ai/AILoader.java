/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ai.events.Squash;
import ai.fantasy_isle.HandysBlockCheckerEvent;
import ai.fantasy_isle.MC_Show;
import ai.fantasy_isle.Parade;
import ai.group_template.BeastFarm;
import ai.group_template.Corpse;
import ai.group_template.DeadlyEffects;
import ai.group_template.DenOfEvil;
import ai.group_template.DragonValley;
import ai.group_template.FairyTrees;
import ai.group_template.FeedableBeasts;
import ai.group_template.FestivalMobs;
import ai.group_template.FleeMonsters;
import ai.group_template.FrozenLabyrinth;
import ai.group_template.GiantsCave;
import ai.group_template.HotSprings;
import ai.group_template.IsleOfPrayer;
import ai.group_template.KrateisCube;
import ai.group_template.LairOfAntharas;
import ai.group_template.LuckyPig;
import ai.group_template.MinionSpawnManager;
import ai.group_template.MonasteryOfSilence;
import ai.group_template.NonAffectedNpcs;
import ai.group_template.NonLethalableNpcs;
import ai.group_template.NonTalkingNpcs;
import ai.group_template.NpcAnimations;
import ai.group_template.OlMahums;
import ai.group_template.PaganTemple;
import ai.group_template.PavelArchaic;
import ai.group_template.PlainsOfDion;
import ai.group_template.PlainsOfLizardman;
import ai.group_template.PolymorphingAngel;
import ai.group_template.PolymorphingOnAttack;
import ai.group_template.PrimevalIsle;
import ai.group_template.PrisonGuards;
import ai.group_template.RaidBossCancel;
import ai.group_template.RandomSpawn;
import ai.group_template.RangeGuard;
import ai.group_template.Remnants;
import ai.group_template.Sandstorms;
import ai.group_template.SeeThroughSilentMove;
import ai.group_template.SelMahumDrill;
import ai.group_template.SelMahumSquad;
import ai.group_template.SilentValley;
import ai.group_template.StakatoNest;
import ai.group_template.SummonPc;
import ai.group_template.TreasureChest;
import ai.group_template.TurekOrcs;
import ai.group_template.VarkaKetra;
import ai.group_template.WalkingGuards;
import ai.group_template.WarriorFishingBlock;
import ai.individual.Anais;
import ai.individual.Ballista;
import ai.individual.Beleth;
import ai.individual.BlackdaggerWing;
import ai.individual.BleedingFly;
import ai.individual.BloodyBerserker;
import ai.individual.BloodyKarik;
import ai.individual.BloodyKarinness;
import ai.individual.Core;
import ai.individual.CrimsonHatuOtis;
import ai.individual.DarkWaterDragon;
import ai.individual.DivineBeast;
import ai.individual.DrakeLeader;
import ai.individual.DrakeMage;
import ai.individual.DrakosWarrior;
import ai.individual.DustRider;
import ai.individual.EmeraldHorn;
import ai.individual.Epidos;
import ai.individual.EvasGiftBox;
import ai.individual.FrightenedRagnaOrc;
import ai.individual.GiganticGolem;
import ai.individual.Gordon;
import ai.individual.GraveRobbers;
import ai.individual.Knoriks;
import ai.individual.MuscleBomber;
import ai.individual.NecromancerOfTheValley;
import ai.individual.Orfen;
import ai.individual.PriestUgoros;
import ai.individual.QueenAnt;
import ai.individual.QueenShyeed;
import ai.individual.RagnaOrcCommander;
import ai.individual.RagnaOrcHero;
import ai.individual.RagnaOrcSeer;
import ai.individual.ShadowSummoner;
import ai.individual.SinEater;
import ai.individual.SinWardens;
import ai.individual.Valakas;
import ai.individual.Antharas.Antharas;
import ai.individual.Baium.Baium;
import ai.individual.Sailren.Sailren;
import ai.individual.VanHalter.VanHalter;
import ai.individual.Venom.Venom;
import ai.npc.Abercrombie.Abercrombie;
import ai.npc.AdventureGuildsman.AdventureGuildsman;
import ai.npc.Alarm.Alarm;
import ai.npc.Alexandria.Alexandria;
import ai.npc.ArenaManager.ArenaManager;
import ai.npc.Asamah.Asamah;
import ai.npc.AvantGarde.AvantGarde;
import ai.npc.BlackJudge.BlackJudge;
import ai.npc.BlackMarketeerOfMammon.BlackMarketeerOfMammon;
import ai.npc.BlacksmithOfMammon.BlacksmithOfMammon;
import ai.npc.BloodAltars.AdenBloodAltar;
import ai.npc.BloodAltars.DarkElfBloodAltar;
import ai.npc.BloodAltars.DionBloodAltar;
import ai.npc.BloodAltars.DwarenBloodAltar;
import ai.npc.BloodAltars.ElvenBloodAltar;
import ai.npc.BloodAltars.GiranBloodAltar;
import ai.npc.BloodAltars.GludinBloodAltar;
import ai.npc.BloodAltars.GludioBloodAltar;
import ai.npc.BloodAltars.GoddardBloodAltar;
import ai.npc.BloodAltars.HeineBloodAltar;
import ai.npc.BloodAltars.KamaelBloodAltar;
import ai.npc.BloodAltars.OrcBloodAltar;
import ai.npc.BloodAltars.OrenBloodAltar;
import ai.npc.BloodAltars.PrimevalBloodAltar;
import ai.npc.BloodAltars.RuneBloodAltar;
import ai.npc.BloodAltars.SchutgartBloodAltar;
import ai.npc.BloodAltars.TalkingIslandBloodAltar;
import ai.npc.CastleAmbassador.CastleAmbassador;
import ai.npc.CastleBlacksmith.CastleBlacksmith;
import ai.npc.CastleChamberlain.CastleChamberlain;
import ai.npc.CastleCourtMagician.CastleCourtMagician;
import ai.npc.CastleMercenaryManager.CastleMercenaryManager;
import ai.npc.CastleSiegeManager.CastleSiegeManager;
import ai.npc.CastleTeleporter.CastleTeleporter;
import ai.npc.CastleWarehouse.CastleWarehouse;
import ai.npc.ClanTrader.ClanTrader;
import ai.npc.ClassMaster.ClassMaster;
import ai.npc.DimensionalMerchant.DimensionalMerchant;
import ai.npc.Dorian.Dorian;
import ai.npc.DragonVortex.DragonVortex;
import ai.npc.EchoCrystals.EchoCrystals;
import ai.npc.FameManager.FameManager;
import ai.npc.FestivalGuide.FestivalGuide;
import ai.npc.FestivalWitch.FestivalWitch;
import ai.npc.Fisherman.Fisherman;
import ai.npc.ForgeOfTheGods.ForgeOfTheGods;
import ai.npc.ForgeOfTheGods.Rooney;
import ai.npc.ForgeOfTheGods.TarBeetle;
import ai.npc.FortressArcherCaptain.FortressArcherCaptain;
import ai.npc.FortressSiegeManager.FortressSiegeManager;
import ai.npc.FreyasSteward.FreyasSteward;
import ai.npc.HealerTrainer.HealerTrainer;
import ai.npc.Jinia.Jinia;
import ai.npc.Katenar.Katenar;
import ai.npc.KetraOrcSupport.KetraOrcSupport;
import ai.npc.Kier.Kier;
import ai.npc.KrateisCubeController.KrateisCubeController;
import ai.npc.ManorManager.ManorManager;
import ai.npc.MercenaryCaptain.MercenaryCaptain;
import ai.npc.MerchantOfMammon.MerchantOfMammon;
import ai.npc.Minigame.Minigame;
import ai.npc.MonumentOfHeroes.MonumentOfHeroes;
import ai.npc.NevitsHerald.NevitsHerald;
import ai.npc.NpcBuffers.NpcBuffers;
import ai.npc.NpcBuffers.impl.CabaleBuffer;
import ai.npc.PriestOfBlessing.PriestOfBlessing;
import ai.npc.RaceManager.RaceManager;
import ai.npc.Rafforty.Rafforty;
import ai.npc.RiftWatcher.RiftWatcher;
import ai.npc.Rignos.Rignos;
import ai.npc.SSQPriest.SSQPriest;
import ai.npc.Selina.Selina;
import ai.npc.Sirra.Sirra;
import ai.npc.SubclassCertification.SubclassCertification;
import ai.npc.Summons.MerchantGolem.GolemTrader;
import ai.npc.Summons.Pets.BabyPets;
import ai.npc.Summons.Pets.ImprovedBabyPets;
import ai.npc.Summons.Servitors.Servitors;
import ai.npc.SupportUnitCaptain.SupportUnitCaptain;
import ai.npc.SymbolMaker.SymbolMaker;
import ai.npc.Teleports.Asher.Asher;
import ai.npc.Teleports.CrumaTower.CrumaTower;
import ai.npc.Teleports.DelusionTeleport.DelusionTeleport;
import ai.npc.Teleports.ElrokiTeleporters.ElrokiTeleporters;
import ai.npc.Teleports.GatekeeperSpirit.GatekeeperSpirit;
import ai.npc.Teleports.GhostChamberlainOfElmoreden.GhostChamberlainOfElmoreden;
import ai.npc.Teleports.GrandBossTeleporters.GrandBossTeleporters;
import ai.npc.Teleports.GuardianBorder.GuardianBorder;
import ai.npc.Teleports.Klemis.Klemis;
import ai.npc.Teleports.MithrilMinesTeleporter.MithrilMinesTeleporter;
import ai.npc.Teleports.NewbieGuide.NewbieGuide;
import ai.npc.Teleports.NoblesseTeleport.NoblesseTeleport;
import ai.npc.Teleports.OracleTeleport.OracleTeleport;
import ai.npc.Teleports.PaganTeleporters.PaganTeleporters;
import ai.npc.Teleports.SSQTeleporter.SSQTeleporter;
import ai.npc.Teleports.SeparatedSoul.SeparatedSoul;
import ai.npc.Teleports.StakatoNestTeleporter.StakatoNestTeleporter;
import ai.npc.Teleports.SteelCitadelTeleport.SteelCitadelTeleport;
import ai.npc.Teleports.StrongholdsTeleports.StrongholdsTeleports;
import ai.npc.Teleports.Survivor.Survivor;
import ai.npc.Teleports.TeleportToFantasy.TeleportToFantasy;
import ai.npc.Teleports.TeleportToRaceTrack.TeleportToRaceTrack;
import ai.npc.Teleports.TeleportToUndergroundColiseum.TeleportToUndergroundColiseum;
import ai.npc.Teleports.TeleportWithCharm.TeleportWithCharm;
import ai.npc.Teleports.ToIVortex.ToIVortex;
import ai.npc.TerritoryManagers.TerritoryManagers;
import ai.npc.TownPets.TownPets;
import ai.npc.Trainer.Trainer;
import ai.npc.Tunatun.Tunatun;
import ai.npc.UndergroundColiseumManager.UndergroundColiseumManager;
import ai.npc.VarkaSilenosSupport.VarkaSilenosSupport;
import ai.npc.VillageMasters.FirstClassTransferTalk.FirstClassTransferTalk;
import ai.npc.WeaverOlf.WeaverOlf;
import ai.npc.WyvernManager.WyvernManager;
import village_master.Alliance.Alliance;
import village_master.Clan.Clan;
import village_master.DarkElfChange1.DarkElfChange1;
import village_master.DarkElfChange2.DarkElfChange2;
import village_master.DwarfBlacksmithChange1.DwarfBlacksmithChange1;
import village_master.DwarfBlacksmithChange2.DwarfBlacksmithChange2;
import village_master.DwarfWarehouseChange1.DwarfWarehouseChange1;
import village_master.DwarfWarehouseChange2.DwarfWarehouseChange2;
import village_master.ElfHumanClericChange2.ElfHumanClericChange2;
import village_master.ElfHumanFighterChange1.ElfHumanFighterChange1;
import village_master.ElfHumanFighterChange2.ElfHumanFighterChange2;
import village_master.ElfHumanWizardChange1.ElfHumanWizardChange1;
import village_master.ElfHumanWizardChange2.ElfHumanWizardChange2;
import village_master.KamaelChange1.KamaelChange1;
import village_master.KamaelChange2.KamaelChange2;
import village_master.OrcChange1.OrcChange1;
import village_master.OrcChange2.OrcChange2;

/**
 * AI loader.
 * @author Zoey76
 */
public class AILoader
{
	private static final Logger LOG = LoggerFactory.getLogger(AILoader.class);
	
	private static final Class<?>[] SCRIPTS =
	{
		// NPC
		Abercrombie.class,
		AdventureGuildsman.class,
		Alarm.class,
		Alexandria.class,
		ArenaManager.class,
		Asamah.class,
		AvantGarde.class,
		BlackJudge.class,
		BlackMarketeerOfMammon.class,
		BlacksmithOfMammon.class,
		AdenBloodAltar.class,
		DarkElfBloodAltar.class,
		DionBloodAltar.class,
		DwarenBloodAltar.class,
		ElvenBloodAltar.class,
		GiranBloodAltar.class,
		GludinBloodAltar.class,
		GludioBloodAltar.class,
		GoddardBloodAltar.class,
		HeineBloodAltar.class,
		KamaelBloodAltar.class,
		OrcBloodAltar.class,
		OrenBloodAltar.class,
		PrimevalBloodAltar.class,
		RuneBloodAltar.class,
		SchutgartBloodAltar.class,
		TalkingIslandBloodAltar.class,
		CastleAmbassador.class,
		CastleBlacksmith.class,
		CastleCourtMagician.class,
		CastleChamberlain.class,
		CastleMercenaryManager.class,
		CastleSiegeManager.class,
		CastleTeleporter.class,
		CastleWarehouse.class,
		ClanTrader.class,
		ClassMaster.class,
		DimensionalMerchant.class,
		Dorian.class,
		DragonVortex.class,
		EchoCrystals.class,
		FameManager.class,
		FestivalGuide.class,
		FestivalWitch.class,
		Fisherman.class,
		ForgeOfTheGods.class,
		Rooney.class,
		TarBeetle.class,
		FortressArcherCaptain.class,
		FortressSiegeManager.class,
		FreyasSteward.class,
		HealerTrainer.class,
		Jinia.class,
		Katenar.class,
		KetraOrcSupport.class,
		Kier.class,
		KrateisCubeController.class,
		ManorManager.class,
		MerchantOfMammon.class,
		MercenaryCaptain.class,
		Minigame.class,
		MonumentOfHeroes.class,
		NevitsHerald.class,
		NpcBuffers.class,
		CabaleBuffer.class,
		PriestOfBlessing.class,
		RaceManager.class,
		RiftWatcher.class,
		Rignos.class,
		Rafforty.class,
		Selina.class,
		Sirra.class,
		SSQPriest.class,
		SubclassCertification.class,
		GolemTrader.class,
		BabyPets.class,
		ImprovedBabyPets.class,
		Servitors.class,
		SupportUnitCaptain.class,
		SymbolMaker.class,
		Asher.class,
		CrumaTower.class,
		DelusionTeleport.class,
		ElrokiTeleporters.class,
		GatekeeperSpirit.class,
		GhostChamberlainOfElmoreden.class,
		GrandBossTeleporters.class,
		GuardianBorder.class,
		Klemis.class,
		MithrilMinesTeleporter.class,
		NewbieGuide.class,
		NoblesseTeleport.class,
		OracleTeleport.class,
		PaganTeleporters.class,
		SeparatedSoul.class,
		SSQTeleporter.class,
		StakatoNestTeleporter.class,
		SteelCitadelTeleport.class,
		StrongholdsTeleports.class,
		Survivor.class,
		TeleportToFantasy.class,
		TeleportToRaceTrack.class,
		TeleportToUndergroundColiseum.class,
		TeleportWithCharm.class,
		ToIVortex.class,
		TerritoryManagers.class,
		TownPets.class,
		Trainer.class,
		Tunatun.class,
		VarkaSilenosSupport.class,
		FirstClassTransferTalk.class,
		WeaverOlf.class,
		WyvernManager.class,
		
		// Events
		Squash.class,
		// Watermelon.class,
		
		// Fantasy Isle
		Parade.class,
		MC_Show.class,
		HandysBlockCheckerEvent.class,
		UndergroundColiseumManager.class,
		
		// Group Template
		BeastFarm.class,
		Corpse.class,
		DeadlyEffects.class,
		DenOfEvil.class,
		DragonValley.class,
		FairyTrees.class,
		FeedableBeasts.class,
		FestivalMobs.class,
		FleeMonsters.class,
		FrozenLabyrinth.class,
		GiantsCave.class,
		HotSprings.class,
		IsleOfPrayer.class,
		KrateisCube.class,
		LairOfAntharas.class,
		LuckyPig.class,
		MinionSpawnManager.class,
		MonasteryOfSilence.class,
		NonAffectedNpcs.class,
		NonLethalableNpcs.class,
		NonTalkingNpcs.class,
		NpcAnimations.class,
		OlMahums.class,
		PaganTemple.class,
		PavelArchaic.class,
		PlainsOfDion.class,
		PlainsOfLizardman.class,
		PolymorphingAngel.class,
		PolymorphingOnAttack.class,
		PrimevalIsle.class,
		PrisonGuards.class,
		RaidBossCancel.class,
		RandomSpawn.class,
		RangeGuard.class,
		Remnants.class,
		Sandstorms.class,
		SeeThroughSilentMove.class,
		SelMahumDrill.class,
		SelMahumSquad.class,
		SilentValley.class,
		StakatoNest.class,
		SummonPc.class,
		TreasureChest.class,
		TurekOrcs.class,
		VarkaKetra.class,
		WalkingGuards.class,
		WarriorFishingBlock.class,
		
		// Individual
		Antharas.class,
		Baium.class,
		Sailren.class,
		VanHalter.class,
		Venom.class,
		Anais.class,
		Ballista.class,
		Beleth.class,
		BlackdaggerWing.class,
		BleedingFly.class,
		BloodyBerserker.class,
		BloodyKarik.class,
		BloodyKarinness.class,
		CrimsonHatuOtis.class,
		Core.class,
		DarkWaterDragon.class,
		DivineBeast.class,
		DrakeLeader.class,
		DrakeMage.class,
		DrakosWarrior.class,
		DustRider.class,
		EmeraldHorn.class,
		Epidos.class,
		EvasGiftBox.class,
		FrightenedRagnaOrc.class,
		GiganticGolem.class,
		Gordon.class,
		GraveRobbers.class,
		Knoriks.class,
		MuscleBomber.class,
		Orfen.class,
		PriestUgoros.class,
		QueenAnt.class,
		QueenShyeed.class,
		RagnaOrcCommander.class,
		RagnaOrcHero.class,
		RagnaOrcSeer.class,
		NecromancerOfTheValley.class,
		ShadowSummoner.class,
		SinEater.class,
		SinWardens.class,
		Valakas.class,
		
		// Village Master
		Clan.class,
		Alliance.class,
		DarkElfChange1.class,
		DarkElfChange2.class,
		DwarfBlacksmithChange1.class,
		DwarfBlacksmithChange2.class,
		DwarfWarehouseChange1.class,
		DwarfWarehouseChange2.class,
		ElfHumanClericChange2.class,
		ElfHumanFighterChange1.class,
		ElfHumanFighterChange2.class,
		ElfHumanWizardChange1.class,
		ElfHumanWizardChange2.class,
		KamaelChange1.class,
		KamaelChange2.class,
		OrcChange1.class,
		OrcChange2.class
	};
	
	public static void main(String[] args)
	{
		var n = 0;
		for (var ai : SCRIPTS)
		{
			try
			{
				ai.getDeclaredConstructor().newInstance();
				n++;
			}
			catch (Exception ex)
			{
				LOG.error("Error loading AI {}!", ai.getSimpleName(), ex);
			}
		}
		LOG.info("Loaded {} AI scripts.", n);
	}
}
