/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.events;

import java.util.Arrays;
import java.util.List;

import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.network.serverpackets.MagicSkillUse;

import ai.npc.AbstractNpcAI;

/**
 * Squash AI (The Fall Harvest event). TODO: need test
 * @author Мо3олЬ
 */
public class Squash extends AbstractNpcAI
{
	// Npc
	public final static int YOUNG_SQUASH = 12774;
	public final static int LARGE_YOUNG_SQUASH = 12777;
	// Monsters
	public final static int HIGH_QUALITY_SQUASH = 12775;
	public final static int LOW_QUALITY_SQUASH = 12776;
	public final static int HIGH_QUALITY_LARGE_SQUASH = 12778;
	public final static int LOW_QUALITY_LARGE_SQUASH = 12779;
	public final static int KING_SQUASH = 13016;
	public final static int EMPEROR_SQUASH = 13017;
	// Skill
	private static final int NECTAR_SKILL = 2005;
	private static int NECTAR_REUSE = 3000;
	public final static int SQUASH_LEVEL_UP = 4513;
	public final static int SQUASH_POISONED = 4514;
	// Others
	private static final List<Integer> SQUASH = Arrays.asList(YOUNG_SQUASH, LARGE_YOUNG_SQUASH);
	private static final List<Integer> CHRONO_SQUASH_LIST = Arrays.asList(HIGH_QUALITY_SQUASH, LOW_QUALITY_SQUASH, HIGH_QUALITY_LARGE_SQUASH, LOW_QUALITY_LARGE_SQUASH, KING_SQUASH, EMPEROR_SQUASH);
	private static final List<Integer> CHRONO_LIST = Arrays.asList(4202, 5133, 5817, 7058, 8350);
	
	private int _nectar;
	private int _tryCount;
	private long _lastNectarUse;
	
	private static final String[] SPAWN_TEXT = new String[]
	{
		"ai_squash_i_only_have_to_drink_nectar_to_be_able_to_grow_up",
		"ai_squash_come_believe_me_sprinkle_a_nectar_i_can_certainly_turn_the_big_pumpkin",
		"ai_squash_take_nectar_to_come_pumpkin_nectar"
	};
	
	private static final String[] _NOCHRONO_TEXT =
	{
		"ai_squash_you_cannot_kill_me_without_chrono",
		"ai_squash_hehe_keep_trying",
		"ai_squash_nice_try",
		"ai_squash_tired",
		"ai_squash_go_go_haha"
	};
	
	private static final String[] ATTACK_TEXT =
	{
		"ai_squash_bites_rat_a_tat_to_change_body",
		"ai_squash_ha_ha_grew_up_completely_on_all",
		"ai_squash_cannot_to_aim_all_had_look_all_flow_out",
		"ai_squash_is_that_also_calculated_hit_look_for_person_which_has_the_strength",
		"ai_squash_dont_waste_your_time",
		"ai_squash_ha_this_sound_is_really_pleasant_to_hear",
		"ai_squash_i_eat_your_attack_to_grow",
		"ai_squash_time_to_hit_again_come_again",
		"ai_squash_only_useful_music_can_open_big_pumpkin_it_can_not_be_opened_with_weapon"
	};
	
	private static final String[] TOO_FAST_TEXT =
	{
		"ai_squash_looks_well_hit",
		"ai_squash_your_skill_mediocre",
		"ai_squash_time_hit_again",
		"ai_squash_eat_your_attack_grow",
		"ai_squash_make_effort_get_down_like_walked",
		"ai_squash_what_this_kind_degree_to_want_open_me",
		"ai_squash_good_fighting_method",
		"ai_squash_strives_excel_strength"
	};
	
	private static final String[] textSuccess0 =
	{
		"ai_squash_lovely_pumpkin_young_fruit_start_glisten_when_taken_threshing_ground",
		"ai_squash_haven_seen_for_long_time",
		"ai_squash_suddenly_thought_soon_see_my_beautiful_appearance",
		"ai_squash_well_this_something_nectar",
		"ai_squash_drink_bottles_be_able_grow_into_big_pumpkin"
	};
	
	private static final String[] textFail0 =
	{
		"ai_squash_if_drink_nectar_i_can_grow_up_faster",
		"ai_squash_come_believe_sprinkle_nectar_i_can_certainly_turn_big_pumpkin",
		"ai_squash_take_nectar_to_come_pumpkin_nectar"
	};
	
	private static final String[] textSuccess1 =
	{
		"ai_squash_wish_the_big_pumpkin",
		"ai_squash_completely_became_recreation_area",
		"ai_squash_guessed_am_mature_or_am_rotten",
		"ai_squash_nectar_is_just_the_best"
	};
	
	private static final String[] textFail1 =
	{
		"ai_squash_randomly_missed_too_quickly_sprinkles_the_nectar",
		"ai_squash_if_i_die_like_this_you_only_could_get_young_pumpkin",
		"ai_squash_cultivate_bit_faster_the_good_speech_becomes_the_big_pumpkin",
		"ai_squash_the_such_small_pumpkin_you_all_must_eat_bring_the_nectar_can_bigger"
	};
	
	private static final String[] textSuccess2 =
	{
		"ai_squash_young_pumpkin_wishing_has_how_already_grown_up",
		"ai_squash_already_grew_up_quickly_sneaked_off",
		"ai_squash_graciousness_is_very_good_come_again_to_see_now_felt_more_well"
	};
	
	private static final String[] textFail2 =
	{
		"ai_squash_hey_was_not_there_here_is_here_not_because_can_not_properly_care_small",
		"ai_squash_wow_stops_like_this_got_down_to_hav_thank",
		"ai_squash_hungry_for_a_nectar_oh",
		"ai_squash_do_you_want_the_big_pumpkin_but_like_young_pumpkin"
	};
	
	private static final String[] textSuccess3 =
	{
		"ai_squash_big_pumpkin_wishing_ask_to_sober",
		"ai_squash_rumble_rumble_it_really_tasty_hasn_it",
		"ai_squash_cultivating_me_just_to_eat_good_casual_your_not_to_give_the_manna_on_the_suicide"
	};
	
	private static final String[] textFail3 =
	{
		"ai_squash_isnt_it_the_water_you_add_what_flavor",
		"ai_squash_master_rescue_my_i_dont_have_the_nectar_flavor_i_must_die"
	};
	
	private static final String[] textSuccess4 =
	{
		"ai_squash_is_very_good_does_extremely_well_knew_what_next_step_should_make",
		"ai_squash_if_you_catch_me_i_give_you_10_million_adena_agree"
	};
	
	private static final String[] textFail4 =
	{
		"ai_squash_hungry_for_a_nectar_oh",
		"ai_squash_if_i_drink_nectar_i_can_grow_up_faster"
	};
	
	//@formatter:off
	private static int[][] DROPLIST =
	{
		// Low Quality Young Squash
		//    NpcID			ItemId min max chance						
		{ LOW_QUALITY_SQUASH, 4410, 1, 1, 50000 },   //Musical Score - Theme of Journey
		{ LOW_QUALITY_SQUASH, 4411, 1, 1, 50000 },   //Echo Crystal - Theme of Journey
		{ LOW_QUALITY_SQUASH, 4412, 1, 1, 50000 },   //Echo Crystal - Theme of Battle
		{ LOW_QUALITY_SQUASH, 4413, 1, 1, 50000 },   //Echo Crystal - Theme of Love
		{ LOW_QUALITY_SQUASH, 4414, 1, 1, 50000 },   //Echo Crystal - Theme of Solitude
		{ LOW_QUALITY_SQUASH, 4415, 1, 1, 50000 },   //Echo Crystal - Theme of the Feast
		{ LOW_QUALITY_SQUASH, 4416, 1, 1, 50000 },   //Echo Crystal - Theme of Celebration
		{ LOW_QUALITY_SQUASH, 4417, 1, 1, 50000 },   //Echo Crystal - Theme of Comedy
		
		// Low Quality Large Squash 
		{ LOW_QUALITY_LARGE_SQUASH, 4410, 1, 1, 50000 },   //Musical Score - Theme of Journey
		{ LOW_QUALITY_LARGE_SQUASH, 4411, 1, 1, 50000 },   //Echo Crystal - Theme of Journey
		{ LOW_QUALITY_LARGE_SQUASH, 4412, 1, 1, 50000 },   //Echo Crystal - Theme of Battle
		{ LOW_QUALITY_LARGE_SQUASH, 4413, 1, 1, 50000 },   //Echo Crystal - Theme of Love
		{ LOW_QUALITY_LARGE_SQUASH, 4414, 1, 1, 50000 },   //Echo Crystal - Theme of Solitude
		{ LOW_QUALITY_LARGE_SQUASH, 4415, 1, 1, 50000 },   //Echo Crystal - Theme of the Feast
		{ LOW_QUALITY_LARGE_SQUASH, 4416, 1, 1, 50000 },   //Echo Crystal - Theme of Celebration
		{ LOW_QUALITY_LARGE_SQUASH, 4417, 1, 1, 50000 },   //Echo Crystal - Theme of Comedy
		
		// High Quality Young Squash 
		{ HIGH_QUALITY_SQUASH, 1459, 5, 20, 150000 }, //Crystal: C-Grade
		{ HIGH_QUALITY_SQUASH, 1460, 5, 15, 100000 }, //Crystal: B-Grade
		{ HIGH_QUALITY_SQUASH, 1461, 5, 10, 50000 },  //Crystal: A-Grade
		{ HIGH_QUALITY_SQUASH, 1539, 1, 3, 500000 },  //Greater Healing Potion
		{ HIGH_QUALITY_SQUASH, 1374, 1, 5, 300000 },  //Greater Haste Potion
		{ HIGH_QUALITY_SQUASH, 1375, 1, 5, 200000 },  //Greater Swift Attack Potion
		{ HIGH_QUALITY_SQUASH, 954, 1, 4, 60000 },    //Crystal Scroll: Enchant Armor (C)
		{ HIGH_QUALITY_SQUASH, 953, 1, 2, 30000 },    //Crystal Scroll: Enchant Weapon (C)
		{ HIGH_QUALITY_SQUASH, 950, 1, 2, 40000 },    //Crystal Scroll: Enchant Armor (B)
		{ HIGH_QUALITY_SQUASH, 949, 1, 1, 20000 },    //Crystal Scroll: Enchant Weapon (B)
		{ HIGH_QUALITY_SQUASH, 732, 1, 2, 70000 },    //Crystal Scroll: Enchant Armor (A)
		{ HIGH_QUALITY_SQUASH, 731, 1, 1, 40000 },    //Crystal Scroll: Enchant Weapon (A)
		{ HIGH_QUALITY_SQUASH, 962, 1, 1, 10000 },    //Crystal Scroll: Enchant Armor (S)
		{ HIGH_QUALITY_SQUASH, 9156, 1, 3, 10000 },   //Blessed Scroll of Escape (Event)
		{ HIGH_QUALITY_SQUASH, 9157, 1, 3, 10000 },   //Blessed Scroll of Resurrection (Event)
		
		// High Quality Large Squash 
		{ HIGH_QUALITY_LARGE_SQUASH, 1459, 5, 20, 150000}, //Crystal: C-Grade
		{ HIGH_QUALITY_LARGE_SQUASH, 1460, 5, 15, 100000}, //Crystal: B-Grade
		{ HIGH_QUALITY_LARGE_SQUASH, 1461, 5, 10, 50000},  //Crystal: A-Grade
		{ HIGH_QUALITY_LARGE_SQUASH, 1539, 1, 3, 500000},  //Greater Healing Potion
		{ HIGH_QUALITY_LARGE_SQUASH, 1374, 1, 5, 300000},  //Greater Haste Potion
		{ HIGH_QUALITY_LARGE_SQUASH, 1375, 1, 5, 200000},  //Greater Swift Attack Potion
		{ HIGH_QUALITY_LARGE_SQUASH, 954, 1, 5, 60000},    //Crystal Scroll: Enchant Armor (C)
		{ HIGH_QUALITY_LARGE_SQUASH, 953, 1, 3, 30000},    //Crystal Scroll: Enchant Weapon (C)
		{ HIGH_QUALITY_LARGE_SQUASH, 950, 1, 4, 40000},    //Crystal Scroll: Enchant Armor (B)
		{ HIGH_QUALITY_LARGE_SQUASH, 949, 1, 2, 20000},    //Crystal Scroll: Enchant Weapon(B)
		{ HIGH_QUALITY_LARGE_SQUASH, 732, 1, 3, 70000},    //Crystal Scroll: Enchant Armor (A)
		{ HIGH_QUALITY_LARGE_SQUASH, 731, 1, 1, 45000},    //Crystal Scroll: Enchant Weapon (A)
		{ HIGH_QUALITY_LARGE_SQUASH, 962, 1, 1, 15000},    //Crystal Scroll: Enchant Armor (S)
		{ HIGH_QUALITY_LARGE_SQUASH, 8660, 1, 1, 10000},   //Demon Horns
		{ HIGH_QUALITY_LARGE_SQUASH, 8661, 1, 1, 10000},   //Mask of Spirits
		{ HIGH_QUALITY_LARGE_SQUASH, 9156, 1, 3, 20000},   //Blessed Scroll of Escape (Event)
		{ HIGH_QUALITY_LARGE_SQUASH, 9157, 1, 3, 20000},   //Blessed Scroll of Resurrection (Event)
		
		// King Squash 
		{ KING_SQUASH, 1459, 5, 20, 150000 }, //Crystal: C-Grade
		{ KING_SQUASH, 1460, 5, 15, 100000 }, //Crystal: B-Grade
		{ KING_SQUASH, 1461, 5, 10, 50000 },  //Crystal: A-Grade
		{ KING_SQUASH, 1539, 1, 3, 500000 },  //Greater Healing Potion
		{ KING_SQUASH, 1374, 1, 5, 300000 },  //Greater Haste Potion
		{ KING_SQUASH, 1375, 1, 5, 200000 },  //Greater Swift Attack Potion
		{ KING_SQUASH, 950, 1, 5, 40000 },    //Crystal Scroll: Enchant Armor (B)
		{ KING_SQUASH, 949, 1, 3, 20000 },    //Crystal Scroll: Enchant Weapon (B)
		{ KING_SQUASH, 732, 1, 2, 70000 },    //Crystal Scroll: Enchant Armor (A)
		{ KING_SQUASH, 731, 1, 1, 45000 },    //Crystal Scroll: Enchant Weapon (A)
		{ KING_SQUASH, 962, 1, 2, 15000 },    //Crystal Scroll: Enchant Armor (S)
		{ KING_SQUASH, 961, 1, 1, 10000 },    //Crystal Scroll: Enchant Weapon (S)
		{ KING_SQUASH, 20520, 1, 1, 2000 },   //Ancient Armor Enchant Crystal (S)
		{ KING_SQUASH, 20948, 1, 1, 10000 },  //Phoenix Agathion 7 Day Pack - Nirvana Rebirth
		{ KING_SQUASH, 20949, 1, 1, 10000 },  //Phoenix Agathion 7 Day Pack - Oriental Phoenix
		{ KING_SQUASH, 20994, 1, 1, 10000 },  //Three-headed Dragon Agathion Pack - Wind Walk
		{ KING_SQUASH, 21082, 1, 1, 10000 },  //Handy Agathion 7 Day Pack
		{ KING_SQUASH, 21067, 1, 1, 10000 },  //Granny Tiger Agathion 7 Day Pack 
		
		// Emperor Squash 
		{ EMPEROR_SQUASH, 1459, 5, 20, 150000}, //Crystal: C-Grade
		{ EMPEROR_SQUASH, 1460, 5, 15, 100000}, //Crystal: B-Grade
		{ EMPEROR_SQUASH, 1461, 5, 10, 50000},  //Crystal: A-Grade
		{ EMPEROR_SQUASH, 1539, 1, 3, 500000},  //Greater Healing Potion
		{ EMPEROR_SQUASH, 1374, 1, 5, 300000},  //Greater Haste Potion
		{ EMPEROR_SQUASH, 1375, 1, 5, 200000},  //Greater Swift Attack Potion
		{ EMPEROR_SQUASH, 950, 1, 5, 40000},    //Crystal Scroll: Enchant Armor (B)
		{ EMPEROR_SQUASH, 949, 1, 3, 20000},    //Crystal Scroll: Enchant Weapon (B)
		{ EMPEROR_SQUASH, 732, 1, 2, 70000},    //Crystal Scroll: Enchant Armor (A)
		{ EMPEROR_SQUASH, 731, 1, 1, 45000},    //Crystal Scroll: Enchant Weapon (A)
		{ EMPEROR_SQUASH, 962, 1, 2, 15000},    //Crystal Scroll: Enchant Armor (S)
		{ EMPEROR_SQUASH, 961, 1, 1, 10000},    //Crystal Scroll: Enchant Weapon (S)
		{ EMPEROR_SQUASH, 20519, 1, 1, 2000},   //Ancient Weapon Enchant Crystal (S)
		{ EMPEROR_SQUASH, 21070, 1, 1, 10000},  //Cheerleader Orodriel Agathion 7 Day Pack
		{ EMPEROR_SQUASH, 21072, 1, 1, 10000},  //Cheerleader Lana Agathion 7 Day Pack
		{ EMPEROR_SQUASH, 21074, 1, 1, 10000},  //Cheerleader Naonin Agathion 7 Day Pack
		{ EMPEROR_SQUASH, 21076, 1, 1, 10000},  //Cheerleader Mortia Agathion 7 Day Pack
		{ EMPEROR_SQUASH, 21078, 1, 1, 10000},  //Cheerleader Kaurin Agathion 7 Day Pack
		{ EMPEROR_SQUASH, 21080, 1, 1, 10000},  //Cheerleader Meruril Agathion 7 Day Pack 
	};
	//@formatter:on
	
	public Squash()
	{
		super(Squash.class.getSimpleName(), "ai/individual");
		
		addSpawnId(SQUASH);
		addSpawnId(CHRONO_SQUASH_LIST);
		addSkillSeeId(SQUASH);
		addAttackId(CHRONO_SQUASH_LIST);
		addKillId(CHRONO_SQUASH_LIST);
	}
	
	@Override
	public String onSpawn(L2Npc npc)
	{
		var npcId = npc.getId();
		
		npc.setIsImmobilized(true);
		npc.disableCoreAI(true);
		
		if (CHRONO_SQUASH_LIST.contains(npcId))
		{
			npc.setIsInvul(true);
		}
		else if (SQUASH.contains(npcId))
		{
			broadcastNpcSayCustom(npc, ChatType.GENERAL, SPAWN_TEXT[getRandom(SPAWN_TEXT.length)]);
		}
		return super.onSpawn(npc);
	}
	
	@Override
	public String onAttack(L2Npc npc, L2PcInstance attacker, int damage, boolean isPet)
	{
		if (CHRONO_SQUASH_LIST.contains(npc.getId()))
		{
			if ((attacker.getActiveWeaponItem() != null) && CHRONO_LIST.contains(attacker.getActiveWeaponItem().getId()))
			{
				if (getRandom(100) < 5)
				{
					broadcastNpcSayCustom(npc, ChatType.GENERAL, ATTACK_TEXT[getRandom(ATTACK_TEXT.length)]);
				}
				npc.setIsInvul(false);
				npc.getStatus().reduceHp(10, attacker);
			}
			else
			{
				if (getRandom(100) < 20)
				{
					broadcastNpcSayCustom(npc, ChatType.GENERAL, _NOCHRONO_TEXT[getRandom(_NOCHRONO_TEXT.length)]);
				}
				npc.setIsInvul(true);
			}
		}
		return super.onAttack(npc, attacker, damage, isPet);
	}
	
	@Override
	public String onSkillSee(L2Npc npc, L2PcInstance caster, Skill skill, List<L2Object> targets, boolean isSummon)
	{
		if (SQUASH.contains(npc.getId()) && (skill.getId() == NECTAR_SKILL))
		{
			switch (npc.getId())
			{
				case YOUNG_SQUASH:
					randomSpawn(KING_SQUASH, HIGH_QUALITY_SQUASH, LOW_QUALITY_SQUASH, npc, true);
					break;
				case LARGE_YOUNG_SQUASH:
					randomSpawn(EMPEROR_SQUASH, HIGH_QUALITY_LARGE_SQUASH, LOW_QUALITY_LARGE_SQUASH, npc, true);
					break;
			}
		}
		return super.onSkillSee(npc, caster, skill, targets, isSummon);
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance killer, boolean isPet)
	{
		if (CHRONO_SQUASH_LIST.contains(npc.getId()))
		{
			dropItem(npc, killer);
		}
		return super.onKill(npc, killer, isPet);
	}
	
	private void dropItem(L2Npc mob, L2PcInstance player)
	{
		final int npcId = mob.getId();
		
		broadcastNpcSayCustom(mob, ChatType.GENERAL, "ai_squash_pampkin_opens");
		broadcastNpcSayCustom(mob, ChatType.GENERAL, "ai_squash_pampkin_opens_thing_many");
		
		switch (npcId)
		{
			case LOW_QUALITY_SQUASH:
			case HIGH_QUALITY_SQUASH:
			case KING_SQUASH:
			case LOW_QUALITY_LARGE_SQUASH:
			case HIGH_QUALITY_LARGE_SQUASH:
			case EMPEROR_SQUASH:
				break;
			default:
				broadcastNpcSayCustom(mob, ChatType.GENERAL, "ai_squash_pampkin_had_died_like_this_could_obtain_nothing");
				broadcastNpcSayCustom(mob, ChatType.GENERAL, "ai_squash_pampkin_news_about_death_shouldnе_spread");
				return;
		}
		
		for (int[] drop : DROPLIST)
		{
			if (npcId < drop[0])
			{
				return;
			}
			
			if ((npcId == drop[0]) && (Rnd.get(100000 / 2) < (drop[4])))
			{
				mob.dropItem(player, drop[1], getRandom(drop[2], drop[3]));
				return;
			}
		}
	}
	
	private void randomSpawn(int low, int medium, int high, L2Npc npc, boolean delete)
	{
		switch (_tryCount)
		{
			case 0:
				_tryCount++;
				_lastNectarUse = System.currentTimeMillis();
				if (Rnd.chance(50))
				{
					_nectar++;
					broadcastNpcSayCustom(npc, ChatType.GENERAL, textSuccess0[getRandom(textSuccess0.length)]);
					npc.broadcastPacket(new MagicSkillUse(npc, npc, SQUASH_LEVEL_UP, 1, NECTAR_REUSE, 0));
				}
				else
				{
					broadcastNpcSayCustom(npc, ChatType.GENERAL, textFail0[getRandom(textFail0.length)]);
					npc.broadcastPacket(new MagicSkillUse(npc, npc, SQUASH_POISONED, 1, NECTAR_REUSE, 0));
				}
				break;
			case 1:
				if ((System.currentTimeMillis() - _lastNectarUse) < NECTAR_REUSE)
				{
					broadcastNpcSayCustom(npc, ChatType.GENERAL, TOO_FAST_TEXT[getRandom(TOO_FAST_TEXT.length)]);
					return;
				}
				_tryCount++;
				_lastNectarUse = System.currentTimeMillis();
				if (Rnd.chance(50))
				{
					_nectar++;
					broadcastNpcSayCustom(npc, ChatType.GENERAL, textSuccess1[getRandom(textSuccess1.length)]);
					npc.broadcastPacket(new MagicSkillUse(npc, npc, SQUASH_LEVEL_UP, 1, NECTAR_REUSE, 0));
				}
				else
				{
					broadcastNpcSayCustom(npc, ChatType.GENERAL, textFail1[getRandom(textFail1.length)]);
					npc.broadcastPacket(new MagicSkillUse(npc, npc, SQUASH_POISONED, 1, NECTAR_REUSE, 0));
				}
				break;
			case 2:
				if ((System.currentTimeMillis() - _lastNectarUse) < NECTAR_REUSE)
				{
					broadcastNpcSayCustom(npc, ChatType.GENERAL, TOO_FAST_TEXT[getRandom(TOO_FAST_TEXT.length)]);
					return;
				}
				_tryCount++;
				_lastNectarUse = System.currentTimeMillis();
				if (Rnd.chance(50))
				{
					_nectar++;
					broadcastNpcSayCustom(npc, ChatType.GENERAL, textSuccess2[getRandom(textSuccess2.length)]);
					npc.broadcastPacket(new MagicSkillUse(npc, npc, SQUASH_LEVEL_UP, 1, NECTAR_REUSE, 0));
				}
				else
				{
					broadcastNpcSayCustom(npc, ChatType.GENERAL, textFail2[getRandom(textFail2.length)]);
					npc.broadcastPacket(new MagicSkillUse(npc, npc, SQUASH_POISONED, 1, NECTAR_REUSE, 0));
				}
				break;
			case 3:
				if ((System.currentTimeMillis() - _lastNectarUse) < NECTAR_REUSE)
				{
					broadcastNpcSayCustom(npc, ChatType.GENERAL, TOO_FAST_TEXT[getRandom(TOO_FAST_TEXT.length)]);
					return;
				}
				_tryCount++;
				_lastNectarUse = System.currentTimeMillis();
				if (Rnd.chance(50))
				{
					_nectar++;
					broadcastNpcSayCustom(npc, ChatType.GENERAL, textSuccess3[getRandom(textSuccess3.length)]);
					npc.broadcastPacket(new MagicSkillUse(npc, npc, SQUASH_LEVEL_UP, 1, NECTAR_REUSE, 0));
				}
				else
				{
					broadcastNpcSayCustom(npc, ChatType.GENERAL, textFail3[getRandom(textFail3.length)]);
					npc.broadcastPacket(new MagicSkillUse(npc, npc, SQUASH_POISONED, 1, NECTAR_REUSE, 0));
				}
				break;
			case 4:
				if ((System.currentTimeMillis() - _lastNectarUse) < NECTAR_REUSE)
				{
					broadcastNpcSayCustom(npc, ChatType.GENERAL, TOO_FAST_TEXT[getRandom(TOO_FAST_TEXT.length)]);
					return;
				}
				_tryCount++;
				_lastNectarUse = System.currentTimeMillis();
				if (Rnd.chance(50))
				{
					_nectar++;
					broadcastNpcSayCustom(npc, ChatType.GENERAL, textSuccess4[getRandom(textSuccess4.length)]);
					npc.broadcastPacket(new MagicSkillUse(npc, npc, SQUASH_LEVEL_UP, 1, NECTAR_REUSE, 0));
				}
				else
				{
					broadcastNpcSayCustom(npc, ChatType.GENERAL, textFail4[getRandom(textFail4.length)]);
					npc.broadcastPacket(new MagicSkillUse(npc, npc, SQUASH_POISONED, 1, NECTAR_REUSE, 0));
				}
				handleSpawnAttempt(low, medium, high, npc);
				break;
		}
	}
	
	private void handleSpawnAttempt(int low, int medium, int high, L2Npc npc)
	{
		if (_nectar < 3)
		{
			spawnNext(low, npc);
		}
		else if (_nectar == 5)
		{
			spawnNext(medium, npc);
		}
		else
		{
			spawnNext(high, npc);
		}
	}
	
	private void spawnNext(int npcId, L2Npc npc)
	{
		_tryCount = 0;
		addSpawn(npcId, npc.getX(), npc.getY(), npc.getZ(), npc.getHeading(), false, 60000);
		npc.deleteMe();
	}
}