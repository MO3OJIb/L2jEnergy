/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.group_template;

import com.l2jserver.gameserver.SevenSignsFestival;
import com.l2jserver.gameserver.enums.CategoryType;
import com.l2jserver.gameserver.model.L2Party;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.Util;

import ai.npc.AbstractNpcAI;

/**
 * Festival of Darkness mobs
 * @author Charus
 */
public final class FestivalMobs extends AbstractNpcAI
{
	// @formatter:off
	private static final int[] DAWN_NPCS = 
	{
		18099, 18101, 18102, 18103, 18104, 18105, 18106, 18107, 18108, 18118, 18317, // room5
		18089, 18091, 18092, 18093, 18094, 18095, 18096, 18097, 18098, 18117, 18315, // room4
		18079, 18081, 18082, 18083, 18084, 18085, 18086, 18087, 18088, 18116, 18313, // room3
		18069, 18071, 18072, 18073, 18074, 18075, 18076, 18077, 18078, 18115, 18311, // room2
		18059, 18061, 18062, 18063, 18064, 18065, 18066, 18067, 18068, 18114, 18309  // room1
	};
	
	private static final int[] DUSK_NPCS = 
	{
		18049, 18051, 18052, 18053, 18054, 18055, 18056, 18057, 18058, 18113, 18307, // room5
		18039, 18041, 18042, 18043, 18044, 18045, 18046, 18047, 18048, 18112, 18305, // room4
		18029, 18031, 18032, 18033, 18034, 18035, 18036, 18037, 18038, 18111, 18303, // room3
		18019, 18021, 18022, 18023, 18024, 18025, 18026, 18027, 18028, 18110, 18301, // room2
		18009, 18011, 18012, 18013, 18014, 18015, 18016, 18017, 18018, 18109, 18299  // room1
	};// @formatter:on
	
	private static final int Q00505_BloodOffering = 505;
	
	public FestivalMobs()
	{
		super(FestivalMobs.class.getSimpleName(), "ai/group_template");
		
		addKillId(DAWN_NPCS);
		addKillId(DUSK_NPCS);
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance killer, boolean isSummon)
	{
		L2PcInstance c0 = killer.getActingPlayer();
		if (c0 != null)
		{
			L2Party party0 = c0.getParty();
			if (party0 != null)
			{
				c0 = party0.getLeader();
				
				if (haveMemo(c0, Q00505_BloodOffering))
				{
					int i1 = 3 + ((3 * 25) / 100);
					
					final double distance = Util.calculateDistance(npc, c0, true, false);
					if (c0.isInCategory(CategoryType.WIZARD_GROUP))
					{
						if (distance < 40)
						{
							i1 = i1 + ((i1 * 30) / 100);
						}
					}
					else
					{
						if (distance < 80)
						{
							i1 = i1 + ((i1 * 30) / 100);
						}
					}
					
					if (c0.isInCategory(CategoryType.WIZARD_GROUP))
					{
						if (getRandom(0, 2) > 0)
						{
							i1 = i1 + ((i1 * 7) / 100);
						}
						else
						{
							i1 = i1 - ((i1 * 15) / 100);
						}
					}
					else
					{
						if (getRandom(0, 2) > 0)
						{
							i1 = i1 + ((i1 * 15) / 100);
						}
						else
						{
							i1 = i1 - ((i1 * 7) / 100);
						}
					}
					
					SystemMessage smsg = SystemMessage.getSystemMessage(SystemMessageId.LEADER_OBTAINED_S2_OF_S1);
					smsg.addItemName(SevenSignsFestival.FESTIVAL_OFFERING_ID);
					smsg.addLong(i1);
					party0.broadcastToPartyMembers(c0, smsg);
					
					giveItems(c0, SevenSignsFestival.FESTIVAL_OFFERING_ID, i1);
				}
			}
		}
		
		return super.onKill(npc, killer, isSummon);
	}
}