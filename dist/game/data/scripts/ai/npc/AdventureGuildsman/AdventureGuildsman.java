/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.npc.AdventureGuildsman;

import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;

import com.l2jserver.gameserver.configuration.config.events.PcCafeConfig;
import com.l2jserver.gameserver.data.xml.impl.SkillData;
import com.l2jserver.gameserver.data.xml.impl.TeleportLocationData;
import com.l2jserver.gameserver.enums.PcCafeType;
import com.l2jserver.gameserver.enums.skills.CommonSkill;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.instance.L2ServitorInstance;
import com.l2jserver.gameserver.model.variables.PlayerVariables;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ExPCCafePointInfo;
import com.l2jserver.gameserver.network.serverpackets.ShowPCCafeCouponShowUI;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

import ai.npc.AbstractNpcAI;

/**
 * AdventureGuildsman AI
 * @author Мо3олЬ
 */
public class AdventureGuildsman extends AbstractNpcAI
{
	private static final Map<String, int[]> PETSKILL = new ConcurrentHashMap<>();
	private static final Map<String, int[]> POINTSSKILL = new ConcurrentHashMap<>();
	
	private boolean WYVERN_ACTIVE = false;
	
	// Items
	private static final int PCCAFE_LOTTERY_TICKET_30DAYS = 15358;
	private static final int PCCAFE_1ST_LOTTERY_TICKET_30DAYS = 15359; // билет на выдачу 100000 очков РС-клуба
	private static final int PCCAFE_2ND_LOTTERY_TICKET_30DAYS = 15360; // билет на выдачу 10000 очков РС-клуба
	private static final int PCCAFE_3RD_LOTTERY_TICKET_30DAYS = 15361; // билет на выдачу 2000 очков РС-клуба
	private static final int PCCAFE_4TH_LOTTERY_TICKET_30DAYS = 15362; // билет на выдачу 1000 очков РС-клуба
	private static final int PCCAFE_5TH_LOTTERY_TICKET_30DAYS = 15363; // билет на выдачу 100 очков РС-клуба
	
	// @formatter:off
	private static final int[] ADVENTURERS_GUILDSMAN =
	{
		31775, 31776, 31777, 31778, 31779, 31780, 31781, 31782, 31783, 31784, 31785, 31786, 
		31787, 31788, 31789, 31790, 31791, 31792, 31793, 31794, 31795, 31796, 31797, 31798, 
		31799, 31800, 31801, 31802, 31803, 31804, 31805, 31806, 31807, 31808, 31809, 31810, 
		31811, 31812, 31813, 31814, 31815, 31816, 31817, 31818, 31819, 31820, 31821, 31822, 
		31823, 31824, 31825, 31826, 31827, 31828, 31829, 31830, 31831, 31832, 31833, 31834, 
		31835, 31836, 31837, 31838, 31839, 31840, 31841, 31991, 31992, 31993, 31994, 31995, 
		32337, 32338, 32339, 32340
	};
 
	static
	{
		// баф для персонажей ниже 40 ур.    ID   LV  Points
		POINTSSKILL.put("S401", new int[] { 4397, 1, 300 }); // Berserker Spirit - 300 points
		POINTSSKILL.put("S402", new int[] { 4393, 1, 200 }); // Might - 200 points
		POINTSSKILL.put("S403", new int[] { 4392, 1, 100 }); // Shield - 100 points
		POINTSSKILL.put("S404", new int[] { 4391, 1, 200 }); // Wind Walk - 200 points
		POINTSSKILL.put("S405", new int[] { 4404, 1, 550 }); // Focus - 550 points
		POINTSSKILL.put("S406", new int[] { 4396, 1, 300 }); // Magic Barrier - 300 points
		POINTSSKILL.put("S407", new int[] { 4405, 1, 650 }); // Death Whisper - 650 points
		POINTSSKILL.put("S408", new int[] { 4403, 1, 200 }); // Guidance - 200 points
		POINTSSKILL.put("S409", new int[] { 4398, 1, 100 }); // Bless Shield - 100 points
		POINTSSKILL.put("S410", new int[] { 4394, 1, 200 }); // Blessed Body - 200 points
		POINTSSKILL.put("S411", new int[] { 4395, 1, 200 }); // Blessed Soul - 200 points
		POINTSSKILL.put("S412", new int[] { 4402, 1, 400 }); // Haste - 400 points
		POINTSSKILL.put("S413", new int[] { 4406, 1, 200 }); // Agility - 200 points
		POINTSSKILL.put("S414", new int[] { 4399, 1, 200 }); // Vampiric Rage - 200 points
		POINTSSKILL.put("S415", new int[] { 4401, 1, 200 }); // Empower - 200 points
		POINTSSKILL.put("S416", new int[] { 4400, 1, 400 }); // Acumen - 400 points

		//баф для персонажей 40-54 ур.      ID   LV  Points
		POINTSSKILL.put("S417", new int[] { 4397, 1, 300 }); // Berserker Spirit - 300 points
		POINTSSKILL.put("S418", new int[] { 4393, 2, 300 }); // Might - 300 points
		POINTSSKILL.put("S419", new int[] { 4392, 2, 150 }); // Shield - 150 points
		POINTSSKILL.put("S420", new int[] { 4391, 2, 300 }); // Wind Walk - 300 points
		POINTSSKILL.put("S421", new int[] { 4404, 2, 650 }); // Focus - 650 points
		POINTSSKILL.put("S422", new int[] { 4396, 1, 300 }); // Magic Barrier - 300 points
		POINTSSKILL.put("S423", new int[] { 4405, 2, 800 }); // Death Whisper - 800 points
		POINTSSKILL.put("S424", new int[] { 4403, 2, 300 }); // Guidance - 300 points
		POINTSSKILL.put("S425", new int[] { 4398, 2, 150 }); // Bless Shield - 150 points
		POINTSSKILL.put("S426", new int[] { 4394, 3, 300 }); // Blessed Body - 300 points
		POINTSSKILL.put("S427", new int[] { 4395, 3, 300 }); // Blessed Soul - 300 points
		POINTSSKILL.put("S428", new int[] { 4402, 1, 400 }); // Haste - 400 points
		POINTSSKILL.put("S429", new int[] { 4406, 2, 300 }); // Agility - 300 points
		POINTSSKILL.put("S430", new int[] { 4399, 2, 300 }); // Vampiric Rage - 300 points
		POINTSSKILL.put("S431", new int[] { 4401, 2, 300 }); // Empower - 300 points
		POINTSSKILL.put("S432", new int[] { 4400, 2, 600 }); // Acumen - 600 points
		
		//баф для персонажей 55 и выше ур.  ID   LV  Points
		POINTSSKILL.put("S433", new int[] { 4397, 2, 500 }); // Berserker Spirit - 500 points
		POINTSSKILL.put("S434", new int[] { 4393, 3, 400 }); // Might - 400 points
		POINTSSKILL.put("S435", new int[] { 4392, 3, 200 }); // Shield - 200 points
		POINTSSKILL.put("S436", new int[] { 4391, 2, 300 }); // Wind Walk - 300 points
		POINTSSKILL.put("S437", new int[] { 4404, 3, 800 }); // Focus - 800 points
		POINTSSKILL.put("S438", new int[] { 4396, 2, 400 }); // Magic Barrier - 400 points
		POINTSSKILL.put("S439", new int[] { 4405, 3, 950 }); // Death Whisper - 950 points
		POINTSSKILL.put("S440", new int[] { 4403, 3, 400 }); // Guidance - 400 points
		POINTSSKILL.put("S441", new int[] { 4398, 3, 200 }); // Bless Shield - 200 points
		POINTSSKILL.put("S442", new int[] { 4394, 4, 400 }); // Blessed Body - 400 points
		POINTSSKILL.put("S443", new int[] { 4395, 4, 400 }); // Blessed Soul - 400 points
		POINTSSKILL.put("S444", new int[] { 4402, 2, 950 }); // Haste - 950 points
		POINTSSKILL.put("S445", new int[] { 4406, 3, 400 }); // Agility - 400 points
		POINTSSKILL.put("S446", new int[] { 4399, 3, 400 }); // Vampiric Rage - 400 points
		POINTSSKILL.put("S447", new int[] { 4401, 3, 400 }); // Empower - 400 points
		POINTSSKILL.put("S448", new int[] { 4400, 3, 950 }); // Acumen - 950 points

		// Pet Skill                      ID   LV  QTY Points
		PETSKILL.put("P4391", new int[] { 4397, 1, 300 }); // Berserker Spirit - 300 points
		PETSKILL.put("P4392", new int[] { 4393, 2, 300 }); // Might - 300 points
		PETSKILL.put("P4393", new int[] { 4392, 2, 150 }); // Shield - 150 points
		PETSKILL.put("P4394", new int[] { 4391, 2, 300 }); // Wind Walk - 300 points
		PETSKILL.put("P4395", new int[] { 4404, 2, 650 }); // Focus - 650 points
		PETSKILL.put("P4396", new int[] { 4396, 1, 300 }); // Magic Barrier - 300 points
		PETSKILL.put("P4397", new int[] { 4405, 2, 800 }); // Death Whisper - 800 points
		PETSKILL.put("P4398", new int[] { 4403, 2, 300 }); // Guidance - 300 points
		PETSKILL.put("P4399", new int[] { 4398, 2, 150 }); // Bless Shield - 150 points
		PETSKILL.put("P4400", new int[] { 4394, 3, 300 }); // Blessed Body - 300 points
		PETSKILL.put("P4401", new int[] { 4395, 3, 300 }); // Blessed Soul - 300 points
		PETSKILL.put("P4402", new int[] { 4402, 1, 400 }); // Haste - 400 points
		PETSKILL.put("P4403", new int[] { 4406, 2, 300 }); // Agility - 300 points
		PETSKILL.put("P4404", new int[] { 4399, 2, 300 }); // Vampiric Rage - 300 points
		PETSKILL.put("P4405", new int[] { 4401, 2, 300 }); // Empower - 300 points
		PETSKILL.put("P4406", new int[] { 4400, 2, 600 }); // Acumen - 600 points
	}
	// @formatter:on
	
	public AdventureGuildsman()
	{
		super(AdventureGuildsman.class.getSimpleName(), "ai/npc");
		addStartNpc(ADVENTURERS_GUILDSMAN);
		addTalkId(ADVENTURERS_GUILDSMAN);
		addFirstTalkId(ADVENTURERS_GUILDSMAN);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		String htmtext = null;
		var st = new StringTokenizer(event, " ");
		switch (st.nextToken())
		{
			case "pccafe_coupon" -> htmtext = "pccoupon001.htm";
			case "show_pccafe_coupon_ui" ->
			{
				if (player.getVariables().getLong(PlayerVariables.PCC_CODE_ATTEMPTS, 1) < PcCafeConfig.ALT_PCBANG_POINTS_MAX_CODE_ENTER_ATTEMPTS)
				{
					player.sendPacket(ShowPCCafeCouponShowUI.STATIC);
				}
				else
				{
					htmtext = "max_attempts.htm";
				}
			}
			case "pccafe_list" -> htmtext = "pccafe_list001.htm";
			case "life_crystal" -> htmtext = npc.getId() + "-1.htm";
			case "item_list" -> htmtext = "pccafe_item001.htm";
			case "buff_list" ->
			{
				if (checkFirstConditions(player))
				{
					if (player.getLevel() < 40)
					{
						htmtext = "pccafe_buff_1001.htm";
					}
					else if ((player.getLevel() >= 40) & (player.getLevel() < 54))
					{
						htmtext = "pccafe_buff_2001.htm";
					}
					else if (player.getLevel() >= 55)
					{
						htmtext = "pccafe_buff_3001.htm";
					}
				}
				else
				{
					htmtext = "pccafe_buff_warning.htm";
				}
			}
			case "buff_list_pet" -> htmtext = "pccafe_summon_newbuff_001.htm";
			case "life_crystal_help.htm", "adventure_manager_help.htm", "raid_boss_position.htm", "pccafe_help_inzone001.htm", "pccafe_help_lottery001.htm", "pccafe_help_lottery002.htm", "pccafe_buff_1002.htm", "pccafe_buff_2002.htm", "pccafe_buff_3002.htm", "pccafe_buff_warning.htm", "pccafe_summon_newbuff_002.htm", "pccoupon001.htm", "pccoupon_error_under20.htm", "pccafe_wyvern001.htm" -> htmtext = event;
			case "tele" -> htmtext = npc.getId() + "-2.htm";
			case "goto" ->
			{
				var list = TeleportLocationData.getInstance().getTeleportLocation(Integer.parseInt(st.nextToken()));
				if (list != null)
				{
					if (player.getPcCafeSystem().getPoints() >= list.getPrice())
					{
						if (takeItems(player, list.getItemId(), list.getPrice()))
						{
							player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() - (list.getPrice()));
							var smsgpc = SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT);
							smsgpc.addInt(list.getPrice());
							player.sendPacket(smsgpc);
							player.teleToLocation(list.getLocX(), list.getLocY(), list.getLocZ());
							player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), -list.getPrice(), 1, PcCafeType.CONSUME, 12));
							return null;
						}
					}
					else
					{
						htmtext = "pccafe_notpoint001.htm";
					}
				}
			}
			case "give_lottery_ticket" ->
			{
				if (!player.getVariables().getBoolean(PlayerVariables.USED_PC_LOTTERY_TICKET, false))
				{
					player.getVariables().set(PlayerVariables.USED_PC_LOTTERY_TICKET, true);
					giveItems(player, PCCAFE_LOTTERY_TICKET_30DAYS, 1);
				}
				else
				{
					htmtext = "pccafe_help_lottery_notoneday.htm";
				}
			}
			case "trade_100" -> htmtext = tradeItem(player, PCCAFE_5TH_LOTTERY_TICKET_30DAYS, 100);
			case "trade_1000" -> htmtext = tradeItem(player, PCCAFE_4TH_LOTTERY_TICKET_30DAYS, 1000);
			case "trade_2000" -> htmtext = tradeItem(player, PCCAFE_3RD_LOTTERY_TICKET_30DAYS, 2000);
			case "trade_10000" -> htmtext = tradeItem(player, PCCAFE_2ND_LOTTERY_TICKET_30DAYS, 10000);
			case "trade_100000" -> htmtext = tradeItem(player, PCCAFE_1ST_LOTTERY_TICKET_30DAYS, 100000);
		}
		
		if (event.equalsIgnoreCase("warrior40"))
		{
			if (player.getPcCafeSystem().getPoints() >= 3100)
			{
				player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() - 3100);
				player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT).addLong(3100));
				player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), -3100, 1, PcCafeType.CONSUME, 12));
				npc.setTarget(player);
				npc.doCast(SkillData.getInstance().getSkill(4397, 1)); // Berserker Spirit
				npc.doCast(SkillData.getInstance().getSkill(4393, 1)); // Might
				npc.doCast(SkillData.getInstance().getSkill(4392, 1)); // Shield
				npc.doCast(SkillData.getInstance().getSkill(4391, 1)); // Wind Walk
				npc.doCast(SkillData.getInstance().getSkill(4404, 1)); // Focus
				npc.doCast(SkillData.getInstance().getSkill(4405, 1)); // Death Whisper
				npc.doCast(SkillData.getInstance().getSkill(4403, 1)); // Guidance
				npc.doCast(SkillData.getInstance().getSkill(4398, 1)); // Bless Shield
				npc.doCast(SkillData.getInstance().getSkill(4394, 1)); // Blessed Body
				npc.doCast(SkillData.getInstance().getSkill(4402, 1)); // Haste
				npc.doCast(SkillData.getInstance().getSkill(4406, 1)); // Agility
				npc.doCast(SkillData.getInstance().getSkill(4399, 1)); // Vampiric Rage
				htmtext = "pccafe_buff_1001.htm";
			}
			else
			{
				htmtext = "pccafe_notpoint001.htm";
			}
		}
		else if (event.equalsIgnoreCase("mage40"))
		{
			if (player.getPcCafeSystem().getPoints() >= 1600)
			{
				player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() - 1600);
				player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT).addLong(1600));
				player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), -1600, 1, PcCafeType.CONSUME, 12));
				npc.setTarget(player);
				npc.doCast(SkillData.getInstance().getSkill(4397, 1)); // Berserker Spirit
				npc.doCast(SkillData.getInstance().getSkill(4396, 1)); // Magic Barrier
				npc.doCast(SkillData.getInstance().getSkill(4392, 1)); // Shield
				npc.doCast(SkillData.getInstance().getSkill(4391, 1)); // Wind Walk
				npc.doCast(SkillData.getInstance().getSkill(4394, 1)); // Blessed Body
				npc.doCast(SkillData.getInstance().getSkill(4395, 1)); // Blessed Soul
				npc.doCast(SkillData.getInstance().getSkill(4401, 1)); // Empower
				npc.doCast(SkillData.getInstance().getSkill(4400, 1)); // Acumen
				htmtext = "pccafe_buff_1001.htm";
			}
			else
			{
				htmtext = "pccafe_notpoint001.htm";
			}
		}
		else if (event.equalsIgnoreCase("warrior54"))
		{
			if (player.getPcCafeSystem().getPoints() >= 4000)
			{
				player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() - 4000);
				player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT).addLong(4000));
				player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), -4000, 1, PcCafeType.CONSUME, 12));
				npc.setTarget(player);
				npc.doCast(SkillData.getInstance().getSkill(4397, 1)); // Berserker Spirit
				npc.doCast(SkillData.getInstance().getSkill(4393, 2)); // Might
				npc.doCast(SkillData.getInstance().getSkill(4392, 2)); // Shield
				npc.doCast(SkillData.getInstance().getSkill(4391, 2)); // Wind Walk
				npc.doCast(SkillData.getInstance().getSkill(4404, 2)); // Focus
				npc.doCast(SkillData.getInstance().getSkill(4405, 2)); // Death Whisper
				npc.doCast(SkillData.getInstance().getSkill(4403, 2)); // Guidance
				npc.doCast(SkillData.getInstance().getSkill(4398, 2)); // Bless Shield
				npc.doCast(SkillData.getInstance().getSkill(4394, 3)); // Blessed Body
				npc.doCast(SkillData.getInstance().getSkill(4402, 1)); // Haste
				npc.doCast(SkillData.getInstance().getSkill(4406, 2)); // Agility
				npc.doCast(SkillData.getInstance().getSkill(4399, 2)); // Vampiric Rage
				htmtext = "pccafe_buff_2001.htm";
			}
			else
			{
				htmtext = "pccafe_notpoint001.htm";
			}
		}
		else if (event.equalsIgnoreCase("mage54"))
		{
			if (player.getPcCafeSystem().getPoints() >= 2100)
			{
				player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() - 2100);
				player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT).addLong(2100));
				player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), -2100, 1, PcCafeType.CONSUME, 12));
				npc.setTarget(player);
				npc.doCast(SkillData.getInstance().getSkill(4397, 1)); // Berserker Spirit
				npc.doCast(SkillData.getInstance().getSkill(4396, 1)); // Magic Barrier
				npc.doCast(SkillData.getInstance().getSkill(4392, 2)); // Shield
				npc.doCast(SkillData.getInstance().getSkill(4391, 2)); // Wind Walk
				npc.doCast(SkillData.getInstance().getSkill(4394, 3)); // Blessed Body
				npc.doCast(SkillData.getInstance().getSkill(4395, 3)); // Blessed Soul
				npc.doCast(SkillData.getInstance().getSkill(4401, 2)); // Empower
				npc.doCast(SkillData.getInstance().getSkill(4400, 2)); // Acumen
				htmtext = "pccafe_buff_2001.htm";
			}
			else
			{
				htmtext = "pccafe_notpoint001.htm";
			}
		}
		else if (event.equalsIgnoreCase("warrior55"))
		{
			if (player.getPcCafeSystem().getPoints() >= 5600)
			{
				player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() - 5600);
				player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT).addLong(5600));
				player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), -5600, 1, PcCafeType.CONSUME, 12));
				npc.setTarget(player);
				npc.doCast(SkillData.getInstance().getSkill(4397, 2)); // Berserker Spirit
				npc.doCast(SkillData.getInstance().getSkill(4393, 3)); // Might
				npc.doCast(SkillData.getInstance().getSkill(4392, 3)); // Shield
				npc.doCast(SkillData.getInstance().getSkill(4391, 2)); // Wind Walk
				npc.doCast(SkillData.getInstance().getSkill(4404, 3)); // Focus
				npc.doCast(SkillData.getInstance().getSkill(4405, 3)); // Death Whisper
				npc.doCast(SkillData.getInstance().getSkill(4403, 3)); // Guidance
				npc.doCast(SkillData.getInstance().getSkill(4398, 3)); // Bless Shield
				npc.doCast(SkillData.getInstance().getSkill(4394, 4)); // Blessed Body
				npc.doCast(SkillData.getInstance().getSkill(4402, 2)); // Haste
				npc.doCast(SkillData.getInstance().getSkill(4406, 3)); // Agility
				npc.doCast(SkillData.getInstance().getSkill(4399, 3)); // Vampiric Rage
				htmtext = "pccafe_buff_3001.htm";
			}
			else
			{
				htmtext = "pccafe_notpoint001.htm";
			}
		}
		else if (event.equalsIgnoreCase("mage55"))
		{
			if (player.getPcCafeSystem().getPoints() >= 3000)
			{
				player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() - 3000);
				player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT).addLong(3000));
				player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), -3000, 1, PcCafeType.CONSUME, 12));
				npc.setTarget(player);
				npc.doCast(SkillData.getInstance().getSkill(4397, 2)); // Berserker Spirit
				npc.doCast(SkillData.getInstance().getSkill(4396, 2)); // Magic Barrier
				npc.doCast(SkillData.getInstance().getSkill(4392, 3)); // Shield
				npc.doCast(SkillData.getInstance().getSkill(4391, 2)); // Wind Walk
				npc.doCast(SkillData.getInstance().getSkill(4394, 4)); // Blessed Body
				npc.doCast(SkillData.getInstance().getSkill(4395, 4)); // Blessed Soul
				npc.doCast(SkillData.getInstance().getSkill(4401, 3)); // Empower
				npc.doCast(SkillData.getInstance().getSkill(4400, 3)); // Acumen
				htmtext = "pccafe_buff_3001.htm";
			}
			else
			{
				htmtext = "pccafe_notpoint001.htm";
			}
		}
		else if (POINTSSKILL.containsKey(event))
		{
			if (player.getLevel() < 40) // ниже 40-го уровня
			{
				if (player.getPcCafeSystem().getPoints() >= POINTSSKILL.get(event)[2])
				{
					player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() - POINTSSKILL.get(event)[2]);
					SystemMessage smsgpc = SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT);
					smsgpc.addInt(POINTSSKILL.get(event)[2]);
					player.sendPacket(smsgpc);
					player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), -POINTSSKILL.get(event)[2], 1, PcCafeType.CONSUME, 12));
					npc.setTarget(player);
					npc.doCast(SkillData.getInstance().getSkill(POINTSSKILL.get(event)[0], POINTSSKILL.get(event)[1]));
					return "pccafe_buff_1002.htm";
				}
				htmtext = "pccafe_notpoint001.htm";
			}
			else if ((player.getLevel() >= 40) & (player.getLevel() < 54)) // для 40-54 уровня
			{
				if (player.getPcCafeSystem().getPoints() >= POINTSSKILL.get(event)[2])
				{
					player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() - POINTSSKILL.get(event)[2]);
					SystemMessage smsgpc = SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT);
					smsgpc.addInt(POINTSSKILL.get(event)[2]);
					player.sendPacket(smsgpc);
					player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), -POINTSSKILL.get(event)[2], 1, PcCafeType.CONSUME, 12));
					npc.setTarget(player);
					npc.doCast(SkillData.getInstance().getSkill(POINTSSKILL.get(event)[0], POINTSSKILL.get(event)[1]));
					return "pccafe_buff_2002.htm";
				}
				htmtext = "pccafe_notpoint001.htm";
			}
			else if (player.getLevel() >= 55) // выше 55-го уровня
			{
				if (player.getPcCafeSystem().getPoints() >= POINTSSKILL.get(event)[2])
				{
					player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() - POINTSSKILL.get(event)[2]);
					SystemMessage smsgpc = SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT);
					smsgpc.addInt(POINTSSKILL.get(event)[2]);
					player.sendPacket(smsgpc);
					player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), -POINTSSKILL.get(event)[2], 1, PcCafeType.CONSUME, 12));
					npc.setTarget(player);
					npc.doCast(SkillData.getInstance().getSkill(POINTSSKILL.get(event)[0], POINTSSKILL.get(event)[1]));
					return "pccafe_buff_3002.htm";
				}
				htmtext = "pccafe_notpoint001.htm";
			}
			else
			{
				htmtext = "pccafe_skill_nolevel.htm";
			}
		}
		else if (event.equalsIgnoreCase("pet_warrior"))
		{
			if ((player.getSummon() == null) || !(player.getSummon() instanceof L2ServitorInstance))
			{
				htmtext = "pccafe_notsummon.htm";
			}
			else if (player.getPcCafeSystem().getPoints() >= 4000)
			{
				player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() - 4000);
				player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT).addLong(4000));
				player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), -4000, 1, PcCafeType.CONSUME, 12));
				npc.setTarget(player.getSummon());
				npc.doCast(SkillData.getInstance().getSkill(4397, 1));
				npc.doCast(SkillData.getInstance().getSkill(4393, 2));
				npc.doCast(SkillData.getInstance().getSkill(4392, 2));
				npc.doCast(SkillData.getInstance().getSkill(4391, 2));
				npc.doCast(SkillData.getInstance().getSkill(4404, 2));
				npc.doCast(SkillData.getInstance().getSkill(4396, 1));
				npc.doCast(SkillData.getInstance().getSkill(4405, 2));
				npc.doCast(SkillData.getInstance().getSkill(4403, 2));
				npc.doCast(SkillData.getInstance().getSkill(4398, 2));
				npc.doCast(SkillData.getInstance().getSkill(4394, 3));
				npc.doCast(SkillData.getInstance().getSkill(4402, 1));
				npc.doCast(SkillData.getInstance().getSkill(4406, 2));
				npc.doCast(SkillData.getInstance().getSkill(4399, 2));
				htmtext = "pccafe_summon_newbuff_001.htm";
			}
			else
			{
				htmtext = "pccafe_notpoint001.htm";
			}
		}
		else if (event.equalsIgnoreCase("pet_mage"))
		{
			if ((player.getSummon() == null) || !(player.getSummon() instanceof L2ServitorInstance))
			{
				htmtext = "pccafe_notsummon.htm";
			}
			else if (player.getPcCafeSystem().getPoints() >= 2100)
			{
				player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() - 2100);
				player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT).addLong(2100));
				player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), -2100, 1, PcCafeType.CONSUME, 12));
				npc.setTarget(player.getSummon());
				npc.doCast(SkillData.getInstance().getSkill(4397, 1));
				npc.doCast(SkillData.getInstance().getSkill(4396, 1));
				npc.doCast(SkillData.getInstance().getSkill(4392, 2));
				npc.doCast(SkillData.getInstance().getSkill(4391, 2));
				npc.doCast(SkillData.getInstance().getSkill(4395, 3));
				npc.doCast(SkillData.getInstance().getSkill(4401, 2));
				npc.doCast(SkillData.getInstance().getSkill(4400, 2));
				htmtext = "pccafe_summon_newbuff_001.htm";
			}
			else
			{
				htmtext = "pccafe_notpoint001.htm";
			}
		}
		else if (PETSKILL.containsKey(event))
		{
			if ((player.getSummon() == null) || !(player.getSummon() instanceof L2ServitorInstance))
			{
				htmtext = "pccafe_notsummon.htm";
			}
			else if (player.getPcCafeSystem().getPoints() >= PETSKILL.get(event)[2])
			{
				player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() - PETSKILL.get(event)[2]);
				SystemMessage smsgpc = SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT);
				smsgpc.addInt(PETSKILL.get(event)[2]);
				player.sendPacket(smsgpc);
				player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), -PETSKILL.get(event)[2], 1, PcCafeType.CONSUME, 12));
				npc.setTarget(player.getSummon());
				npc.doCast(SkillData.getInstance().getSkill(PETSKILL.get(event)[0], PETSKILL.get(event)[1]));
				return "pccafe_summon_newbuff_002.htm";
			}
			else
			{
				htmtext = "pccafe_notpoint001.htm";
			}
		}
		else if (event.equalsIgnoreCase("wyvern"))
		{
			if (player.getPcCafeSystem().getPoints() >= PcCafeConfig.ALT_PC_BANG_WIVERN_PRICE)
			{
				if (WYVERN_ACTIVE)
				{
					htmtext = "pccafe_no_wyvern001.htm";
				}
				else
				{
					if (!(player.getSummon() == null) || (player.getSummon() instanceof L2ServitorInstance))
					{
						player.sendPacket(SystemMessageId.SUMMON_ONLY_ONE);
						return null;
					}
					
					player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() - PcCafeConfig.ALT_PC_BANG_WIVERN_PRICE);
					player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT).addLong(PcCafeConfig.ALT_PC_BANG_WIVERN_PRICE));
					player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), -PcCafeConfig.ALT_PC_BANG_WIVERN_PRICE, 1, PcCafeType.CONSUME, 12));
					player.dismount();
					player.mount(12621, 0, true);
					player.addSkill(CommonSkill.WYVERN_BREATH.getSkill());
					startQuestTimer("WYVERN_EFFECTS", PcCafeConfig.ALT_PC_BANG_WIVERN_TIME * 60 * 1000, null, player, false);
					WYVERN_ACTIVE = true;
					return null;
				}
			}
			else
			{
				htmtext = "pccafe_notpoint001.htm";
			}
		}
		return htmtext;
	}
	
	private String tradeItem(L2PcInstance player, int itemId, int points)
	{
		if (player.getPcCafeSystem().getPoints() >= PcCafeConfig.MAX_PC_BANG_POINTS)
		{
			return "pccafe_help_lottery_fail2.htm";
		}
		
		if ((player.getInventory().getItemByItemId(itemId) == null))
		{
			player.sendPacket(SystemMessageId.NOT_ENOUGH_ITEMS);
			return "pccafe_help_lottery_fail.htm";
		}
		
		if (takeItems(player, itemId, 1))
		{
			player.getPcCafeSystem().setPoints(player.getPcCafeSystem().getPoints() + points);
			player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.YOU_ACQUIRED_S1_PC_BANG_POINT).addLong(points));
			player.sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), points, 1, PcCafeType.ADD, 12));
			return "pccafe_help_lottery003.htm";
		}
		return "pccafe_help_lottery_fail.htm";
	}
	
	public boolean checkFirstConditions(L2PcInstance player)
	{
		if (player == null)
		{
			return false;
		}
		
		if (player.isDead() || player.isAlikeDead() || player.isFakeDeath() || player.isCastingNow() || player.isInCombat() || player.isAttackingNow() || player.isInDuel())
		{
			return false;
		}
		return true;
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		return npc.getId() + ".htm";
	}
}