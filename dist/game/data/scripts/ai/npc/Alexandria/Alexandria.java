/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.npc.Alexandria;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.AdditionalQuestItemHolder;
import com.l2jserver.gameserver.model.holders.IntLongHolder;
import com.l2jserver.gameserver.model.holders.ItemChanceHolder;

import ai.npc.AbstractNpcAI;

/**
 * Alexandria (Armor Merchant) AI.
 * @author xban1x
 */
public final class Alexandria extends AbstractNpcAI
{
	// NPC
	private static final int ALEXANDRIA = 30098;
	// Items
	private static final IntLongHolder[] REQUIRED_ITEMS = new IntLongHolder[]
	{
		new IntLongHolder(57, 7500000),
		new IntLongHolder(5094, 50),
		new IntLongHolder(6471, 25),
		new IntLongHolder(9814, 4),
		new IntLongHolder(9815, 3),
		new IntLongHolder(9816, 5),
		new IntLongHolder(9817, 5),
	};
	// Agathions
	private static final ItemChanceHolder[] LITTLE_DEVILS = new ItemChanceHolder[]
	{
		new AdditionalQuestItemHolder(10321, 600, 1, 10408),
		new ItemChanceHolder(10322, 10),
		new ItemChanceHolder(10323, 10),
		new ItemChanceHolder(10324, 5),
		new ItemChanceHolder(10325, 5),
		new ItemChanceHolder(10326, 370),
	};
	private static final ItemChanceHolder[] LITTLE_ANGELS = new ItemChanceHolder[]
	{
		new AdditionalQuestItemHolder(10315, 600, 1, 10408),
		new ItemChanceHolder(10316, 10),
		new ItemChanceHolder(10317, 10),
		new ItemChanceHolder(10318, 5),
		new ItemChanceHolder(10319, 5),
		new ItemChanceHolder(10320, 370),
	};
	private static final Map<String, List<ItemChanceHolder>> AGATHIONS = new HashMap<>();
	static
	{
		AGATHIONS.put("littleAngel", Arrays.asList(LITTLE_ANGELS));
		AGATHIONS.put("littleDevil", Arrays.asList(LITTLE_DEVILS));
	}
	
	public Alexandria()
	{
		super(Alexandria.class.getSimpleName(), "ai/npc");
		addStartNpc(ALEXANDRIA);
		addTalkId(ALEXANDRIA);
		addFirstTalkId(ALEXANDRIA);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		String htmltext = null;
		if (event.equals("30098-02.html"))
		{
			htmltext = event;
		}
		else if (AGATHIONS.containsKey(event))
		{
			var chance = getRandom(1000);
			var chance2 = 0;
			var chance3 = 0;
			for (var agathion : AGATHIONS.get(event))
			{
				chance3 += agathion.getChance();
				if ((chance2 <= chance) && (chance < chance3))
				{
					if (takeAllItems(player, REQUIRED_ITEMS))
					{
						giveItems(player, agathion);
						htmltext = "30098-03.html";
						
						if (agathion instanceof AdditionalQuestItemHolder)
						{
							giveItems(player, ((AdditionalQuestItemHolder) agathion).getAdditionalId(), 1);
							htmltext = "30098-03a.html";
						}
					}
					else
					{
						htmltext = "30098-04.html";
					}
					break;
				}
				chance2 += agathion.getChance();
			}
		}
		return htmltext;
	}
}
