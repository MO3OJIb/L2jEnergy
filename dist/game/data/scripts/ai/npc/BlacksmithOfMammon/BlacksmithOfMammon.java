/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.npc.BlacksmithOfMammon;

import com.l2jserver.gameserver.SevenSigns;
import com.l2jserver.gameserver.configuration.config.FeatureConfig;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

import ai.npc.AbstractNpcAI;

/**
 * Blacksmith of Mammon AI
 * @author Charus
 */
public final class BlacksmithOfMammon extends AbstractNpcAI
{
	// NPC
	private static final int BLACKSMITH_MAMMON = 31126;
	
	public BlacksmithOfMammon()
	{
		super(BlacksmithOfMammon.class.getSimpleName(), "ai/npc");
		addStartNpc(BLACKSMITH_MAMMON);
		addFirstTalkId(BLACKSMITH_MAMMON);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		if (event.endsWith(".htm"))
		{
			return "blacksmith_of_mammon" + event;
		}
		return super.onAdvEvent(event, npc, player);
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		var compWinner = SevenSigns.getInstance().getCabalHighestScore();
		var sealGnosisOwner = SevenSigns.getInstance().getSealOwner(SevenSigns.SEAL_GNOSIS);
		var playerCabal = SevenSigns.getInstance().getPlayerCabal(player.getObjectId());
		
		if (FeatureConfig.ALT_STRICT_SEVENSIGNS)
		{
			switch (compWinner)
			{
				case SevenSigns.CABAL_DAWN ->
				{
					if ((playerCabal != compWinner) || (playerCabal != sealGnosisOwner))
					{
						return "blacksmith_of_mammon002.htm";
					}
				}
				case SevenSigns.CABAL_DUSK ->
				{
					if ((playerCabal != compWinner) || (playerCabal != sealGnosisOwner))
					{
						return "blacksmith_of_mammon002.htm";
					}
				}
			}
		}
		return "blacksmith_of_mammon001.htm";
	}
}