/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.npc.EchoCrystals;

import java.util.HashMap;
import java.util.Map;

import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.RewardInfoHolder;
import com.l2jserver.gameserver.model.itemcontainer.Inventory;

import ai.npc.AbstractNpcAI;

/**
 * Echo Crystals AI.
 * @author Plim
 * @author Adry_85
 */
public final class EchoCrystals extends AbstractNpcAI
{
	// NPCs
	private final static int[] NPCs =
	{
		31042, // Kantabilon
		31043, // Octavia
	};
	
	private static final Map<Integer, RewardInfoHolder> SCORES = new HashMap<>();
	static
	{
		SCORES.put(4410, new RewardInfoHolder(4411, "01", "02", "03"));
		SCORES.put(4409, new RewardInfoHolder(4412, "04", "05", "06"));
		SCORES.put(4408, new RewardInfoHolder(4413, "07", "08", "09"));
		SCORES.put(4420, new RewardInfoHolder(4414, "10", "11", "12"));
		SCORES.put(4421, new RewardInfoHolder(4415, "13", "14", "15"));
		SCORES.put(4419, new RewardInfoHolder(4417, "16", "02", "03"));
		SCORES.put(4418, new RewardInfoHolder(4416, "17", "02", "03"));
	}
	
	public EchoCrystals()
	{
		super(EchoCrystals.class.getSimpleName(), "ai/npc");
		addStartNpc(NPCs);
		addTalkId(NPCs);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		String htmltext = null;
		var score = Integer.valueOf(event);
		if (SCORES.containsKey(score))
		{
			if (!hasQuestItems(player, score))
			{
				htmltext = npc.getId() + "-" + SCORES.get(score).getMessageNoScore() + ".htm";
			}
			else if (player.getAdena() < 200)
			{
				htmltext = npc.getId() + "-" + SCORES.get(score).getMessageNoAdena() + ".htm";
			}
			else
			{
				takeItems(player, Inventory.ADENA_ID, 200);
				giveItems(player, SCORES.get(score).getCrystalId(), 1);
				htmltext = npc.getId() + "-" + SCORES.get(score).getMessageOk() + ".htm";
			}
		}
		return htmltext;
	}
}
