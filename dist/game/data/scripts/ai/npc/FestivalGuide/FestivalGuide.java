/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.npc.FestivalGuide;

import java.util.Calendar;

import com.l2jserver.gameserver.SevenSigns;
import com.l2jserver.gameserver.SevenSignsFestival;
import com.l2jserver.gameserver.configuration.config.FeatureConfig;
import com.l2jserver.gameserver.enums.audio.Sound;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.instancemanager.QuestManager;
import com.l2jserver.gameserver.model.L2Party;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.L2Summon;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.SkillHolder;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.QuestState;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.network.NpcStringId;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.util.Util;

import ai.npc.AbstractNpcAI;

/**
 * Seven Signs Festival AI
 * @author Charus
 */
public final class FestivalGuide extends AbstractNpcAI
{
	// @formatter:off
	// NPCs 
	private static final int[] DAWN_NPCS = 
	{
		31127, 31128, 31129, 31130, 31131
	};
	
	private static final int[] DUSK_NPCS = 
	{
		31137, 31138, 31139, 31140, 31141
	};// @formatter:on
	
	private static final SkillHolder SSQ_CANCEL = new SkillHolder(4334, 1);
	
	private static final int Q00255_Tutorial = 255;
	private static final int Q00505_BloodOffering = 505;
	private static final int Q00635_IntoTheDimensionalRift = 635;
	
	private static final int BLOOD_OF_OFFERING = 5901;
	private static final int DIMENSIONAL_FRAGMENT = 7079;
	private static final int THE_STAFF = 214;
	
	private static final double WEIGHT_LIMIT = 0.80;
	private static final double QUEST_LIMIT = 0.90;
	private static final int MAX_QUEST_COUNT = 40;
	
	private static final int MAX_DISTANCE = 1500;
	
	private final String fnHi(int npcId)
	{
		return switch (npcId)
		{
			case 31127 -> "dawn_acolyte1001.htm";
			case 31128 -> "dawn_acolyte2001.htm";
			case 31129 -> "dawn_acolyte3001.htm";
			case 31130 -> "dawn_acolyte4001.htm";
			case 31131 -> "dawn_acolyte5001.htm";
			case 31137 -> "dusk_acolyte1001.htm";
			case 31138 -> "dusk_acolyte2001.htm";
			case 31139 -> "dusk_acolyte3001.htm";
			case 31140 -> "dusk_acolyte4001.htm";
			case 31141 -> "dusk_acolyte5001.htm";
			default -> null;
		};
	}
	
	private final String fStringRoom(int id)
	{
		return switch (id)
		{
			case 1000312 -> "Level 31 or lower";
			case 1000313 -> "Level 42 or lower";
			case 1000314 -> "Level 53 or lower";
			case 1000315 -> "Level 64 or lower";
			case 1000316 -> "No Level Limit";
			default -> null;
		};
	}
	
	private final String getParticipateHtml(int npcId)
	{
		return switch (npcId)
		{
			case 31127 -> "ssq_main_event_acolyte_q0505_20f.htm";
			case 31128 -> "ssq_main_event_acolyte_q0505_20g.htm";
			case 31129 -> "ssq_main_event_acolyte_q0505_20h.htm";
			case 31130 -> "ssq_main_event_acolyte_q0505_20i.htm";
			case 31131 -> "ssq_main_event_acolyte_q0505_20j.htm";
			case 31137 -> "ssq_main_event_acolyte_q0505_20a.htm";
			case 31138 -> "ssq_main_event_acolyte_q0505_20b.htm";
			case 31139 -> "ssq_main_event_acolyte_q0505_20c.htm";
			case 31140 -> "ssq_main_event_acolyte_q0505_20d.htm";
			case 31141 -> "ssq_main_event_acolyte_q0505_20e.htm";
			default -> null;
		};
	}
	
	private final String fStringLevel(int npcId)
	{
		return switch (npcId)
		{
			case 31127, 31137 -> "32";
			case 31128, 31138 -> "43";
			case 31129, 31139 -> "54";
			case 31130, 31140 -> "65";
			default -> null;
		};
	}
	
	private final String fStringLevelHtml(int npcId)
	{
		return switch (npcId)
		{
			case 31127, 31137 -> "ssq_main_event_acolyte_q0505_04a.htm";
			case 31128, 31138 -> "ssq_main_event_acolyte_q0505_04b.htm";
			case 31129, 31139 -> "ssq_main_event_acolyte_q0505_04c.htm";
			case 31130, 31140 -> "ssq_main_event_acolyte_q0505_04d.htm";
			default -> null;
		};
	}
	
	private final int getMinLevel(int npcId)
	{
		return switch (npcId)
		{
			case 31127, 31137 -> 31;
			case 31128, 31138 -> 42;
			case 31129, 31139 -> 53;
			case 31130, 31140 -> 64;
			case 31131, 31141 -> 99;
			default -> 0;
		};
	}
	
	private final int getBlueStoneCount(int npcId)
	{
		return switch (npcId)
		{
			case 31127, 31137 -> 900;
			case 31128, 31138 -> 1500;
			case 31129, 31139 -> 3000;
			case 31130, 31140 -> 4500;
			case 31131, 31141 -> 6000;
			default -> 0;
		};
	}
	
	private final int getGreenStoneCount(int npcId)
	{
		return switch (npcId)
		{
			case 31127, 31137 -> 540;
			case 31128, 31138 -> 900;
			case 31129, 31139 -> 1800;
			case 31130, 31140 -> 2700;
			case 31131, 31141 -> 3600;
			default -> 0;
		};
	}
	
	private final int getRedStoneCount(int npcId)
	{
		return switch (npcId)
		{
			case 31127, 31137 -> 270;
			case 31128, 31138 -> 450;
			case 31129, 31139 -> 900;
			case 31130, 31140 -> 1350;
			case 31131, 31141 -> 1800;
			default -> 0;
		};
	}
	
	private final int getTimeAttackFee(int npcId)
	{
		return switch (npcId)
		{
			case 31127, 31137 -> 2700;
			case 31128, 31138 -> 4500;
			case 31129, 31139 -> 9000;
			case 31130, 31140 -> 13500;
			case 31131, 31141 -> 18000;
			default -> 0;
		};
	}
	
	public FestivalGuide()
	{
		super(FestivalGuide.class.getSimpleName(), "ai/npc");
		
		addSpawnId(DAWN_NPCS);
		addSpawnId(DUSK_NPCS);
		addTalkId(DAWN_NPCS);
		addTalkId(DUSK_NPCS);
		addStartNpc(DAWN_NPCS);
		addStartNpc(DUSK_NPCS);
		addFirstTalkId(DAWN_NPCS);
		addFirstTalkId(DUSK_NPCS);
		addSpellFinishedId(DAWN_NPCS);
		addSpellFinishedId(DUSK_NPCS);
	}
	
	@Override
	public String onSpawn(L2Npc npc)
	{
		npc.setScriptValue(0);
		startQuestTimer("3001", 1000, npc, null);
		
		return super.onSpawn(npc);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		if (event.endsWith(".htm"))
		{
			return event;
		}
		
		final int roomIndex = npc.getTemplate().getParameters().getInt("RoomIndex", 0);
		final String partType = npc.getTemplate().getParameters().getString("part_type", null);
		final int ssqType = partType.equals("TWILIGHT") ? 1 : partType.equals("DAWN") ? 2 : 0;
		
		final SevenSigns ss = SevenSigns.getInstance();
		final SevenSignsFestival ssf = SevenSignsFestival.getInstance();
		
		if (event.equalsIgnoreCase("3001"))
		{
			final int shoutSysMsg = npc.getTemplate().getParameters().getInt("ShoutSysMsg", 0);
			final String sibylName = npc.getTemplate().getParameters().getString("SibylName", null);
			final int sibylSilhouette = npc.getTemplate().getParameters().getInt("SibylSilhouette", 0);
			
			final int sibylPosX = npc.getTemplate().getParameters().getInt("SibylPosX", 0);
			final int sibylPosY = npc.getTemplate().getParameters().getInt("SibylPosY", 0);
			final int sibylPosZ = npc.getTemplate().getParameters().getInt("SibylPosZ", 0);
			
			if (ss.isCompetitionPeriod())
			{
				final Calendar cal = Calendar.getInstance();
				int i0 = cal.get(Calendar.MINUTE);
				long i1 = ss.getMilliToPeriodChange() / 1000;
				if (((i0 == 18) || (i0 == 38) || (i0 == 58)) && (i1 >= (18 * 60)))
				{
					if (npc.getScriptValue() < 1)
					{
						if (shoutSysMsg == 1)
						{
							broadcastNpcSay(npc, ChatType.NPC_SHOUT, NpcStringId.THE_MAIN_EVENT_WILL_START_IN_2_MINUTES_PLEASE_REGISTER_NOW);
						}
						
						npc.setScriptValue(npc.getScriptValue() + 1);
					}
				}
				else
				{
					if (((i0 == 0) || (i0 == 20) || (i0 == 40)) && (i1 >= (18 * 60)))
					{
						if (npc.getScriptValue() < 1)
						{
							if (shoutSysMsg == 1)
							{
								broadcastNpcSay(npc, ChatType.NPC_SHOUT, NpcStringId.THE_MAIN_EVENT_IS_NOW_STARTING);
							}
							
							npc.setScriptValue(npc.getScriptValue() + 1);
							
							if (sibylName != null)
							{
								addSpawn(sibylSilhouette, sibylPosX, sibylPosY, sibylPosZ, 0, false, 0, false);
							}
						}
					}
					else
					{
						if ((i0 == 13) || (i0 == 33) || (i0 == 53))
						{
							if (npc.getScriptValue() < 1)
							{
								if (shoutSysMsg == 1)
								{
									broadcastNpcSay(npc, ChatType.NPC_SHOUT, NpcStringId.THE_MAIN_EVENT_WILL_CLOSE_IN_5_MINUTES);
								}
								
								npc.setScriptValue(npc.getScriptValue() + 1);
							}
						}
						else
						{
							if ((i0 == 16) || (i0 == 36) || (i0 == 56))
							{
								if ((npc.getScriptValue() < 1) && (shoutSysMsg == 1))
								{
									if (i1 >= (20 * 60))
									{
										broadcastNpcSay(npc, ChatType.NPC_SHOUT, NpcStringId.THE_MAIN_EVENT_WILL_FINISH_IN_2_MINUTES_PLEASE_PREPARE_FOR_THE_NEXT_GAME);
									}
									else
									{
										broadcastNpcSay(npc, ChatType.NPC_SHOUT, NpcStringId.THE_FESTIVAL_OF_DARKNESS_WILL_END_IN_TWO_MINUTES);
									}
									
									npc.setScriptValue(npc.getScriptValue() + 1);
								}
							}
							else
							{
								npc.setScriptValue(0);
							}
						}
					}
				}
			}
			
			startQuestTimer("3001", 7000, npc, null);
		}
		
		int ask = Integer.parseInt(event.split(";")[0]);
		switch (ask)
		{
			case 505 ->
			{
				if ((player.getInventory().getSize(true) >= (player.getQuestInventoryLimit() * QUEST_LIMIT)) || (player.getInventory().getSize(false) >= (player.getInventoryLimit() * WEIGHT_LIMIT)) || (player.getCurrentLoad() >= (player.getMaxLoad() * WEIGHT_LIMIT)))
				{
					player.sendPacket(SystemMessageId.YOU_CAN_PROCEED_ONLY_WHEN_THE_INVENTORY_WEIGHT_IS_BELOW_80_PERCENT_AND_THE_QUANTITY_IS_BELOW_90_PERCENT);
					return null;
				}
				
				int reply = Integer.parseInt(event.split(";")[1]);
				switch (reply)
				{
					case 1 ->
					{ // participate request
						final Calendar cal = Calendar.getInstance();
						int i3 = cal.get(Calendar.MINUTE);
						if (!ss.isCompetitionPeriod())
						{
							return "ssq_main_event_acolyte_q0505_02.htm";
						}
						
						if (((i3 >= 0) && (i3 < 18)) || ((i3 >= 20) && (i3 < 38)) || ((i3 >= 40) && (i3 < 58)) || ((ss.getMilliToPeriodChange() / 1000) <= 120))
						{
							return "ssq_main_event_acolyte_q0505_22.htm";
						}
						
						return getParticipateHtml(npc.getId());
					}
					case 2 ->
					{ // register score
						L2Party party0 = player.getParty();
						if (party0 != null)
						{
							Quest q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
							QuestState qs505 = q505.getQuestState(player, true);
							
							for (L2PcInstance i0 : party0.getMembers())
							{
								Quest q505i0 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
								QuestState qs505i0 = q505i0.getQuestState(i0, true);
								
								if (qs505.getMemoState() != qs505i0.getMemoState())
								{
									return null;
								}
							}
						}
						
						if (ss.getPlayerCabal(player.getObjectId()) != ssqType)
						{
							return "ssq_main_event_acolyte_q0505_08.htm";
						}
						
						if (!ss.isCompetitionPeriod())
						{
							return "ssq_main_event_acolyte_q0505_02.htm";
						}
						
						if ((party0 == null) || (party0.getLeaderObjectId() != player.getObjectId()))
						{
							return "ssq_main_event_acolyte_q0505_10.htm";
						}
						
						Quest q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
						QuestState qs505 = q505.getQuestState(player, true);
						if ((((System.currentTimeMillis() / 1000) - qs505.getMemoState()) / 60) > 40)
						{
							return "ssq_main_event_acolyte_q0505_11.htm";
						}
						
						if (qs505.getMemoStateEx(1) != (roomIndex - 1))
						{
							return "ssq_main_event_acolyte_q0505_28.htm";
						}
						
						if (ssf.getHighestScore(ssqType, roomIndex - 1) > getQuestItemsCount(player, BLOOD_OF_OFFERING))
						{
							for (L2PcInstance pc : party0.getMembers())
							{
								QuestState qs505pc = q505.getQuestState(pc, true);
								qs505pc.exitQuest(true, false);
							}
							
							long i2 = getQuestItemsCount(player, BLOOD_OF_OFFERING);
							switch (npc.getId())
							{
								case 31127, 31128, 31129, 31130, 31137, 31138, 31139, 31140 ->
								{
									broadcastNpcSay(npc, ChatType.SHOUT, NpcStringId.S1_HAS_WON_THE_MAIN_EVENT_FOR_PLAYERS_UNDER_LEVEL_S2_AND_EARNED_S3_POINTS, player.getName(), fStringLevel(npc.getId()), String.valueOf(i2));
								}
								case 31131, 31141 ->
								{
									broadcastNpcSay(npc, ChatType.SHOUT, NpcStringId.S1_HAS_EARNED_S2_POINTS_IN_THE_MAIN_EVENT_FOR_UNLIMITED_LEVELS, player.getName(), String.valueOf(i2));
								}
							}
							
							takeItems(player, BLOOD_OF_OFFERING, getQuestItemsCount(player, BLOOD_OF_OFFERING));
							
							if (i2 >= 3000)
							{
								return "ssq_main_event_acolyte_q0505_12a.htm";
							}
							
							return "ssq_main_event_acolyte_q0505_12.htm";
						}
						
						for (L2PcInstance pc : party0.getMembers())
						{
							QuestState qs505pc = q505.getQuestState(pc, true);
							qs505pc.exitQuest(true, false);
						}
						
						long i2 = getQuestItemsCount(player, BLOOD_OF_OFFERING);
						switch (npc.getId())
						{
							case 31127, 31128, 31129, 31130, 31137, 31138, 31139, 31140 ->
							{
								broadcastNpcSay(npc, ChatType.SHOUT, NpcStringId.S1_HAS_WON_THE_MAIN_EVENT_FOR_PLAYERS_UNDER_LEVEL_S2_AND_EARNED_S3_POINTS, player.getName(), fStringLevel(npc.getId()), String.valueOf(i2));
							}
							case 31131, 31141 ->
							{
								broadcastNpcSay(npc, ChatType.SHOUT, NpcStringId.S1_HAS_EARNED_S2_POINTS_IN_THE_MAIN_EVENT_FOR_UNLIMITED_LEVELS, player.getName(), String.valueOf(i2));
							}
						}
						
						ssf.addTimeAttackRecord(ssqType, roomIndex - 1, party0, i2);
						playSound(player, Sound.SKILLSOUND_JEWEL_CELEBRATE);
						takeItems(player, BLOOD_OF_OFFERING, getQuestItemsCount(player, BLOOD_OF_OFFERING));
						
						if (i2 >= 3000)
						{
							return "ssq_main_event_acolyte_q0505_13a.htm";
						}
						
						return "ssq_main_event_acolyte_q0505_13.htm";
					}
					case 3 ->
					{ // high scores
						String html = getHtm(player, "ssq_main_event_acolyte_q0505_14.htm");
						
						for (int i0 = 1; i0 < 4; i0++)
						{
							int i1 = 0;
							if (i0 == 1)
							{
								i1 = 2;
							}
							else
							{
								if (i0 == 2)
								{
									i1 = 1;
								}
								else
								{
									if (i0 == 3)
									{
										i1 = 0;
										final StatsSet s0 = ssf.getOverallHighestScoreData(roomIndex - 1);
										if (s0 != null)
										{
											if (s0.getString("cabal").equals("dusk"))
											{
												html = html.replace("<?Winner?>", "Dusk");
											}
											else
											{
												if (s0.getString("cabal").equals("dawn"))
												{
													html = html.replace("<?Winner?>", "Dawn");
												}
											}
											
											long i2 = Long.valueOf(s0.getString("date"));
											if (i2 == 0)
											{
												html = html.replace("<?date" + i0 + "?>", "No record exists");
											}
											else
											{
												Calendar cal = Calendar.getInstance();
												cal.setTimeInMillis(i2);
												html = html.replace("<?date" + i0 + "?>", cal.get(Calendar.YEAR) + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.DAY_OF_MONTH));
											}
											
											html = html.replace("<?score" + i0 + "?>", String.valueOf(s0.getInt("score")));
											
											String[] members = s0.getString("members").split(",");
											int count = members.length;
											if (count > 0)
											{
												for (int i = 1; i <= count; i++)
												{
													String comma = i != count ? ", " : "";
													String name = members[i - 1] + comma;
													html = html.replace("<?player" + i0 + "" + i + "?>", name);
												}
											}
											for (int i = count + 1; i < 10; i++)
											{
												html = html.replace("<?player" + i0 + "" + i + "?>", "");
											}
										}
										else
										{
											html = html.replace("<?Winner?>", "");
											html = html.replace("<?date" + i0 + "?>", "No record exists");
											html = html.replace("<?score" + i0 + "?>", String.valueOf(0));
											
											for (int i = 1; i < 10; i++)
											{
												html = html.replace("<?player" + i0 + "" + i + "?>", "");
											}
										}
									}
								}
							}
							
							if ((i1 == 1) || (i1 == 2))
							{
								final StatsSet s0 = ssf.getHighestScoreData(i1, roomIndex - 1);
								long i2 = Long.valueOf(s0.getString("date"));
								if (i2 == 0)
								{
									html = html.replace("<?date" + i0 + "?>", "No record exists");
								}
								else
								{
									Calendar cal = Calendar.getInstance();
									cal.setTimeInMillis(i2);
									html = html.replace("<?date" + i0 + "?>", cal.get(Calendar.YEAR) + "/" + cal.get(Calendar.MONTH) + "/" + cal.get(Calendar.DAY_OF_MONTH));
								}
								
								html = html.replace("<?score" + i0 + "?>", String.valueOf(s0.getInt("score")));
								
								String[] members = s0.getString("members").split(",");
								int count = members.length;
								if (count > 0)
								{
									for (int i = 1; i <= count; i++)
									{
										String comma = i != count ? ", " : "";
										String name = members[i - 1] + comma;
										html = html.replace("<?player" + i0 + "" + i + "?>", name);
									}
								}
								for (int i = count + 1; i < 10; i++)
								{
									html = html.replace("<?player" + i0 + "" + i + "?>", "");
								}
							}
						}
						
						return html;
					}
					case 4 ->
					{ // festival status
						String html = getHtm(player, "ssq_main_event_acolyte_q0505_15.htm");
						
						for (int i0 = 1; i0 < 6; i0++)
						{
							int i1 = 1000311 + i0;
							html = html.replace("<?Room" + i0 + "?>", fStringRoom(i1));
							
							int s0 = ssf.getHighestScore(SevenSigns.CABAL_DUSK, i0 - 1);
							html = html.replace("<?Score" + i0 + "1?>", String.valueOf(s0));
							int s1 = ssf.getHighestScore(SevenSigns.CABAL_DAWN, i0 - 1);
							html = html.replace("<?Score" + i0 + "2?>", String.valueOf(s1));
							
							if (s0 > s1)
							{
								html = html.replace("<?Winner" + i0 + "?>", "Dusk");
							}
							else
							{
								if (s0 < s1)
								{
									html = html.replace("<?Winner" + i0 + "?>", "Dawn");
								}
								else
								{
									html = html.replace("<?Winner" + i0 + "?>", "Draw");
								}
							}
						}
						
						return html;
					}
					case 6 ->
					{ // what is my bonus
						if (!ss.isSealValidationPeriod())
						{
							return "ssq_main_event_acolyte_q0505_23.htm";
						}
						if (ss.getPlayerCabal(player.getObjectId()) != ssqType)
						{
							return "ssq_main_event_acolyte_q0505_08.htm";
						}
						if (ss.getPlayerCabal(player.getObjectId()) != ss.getCabalHighestScore())
						{
							return "ssq_main_event_acolyte_q0505_24.htm";
						}
						if (!ssf.isWinnerOfTimeAttackEvent(player, roomIndex - 1))
						{
							return "ssq_main_event_acolyte_q0505_17.htm";
						}
						if (ssf.getTimeAttackRewardFlag(player, roomIndex - 1))
						{
							return "ssq_main_event_acolyte_q0505_19.htm";
						}
						
						final StatsSet i0 = ssf.getHighestScoreData(ss.getPlayerCabal(player.getObjectId()), roomIndex - 1);
						int i1 = ssf.getAccumulatedBonus(roomIndex - 1) / i0.getString("members").split(",").length;
						ssf.giveTimeAttackReward(player, roomIndex - 1, i1);
						player.setFame(player.getFame() + 1080);
						
						return "ssq_main_event_acolyte_q0505_18.htm";
					}
					case 7 ->
					{ // participate with blue stones
						L2Party party0 = player.getParty();
						if (party0 != null)
						{
							for (L2PcInstance c0 : party0.getMembers())
							{
								if (ss.getPlayerCabal(c0.getObjectId()) != ssqType)
								{
									String html = getHtm(player, "ssq_main_event_acolyte_q0505_01.htm");
									html = html.replace("<?name?>", c0.getName());
									return html;
								}
								
								if (c0.getLevel() > getMinLevel(npc.getId()))
								{
									String html = getHtm(player, fStringLevelHtml(npc.getId()));
									html = html.replace("<?name?>", c0.getName());
									return html;
								}
							}
						}
						
						if (!ss.isCompetitionPeriod())
						{
							return "ssq_main_event_acolyte_q0505_02.htm";
						}
						
						if ((party0 == null) || (party0.getLeaderObjectId() != player.getObjectId()))
						{
							return "ssq_main_event_acolyte_q0505_03.htm";
						}
						
						Quest q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
						QuestState qs505 = q505.getQuestState(player, true);
						qs505.setMemoStateEx(1, getBlueStoneCount(npc.getId()));
						
						return "ssq_main_event_acolyte_q0505_06.htm";
					}
					case 8 ->
					{ // participate with green stones
						L2Party party0 = player.getParty();
						if (party0 != null)
						{
							for (L2PcInstance c0 : party0.getMembers())
							{
								if (ss.getPlayerCabal(c0.getObjectId()) != ssqType)
								{
									String html = getHtm(player, "ssq_main_event_acolyte_q0505_01.htm");
									html = html.replace("<?name?>", c0.getName());
									return html;
								}
								
								if (c0.getLevel() > getMinLevel(npc.getId()))
								{
									String html = getHtm(player, "ssq_main_event_acolyte_q0505_04a.htm");
									html = html.replace("<?name?>", c0.getName());
									return html;
								}
							}
						}
						
						if (!ss.isCompetitionPeriod())
						{
							return "ssq_main_event_acolyte_q0505_02.htm";
						}
						
						if ((party0 == null) || (party0.getLeaderObjectId() != player.getObjectId()))
						{
							return "ssq_main_event_acolyte_q0505_03.htm";
						}
						
						Quest q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
						QuestState qs505 = q505.getQuestState(player, true);
						qs505.setMemoStateEx(1, getGreenStoneCount(npc.getId()));
						
						return "ssq_main_event_acolyte_q0505_06.htm";
					}
					case 9 ->
					{ // participate with red stones
						L2Party party0 = player.getParty();
						if (party0 != null)
						{
							for (L2PcInstance c0 : party0.getMembers())
							{
								if (ss.getPlayerCabal(c0.getObjectId()) != ssqType)
								{
									String html = getHtm(player, "ssq_main_event_acolyte_q0505_01.htm");
									html = html.replace("<?name?>", c0.getName());
									return html;
								}
								
								if (c0.getLevel() > getMinLevel(npc.getId()))
								{
									String html = getHtm(player, "ssq_main_event_acolyte_q0505_04a.htm");
									html = html.replace("<?name?>", c0.getName());
									return html;
								}
							}
						}
						
						if (!ss.isCompetitionPeriod())
						{
							return "ssq_main_event_acolyte_q0505_02.htm";
						}
						
						if ((party0 == null) || (party0.getLeaderObjectId() != player.getObjectId()))
						{
							return "ssq_main_event_acolyte_q0505_03.htm";
						}
						
						Quest q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
						QuestState qs505 = q505.getQuestState(player, true);
						qs505.setMemoStateEx(1, getRedStoneCount(npc.getId()));
						
						return "ssq_main_event_acolyte_q0505_06.htm";
					}
					case 10 ->
					{ // back
						String html = getHtm(player, fnHi(npc.getId()));
						
						final Calendar cal = Calendar.getInstance();
						int i0 = cal.get(Calendar.MINUTE);
						if ((i0 == 58) || (i0 == 18) || (i0 == 38) || (i0 == 59) || (i0 == 19) || (i0 == 39))
						{
							html = html.replace("<?minute?>", "0 minute");
						}
						else
						{
							int i1;
							if (i0 > 39)
							{
								i1 = 58 - i0;
							}
							else
							{
								if (i0 > 19)
								{
									i1 = 38 - i0;
								}
								else
								{
									i1 = 18 - i0;
								}
							}
							html = html.replace("<?minute?>", i1 + " Minute");
						}
						
						return html;
					}
					case 11 ->
					{ // accumulated bonus
						String html = getHtm(player, "ssq_main_event_acolyte_q0505_27.htm");
						for (int i0 = 1; i0 < 6; i0++)
						{
							int i1 = 1000311 + i0;
							html = html.replace("<?Room" + i0 + "?>", fStringRoom(i1));
							i1 = ssf.getAccumulatedBonus(i0 - 1);
							html = html.replace("<?Money" + i0 + "?>", String.valueOf(i1));
						}
						
						return html;
					}
					case 12 ->
					{ // low score, teleport to dimensional rift
						long i3 = System.currentTimeMillis() / 1000;
						
						L2Party party0 = player.getParty();
						if (party0 != null)
						{
							int i2 = 0;
							
							for (L2PcInstance c1 : party0.getMembers())
							{
								if ((c1.getAllActiveQuests().size() > MAX_QUEST_COUNT) && !haveMemo(c1, Q00635_IntoTheDimensionalRift))
								{
									i2 = 0;
									
									String html = getHtm(player, "ssq_main_event_acolyte_q0505_13e.htm");
									html = html.replace("<?name?>", c1.getName());
									return html;
								}
								
								i2 = 1;
							}
							
							if (i2 == 1)
							{
								for (L2PcInstance c1 : party0.getMembers())
								{
									Quest q635 = QuestManager.getInstance().getQuest(Q00635_IntoTheDimensionalRift);
									QuestState qs635 = q635.getQuestState(c1, true);
									
									qs635.setMemoState(2);
									qs635.setMemoStateEx(1, i3);
									
									c1.teleToLocation(new Location(-114796, -179334, -6752), 1000);
								}
							}
							
							return "ssq_main_event_acolyte_q0505_13b.htm";
						}
					}
					case 13 ->
					{ // low score, stay at festival
						L2Party party0 = player.getParty();
						if (party0 != null)
						{
							for (L2PcInstance c1 : party0.getMembers())
							{
								giveItems(c1, DIMENSIONAL_FRAGMENT, 4);
							}
							
							return "ssq_main_event_acolyte_q0505_13c.htm";
						}
					}
					case 14 ->
					{ // dimensional rift
						player.teleToLocation(-114796, -179334, -6752);
						return "ssq_main_event_acolyte_q0505_13d.htm";
					}
				}
			}
			case 507 ->
			{
				if ((player.getInventory().getSize(true) >= (player.getQuestInventoryLimit() * QUEST_LIMIT)) || (player.getInventory().getSize(false) >= (player.getInventoryLimit() * WEIGHT_LIMIT)) || (player.getCurrentLoad() >= (player.getMaxLoad() * WEIGHT_LIMIT)))
				{
					player.sendPacket(SystemMessageId.YOU_CAN_PROCEED_ONLY_WHEN_THE_INVENTORY_WEIGHT_IS_BELOW_80_PERCENT_AND_THE_QUANTITY_IS_BELOW_90_PERCENT);
					return null;
				}
				
				int reply = Integer.parseInt(event.split(";")[1]);
				switch (reply)
				{
					case 2 ->
					{ // return to village request
						return "ssq_main_event_acolyte_q0507_28.htm";
					}
					case 3 ->
					{ // return to village teleport
						if (haveMemo(player, Q00505_BloodOffering))
						{
							Quest q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
							QuestState qs505 = q505.getQuestState(player, true);
							qs505.exitQuest(true, false);
						}
						
						takeItems(player, BLOOD_OF_OFFERING, getQuestItemsCount(player, BLOOD_OF_OFFERING));
						
						Quest q255 = QuestManager.getInstance().getQuest(Q00255_Tutorial);
						QuestState qs255 = q255.getQuestState(player, true);
						
						int i0 = qs255.getMemoStateEx(1);
						int i1 = i0 % 10000;
						int i2 = ((i0 - i1) + 5) / 10000;
						
						if ((i1 >= 95) && (i1 < 195))
						{
							if (Util.contains(DAWN_NPCS, npc.getId()))
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(-80542, 150315, -3040);
									}
									case 1 ->
									{
										player.teleToLocation(-80602, 150352, -3040);
									}
								}
							}
							else
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(-82340, 151575, -3120);
									}
									case 1 ->
									{
										player.teleToLocation(-82392, 151584, -3120);
									}
								}
							}
							return null;
						}
						if ((i1 >= 195) && (i1 < 295))
						{
							if (Util.contains(DAWN_NPCS, npc.getId()))
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(-13996, 121413, -2984);
									}
									case 1 ->
									{
										player.teleToLocation(-13998, 121479, -2984);
									}
								}
							}
							else
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(-14727, 124002, -3112);
									}
									case 1 ->
									{
										player.teleToLocation(-14727, 124012, -3112);
									}
								}
							}
							return null;
						}
						if ((i1 >= 295) && (i1 < 395))
						{
							if (Util.contains(DAWN_NPCS, npc.getId()))
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(16320, 142915, -2696);
									}
									case 1 ->
									{
										player.teleToLocation(16383, 142899, -2696);
									}
								}
							}
							else
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(18501, 144673, -3056);
									}
									case 1 ->
									{
										player.teleToLocation(18523, 144624, -3056);
									}
								}
							}
							return null;
						}
						if ((i1 >= 395) && (i1 < 495))
						{
							if (Util.contains(DAWN_NPCS, npc.getId()))
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(83312, 149236, -3400);
									}
									case 1 ->
									{
										player.teleToLocation(83313, 149304, -3400);
									}
								}
							}
							else
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(81572, 148580, -3464);
									}
									case 1 ->
									{
										player.teleToLocation(81571, 148641, -3464);
									}
								}
							}
							return null;
						}
						if ((i1 >= 495) && (i1 < 595))
						{
							if (Util.contains(DAWN_NPCS, npc.getId()))
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(111359, 220959, -3544);
									}
									case 1 ->
									{
										player.teleToLocation(111411, 220955, -3544);
									}
								}
							}
							else
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(112441, 220149, -3544);
									}
									case 1 ->
									{
										player.teleToLocation(112452, 220204, -3592);
									}
								}
							}
							return null;
						}
						if ((i1 >= 595) && (i1 < 695))
						{
							if (Util.contains(DAWN_NPCS, npc.getId()))
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(83057, 53983, -1488);
									}
									case 1 ->
									{
										player.teleToLocation(83069, 54043, -1488);
									}
								}
							}
							else
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(82842, 54613, -1520);
									}
									case 1 ->
									{
										player.teleToLocation(82791, 54616, -1520);
									}
								}
							}
							return null;
						}
						if ((i1 >= 695) && (i1 < 795))
						{
							if (Util.contains(DAWN_NPCS, npc.getId()))
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(146955, 26690, -2200);
									}
									case 1 ->
									{
										player.teleToLocation(147015, 26689, -2200);
									}
								}
							}
							else
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(147528, 28899, -2264);
									}
									case 1 ->
									{
										player.teleToLocation(147528, 28962, -2264);
									}
								}
							}
							return null;
						}
						if ((i1 >= 795) && (i1 < 895))
						{
							if (Util.contains(DAWN_NPCS, npc.getId()))
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(115206, 74775, -2600);
									}
									case 1 ->
									{
										player.teleToLocation(115174, 74722, -2608);
									}
								}
							}
							else
							{
								switch (getRandom(1))
								{
									case 0 ->
									{
										player.teleToLocation(116651, 77512, -2688);
									}
									case 1 ->
									{
										player.teleToLocation(116597, 77539, -2688);
									}
								}
							}
							return null;
						}
						if ((i1 >= 995) && (i1 < 1095))
						{
							if (Util.contains(DAWN_NPCS, npc.getId()))
							{
								player.teleToLocation(148326, -55533, -2776);
							}
							else
							{
								player.teleToLocation(149968, -56645, -2976);
							}
							return null;
						}
						if ((i1 >= 1095) && (i1 < 1195))
						{
							if (Util.contains(DAWN_NPCS, npc.getId()))
							{
								player.teleToLocation(45605, -50360, -792);
							}
							else
							{
								player.teleToLocation(44505, -48331, -792);
							}
							return null;
						}
						if ((i1 >= 1195) && (i1 < 1295))
						{
							if (Util.contains(DAWN_NPCS, npc.getId()))
							{
								player.teleToLocation(86730, -143148, -1336);
							}
							else
							{
								player.teleToLocation(85048, -142046, -1536);
							}
							return null;
						}
						
						if (i2 == 1)
						{
							player.teleToLocation(-41443, 210030, -5080);
						}
						else
						{
							if (i2 == 2)
							{
								player.teleToLocation(-53034, -250421, -7935);
							}
							else
							{
								if (i2 == 3)
								{
									player.teleToLocation(45160, 123605, -5408);
								}
								else
								{
									if (i2 == 4)
									{
										player.teleToLocation(46488, 170184, -4976);
									}
									else
									{
										if (i2 == 5)
										{
											player.teleToLocation(111521, 173905, -5432);
										}
										else
										{
											if (i2 == 6)
											{
												player.teleToLocation(-20395, -250930, -8191);
											}
											else
											{
												if (i2 == 7)
												{
													player.teleToLocation(-21482, 77253, -5168);
												}
												else
												{
													if (i2 == 8)
													{
														player.teleToLocation(140688, 79565, -5424);
													}
													else
													{
														if (i2 == 9)
														{
															player.teleToLocation(-52007, 78986, -4736);
														}
														else
														{
															if (i2 == 10)
															{
																player.teleToLocation(118547, 132669, -4824);
															}
															else
															{
																if (i2 == 11)
																{
																	player.teleToLocation(172562, -17730, -4896);
																}
																else
																{
																	if (i2 == 12)
																	{
																		player.teleToLocation(83344, 209110, -5432);
																	}
																	else
																	{
																		if (i2 == 13)
																		{
																			player.teleToLocation(-19154, 13415, -4896);
																		}
																		else
																		{
																			if (i2 == 14)
																			{
																				player.teleToLocation(12747, -248614, -9607);
																			}
																			else
																			{
																				if (i1 < 95)
																				{
																					return "ssq_main_event_acolyte_q0507_29.htm";
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		return super.onAdvEvent(event, npc, player);
	}
	
	@Override
	public String onTalk(L2Npc npc, L2PcInstance player)
	{
		if ((player.getInventory().getSize(true) >= (player.getQuestInventoryLimit() * QUEST_LIMIT)) || (player.getInventory().getSize(false) >= (player.getInventoryLimit() * WEIGHT_LIMIT)) || (player.getCurrentLoad() >= (player.getMaxLoad() * WEIGHT_LIMIT)))
		{
			player.sendPacket(SystemMessageId.YOU_CAN_PROCEED_ONLY_WHEN_THE_INVENTORY_WEIGHT_IS_BELOW_80_PERCENT_AND_THE_QUANTITY_IS_BELOW_90_PERCENT);
			return null;
		}
		
		final int roomIndex = npc.getTemplate().getParameters().getInt("RoomIndex", 0);
		final String partType = npc.getTemplate().getParameters().getString("part_type", null);
		final int ssqType = partType.equals("TWILIGHT") ? 1 : partType.equals("DAWN") ? 2 : 0;
		
		final SevenSigns ss = SevenSigns.getInstance();
		final SevenSignsFestival ssf = SevenSignsFestival.getInstance();
		
		L2Party party0 = player.getParty();
		if (party0 != null)
		{
			L2PcInstance c0 = party0.getLeader();
			int i1 = party0.getMemberCount();
			
			for (L2PcInstance c1 : party0.getMembers())
			{
				L2Summon c2 = c1.getSummon();
				
				takeItems(c1, BLOOD_OF_OFFERING, getQuestItemsCount(c1, BLOOD_OF_OFFERING));
				
				if (ss.getPlayerCabal(player.getObjectId()) != ssqType)
				{
					Quest q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
					QuestState qs505 = q505.getQuestState(c1, true);
					qs505.exitQuest(true, false);
					
					String html = getHtm(player, "ssq_main_event_acolyte_q0505_01.htm");
					html = html.replace("<?name?>", c1.getName());
					return html;
				}
				
				if (c1.getLevel() > getMinLevel(npc.getId()))
				{
					Quest q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
					QuestState qs505 = q505.getQuestState(c1, true);
					qs505.exitQuest(true, false);
					
					String html = getHtm(player, fStringLevelHtml(npc.getId()));
					html = html.replace("<?name?>", c1.getName());
					return html;
				}
				
				if (player.getAllActiveQuests().size() > (MAX_QUEST_COUNT - 1))
				{
					Quest q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
					QuestState qs505 = q505.getQuestState(c1, true);
					qs505.exitQuest(true, false);
					
					String html = getHtm(player, "ssq_main_event_acolyte_q0505_04e.htm");
					html = html.replace("<?name?>", c1.getName());
					return html;
				}
				
				if (c2 != null)
				{
					Quest q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
					QuestState qs505 = q505.getQuestState(c1, true);
					qs505.exitQuest(true, false);
					
					return "ssq_main_event_acolyte_q0505_31.htm";
				}
			}
			
			final Calendar cal = Calendar.getInstance();
			int i0 = cal.get(Calendar.MINUTE);
			if (((i0 >= 0) && (i0 < 18)) || ((i0 >= 20) && (i0 < 38)) || ((i0 >= 40) && (i0 < 58)) || ((ss.getMilliToPeriodChange() / 1000) <= 120))
			{
				Quest q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
				QuestState qs505 = q505.getQuestState(player, true);
				qs505.exitQuest(true, false);
				
				return "ssq_main_event_acolyte_q0505_22.htm";
			}
			
			long time = System.currentTimeMillis() / 1000;
			i0 = Math.toIntExact(time);
			if ((i1 < FeatureConfig.ALT_FESTIVAL_MIN_PLAYER) && !hasQuestItems(player, THE_STAFF))
			{
				Quest q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
				QuestState qs505 = q505.getQuestState(player, true);
				qs505.exitQuest(true, false);
				
				return "ssq_main_event_acolyte_q0505_30.htm";
			}
			
			if (player.getAllActiveQuests().size() < MAX_QUEST_COUNT)
			{
				if ((ss.getPlayerCabal(player.getObjectId()) == ssqType) && ss.isCompetitionPeriod() && (c0.getObjectId() == player.getObjectId()))
				{
					Quest q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
					QuestState qs505 = q505.getQuestState(player, true);
					
					if (qs505.getMemoStateEx(1) == getBlueStoneCount(npc.getId()))
					{
						if (getQuestItemsCount(player, SevenSigns.SEAL_STONE_BLUE_ID) < getBlueStoneCount(npc.getId()))
						{
							return "ssq_main_event_acolyte_q0505_21a.htm";
						}
					}
					
					if (qs505.getMemoStateEx(1) == getGreenStoneCount(npc.getId()))
					{
						if (getQuestItemsCount(player, SevenSigns.SEAL_STONE_GREEN_ID) < getGreenStoneCount(npc.getId()))
						{
							return "ssq_main_event_acolyte_q0505_21a.htm";
						}
					}
					
					if (qs505.getMemoStateEx(1) == getRedStoneCount(npc.getId()))
					{
						if (getQuestItemsCount(player, SevenSigns.SEAL_STONE_RED_ID) < getRedStoneCount(npc.getId()))
						{
							return "ssq_main_event_acolyte_q0505_21a.htm";
						}
					}
					
					if (ssf.checkEventRoom(ssqType, roomIndex - 1))
					{
						if (qs505.getMemoStateEx(1) == getBlueStoneCount(npc.getId()))
						{
							if (getQuestItemsCount(player, SevenSigns.SEAL_STONE_BLUE_ID) < getBlueStoneCount(npc.getId()))
							{
								return "ssq_main_event_acolyte_q0505_21a.htm";
							}
							
							takeItems(player, SevenSigns.SEAL_STONE_BLUE_ID, getBlueStoneCount(npc.getId()));
						}
						
						if (qs505.getMemoStateEx(1) == getGreenStoneCount(npc.getId()))
						{
							if (getQuestItemsCount(player, SevenSigns.SEAL_STONE_GREEN_ID) < getGreenStoneCount(npc.getId()))
							{
								return "ssq_main_event_acolyte_q0505_21a.htm";
							}
							
							takeItems(player, SevenSigns.SEAL_STONE_GREEN_ID, getGreenStoneCount(npc.getId()));
						}
						
						if (qs505.getMemoStateEx(1) == getRedStoneCount(npc.getId()))
						{
							if (getQuestItemsCount(player, SevenSigns.SEAL_STONE_RED_ID) < getRedStoneCount(npc.getId()))
							{
								return "ssq_main_event_acolyte_q0505_21a.htm";
							}
							
							takeItems(player, SevenSigns.SEAL_STONE_RED_ID, getRedStoneCount(npc.getId()));
						}
						
						for (L2PcInstance c1 : party0.getMembers())
						{
							Quest q505c1 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
							QuestState qs505c1 = q505c1.getQuestState(c1, true);
							
							qs505c1.setMemoState(i0);
						}
						
						qs505.startQuest();
						qs505.setMemoStateEx(1, roomIndex - 1);
						
						if (npc.checkDoCastConditions(SSQ_CANCEL.getSkill()))
						{
							npc.setTarget(player);
							npc.doCast(SSQ_CANCEL);
						}
						
						ssf.addPartyToEventRoom(ssqType, roomIndex - 1, party0);
						ssf.addTimeAttackFee(roomIndex - 1, getTimeAttackFee(npc.getId()));
					}
					else
					{
						return "ssq_main_event_acolyte_q0505_05.htm";
					}
				}
			}
			else
			{
				return "data/html/fullquest.html";
			}
		}
		
		return super.onTalk(npc, player);
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		String html = getHtm(player, fnHi(npc.getId()));
		
		final Calendar cal = Calendar.getInstance();
		int i0 = cal.get(Calendar.MINUTE);
		if ((i0 == 58) || (i0 == 18) || (i0 == 38) || (i0 == 59) || (i0 == 19) || (i0 == 39))
		{
			html = html.replace("<?minute?>", "0 minute");
		}
		else
		{
			int i1;
			if (i0 > 39)
			{
				i1 = 58 - i0;
			}
			else
			{
				if (i0 > 19)
				{
					i1 = 38 - i0;
				}
				else
				{
					i1 = 18 - i0;
				}
			}
			html = html.replace("<?minute?>", i1 + " Minute");
		}
		
		return html;
	}
	
	@Override
	public String onSpellFinished(L2Npc npc, L2PcInstance player, Skill skill)
	{
		final int sibylPosX = npc.getTemplate().getParameters().getInt("SibylPosX", 0);
		final int sibylPosY = npc.getTemplate().getParameters().getInt("SibylPosY", 0);
		final int sibylPosZ = npc.getTemplate().getParameters().getInt("SibylPosZ", 0);
		
		L2Party party0 = player.getParty();
		if (party0 != null)
		{
			for (L2PcInstance pc : party0.getMembers())
			{
				
				final double distance = Util.calculateDistance(npc, pc, true, false);
				if (distance <= MAX_DISTANCE)
				{
					pc.teleToLocation(new Location(sibylPosX, sibylPosY, sibylPosZ));
				}
			}
		}
		return super.onSpellFinished(npc, player, skill);
	}
}