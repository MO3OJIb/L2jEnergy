/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.npc.FestivalMobs;

import com.l2jserver.gameserver.SevenSignsFestival;
import com.l2jserver.gameserver.enums.CategoryType;
import com.l2jserver.gameserver.model.L2Party;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.SkillHolder;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.Util;

import ai.npc.AbstractNpcAI;

/**
 * SSQ Event Bomb
 * @author Charus
 */
public final class FestivalBomb extends AbstractNpcAI
{
	// @formatter:off
	// Npcs
	private final static int[] DAWN_NPCS = 
	{
		18317, 18315, 18313, 18311, 18309
	};
	
	private final static int[] DUSK_NPCS = 
	{
		18307, 18305, 18303, 18301, 18299
	};
	 // @formatter:on
	
	private static final int Q00505_BloodOffering = 505;
	
	private static final SkillHolder DDMagic = new SkillHolder(4614, 9);
	
	public FestivalBomb()
	{
		super(FestivalBomb.class.getSimpleName(), "ai/npc");
		
		addSpawnId(DAWN_NPCS);
		addSpawnId(DUSK_NPCS);
		addAttackId(DAWN_NPCS);
		addAttackId(DUSK_NPCS);
		addKillId(DAWN_NPCS);
		addKillId(DUSK_NPCS);
		addMoveFinishedId(DAWN_NPCS);
		addMoveFinishedId(DUSK_NPCS);
		addSpellFinishedId(DAWN_NPCS);
		addSpellFinishedId(DUSK_NPCS);
	}
	
	@Override
	public String onSpawn(L2Npc npc)
	{
		final int roomIndex = npc.getTemplate().getParameters().getInt("RoomIndex", 0);
		final String partType = npc.getTemplate().getParameters().getString("SSQPart", null);
		final int ssqPart = partType.equals("TWILIGHT") ? 1 : partType.equals("DAWN") ? 2 : 0;
		
		final int[] loc = ssqPart == 1 ? SevenSignsFestival.FESTIVAL_DUSK_PLAYER_SPAWNS[roomIndex - 1] : SevenSignsFestival.FESTIVAL_DAWN_PLAYER_SPAWNS[roomIndex - 1];
		addMoveToDesire(npc, new Location(loc[0], loc[1], loc[2]), 50);
		
		return super.onSpawn(npc);
	}
	
	@Override
	public String onAttack(L2Npc npc, L2PcInstance attacker, int damage, boolean isSummon, Skill skill)
	{
		final int roomIndex = npc.getTemplate().getParameters().getInt("RoomIndex", 0);
		final String partType = npc.getTemplate().getParameters().getString("SSQPart", null);
		final int ssqPart = partType.equals("TWILIGHT") ? 1 : partType.equals("DAWN") ? 2 : 0;
		
		if ((roomIndex != -1) && (ssqPart != 0))
		{
			L2Party party0 = attacker.getParty();
			if (party0 == null)
			{
				npc.setScriptValue("i_ai1", 1);
			}
		}
		if (attacker.isInCategory(CategoryType.PET_GROUP))
		{
			npc.setScriptValue("i_ai1", 1);
		}
		
		return super.onAttack(npc, attacker, damage, isSummon, skill);
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance killer, boolean isSummon)
	{
		int i1;
		
		L2PcInstance c0 = killer.getActingPlayer();
		if (c0 != null)
		{
			L2Party party0 = c0.getParty();
			if (party0 != null)
			{
				c0 = party0.getLeader();
				
				if (haveMemo(c0, Q00505_BloodOffering))
				{
					if (npc.getScriptValue("i_ai1") == 1)
					{
						i1 = 3 + ((3 * 25) / 100);
					}
					else
					{
						i1 = 30 + ((30 * 25) / 100);
					}
					
					final double distance = Util.calculateDistance(npc, c0, true, false);
					if (c0.isInCategory(CategoryType.WIZARD_GROUP))
					{
						if (distance < 40)
						{
							i1 = i1 + ((i1 * 30) / 100);
						}
					}
					else
					{
						if (distance < 80)
						{
							i1 = i1 + ((i1 * 30) / 100);
						}
					}
					
					if (c0.isInCategory(CategoryType.WIZARD_GROUP))
					{
						if (getRandom(0, 2) > 0)
						{
							i1 = i1 + ((i1 * 7) / 100);
						}
						else
						{
							i1 = i1 - ((i1 * 15) / 100);
						}
					}
					else
					{
						if (getRandom(0, 2) > 0)
						{
							i1 = i1 + ((i1 * 15) / 100);
						}
						else
						{
							i1 = i1 - ((i1 * 7) / 100);
						}
					}
					
					SystemMessage smsg = SystemMessage.getSystemMessage(SystemMessageId.LEADER_OBTAINED_S2_OF_S1);
					smsg.addItemName(SevenSignsFestival.FESTIVAL_OFFERING_ID);
					smsg.addLong(i1);
					party0.broadcastToPartyMembers(c0, smsg);
					
					giveItems(c0, SevenSignsFestival.FESTIVAL_OFFERING_ID, i1);
				}
			}
		}
		
		return super.onKill(npc, killer, isSummon);
	}
	
	@Override
	public void onMoveFinished(L2Npc npc)
	{
		if (npc.checkDoCastConditions(DDMagic.getSkill()))
		{
			npc.doCast(DDMagic);
		}
	}
	
	@Override
	public String onSpellFinished(L2Npc npc, L2PcInstance player, Skill skill)
	{
		npc.doDie(npc);
		
		return super.onSpellFinished(npc, player, skill);
	}
}