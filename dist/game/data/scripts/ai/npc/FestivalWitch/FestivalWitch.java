/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.npc.FestivalWitch;

import java.util.HashMap;
import java.util.Map;

import com.l2jserver.gameserver.SevenSignsFestival;
import com.l2jserver.gameserver.SevenSignsFestival.L2DarknessFestival;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.instancemanager.ZoneManager;
import com.l2jserver.gameserver.model.L2Party;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.zone.L2ZoneType;
import com.l2jserver.gameserver.network.NpcStringId;
import com.l2jserver.gameserver.network.serverpackets.PlaySound;

import ai.npc.AbstractNpcAI;

/**
 * Seven Signs Festival AI
 * @author Charus
 */
public final class FestivalWitch extends AbstractNpcAI
{
	// @formatter:off
	// NPCs
	private static final int[] DAWN_NPCS = 
	{
		31132, 31133, 31134, 31135, 31136
	};
	
	private static final int[] DUSK_NPCS = 
	{
		31142, 31143, 31144, 31145, 31146
	};
	
	private final static int[][] ZONES = 
	{
		{ 31136, 1721001 }, { 31135, 1721002 }, { 31134, 1721003 }, { 31133, 1721004 }, { 31132, 1721005 },
		{ 31146, 1720001 }, { 31145, 1720002 }, { 31144, 1720003 }, { 31143, 1720004 }, { 31142, 1720005 }
	};
    // @formatter:on
	
	private static Map<Integer, Integer> _zoneList = new HashMap<>();
	
	private final String fnHi(int npcId)
	{
		return switch (npcId)
		{
			case 31132 -> "dawn_sibyl1001.htm";
			case 31133 -> "dawn_sibyl2001.htm";
			case 31134 -> "dawn_sibyl3001.htm";
			case 31135 -> "dawn_sibyl4001.htm";
			case 31136 -> "dawn_sibyl5001.htm";
			case 31142 -> "dusk_sibyl1001.htm";
			case 31143 -> "dusk_sibyl2001.htm";
			case 31144 -> "dusk_sibyl3001.htm";
			case 31145 -> "dusk_sibyl4001.htm";
			case 31146 -> "dusk_sibyl5001.htm";
			default -> null;
		};
	}
	
	public FestivalWitch()
	{
		super(FestivalWitch.class.getSimpleName(), "ai/npc");
		
		addSpawnId(DAWN_NPCS);
		addSpawnId(DUSK_NPCS);
		addStartNpc(DAWN_NPCS);
		addStartNpc(DUSK_NPCS);
		addFirstTalkId(DAWN_NPCS);
		addFirstTalkId(DUSK_NPCS);
		
		for (int[] zone : ZONES)
		{
			_zoneList.put(zone[0], zone[1]);
		}
	}
	
	@Override
	public String onSpawn(L2Npc npc)
	{
		npc.setScriptValue("i_ai0", System.currentTimeMillis());
		npc.setScriptValue("i_ai1", 0);
		npc.setScriptValue("i_ai3", System.currentTimeMillis());
		npc.setScriptValue("i_quest0", 0);
		npc.setScriptValue("i_quest1", 1);
		npc.setScriptValue("i_quest2", 0);
		npc.setScriptValue("i_quest3", 0);
		npc.setScriptValue("i_quest4", 1);
		npc.setScriptValue("i_ai2", 0);
		npc.setScriptValue("i_ai4", 1);
		npc.setScriptValue("sm.param1", 0);
		npc.setScriptValue("sm.flag", 0);
		startQuestTimer("3001", 3000, npc, null);
		npc.broadcastPacket(PlaySound.createSound("B06_S01"));
		startQuestTimer("3023", 20 * 1000, npc, null);
		
		return super.onSpawn(npc);
		
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		final int roomIndex = npc.getTemplate().getParameters().getInt("room_index", 0);
		final String partType = npc.getTemplate().getParameters().getString("part_type", null);
		final int ssqType = partType.equals("TWILIGHT") ? 1 : partType.equals("DAWN") ? 2 : 0;
		
		final int escapeX = npc.getTemplate().getParameters().getInt("escape_x", 0);
		final int escapeY = npc.getTemplate().getParameters().getInt("escape_y", 0);
		final int coordZ = npc.getTemplate().getParameters().getInt("coord_z", 0);
		
		final int battlePresentName = npc.getTemplate().getParameters().getInt("battle_present_name", 0);
		final int battleBombManName = npc.getTemplate().getParameters().getInt("battle_bomb_man_name", 0);
		
		final SevenSignsFestival ssf = SevenSignsFestival.getInstance();
		
		if (event.equalsIgnoreCase("3001"))
		{
			if ((((System.currentTimeMillis() - npc.getScriptValue("i_ai0")) / 1000) > ((60 * 17) + 45)) && (npc.getScriptValue("i_ai1") == 2))
			{
				npc.setScriptValue("i_ai1", 3);
				broadcastNpcSay(npc, ChatType.NPC_SHOUT, NpcStringId.THAT_WILL_DO_ILL_MOVE_YOU_TO_THE_OUTSIDE_SOON);
			}
			if ((((System.currentTimeMillis() - npc.getScriptValue("i_ai0")) / 1000) > (60 * 10)) && (npc.getScriptValue("i_ai1") == 1))
			{
				for (int i0 = 0; i0 < 10; i0++)
				{
					int i1 = getRandom(4);
					int i2 = npc.getX();
					int i3 = npc.getY();
					
					if (i1 == 0)
					{
						i2 = ((i2 + 200) + 50) - getRandom(100);
						i3 = ((i3 + 200) + 50) - getRandom(100);
					}
					else
					{
						if (i1 == 1)
						{
							i2 = ((i2 + 200) + 50) - getRandom(100);
							i3 = ((i3 - 200) + 50) - getRandom(100);
						}
						else
						{
							if (i1 == 2)
							{
								i2 = ((i2 - 200) + 50) - getRandom(100);
								i3 = ((i3 + 200) + 50) - getRandom(100);
							}
							else
							{
								i2 = ((i2 - 200) + 50) - getRandom(100);
								i3 = ((i3 - 200) + 50) - getRandom(100);
							}
						}
					}
					
					addSpawn(battlePresentName, i2, i3, coordZ, 0, false, 0, false);
				}
				
				npc.setScriptValue("i_ai1", 2);
			}
			if (((System.currentTimeMillis() - npc.getScriptValue("i_ai0")) / 1000) > ((60 * 17) + 50))
			{
				instantTeleportInMyTerritory(npc, escapeX, escapeY, coordZ);
				
				L2DarknessFestival festival = ssf.getManagerInstance().getFestivalInstance(ssqType, roomIndex - 1);
				festival.festivalEnd();
				npc.deleteMe();
				return null;
			}
			if ((((System.currentTimeMillis() - npc.getScriptValue("i_ai0")) / 1000) > 10) && (npc.getScriptValue("i_ai1") == 0))
			{
				npc.setScriptValue("i_ai1", 1);
			}
			if (((System.currentTimeMillis() - npc.getScriptValue("i_ai0")) / 1000) > (60 * 5))
			{
				long i0 = (System.currentTimeMillis() - npc.getScriptValue("i_ai0")) / 1000;
				if (i0 > (15 * 60))
				{
					npc.setScriptValue("i_ai4", 4);
				}
				else
				{
					if (i0 > (10 * 60))
					{
						npc.setScriptValue("i_ai4", 3);
					}
					else
					{
						if (i0 > (8 * 60))
						{
							npc.setScriptValue("i_ai4", 4);
						}
						else
						{
							if (i0 > (6 * 60))
							{
								npc.setScriptValue("i_ai4", 2);
							}
						}
					}
				}
				
				i0 = 0;
				if ((npc.getScriptValue("i_ai2") & 1) == 1)
				{
					i0 = i0 + 1;
				}
				if ((npc.getScriptValue("i_ai2") & 16) == 16)
				{
					i0 = i0 + 1;
				}
				if ((npc.getScriptValue("i_ai2") & 256) == 256)
				{
					i0 = i0 + 1;
				}
				if ((npc.getScriptValue("i_ai2") & 4096) == 4096)
				{
					i0 = i0 + 1;
				}
				if (i0 < npc.getScriptValue("i_ai4"))
				{
					int i2 = getRandom(1, 4);
					if (i2 == 1)
					{
						i2 = 1;
					}
					else
					{
						if (i2 == 2)
						{
							i2 = 16;
						}
						else
						{
							if (i2 == 3)
							{
								i2 = 256;
							}
							else
							{
								if (i2 == 4)
								{
									i2 = 4096;
								}
							}
						}
					}
					
					i0 = 0;
					for (int i1 = 1; i1 < 5; i1++)
					{
						if ((i0 == 0) && ((i2 & npc.getScriptValue("i_ai2")) == 0))
						{
							i0 = i2;
							npc.setScriptValue("i_ai2", npc.getScriptValue("i_ai2") + i0);
						}
						else
						{
							if (i2 == 1)
							{
								i2 = 16;
							}
							else
							{
								if (i2 == 16)
								{
									i2 = 256;
								}
								else
								{
									if (i2 == 256)
									{
										i2 = 4096;
									}
									else
									{
										if (i2 == 4096)
										{
											i2 = 1;
										}
									}
								}
							}
						}
					}
					
					long i1 = 3000 + (((60 * 18) - ((System.currentTimeMillis() - npc.getScriptValue("i_ai0")) / 1000)) * 10);
					if (i0 == 1)
					{
						startQuestTimer("3010", i1, npc, null);
					}
					else
					{
						if (i0 == 16)
						{
							startQuestTimer("3011", i1, npc, null);
						}
						else
						{
							if (i0 == 256)
							{
								startQuestTimer("3012", i1, npc, null);
							}
							else
							{
								if (i0 == 4096)
								{
									startQuestTimer("3013", i1, npc, null);
								}
							}
						}
					}
				}
				
				i0 = (System.currentTimeMillis() - npc.getScriptValue("i_ai0")) / 1000;
				if (i0 > (16 * 60))
				{
					npc.setScriptValue("i_quest4", 4);
				}
				else
				{
					if (i0 > (13 * 60))
					{
						npc.setScriptValue("i_quest4", 3);
					}
					else
					{
						if (i0 > (10 * 60))
						{
							npc.setScriptValue("i_quest4", 2);
						}
						else
						{
							if (i0 > (8 * 60))
							{
								npc.setScriptValue("i_quest4", 3);
							}
							else
							{
								if (i0 > (6 * 60))
								{
									npc.setScriptValue("i_quest4", 2);
								}
							}
						}
					}
				}
				
				i0 = 0;
				if ((npc.getScriptValue("i_quest2") & 1) == 1)
				{
					i0 = i0 + 1;
				}
				if ((npc.getScriptValue("i_quest2") & 16) == 16)
				{
					i0 = i0 + 1;
				}
				if ((npc.getScriptValue("i_quest2") & 256) == 256)
				{
					i0 = i0 + 1;
				}
				if ((npc.getScriptValue("i_quest2") & 4096) == 4096)
				{
					i0 = i0 + 1;
				}
				
				if (i0 < npc.getScriptValue("i_quest4"))
				{
					int i2 = getRandom(1, 4);
					if (i2 == 1)
					{
						i2 = 1;
					}
					else
					{
						if (i2 == 2)
						{
							i2 = 16;
						}
						else
						{
							if (i2 == 3)
							{
								i2 = 256;
							}
							else
							{
								if (i2 == 4)
								{
									i2 = 4096;
								}
							}
						}
					}
					
					i0 = 0;
					for (int i1 = 1; i1 < 5; i1++)
					{
						if ((i0 == 0) && ((i2 & npc.getScriptValue("i_quest2")) == 0))
						{
							i0 = i2;
							npc.setScriptValue("i_quest2", npc.getScriptValue("i_quest2") + i0);
						}
						else
						{
							if (i2 == 1)
							{
								i2 = 16;
							}
							else
							{
								if (i2 == 16)
								{
									i2 = 256;
								}
								else
								{
									if (i2 == 256)
									{
										i2 = 4096;
									}
									else
									{
										if (i2 == 4096)
										{
											i2 = 1;
										}
									}
								}
							}
						}
					}
					
					long i1 = 3000 + (((60 * 18) - ((System.currentTimeMillis() - npc.getScriptValue("i_ai0")) / 1000)) * 10);
					if (i0 == 1)
					{
						startQuestTimer("3014", i1, npc, null);
					}
					else
					{
						if (i0 == 16)
						{
							startQuestTimer("3015", i1, npc, null);
						}
						else
						{
							if (i0 == 256)
							{
								startQuestTimer("3016", i1, npc, null);
							}
							else
							{
								if (i0 == 4096)
								{
									startQuestTimer("3017", i1, npc, null);
								}
							}
						}
					}
				}
				
				i0 = 0;
				if ((npc.getScriptValue("i_quest3") & 1) == 1)
				{
					i0 = i0 + 1;
				}
				if ((npc.getScriptValue("i_quest3") & 16) == 16)
				{
					i0 = i0 + 1;
				}
				if ((npc.getScriptValue("i_quest3") & 256) == 256)
				{
					i0 = i0 + 1;
				}
				if ((npc.getScriptValue("i_quest3") & 4096) == 4096)
				{
					i0 = i0 + 1;
				}
				
				if (i0 < npc.getScriptValue("i_quest4"))
				{
					int i2 = getRandom(1, 4);
					if (i2 == 1)
					{
						i2 = 1;
					}
					else
					{
						if (i2 == 2)
						{
							i2 = 16;
						}
						else
						{
							if (i2 == 3)
							{
								i2 = 256;
							}
							else
							{
								if (i2 == 4)
								{
									i2 = 4096;
								}
							}
						}
					}
					
					i0 = 0;
					for (int i1 = 1; i1 < 5; i1++)
					{
						if ((i0 == 0) && ((i2 & npc.getScriptValue("i_quest3")) == 0))
						{
							i0 = i2;
							npc.setScriptValue("i_quest3", npc.getScriptValue("i_quest3") + i0);
						}
						else
						{
							if (i2 == 1)
							{
								i2 = 16;
							}
							else
							{
								if (i2 == 16)
								{
									i2 = 256;
								}
								else
								{
									if (i2 == 256)
									{
										i2 = 4096;
									}
									else
									{
										if (i2 == 4096)
										{
											i2 = 1;
										}
									}
								}
							}
						}
					}
					
					long i1 = 3000 + (((60 * 18) - ((System.currentTimeMillis() - npc.getScriptValue("i_ai0")) / 1000)) * 10);
					if (i0 == 1)
					{
						startQuestTimer("3018", i1, npc, null);
					}
					else
					{
						if (i0 == 16)
						{
							startQuestTimer("3019", i1, npc, null);
						}
						else
						{
							if (i0 == 256)
							{
								startQuestTimer("3020", i1, npc, null);
							}
							else
							{
								if (i0 == 4096)
								{
									startQuestTimer("3021", i1, npc, null);
								}
							}
						}
					}
				}
				
				if (npc.getScriptValue("sm.flag") == 0)
				{
					if (getRandom(100) < 10)
					{
						addSpawn(battleBombManName, (npc.getX() + 250) + getRandom(100), (npc.getY() + 250) + getRandom(100), npc.getZ(), 0, false, 0, false);
						npc.setScriptValue("sm.flag", System.currentTimeMillis());
					}
					else
					{
						if ((((System.currentTimeMillis() - npc.getScriptValue("sm.flag")) / 1000) > 260) && (getRandom(100) < 30))
						{
							addSpawn(battleBombManName, (npc.getX() + 250) + getRandom(100), (npc.getY() + 250) + getRandom(100), npc.getZ(), 0, false, 0, false);
							npc.setScriptValue("sm.flag", System.currentTimeMillis());
						}
					}
				}
			}
			
			long i0 = (System.currentTimeMillis() - npc.getScriptValue("i_ai0")) / 1000;
			if (i0 > (14 * 60))
			{
				npc.setScriptValue("i_quest1", 4);
			}
			else
			{
				if (i0 > (12 * 60))
				{
					npc.setScriptValue("i_quest1", 3);
				}
				else
				{
					if (i0 > (10 * 60))
					{
						npc.setScriptValue("i_quest1", 2);
					}
					else
					{
						if (i0 > (8 * 60))
						{
							npc.setScriptValue("i_quest1", 3);
						}
						else
						{
							if (i0 > (6 * 60))
							{
								npc.setScriptValue("i_quest1", 2);
							}
						}
					}
				}
			}
			
			i0 = 0;
			if ((npc.getScriptValue("i_quest0") & 1) == 1)
			{
				i0 = i0 + 1;
			}
			if ((npc.getScriptValue("i_quest0") & 16) == 16)
			{
				i0 = i0 + 1;
			}
			if ((npc.getScriptValue("i_quest0") & 256) == 256)
			{
				i0 = i0 + 1;
			}
			if ((npc.getScriptValue("i_quest0") & 4096) == 4096)
			{
				i0 = i0 + 1;
			}
			
			if (i0 < npc.getScriptValue("i_quest1"))
			{
				int i2 = getRandom(1, 4);
				if (i2 == 1)
				{
					i2 = 1;
				}
				else
				{
					if (i2 == 2)
					{
						i2 = 16;
					}
					else
					{
						if (i2 == 3)
						{
							i2 = 256;
						}
						else
						{
							if (i2 == 4)
							{
								i2 = 4096;
							}
						}
					}
				}
				
				i0 = 0;
				for (int i1 = 1; i1 < 5; i1++)
				{
					if ((i0 == 0) && ((i2 & npc.getScriptValue("i_quest0")) == 0))
					{
						i0 = i2;
						npc.setScriptValue("i_quest0", npc.getScriptValue("i_quest0") + i0);
					}
					else
					{
						if (i2 == 1)
						{
							i2 = 16;
						}
						else
						{
							if (i2 == 16)
							{
								i2 = 256;
							}
							else
							{
								if (i2 == 256)
								{
									i2 = 4096;
								}
								else
								{
									if (i2 == 4096)
									{
										i2 = 1;
									}
								}
							}
						}
					}
				}
				
				long i1 = 3000 + (((60 * 18) - ((System.currentTimeMillis() - npc.getScriptValue("i_ai0")) / 1000)) * 10);
				if (i0 == 1)
				{
					startQuestTimer("3006", i1, npc, null);
				}
				else
				{
					if (i0 == 16)
					{
						startQuestTimer("3007", i1, npc, null);
					}
					else
					{
						if (i0 == 256)
						{
							startQuestTimer("3008", i1, npc, null);
						}
						else
						{
							if (i0 == 4096)
							{
								startQuestTimer("3009", i1, npc, null);
							}
						}
					}
				}
			}
			
			startQuestTimer("3001", 3000, npc, null);
		}
		
		if (event.equalsIgnoreCase("3022"))
		{
			instantTeleportInMyTerritory(npc, escapeX, escapeY, coordZ);
			// ClearEventRoom(room_index, part_type, 0);
		}
		
		if (event.equalsIgnoreCase("3023"))
		{
			npc.broadcastPacket(PlaySound.createSound("B06_S01"));
			startQuestTimer("3024", 180 * 1000, npc, null);
		}
		
		if (event.equalsIgnoreCase("3024"))
		{
			npc.broadcastPacket(PlaySound.createSound("B07_S01"));
			startQuestTimer("3025", 122 * 1000, npc, null);
		}
		
		if (event.equalsIgnoreCase("3025"))
		{
			npc.broadcastPacket(PlaySound.createSound("B07_S01"));
			startQuestTimer("3026", 122 * 1000, npc, null);
		}
		
		if (event.equalsIgnoreCase("3026"))
		{
			npc.broadcastPacket(PlaySound.createSound("B07_S01"));
			startQuestTimer("3027", 122 * 1000, npc, null);
		}
		
		if (event.equalsIgnoreCase("3027"))
		{
			npc.broadcastPacket(PlaySound.createSound("B07_S01"));
			startQuestTimer("3028", 180 * 1000, npc, null);
		}
		
		if (event.equalsIgnoreCase("3028"))
		{
			npc.broadcastPacket(PlaySound.createSound("B06_F"));
			startQuestTimer("3029", 120 * 1000, npc, null);
		}
		
		if (event.equalsIgnoreCase("3029"))
		{
			npc.broadcastPacket(PlaySound.createSound("B06_F"));
		}
		
		int ask = Integer.parseInt(event.split(";")[0]);
		switch (ask)
		{
			case 505 ->
			{
				int reply = Integer.parseInt(event.split(";")[1]);
				switch (reply)
				{
					case 1 ->
					{
						L2Party party = player.getParty();
						if (party == null)
						{
							player.teleToLocation(escapeX, escapeY, coordZ);
						}
						else
						{
							if (party.getLeaderObjectId() != player.getObjectId())
							{
								return "ssq_main_event_sibyl_q0505_04.htm";
							}
							
							return "ssq_main_event_sibyl_q0505_01.htm";
						}
					}
					case 2 ->
					{
						L2Party party = player.getParty();
						if ((party == null) || (party.getLeaderObjectId() != player.getObjectId()))
						{
							return "ssq_main_event_sibyl_q0505_04.htm";
						}
						
						return "ssq_main_event_sibyl_q0505_02.htm";
					}
					case 3 ->
					{
						startQuestTimer("3022", 7000, npc, null);
						
						return "ssq_main_event_sibyl_q0505_05.htm";
					}
					case 4 ->
					{
						if ((((System.currentTimeMillis() - npc.getScriptValue("i_ai3")) / 1000) < 10) || (npc.getScriptValue("sm.param1") >= 5))
						{
							return "ssq_main_event_sibyl_q0505_03.htm";
						}
						
						ssf.increaseChallenge(ssqType, roomIndex - 1);
						npc.setScriptValue("sm.param1", npc.getScriptValue("sm.param1") + 1);
						npc.setScriptValue("i_ai3", System.currentTimeMillis());
					}
				}
			}
		}
		
		return super.onAdvEvent(event, npc, player);
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		return fnHi(npc.getId());
	}
	
	private void instantTeleportInMyTerritory(L2Npc npc, int escapeX, int escapeY, int coordZ)
	{
		L2ZoneType zone = ZoneManager.getInstance().getZoneById(_zoneList.get(npc.getId()));
		for (L2PcInstance pc : zone.getPlayersInside())
		{
			if (pc != null)
			{
				pc.teleToLocation(new Location(escapeX, escapeY, coordZ), 100);
			}
		}
	}
}