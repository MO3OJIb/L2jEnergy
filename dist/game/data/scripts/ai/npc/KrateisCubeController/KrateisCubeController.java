/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.npc.KrateisCubeController;

import com.l2jserver.gameserver.instancemanager.games.KrateisCubeManager;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.KrateisCubeEngine;
import com.l2jserver.gameserver.model.olympiad.OlympiadManager;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ExPVPMatchCCRetire;

import ai.npc.AbstractNpcAI;

/**
 * Krateis Cube Controller AI.
 * @author U3Games
 */
// TODO: need fix and rework
public class KrateisCubeController extends AbstractNpcAI
{
	// Npc Manager
	private static final int MATCH_MANAGER = 32503;
	
	// Npc Instance
	private static final int ENTRANCE_MANAGER_LVL_70 = 32504;
	private static final int ENTRANCE_MANAGER_LVL_76 = 32505;
	private static final int ENTRANCE_MANAGER_LVL_80 = 32506;
	
	private static final Location LOCATION_FANTASY_ISLE = new Location(-59716, -55920, -2048);
	
	public KrateisCubeController()
	{
		super(KrateisCubeController.class.getSimpleName(), "ai/npc");
		addStartNpc(MATCH_MANAGER, ENTRANCE_MANAGER_LVL_70, ENTRANCE_MANAGER_LVL_76, ENTRANCE_MANAGER_LVL_80);
		addTalkId(MATCH_MANAGER, ENTRANCE_MANAGER_LVL_70, ENTRANCE_MANAGER_LVL_76, ENTRANCE_MANAGER_LVL_80);
		addFirstTalkId(MATCH_MANAGER, ENTRANCE_MANAGER_LVL_70, ENTRANCE_MANAGER_LVL_76, ENTRANCE_MANAGER_LVL_80);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		String htmltext = null;
		if (npc.getId() == MATCH_MANAGER)
		{
			switch (event)
			{
				case "32503-01.html":
				{
					htmltext = event;
					break;
				}
				case "SeeList":
				{
					if (player.getLevel() < 70)
					{
						htmltext = "32503-09.html";
					}
					else
					{
						htmltext = "32503-02.html";
					}
					break;
				}
				case "Kratei_Register":
				{
					int arena = checkArena(player);
					
					if ((player.getWeightPenalty() >= 3) || ((player.getInventoryLimit() * 0.8) <= player.getInventory().getSize()))
					{
						htmltext = "32503-05.html";
					}
					
					if (player.isInParty())
					{
						htmltext = "32503-06.html";
					}
					
					if ((player.getPvpFlag() > 0) || (player.getKarma() > 0))
					{
						htmltext = "32503-12.html";
					}
					
					if (player.isCursedWeaponEquipped())
					{
						player.sendPacket(SystemMessageId.YOU_CANNOT_REGISTER_WHILE_IN_POSSESSION_OF_A_CURSED_WEAPON);
					}
					
					if (player.isOnEvent() || player.isInOlympiadMode())
					{
						player.sendPacket(SystemMessageId.YOU_CANNOT_BE_SIMULTANEOUSLY_REGISTERED_FOR_PVP_MATCHES_SUCH_AS_THE_OLYMPIAD_UNDERGROUND_COLISEUM_AERIAL_CLEFT_KRATEIS_CUBE_AND_HANDYS_BLOCK_CHECKERS);
					}
					
					if (player.isDead() || player.isAlikeDead())
					{
						player.sendPacket(SystemMessageId.CANNOT_ENTER_CAUSE_DONT_MATCH_REQUIREMENTS);
					}
					
					if (player.isInCombat())
					{
						player.sendPacket(SystemMessageId.YOU_ARE_UNABLE_TO_ENGAGE_IN_COMBAT_PLEASE_GO_TO_THE_NEAREST_RESTART_POINT);
					}
					
					if (OlympiadManager.getInstance().isRegistered(player))
					{
						OlympiadManager.getInstance().unRegisterNoble(player);
						player.sendPacket(SystemMessageId.APPLICANTS_FOR_THE_OLYMPIAD_UNDERGROUND_COLISEUM_OR_KRATEIS_CUBE_MATCHES_CANNOT_REGISTER);
					}
					
					// TODO: add
					// if(UnderGroundColiseum.getInstance().isRegisteredPlayer(player))
					// {
					// UngerGroundColiseum.getInstance().removeParticipant(player);
					// player.sendPacket(SystemMessageId.COLISEUM_OLYMPIAD_KRATEIS_APPLICANTS_CANNOT_PARTICIPATE));
					// }
					// HandysBlock isRegisteredPlayer
					
					if (!KrateisCubeManager.checkMaxPlayersArena(arena))
					{
						htmltext = "32503-04.html";
					}
					
					if (KrateisCubeManager.getInstance().checkIsRegistered(player))
					{
						htmltext = "32503-07.html";
					}
					else
					{
						int cmdChoice = Integer.parseInt(event.substring(9, 10).trim());
						switch (cmdChoice)
						{
							case 1:
								if ((player.getLevel() < 70) || (player.getLevel() > 75))
								{
									htmltext = "32503-09.html";
								}
								break;
							
							case 2:
								if ((player.getLevel() < 76) || (player.getLevel() > 79))
								{
									htmltext = "32503-09.html";
								}
								break;
							
							case 3:
								if (player.getLevel() < 80)
								{
									htmltext = "32503-09.html";
								}
								break;
						}
						
						if (KrateisCubeManager.getInstance().isTimeToRegister())
						{
							KrateisCubeManager.getInstance().registerPlayer(player, arena);
							htmltext = "32503-08.html";
						}
						else
						{
							htmltext = "32503-07.html";
						}
					}
					htmltext = "32503-02.html";
					break;
				}
				case "UnRegister":
				{
					if (KrateisCubeManager.getInstance().checkIsRegistered(player))
					{
						KrateisCubeManager.getInstance().unregisterPlayer(player);
						htmltext = "32503-11.html";
					}
					break;
				}
				case "teleportToFantasy":
				{
					player.teleToLocation(LOCATION_FANTASY_ISLE);
					break;
				}
			}
		}
		
		if ((npc.getId() == ENTRANCE_MANAGER_LVL_70) || (npc.getId() == ENTRANCE_MANAGER_LVL_76) || (npc.getId() == ENTRANCE_MANAGER_LVL_80))
		{
			switch (event)
			{
				case "KrateiExit":
				{
					if (KrateisCubeManager.checkIsInsided(player))
					{
						// Delete player
						KrateisCubeManager.getInstance().unregisterPlayer(player);
						
						// Effect
						player.getEffectList().stopAllEffects();
						player.stopAllEffects();
						
						// Teleport
						player.setInstanceId(0);
						player.teleToLocation(-70381, -70937, -1428, 0, 0);
						
						// Screen
						player.sendPacket(ExPVPMatchCCRetire.STATIC_PACKET);
						
						// Update
						player.broadcastStatusUpdate();
						player.broadcastUserInfo();
					}
					else
					{
						player.teleToLocation(-70381, -70937, -1428, 0, 0);
					}
					break;
				}
				case "KrateiEnter":
				{
					if (KrateisCubeManager.checkIsInsided(player))
					{
						if (KrateisCubeEngine.getInstance().isActive())
						{
							KrateisCubeManager.getInstance().teleportToInstance(player);
						}
						else
						{
							htmltext = npc.getId() + "-02.html";
						}
					}
					else
					{
						player.sendPacket(SystemMessageId.CANNOT_ENTER_CAUSE_DONT_MATCH_REQUIREMENTS);
						player.teleToLocation(-70381, -70937, -1428, 0, 0);
					}
					break;
				}
			}
		}
		return htmltext;
	}
	
	private int checkArena(L2PcInstance player)
	{
		int arena = 0;
		if ((player.getLevel() >= 70) && (player.getLevel() <= 75))
		{
			arena = 1;
		}
		else if ((player.getLevel() >= 76) && (player.getLevel() <= 79))
		{
			arena = 2;
		}
		else if (player.getLevel() >= 80)
		{
			arena = 3;
		}
		return arena;
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		return npc.getId() + ".html";
	}
}
