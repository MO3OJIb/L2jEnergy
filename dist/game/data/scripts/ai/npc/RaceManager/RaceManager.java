/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.npc.RaceManager;

import java.util.Locale;
import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.enums.events.EventState;
import com.l2jserver.gameserver.idfactory.IdFactory;
import com.l2jserver.gameserver.instancemanager.games.MonsterRace;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

import ai.npc.AbstractNpcAI;

/**
 * @author Мо3олЬ
 */
public class RaceManager extends AbstractNpcAI
{
	private static final int RACE_MANAGER = 30995;
	
	protected static final int TICKET_PRICES[] =
	{
		100,
		500,
		1000,
		5000,
		10000,
		20000,
		50000,
		100000
	};
	
	public RaceManager()
	{
		super(RaceManager.class.getSimpleName(), "ai/npc");
		
		addStartNpc(RACE_MANAGER);
		addFirstTalkId(RACE_MANAGER);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		String htmtext = null;
		final StringTokenizer st = new StringTokenizer(event, " ");
		switch (st.nextToken())
		{
			case "30995.html":
			case "30995-1.html":
			{
				htmtext = event;
				break;
			}
			case "BuyTicket":
			{
				var currentRaceState = MonsterRace.getInstance().getCurrentRaceState();
				if (currentRaceState != EventState.PARTICIPATING)
				{
					player.sendPacket(SystemMessageId.MONSTER_RACE_TICKETS_ARE_NO_LONGER_AVAILABLE);
					return htmtext;
				}
				
				var val = Integer.parseInt(st.nextToken());
				if (val == 0)
				{
					player.setRace(0, 0);
					player.setRace(1, 0);
				}
				else if (((val == 10) && (player.getRace(0) == 0)) || ((val == 20) && (player.getRace(0) == 0) && (player.getRace(1) == 0)))
				{
					val = 0;
				}
				
				var html = new NpcHtmlMessage();
				
				if (val < 10)
				{
					html.setHtml(getHtm(player, "30995-2.html"));
					for (var i = 0; i < 8; i++)
					{
						var n = i + 1;
						var search = "Mob" + n;
						html.replace(search, MonsterRace.getInstance().getRunnerName(i));
					}
					var search = "No1";
					if (val != 0)
					{
						html.replace(search, val);
						player.setRace(0, val);
					}
					else
					{
						html.replace(search, "");
					}
				}
				else if (val < 20)
				{
					if (player.getRace(0) == 0)
					{
						return htmtext;
					}
					
					html.setHtml(getHtm(player, "30995-3.html"));
					html.replace("0place", player.getRace(0));
					var search = "Mob1";
					var replace = MonsterRace.getInstance().getRunnerName(player.getRace(0) - 1);
					html.replace(search, replace);
					search = "0adena";
					
					if (val != 10)
					{
						html.replace(search, TICKET_PRICES[val - 11]);
						player.setRace(1, val - 10);
					}
					else
					{
						html.replace(search, "");
					}
				}
				else if (val == 20)
				{
					if ((player.getRace(0) == 0) || (player.getRace(1) == 0))
					{
						return htmtext;
					}
					
					var ticketPrice = TICKET_PRICES[player.getRace(1) - 1];
					var tax = 0;
					
					html.setHtml(getHtm(player, "30995-4.html"));
					html.replace("0place", player.getRace(0));
					html.replace("Mob1", MonsterRace.getInstance().getRunnerName(player.getRace(0) - 1));
					html.replace("0adena", ticketPrice);
					html.replace("0tax", tax);
					html.replace("0total", ticketPrice + tax);
				}
				else
				{
					if ((player.getRace(0) == 0) || (player.getRace(1) == 0))
					{
						return htmtext;
					}
					
					int raceId = player.getRace(0);
					int priceId = player.getRace(1);
					int ticketPrice = TICKET_PRICES[priceId - 1];
					
					if (!player.reduceAdena("Race", TICKET_PRICES[priceId - 1], npc, true))
					{
						return htmtext;
					}
					
					player.setRace(0, 0);
					player.setRace(1, 0);
					
					var item = new L2ItemInstance(IdFactory.getInstance().getNextId(), 4443);
					item.setCount(1);
					item.setEnchantLevel(MonsterRace.getInstance().getRaceNumber());
					item.setCustomType1(raceId);
					item.setCustomType2(ticketPrice / 100);
					
					player.addItem("Race", item, player, false);
					final SystemMessage msg = SystemMessage.getSystemMessage(SystemMessageId.ACQUIRED_S1_S2);
					msg.addInt(MonsterRace.getInstance().getRaceNumber());
					msg.addItemName(4443);
					player.sendPacket(msg);
					
					// Refresh lane bet.
					MonsterRace.getInstance().setBetOnLane(raceId, ticketPrice, true);
					return htmtext;
				}
				
				html.replace("1race", MonsterRace.getInstance().getRaceNumber());
				player.sendPacket(html);
				break;
			}
			case "showOdds":
			{
				var currentRaceState = MonsterRace.getInstance().getCurrentRaceState();
				if (currentRaceState == EventState.PARTICIPATING)
				{
					player.sendPacket(SystemMessageId.MONSTER_RACE_PAYOUT_INFORMATION_IS_NOT_AVAILABLE_WHILE_TICKETS_ARE_BEING_SOLD);
					return htmtext;
				}
				
				var html = new NpcHtmlMessage();
				html.setHtml(getHtm(player, "30995-5.html"));
				
				for (int i = 0; i < 8; i++)
				{
					var n = i + 1;
					var search = "Mob" + n;
					var search1 = "Odd" + n;
					html.replace(search, MonsterRace.getInstance().getRunnerName(i));
					// Odd
					var odd = MonsterRace.getInstance().getOdds().get(i);
					html.setHtml(html.getHtml().replace(search1, (odd > 0D) ? String.format(Locale.ENGLISH, "%.1f", odd) : "&$804;"));
				}
				html.replace("1race", MonsterRace.getInstance().getRaceNumber());
				player.sendPacket(html);
				break;
			}
			case "showInfo":
			{
				var html = new NpcHtmlMessage();
				html.setHtml(getHtm(player, "30995-6.html"));
				for (var i = 0; i <= 8; i++)
				{
					var search = "Mob" + i;
					html.replace(search, MonsterRace.getInstance().getRunnerName(i - 1));
				}
				player.sendPacket(html);
				break;
			}
			case "showTickets":
			{
				var sb = new StringBuilder();
				
				for (var ticket : player.getInventory().getAllItemsByItemId(4443))
				{
					// Skip tickets for the current race.
					if (ticket.getEnchantLevel() == MonsterRace.getInstance().getRaceNumber())
					{
						continue;
					}
					StringUtil.append(sb, "<tr><td><a action=\"bypass -h Quest RaceManager showTicket ", ticket.getObjectId(), "\">", ticket.getEnchantLevel(), " Race</a></td><td align=center><font color=\"LEVEL\">", ticket
						.getCustomType1(), "</font> Number</td><td align=right><font color=\"LEVEL\">", ticket.getCustomType2()
							* 100, "</font> Adena</td></tr>");
				}
				
				var html = new NpcHtmlMessage();
				html.setHtml(getHtm(player, "30995-7.html"));
				html.replace("%tickets%", sb.toString());
				player.sendPacket(html);
				break;
			}
			case "showTicket":
			{
				var ticketId = Integer.parseInt(st.nextToken());
				if (ticketId == 0)
				{
					return htmtext;
				}
				
				// Find ticket in player's inventory.
				var ticket = player.getInventory().getItemByObjectId(ticketId);
				if (ticket == null)
				{
					return htmtext;
				}
				
				var raceIds = ticket.getEnchantLevel();
				var lane = ticket.getCustomType1();
				var bet = ticket.getCustomType2() * 100;
				
				// Retrieve HistoryInfo for that race.
				var info = MonsterRace.getInstance().getHistoryInfo(raceIds);
				if (info == null)
				{
					return htmtext;
				}
				
				var html = new NpcHtmlMessage();
				html.setHtml(getHtm(player, "30995-8.html"));
				html.replace("%raceId%", raceIds);
				html.replace("%lane%", lane);
				html.replace("%bet%", bet);
				html.replace("%firstLane%", info.getFirst() + 1);
				html.replace("%secondLane%", info.getSecond() + 1);
				html.replace("%odd%", lane == (info.getFirst() + 1) ? String.format(Locale.ENGLISH, "%.2f", info.getOddRate()) : lane == (info.getSecond() + 1) ? String.format(Locale.ENGLISH, "%.2f", info.getOddRate() * 0.6) : "0.01");
				html.replace("%ticketObjectId%", ticketId);
				player.sendPacket(html);
				break;
			}
			case "calculateWin":
			{
				// Extract ticket objectId.
				var ticketId = Integer.parseInt(st.nextToken());
				if (ticketId == 0)
				{
					return htmtext;
				}
				
				// Check if player has the ticket.
				var ticket = player.getInventory().getItemByObjectId(ticketId);
				if (ticket == null)
				{
					return htmtext;
				}
				
				// Extract ticket info.
				var raceIds = ticket.getEnchantLevel();
				var lane = ticket.getCustomType1();
				var bet = ticket.getCustomType2() * 100;
				
				// Get race history.
				var raceHistory = MonsterRace.getInstance().getHistoryInfo(raceIds);
				if (raceHistory == null)
				{
					return htmtext;
				}
				
				// Calculate and reward winnings.
				var firstPlace = raceHistory.getFirst() + 1;
				var secondPlace = raceHistory.getSecond() + 1;
				var oddRate = (lane == firstPlace) ? raceHistory.getOddRate() : (lane == secondPlace) ? raceHistory.getOddRate() * 0.6 : 0.10;
				var winnings = (int) (bet * oddRate);
				
				if (player.destroyItem("MonsterTrack", ticket, npc, true))
				{
					player.addAdena("MonsterTrack", winnings, npc, true);
				}
				// Notify player and exit bypass.
				return htmtext;
			}
			case "viewHistory":
			{
				var sb = new StringBuilder();
				var raceNumber = MonsterRace.getInstance().getRaceNumber();
				
				// Use whole history, pickup from 'last element' and stop at 'latest element - 7'.
				for (var info : MonsterRace.getInstance().getLastHistoryEntries())
				{
					StringUtil.append(sb, "<tr><td><font color=\"LEVEL\">", info.getRaceId(), "</font></td><td><font color=\"LEVEL\"><div style=\"text-align:center;\">", (raceNumber == info.getRaceId()) ? "Race in progress.." : info.getWinner(), "</div></font> </td><td align=right><font color=00ffff>", String
						.format(Locale.ENGLISH, "%.2f", info.getOddRate()), "</font></td></tr>");
				}
				
				var html = new NpcHtmlMessage(npc.getObjectId());
				html.setHtml(getHtm(player, "30995-9.html"));
				html.replace("%infos%", sb.toString());
				player.sendPacket(html);
				break;
			}
		}
		return htmtext;
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		return npc.getId() + ".html";
	}
}
