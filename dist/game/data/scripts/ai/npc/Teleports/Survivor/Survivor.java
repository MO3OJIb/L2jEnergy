/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.npc.Teleports.Survivor;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.instancemanager.GraciaSeedsManager;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.itemcontainer.Inventory;

import ai.npc.AbstractNpcAI;

/**
 * Gracia Survivor teleport AI
 * @author Plim
 */
public final class Survivor extends AbstractNpcAI
{
	// NPC
	private static final int SURVIVOR = 32632;
	// Misc
	private static final int MIN_LEVEL = 75;
	// Location
	private static final Location TELEPORT = new Location(-149406, 255247, -80);
	
	public Survivor()
	{
		super(Survivor.class.getSimpleName(), "ai/npc/Teleports");
		
		addFirstTalkId(SURVIVOR);
		addStartNpc(SURVIVOR);
		addTalkId(SURVIVOR);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		if (event.equalsIgnoreCase("ask_status"))
		{
			String htmltext = getHtm(player, "32632-02.html");
			String destructionStatus = "";
			String infinityStatus = "";
			
			// TODO: Implement Seed of Infinity & stages
			switch (GraciaSeedsManager.getInstance().getSoIState())
			{
				case 1:
					infinityStatus = MessagesData.getInstance().getMessage(player, "ai_survivor_under_enemy_occupation_military_forces_adventurers_clan_members_unleashing_onslaught");
				case 2:
					infinityStatus = MessagesData.getInstance().getMessage(player, "ai_survivor_under_enemy_occupation_situation_currently_favorable_infiltration_route_heart");
				case 3:
					infinityStatus = MessagesData.getInstance().getMessage(player, "ai_survivor_our_forces_have_occupied_currently_investigating_depths");
				case 4:
					infinityStatus = MessagesData.getInstance().getMessage(player, "ai_survivor_under_occupation_forces_enemy_resurrected_attacking_toward");
				case 5:
					infinityStatus = MessagesData.getInstance().getMessage(player, "ai_survivor_under_occupation_forces_enemy_already_overtaken");
			}
			
			switch (GraciaSeedsManager.getInstance().getSoDState())
			{
				case 1:
					destructionStatus = MessagesData.getInstance().getMessage(player, "ai_survivor_currently_occupied_enemy_troops_attacking");
				case 2:
					destructionStatus = MessagesData.getInstance().getMessage(player, "ai_survivor_under_occupation_forces_heard_kucereus");
				default:
					destructionStatus = MessagesData.getInstance().getMessage(player, "ai_survivor_although_currently_have_control_enemy_pushing_back_powerful_attack");
			}
			htmltext = htmltext.replaceAll("%stat_dest%", destructionStatus);
			htmltext = htmltext.replaceAll("%stat_unde%", infinityStatus);
			return htmltext;
		}
		
		if (event.equalsIgnoreCase("ask_help"))
		{
			return "32632-01.html";
		}
		
		if (event.equalsIgnoreCase("teleport"))
		{
			if (player.getLevel() < MIN_LEVEL)
			{
				event = "32632-04.html";
			}
			else if (player.getAdena() < 150000)
			{
				return "32632-03.html";
			}
			else
			{
				takeItems(player, Inventory.ADENA_ID, 150000);
				player.teleToLocation(TELEPORT);
				return null;
			}
		}
		return event;
		
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		return npc.getId() + ".html";
	}
}
