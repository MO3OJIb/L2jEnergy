/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.npc.Trainer;

import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

import ai.npc.AbstractNpcAI;

/**
 * Trainer AI.
 * @author Мо3олЬ
 */
public class Trainer extends AbstractNpcAI
{
	// NPC
	// @formatter:off
	private static final int[] TRAINERS =
	{
		30010, 30014, 30027, 30028, 30029, 30033, 30034, 30035, 30064, 30065,
		30069, 30105, 30106, 30107, 30108, 30110, 30111, 30112, 30113, 30114,
		30143, 30155, 30156, 30157, 30158, 30171, 30184, 30185, 30186, 30189,
		30190, 30192, 30298, 30300, 30317, 30325, 30326, 30327, 30328, 30329, 
		30344, 30345, 30360, 30369, 30374, 30376, 30378, 30458, 30459, 30460, 
		30461, 30463, 30471, 30472, 30475, 30501, 30502, 30506, 30507, 30509, 
		30510, 30514, 30515, 30526, 30527, 30569, 30570, 30571, 30572, 30678, 
		30679, 30682, 30683, 30688, 30690, 30691, 30692, 30693, 30695, 30696, 
		30697, 30698, 30700, 30705, 30706, 30715, 30717, 30718, 30833, 30835, 
		30846, 30850, 30851, 30852, 30853, 30855, 30856, 30863, 30866, 30867, 
		30898, 30901, 30902, 30903, 30904, 30907, 30909, 30911, 30914, 30915, 
        31271, 31277, 31278, 31282, 31283, 31286, 31289, 31290, 31316, 31322, 
        31323, 31325, 31327, 31332, 31333, 31337, 31580, 31581, 31582, 31583, 
        31960, 31966, 31967, 31971, 31972, 31975, 31978, 31979, 31990, 32141, 
        32142, 32143, 32144, 32148, 32149, 32151, 32152, 32156, 32159, 32161, 
        32182, 32183, 32194, 32195, 32197, 32198, 32200, 32201, 32203, 32204, 
        32207, 32208, 32211, 32212, 32215, 32216, 32219, 32220, 32223, 32224, 
        32227, 32228, 32231, 32232
	};
	// @formatter:on
	
	public Trainer()
	{
		super(Trainer.class.getSimpleName(), "ai/npc");
		addStartNpc(TRAINERS);
		addTalkId(TRAINERS);
		addFirstTalkId(TRAINERS);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		String htmltext = null;
		switch (event)
		{
			case "info":
			{
				htmltext = npc.getId() + "-1.html";
				break;
			}
			case "about":
			{
				htmltext = npc.getId() + "-2.html";
				break;
			}
			case "seal":
			{
				htmltext = npc.getId() + "-seal.html";
				break;
			}
			case "augment":
			{
				htmltext = npc.getId() + "-augment.html";
				break;
			}
			case "back":
			{
				htmltext = npc.getId() + ".html";
				break;
			}
		}
		return htmltext;
	}
}
