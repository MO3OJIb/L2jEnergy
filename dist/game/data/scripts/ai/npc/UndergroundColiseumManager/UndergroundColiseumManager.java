/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package ai.npc.UndergroundColiseumManager;

import java.util.StringTokenizer;

import com.l2jserver.gameserver.configuration.config.events.UCConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.instancemanager.games.UndergroundColiseum;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.coliseum.UCTeam;
import com.l2jserver.gameserver.model.coliseum.UCWaiting;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

import ai.npc.AbstractNpcAI;

/**
 * UndergroundColiseumManager AI
 * @author Мо3олЬ
 */
public class UndergroundColiseumManager extends AbstractNpcAI
{
	// @formatter:off
	private static final int[] UNDERGROUND_COLISEUM_MANAGER =
	{
		32513, 32514,32515, 32516, 32377 
	};
	// @formatter:on
	
	public UndergroundColiseumManager()
	{
		super(UndergroundColiseumManager.class.getSimpleName(), "ai/npc");
		
		addStartNpc(UNDERGROUND_COLISEUM_MANAGER);
		addFirstTalkId(UNDERGROUND_COLISEUM_MANAGER);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		String htmtext = null;
		final StringTokenizer st = new StringTokenizer(event, " ");
		switch (st.nextToken())
		{
			case "32513.html":
			case "32514.html":
			case "32515.html":
			case "32516.html":
			case "32377.html":
			{
				htmtext = event;
				break;
			}
			case "info":
			{
				htmtext = npc.getId() + "-3.html";
				break;
			}
			case "register":
			{
				try
				{
					if (!player.isInParty())
					{
						htmtext = npc.getId() + "-1.html";
						return htmtext;
					}
					
					var playerParty = player.getParty();
					if (playerParty == null)
					{
						return htmtext;
					}
					
					if (!UndergroundColiseum.getInstance().isStarted())
					{
						player.sendMessage(MessagesData.getInstance().getMessage(player, "ai_no_registration_period"));
						return htmtext;
					}
					else if (!player.getParty().isLeader(player))
					{
						player.sendMessage(MessagesData.getInstance().getMessage(player, "ai_you_not_the_party_leader"));
						return htmtext;
					}
					else if (player.getParty().getUCState() instanceof UCWaiting)
					{
						player.sendMessage(MessagesData.getInstance().getMessage(player, "ai_you_already_registered_fight"));
						return htmtext;
					}
					
					var val = Integer.parseInt(st.nextToken());
					var arena = UndergroundColiseum.getInstance().getArena(val);
					if (arena == null)
					{
						player.sendMessage(MessagesData.getInstance().getMessage(player, "ai_this_arena_temporarly_unavailable"));
						return htmtext;
					}
					
					var realCount = 0;
					
					for (var member : player.getParty().getMembers())
					{
						if (member == null)
						{
							continue;
						}
						
						if (!((member.getLevel() >= arena.getMinLevel()) && (member.getLevel() <= arena.getMaxLevel())))
						{
							player.sendMessage(MessagesData.getInstance().getMessage(player, "ai_the_level_of_party_member_is_outside_the_level_category").replace("%i%", member.getName() + ""));
							return htmtext;
						}
						realCount++;
					}
					
					if (realCount < UCConfig.UC_PARTY_LIMIT)
					{
						player.sendMessage(MessagesData.getInstance().getMessage(player, "ai_only_parties_with_more_than_members_can_battle_arena"));
					}
					
					if (arena.getWaitingList().size() >= 5)
					{
						player.sendMessage(MessagesData.getInstance().getMessage(player, "ai_no_place_register"));
						return htmtext;
					}
					
					var waiting = new UCWaiting(player.getParty(), arena);
					arena.getWaitingList().add(waiting);
					waiting.hasRegisterdNow();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				break;
			}
			case "cancel":
			{
				if ((player.getParty() == null) || ((player.getParty() != null) && !player.getParty().isLeader(player)))
				{
					return htmtext;
				}
				
				if (player.getParty().getUCState() instanceof UCWaiting)
				{
					var waiting = (UCWaiting) player.getParty().getUCState();
					if (waiting != null)
					{
						waiting.setParty(null);
						waiting.clean();
						var baseArena = waiting.getBaseArena();
						if (baseArena != null)
						{
							baseArena.getWaitingList().remove(waiting);
						}
						player.sendMessage(MessagesData.getInstance().getMessage(player, "ai_you_cancelled_registration"));
						return htmtext;
					}
				}
				break;
			}
			case "bestTeam":
			{
				var val = Integer.parseInt(st.nextToken());
				var arena = UndergroundColiseum.getInstance().getArena(val);
				var top = 0;
				var name = "" + MessagesData.getInstance().getMessage(player, "html_none") + "";
				for (UCTeam team : arena.getTeams())
				{
					if (team == null)
					{
						continue;
					}
					
					if (top < team.getConsecutiveWins())
					{
						top = team.getConsecutiveWins();
						name = team.getParty().getLeader().getName();
					}
				}
				if (top > 1)
				{
					var html = new NpcHtmlMessage(npc.getObjectId());
					html.setHtml(getHtm(player, npc.getId() + "-5.html"));
					html.replace("%name%", name);
					html.replace("%best%", String.valueOf(top));
					player.sendPacket(html);
				}
				else
				{
					var html = new NpcHtmlMessage(npc.getObjectId());
					html.setHtml(getHtm(player, npc.getId() + "-4.html"));
					player.sendPacket(html);
				}
				break;
			}
			case "listTeams":
			{
				var val = Integer.parseInt(st.nextToken());
				
				var arena = UndergroundColiseum.getInstance().getArena(val);
				if (arena == null)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "ai_this_arena_temporarly_unavailable"));
					return htmtext;
				}
				
				var html = new NpcHtmlMessage(npc.getObjectId());
				html.setHtml(getHtm(player, npc.getId() + "-2.html"));
				
				var list = "";
				int i = 1;
				for (var point : arena.getPoints())
				{
					if (point.getParty() == null)
					{
						list += i + ". " + MessagesData.getInstance().getMessage(player, "html_none") + " <br>";
					}
					else
					{
						var teamList = "";
						for (var m : point.getParty().getMembers())
						{
							if (m != null)
							{
								teamList += m.getName() + ";";
							}
						}
						
						list += i + ". (" + MessagesData.getInstance().getMessage(player, "html_participating_team") + " <font color=00ffff>" + teamList + "</font>)<br>";
					}
					i++;
				}
				
				html.replace("%list%", list);
				player.sendPacket(html);
				break;
			}
		}
		return htmtext;
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		return npc.getId() + ".html";
	}
}