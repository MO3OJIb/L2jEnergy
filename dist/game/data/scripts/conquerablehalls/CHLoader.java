/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package conquerablehalls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import conquerablehalls.DevastatedCastle.DevastatedCastle;
import conquerablehalls.FortressOfResistance.FortressOfResistance;
import conquerablehalls.FortressOfTheDead.FortressOfTheDead;
import conquerablehalls.RainbowSpringsChateau.RainbowSpringsChateau;
import conquerablehalls.flagwar.BanditStronghold.BanditStronghold;
import conquerablehalls.flagwar.WildBeastReserve.WildBeastReserve;

/**
 * @author Мо3олЬ
 */
public class CHLoader
{
	private static final Logger LOG = LoggerFactory.getLogger(CHLoader.class);
	
	private static final Class<?>[] SCRIPTS =
	{
		DevastatedCastle.class,
		BanditStronghold.class,
		WildBeastReserve.class,
		FortressOfResistance.class,
		FortressOfTheDead.class,
		RainbowSpringsChateau.class
	};
	
	public static void main(String[] args)
	{
		var n = 0;
		for (var ai : SCRIPTS)
		{
			try
			{
				ai.getDeclaredConstructor().newInstance();
				n++;
			}
			catch (Exception ex)
			{
				LOG.error("Error loading {}!", ai.getSimpleName(), ex);
			}
		}
		LOG.info("Loaded {} Conquerable Clan Halls.", n);
	}
}
