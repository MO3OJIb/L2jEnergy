/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package conquerablehalls.FortressOfTheDead;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.l2jserver.gameserver.GameTimeController;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.model.L2Clan;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.clanhall.ClanHallSiegeEngine;
import com.l2jserver.gameserver.network.NpcStringId;

/**
 * Fortress of the Dead clan hall siege script.
 * @author BiggBoss
 */
public final class FortressOfTheDead extends ClanHallSiegeEngine
{
	private static final int LIDIA = 35629;
	private static final int ALFRED = 35630;
	private static final int GISELLE = 35631;
	
	private final Map<Integer, Integer> _damageToLidia = new ConcurrentHashMap<>();
	
	public FortressOfTheDead()
	{
		super(FortressOfTheDead.class.getSimpleName(), "conquerablehalls", FORTRESS_OF_DEAD);
		addAttackId(LIDIA);
		
		addKillId(LIDIA, ALFRED, GISELLE);
		addSpawnId(LIDIA, ALFRED, GISELLE);;
	}
	
	@Override
	public String onSpawn(L2Npc npc)
	{
		if (npc.getId() == LIDIA)
		{
			broadcastNpcSay(npc, ChatType.NPC_SHOUT, NpcStringId.HMM_THOSE_WHO_ARE_NOT_OF_THE_BLOODLINE_ARE_COMING_THIS_WAY_TO_TAKE_OVER_THE_CASTLE_HUMPH_THE_BITTER_GRUDGES_OF_THE_DEAD_YOU_MUST_NOT_MAKE_LIGHT_OF_THEIR_POWER);
		}
		else if (npc.getId() == ALFRED)
		{
			broadcastNpcSay(npc, ChatType.NPC_SHOUT, NpcStringId.HEH_HEH_I_SEE_THAT_THE_FEAST_HAS_BEGUN_BE_WARY_THE_CURSE_OF_THE_HELLMANN_FAMILY_HAS_POISONED_THIS_LAND);
		}
		else if (npc.getId() == GISELLE)
		{
			broadcastNpcSay(npc, ChatType.NPC_SHOUT, NpcStringId.ARISE_MY_FAITHFUL_SERVANTS_YOU_MY_PEOPLE_WHO_HAVE_INHERITED_THE_BLOOD_IT_IS_THE_CALLING_OF_MY_DAUGHTER_THE_FEAST_OF_BLOOD_WILL_NOW_BEGIN);
		}
		return null;
	}
	
	@Override
	public String onAttack(L2Npc npc, L2PcInstance attacker, int damage, boolean isSummon)
	{
		if (!_hall.isInSiege())
		{
			return null;
		}
		
		var clan = attacker.getActingPlayer().getClan();
		if ((clan != null) && checkIsAttacker(clan))
		{
			_damageToLidia.merge(clan.getId(), damage, Integer::sum);
		}
		return null;
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance killer, boolean isSummon)
	{
		if (!_hall.isInSiege())
		{
			return null;
		}
		
		var npcId = npc.getId();
		
		if ((npcId == ALFRED) || (npcId == GISELLE))
		{
			broadcastNpcSay(npc, ChatType.NPC_SHOUT, NpcStringId.AARGH_IF_I_DIE_THEN_THE_MAGIC_FORCE_FIELD_OF_BLOOD_WILL);
		}
		else if (npcId == LIDIA)
		{
			broadcastNpcSay(npc, ChatType.NPC_SHOUT, NpcStringId.GRARR_FOR_THE_NEXT_2_MINUTES_OR_SO_THE_GAME_ARENA_ARE_WILL_BE_CLEANED_THROW_ANY_ITEMS_YOU_DONT_NEED_TO_THE_FLOOR_NOW);
			_missionAccomplished = true;
			
			cancelSiegeTask();
			endSiege();
		}
		return null;
	}
	
	@Override
	public L2Clan getWinner()
	{
		// If none did damages, simply return null.
		if (_damageToLidia.isEmpty())
		{
			return null;
		}
		
		// Retrieve clanId who did the biggest amount of damage.
		var clanId = Collections.max(_damageToLidia.entrySet(), Map.Entry.comparingByValue()).getKey();
		
		// Clear the Map for future usage.
		_damageToLidia.clear();
		
		// Return the Clan winner.
		return ClanTable.getInstance().getClan(clanId);
	}
	
	@Override
	public void startSiege()
	{
		// Siege must start at night
		var gameTimeController = GameTimeController.getInstance();
		var hoursLeft = gameTimeController.getGameHour();
		
		if ((hoursLeft < 18) || (hoursLeft >= 6))
		{
			cancelSiegeTask();
			// Calculate time until the next night (in minutes)
			var minutesUntilNight = (hoursLeft < 6) ? ((6 - hoursLeft) * 60) - gameTimeController.getGameMinute() : (((24 - hoursLeft) + 6) * 60) - gameTimeController.getGameMinute();
			// Convert minutes to milliseconds for scheduling the task
			var scheduleTime = minutesUntilNight * 60 * 1000;
			_siegeTask = ThreadPoolManager.getInstance().scheduleGeneral(() -> startSiege(), scheduleTime);
		}
		else
		{
			super.startSiege();
		}
	}
}
