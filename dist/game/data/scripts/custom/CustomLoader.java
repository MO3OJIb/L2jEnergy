/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package custom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import custom.PcPoint.PcPointDropMobs;
import custom.Validators.SubClassSkills;
import features.SkillTransfer.SkillTransfer;

/**
 * @author Мо3олЬ
 */
public class CustomLoader
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomLoader.class);
	
	private static final Class<?>[] SCRIPTS =
	{
		PcPointDropMobs.class, // PcCafeConfig.ALT_PCBANG_POINTS_MOB_ENABLED
		SkillTransfer.class,
		SubClassSkills.class
	};
	
	public static void main(String[] args)
	{
		var n = 0;
		for (var script : SCRIPTS)
		{
			try
			{
				script.getDeclaredConstructor().newInstance();
				n++;
			}
			catch (Exception ex)
			{
				LOG.error("Failed loading {}!", script.getSimpleName(), ex);
			}
		}
		LOG.info("Loaded {} Custom Scripts.", n);
	}
}
