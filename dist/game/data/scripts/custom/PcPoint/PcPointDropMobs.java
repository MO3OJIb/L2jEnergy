/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package custom.PcPoint;

import java.util.Arrays;

import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.configuration.config.events.PcCafeConfig;
import com.l2jserver.gameserver.data.xml.impl.NpcData;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.Quest;

public class PcPointDropMobs extends Quest
{
	public PcPointDropMobs()
	{
		super(-1, PcPointDropMobs.class.getSimpleName(), "custom");
		for (var npc : NpcData.getInstance().getTemplates(npc -> Arrays.binarySearch(PcCafeConfig.ALT_PCBANG_POINTS_MOB_ID, npc.getId()) >= 0))
		{
			addKillId(npc.getId());
		}
	}
	
	@Override
	public final String onKill(L2Npc npc, L2PcInstance player, boolean isSummon)
	{
		if ((player.getLevel() >= 75) && (npc.getLevel() < 60))
		{
			return "";
		}
		
		player.getPcCafeSystem().addPoints(PcCafeConfig.ALT_PCBANG_POINTS_PER_MOB, (PcCafeConfig.ALT_PCBANG_POINTS_BONUS_DOUBLE_CHANCE > 0) && (Rnd.get(100) < PcCafeConfig.ALT_PCBANG_POINTS_BONUS_DOUBLE_CHANCE));
		return super.onKill(npc, player, isSummon);
	}
}