/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import custom.events.Elpies.Elpies;
import custom.events.Rabbits.Rabbits;
import custom.events.Race.Race;
import custom.events.Wedding.Wedding;
import events.AngelCat.AngelCat;
import events.CatchATiger.CatchATiger;
import events.CharacterBirthday.CharacterBirthday;
import events.CofferOfShadows.CofferOfShadows;
import events.FreyaCelebration.FreyaCelebration;
import events.GiftOfVitality.GiftOfVitality;
import events.HeavyMedal.HeavyMedal;
import events.LoveYourGatekeeper.LoveYourGatekeeper;
import events.LoversJubilee.LoversJubilee;
import events.MasterOfEnchanting.MasterOfEnchanting;
import events.PlayingWithFire.PlayingWithFire;
import events.SavingSanta.SavingSanta;
import events.TheFallHarvest.TheFallHarvest;
import events.TheValentineEvent.TheValentineEvent;

/**
 * @author Мо3олЬ
 */
public class EventsLoader
{
	private static final Logger LOG = LoggerFactory.getLogger(EventsLoader.class);
	
	private static final Class<?>[] SCRIPTS =
	{
		AngelCat.class,
		CatchATiger.class,
		CharacterBirthday.class,
		CofferOfShadows.class,
		FreyaCelebration.class,
		GiftOfVitality.class,
		// Halloween.class,
		HeavyMedal.class,
		LoversJubilee.class,
		LoveYourGatekeeper.class,
		MasterOfEnchanting.class,
		PlayingWithFire.class,
		SavingSanta.class,
		TheFallHarvest.class,
		TheValentineEvent.class,
		
		// Custom Events
		Elpies.class,
		Rabbits.class,
		Race.class,
		Wedding.class
	};
	
	public static void main(String[] args)
	{
		var n = 0;
		for (var script : SCRIPTS)
		{
			try
			{
				script.getDeclaredConstructor().newInstance();
				n++;
			}
			catch (Exception ex)
			{
				LOG.error("Failed loading {}!", script.getSimpleName(), ex);
			}
		}
		LOG.info("Loaded {} Events.", n);
	}
}
