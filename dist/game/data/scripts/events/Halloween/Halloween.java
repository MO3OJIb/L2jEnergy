/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package events.Halloween;

import com.l2jserver.gameserver.ai.CtrlIntention;
import com.l2jserver.gameserver.data.xml.impl.MultisellData;
import com.l2jserver.gameserver.enums.events.EventType;
import com.l2jserver.gameserver.enums.events.ListenerRegisterType;
import com.l2jserver.gameserver.model.actor.L2Attackable;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.event.LongTimeEvent;
import com.l2jserver.gameserver.model.events.annotations.RegisterEvent;
import com.l2jserver.gameserver.model.events.annotations.RegisterType;
import com.l2jserver.gameserver.model.events.impl.character.player.OnPlayerLogin;
import com.l2jserver.gameserver.network.serverpackets.ExBrBroadcastEventState;

/**
 * Halloween event AI.
 * @author U3Games, Sacrifice
 */
public final class Halloween extends LongTimeEvent
{
	// NPC
	private static final int HALLOWEN_NPC = 125;
	// Monster
	private static final int PUMPKIN_GHOST = 13135;
	// Item
	private static final int HALLOWEEN_CANDY = 15430;
	
	//@formatter:off
	private static final int[] FOREST_OF_DEATH_NIGHT =
	{
		18119, 21547, 21553, 21557, 21559, 21561, 21563,
		21565, 21567, 21570, 21572, 21574, 21578, 21580,
		21581, 21583, 21587, 21590, 21593, 21596, 21599
	};
	
	private static final int[] THE_CEMENTARY =
	{
		20666, 20668, 20669, 20678,
		20997, 20998, 20999, 21000
	};
	
	private static final int[] IMPERIAL_TOMB =
	{
		21396, 21397, 21398, 21399, 21400, 21401, 21402,
		21403, 21404, 21405, 21406, 21407, 21408, 21409,
		21410, 21411, 21412, 21413, 21414, 21415, 21416,
		21417, 21418, 21419, 21420, 21421, 21422, 21423,
		21424, 21425, 21426, 21427, 21428, 21429, 21430,
		21431
	};
	//@formatter:on
	
	public Halloween()
	{
		super(Halloween.class.getSimpleName(), "events");
		addStartNpc(HALLOWEN_NPC);
		addFirstTalkId(HALLOWEN_NPC);
		addTalkId(HALLOWEN_NPC);
		addKillId(PUMPKIN_GHOST);
		
		for (var id : FOREST_OF_DEATH_NIGHT)
		{
			addKillId(id);
		}
		
		for (var id : THE_CEMENTARY)
		{
			addKillId(id);
		}
		
		for (var id : IMPERIAL_TOMB)
		{
			addKillId(id);
		}
	}
	
	@RegisterEvent(EventType.ON_PLAYER_LOGIN)
	@RegisterType(ListenerRegisterType.GLOBAL_PLAYERS)
	public void onPlayerLogin(OnPlayerLogin event)
	{
		if (isEventPeriod())
		{
			event.getActiveChar().sendPacket(new ExBrBroadcastEventState(ExBrBroadcastEventState.HALLOWEEN_EVENT, 1));
		}
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		var st = player.getQuestState(getName());
		
		if (st == null)
		{
			st = newQuestState(player);
		}
		
		var htmltext = event;
		switch (event)
		{
			case "main":
			{
				htmltext = "125.htm";
				break;
			}
			case "info":
			{
				htmltext = "125-info.htm";
				break;
			}
			case "rewards":
			{
				MultisellData.getInstance().separateAndSend(125, player, npc, false);
				break;
			}
		}
		return htmltext;
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		if (player.getQuestState(getName()) == null)
		{
			newQuestState(player);
		}
		return "125.htm";
	}
	
	@Override
	public final String onKill(L2Npc npc, L2PcInstance player, boolean isPet)
	{
		var st = player.getQuestState(getName());
		
		if (st == null)
		{
			st = newQuestState(player);
		}
		
		for (var Id : FOREST_OF_DEATH_NIGHT)
		{
			if (npc.getId() == Id)
			{
				spawnGhost(npc, player);
			}
		}
		
		for (var Id : THE_CEMENTARY)
		{
			if (npc.getId() == Id)
			{
				spawnGhost(npc, player);
			}
		}
		
		for (var Id : IMPERIAL_TOMB)
		{
			if (npc.getId() == Id)
			{
				spawnGhost(npc, player);
			}
		}
		
		if (npc.getId() == PUMPKIN_GHOST)
		{
			npc.dropItem(player, HALLOWEEN_CANDY, 3);
		}
		return super.onKill(npc, player, isPet);
	}
	
	private void spawnGhost(L2Npc npc, L2PcInstance player)
	{
		var st = player.getQuestState(getName());
		
		if (st == null)
		{
			st = newQuestState(player);
		}
		
		var pumpkinGhost = (L2Attackable) st.addSpawn(PUMPKIN_GHOST, 60000);
		if (pumpkinGhost != null)
		{
			var isPet = false;
			
			if (player.getSummon() != null)
			{
				isPet = true;
			}
			var originalAttacker = isPet ? player.getSummon() : player;
			pumpkinGhost.setRunning();
			pumpkinGhost.addDamageHate(originalAttacker, 0, 500);
			pumpkinGhost.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, originalAttacker);
		}
	}
}
