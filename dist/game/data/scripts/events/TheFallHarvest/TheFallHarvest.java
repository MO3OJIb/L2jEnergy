/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package events.TheFallHarvest;

import com.l2jserver.gameserver.data.xml.impl.MultisellData;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.event.LongTimeEvent;

/**
 * The Fall Harvest event AI.
 */
public class TheFallHarvest extends LongTimeEvent
{
	// NPC
	private static final int BUZZ_THE_CAT = 31255;
	
	public TheFallHarvest()
	{
		super(TheFallHarvest.class.getSimpleName(), "events");
		
		addStartNpc(BUZZ_THE_CAT);
		addFirstTalkId(BUZZ_THE_CAT);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		var st = player.getQuestState(getName());
		
		if (st == null)
		{
			st = newQuestState(player);
		}
		
		var htmltext = event;
		switch (event)
		{
			case "main":
			{
				htmltext = "31255.htm";
				break;
			}
			case "info":
			{
				htmltext = "31255-01.htm";
				break;
			}
			case "buy":
			{
				MultisellData.getInstance().separateAndSend(31255, player, npc, false);
				break;
			}
		}
		return htmltext;
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance player)
	{
		if (player.getQuestState(getName()) == null)
		{
			newQuestState(player);
		}
		return "31255.htm";
	}
}
