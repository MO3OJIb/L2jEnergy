/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.handler.EffectHandler;
import com.l2jserver.gameserver.model.effects.AbstractEffect;

import handlers.effecthandlers.consume.ConsumeAgathionEnergy;
import handlers.effecthandlers.consume.ConsumeChameleonRest;
import handlers.effecthandlers.consume.ConsumeFakeDeath;
import handlers.effecthandlers.consume.ConsumeHp;
import handlers.effecthandlers.consume.ConsumeMp;
import handlers.effecthandlers.consume.ConsumeMpByLevel;
import handlers.effecthandlers.consume.ConsumeRest;
import handlers.effecthandlers.custom.BlockAction;
import handlers.effecthandlers.custom.Buff;
import handlers.effecthandlers.custom.Debuff;
import handlers.effecthandlers.custom.Detection;
import handlers.effecthandlers.custom.Flag;
import handlers.effecthandlers.custom.Grow;
import handlers.effecthandlers.custom.Mute;
import handlers.effecthandlers.custom.OpenChest;
import handlers.effecthandlers.custom.OpenDoor;
import handlers.effecthandlers.custom.Recovery;
import handlers.effecthandlers.custom.Root;
import handlers.effecthandlers.custom.SilentMove;
import handlers.effecthandlers.custom.Sleep;
import handlers.effecthandlers.custom.Stun;
import handlers.effecthandlers.custom.ThrowUp;
import handlers.effecthandlers.instant.*;
import handlers.effecthandlers.pump.AttackTrait;
import handlers.effecthandlers.pump.Betray;
import handlers.effecthandlers.pump.BlockBuff;
import handlers.effecthandlers.pump.BlockBuffSlot;
import handlers.effecthandlers.pump.BlockChat;
import handlers.effecthandlers.pump.BlockDamage;
import handlers.effecthandlers.pump.BlockDebuff;
import handlers.effecthandlers.pump.BlockParty;
import handlers.effecthandlers.pump.BlockResurrection;
import handlers.effecthandlers.pump.BonusTimeLimitUp;
import handlers.effecthandlers.pump.ChangeFishingMastery;
import handlers.effecthandlers.pump.CrystalGradeModify;
import handlers.effecthandlers.pump.CubicMastery;
import handlers.effecthandlers.pump.DefenceTrait;
import handlers.effecthandlers.pump.Disarm;
import handlers.effecthandlers.pump.EnableCloak;
import handlers.effecthandlers.pump.Fear;
import handlers.effecthandlers.pump.Hide;
import handlers.effecthandlers.pump.Lucky;
import handlers.effecthandlers.pump.MaxCp;
import handlers.effecthandlers.pump.MaxHp;
import handlers.effecthandlers.pump.MaxMp;
import handlers.effecthandlers.pump.NoblesseBless;
import handlers.effecthandlers.pump.Passive;
import handlers.effecthandlers.pump.PhysicalAttackMute;
import handlers.effecthandlers.pump.PhysicalMute;
import handlers.effecthandlers.pump.ProtectionBlessing;
import handlers.effecthandlers.pump.ResistSkill;
import handlers.effecthandlers.pump.ResurrectionSpecial;
import handlers.effecthandlers.pump.ServitorShare;
import handlers.effecthandlers.pump.SingleTarget;
import handlers.effecthandlers.pump.SoulEating;
import handlers.effecthandlers.pump.TalismanSlot;
import handlers.effecthandlers.pump.TargetMe;
import handlers.effecthandlers.pump.TransferDamage;
import handlers.effecthandlers.pump.TransformHangover;
import handlers.effecthandlers.pump.Transformation;
import handlers.effecthandlers.pump.TriggerSkillByAttack;
import handlers.effecthandlers.pump.TriggerSkillByAvoid;
import handlers.effecthandlers.pump.TriggerSkillByDamage;
import handlers.effecthandlers.pump.TriggerSkillBySkill;
import handlers.effecthandlers.ticks.TickHp;
import handlers.effecthandlers.ticks.TickHpFatal;
import handlers.effecthandlers.ticks.TickMp;

/**
 * Effect Master handler.
 * @author BiggBoss
 * @author Zoey76
 */
public final class EffectMasterHandler
{
	private static final Logger LOG = LoggerFactory.getLogger(EffectMasterHandler.class);
	
	private static final Class<?>[] EFFECTS =
	{
		AddHate.class,
		AttackTrait.class,
		Backstab.class,
		Betray.class,
		Blink.class,
		BlockAction.class,
		BlockBuff.class,
		BlockBuffSlot.class,
		BlockChat.class,
		BlockDamage.class,
		BlockDebuff.class,
		BlockParty.class,
		BlockResurrection.class,
		Bluff.class,
		Buff.class,
		BonusCountUp.class,
		BonusTimeLimitUp.class,
		CallParty.class,
		CallPc.class,
		CallSkill.class,
		ChangeFace.class,
		ChangeFishingMastery.class,
		ChangeHairColor.class,
		ChangeHairStyle.class,
		ClanGate.class,
		Confuse.class,
		ConsumeAgathionEnergy.class,
		ConsumeBody.class,
		ConsumeChameleonRest.class,
		ConsumeFakeDeath.class,
		ConsumeHp.class,
		ConsumeMp.class,
		ConsumeMpByLevel.class,
		ConsumeRest.class,
		ConvertItem.class,
		Cp.class,
		CrystalGradeModify.class,
		CubicMastery.class,
		DeathLink.class,
		Debuff.class,
		DefenceTrait.class,
		DeleteHate.class,
		DeleteHateOfMe.class,
		DetectHiddenObjects.class,
		Detection.class,
		Disarm.class,
		DispelAll.class,
		DispelByCategory.class,
		DispelBySlot.class,
		DispelBySlotProbability.class,
		EnableCloak.class,
		EnergyAttack.class,
		Escape.class,
		FatalBlow.class,
		Fear.class,
		Fishing.class,
		Flag.class,
		FlySelf.class,
		FocusEnergy.class,
		FocusMaxEnergy.class,
		FocusSouls.class,
		FoodForPet.class,
		GetAgro.class,
		GiveSp.class,
		Grow.class,
		Harvesting.class,
		HeadquarterCreate.class,
		Heal.class,
		Hide.class,
		Hp.class,
		HpDrain.class,
		HpPerMax.class,
		InstantAgathionEnergy.class,
		InstantBetray.class,
		InstantCallTargetParty.class,
		InstantDespawn.class,
		InstantDispelByName.class,
		InstantDispelBySlotMyself.class,
		InstantEnchantArmor.class,
		InstantHpByLevelSelf.class,
		InstantMagicalAttackOverHit.class,
		InstantMpByLevelSelf.class,
		Lethal.class,
		Lucky.class,
		MagicalAttack.class,
		MagicalAttackByAbnormal.class,
		MagicalAttackRange.class,
		MagicalAttackMp.class,
		MagicalSoulAttack.class,
		ManaHealByLevel.class,
		MaxCp.class,
		MaxHp.class,
		MaxMp.class,
		Mp.class,
		MpPerMax.class,
		Mute.class,
		NoblesseBless.class,
		OpenChest.class,
		OpenCommonRecipeBook.class,
		OpenDoor.class,
		OpenDwarfRecipeBook.class,
		OutpostCreate.class,
		OutpostDestroy.class,
		Passive.class,
		PcBangPointUp.class,
		PhysicalAttack.class,
		PhysicalAttackHpLink.class,
		PhysicalAttackMute.class,
		PhysicalMute.class,
		PhysicalSoulAttack.class,
		ProtectionBlessing.class,
		Pumping.class,
		RandomizeHate.class,
		RebalanceHP.class,
		Recovery.class,
		Reeling.class,
		RefuelAirship.class,
		ResistSkill.class,
		Restoration.class,
		RestorationRandom.class,
		Resurrection.class,
		ResurrectionSpecial.class,
		Root.class,
		RunAway.class,
		ServitorShare.class,
		SetSkill.class,
		SilentMove.class,
		SingleTarget.class,
		SkillTurning.class,
		Sleep.class,
		SoulBlow.class,
		SoulEating.class,
		Sow.class,
		Spoil.class,
		StaticDamage.class,
		StealAbnormal.class,
		Stun.class,
		Summon.class,
		SummonAgathion.class,
		SummonCubic.class,
		SummonNpc.class,
		SummonPet.class,
		SummonTrap.class,
		Sweeper.class,
		TakeCastle.class,
		TakeFort.class,
		TakeFortStart.class,
		TakeTerritoryFlag.class,
		TalismanSlot.class,
		TargetCancel.class,
		TargetMe.class,
		TargetMeProbability.class,
		Teleport.class,
		TeleportToTarget.class,
		ThrowUp.class,
		TickHp.class,
		TickHpFatal.class,
		TickMp.class,
		TransferDamage.class,
		TransferHate.class,
		TransformHangover.class,
		Transformation.class,
		TrapDetect.class,
		TrapRemove.class,
		TriggerSkillByAttack.class,
		TriggerSkillByAvoid.class,
		TriggerSkillByDamage.class,
		TriggerSkillBySkill.class,
		Unsummon.class,
		UnsummonAgathion.class,
		VitalityPointUp.class
	};
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args)
	{
		for (var c : EFFECTS)
		{
			EffectHandler.getInstance().registerHandler((Class<? extends AbstractEffect>) c);
		}
		LOG.info("Loaded {} effect handlers.", EffectHandler.getInstance().size());
	}
}
