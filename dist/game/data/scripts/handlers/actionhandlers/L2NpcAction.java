/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.actionhandlers;

import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.GeoData;
import com.l2jserver.gameserver.ai.CtrlIntention;
import com.l2jserver.gameserver.configuration.config.LimitsConfig;
import com.l2jserver.gameserver.enums.InstanceType;
import com.l2jserver.gameserver.enums.events.EventType;
import com.l2jserver.gameserver.handler.IActionHandler;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.eventengine.GameEventManager;
import com.l2jserver.gameserver.model.events.EventDispatcher;
import com.l2jserver.gameserver.model.events.impl.character.npc.OnNpcFirstTalk;
import com.l2jserver.gameserver.network.serverpackets.MoveToPawn;

public class L2NpcAction implements IActionHandler
{
	@Override
	public boolean action(L2PcInstance player, L2Object target, boolean interact)
	{
		var npc = (L2Npc) target;
		if (!npc.canTarget(player))
		{
			return false;
		}
		player.setLastFolkNPC(npc);
		// Check if the L2PcInstance already target the L2Npc
		if (npc != player.getTarget())
		{
			// Set the target of the L2PcInstance activeChar
			player.setTarget(npc);
			// Check if the activeChar is attackable (without a forced attack)
			if (npc.isAutoAttackable(player))
			{
				npc.getAI(); // wake up ai
			}
		}
		else if (interact)
		{
			// Check if the activeChar is attackable (without a forced attack) and isn't dead
			if (npc.isAutoAttackable(player) && !npc.isAlikeDead())
			{
				if (GeoData.getInstance().canSeeTarget(player, npc))
				{
					player.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, npc);
				}
				else
				{
					var destination = GeoData.getInstance().moveCheck(player, npc);
					player.getAI().setIntention(CtrlIntention.AI_INTENTION_MOVE_TO, destination);
				}
			}
			else if (!npc.isAutoAttackable(player))
			{
				if (!GeoData.getInstance().canSeeTarget(player, npc))
				{
					var destination = GeoData.getInstance().moveCheck(player, npc);
					player.getAI().setIntention(CtrlIntention.AI_INTENTION_MOVE_TO, destination);
					return true;
				}
				
				// Verifies if the NPC can interact with the player.
				if (!npc.canInteract(player))
				{
					// Notify the L2PcInstance AI with AI_INTENTION_INTERACT
					player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, npc);
				}
				else
				{
					// Turn NPC to the player.
					player.sendPacket(new MoveToPawn(player, npc, 100));
					if (npc.hasRandomAnimation())
					{
						npc.onRandomAnimation(Rnd.get(8));
					}
					// Open a chat window on client with the text of the L2Npc
					if (npc.isEventMob())
					{
						GameEventManager.getInstance().getEvent().showEventHtml(player, String.valueOf(npc.getObjectId()));
					}
					else
					{
						if (npc.hasListener(EventType.ON_NPC_QUEST_START))
						{
							player.setLastQuestNpcObject(npc.getObjectId());
						}
						if (npc.hasListener(EventType.ON_NPC_FIRST_TALK))
						{
							EventDispatcher.getInstance().notifyEventAsync(new OnNpcFirstTalk(npc, player), npc);
						}
						else
						{
							npc.showChatWindow(player);
						}
					}
					if ((LimitsConfig.PLAYER_MOVEMENT_BLOCK_TIME > 0) && !player.isGM())
					{
						player.updateNotMoveUntil();
					}
				}
			}
		}
		return true;
	}
	
	@Override
	public InstanceType getInstanceType()
	{
		return InstanceType.L2Npc;
	}
}