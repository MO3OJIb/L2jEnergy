/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.actionhandlers;

import com.l2jserver.gameserver.GeoData;
import com.l2jserver.gameserver.ai.CtrlIntention;
import com.l2jserver.gameserver.enums.InstanceType;
import com.l2jserver.gameserver.enums.PrivateStoreType;
import com.l2jserver.gameserver.handler.IActionHandler;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.eventengine.GameEventManager;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ActionFailed;

public class L2PcInstanceAction implements IActionHandler
{
	private static final int CURSED_WEAPON_VICTIM_MIN_LEVEL = 21;
	
	@Override
	public boolean action(L2PcInstance player, L2Object target, boolean interact)
	{
		// See description in GameEventManager.java
		if (!GameEventManager.getInstance().getEvent().onAction(player, target.getObjectId()))
		{
			return false;
		}
		
		// Check if the L2PcInstance is confused
		if (player.isOutOfControl())
		{
			return false;
		}
		
		// Aggression target lock effect
		if (player.isLockedTarget() && (player.getLockedTarget() != target))
		{
			player.sendPacket(SystemMessageId.FAILED_CHANGE_TARGET);
			return false;
		}
		
		// Check if the activeChar already target this L2PcInstance
		if (player.getTarget() != target)
		{
			// Set the target of the activeChar
			player.setTarget(target);
		}
		else if (interact)
		{
			var pl = target.getActingPlayer();
			// Check if this L2PcInstance has a Private Store
			if (pl.getPrivateStoreType() != PrivateStoreType.NONE)
			{
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, pl);
			}
			else
			{
				// Check if this L2PcInstance is autoAttackable
				if (pl.isAutoAttackable(player))
				{
					if ((pl.isCursedWeaponEquipped() && (player.getLevel() < CURSED_WEAPON_VICTIM_MIN_LEVEL)) //
						|| (player.isCursedWeaponEquipped() && (pl.getLevel() < CURSED_WEAPON_VICTIM_MIN_LEVEL)))
					{
						player.sendPacket(ActionFailed.STATIC_PACKET);
					}
					else
					{
						if (GeoData.getInstance().canSeeTarget(player, pl))
						{
							player.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, pl);
						}
						else
						{
							final Location destination = GeoData.getInstance().moveCheck(player, pl);
							player.getAI().setIntention(CtrlIntention.AI_INTENTION_MOVE_TO, destination);
						}
						player.onActionRequest();
					}
				}
				else
				{
					// This Action Failed packet avoids activeChar getting stuck when clicking three or more times
					player.sendPacket(ActionFailed.STATIC_PACKET);
					if (GeoData.getInstance().canSeeTarget(player, pl))
					{
						player.getAI().setIntention(CtrlIntention.AI_INTENTION_FOLLOW, pl);
					}
					else
					{
						final Location destination = GeoData.getInstance().moveCheck(player, pl);
						player.getAI().setIntention(CtrlIntention.AI_INTENTION_MOVE_TO, destination);
					}
				}
			}
		}
		return true;
	}
	
	@Override
	public InstanceType getInstanceType()
	{
		return InstanceType.L2PcInstance;
	}
}
