/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.gameserver.data.xml.impl.AdminData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;

public class AdminAdmin implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_admin",
		"admin_admin1",
		"admin_admin2",
		"admin_admin3",
		"admin_admin4",
		"admin_admin5",
		"admin_admin6",
		"admin_admin7",
		"admin_gmliston",
		"admin_gmlistoff",
		"admin_silence",
		"admin_diet",
		"admin_tradeoff"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.startsWith("admin_admin"))
		{
			showMainPage(player, command);
		}
		else if (command.startsWith("admin_gmliston"))
		{
			AdminData.getInstance().addGm(player, false);
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_reg_gm_list"));
			AdminHtml.showAdminHtml(player, "gm_menu.htm");
		}
		else if (command.startsWith("admin_gmlistoff"))
		{
			AdminData.getInstance().addGm(player, true);
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unreg_gm_list"));
			AdminHtml.showAdminHtml(player, "gm_menu.htm");
		}
		else if (command.startsWith("admin_silence"))
		{
			if (player.isSilenceMode()) // already in message refusal mode
			{
				player.setSilenceMode(false);
				player.sendPacket(SystemMessageId.MESSAGE_ACCEPTANCE_MODE);
			}
			else
			{
				player.setSilenceMode(true);
				player.sendPacket(SystemMessageId.MESSAGE_REFUSAL_MODE);
			}
			AdminHtml.showAdminHtml(player, "gm_menu.htm");
		}
		else if (command.startsWith("admin_diet"))
		{
			try
			{
				var st = new StringTokenizer(command);
				st.nextToken();
				if (st.nextToken().equalsIgnoreCase("on"))
				{
					player.setDietMode(true);
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_diet_on"));
				}
				else if (st.nextToken().equalsIgnoreCase("off"))
				{
					player.setDietMode(false);
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_diet_off"));
				}
			}
			catch (Exception ex)
			{
				if (player.getDietMode())
				{
					player.setDietMode(false);
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_diet_off"));
				}
				else
				{
					player.setDietMode(true);
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_diet_on"));
				}
			}
			finally
			{
				player.refreshOverloaded();
			}
			AdminHtml.showAdminHtml(player, "gm_menu.htm");
		}
		else if (command.startsWith("admin_tradeoff"))
		{
			try
			{
				var mode = command.substring(15);
				if (mode.equalsIgnoreCase("on"))
				{
					player.setTradeRefusal(true);
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_trade_on"));
				}
				else if (mode.equalsIgnoreCase("off"))
				{
					player.setTradeRefusal(false);
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_trade_off"));
				}
			}
			catch (Exception ex)
			{
				if (player.getTradeRefusal())
				{
					player.setTradeRefusal(false);
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_trade_off"));
				}
				else
				{
					player.setTradeRefusal(true);
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_trade_on"));
				}
			}
			AdminHtml.showAdminHtml(player, "gm_menu.htm");
		}
		return true;
	}
	
	private void showMainPage(L2PcInstance player, String command)
	{
		var mode = 0;
		String filename = null;
		try
		{
			mode = Integer.parseInt(command.substring(11));
		}
		catch (Exception e)
		{
		}
		switch (mode)
		{
			case 1 -> filename = "main";
			case 2 -> filename = "game";
			case 3 -> filename = "effects";
			case 4 -> filename = "develop";
			case 5 -> filename = "char";
			case 6 -> filename = "gm";
			case 7 -> filename = "events";
			default -> filename = "main";
		}
		AdminHtml.showAdminHtml(player, filename + "_menu.htm");
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}