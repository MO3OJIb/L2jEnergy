/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.AdminConfig;
import com.l2jserver.gameserver.data.sql.impl.AnnouncementsTable;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.AnnouncementType;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.announce.Announcement;
import com.l2jserver.gameserver.model.announce.AutoAnnouncement;
import com.l2jserver.gameserver.model.announce.IAnnouncement;
import com.l2jserver.gameserver.util.Broadcast;
import com.l2jserver.gameserver.util.HtmlUtil;
import com.l2jserver.gameserver.util.Util;

/**
 * Announcements commands.
 * @author UnAfraid
 */
public class AdminAnnouncements implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_announce",
		"admin_announce_crit",
		"admin_announce_screen",
		"admin_announces",
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command);
		var cmd = st.hasMoreTokens() ? st.nextToken() : "";
		switch (cmd)
		{
			case "admin_announce":
			case "admin_announce_crit":
			case "admin_announce_screen":
			{
				if (!st.hasMoreTokens())
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_text"));
					return false;
				}
				var announce = st.nextToken();
				while (st.hasMoreTokens())
				{
					announce += " " + st.nextToken();
				}
				if (cmd.equals("admin_announce_screen"))
				{
					Broadcast.toAllOnlinePlayersOnScreen(announce);
				}
				else
				{
					if (AdminConfig.GM_ANNOUNCER_NAME)
					{
						announce = announce + " [" + player.getName() + "]";
					}
					Broadcast.announceToOnlinePlayers(announce, cmd.equals("admin_announce_crit"));
				}
				AdminHtml.showAdminHtml(player, "gm_menu.htm");
				break;
			}
			case "admin_announces":
			{
				var subCmd = st.hasMoreTokens() ? st.nextToken() : "";
				switch (subCmd)
				{
					case "add":
					{
						if (!st.hasMoreTokens())
						{
							var content = HtmCache.getInstance().getHtm(player, "data/html/admin/submenu/announces/add.htm");
							Util.sendCBHtml(player, content);
							break;
						}
						var annType = st.nextToken();
						final AnnouncementType type = AnnouncementType.findByName(annType);
						// ************************************
						if (!st.hasMoreTokens())
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_add"));
							break;
						}
						var annInitDelay = st.nextToken();
						if (!StringUtil.isDigit(annInitDelay))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_add"));
							break;
						}
						var initDelay = Integer.parseInt(annInitDelay) * 1000;
						// ************************************
						if (!st.hasMoreTokens())
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_add"));
							break;
						}
						var annDelay = st.nextToken();
						if (!StringUtil.isDigit(annDelay))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_add"));
							break;
						}
						var delay = Integer.parseInt(annDelay) * 1000;
						if ((delay < (10 * 1000)) && ((type == AnnouncementType.AUTO_NORMAL) || (type == AnnouncementType.AUTO_CRITICAL)))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_delay_cannot_10_sec"));
							break;
						}
						// ************************************
						if (!st.hasMoreTokens())
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_add"));
							break;
						}
						var annRepeat = st.nextToken();
						if (!StringUtil.isDigit(annRepeat))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_add"));
							break;
						}
						var repeat = Integer.parseInt(annRepeat);
						if (repeat == 0)
						{
							repeat = -1;
						}
						// ************************************
						if (!st.hasMoreTokens())
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_add"));
							break;
						}
						var content = st.nextToken();
						while (st.hasMoreTokens())
						{
							content += " " + st.nextToken();
						}
						// ************************************
						final IAnnouncement announce;
						if ((type == AnnouncementType.AUTO_CRITICAL) || (type == AnnouncementType.AUTO_NORMAL))
						{
							announce = new AutoAnnouncement(type, content, player.getName(), initDelay, delay, repeat);
						}
						else
						{
							announce = new Announcement(type, content, player.getName());
						}
						AnnouncementsTable.getInstance().addAnnouncement(announce);
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_been_successfully_added"));
						return useAdminCommand("admin_announces list", player);
					}
					case "edit":
					{
						if (!st.hasMoreTokens())
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_edit"));
							break;
						}
						var annId = st.nextToken();
						if (!StringUtil.isDigit(annId))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_edit"));
							break;
						}
						var id = Integer.parseInt(annId);
						final IAnnouncement announce = AnnouncementsTable.getInstance().getAnnounce(id);
						if (announce == null)
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_doesnt_exists"));
							break;
						}
						if (!st.hasMoreTokens())
						{
							var html = HtmCache.getInstance().getHtm(player, "data/html/admin/submenu/announces/edit.htm");
							var announcementId = "" + announce.getId();
							var announcementType = announce.getType().name();
							var announcementInital = "0";
							var announcementDelay = "0";
							var announcementRepeat = "0";
							var announcementAuthor = announce.getAuthor();
							var announcementContent = announce.getContent();
							if (announce instanceof AutoAnnouncement)
							{
								var autoAnnounce = (AutoAnnouncement) announce;
								announcementInital = "" + (autoAnnounce.getInitial() / 1000);
								announcementDelay = "" + (autoAnnounce.getDelay() / 1000);
								announcementRepeat = "" + autoAnnounce.getRepeat();
							}
							html = html.replaceAll("%id%", announcementId);
							html = html.replaceAll("%type%", announcementType);
							html = html.replaceAll("%initial%", announcementInital);
							html = html.replaceAll("%delay%", announcementDelay);
							html = html.replaceAll("%repeat%", announcementRepeat);
							html = html.replaceAll("%author%", announcementAuthor);
							html = html.replaceAll("%content%", announcementContent);
							Util.sendCBHtml(player, html);
							break;
						}
						final var annType = st.nextToken();
						final var type = AnnouncementType.findByName(annType);
						switch (announce.getType())
						{
							case AUTO_CRITICAL:
							case AUTO_NORMAL:
							{
								switch (type)
								{
									case AUTO_CRITICAL:
									case AUTO_NORMAL:
									{
										break;
									}
									default:
									{
										player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_type_changed_auto"));
										return false;
									}
								}
								break;
							}
							case NORMAL:
							case CRITICAL:
							{
								switch (type)
								{
									case NORMAL:
									case CRITICAL:
									{
										break;
									}
									default:
									{
										player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_type_changed_normal"));
										return false;
									}
								}
								break;
							}
						}
						// ************************************
						if (!st.hasMoreTokens())
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_add"));
							break;
						}
						var annInitDelay = st.nextToken();
						if (!StringUtil.isDigit(annInitDelay))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_add"));
							break;
						}
						var initDelay = Integer.parseInt(annInitDelay);
						// ************************************
						if (!st.hasMoreTokens())
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_add"));
							break;
						}
						var annDelay = st.nextToken();
						if (!StringUtil.isDigit(annDelay))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_add"));
							break;
						}
						var delay = Integer.parseInt(annDelay);
						if ((delay < 10) && ((type == AnnouncementType.AUTO_NORMAL) || (type == AnnouncementType.AUTO_CRITICAL)))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_delay_cannot_10_sec"));
							break;
						}
						// ************************************
						if (!st.hasMoreTokens())
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_add"));
							break;
						}
						var annRepeat = st.nextToken();
						if (!StringUtil.isDigit(annRepeat))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_add"));
							break;
						}
						var repeat = Integer.parseInt(annRepeat);
						if (repeat == 0)
						{
							repeat = -1;
						}
						// ************************************
						var content = "";
						if (st.hasMoreTokens())
						{
							content = st.nextToken();
							while (st.hasMoreTokens())
							{
								content += " " + st.nextToken();
							}
						}
						if (content.isEmpty())
						{
							content = announce.getContent();
						}
						// ************************************
						announce.setType(type);
						announce.setContent(content);
						announce.setAuthor(player.getName());
						if (announce instanceof AutoAnnouncement)
						{
							var autoAnnounce = (AutoAnnouncement) announce;
							autoAnnounce.setInitial(initDelay * 1000);
							autoAnnounce.setDelay(delay * 1000);
							autoAnnounce.setRepeat(repeat);
						}
						announce.updateMe();
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_been_successfully_edited"));
						return useAdminCommand("admin_announces list", player);
					}
					case "remove":
					{
						if (!st.hasMoreTokens())
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_remove"));
							break;
						}
						var token = st.nextToken();
						if (!StringUtil.isDigit(token))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_remove"));
							break;
						}
						var id = Integer.parseInt(token);
						if (AnnouncementsTable.getInstance().deleteAnnouncement(id))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_been_successfully_removed"));
						}
						else
						{
							player.sendMessage(MessagesData.getInstance().getMessage(player, "admin_announce_doesnt_exists"));
						}
						return useAdminCommand("admin_announces list", player);
					}
					case "restart":
					{
						if (!st.hasMoreTokens())
						{
							for (var announce : AnnouncementsTable.getInstance().getAllAnnouncements())
							{
								if (announce instanceof AutoAnnouncement)
								{
									var autoAnnounce = (AutoAnnouncement) announce;
									autoAnnounce.restartMe();
								}
							}
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_been_successfully_restarted"));
							break;
						}
						var token = st.nextToken();
						if (!StringUtil.isDigit(token))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_show"));
							break;
						}
						var id = Integer.parseInt(token);
						var announce = AnnouncementsTable.getInstance().getAnnounce(id);
						if (announce != null)
						{
							if (announce instanceof AutoAnnouncement)
							{
								var autoAnnounce = (AutoAnnouncement) announce;
								autoAnnounce.restartMe();
								player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_been_successfully_restarted"));
							}
							else
							{
								player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_option_effect_only_announcements"));
							}
						}
						else
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_doesnt_exists"));
						}
						break;
					}
					case "show":
					{
						if (!st.hasMoreTokens())
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_show"));
							break;
						}
						var token = st.nextToken();
						if (!StringUtil.isDigit(token))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_announce_show"));
							break;
						}
						var id = Integer.parseInt(token);
						var announce = AnnouncementsTable.getInstance().getAnnounce(id);
						if (announce != null)
						{
							var html = HtmCache.getInstance().getHtm(player, "data/html/admin/submenu/announces/show.htm");
							var announcementId = "" + announce.getId();
							var announcementType = announce.getType().name();
							var announcementInital = "0";
							var announcementDelay = "0";
							var announcementRepeat = "0";
							var announcementAuthor = announce.getAuthor();
							var announcementContent = announce.getContent();
							if (announce instanceof AutoAnnouncement)
							{
								var autoAnnounce = (AutoAnnouncement) announce;
								announcementInital = "" + (autoAnnounce.getInitial() / 1000);
								announcementDelay = "" + (autoAnnounce.getDelay() / 1000);
								announcementRepeat = "" + autoAnnounce.getRepeat();
							}
							html = html.replaceAll("%id%", announcementId);
							html = html.replaceAll("%type%", announcementType);
							html = html.replaceAll("%initial%", announcementInital);
							html = html.replaceAll("%delay%", announcementDelay);
							html = html.replaceAll("%repeat%", announcementRepeat);
							html = html.replaceAll("%author%", announcementAuthor);
							html = html.replaceAll("%content%", announcementContent);
							Util.sendCBHtml(player, html);
							break;
						}
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_announce_doesnt_exists"));
						return useAdminCommand("admin_announces list", player);
					}
					case "list":
					{
						var page = 0;
						if (st.hasMoreTokens())
						{
							var token = st.nextToken();
							if (StringUtil.isDigit(token))
							{
								page = Integer.valueOf(token);
							}
						}
						
						var html = HtmCache.getInstance().getHtm(player, "data/html/admin/submenu/announces/list.htm");
						var result = HtmlUtil.createPage(AnnouncementsTable.getInstance().getAllAnnouncements(), page, 8, currentPage ->
						{
							return "<td align=center><button action=\"bypass admin_announces list " + currentPage + "\" value=\"" + (currentPage + 1) + "\" width=35 height=20 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";
						}, announcement ->
						{
							var sb = new StringBuilder();
							sb.append("<tr>");
							sb.append("<td width=5></td>");
							sb.append("<td width=80>" + announcement.getId() + "</td>");
							sb.append("<td width=100>" + announcement.getType() + "</td>");
							sb.append("<td width=100>" + announcement.getAuthor() + "</td>");
							if ((announcement.getType() == AnnouncementType.AUTO_NORMAL) || (announcement.getType() == AnnouncementType.AUTO_CRITICAL))
							{
								sb.append("<td width=60><button action=\"bypass -h admin_announces restart " + announcement.getId() + "\" value=\"" + MessagesData.getInstance().getMessage(player, "admin_button_restart")
									+ "\" width=\"60\" height=\"21\" back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
							}
							else
							{
								sb.append("<td width=60><button action=\"\" value=\"\" width=\"60\" height=\"21\" back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
							}
							if (announcement.getType() == AnnouncementType.EVENT)
							{
								sb.append("<td width=60><button action=\"bypass -h admin_announces show " + announcement.getId() + "\" value=\"" + MessagesData.getInstance().getMessage(player, "admin_button_show")
									+ "\" width=\"60\" height=\"21\" back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
								sb.append("<td width=60></td>");
							}
							else
							{
								sb.append("<td width=60><button action=\"bypass -h admin_announces show " + announcement.getId() + "\" value=\"" + MessagesData.getInstance().getMessage(player, "admin_button_show")
									+ "\" width=\"60\" height=\"21\" back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
								sb.append("<td width=60><button action=\"bypass -h admin_announces edit " + announcement.getId() + "\" value=\"" + MessagesData.getInstance().getMessage(player, "admin_button_edit")
									+ "\" width=\"60\" height=\"21\" back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
							}
							sb.append("<td width=60><button action=\"bypass -h admin_announces remove " + announcement.getId() + "\" value=\"" + MessagesData.getInstance().getMessage(player, "admin_button_delete")
								+ "\" width=\"60\" height=\"21\" back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
							sb.append("<td width=5></td>");
							sb.append("</tr>");
							return sb.toString();
						});
						html = html.replaceAll("%pages%", result.getPagerTemplate().toString());
						html = html.replaceAll("%announcements%", result.getBodyTemplate().toString());
						Util.sendCBHtml(player, html);
						break;
					}
				}
			}
		}
		return false;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}