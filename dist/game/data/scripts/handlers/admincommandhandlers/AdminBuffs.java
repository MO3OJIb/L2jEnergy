/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.configuration.config.AdminConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.SkillTreesData;
import com.l2jserver.gameserver.enums.skills.AbnormalType;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.effects.AbstractEffect;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.network.serverpackets.SkillCoolTime;

/**
 * Buffs commands.
 */
public class AdminBuffs implements IAdminCommandHandler
{
	private final static int PAGE_LIMIT = 20;
	
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_getbuffs",
		"admin_getbuffs_ps",
		"admin_stopbuff",
		"admin_stopallbuffs",
		"admin_areacancel",
		"admin_removereuse",
		"admin_switch_gm_buffs"
	};
	// Misc
	private static final String FONT_RED1 = "<font color=\"FF0000\">";
	private static final String FONT_RED2 = "</font>";
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.startsWith("admin_getbuffs"))
		{
			var st = new StringTokenizer(command, " ");
			command = st.nextToken();
			if (st.hasMoreTokens())
			{
				var name = st.nextToken();
				L2PcInstance pl = L2World.getInstance().getPlayer(name);
				if (pl != null)
				{
					var page = 1;
					if (st.hasMoreTokens())
					{
						page = Integer.parseInt(st.nextToken());
					}
					showBuffs(player, pl, page, command.endsWith("_ps"));
					return true;
				}
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_player_not_online").replace("%s%", name + ""));
				return false;
			}
			else if ((player.getTarget() != null) && player.getTarget().isCharacter())
			{
				showBuffs(player, (L2Character) player.getTarget(), 1, command.endsWith("_ps"));
				return true;
			}
			else
			{
				player.sendPacket(SystemMessageId.TARGET_IS_INCORRECT);
				return false;
			}
		}
		else if (command.startsWith("admin_stopbuff"))
		{
			try
			{
				var st = new StringTokenizer(command, " ");
				
				st.nextToken();
				var objectId = Integer.parseInt(st.nextToken());
				int skillId = Integer.parseInt(st.nextToken());
				
				removeBuff(player, objectId, skillId);
				return true;
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_failed_removing_effect").replace("%s%", ex + ""));
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_stopbuff"));
				return false;
			}
		}
		else if (command.startsWith("admin_stopallbuffs"))
		{
			try
			{
				var st = new StringTokenizer(command, " ");
				st.nextToken();
				var objectId = Integer.parseInt(st.nextToken());
				removeAllBuffs(player, objectId);
				return true;
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_failed_removing_all_effect").replace("%s%", ex + ""));
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_stopallbuffs"));
				return false;
			}
		}
		else if (command.startsWith("admin_areacancel"))
		{
			var st = new StringTokenizer(command, " ");
			st.nextToken();
			var val = st.nextToken();
			try
			{
				var radius = Integer.parseInt(val);
				
				for (L2Character knownChar : player.getKnownList().getKnownCharactersInRadius(radius))
				{
					if (knownChar.isPlayer() && !knownChar.equals(player))
					{
						knownChar.stopAllEffects();
					}
				}
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_canceled_all_effect_radius").replace("%s%", radius + ""));
				return true;
			}
			catch (NumberFormatException ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_areacancel"));
				return false;
			}
		}
		else if (command.startsWith("admin_removereuse"))
		{
			var st = new StringTokenizer(command, " ");
			command = st.nextToken();
			
			L2Character creature = null;
			if (st.hasMoreTokens())
			{
				creature = L2World.getInstance().getPlayer(st.nextToken());
				if (creature == null)
				{
					player.sendPacket(SystemMessageId.THAT_PLAYER_IS_NOT_ONLINE);
					return false;
				}
			}
			else
			{
				var target = player.getTarget();
				if ((target != null) && target.isCharacter())
				{
					creature = (L2Character) target;
				}
				
				if (creature == null)
				{
					player.sendPacket(SystemMessageId.TARGET_IS_INCORRECT);
					return false;
				}
			}
			
			creature.resetTimeStamps();
			creature.resetDisabledSkills();
			if (creature.isPlayer())
			{
				creature.sendPacket(new SkillCoolTime(creature.getActingPlayer()));
			}
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_skill_reuse_removed_from").replace("%s%", creature.getName() + ""));
			return true;
		}
		else if (command.startsWith("admin_switch_gm_buffs"))
		{
			if (AdminConfig.GM_GIVE_SPECIAL_SKILLS != AdminConfig.GM_GIVE_SPECIAL_AURA_SKILLS)
			{
				var toAuraSkills = player.getKnownSkill(7041) != null;
				switchSkills(player, toAuraSkills);
				player.sendSkillList();
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_have_succefully_changed_target").replace("%s%", (toAuraSkills ? "aura" : "one")));
				return true;
			}
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_nothing_switch"));
			return false;
		}
		return true;
	}
	
	public static void switchSkills(L2PcInstance player, boolean toAuraSkills)
	{
		var skills = toAuraSkills ? SkillTreesData.getInstance().getGMSkillTree().values() : SkillTreesData.getInstance().getGMAuraSkillTree().values();
		for (var skill : skills)
		{
			player.removeSkill(skill, false); // Don't Save GM skills to database
		}
		SkillTreesData.getInstance().addSkills(player, toAuraSkills);
	}
	
	public static void showBuffs(L2PcInstance player, L2Character target, int page, boolean passive)
	{
		final List<BuffInfo> effects = new ArrayList<>();
		if (!passive)
		{
			effects.addAll(target.getEffectList().getEffects());
		}
		else
		{
			effects.addAll(target.getEffectList().getPassives());
		}
		
		if ((page > ((effects.size() / PAGE_LIMIT) + 1)) || (page < 1))
		{
			return;
		}
		
		var max = effects.size() / PAGE_LIMIT;
		if (effects.size() > (PAGE_LIMIT * max))
		{
			max++;
		}
		
		final var sb = new StringBuilder("<html><table width=\"100%\"><tr><td width=45><button value=\"Main\" action=\"bypass -h admin_admin\" width=45 height=21 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td><td width=180><center><font color=\"LEVEL\">Effects of " + target.getName()
			+ "</font></td><td width=45><button value=\"Back\" action=\"bypass -h admin_current_player\" width=45 height=21 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr></table><br><table width=\"100%\"><tr><td width=200>Skill</td><td width=30>Rem. Time</td><td width=70>Action</td></tr>");
		var start = ((page - 1) * PAGE_LIMIT);
		var end = Math.min(((page - 1) * PAGE_LIMIT) + PAGE_LIMIT, effects.size());
		var count = 0;
		for (BuffInfo info : effects)
		{
			if ((count >= start) && (count < end))
			{
				var skill = info.getSkill();
				for (AbstractEffect effect : info.getEffects())
				{
					StringUtil.append(sb, "<tr><td>", (!info.isInUse() ? FONT_RED1 : "") + skill.getName(), " Lv ", String.valueOf(skill.getLevel()), " (", effect.getClass().getSimpleName(), ")"
						+ (!info.isInUse() ? FONT_RED2 : ""), "</td><td>", skill.isToggle() ? "T (" + info.getTickCount(effect) + ")" : skill.isPassive() ? "P" : info.getTime()
							+ "s", "</td><td><button value=\"X\" action=\"bypass -h admin_stopbuff ", Integer.toString(target.getObjectId()), " ", String.valueOf(skill.getId()), "\" width=30 height=21 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr>");
				}
			}
			count++;
		}
		
		sb.append("</table><br><table width=300 bgcolor=444444><tr>");
		for (int x = 0; x < max; x++)
		{
			int pagenr = x + 1;
			if (page == pagenr)
			{
				StringUtil.append(sb, "<td>Page ", pagenr, "</td>");
			}
			else
			{
				StringUtil.append(sb, "<td><a action=\"bypass -h admin_getbuffs" + (passive ? "_ps " : " "), target.getName(), " ", x + 1, "\"> Page ", pagenr, "</a></td>");
			}
		}
		
		// Buttons
		StringUtil.append(sb, "</tr></table><br><br><center><button value=\"" + MessagesData.getInstance().getMessage(player, "admin_button_refresh")
			+ "\" action=\"bypass -h admin_getbuffs", (passive ? "_ps " : " "), target.getName(), "\" width=80 height=21 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\">");
		StringUtil.append(sb, "<button value=\"" + MessagesData.getInstance().getMessage(player, "admin_button_remove_all") + "\" action=\"bypass -h admin_stopallbuffs ", Integer.toString(target.getObjectId()), "\" width=80 height=21 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"><br>");
		// Legend
		if (!passive)
		{
			StringUtil.append(sb, FONT_RED1, "Inactive buffs: ", String.valueOf(target.getEffectList().getHiddenBuffsCount()), FONT_RED2, "<br>");
		}
		StringUtil.append(sb, "Total", passive ? " passive" : "", " buff count: ", String.valueOf(effects.size()));
		if ((target.getEffectList().getAllBlockedBuffSlots() != null) && !target.getEffectList().getAllBlockedBuffSlots().isEmpty())
		{
			StringUtil.append(sb, "<br>Blocked buff slots: ");
			var slots = "";
			for (AbnormalType slot : target.getEffectList().getAllBlockedBuffSlots())
			{
				slots += slot.toString() + ", ";
			}
			
			if (!slots.isEmpty() && (slots.length() > 3))
			{
				StringUtil.append(sb, slots.substring(0, slots.length() - 2));
			}
		}
		StringUtil.append(sb, "</html>");
		
		var html = new NpcHtmlMessage(0);
		html.setHtml(sb.toString());
		player.sendPacket(html);
	}
	
	private static void removeBuff(L2PcInstance player, int objectId, int skillId)
	{
		L2Character target = null;
		try
		{
			target = (L2Character) L2World.getInstance().getObject(objectId);
		}
		catch (Exception e)
		{
		}
		
		if ((target != null) && (skillId > 0))
		{
			if (target.isAffectedBySkill(skillId))
			{
				target.stopSkillEffects(true, skillId);
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_removed_skill_id").replace("%s%", skillId + "").replace("%c%", target.getName() + "").replace("%i%", objectId + ""));
			}
			showBuffs(player, target, 1, false);
		}
	}
	
	private static void removeAllBuffs(L2PcInstance player, int objectId)
	{
		L2Character target = null;
		try
		{
			target = (L2Character) L2World.getInstance().getObject(objectId);
		}
		catch (Exception e)
		{
		}
		
		if (target != null)
		{
			target.stopAllEffects();
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_removed_all_effects").replace("%c%", target.getName() + "").replace("%i%", objectId + ""));
			showBuffs(player, target, 1, false);
		}
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
