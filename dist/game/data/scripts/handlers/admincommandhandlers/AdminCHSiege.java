/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.Calendar;

import com.l2jserver.gameserver.configuration.config.DeveloperConfig;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.CHSiegeManager;
import com.l2jserver.gameserver.model.L2Clan;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.clanhall.SiegableHall;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.network.serverpackets.SiegeInfo;

/**
 * Siege command.
 * @author BiggBoss
 */
public final class AdminCHSiege implements IAdminCommandHandler
{
	private static final String[] COMMANDS =
	{
		"admin_chsiege_siegablehall",
		"admin_chsiege_startSiege",
		"admin_chsiege_endsSiege",
		"admin_chsiege_setSiegeDate",
		"admin_chsiege_addAttacker",
		"admin_chsiege_removeAttacker",
		"admin_chsiege_clearAttackers",
		"admin_chsiege_listAttackers",
		"admin_chsiege_forwardSiege"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var split = command.split(" ");
		SiegableHall hall = null;
		if (DeveloperConfig.ALT_DEV_NO_QUESTS)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_clan_hall_sieges_disabled"));
			return false;
		}
		if (split.length < 2)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_have_specify_hall_least"));
			return false;
		}
		if ((hall = getHall(split[1], player)) == null)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_couldnt_find_desired_siegable_hall").replace("%i%", split[1] + ""));
			return false;
		}
		if (hall.getSiege() == null)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_given_hall_dont_have_any_attached_siege"));
			return false;
		}
		
		if (split[0].equals(COMMANDS[1]))
		{
			if (hall.isInSiege())
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_requested_clan_hall_alredy_siege"));
			}
			else
			{
				var owner = ClanTable.getInstance().getClan(hall.getOwnerId());
				if (owner != null)
				{
					hall.free();
					owner.setHideoutId(0);
					hall.addAttacker(owner);
				}
				hall.getSiege().startSiege();
			}
		}
		else if (split[0].equals(COMMANDS[2]))
		{
			if (!hall.isInSiege())
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_requested_clan_hall_siege"));
			}
			else
			{
				hall.getSiege().endSiege();
			}
		}
		else if (split[0].equals(COMMANDS[3]))
		{
			if (!hall.isRegistering())
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_cannot_change_siege_date_hall_siege"));
			}
			else if (split.length < 3)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_incorrect_date_format_try_again"));
			}
			else
			{
				var rawDate = split[2].split(";");
				if (rawDate.length < 2)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_have_specify_format"));
				}
				else
				{
					var day = rawDate[0].split("-");
					var hour = rawDate[1].split(":");
					if ((day.length < 3) || (hour.length < 2))
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_incomplete_day_hour_both"));
					}
					else
					{
						var d = parseInt(day[0]);
						var month = parseInt(day[1]) - 1;
						var year = parseInt(day[2]);
						var h = parseInt(hour[0]);
						var min = parseInt(hour[1]);
						if (((month == 2) && (d > 28)) || (d > 31) || (d <= 0) || (month <= 0) || (month > 12) || (year < Calendar.getInstance().get(Calendar.YEAR)))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_wrong_day_month_year_gave"));
						}
						else if ((h <= 0) || (h > 24) || (min < 0) || (min >= 60))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_wrong_hour_minutes_gave"));
						}
						else
						{
							var c = Calendar.getInstance();
							c.set(Calendar.YEAR, year);
							c.set(Calendar.MONTH, month);
							c.set(Calendar.DAY_OF_MONTH, d);
							c.set(Calendar.HOUR_OF_DAY, h);
							c.set(Calendar.MINUTE, min);
							c.set(Calendar.SECOND, 0);
							
							if (c.getTimeInMillis() > System.currentTimeMillis())
							{
								player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_your_access_level_has_been_changed_to").replace("%c%", hall.getName() + "").replace("%i%", c.getTime().toString() + ""));
								hall.setNextSiegeDate(c.getTimeInMillis());
								hall.getSiege().updateSiege();
								hall.updateDb();
							}
							else
							{
								player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_given_time_past"));
							}
						}
					}
					
				}
			}
		}
		else if (split[0].equals(COMMANDS[4]))
		{
			if (hall.isInSiege())
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_clan_hall_siege_cannot_add_attackers_now"));
				return false;
			}
			
			L2Clan attacker = null;
			if (split.length < 3)
			{
				var object = player.getTarget();
				L2PcInstance target = null;
				if (object == null)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_must_target_clan_member_attacker"));
				}
				else if (!(object instanceof L2PcInstance))
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_must_target_player_with_clan"));
				}
				else if ((target = (L2PcInstance) object).getClan() == null)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_target_does_not_have_any_clan"));
				}
				else if (hall.getSiege().checkIsAttacker(target.getClan()))
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_target_clan_alredy_participating"));
				}
				else
				{
					attacker = target.getClan();
				}
			}
			else
			{
				var clan = ClanTable.getInstance().getClanByName(split[2]);
				if (clan == null)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_given_clan_not_exist"));
				}
				else if (hall.getSiege().checkIsAttacker(clan))
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_given_clan_alredy_participating"));
				}
				else
				{
					attacker = clan;
				}
			}
			
			if (attacker != null)
			{
				hall.addAttacker(attacker);
			}
		}
		else if (split[0].equals(COMMANDS[5]))
		{
			if (hall.isInSiege())
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_clan_hall_siege_cannot_remove_attackers_now"));
				return false;
			}
			
			if (split.length < 3)
			{
				var object = player.getTarget();
				L2PcInstance target = null;
				if (object == null)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_must_target_clan_member_attacker"));
				}
				else if (!(object instanceof L2PcInstance))
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_must_target_player_with_clan"));
				}
				else if ((target = (L2PcInstance) object).getClan() == null)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_target_does_not_have_any_clan"));
				}
				else if (!hall.getSiege().checkIsAttacker(target.getClan()))
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_target_clan_not_participating"));
				}
				else
				{
					hall.removeAttacker(target.getClan());
				}
			}
			else
			{
				var clan = ClanTable.getInstance().getClanByName(split[2]);
				if (clan == null)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_given_clan_not_exist"));
				}
				else if (!hall.getSiege().checkIsAttacker(clan))
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_given_clan_not_participating"));
				}
				else
				{
					hall.removeAttacker(clan);
				}
			}
		}
		else if (split[0].equals(COMMANDS[6]))
		{
			if (hall.isInSiege())
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_requested_hall_siege_right_now_cannot_clear_attacker_list"));
			}
			else
			{
				hall.getSiege().getAttackers().clear();
			}
		}
		else if (split[0].equals(COMMANDS[7]))
		{
			player.sendPacket(new SiegeInfo(hall));
		}
		else if (split[0].equals(COMMANDS[8]))
		{
			var siegable = hall.getSiege();
			siegable.cancelSiegeTask();
			switch (hall.getSiegeStatus())
			{
				case REGISTERING -> siegable.prepareOwner();
				case WAITING_BATTLE -> siegable.startSiege();
				case RUNNING -> siegable.endSiege();
			}
		}
		sendSiegableHallPage(player, split[1], hall);
		return false;
	}
	
	private SiegableHall getHall(String id, L2PcInstance player)
	{
		var ch = parseInt(id);
		if (ch == 0)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_wrong_clan_hall_id"));
			return null;
		}
		
		var hall = CHSiegeManager.getInstance().getSiegableHall(ch);
		
		if (hall == null)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_сouldnt_find_clan_hall"));
		}
		return hall;
	}
	
	private int parseInt(String st)
	{
		var val = 0;
		try
		{
			val = Integer.parseInt(st);
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		return val;
	}
	
	private void sendSiegableHallPage(L2PcInstance player, String hallId, SiegableHall hall)
	{
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/siegablehall.htm");
		
		html.replace("%clanhallId%", hallId);
		html.replace("%clanhallName%", hall.getName());
		if (hall.getOwnerId() > 0)
		{
			var owner = ClanTable.getInstance().getClan(hall.getOwnerId());
			if (owner != null)
			{
				html.replace("%clanhallOwner%", owner.getName());
			}
			else
			{
				html.replace("%clanhallOwner%", " " + MessagesData.getInstance().getMessage(player, "admin_no_owner") + "");
			}
		}
		else
		{
			html.replace("%clanhallOwner%", " " + MessagesData.getInstance().getMessage(player, "admin_no_owner") + "");
		}
		player.sendPacket(html);
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return COMMANDS;
	}
}
