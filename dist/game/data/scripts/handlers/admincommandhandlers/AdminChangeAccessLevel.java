/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.xml.impl.AdminData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;

/**
 * Change access level command.
 */
public final class AdminChangeAccessLevel implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_changelvl"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var parts = command.split(" ");
		if (parts.length == 2)
		{
			try
			{
				var lvl = Integer.parseInt(parts[1]);
				if (player.getTarget() instanceof L2PcInstance)
				{
					onlineChange(player, (L2PcInstance) player.getTarget(), lvl);
				}
				else
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_changelvl"));
			}
		}
		else if (parts.length == 3)
		{
			var name = parts[1];
			var lvl = Integer.parseInt(parts[2]);
			var pl = L2World.getInstance().getPlayer(name);
			if (pl != null)
			{
				onlineChange(player, pl, lvl);
			}
			else
			{
				DAOFactory.getInstance().getPlayerDAO().getChangeAccessLevel(lvl, name, player);
			}
		}
		return true;
	}
	
	private static void onlineChange(L2PcInstance player, L2PcInstance target, int lvl)
	{
		if (lvl >= 0)
		{
			if (AdminData.getInstance().hasAccessLevel(lvl))
			{
				var acccessLevel = AdminData.getInstance().getAccessLevel(lvl);
				target.setAccessLevel(lvl);
				target.sendMessage(MessagesData.getInstance().getMessage(target, "admin_your_access_level_has_been_changed_to").replace("%c%", acccessLevel.getName() + "").replace("%i%", acccessLevel.getName() + ""));
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "players_access_level_has_been_changed_to").replace("%s%", target.getName() + "").replace("%c%", acccessLevel.getName() + "").replace("%i%", acccessLevel.getName() + ""));
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_trying_set_unexisting_access_level").replace("%s%", lvl + ""));
			}
		}
		else
		{
			target.setAccessLevel(lvl);
			target.sendMessage(MessagesData.getInstance().getMessage(player, "player_your_character_banned"));
			target.logout();
		}
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
