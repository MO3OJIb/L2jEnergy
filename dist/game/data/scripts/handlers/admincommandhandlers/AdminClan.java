/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.instancemanager.ClanHallManager;
import com.l2jserver.gameserver.instancemanager.FortManager;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * Clan command.
 * @author UnAfraid
 * @author Zoey76
 */
public class AdminClan implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_clan_info",
		"admin_clan_changeleader",
		"admin_clan_show_pending",
		"admin_clan_force_pending"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command);
		var cmd = st.nextToken();
		switch (cmd)
		{
			case "admin_clan_info" ->
			{
				var pl = getPlayer(player, st);
				if (pl == null)
				{
					break;
				}
				
				var clan = pl.getClan();
				if (clan == null)
				{
					player.sendPacket(SystemMessageId.TARGET_MUST_BE_IN_CLAN);
					return false;
				}
				
				var html = new NpcHtmlMessage(0);
				html.setFile(player, "data/html/admin/submenu/claninfo.htm");
				
				html.replace("%clan_name%", clan.getName());
				html.replace("%clan_leader%", clan.getLeaderName());
				html.replace("%clan_level%", String.valueOf(clan.getLevel()));
				html.replace("%clan_has_castle%", clan.getCastleId() > 0 ? CastleManager.getInstance().getCastleById(clan.getCastleId()).getName() : MessagesData.getInstance().getMessage(player, "admin_htm_no"));
				html.replace("%clan_has_clanhall%", clan.getHideoutId() > 0 ? ClanHallManager.getInstance().getClanHallById(clan.getHideoutId()).getName() : MessagesData.getInstance().getMessage(player, "admin_htm_no"));
				html.replace("%clan_has_fortress%", clan.getFortId() > 0 ? FortManager.getInstance().getFortById(clan.getFortId()).getName() : MessagesData.getInstance().getMessage(player, "admin_htm_no"));
				html.replace("%clan_points%", String.valueOf(clan.getReputationScore()));
				html.replace("%clan_players_count%", String.valueOf(clan.getMembersCount()));
				html.replace("%clan_ally%", clan.getAllyId() > 0 ? clan.getAllyName() : MessagesData.getInstance().getMessage(player, "admin_htm_not_in_ally"));
				html.replace("%current_player_objectId%", String.valueOf(pl.getObjectId()));
				html.replace("%current_player_name%", pl.getName());
				player.sendPacket(html);
			}
			case "admin_clan_changeleader" ->
			{
				var pl = getPlayer(player, st);
				if (pl == null)
				{
					break;
				}
				
				var clan = pl.getClan();
				if (clan == null)
				{
					player.sendPacket(SystemMessageId.TARGET_MUST_BE_IN_CLAN);
					return false;
				}
				
				var member = clan.getClanMember(pl.getObjectId());
				if (member != null)
				{
					if (pl.isAcademyMember())
					{
						pl.sendPacket(SystemMessageId.RIGHT_CANT_TRANSFERRED_TO_ACADEMY_MEMBER);
					}
					else
					{
						clan.setNewLeader(member);
					}
				}
			}
			case "admin_clan_show_pending" ->
			{
				var html = new NpcHtmlMessage(0);
				html.setFile(player, "data/html/admin/submenu/clanchanges.htm");
				
				var sb = new StringBuilder();
				for (var clan : ClanTable.getInstance().getClans())
				{
					if (clan.getNewLeaderId() != 0)
					{
						sb.append("<tr>");
						sb.append("<td>" + clan.getName() + "</td>");
						sb.append("<td>" + clan.getNewLeaderName() + "</td>");
						sb.append("<td><a action=\"bypass -h admin_clan_force_pending " + clan.getId() + "\"> " + MessagesData.getInstance().getMessage(player, "admin_button_forse") + "</a></td>");
						sb.append("</tr>");
					}
				}
				html.replace("%data%", sb.toString());
				player.sendPacket(html);
			}
			case "admin_clan_force_pending" ->
			{
				if (st.hasMoreElements())
				{
					var token = st.nextToken();
					if (!StringUtil.isDigit(token))
					{
						break;
					}
					var clanId = Integer.parseInt(token);
					
					var clan = ClanTable.getInstance().getClan(clanId);
					if (clan == null)
					{
						break;
					}
					
					var member = clan.getClanMember(clan.getNewLeaderId());
					if (member == null)
					{
						break;
					}
					clan.setNewLeader(member);
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_task_have_been_forcely_executed"));
				}
			}
		}
		return true;
	}
	
	private L2PcInstance getPlayer(L2PcInstance player, StringTokenizer st)
	{
		String val;
		L2PcInstance pl = null;
		if (st.hasMoreTokens())
		{
			val = st.nextToken();
			if (StringUtil.isDigit(val))
			{
				pl = L2World.getInstance().getPlayer(Integer.parseInt(val));
				if (pl == null)
				{
					player.sendPacket(SystemMessageId.THAT_PLAYER_IS_NOT_ONLINE);
					return null;
				}
			}
			else
			{
				pl = L2World.getInstance().getPlayer(val);
				if (pl == null)
				{
					player.sendPacket(SystemMessageId.INCORRECT_NAME_TRY_AGAIN);
					return null;
				}
			}
		}
		else
		{
			var targetObj = player.getTarget();
			if (targetObj instanceof L2PcInstance)
			{
				pl = targetObj.getActingPlayer();
			}
			else
			{
				player.sendPacket(SystemMessageId.INVALID_TARGET);
				return null;
			}
		}
		return pl;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}