/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;

/**
 * Create item command.
 */
public class AdminCreateItem implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_itemcreate",
		"admin_create_item",
		"admin_create_coin",
		"admin_give_item_target",
		"admin_give_item_to_all"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.equals("admin_itemcreate"))
		{
			AdminHtml.showAdminHtml(player, "submenu/itemcreation.htm");
		}
		else if (command.startsWith("admin_create_item"))
		{
			try
			{
				var val = command.substring(17);
				var st = new StringTokenizer(val);
				if (st.countTokens() == 2)
				{
					var id = st.nextToken();
					var idval = Integer.parseInt(id);
					var num = st.nextToken();
					var numval = Long.parseLong(num);
					createItem(player, player, idval, numval);
				}
				else if (st.countTokens() == 1)
				{
					var id = st.nextToken();
					var idval = Integer.parseInt(id);
					createItem(player, player, idval, 1);
				}
			}
			catch (StringIndexOutOfBoundsException e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_create_item"));
			}
			catch (NumberFormatException nfe)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_specify_valid_number"));
			}
			AdminHtml.showAdminHtml(player, "submenu/itemcreation.htm");
		}
		else if (command.startsWith("admin_create_coin"))
		{
			try
			{
				var val = command.substring(17);
				var st = new StringTokenizer(val);
				if (st.countTokens() == 2)
				{
					var name = st.nextToken();
					var idval = getCoinId(name);
					if (idval > 0)
					{
						var num = st.nextToken();
						var numval = Long.parseLong(num);
						createItem(player, player, idval, numval);
					}
				}
				else if (st.countTokens() == 1)
				{
					var name = st.nextToken();
					var idval = getCoinId(name);
					createItem(player, player, idval, 1);
				}
			}
			catch (StringIndexOutOfBoundsException e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_create_coin"));
			}
			catch (NumberFormatException nfe)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_specify_valid_number"));
			}
			AdminHtml.showAdminHtml(player, "submenu/itemcreation.htm");
		}
		else if (command.startsWith("admin_give_item_target"))
		{
			try
			{
				L2PcInstance target;
				if (player.getTarget() instanceof L2PcInstance)
				{
					target = (L2PcInstance) player.getTarget();
				}
				else
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
					return false;
				}
				
				var val = command.substring(22);
				var st = new StringTokenizer(val);
				if (st.countTokens() == 2)
				{
					var id = st.nextToken();
					var idval = Integer.parseInt(id);
					var num = st.nextToken();
					var numval = Long.parseLong(num);
					createItem(player, target, idval, numval);
				}
				else if (st.countTokens() == 1)
				{
					var id = st.nextToken();
					var idval = Integer.parseInt(id);
					createItem(player, target, idval, 1);
				}
			}
			catch (StringIndexOutOfBoundsException e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_give_item_target"));
			}
			catch (NumberFormatException nfe)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_specify_valid_number"));
			}
			AdminHtml.showAdminHtml(player, "submenu/itemcreation.htm");
		}
		else if (command.startsWith("admin_give_item_to_all"))
		{
			var val = command.substring(22);
			var st = new StringTokenizer(val);
			var idval = 0;
			var numval = 0;
			if (st.countTokens() == 2)
			{
				var id = st.nextToken();
				idval = Integer.parseInt(id);
				var num = st.nextToken();
				numval = Integer.parseInt(num);
			}
			else if (st.countTokens() == 1)
			{
				var id = st.nextToken();
				idval = Integer.parseInt(id);
				numval = 1;
			}
			var counter = 0;
			var item = ItemTable.getInstance().getTemplate(idval);
			if (item == null)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_item_doesnt_exist"));
				return false;
			}
			if ((numval > 10) && !item.isStackable())
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_item_does_not_stack"));
				return false;
			}
			for (var pl : L2World.getInstance().getPlayers())
			{
				if ((player != pl) && pl.isOnline() && ((pl.getClient() != null) && !pl.getClient().isDetached()))
				{
					pl.getInventory().addItem("Admin", idval, numval, pl, player);
					pl.sendMessage(MessagesData.getInstance().getMessage(player, "player_admin_spawned_in_your_inventory").replace("%s%", numval + "").replace("%i%", item.getName() + ""));
					counter++;
				}
			}
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_players_rewarded_with").replace("%s%", counter + "").replace("%i%", item.getName() + ""));
		}
		return true;
	}
	
	private void createItem(L2PcInstance player, L2PcInstance target, int id, long num)
	{
		var item = ItemTable.getInstance().getTemplate(id);
		if (item == null)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_item_doesnt_exist"));
			return;
		}
		if ((num > 10) && !item.isStackable())
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_item_does_not_stack"));
			return;
		}
		
		target.getInventory().addItem("Admin", id, num, player, null);
		
		if (player != target)
		{
			target.sendMessage(MessagesData.getInstance().getMessage(player, "player_admin_spawned_in_your_inventory").replace("%s%", num + "").replace("%i%", item.getName() + ""));
		}
		player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_have_spawned_inventory").replace("%s%", num + "").replace("%c%", item.getName() + "").replace("%i%", id + "").replace("%n%", target.getName() + ""));
	}
	
	private int getCoinId(String name)
	{
		int id;
		if (name.equalsIgnoreCase("adena"))
		{
			id = 57;
		}
		else if (name.equalsIgnoreCase("ancientadena"))
		{
			id = 5575;
		}
		else if (name.equalsIgnoreCase("festivaladena"))
		{
			id = 6673;
		}
		else if (name.equalsIgnoreCase("blueeva"))
		{
			id = 4355;
		}
		else if (name.equalsIgnoreCase("goldeinhasad"))
		{
			id = 4356;
		}
		else if (name.equalsIgnoreCase("silvershilen"))
		{
			id = 4357;
		}
		else if (name.equalsIgnoreCase("bloodypaagrio"))
		{
			id = 4358;
		}
		else if (name.equalsIgnoreCase("fantasyislecoin"))
		{
			id = 13067;
		}
		else
		{
			id = 0;
		}
		return id;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
