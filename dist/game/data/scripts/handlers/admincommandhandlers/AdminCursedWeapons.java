/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.CursedWeaponsManager;
import com.l2jserver.gameserver.model.CursedWeapon;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * Cursed Weapons command.
 */
public class AdminCursedWeapons implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_cw_info",
		"admin_cw_remove",
		"admin_cw_goto",
		"admin_cw_add",
		"admin_cw_info_menu"
	};
	
	private int itemId;
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var id = 0;
		
		var st = new StringTokenizer(command);
		st.nextToken();
		
		if (command.startsWith("admin_cw_info"))
		{
			if (!command.contains("menu"))
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_info_cursed_weapons"));
				for (var cw : CursedWeaponsManager.getInstance().getCursedWeapons())
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_info_player").replace("%i%", cw.getName() + "").replace("%s%", cw.getItemId() + ""));
					if (cw.isActivated())
					{
						var pl = cw.getPlayer();
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_info_player_holding").replace("%i%", (pl == null ? "" + MessagesData.getInstance().getMessage(player, "admin_info_null") + "" : pl.getName()) + ""));
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_info_player_karma").replace("%i%", cw.getPlayerKarma() + ""));
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_info_time_remaining").replace("%i%", (cw.getTimeLeft() / 60000) + ""));
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_info_kills").replace("%i%", cw.getNbKills() + ""));
					}
					else if (cw.isDropped())
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_info_lying_ground"));
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_info_time_remaining").replace("%i%", (cw.getTimeLeft() / 60000) + ""));
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_info_kills").replace("%i%", cw.getNbKills() + ""));
					}
					else
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_info_dont_exist_world"));
					}
					player.sendPacket(SystemMessageId.FRIEND_LIST_FOOTER);
				}
			}
			else
			{
				var cws = CursedWeaponsManager.getInstance().getCursedWeapons();
				var sb = new StringBuilder(cws.size() * 300);
				var html = new NpcHtmlMessage(0);
				html.setFile(player, "data/html/admin/submenu/cwinfo.htm");
				
				for (var cw : CursedWeaponsManager.getInstance().getCursedWeapons())
				{
					itemId = cw.getItemId();
					
					// TODO: переписать
					StringUtil.append(sb, "<table width=270><tr><td>Name:</td><td>", cw.getName(), "</td></tr>");
					
					if (cw.isActivated())
					{
						var pl = cw.getPlayer();
						StringUtil.append(sb, "<tr><td>Weilder:</td><td>", (pl == null ? "null" : pl.getName()), "</td></tr>" + "<tr><td>Karma:</td><td>", cw.getPlayerKarma(), "</td></tr>" + "<tr><td>Kills:</td><td>", cw.getPlayerPkKills(), "/", cw.getNbKills(), "</td></tr>"
							+ "<tr><td>Time remaining:</td><td>", cw.getTimeLeft() / 60000, " min.</td></tr>" + "<tr><td><button value=\"Remove\" action=\"bypass -h admin_cw_remove ", itemId, "\" width=73 height=21 back=\"L2UI_CT1.ListCTRL_DF_Title_Down\" fore=\"L2UI_CT1.ListCTRL_DF_Title\"></td>"
								+ "<td><button value=\"Go\" action=\"bypass -h admin_cw_goto ", itemId, "\" width=73 height=21 back=\"L2UI_CT1.ListCTRL_DF_Title_Down\" fore=\"L2UI_CT1.ListCTRL_DF_Title\"></td></tr>");
					}
					else if (cw.isDropped())
					{
						StringUtil.append(sb, "<tr><td>Position:</td><td>Lying on the ground</td></tr>" + "<tr><td>Time remaining:</td><td>", cw.getTimeLeft() / 60000, " min.</td></tr>" + "<tr><td>Kills:</td><td>", cw.getNbKills(), "</td></tr>"
							+ "<tr><td><button value=\"Remove\" action=\"bypass -h admin_cw_remove ", itemId, "\" width=73 height=21 back=\"L2UI_CT1.ListCTRL_DF_Title_Down\" fore=\"L2UI_CT1.ListCTRL_DF_Title\"></td>"
								+ "<td><button value=\"Go\" action=\"bypass -h admin_cw_goto ", itemId, "\" width=73 height=21 back=\"L2UI_CT1.ListCTRL_DF_Title_Down\" fore=\"L2UI_CT1.ListCTRL_DF_Title\"></td></tr>");
					}
					else
					{
						StringUtil.append(sb, "<tr><td>Position:</td><td>Doesn't exist.</td></tr>" + "<tr><td><button value=\"Give to Target\" action=\"bypass -h admin_cw_add ", itemId, "\" width=130 height=21 back=\"L2UI_CT1.ListCTRL_DF_Title_Down\" fore=\"L2UI_CT1.ListCTRL_DF_Title\"></td><td></td></tr>");
					}
					
					sb.append("</table><br>");
				}
				html.replace("%cwinfo%", sb.toString());
				player.sendPacket(html);
			}
		}
		else
		{
			CursedWeapon cw = null;
			try
			{
				var parameter = st.nextToken();
				if (parameter.matches("[0-9]*"))
				{
					id = Integer.parseInt(parameter);
				}
				else
				{
					parameter = parameter.replace('_', ' ');
					for (var cwp : CursedWeaponsManager.getInstance().getCursedWeapons())
					{
						if (cwp.getName().toLowerCase().contains(parameter.toLowerCase()))
						{
							id = cwp.getItemId();
							break;
						}
					}
				}
				cw = CursedWeaponsManager.getInstance().getCursedWeapon(id);
			}
			catch (Exception e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_cw"));
			}
			
			if (cw == null)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unknown_cursed_weapon_id"));
				return false;
			}
			
			if (command.startsWith("admin_cw_remove "))
			{
				cw.endOfLife();
			}
			else if (command.startsWith("admin_cw_goto "))
			{
				cw.goTo(player);
			}
			else if (command.startsWith("admin_cw_add"))
			{
				if (cw.isActive())
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_this_cursed_weapon_already_active"));
				}
				else
				{
					var target = player.getTarget();
					if (target instanceof L2PcInstance)
					{
						((L2PcInstance) target).addItem("AdminCursedWeaponAdd", id, 1, target, true);
					}
					else
					{
						player.addItem("AdminCursedWeaponAdd", id, 1, player, true);
					}
					cw.setEndTime(System.currentTimeMillis() + (cw.getDuration() * 60000L));
					cw.reActivate();
				}
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unknown_command"));
			}
		}
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}