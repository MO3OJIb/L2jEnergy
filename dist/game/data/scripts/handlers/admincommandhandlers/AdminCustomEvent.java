/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.eventengine.GameEventManager;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * Custom Event command.
 */
public class AdminCustomEvent implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_events",
		"admin_events_start",
		"admin_events_stop"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (player == null)
		{
			return false;
		}
		
		var event_name = "";
		var st = new StringTokenizer(command, " ");
		st.nextToken();
		if (st.hasMoreTokens())
		{
			event_name = st.nextToken();
		}
		
		if (command.contains("_events"))
		{
			showMainPage(player);
		}
		
		if (command.startsWith("admin_events_start"))
		{
			try
			{
				if (event_name != null)
				{
					var event = GameEventManager.getInstance().findEvent(event_name);
					if (event != null)
					{
						GameEventManager.getInstance().startRegistration();
						player.sendAdminMessage("Custom Events " + event.getEventName() + " started.");
						return true;
					}
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage("Usage: //events_start <eventname>");
				return false;
			}
		}
		else if (command.startsWith("admin_events_stop"))
		{
			try
			{
				if (event_name != null)
				{
					var event = GameEventManager.getInstance().findEvent(event_name);
					if (event != null)
					{
						GameEventManager.getInstance().endEvent();
						player.sendAdminMessage("Custom Event " + event.getEventName() + " stopped.");
						return true;
					}
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage("Usage: //events_start <eventname>");
				return false;
			}
		}
		return true;
	}
	
	public void showMainPage(L2PcInstance player)
	{
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/events/custom_events.htm");
		
		var sb = new StringBuilder(500);
		
		for (var event : GameEventManager.getInstance().getAllEvents())
		{
			StringUtil.append(sb, "<tr><td><font color=\"LEVEL\">", event.getEventName(), ":</font></td><br><td><button value=\"Start\" action=\"bypass -h admin_events_start ", event
				.getEventName(), "\" width=80 height=25 back=\"L2UI_CT1.ListCTRL_DF_Title_Down\" fore=\"L2UI_CT1.ListCTRL_DF_Title\"></td><td><button value=\"Stop\" action=\"bypass -h admin_events_stop ", event
					.getEventName(), "\" width=80 height=25 back=\"L2UI_CT1.ListCTRL_DF_Title_Down\" fore=\"L2UI_CT1.ListCTRL_DF_Title\"></td></tr>");
		}
		html.replace("%events%", sb.toString());
		player.sendPacket(html);
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
