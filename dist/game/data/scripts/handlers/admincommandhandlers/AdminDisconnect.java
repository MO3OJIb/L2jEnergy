/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * Disconnect command.
 */
public class AdminDisconnect implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_character_disconnect"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.equals("admin_character_disconnect"))
		{
			disconnectCharacter(player);
		}
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
	
	private void disconnectCharacter(L2PcInstance player)
	{
		var target = player.getTarget();
		L2PcInstance pl = null;
		if (target instanceof L2PcInstance)
		{
			pl = (L2PcInstance) target;
		}
		else
		{
			return;
		}
		
		if (pl == player)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_cannot_logout_your_character"));
		}
		else
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_character_disconnected_server").replace("%i%", pl.getName() + ""));
			pl.logout();
		}
	}
}
