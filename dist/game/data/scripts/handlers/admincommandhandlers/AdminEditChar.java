/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.data.sql.impl.CharNameTable;
import com.l2jserver.gameserver.data.xml.impl.ClassListData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.TransformData;
import com.l2jserver.gameserver.enums.actors.ClassId;
import com.l2jserver.gameserver.enums.actors.PlayerSex;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.PremiumManager;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.L2Playable;
import com.l2jserver.gameserver.model.actor.L2Summon;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.instance.L2PetInstance;
import com.l2jserver.gameserver.network.L2GameClient;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ExVoteSystemInfo;
import com.l2jserver.gameserver.network.serverpackets.GMViewItemList;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.network.serverpackets.PartySmallWindowAll;
import com.l2jserver.gameserver.network.serverpackets.PartySmallWindowDeleteAll;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.HtmlUtil;
import com.l2jserver.gameserver.util.Util;

/**
 * Edit player command.
 */
public class AdminEditChar implements IAdminCommandHandler
{
	private static final int PAGE_LIMIT = 20;
	
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_edit_character",
		"admin_current_player",
		"admin_setkarma", // sets karma of target char to any amount. //setkarma <karma>
		"admin_setfame", // sets fame of target char to any amount. //setfame <fame>
		"admin_character_list", // same as character_info, kept for compatibility purposes
		"admin_character_info", // given a player name, displays an information window
		"admin_show_characters", // list of characters
		"admin_find_character", // find a player by his name or a part of it (case-insensitive)
		"admin_find_ip", // find all the player connections from a given IPv4 number
		"admin_find_hwid", // find all the player connections from a given HWID number
		"admin_find_account", // list all the characters from an account (useful for GMs w/o DB access)
		"admin_find_dualbox", // list all the IPs with more than 1 char logged in (dualbox)
		"admin_strict_find_dualbox",
		"admin_tracert",
		"admin_rec", // gives recommendation points
		"admin_settitle", // changes char title
		"admin_changename", // changes char name
		"admin_setsex", // changes characters' sex
		"admin_setcolor", // change charnames' color display
		"admin_settcolor", // change char title color
		"admin_setclass", // changes chars' classId
		"admin_setpk", // changes PK count
		"admin_setpvp", // changes PVP count
		"admin_set_pvp_flag",
		"admin_fullfood", // fulfills a pet's food bar
		"admin_remove_clan_penalty", // removes clan penalties
		"admin_summon_info", // displays an information window about target summon
		"admin_unsummon",
		"admin_summon_setlvl",
		"admin_show_pet_inv",
		"admin_partyinfo",
		"admin_setnoble",
		"admin_set_hp",
		"admin_set_mp",
		"admin_set_cp",
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.equals("admin_current_player"))
		{
			showCharacterInfo(player, player);
		}
		else if (command.startsWith("admin_character_info"))
		{
			var data = command.split(" ");
			if ((data.length > 1))
			{
				showCharacterInfo(player, L2World.getInstance().getPlayer(data[1]));
			}
			else if (player.getTarget() instanceof L2PcInstance)
			{
				showCharacterInfo(player, player.getTarget().getActingPlayer());
			}
			else
			{
				player.sendPacket(SystemMessageId.INVALID_TARGET);
			}
		}
		else if (command.startsWith("admin_character_list"))
		{
			listCharacters(player, 0);
		}
		else if (command.startsWith("admin_show_characters"))
		{
			try
			{
				var val = command.substring(22);
				var page = Integer.parseInt(val);
				listCharacters(player, page);
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_show_characters"));
			}
		}
		else if (command.startsWith("admin_find_character"))
		{
			try
			{
				var val = command.substring(21);
				findCharacter(player, val);
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_find_character"));
				listCharacters(player, 0);
			}
		}
		else if (command.startsWith("admin_find_ip"))
		{
			try
			{
				var val = command.substring(14);
				findCharactersPerIp(player, val);
			}
			catch (Exception e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_find_ip"));
				listCharacters(player, 0);
			}
		}
		else if (command.startsWith("admin_find_hwid"))
		{
			try
			{
				var val = command.substring(14);
				findPlayersByHWID(player, val);
			}
			catch (Exception e)
			{
				// TODO: add Message
				listCharacters(player, 0);
			}
		}
		else if (command.startsWith("admin_find_account"))
		{
			try
			{
				var val = command.substring(19);
				findCharactersPerAccount(player, val);
			}
			catch (Exception e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_find_account"));
				listCharacters(player, 0);
			}
		}
		else if (command.startsWith("admin_edit_character"))
		{
			var data = command.split(" ");
			if ((data.length > 1))
			{
				editCharacter(player, data[1]);
			}
			else if (player.getTarget() instanceof L2PcInstance)
			{
				editCharacter(player, null);
			}
			else
			{
				player.sendPacket(SystemMessageId.INVALID_TARGET);
			}
		}
		else if (command.startsWith("admin_setkarma"))
		{
			try
			{
				setTargetKarma(player, Integer.parseInt(command.substring(15)));
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_setkarma"));
			}
		}
		else if (command.startsWith("admin_setpk"))
		{
			try
			{
				var val = command.substring(12);
				var pk = Integer.parseInt(val);
				var target = player.getTarget();
				if (target instanceof L2PcInstance)
				{
					var pl = (L2PcInstance) target;
					pl.setPkKills(pk);
					pl.broadcastUserInfo();
					pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_your_pk_count_changed").replace("%s%", pk + ""));
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_pk_count_changed").replace("%i%", pl.getName() + "").replace("%s%", pk + ""));
				}
				else
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_setpk"));
			}
		}
		else if (command.startsWith("admin_setpvp"))
		{
			try
			{
				var val = command.substring(13);
				var pvp = Integer.parseInt(val);
				var target = player.getTarget();
				if (target instanceof L2PcInstance)
				{
					var pl = (L2PcInstance) target;
					pl.setPvpKills(pvp);
					pl.broadcastUserInfo();
					pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_your_pvp_count_changed").replace("%s%", pvp + ""));
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_pvp_count_changed").replace("%i%", pl.getName() + "").replace("%s%", pvp + ""));
				}
				else
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_setpvp"));
			}
		}
		else if (command.startsWith("admin_setfame"))
		{
			try
			{
				var val = command.substring(14);
				var fame = Integer.parseInt(val);
				var target = player.getTarget();
				if (target instanceof L2PcInstance)
				{
					var pl = (L2PcInstance) target;
					pl.setFame(fame);
					pl.broadcastUserInfo();
					pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_your_reputation_points_count_changed").replace("%s%", fame + ""));
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_reputation_points_count_changed").replace("%i%", pl.getName() + "").replace("%s%", fame + ""));
				}
				else
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_setfame"));
			}
		}
		else if (command.startsWith("admin_rec"))
		{
			try
			{
				var val = command.substring(10);
				var recVal = Integer.parseInt(val);
				var target = player.getTarget();
				if (target instanceof L2PcInstance)
				{
					var pl = (L2PcInstance) target;
					pl.getRecSystem().setHave(recVal);
					pl.broadcastUserInfo();
					pl.sendPacket(new ExVoteSystemInfo(pl));
					pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_your_recommend_points_count_changed").replace("%s%", recVal + ""));
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_recommend_points_count_changed").replace("%i%", pl.getName() + "").replace("%s%", recVal + ""));
				}
				else
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
				}
			}
			catch (Exception e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_rec"));
			}
		}
		else if (command.startsWith("admin_setclass"))
		{
			try
			{
				var val = command.substring(15).trim();
				var classidval = Integer.parseInt(val);
				var target = player.getTarget();
				L2PcInstance pl = null;
				if (target instanceof L2PcInstance)
				{
					pl = (L2PcInstance) target;
				}
				else
				{
					return false;
				}
				var valid = false;
				for (var classid : ClassId.values())
				{
					if (classidval == classid.getId())
					{
						valid = true;
					}
				}
				if (valid && (pl.getClassId().getId() != classidval))
				{
					TransformData.getInstance().transformPlayer(255, pl);
					pl.setClassId(classidval);
					if (!pl.isSubClassActive())
					{
						pl.setBaseClass(classidval);
					}
					var newclass = ClassListData.getInstance().getClass(pl.getClassId()).getClassName();
					pl.storeMe();
					pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_your_class_changed").replace("%s%", newclass + ""));
					pl.untransform();
					pl.broadcastUserInfo();
					player.setTarget(null);
					player.setTarget(pl);
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_class_changed").replace("%i%", pl.getName() + "").replace("%s%", newclass + ""));
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_setclass"));
				}
			}
			catch (StringIndexOutOfBoundsException e)
			{
				AdminHtml.showAdminHtml(player, "submenu/setclass/human_fighter.htm");
			}
			catch (NumberFormatException e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_setclass"));
			}
		}
		else if (command.startsWith("admin_settitle"))
		{
			try
			{
				var val = command.substring(15);
				var target = player.getTarget();
				L2PcInstance pl = null;
				if (target instanceof L2PcInstance)
				{
					pl = (L2PcInstance) target;
				}
				else
				{
					return false;
				}
				pl.setTitle(val);
				pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_your_title_changed"));
				pl.broadcastTitleInfo();
			}
			catch (StringIndexOutOfBoundsException e)
			{ // Case of empty character title
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_title_changed"));
			}
		}
		else if (command.startsWith("admin_changename"))
		{
			try
			{
				var val = command.substring(17);
				var target = player.getTarget();
				L2PcInstance pl = null;
				if (target instanceof L2PcInstance)
				{
					pl = (L2PcInstance) target;
				}
				else
				{
					return false;
				}
				if (CharNameTable.getInstance().getIdByName(val) > 0)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_warning_player_already_exists").replace("%i%", val + ""));
					return false;
				}
				pl.setName(val);
				pl.storeMe();
				
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_name_changed").replace("%i%", val + ""));
				pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_your_name_changed"));
				pl.broadcastUserInfo();
				
				if (pl.isInParty())
				{
					pl.getParty().broadcastToPartyMembers(pl, PartySmallWindowDeleteAll.STATIC_PACKET);
					for (var member : pl.getParty().getMembers())
					{
						if (member != pl)
						{
							member.sendPacket(new PartySmallWindowAll(member, pl.getParty()));
						}
					}
				}
				if (pl.getClan() != null)
				{
					pl.getClan().broadcastClanStatus();
				}
			}
			catch (StringIndexOutOfBoundsException e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_setname"));
			}
		}
		else if (command.startsWith("admin_setsex"))
		{
			var target = player.getTarget();
			L2PcInstance pl = null;
			if (target instanceof L2PcInstance)
			{
				pl = (L2PcInstance) target;
			}
			else
			{
				return false;
			}
			
			var male = PlayerSex.MALE;
			var female = PlayerSex.FEMALE;
			
			if (male != pl.getAppearance().getSex())
			{
				pl.getAppearance().setSex(male);
				pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_your_gender_changed"));
				pl.decayMe();
				pl.spawnMe(pl.getX(), pl.getY(), pl.getZ());
				pl.broadcastUserInfo();
			}
			else
			{
				pl.getAppearance().setSex(female);
				pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_your_gender_changed"));
				pl.decayMe();
				pl.spawnMe(pl.getX(), pl.getY(), pl.getZ());
				pl.broadcastUserInfo();
			}
		}
		else if (command.startsWith("admin_setcolor"))
		{
			try
			{
				var val = command.substring(15);
				var target = player.getTarget();
				L2PcInstance pl = null;
				if (target instanceof L2PcInstance)
				{
					pl = (L2PcInstance) target;
				}
				else
				{
					return false;
				}
				pl.getAppearance().setNameColor(Integer.decode("0x" + val));
				pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_your_name_color_changed"));
				pl.broadcastUserInfo();
			}
			catch (Exception e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_need_specify_valid_new_color"));
			}
		}
		else if (command.startsWith("admin_settcolor"))
		{
			try
			{
				var val = command.substring(16);
				var target = player.getTarget();
				L2PcInstance pl = null;
				if (target instanceof L2PcInstance)
				{
					pl = (L2PcInstance) target;
				}
				else
				{
					return false;
				}
				pl.getAppearance().setTitleColor(Integer.decode("0x" + val));
				pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_your_title_color_changed"));
				pl.broadcastUserInfo();
			}
			catch (Exception e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_need_specify_valid_new_color"));
			}
		}
		else if (command.startsWith("admin_fullfood"))
		{
			var target = player.getTarget();
			if (target instanceof L2PetInstance)
			{
				var targetPet = (L2PetInstance) target;
				targetPet.setCurrentFed(targetPet.getMaxFed());
				targetPet.broadcastStatusUpdate();
			}
			else
			{
				player.sendPacket(SystemMessageId.INVALID_TARGET);
			}
		}
		else if (command.startsWith("admin_remove_clan_penalty"))
		{
			try
			{
				var st = new StringTokenizer(command, " ");
				if (st.countTokens() != 3)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_remove_clan_penalty"));
					return false;
				}
				
				st.nextToken();
				
				var changeCreateExpiryTime = st.nextToken().equalsIgnoreCase("create");
				
				var playerName = st.nextToken();
				L2PcInstance pl = null;
				pl = L2World.getInstance().getPlayer(playerName);
				
				if (pl == null)
				{
					var updateQuery = "UPDATE characters SET " + (changeCreateExpiryTime ? "clan_create_expiry_time" : "clan_join_expiry_time") + " WHERE char_name=? LIMIT 1";
					try (var con = ConnectionFactory.getInstance().getConnection();
						var ps = con.prepareStatement(updateQuery))
					{
						ps.setString(1, playerName);
						ps.execute();
					}
				}
				else
				{
					// removing penalty
					if (changeCreateExpiryTime)
					{
						pl.setClanCreateExpiryTime(0);
					}
					else
					{
						pl.setClanJoinExpiryTime(0);
					}
				}
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_clan_penalty_successfully_removed_character").replace("%i%", playerName + ""));
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else if (command.startsWith("admin_find_dualbox"))
		{
			var multibox = 2;
			try
			{
				var val = command.substring(19);
				multibox = Integer.parseInt(val);
				if (multibox < 1)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_find_dualbox"));
					return false;
				}
			}
			catch (Exception e)
			{
			}
			findDualbox(player, multibox);
		}
		else if (command.startsWith("admin_strict_find_dualbox"))
		{
			var multibox = 2;
			try
			{
				var val = command.substring(26);
				multibox = Integer.parseInt(val);
				if (multibox < 1)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_strict_find_dualbox"));
					return false;
				}
			}
			catch (Exception e)
			{
			}
			findDualboxStrict(player, multibox);
		}
		else if (command.startsWith("admin_tracert"))
		{
			var data = command.split(" ");
			L2PcInstance pl = null;
			if ((data.length > 1))
			{
				pl = L2World.getInstance().getPlayer(data[1]);
			}
			else
			{
				var target = player.getTarget();
				if (target instanceof L2PcInstance)
				{
					pl = (L2PcInstance) target;
				}
			}
			
			if (pl == null)
			{
				player.sendPacket(SystemMessageId.INVALID_TARGET);
				return false;
			}
			
			var client = pl.getClient();
			if (client == null)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_client_null"));
				return false;
			}
			
			if (client.isDetached())
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_client_detached"));
				return false;
			}
			
			String ip;
			var trace = client.getTrace();
			for (var i = 0; i < trace.length; i++)
			{
				ip = "";
				for (var o = 0; o < trace[0].length; o++)
				{
					ip = ip + trace[i][o];
					if (o != (trace[0].length - 1))
					{
						ip = ip + ".";
					}
				}
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_hop").replace("%i%", i + "").replace("%s%", ip + ""));
			}
		}
		else if (command.startsWith("admin_summon_info"))
		{
			var target = player.getTarget();
			if (target instanceof L2Summon)
			{
				gatherSummonInfo((L2Summon) target, player);
			}
			else
			{
				player.sendPacket(SystemMessageId.INVALID_TARGET);
			}
		}
		else if (command.startsWith("admin_unsummon"))
		{
			var target = player.getTarget();
			if (target instanceof L2Summon)
			{
				((L2Summon) target).unSummon(((L2Summon) target).getOwner());
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usable_only_with_pets_summons"));
			}
		}
		else if (command.startsWith("admin_summon_setlvl"))
		{
			var target = player.getTarget();
			if (target instanceof L2PetInstance)
			{
				var pet = (L2PetInstance) target;
				try
				{
					var val = command.substring(20);
					var level = Integer.parseInt(val);
					long newexp, oldexp = 0;
					oldexp = pet.getStat().getExp();
					newexp = pet.getStat().getExpForLevel(level);
					if (oldexp > newexp)
					{
						pet.getStat().removeExp(oldexp - newexp);
					}
					else if (oldexp < newexp)
					{
						pet.getStat().addExp(newexp - oldexp);
					}
				}
				catch (Exception ex)
				{
				}
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usable_only_with_pets"));
			}
		}
		else if (command.startsWith("admin_show_pet_inv"))
		{
			L2Object target;
			try
			{
				var val = command.substring(19);
				var objId = Integer.parseInt(val);
				target = L2World.getInstance().getPet(objId);
			}
			catch (Exception ex)
			{
				target = player.getTarget();
			}
			
			if (target instanceof L2PetInstance)
			{
				player.sendPacket(new GMViewItemList((L2PetInstance) target));
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usable_only_with_pets"));
			}
			
		}
		else if (command.startsWith("admin_partyinfo"))
		{
			L2Object target;
			try
			{
				var val = command.substring(16);
				target = L2World.getInstance().getPlayer(val);
				if (target == null)
				{
					target = player.getTarget();
				}
			}
			catch (Exception ex)
			{
				target = player.getTarget();
			}
			
			if (target instanceof L2PcInstance)
			{
				if (((L2PcInstance) target).isInParty())
				{
					gatherPartyInfo((L2PcInstance) target, player);
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_not_party"));
				}
			}
			else
			{
				player.sendPacket(SystemMessageId.INVALID_TARGET);
			}
		}
		else if (command.equals("admin_setnoble"))
		{
			L2PcInstance pl = null;
			if (player.getTarget() == null)
			{
				pl = player;
			}
			else if ((player.getTarget() != null) && (player.getTarget() instanceof L2PcInstance))
			{
				pl = (L2PcInstance) player.getTarget();
			}
			
			if (pl != null)
			{
				pl.setNoble(!pl.isNoble());
				if (pl.getObjectId() != player.getObjectId())
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_your_nobless_status_changed").replace("%i%", pl.getName() + ""));
				}
				pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_nobless_status_changed"));
			}
		}
		else if (command.startsWith("admin_set_hp"))
		{
			var data = command.split(" ");
			try
			{
				var target = player.getTarget();
				if ((target == null) || !target.isCharacter())
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
					return false;
				}
				((L2Character) target).setCurrentHp(Double.parseDouble(data[1]));
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_set_hp"));
			}
		}
		else if (command.startsWith("admin_set_mp"))
		{
			var data = command.split(" ");
			try
			{
				var target = player.getTarget();
				if ((target == null) || !target.isCharacter())
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
					return false;
				}
				((L2Character) target).setCurrentMp(Double.parseDouble(data[1]));
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_set_mp"));
			}
		}
		else if (command.startsWith("admin_set_cp"))
		{
			var data = command.split(" ");
			try
			{
				var target = player.getTarget();
				if ((target == null) || !target.isCharacter())
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
					return false;
				}
				((L2Character) target).setCurrentCp(Double.parseDouble(data[1]));
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_set_cp"));
			}
		}
		else if (command.startsWith("admin_set_pvp_flag"))
		{
			try
			{
				var target = player.getTarget();
				if ((target == null) || !target.isPlayable())
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
					return false;
				}
				var pl = ((L2Playable) target);
				pl.updatePvPFlag(Math.abs(pl.getPvpFlag() - 1));
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_set_pvp_flag"));
			}
		}
		return true;
	}
	
	private void listCharacters(L2PcInstance player, int page)
	{
		var players = L2World.getInstance().getPlayersSortedBy(Comparator.comparingLong(L2PcInstance::getUptime));
		
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/charlist.htm");
		
		var pr = HtmlUtil.createPage(players, page, PAGE_LIMIT, i ->
		{
			return "<td align=center><a action=\"bypass -h admin_show_characters " + i + "\">" + MessagesData.getInstance().getMessage(player, "admin_button_page") + "" + (i + 1) + "</a></td>";
		}, pl ->
		{
			var sb = new StringBuilder();
			sb.append("<tr>");
			sb.append("<td width=80><a action=\"bypass -h admin_character_info " + pl.getName() + "\">" + pl.getName() + "</a></td>");
			sb.append("<td width=110>" + ClassListData.getInstance().getClass(pl.getClassId()).getClientCode() + "</td><td width=40>" + pl.getLevel() + "</td>");
			sb.append("</tr>");
			return sb.toString();
		});
		
		if (pr.getPages() > 0)
		{
			html.replace("%pages%", "<table width=280 cellspacing=0><tr>" + pr.getPagerTemplate() + "</tr></table>");
		}
		else
		{
			html.replace("%pages%", "");
		}
		
		html.replace("%players%", pr.getBodyTemplate().toString());
		player.sendPacket(html);
	}
	
	private void showCharacterInfo(L2PcInstance player, L2PcInstance pl)
	{
		if (pl == null)
		{
			var target = player.getTarget();
			if (target instanceof L2PcInstance)
			{
				pl = (L2PcInstance) target;
			}
			else
			{
				return;
			}
		}
		else
		{
			player.setTarget(pl);
		}
		gatherCharacterInfo(player, pl, "charinfo.htm");
	}
	
	/**
	 * Retrieve and replace player's info in filename htm file, sends it to activeChar as NpcHtmlMessage.
	 * @param player
	 * @param pl
	 * @param filename
	 */
	private void gatherCharacterInfo(L2PcInstance player, L2PcInstance pl, String filename)
	{
		if (pl == null)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_player_null"));
			return;
		}
		
		var instant = Instant.ofEpochMilli(PremiumManager.getInstance().getPremiumExpiration(player.getAccountName()));
		
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/" + filename);
		
		html.replace("%name%", pl.getName());
		html.replace("%level%", String.valueOf(pl.getLevel()));
		html.replace("%clan%", String.valueOf(pl.getClan() != null ? "<a action=\"bypass -h admin_clan_info " + pl.getObjectId() + "\">" + pl.getClan().getName() + "</a>" : null));
		html.replace("%xp%", String.valueOf(pl.getExp()));
		html.replace("%sp%", String.valueOf(pl.getSp()));
		html.replace("%class%", ClassListData.getInstance().getClass(pl.getClassId()).getClientCode());
		html.replace("%ordinal%", String.valueOf(pl.getClassId().ordinal()));
		html.replace("%classid%", String.valueOf(pl.getClassId()));
		html.replace("%baseclass%", ClassListData.getInstance().getClass(pl.getBaseClass()).getClientCode());
		html.replace("%x%", String.valueOf(pl.getX()));
		html.replace("%y%", String.valueOf(pl.getY()));
		html.replace("%z%", String.valueOf(pl.getZ()));
		html.replace("%currenthp%", String.valueOf((int) pl.getCurrentHp()));
		html.replace("%maxhp%", String.valueOf(pl.getMaxHp()));
		html.replace("%karma%", String.valueOf(pl.getKarma()));
		html.replace("%currentmp%", String.valueOf((int) pl.getCurrentMp()));
		html.replace("%maxmp%", String.valueOf(pl.getMaxMp()));
		html.replace("%pvpflag%", String.valueOf(pl.getPvpFlag()));
		html.replace("%currentcp%", String.valueOf((int) pl.getCurrentCp()));
		html.replace("%maxcp%", String.valueOf(pl.getMaxCp()));
		html.replace("%pvpkills%", String.valueOf(pl.getPvpKills()));
		html.replace("%pkkills%", String.valueOf(pl.getPkKills()));
		html.replace("%currentload%", String.valueOf(pl.getCurrentLoad()));
		html.replace("%maxload%", String.valueOf(pl.getMaxLoad()));
		html.replace("%percent%", String.valueOf(Util.roundTo(((float) pl.getCurrentLoad() / (float) pl.getMaxLoad()) * 100, 2)));
		html.replace("%patk%", String.valueOf((int) pl.getPAtk(null)));
		html.replace("%matk%", String.valueOf((int) pl.getMAtk(null, null)));
		html.replace("%pdef%", String.valueOf((int) pl.getPDef(null)));
		html.replace("%mdef%", String.valueOf((int) pl.getMDef(null, null)));
		html.replace("%accuracy%", String.valueOf(pl.getAccuracy()));
		html.replace("%evasion%", String.valueOf(pl.getEvasionRate(null)));
		html.replace("%critical%", String.valueOf(pl.getCriticalHit(null, null)));
		html.replace("%runspeed%", String.valueOf((int) pl.getRunSpeed()));
		html.replace("%patkspd%", String.valueOf(pl.getPAtkSpd()));
		html.replace("%matkspd%", String.valueOf(pl.getMAtkSpd()));
		html.replace("%access%", pl.getAccessLevel().getLevel() + " (" + pl.getAccessLevel().getName() + ")");
		html.replace("%account%", pl.getAccountName());
		html.replace("%ip%", pl.getIPAddress());
		html.replace("%hwid%", pl.getHWID());
		html.replace("%ai%", String.valueOf(pl.getAI().getIntention().name()));
		html.replace("%inst%", pl.getInstanceId() > 0 ? "<tr><td>InstanceId:</td><td><a action=\"bypass -h admin_instance_spawns " + String.valueOf(pl.getInstanceId()) + "\">" + String.valueOf(pl.getInstanceId()) + "</a></td></tr>" : "");
		html.replace("%noblesse%", pl.isNoble() ? "" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + "");
		html.replace("%premium%", pl.getPcPremiumSystem().isActive() ? "" + MessagesData.getInstance().getMessage(player, "admin_htm_premium_date").replace("%i%", TimeUtils.dateTimeFormat(instant) + "") + "" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + "");
		player.sendPacket(html);
	}
	
	private void setTargetKarma(L2PcInstance player, int count)
	{
		// function to change karma of selected char
		var target = player.getTarget();
		L2PcInstance pl = null;
		if (target instanceof L2PcInstance)
		{
			pl = (L2PcInstance) target;
		}
		else
		{
			return;
		}
		
		if (count >= 0)
		{
			var oldKarma = pl.getKarma();
			pl.setKarma(count);
			SystemMessage sm = SystemMessage.getSystemMessage(SystemMessageId.YOUR_KARMA_HAS_BEEN_CHANGED_TO_S1);
			sm.addInt(count);
			pl.sendPacket(sm);
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_successfully_сhanged_karma_for_player").replace("%i%", pl.getName() + "").replace("%s%", oldKarma + "").replace("%t%", count + ""));
		}
		else
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_must_enter_value_for_karma_greater"));
		}
	}
	
	private void editCharacter(L2PcInstance player, String name)
	{
		L2Object target = null;
		if (name != null)
		{
			target = L2World.getInstance().getPlayer(name);
		}
		else
		{
			target = player.getTarget();
		}
		
		if (target instanceof L2PcInstance)
		{
			var pl = (L2PcInstance) target;
			gatherCharacterInfo(player, pl, "charedit.htm");
		}
	}
	
	private void findCharacter(L2PcInstance player, String character)
	{
		var CharactersFound = 0;
		String name;
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/charfind.htm");
		
		var sb = new StringBuilder(1000);
		
		for (var pl : L2World.getInstance().getPlayersSortedBy(Comparator.comparingLong(L2PcInstance::getUptime)))
		{ // Add player info into new Table row
			name = pl.getName();
			if (name.toLowerCase().contains(character.toLowerCase()))
			{
				CharactersFound = CharactersFound + 1;
				StringUtil.append(sb, "<tr><td width=80><a action=\"bypass -h admin_character_info ", name, "\">", name, "</a></td><td width=110>", ClassListData.getInstance().getClass(pl.getClassId()).getClientCode(), "</td><td width=40>", String.valueOf(pl.getLevel()), "</td></tr>");
			}
			if (CharactersFound > 20)
			{
				break;
			}
		}
		html.replace("%results%", sb.toString());
		
		final String msg;
		
		if (CharactersFound == 0)
		{
			msg = "" + MessagesData.getInstance().getMessage(player, "admin_htm_please_try_again") + "";
		}
		else if (CharactersFound > 20)
		{
			html.replace("%number%", " " + MessagesData.getInstance().getMessage(player, "admin_htm_more_than_20") + "");
			msg = "<br>" + MessagesData.getInstance().getMessage(player, "admin_htm_please_refine_your_search_see_all_the_results") + "";
		}
		else if (CharactersFound == 1)
		{
			msg = ".";
		}
		else
		{
			msg = "s.";
		}
		
		html.replace("%number%", String.valueOf(CharactersFound));
		html.replace("%end%", msg);
		player.sendPacket(html);
	}
	
	private void findCharactersPerIp(L2PcInstance player, String ipAdress) throws IllegalArgumentException
	{
		var findDisconnected = false;
		
		if (ipAdress.equals("disconnected"))
		{
			findDisconnected = true;
		}
		else
		{
			if (!ipAdress.matches("^(?:(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9][0-9]|2(?:[0-4][0-9]|5[0-5]))$"))
			{
				throw new IllegalArgumentException("" + MessagesData.getInstance().getMessage(player, "admin_htm_malformed_number") + "");
			}
		}
		
		var CharactersFound = 0;
		L2GameClient client;
		String name, ip = "0.0.0.0";
		var sb = new StringBuilder(1000);
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/ipfind.htm");
		
		for (var pl : L2World.getInstance().getPlayersSortedBy(Comparator.comparingLong(L2PcInstance::getUptime)))
		{
			client = pl.getClient();
			if (client == null)
			{
				continue;
			}
			
			if (client.isDetached())
			{
				if (!findDisconnected)
				{
					continue;
				}
			}
			else
			{
				if (findDisconnected)
				{
					continue;
				}
				
				ip = client.getConnection().getInetAddress().getHostAddress();
				if (!ip.equals(ipAdress))
				{
					continue;
				}
			}
			
			name = pl.getName();
			CharactersFound = CharactersFound + 1;
			StringUtil.append(sb, "<tr><td width=80><a action=\"bypass -h admin_character_info ", name, "\">", name, "</a></td><td width=110>", ClassListData.getInstance().getClass(pl.getClassId()).getClientCode(), "</td><td width=40>", String.valueOf(pl.getLevel()), "</td></tr>");
			
			if (CharactersFound > 20)
			{
				break;
			}
		}
		html.replace("%results%", sb.toString());
		
		final String replyMSG2;
		
		if (CharactersFound == 0)
		{
			replyMSG2 = "" + MessagesData.getInstance().getMessage(player, "admin_htm_maybe_they_got") + "";
		}
		else if (CharactersFound > 20)
		{
			html.replace("%number%", "" + MessagesData.getInstance().getMessage(player, "admin_htm_more_than").replace("%i%", String.valueOf(CharactersFound) + "") + " ");
			replyMSG2 = "<br>" + MessagesData.getInstance().getMessage(player, "admin_htm_in_order_avoid_you_client_crash") + "<br1>" + MessagesData.getInstance().getMessage(player, "admin_htm_display_results_beyond_20th") + "";
		}
		else if (CharactersFound == 1)
		{
			replyMSG2 = ".";
		}
		else
		{
			replyMSG2 = "s.";
		}
		html.replace("%ip%", ipAdress);
		html.replace("%number%", String.valueOf(CharactersFound));
		html.replace("%end%", replyMSG2);
		player.sendPacket(html);
	}
	
	private void findPlayersByHWID(L2PcInstance player, String searchHWID) throws IllegalArgumentException
	{
		var CharactersFound = 0;
		var sb = new StringBuilder(1000);
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/hwidfind.htm");
		
		for (var pl : L2World.getInstance().getPlayersSortedBy(Comparator.comparingLong(L2PcInstance::getUptime)))
		{
			var hwid = pl.getHWID();
			if (!hwid.equals(searchHWID))
			{
				continue;
			}
			
			var name = pl.getName();
			CharactersFound = CharactersFound + 1;
			StringUtil.append(sb, "<tr><td width=80><a action=\"bypass -h admin_character_info ", name, "\">", name, "</a></td><td width=110>", ClassListData.getInstance().getClass(pl.getClassId()).getClientCode(), "</td><td width=40>", String.valueOf(pl.getLevel()), "</td></tr>");
			
			if (CharactersFound > 20)
			{
				break;
			}
		}
		html.replace("%results%", sb.toString());
		
		final String replyMSG2;
		
		if (CharactersFound == 0)
		{
			replyMSG2 = "" + MessagesData.getInstance().getMessage(player, "admin_htm_maybe_they_got") + "";
		}
		else if (CharactersFound > 20)
		{
			html.replace("%number%", "" + MessagesData.getInstance().getMessage(player, "admin_htm_more_than").replace("%i%", String.valueOf(CharactersFound) + "") + " ");
			replyMSG2 = "<br>" + MessagesData.getInstance().getMessage(player, "admin_htm_in_order_avoid_you_client_crash") + "<br1>" + MessagesData.getInstance().getMessage(player, "admin_htm_display_results_beyond_20th") + "";
		}
		else if (CharactersFound == 1)
		{
			replyMSG2 = ".";
		}
		else
		{
			replyMSG2 = "s.";
		}
		html.replace("%hwid%", searchHWID);
		html.replace("%number%", String.valueOf(CharactersFound));
		html.replace("%end%", replyMSG2);
		player.sendPacket(html);
	}
	
	/**
	 * @param player
	 * @param characterName
	 * @throws IllegalArgumentException
	 */
	private void findCharactersPerAccount(L2PcInstance player, String character) throws IllegalArgumentException
	{
		var pl = L2World.getInstance().getPlayer(character);
		if (pl == null)
		{
			throw new IllegalArgumentException("" + MessagesData.getInstance().getMessage(player, "admin_htm_player_doesn_exist") + "");
		}
		
		var chars = pl.getAccountChars();
		var sb = new StringBuilder(chars.size() * 20);
		chars.values().stream().forEachOrdered(name -> StringUtil.append(sb, name, "<br1>"));
		
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/accountinfo.htm");
		
		html.replace("%account%", pl.getAccountName());
		html.replace("%player%", character);
		html.replace("%characters%", sb.toString());
		player.sendPacket(html);
	}
	
	/**
	 * @param player
	 * @param multibox
	 */
	private void findDualbox(L2PcInstance player, int multibox)
	{
		final Map<String, List<L2PcInstance>> ipMap = new HashMap<>();
		final Map<String, Integer> dualboxIPs = new HashMap<>();
		var ip = "0.0.0.0";
		L2GameClient client;
		
		for (var pl : L2World.getInstance().getPlayersSortedBy(Comparator.comparingLong(L2PcInstance::getUptime)))
		{
			client = pl.getClient();
			if ((client == null) || client.isDetached())
			{
				continue;
			}
			
			ip = client.getConnection().getInetAddress().getHostAddress();
			if (ipMap.get(ip) == null)
			{
				ipMap.put(ip, new ArrayList<>());
			}
			ipMap.get(ip).add(pl);
			
			if (ipMap.get(ip).size() >= multibox)
			{
				Integer count = dualboxIPs.get(ip);
				if (count == null)
				{
					dualboxIPs.put(ip, multibox);
				}
				else
				{
					dualboxIPs.put(ip, count + 1);
				}
			}
		}
		
		var keys = new ArrayList<>(dualboxIPs.keySet());
		keys.sort(Comparator.comparing(s -> dualboxIPs.get(s)).reversed());
		
		var sb = new StringBuilder();
		for (var dualboxIP : keys)
		{
			StringUtil.append(sb, "<a action=\"bypass -h admin_find_ip " + dualboxIP + "\">" + dualboxIP + " (" + dualboxIPs.get(dualboxIP) + ")</a><br1>");
		}
		
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/dualbox.htm");
		
		html.replace("%multibox%", String.valueOf(multibox));
		html.replace("%results%", sb.toString());
		html.replace("%strict%", "");
		player.sendPacket(html);
	}
	
	private void findDualboxStrict(L2PcInstance player, int multibox)
	{
		final Map<IpPack, List<L2PcInstance>> ipMap = new HashMap<>();
		final Map<IpPack, Integer> dualboxIPs = new HashMap<>();
		L2GameClient client;
		
		for (var pl : L2World.getInstance().getPlayersSortedBy(Comparator.comparingLong(L2PcInstance::getUptime)))
		{
			client = pl.getClient();
			if ((client == null) || client.isDetached())
			{
				continue;
			}
			
			var pack = new IpPack(client.getConnection().getInetAddress().getHostAddress(), client.getTrace());
			if (ipMap.get(pack) == null)
			{
				ipMap.put(pack, new ArrayList<>());
			}
			ipMap.get(pack).add(pl);
			
			if (ipMap.get(pack).size() >= multibox)
			{
				var count = dualboxIPs.get(pack);
				if (count == null)
				{
					dualboxIPs.put(pack, multibox);
				}
				else
				{
					dualboxIPs.put(pack, count + 1);
				}
			}
		}
		
		var keys = new ArrayList<>(dualboxIPs.keySet());
		keys.sort(Comparator.comparing(s -> dualboxIPs.get(s)).reversed());
		
		var sb = new StringBuilder();
		for (var dualboxIP : keys)
		{
			StringUtil.append(sb, "<a action=\"bypass -h admin_find_ip " + dualboxIP.getIp() + "\">" + dualboxIP.getIp() + " (" + dualboxIPs.get(dualboxIP) + ")</a><br1>");
		}
		
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/dualbox.htm");
		
		html.replace("%multibox%", String.valueOf(multibox));
		html.replace("%results%", sb.toString());
		html.replace("%strict%", "strict_");
		player.sendPacket(html);
	}
	
	private final class IpPack
	{
		private final String _ip;
		private final int[][] _tracert;
		
		public IpPack(String ip, int[][] tracert)
		{
			_ip = ip;
			_tracert = tracert;
		}
		
		@Override
		public int hashCode()
		{
			var prime = 31;
			var result = 1;
			result = (prime * result) + ((getIp() == null) ? 0 : getIp().hashCode());
			for (var array : getTracert())
			{
				result = (prime * result) + Arrays.hashCode(array);
			}
			return result;
		}
		
		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (obj == null)
			{
				return false;
			}
			if (getClass() != obj.getClass())
			{
				return false;
			}
			var other = (IpPack) obj;
			if (!getOuterType().equals(other.getOuterType()))
			{
				return false;
			}
			if (getIp() == null)
			{
				if (other.getIp() != null)
				{
					return false;
				}
			}
			else if (!getIp().equals(other.getIp()))
			{
				return false;
			}
			for (int i = 0; i < getTracert().length; i++)
			{
				for (int o = 0; o < getTracert()[0].length; o++)
				{
					if (getTracert()[i][o] != other.getTracert()[i][o])
					{
						return false;
					}
				}
			}
			return true;
		}
		
		private AdminEditChar getOuterType()
		{
			return AdminEditChar.this;
		}
		
		public String getIp()
		{
			return _ip;
		}
		
		public int[][] getTracert()
		{
			return _tracert;
		}
	}
	
	private void gatherSummonInfo(L2Summon target, L2PcInstance player)
	{
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/petinfo.htm");
		
		String name = target.getName();
		html.replace("%name%", name == null ? "" + MessagesData.getInstance().getMessage(player, "admin_htm_ip") + "" : name);
		html.replace("%level%", Integer.toString(target.getLevel()));
		html.replace("%exp%", Long.toString(target.getStat().getExp()));
		String owner = target.getActingPlayer().getName();
		html.replace("%owner%", " <a action=\"bypass -h admin_character_info " + owner + "\">" + owner + "</a>");
		html.replace("%class%", target.getClass().getSimpleName());
		html.replace("%ai%", target.hasAI() ? String.valueOf(target.getAI().getIntention().name()) : "NULL");
		html.replace("%hp%", (int) target.getStatus().getCurrentHp() + "/" + target.getStat().getMaxHp());
		html.replace("%mp%", (int) target.getStatus().getCurrentMp() + "/" + target.getStat().getMaxMp());
		html.replace("%karma%", Integer.toString(target.getKarma()));
		html.replace("%race%", target.getTemplate().getRace().toString());
		if (target instanceof L2PetInstance)
		{
			var objId = target.getActingPlayer().getObjectId();
			html.replace("%inv%", " <a action=\"bypass admin_show_pet_inv " + objId + "\">view</a>");
		}
		else
		{
			html.replace("%inv%", "" + MessagesData.getInstance().getMessage(player, "admin_htm_none") + "");
		}
		if (target instanceof L2PetInstance)
		{
			html.replace("%food%", ((L2PetInstance) target).getCurrentFed() + "/" + ((L2PetInstance) target).getPetLevelData().getPetMaxFeed());
			html.replace("%load%", ((L2PetInstance) target).getInventory().getTotalWeight() + "/" + ((L2PetInstance) target).getMaxLoad());
		}
		else
		{
			html.replace("%food%", "" + MessagesData.getInstance().getMessage(player, "admin_htm_ip") + "");
			html.replace("%load%", "" + MessagesData.getInstance().getMessage(player, "admin_htm_ip") + "");
		}
		player.sendPacket(html);
	}
	
	private void gatherPartyInfo(L2PcInstance target, L2PcInstance player)
	{
		var color = true;
		
		var sb = new StringBuilder(400);
		for (var member : target.getParty().getMembers())
		{
			if (color)
			{
				sb.append("<tr><td><table width=270 border=0 bgcolor=131210 cellpadding=2><tr><td width=30 align=right>");
			}
			else
			{
				sb.append("<tr><td><table width=270 border=0 cellpadding=2><tr><td width=30 align=right>");
			}
			StringUtil.append(sb, member.getLevel() + "</td><td width=130><a action=\"bypass -h admin_character_info " + member.getName() + "\">" + member.getName() + "</a>");
			sb.append("</td><td width=110 align=right>" + member.getClassId().toString() + "</td></tr></table></td></tr>");
			color = !color;
		}
		
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/partyinfo.htm");
		
		html.replace("%player%", target.getName());
		html.replace("%party%", sb.toString());
		player.sendPacket(html);
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}