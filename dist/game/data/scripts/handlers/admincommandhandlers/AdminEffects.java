/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.SkillData;
import com.l2jserver.gameserver.enums.events.Team;
import com.l2jserver.gameserver.enums.skills.AbnormalVisualEffect;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2ChestInstance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.Earthquake;
import com.l2jserver.gameserver.network.serverpackets.ExRedSky;
import com.l2jserver.gameserver.network.serverpackets.L2GameServerPacket;
import com.l2jserver.gameserver.network.serverpackets.MagicSkillUse;
import com.l2jserver.gameserver.network.serverpackets.PlaySound;
import com.l2jserver.gameserver.network.serverpackets.SSQInfo;
import com.l2jserver.gameserver.network.serverpackets.SocialAction;
import com.l2jserver.gameserver.network.serverpackets.SunRise;
import com.l2jserver.gameserver.network.serverpackets.SunSet;
import com.l2jserver.gameserver.util.Broadcast;

/**
 * Effects command.
 */
public class AdminEffects implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_invis",
		"admin_invisible",
		"admin_setinvis",
		"admin_vis",
		"admin_visible",
		"admin_invis_menu",
		"admin_earthquake",
		"admin_earthquake_menu",
		"admin_bighead",
		"admin_shrinkhead",
		"admin_gmspeed",
		"admin_gmspeed_menu",
		"admin_unpara_all",
		"admin_para_all",
		"admin_unpara",
		"admin_para",
		"admin_unpara_all_menu",
		"admin_para_all_menu",
		"admin_unpara_menu",
		"admin_para_menu",
		"admin_polyself",
		"admin_unpolyself",
		"admin_polyself_menu",
		"admin_unpolyself_menu",
		"admin_clearteams",
		"admin_setteam_close",
		"admin_setteam",
		"admin_social",
		"admin_effect",
		"admin_effect_menu",
		"admin_ave_abnormal",
		"admin_ave_special",
		"admin_ave_event",
		"admin_social_menu",
		"admin_play_sounds",
		"admin_play_sound",
		"admin_atmosphere",
		"admin_atmosphere_menu",
		"admin_set_displayeffect",
		"admin_set_displayeffect_menu"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command);
		st.nextToken();
		
		if (command.equals("admin_invis_menu"))
		{
			if (!player.isInvisible())
			{
				player.setInvisible(true);
				player.broadcastUserInfo();
				player.decayMe();
				player.spawnMe();
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_now_invisible"));
			}
			else
			{
				player.setInvisible(false);
				player.broadcastUserInfo();
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_now_visible"));
			}
			
			command = "";
			AdminHtml.showAdminHtml(player, "gm_menu.htm");
		}
		else if (command.startsWith("admin_invis"))
		{
			player.setInvisible(true);
			player.broadcastUserInfo();
			player.decayMe();
			player.spawnMe();
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_now_invisible"));
		}
		else if (command.startsWith("admin_vis"))
		{
			player.setInvisible(false);
			player.broadcastUserInfo();
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_now_visible"));
		}
		else if (command.startsWith("admin_setinvis"))
		{
			if ((player.getTarget() == null) || !player.getTarget().isCharacter())
			{
				player.sendPacket(SystemMessageId.INVALID_TARGET);
				return false;
			}
			var target = (L2Character) player.getTarget();
			target.setInvisible(!target.isInvisible());
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_made").replace("%i%", target.getName()
				+ "").replace("%i%", (target.isInvisible() ? "" + MessagesData.getInstance().getMessage(player, "admin_invisible") + "" : "" + MessagesData.getInstance().getMessage(player, "admin_visible") + "") + ""));
			
			if (target.isPlayer())
			{
				((L2PcInstance) target).broadcastUserInfo();
			}
		}
		else if (command.startsWith("admin_earthquake"))
		{
			try
			{
				var val1 = st.nextToken();
				var intensity = Integer.parseInt(val1);
				var val2 = st.nextToken();
				var duration = Integer.parseInt(val2);
				var eq = new Earthquake(player.getX(), player.getY(), player.getZ(), intensity, duration);
				player.broadcastPacket(eq);
			}
			catch (Exception e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_earthquake"));
			}
		}
		else if (command.startsWith("admin_atmosphere"))
		{
			try
			{
				var type = st.nextToken();
				var state = st.nextToken();
				var duration = Integer.parseInt(st.nextToken());
				adminAtmosphere(type, state, duration, player);
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_atmosphere"));
			}
		}
		else if (command.equals("admin_play_sounds"))
		{
			AdminHtml.showAdminHtml(player, "submenu/songs/songs.htm");
		}
		else if (command.startsWith("admin_play_sounds"))
		{
			try
			{
				AdminHtml.showAdminHtml(player, "submenu/songs/songs" + command.substring(18) + ".htm");
			}
			catch (StringIndexOutOfBoundsException e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_play_sounds_pagenumber"));
			}
		}
		else if (command.startsWith("admin_play_sound"))
		{
			try
			{
				playAdminSound(player, command.substring(17));
			}
			catch (StringIndexOutOfBoundsException e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_play_sounds_soundname"));
			}
		}
		else if (command.equals("admin_para_all"))
		{
			try
			{
				var plrs = player.getKnownList().getKnownPlayers().values();
				for (var pl : plrs)
				{
					if (!pl.isGM())
					{
						pl.startAbnormalVisualEffect(true, AbnormalVisualEffect.PARALYZE);
						pl.startStunning();
					}
				}
			}
			catch (Exception ex)
			{
			}
		}
		else if (command.equals("admin_unpara_all"))
		{
			try
			{
				var plrs = player.getKnownList().getKnownPlayers().values();
				for (var pl : plrs)
				{
					pl.stopAbnormalVisualEffect(true, AbnormalVisualEffect.PARALYZE);
					pl.stopStunning(false);
				}
			}
			catch (Exception ex)
			{
			}
		}
		else if (command.startsWith("admin_para")) // || command.startsWith("admin_para_menu"))
		{
			var type = "1";
			try
			{
				type = st.nextToken();
			}
			catch (Exception e)
			{
			}
			try
			{
				var target = player.getTarget();
				L2Character pl = null;
				if (target instanceof L2Character)
				{
					pl = (L2Character) target;
					if (type.equals("1"))
					{
						pl.startAbnormalVisualEffect(true, AbnormalVisualEffect.PARALYZE);
					}
					else
					{
						pl.startAbnormalVisualEffect(true, AbnormalVisualEffect.FLESH_STONE);
					}
					pl.startStunning();
				}
			}
			catch (Exception ex)
			{
			}
		}
		else if (command.startsWith("admin_unpara")) // || command.startsWith("admin_unpara_menu"))
		{
			var type = "1";
			try
			{
				type = st.nextToken();
			}
			catch (Exception ex)
			{
			}
			try
			{
				var target = player.getTarget();
				L2Character pl = null;
				if (target instanceof L2Character)
				{
					pl = (L2Character) target;
					if (type.equals("1"))
					{
						pl.stopAbnormalVisualEffect(true, AbnormalVisualEffect.PARALYZE);
					}
					else
					{
						pl.stopAbnormalVisualEffect(true, AbnormalVisualEffect.FLESH_STONE);
					}
					pl.stopStunning(false);
				}
			}
			catch (Exception ex)
			{
			}
		}
		else if (command.startsWith("admin_bighead"))
		{
			try
			{
				var target = player.getTarget();
				L2Character pl = null;
				if (target instanceof L2Character)
				{
					pl = (L2Character) target;
					pl.startAbnormalVisualEffect(true, AbnormalVisualEffect.BIG_HEAD);
				}
			}
			catch (Exception ex)
			{
			}
		}
		else if (command.startsWith("admin_shrinkhead"))
		{
			try
			{
				var target = player.getTarget();
				L2Character pl = null;
				if (target instanceof L2Character)
				{
					pl = (L2Character) target;
					pl.stopAbnormalVisualEffect(true, AbnormalVisualEffect.BIG_HEAD);
				}
			}
			catch (Exception ex)
			{
			}
		}
		else if (command.startsWith("admin_gmspeed"))
		{
			try
			{
				var val = Integer.parseInt(st.nextToken());
				var sendMessage = player.isAffectedBySkill(7029);
				player.stopSkillEffects((val == 0) && sendMessage, 7029);
				if ((val >= 1) && (val <= 4))
				{
					var gmSpeedSkill = SkillData.getInstance().getSkill(7029, val);
					player.doSimultaneousCast(gmSpeedSkill);
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_gmspeed"));
			}
			if (command.contains("_menu"))
			{
				command = "";
				AdminHtml.showAdminHtml(player, "gm_menu.htm");
			}
		}
		else if (command.startsWith("admin_polyself"))
		{
			try
			{
				var id = st.nextToken();
				player.getPoly().setPolyInfo("npc", id);
				player.teleToLocation(player.getLocation());
				player.broadcastUserInfo();
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_polyself"));
			}
		}
		else if (command.startsWith("admin_unpolyself"))
		{
			player.getPoly().setPolyInfo(null, "1");
			player.decayMe();
			player.spawnMe(player.getX(), player.getY(), player.getZ());
			player.broadcastUserInfo();
		}
		else if (command.equals("admin_clearteams"))
		{
			try
			{
				var plrs = player.getKnownList().getKnownPlayers().values();
				for (var pl : plrs)
				{
					pl.setTeam(Team.NONE);
					pl.broadcastUserInfo();
				}
			}
			catch (Exception e)
			{
			}
		}
		else if (command.startsWith("admin_setteam_close"))
		{
			try
			{
				var val = st.nextToken();
				var radius = 400;
				if (st.hasMoreTokens())
				{
					radius = Integer.parseInt(st.nextToken());
				}
				var team = Team.valueOf(val.toUpperCase());
				var plrs = player.getKnownList().getKnownCharactersInRadius(radius);
				
				for (var pl : plrs)
				{
					pl.setTeam(team);
				}
			}
			catch (Exception e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_setteam_close"));
			}
		}
		else if (command.startsWith("admin_setteam"))
		{
			try
			{
				var team = Team.valueOf(st.nextToken().toUpperCase());
				L2Character target = null;
				if (player.getTarget() instanceof L2Character)
				{
					target = (L2Character) player.getTarget();
				}
				else
				{
					return false;
				}
				target.setTeam(team);
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_setteam"));
			}
		}
		else if (command.startsWith("admin_social"))
		{
			try
			{
				String target = null;
				var obj = player.getTarget();
				if (st.countTokens() == 2)
				{
					var social = Integer.parseInt(st.nextToken());
					target = st.nextToken();
					if (target != null)
					{
						var pl = L2World.getInstance().getPlayer(target);
						if (pl != null)
						{
							if (performSocial(social, pl, player))
							{
								player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_player_affected_your_request").replace("%i%", pl.getName() + ""));
							}
						}
						else
						{
							try
							{
								var radius = Integer.parseInt(target);
								var objs = player.getKnownList().getKnownObjects().values();
								for (var object : objs)
								{
									if (player.isInsideRadius(object, radius, false, false))
									{
										performSocial(social, object, player);
									}
								}
								player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_units_radius_affected_your_request").replace("%i%", radius + ""));
							}
							catch (NumberFormatException nbe)
							{
								player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_invalid_parameter"));
							}
						}
					}
				}
				else if (st.countTokens() == 1)
				{
					var social = Integer.parseInt(st.nextToken());
					if (obj == null)
					{
						obj = player;
					}
					
					if (performSocial(social, obj, player))
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_player_affected_your_request").replace("%i%", obj.getName() + ""));
					}
					else
					{
						player.sendPacket(SystemMessageId.NOTHING_HAPPENED);
					}
				}
				else if (!command.contains("menu"))
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_social"));
				}
			}
			catch (Exception e)
			{
				
			}
		}
		else if (command.startsWith("admin_ave_abnormal") || command.startsWith("admin_ave_special") || command.startsWith("admin_ave_event"))
		{
			if (st.countTokens() > 0)
			{
				var param1 = st.nextToken();
				AbnormalVisualEffect ave;
				
				try
				{
					ave = AbnormalVisualEffect.valueOf(param1);
				}
				catch (Exception ex)
				{
					
					return false;
				}
				
				var radius = 0;
				String param2 = null;
				if (st.countTokens() == 1)
				{
					param2 = st.nextToken();
					if (StringUtil.isDigit(param2))
					{
						radius = Integer.parseInt(param2);
					}
				}
				
				if (radius > 0)
				{
					for (var object : player.getKnownList().getKnownObjects().values())
					{
						if (player.isInsideRadius(object, radius, false, false))
						{
							performAbnormalVisualEffect(ave, object);
						}
					}
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_affected_all_characters_radius_abnormal_visual_effect").replace("%i%", param2 + "").replace("%s%", param1 + ""));
				}
				else
				{
					var obj = player.getTarget() != null ? player.getTarget() : player;
					if (performAbnormalVisualEffect(ave, obj))
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_affected_abnormal_visual_effect").replace("%i%", obj.getName() + "").replace("%s%", param1 + ""));
					}
					else
					{
						player.sendPacket(SystemMessageId.NOTHING_HAPPENED);
					}
				}
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_abnormal_visual_effect").replace("%i%", command.replace("admin_", "") + ""));
			}
		}
		else if (command.startsWith("admin_effect"))
		{
			try
			{
				var obj = player.getTarget();
				int level = 1, hittime = 1;
				var skill = Integer.parseInt(st.nextToken());
				if (st.hasMoreTokens())
				{
					level = Integer.parseInt(st.nextToken());
				}
				if (st.hasMoreTokens())
				{
					hittime = Integer.parseInt(st.nextToken());
				}
				if (obj == null)
				{
					obj = player;
				}
				if (!(obj instanceof L2Character))
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
				}
				else
				{
					var target = (L2Character) obj;
					target.broadcastPacket(new MagicSkillUse(target, player, skill, level, hittime, 0));
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_performs_msu_your_request").replace("%i%", obj.getName() + "").replace("%s%", skill + "").replace("%t%", level + ""));
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_effect_skill"));
			}
		}
		else if (command.startsWith("admin_set_displayeffect"))
		{
			var target = player.getTarget();
			if (!(target instanceof L2Npc))
			{
				player.sendPacket(SystemMessageId.INVALID_TARGET);
				return false;
			}
			var npc = (L2Npc) target;
			try
			{
				var type = st.nextToken();
				var diplayeffect = Integer.parseInt(type);
				npc.setDisplayEffect(diplayeffect);
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_set_displayeffect"));
			}
		}
		
		if (command.contains("menu") || command.contains("ave_"))
		{
			showMainPage(player, command);
		}
		return true;
	}
	
	/**
	 * @param ave the abnormal visual effect
	 * @param target the target
	 * @return {@code true} if target's abnormal state was affected, {@code false} otherwise.
	 */
	private boolean performAbnormalVisualEffect(AbnormalVisualEffect ave, L2Object target)
	{
		if (target instanceof L2Character)
		{
			var character = (L2Character) target;
			if (character.hasAbnormalVisualEffect(ave))
			{
				character.stopAbnormalVisualEffect(true, ave);
			}
			else
			{
				character.startAbnormalVisualEffect(true, ave);
			}
			return true;
		}
		return false;
	}
	
	private boolean performSocial(int action, L2Object target, L2PcInstance player)
	{
		try
		{
			if (target instanceof L2Character)
			{
				if (target instanceof L2ChestInstance)
				{
					player.sendPacket(SystemMessageId.NOTHING_HAPPENED);
					return false;
				}
				if ((target instanceof L2Npc) && ((action < 1) || (action > 3)))
				{
					player.sendPacket(SystemMessageId.NOTHING_HAPPENED);
					return false;
				}
				if ((target instanceof L2PcInstance) && ((action < 2) || ((action > 18) && (action != SocialAction.LEVEL_UP))))
				{
					player.sendPacket(SystemMessageId.NOTHING_HAPPENED);
					return false;
				}
				var character = (L2Character) target;
				character.broadcastPacket(new SocialAction(character.getObjectId(), action));
			}
			else
			{
				return false;
			}
		}
		catch (Exception ex)
		{
		}
		return true;
	}
	
	/**
	 * @param type - atmosphere type (signssky,sky)
	 * @param state - atmosphere state(night,day)
	 * @param duration
	 * @param player
	 */
	private void adminAtmosphere(String type, String state, int duration, L2PcInstance player)
	{
		L2GameServerPacket packet = null;
		
		if (type.equals("ssqinfo"))
		{
			if (state.equals("dawn"))
			{
				packet = SSQInfo.DAWN_SKY_PACKET;
			}
			else if (state.equals("dusk"))
			{
				packet = SSQInfo.DUSK_SKY_PACKET;
			}
			else if (state.equals("red"))
			{
				packet = SSQInfo.RED_SKY_PACKET;
			}
			else if (state.equals("regular"))
			{
				packet = SSQInfo.REGULAR_SKY_PACKET;
			}
		}
		else if (type.equals("sky"))
		{
			if (state.equals("night"))
			{
				packet = SunSet.STATIC_PACKET;
			}
			else if (state.equals("day"))
			{
				packet = SunRise.STATIC_PACKET;
			}
			else if (state.equals("red"))
			{
				if (duration != 0)
				{
					packet = new ExRedSky(duration);
				}
				else
				{
					packet = new ExRedSky(10);
				}
			}
		}
		else
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_atmosphere"));
		}
		if (packet != null)
		{
			Broadcast.toAllOnlinePlayers(packet);
		}
	}
	
	private void playAdminSound(L2PcInstance player, String sound)
	{
		var _snd = PlaySound.createSound(sound);
		player.sendPacket(_snd);
		player.broadcastPacket(_snd);
		player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_playing").replace("%i%", sound + ""));
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
	
	private void showMainPage(L2PcInstance player, String command)
	{
		var filename = "effects_menu";
		if (command.contains("ave_abnormal"))
		{
			filename = "submenu/ave_abnormal";
		}
		else if (command.contains("ave_special"))
		{
			filename = "submenu/ave_special";
		}
		else if (command.contains("ave_event"))
		{
			filename = "submenu/ave_event";
		}
		else if (command.contains("social"))
		{
			filename = "submenu/social";
		}
		AdminHtml.showAdminHtml(player, filename + ".htm");
	}
}
