/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.Elementals;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.itemcontainer.Inventory;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.InventoryUpdate;

/**
 * Element command.
 */
public class AdminElement implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_setlh",
		"admin_setlc",
		"admin_setll",
		"admin_setlg",
		"admin_setlb",
		"admin_setlw",
		"admin_setls"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var armorType = -1;
		
		if (command.startsWith("admin_setlh"))
		{
			armorType = Inventory.PAPERDOLL_HEAD;
		}
		else if (command.startsWith("admin_setlc"))
		{
			armorType = Inventory.PAPERDOLL_CHEST;
		}
		else if (command.startsWith("admin_setlg"))
		{
			armorType = Inventory.PAPERDOLL_GLOVES;
		}
		else if (command.startsWith("admin_setlb"))
		{
			armorType = Inventory.PAPERDOLL_FEET;
		}
		else if (command.startsWith("admin_setll"))
		{
			armorType = Inventory.PAPERDOLL_LEGS;
		}
		else if (command.startsWith("admin_setlw"))
		{
			armorType = Inventory.PAPERDOLL_RHAND;
		}
		else if (command.startsWith("admin_setls"))
		{
			armorType = Inventory.PAPERDOLL_LHAND;
		}
		
		if (armorType != -1)
		{
			try
			{
				var args = command.split(" ");
				
				var element = Elementals.getElementId(args[1]);
				var value = Integer.parseInt(args[2]);
				if ((element < -1) || (element > 5) || (value < 0) || (value > 450))
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_setlh_1"));
					return false;
				}
				setElement(player, element, value, armorType);
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_setlh_2"));
				return false;
			}
		}
		return true;
	}
	
	private void setElement(L2PcInstance player, byte type, int value, int armor)
	{
		// get the target
		var object = player.getTarget();
		if (object == null)
		{
			object = player;
		}
		L2PcInstance target = null;
		if (object instanceof L2PcInstance)
		{
			target = (L2PcInstance) object;
		}
		else
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		
		L2ItemInstance item = null;
		
		// only attempt to enchant if there is a weapon equipped
		var parmorInstance = target.getInventory().getPaperdollItem(armor);
		if ((parmorInstance != null) && (parmorInstance.getLocationSlot() == armor))
		{
			item = parmorInstance;
		}
		
		if (item != null)
		{
			String old, current;
			var element = item.getElemental(type);
			if (element == null)
			{
				old = "" + MessagesData.getInstance().getMessage(player, "admin_htm_none") + "";
			}
			else
			{
				old = element.toString();
			}
			
			// set enchant value
			target.getInventory().unEquipItemInSlot(armor);
			if (type == -1)
			{
				item.clearElementAttr(type);
			}
			else
			{
				item.setElementAttr(type, value);
			}
			target.getInventory().equipItem(item);
			
			if (item.getElementals() == null)
			{
				current = "" + MessagesData.getInstance().getMessage(player, "admin_htm_none") + "";
			}
			else
			{
				current = item.getElemental(type).toString();
			}
			
			// send packets
			var iu = new InventoryUpdate();
			iu.addModifiedItem(item);
			target.sendPacket(iu);
			
			// informations
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_changed_elemental_power").replace("%i%", target.getName() + "").replace("%s%", item.getItem().getName() + "").replace("%t%", old + "").replace("%c%", current + ""));
			if (target != player)
			{
				target.sendMessage(MessagesData.getInstance().getMessage(player, "admin_has_changed_elemental_power_of_your").replace("%i%", player.getName() + "").replace("%s%", item.getItem().getName() + "").replace("%t%", old + "").replace("%c%", current + ""));
			}
		}
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
