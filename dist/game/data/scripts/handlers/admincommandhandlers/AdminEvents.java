/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.QuestManager;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.Event;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * Events command.
 */
public class AdminEvents implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_event_menu",
		"admin_event_start",
		"admin_event_stop",
		"admin_event_start_menu",
		"admin_event_stop_menu",
		"admin_event_bypass"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (player == null)
		{
			return false;
		}
		
		var name = "";
		var bypass = "";
		var st = new StringTokenizer(command, " ");
		st.nextToken();
		if (st.hasMoreTokens())
		{
			name = st.nextToken();
		}
		if (st.hasMoreTokens())
		{
			bypass = st.nextToken();
		}
		
		if (command.contains("_menu"))
		{
			showMenu(player);
		}
		
		if (command.startsWith("admin_event_start"))
		{
			try
			{
				if (name != null)
				{
					var event = (Event) QuestManager.getInstance().getQuest(name);
					if (event != null)
					{
						if (event.eventStart(player))
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_event_started").replace("%i%", name + ""));
							return true;
						}
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_event_problem_starting").replace("%i%", name + ""));
						return true;
					}
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_event_start"));
				ex.printStackTrace();
				return false;
			}
		}
		else if (command.startsWith("admin_event_stop"))
		{
			try
			{
				if (name != null)
				{
					var event = (Event) QuestManager.getInstance().getQuest(name);
					if (event != null)
					{
						if (event.eventStop())
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_event_stopped").replace("%i%", name + ""));
							return true;
						}
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_event_problem_stopped").replace("%i%", name + ""));
						return true;
					}
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_event_start"));
				ex.printStackTrace();
				return false;
			}
		}
		else if (command.startsWith("admin_event_bypass"))
		{
			try
			{
				if (name != null)
				{
					var event = (Event) QuestManager.getInstance().getQuest(name);
					if (event != null)
					{
						event.eventBypass(player, bypass);
					}
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_event_bypass"));
				ex.printStackTrace();
				return false;
			}
		}
		return false;
	}
	
	private void showMenu(L2PcInstance player)
	{
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/events/gm_events.htm");
		
		var sb = new StringBuilder(500);
		for (var event : QuestManager.getInstance().getScripts().values())
		{
			if (event instanceof Event)
			{
				StringUtil.append(sb, "<tr><td><font color=\"LEVEL\">", event.getName(), ":</font></td><br><td><button value=\"" + MessagesData.getInstance().getMessage(player, "admin_button_start")
					+ "\" action=\"bypass -h admin_event_start_menu ", event.getName(), "\" width=80 height=25 back=\"L2UI_CT1.ListCTRL_DF_Title_Down\" fore=\"L2UI_CT1.ListCTRL_DF_Title\"></td><td><button value=\"" + MessagesData.getInstance().getMessage(player, "admin_button_stop")
						+ "\" action=\"bypass -h admin_event_stop_menu ", event.getName() + "\" width=80 height=25 back=\"L2UI_CT1.ListCTRL_DF_Title_Down\" fore=\"L2UI_CT1.ListCTRL_DF_Title\"></td></tr>");
			}
		}
		html.replace("%LIST%", sb.toString());
		player.sendPacket(html);
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}