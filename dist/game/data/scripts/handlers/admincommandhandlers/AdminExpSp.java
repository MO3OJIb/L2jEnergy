/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.gameserver.data.xml.impl.ClassListData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * ExpSp command.
 */
public class AdminExpSp implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_add_exp_sp_to_character",
		"admin_add_exp_sp",
		"admin_remove_exp_sp"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.startsWith("admin_add_exp_sp"))
		{
			try
			{
				var val = command.substring(16);
				if (!adminAddExpSp(player, val))
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_add_exp_sp"));
				}
			}
			catch (StringIndexOutOfBoundsException e)
			{ // Case of missing parameter
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_add_exp_sp"));
			}
		}
		else if (command.startsWith("admin_remove_exp_sp"))
		{
			try
			{
				var val = command.substring(19);
				if (!adminRemoveExpSP(player, val))
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_remove_exp_sp"));
				}
			}
			catch (StringIndexOutOfBoundsException e)
			{ // Case of missing parameter
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_remove_exp_sp"));
			}
		}
		addExpSp(player);
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
	
	private void addExpSp(L2PcInstance player)
	{
		var target = player.getTarget();
		L2PcInstance pl = null;
		if (target instanceof L2PcInstance)
		{
			pl = (L2PcInstance) target;
		}
		else
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		
		var html = new NpcHtmlMessage();
		html.setFile(player, "data/html/admin/submenu/expsp.htm");
		
		html.replace("%name%", pl.getName());
		html.replace("%level%", String.valueOf(pl.getLevel()));
		html.replace("%xp%", String.valueOf(pl.getExp()));
		html.replace("%sp%", String.valueOf(pl.getSp()));
		html.replace("%class%", ClassListData.getInstance().getClass(pl.getClassId()).getClientCode());
		player.sendPacket(html);
	}
	
	private boolean adminAddExpSp(L2PcInstance player, String ExpSp)
	{
		var target = player.getTarget();
		L2PcInstance pl = null;
		if (target instanceof L2PcInstance)
		{
			pl = (L2PcInstance) target;
		}
		else
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return false;
		}
		var st = new StringTokenizer(ExpSp);
		if (st.countTokens() != 2)
		{
			return false;
		}
		
		var exp = st.nextToken();
		var sp = st.nextToken();
		long expval = 0;
		int spval = 0;
		try
		{
			expval = Long.parseLong(exp);
			spval = Integer.parseInt(sp);
		}
		catch (Exception ex)
		{
			return false;
		}
		if ((expval != 0) || (spval != 0))
		{
			pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_added_you_xp_and_sp").replace("%i%", expval + "").replace("%s%", spval + ""));
			pl.addExpAndSp(expval, spval);
			pl.broadcastUserInfo();
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_added_xp_and_sp").replace("%i%", expval + "").replace("%s%", spval + "").replace("%c%", pl.getName() + ""));
		}
		return true;
	}
	
	private boolean adminRemoveExpSP(L2PcInstance player, String ExpSp)
	{
		var target = player.getTarget();
		L2PcInstance pl = null;
		if (target instanceof L2PcInstance)
		{
			pl = (L2PcInstance) target;
		}
		else
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return false;
		}
		var st = new StringTokenizer(ExpSp);
		if (st.countTokens() != 2)
		{
			return false;
		}
		
		var exp = st.nextToken();
		var sp = st.nextToken();
		long expval = 0;
		var spval = 0;
		try
		{
			expval = Long.parseLong(exp);
			spval = Integer.parseInt(sp);
		}
		catch (Exception e)
		{
			return false;
		}
		if ((expval != 0) || (spval != 0))
		{
			pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_removing_you_xp_and_sp").replace("%i%", expval + "").replace("%s%", spval + ""));
			pl.removeExpAndSp(expval, spval);
			pl.broadcastUserInfo();
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_removed_xp_and_sp").replace("%i%", expval + "").replace("%s%", spval + "").replace("%c%", pl.getName() + ""));
		}
		return true;
	}
}
