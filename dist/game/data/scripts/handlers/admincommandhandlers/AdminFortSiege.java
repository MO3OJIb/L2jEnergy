/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.FortManager;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.Fort;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * Siege commands.
 * @author U3Games (rework)
 */
public class AdminFortSiege implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_fortsiege",
		"admin_add_fortattacker",
		"admin_list_fortsiege_clans",
		"admin_clear_fortsiege_list",
		"admin_spawn_fortdoors",
		"admin_endfortsiege",
		"admin_startfortsiege",
		"admin_setfort",
		"admin_removefort"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command, " ");
		command = st.nextToken(); // Get actual command
		
		// Get fort
		Fort fort = null;
		var fortId = 0;
		
		if (st.hasMoreTokens())
		{
			fortId = Integer.parseInt(st.nextToken());
			fort = FortManager.getInstance().getFortById(fortId);
		}
		
		// Check fort
		if (((fort == null) || (fortId == 0)))
		{
			// No fort specified
			showFortSelectPage(player);
		}
		else
		{
			L2PcInstance target = null;
			if ((player.getTarget() != null) && player.getTarget().isPlayer())
			{
				target = player.getTarget().getActingPlayer();
			}
			
			switch (command)
			{
				case "admin_add_fortattacker" ->
				{
					if (target == null)
					{
						player.sendPacket(SystemMessageId.TARGET_IS_INCORRECT);
					}
					else
					{
						if (fort.getSiege().addAttacker(target, false) == 4)
						{
							var sm = SystemMessage.getSystemMessage(SystemMessageId.REGISTERED_TO_S1_FORTRESS_BATTLE);
							sm.addCastleId(fort.getResidenceId());
							target.sendPacket(sm);
						}
						else
						{
							target.sendMessage(MessagesData.getInstance().getMessage(player, "admin_during_registering_error_occurred"));
						}
					}
				}
				case "admin_clear_fortsiege_list" -> fort.getSiege().clearSiegeClan();
				case "admin_endfortsiege" -> fort.getSiege().endSiege();
				case "admin_list_fortsiege_clans" -> player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_not_implemented_yet"));
				case "admin_setfort" ->
				{
					if ((target == null) || (target.getClan() == null))
					{
						player.sendPacket(SystemMessageId.TARGET_IS_INCORRECT);
					}
					else
					{
						fort.endOfSiege(target.getClan());
					}
				}
				case "admin_removefort" ->
				{
					var clan = fort.getOwnerClan();
					if (clan != null)
					{
						fort.removeOwner(true);
					}
					else
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unable_to_remove_fort"));
					}
				}
				case "admin_spawn_fortdoors" -> fort.resetDoors();
				case "admin_startfortsiege" -> fort.getSiege().startSiege();
			}
			showFortSiegePage(player, fort);
		}
		return true;
	}
	
	private void showFortSelectPage(L2PcInstance player)
	{
		var i = 0;
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/forts.htm");
		
		var forts = FortManager.getInstance().getForts();
		var sb = new StringBuilder(forts.size() * 100);
		
		for (var fort : forts)
		{
			if (fort != null)
			{
				StringUtil.append(sb, "<td fixwidth=90><a action=\"bypass -h admin_fortsiege ", fort.getResidenceId(), "\">", fort.getName(), " id: ", fort.getResidenceId(), "</a></td>");
				i++;
			}
			
			if (i > 2)
			{
				sb.append("</tr><tr>");
				i = 0;
			}
		}
		
		html.replace("%forts%", sb.toString());
		player.sendPacket(html);
	}
	
	private void showFortSiegePage(L2PcInstance player, Fort fort)
	{
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/fort.htm");
		
		html.replace("%fortName%", fort.getName());
		html.replace("%fortId%", String.valueOf(fort.getResidenceId()));
		player.sendPacket(html);
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}