/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.gameserver.GeoData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.util.GeoUtils;

/**
 * Geodata command.
 * @author -Nemesiss-
 * @author HorridoJoho
 */
public class AdminGeodata implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_geo_pos",
		"admin_geo_spawn_pos",
		"admin_geo_can_move",
		"admin_geo_can_see",
		"admin_geogrid",
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command, " ");
		var actualCommand = st.nextToken();
		switch (actualCommand.toLowerCase())
		{
			case "admin_geo_pos" ->
			{
				var worldX = player.getX();
				var worldY = player.getY();
				var worldZ = player.getZ();
				var geoX = GeoData.getInstance().getGeoX(worldX);
				var geoY = GeoData.getInstance().getGeoY(worldY);
				
				if (GeoData.getInstance().hasGeoPos(geoX, geoY))
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_world_x_y_z_geo_x_y_z").replace("%i%", worldX + "").replace("%s%", worldY + "").replace("%c%", worldZ + "").replace("%t%", geoX + "").replace("%y%", geoY
						+ "").replace("%z%", GeoData.getInstance().getNearestZ(geoX, geoY, worldZ) + ""));
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_there_no_geodata_position"));
				}
			}
			case "admin_geo_spawn_pos" ->
			{
				var worldX = player.getX();
				var worldY = player.getY();
				var worldZ = player.getZ();
				var geoX = GeoData.getInstance().getGeoX(worldX);
				var geoY = GeoData.getInstance().getGeoY(worldY);
				
				if (GeoData.getInstance().hasGeoPos(geoX, geoY))
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_world_x_y_z_geo_x_y_z").replace("%i%", worldX + "").replace("%s%", worldY + "").replace("%c%", worldZ + "").replace("%t%", geoX + "").replace("%y%", geoY
						+ "").replace("%z%", GeoData.getInstance().getSpawnHeight(worldX, worldY, worldZ) + ""));
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_there_no_geodata_position"));
				}
			}
			case "admin_geo_can_move" ->
			{
				var target = player.getTarget();
				if (target != null)
				{
					if (GeoData.getInstance().canSeeTarget(player, target))
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_can_move_beeline"));
					}
					else
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_can_not_move_beeline"));
					}
				}
				else
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
				}
			}
			case "admin_geo_can_see" ->
			{
				var target = player.getTarget();
				if (target != null)
				{
					if (GeoData.getInstance().canSeeTarget(player, target))
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_can_see_target"));
					}
					else
					{
						player.sendPacket(SystemMessageId.CANT_SEE_TARGET);
					}
				}
				else
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
				}
			}
			case "admin_geogrid" -> GeoUtils.debugGrid(player);
		}
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
