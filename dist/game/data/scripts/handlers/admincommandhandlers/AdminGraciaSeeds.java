/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.Calendar;
import java.util.StringTokenizer;

import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.GraciaSeedsManager;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * GraciaSeeds command.
 */
public class AdminGraciaSeeds implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_gracia_seeds",
		"admin_kill_tiat",
		"admin_kill_cohemenes",
		"admin_kill_twin",
		"admin_kill_ekimus",
		"admin_set_sodstate",
		"admin_set_soistate"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command, " ");
		var actualCommand = st.nextToken(); // Get actual command
		
		String val = "";
		if (st.countTokens() >= 1)
		{
			val = st.nextToken();
		}
		
		if (actualCommand.equalsIgnoreCase("admin_kill_tiat"))
		{
			GraciaSeedsManager.getInstance().increaseSoDTiatKilled();
		}
		else if (actualCommand.equalsIgnoreCase("admin_kill_cohemenes"))
		{
			GraciaSeedsManager.getInstance().increaseSoICohemenesKilled();
		}
		else if (actualCommand.equalsIgnoreCase("admin_kill_twin"))
		{
			GraciaSeedsManager.getInstance().increaseSoITwinKilled();
		}
		else if (actualCommand.equalsIgnoreCase("admin_kill_ekimus"))
		{
			GraciaSeedsManager.getInstance().increaseSoIEkimusKilled();
		}
		else if (actualCommand.equalsIgnoreCase("admin_set_sodstate"))
		{
			GraciaSeedsManager.getInstance().setSoDState(Integer.parseInt(val), true, Integer.parseInt(val) < 3);
		}
		else if (actualCommand.equalsIgnoreCase("admin_set_soistate"))
		{
			GraciaSeedsManager.getInstance().setSoIState(Integer.parseInt(val), true);
		}
		showMenu(player);
		return true;
	}
	
	private void showMenu(L2PcInstance player)
	{
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/graciaseeds.htm");
		
		html.replace("%sodstate%", String.valueOf(GraciaSeedsManager.getInstance().getSoDState()));
		html.replace("%sodtiatkill%", String.valueOf(GraciaSeedsManager.getInstance().getSoDTiatKilled()));
		if (GraciaSeedsManager.getInstance().getSoDTimeForNextStateChange() > 0)
		{
			var nextChangeDate = Calendar.getInstance();
			nextChangeDate.setTimeInMillis(System.currentTimeMillis() + GraciaSeedsManager.getInstance().getSoDTimeForNextStateChange());
			html.replace("%sodtime%", nextChangeDate.getTime().toString());
		}
		else
		{
			html.replace("%sodtime%", "-1");
		}
		html.replace("%soistate%", String.valueOf(GraciaSeedsManager.getInstance().getSoIState()));
		html.replace("%twinkill%", String.valueOf(GraciaSeedsManager.getInstance().getSoITwinKilled()));
		html.replace("%cohemeneskill%", String.valueOf(GraciaSeedsManager.getInstance().getSoICohemenesKilled()));
		html.replace("%ekimuskill%", String.valueOf(GraciaSeedsManager.getInstance().getSoIEkimusKilled()));
		if (GraciaSeedsManager.getInstance().getSoITimeForNextStateChange() > 0)
		{
			var nextChangeDate = Calendar.getInstance();
			nextChangeDate.setTimeInMillis(System.currentTimeMillis() + GraciaSeedsManager.getInstance().getSoITimeForNextStateChange());
			html.replace("%soitime%", nextChangeDate.getTime().toString());
		}
		else
		{
			html.replace("%soitime%", "-1");
		}
		player.sendPacket(html);
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
