/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.Arrays;
import java.util.StringTokenizer;

import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.GrandBossManager;
import com.l2jserver.gameserver.instancemanager.QuestManager;
import com.l2jserver.gameserver.instancemanager.ZoneManager;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.zone.type.L2NoRestartZone;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

import ai.individual.QueenAnt;
import ai.individual.Antharas.Antharas;
import ai.individual.Baium.Baium;

/**
 * GrandBoss command.
 * @author St3eT
 */
public class AdminGrandBoss implements IAdminCommandHandler
{
	private static final int ANTHARAS = 29068; // Antharas
	private static final int ANTHARAS_ZONE = 70050; // Antharas Nest
	private static final int VALAKAS = 29028; // Valakas
	private static final int BAIUM = 29020; // Baium
	private static final int BAIUM_ZONE = 70051; // Baium Nest
	private static final int QUEEN_ANT = 29001; // Queen Ant
	private static final int ORFEN = 29014; // Orfen
	private static final int CORE = 29006; // Core
	
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_grandboss",
		"admin_grandboss_skip",
		"admin_grandboss_respawn",
		"admin_grandboss_minions",
		"admin_grandboss_abort",
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command, " ");
		var actualCommand = st.nextToken();
		switch (actualCommand.toLowerCase())
		{
			case "admin_grandboss" ->
			{
				if (st.hasMoreTokens())
				{
					var grandBossId = Integer.parseInt(st.nextToken());
					manageHtml(player, grandBossId);
				}
				else
				{
					var html = new NpcHtmlMessage(0);
					html.setHtml(HtmCache.getInstance().getHtm(player, "data/html/admin/submenu/grandboss.htm"));
					player.sendPacket(html);
				}
			}
			case "admin_grandboss_skip" ->
			{
				if (st.hasMoreTokens())
				{
					var grandBossId = Integer.parseInt(st.nextToken());
					
					if (grandBossId == ANTHARAS)
					{
						getAntharasAI().notifyEvent("SKIP_WAITING", null, player);
						manageHtml(player, grandBossId);
					}
					else
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_wrong_id"));
					}
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_grandboss_skip"));
				}
			}
			case "admin_grandboss_respawn" ->
			{
				if (st.hasMoreTokens())
				{
					var grandBossId = Integer.parseInt(st.nextToken());
					
					switch (grandBossId)
					{
						case ANTHARAS ->
						{
							getAntharasAI().notifyEvent("RESPAWN_ANTHARAS", null, player);
							manageHtml(player, grandBossId);
						}
						case BAIUM ->
						{
							getBaiumAI().notifyEvent("RESPAWN_BAIUM", null, player);
							manageHtml(player, grandBossId);
						}
						case QUEEN_ANT ->
						{
							getQueenAntAI().notifyEvent("RESPAWN_QUEEN", null, player);
							manageHtml(player, grandBossId);
						}
						default -> player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_wrong_id"));
					}
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_grandboss_respawn"));
				}
			}
			case "admin_grandboss_minions" ->
			{
				if (st.hasMoreTokens())
				{
					var grandBossId = Integer.parseInt(st.nextToken());
					
					switch (grandBossId)
					{
						case ANTHARAS -> getAntharasAI().notifyEvent("DESPAWN_MINIONS", null, player);
						case BAIUM -> getBaiumAI().notifyEvent("DESPAWN_MINIONS", null, player);
						default -> player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_wrong_id"));
					}
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_grandboss_minions"));
				}
			}
			case "admin_grandboss_abort" ->
			{
				if (st.hasMoreTokens())
				{
					var grandBossId = Integer.parseInt(st.nextToken());
					
					switch (grandBossId)
					{
						case ANTHARAS ->
						{
							getAntharasAI().notifyEvent("ABORT_FIGHT", null, player);
							manageHtml(player, grandBossId);
						}
						case BAIUM ->
						{
							getBaiumAI().notifyEvent("ABORT_FIGHT", null, player);
							manageHtml(player, grandBossId);
						}
						default -> player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_wrong_id"));
					}
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_grandboss_abort"));
				}
			}
		}
		return true;
	}
	
	private void manageHtml(L2PcInstance player, int grandBossId)
	{
		if (Arrays.asList(ANTHARAS, VALAKAS, BAIUM, QUEEN_ANT, ORFEN, CORE).contains(grandBossId))
		{
			var bossStatus = GrandBossManager.getInstance().getBossStatus(grandBossId);
			L2NoRestartZone bossZone = null;
			String textColor = null;
			String text = null;
			String htmlPatch = null;
			var deadStatus = 0;
			
			switch (grandBossId)
			{
				case ANTHARAS ->
				{
					bossZone = ZoneManager.getInstance().getZoneById(ANTHARAS_ZONE, L2NoRestartZone.class);
					htmlPatch = "data/html/admin/submenu/grandboss/antharas.htm";
				}
				case VALAKAS -> htmlPatch = "data/html/admin/submenu/grandboss/valakas.htm";
				case BAIUM ->
				{
					bossZone = ZoneManager.getInstance().getZoneById(BAIUM_ZONE, L2NoRestartZone.class);
					htmlPatch = "data/html/admin/submenu/grandboss/baium.htm";
				}
				case QUEEN_ANT -> htmlPatch = "data/html/admin/submenu/grandboss/queenant.htm";
				case ORFEN -> htmlPatch = "data/html/admin/submenu/grandboss/orfen.htm";
				case CORE -> htmlPatch = "data/html/admin/submenu/grandboss/core.htm";
			}
			
			if (Arrays.asList(ANTHARAS, VALAKAS, BAIUM).contains(grandBossId))
			{
				deadStatus = 3;
				switch (bossStatus)
				{
					case 0 ->
					{
						textColor = "00FF00"; // Green
						text = "" + MessagesData.getInstance().getMessage(player, "html_alive") + "";
					}
					case 1 ->
					{
						textColor = "FFFF00"; // Yellow
						text = "" + MessagesData.getInstance().getMessage(player, "html_waiting") + "";
					}
					case 2 ->
					{
						textColor = "FF9900"; // Orange
						text = "" + MessagesData.getInstance().getMessage(player, "html_fight") + "";
					}
					case 3 ->
					{
						textColor = "FF0000"; // Red
						text = "" + MessagesData.getInstance().getMessage(player, "html_dead") + "";
					}
				}
			}
			else
			{
				deadStatus = 1;
				switch (bossStatus)
				{
					case 0 ->
					{
						textColor = "00FF00"; // Green
						text = "" + MessagesData.getInstance().getMessage(player, "html_alive") + "";
					}
					case 1 ->
					{
						textColor = "FF0000"; // Red
						text = "" + MessagesData.getInstance().getMessage(player, "html_dead") + "";
					}
				}
			}
			
			var info = GrandBossManager.getInstance().getStatsSet(grandBossId);
			var bossRespawn = TimeUtils.DATE_TIME_FORMATTER_FIX.format(info.getLong("respawn_time"));
			
			var html = new NpcHtmlMessage(0);
			html.setHtml(HtmCache.getInstance().getHtm(player, htmlPatch));
			
			html.replace("%bossStatus%", text);
			html.replace("%bossColor%", textColor);
			html.replace("%respawnTime%", bossStatus == deadStatus ? bossRespawn : "" + MessagesData.getInstance().getMessage(player, "html_respawned") + "");
			html.replace("%playersInside%", bossZone != null ? String.valueOf(bossZone.getPlayersInside().size()) : "" + MessagesData.getInstance().getMessage(player, "html_zone_not_found") + "");
			player.sendPacket(html);
		}
		else
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_wrong_id"));
		}
	}
	
	private Quest getAntharasAI()
	{
		return QuestManager.getInstance().getQuest(Antharas.class.getSimpleName());
	}
	
	private Quest getBaiumAI()
	{
		return QuestManager.getInstance().getQuest(Baium.class.getSimpleName());
	}
	
	private Quest getQueenAntAI()
	{
		return QuestManager.getInstance().getQuest(QueenAnt.class.getSimpleName());
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
