/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;

/**
 * Heal command.
 */
public class AdminHeal implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_heal"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.equals("admin_heal"))
		{
			handleHeal(player);
		}
		else if (command.startsWith("admin_heal"))
		{
			try
			{
				var healTarget = command.substring(11);
				handleHeal(player, healTarget);
			}
			catch (StringIndexOutOfBoundsException e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_incorrect_target_radius_specified"));
			}
		}
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
	
	private void handleHeal(L2PcInstance player)
	{
		handleHeal(player, null);
	}
	
	private void handleHeal(L2PcInstance player, String name)
	{
		
		var obj = player.getTarget();
		if (name != null)
		{
			var plyr = L2World.getInstance().getPlayer(name);
			
			if (plyr != null)
			{
				obj = plyr;
			}
			else
			{
				try
				{
					int radius = Integer.parseInt(name);
					var objs = player.getKnownList().getKnownObjects().values();
					for (var object : objs)
					{
						if (object instanceof L2Character)
						{
							var character = (L2Character) object;
							character.setCurrentHpMp(character.getMaxHp(), character.getMaxMp());
							if (object instanceof L2PcInstance)
							{
								character.setCurrentCp(character.getMaxCp());
							}
						}
					}
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_healed_within_unit_radius").replace("%i%", radius + ""));
					return;
				}
				catch (NumberFormatException nbe)
				{
				}
			}
		}
		if (obj == null)
		{
			obj = player;
		}
		if (obj instanceof L2Character)
		{
			var target = (L2Character) obj;
			target.setCurrentHpMp(target.getMaxHp(), target.getMaxMp());
			if (target instanceof L2PcInstance)
			{
				target.setCurrentCp(target.getMaxCp());
			}
		}
		else
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
		}
	}
}
