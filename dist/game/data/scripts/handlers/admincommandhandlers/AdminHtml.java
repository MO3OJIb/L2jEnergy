/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.io.File;
import java.util.StringTokenizer;

import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.ServerConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * Html command.
 * @author NosBit
 */
public class AdminHtml implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_html",
		"admin_loadhtml"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command, " ");
		var actualCommand = st.nextToken();
		switch (actualCommand.toLowerCase())
		{
			case "admin_html" ->
			{
				if (!st.hasMoreTokens())
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_html_path"));
					return false;
				}
				
				var path = st.nextToken();
				showAdminHtml(player, path);
			}
			case "admin_loadhtml" ->
			{
				if (!st.hasMoreTokens())
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_loadhtml_path"));
					return false;
				}
				
				var path = st.nextToken();
				showHtml(player, path, true);
			}
		}
		return true;
	}
	
	public static void showAdminHtml(L2PcInstance player, String path)
	{
		showHtml(player, "data/html/admin/" + path, false);
	}
	
	public static void showHtml(L2PcInstance player, String path, boolean reload)
	{
		String content = null;
		if (!reload)
		{
			content = HtmCache.getInstance().getHtm(player, path);
		}
		else
		{
			var file = new File(ServerConfig.DATAPACK_ROOT, path);
			content = HtmCache.getInstance().loadFile(file);
		}
		var html = new NpcHtmlMessage();
		if (content != null)
		{
			html.setHtml(content);
		}
		else
		{
			html.setHtml("<html><body>" + MessagesData.getInstance().getMessage(player, "admin_htm_text_missing") + "<br>" + path + "</body></html>");
		}
		player.sendPacket(html);
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
