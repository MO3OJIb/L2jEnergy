/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.InstanceManager;
import com.l2jserver.gameserver.model.actor.L2Summon;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;

/**
 * Instance command.
 * @author evill33t
 * @author GodKratos
 */
public class AdminInstance implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_setinstance",
		"admin_ghoston",
		"admin_ghostoff",
		"admin_createinstance",
		"admin_destroyinstance",
		"admin_listinstances"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command);
		st.nextToken();
		
		// create new instance
		if (command.startsWith("admin_createinstance"))
		{
			var parts = command.split(" ");
			if (parts.length != 3)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_create_instance"));
			}
			else
			{
				try
				{
					var id = Integer.parseInt(parts[1]);
					if ((id < 300000) && InstanceManager.getInstance().createInstanceFromTemplate(id, parts[2]))
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_instance_created"));
					}
					else
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_failed_create_instance"));
					}
					return true;
				}
				catch (Exception ex)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_failed_loading").replace("%i%", parts[1] + "").replace("%s%", parts[2] + ""));
					return false;
				}
			}
		}
		else if (command.startsWith("admin_listinstances"))
		{
			for (var temp : InstanceManager.getInstance().getInstances().values())
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_id_name_instance").replace("%i%", temp.getId() + "").replace("%s%", temp.getName() + ""));
			}
		}
		else if (command.startsWith("admin_setinstance"))
		{
			try
			{
				var val = Integer.parseInt(st.nextToken());
				if (InstanceManager.getInstance().getInstance(val) == null)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_instance_doesnt_exist").replace("%i%", val + ""));
					return false;
				}
				
				var target = player.getTarget();
				if ((target == null) || (target instanceof L2Summon)) // Don't separate summons from masters
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
					return false;
				}
				target.setInstanceId(val);
				if (target instanceof L2PcInstance)
				{
					var pl = (L2PcInstance) target;
					pl.sendMessage(MessagesData.getInstance().getMessage(pl, "admin_set_your_instance_to").replace("%i%", val + ""));
					pl.teleToLocation(pl.getLocation());
				}
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_moved_player_to_instance").replace("%i%", target.getName() + "").replace("%s%", target.getInstanceId() + ""));
				return true;
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_setinstance"));
			}
		}
		else if (command.startsWith("admin_destroyinstance"))
		{
			try
			{
				var val = Integer.parseInt(st.nextToken());
				InstanceManager.getInstance().destroyInstance(val);
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_instance_destroyed"));
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_destroyinstance"));
			}
		}
		
		// set ghost mode on aka not appearing on any knownlist
		// you will be invis to all players but you also dont get update packets ;)
		// you will see snapshots (knownlist echoes?) if you port
		// so kinda useless atm
		// TODO: enable broadcast packets for ghosts
		else if (command.startsWith("admin_ghoston"))
		{
			player.getAppearance().setGhostMode(true);
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_ghoston_mode_enabled"));
			player.broadcastUserInfo();
			player.decayMe();
			player.spawnMe();
		}
		// ghost mode off
		else if (command.startsWith("admin_ghostoff"))
		{
			player.getAppearance().setGhostMode(false);
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_ghoston_mode_disabled"));
			player.broadcastUserInfo();
			player.decayMe();
			player.spawnMe();
		}
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}