/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.InstanceManager;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * Instance Zone command.
 */
public class AdminInstanceZone implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_instancezone",
		"admin_instancezone_clear"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.startsWith("admin_instancezone_clear"))
		{
			try
			{
				var st = new StringTokenizer(command, " ");
				
				st.nextToken();
				var pl = L2World.getInstance().getPlayer(st.nextToken());
				var instanceId = Integer.parseInt(st.nextToken());
				var name = InstanceManager.getInstance().getInstanceIdName(instanceId);
				InstanceManager.getInstance().deleteInstanceTime(pl.getObjectId(), instanceId);
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_instancezone_cleared_for_player").replace("%i%", name + "").replace("%s%", pl.getName() + ""));
				pl.sendAdminMessage(MessagesData.getInstance().getMessage(pl, "player_cleared_instance_zone_for_you").replace("%i%", name + ""));
				return true;
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_instancezone_clear"));
				return false;
			}
		}
		else if (command.startsWith("admin_instancezone"))
		{
			var st = new StringTokenizer(command, " ");
			command = st.nextToken();
			
			if (st.hasMoreTokens())
			{
				L2PcInstance pl = null;
				var playername = st.nextToken();
				
				try
				{
					pl = L2World.getInstance().getPlayer(playername);
				}
				catch (Exception ex)
				{
				}
				
				if (pl != null)
				{
					display(pl, player);
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_player_is_not_online").replace("%i%", playername + ""));
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_instancezone"));
					return false;
				}
			}
			else if (player.getTarget() != null)
			{
				if (player.getTarget() instanceof L2PcInstance)
				{
					display((L2PcInstance) player.getTarget(), player);
				}
			}
			else
			{
				display(player, player);
			}
		}
		return true;
	}
	
	private void display(L2PcInstance pl, L2PcInstance player)
	{
		var instanceTimes = InstanceManager.getInstance().getAllInstanceTimes(pl.getObjectId());
		var sb = new StringBuilder(200 + (instanceTimes.size() * 100));
		StringUtil.append(sb, "<html><center><table width=270><tr>" + "<td width=45><button value=\"" + MessagesData.getInstance().getMessage(player, "admin_button_main") + "\" action=\"bypass -h admin_admin\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>"
			+ "<td width=180><center>Character Instances</center></td>" + "<td width=45><button value=\"" + MessagesData.getInstance().getMessage(player, "admin_button_back")
			+ "\" action=\"bypass -h admin_current_player\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>"
			+ "</tr></table><br><font color=\"LEVEL\">Instances for ", pl.getName(), "</font><center><br>" + "<table>" + "<tr><td width=150>Name</td><td width=50>Time</td><td width=70>Action</td></tr>");
		
		for (var id : instanceTimes.keySet())
		{
			var hours = 0;
			var minutes = 0;
			var remainingTime = (instanceTimes.get(id) - System.currentTimeMillis()) / 1000;
			if (remainingTime > 0)
			{
				hours = (int) (remainingTime / 3600);
				minutes = (int) ((remainingTime % 3600) / 60);
			}
			StringUtil.append(sb, "<tr><td>", InstanceManager.getInstance().getInstanceIdName(id), "</td><td>", hours, ":", minutes, "</td><td><button value=\"Clear\" action=\"bypass -h admin_instancezone_clear ", pl.getName(), " ", id, "\" width=60 height=15 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td></tr>");
		}
		
		StringUtil.append(sb, "</table></html>");
		var html = new NpcHtmlMessage(0);
		html.setHtml(sb.toString());
		player.sendPacket(html);
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}