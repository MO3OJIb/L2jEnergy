/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * Invul command.
 */
public class AdminInvul implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_invul",
		"admin_setinvul"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		
		if (command.equals("admin_invul"))
		{
			handleInvul(player);
			AdminHtml.showAdminHtml(player, "gm_menu.htm");
		}
		if (command.equals("admin_setinvul"))
		{
			var target = player.getTarget();
			if (target instanceof L2PcInstance)
			{
				handleInvul((L2PcInstance) target);
			}
		}
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
	
	private void handleInvul(L2PcInstance player)
	{
		String text;
		if (player.isInvul())
		{
			player.setIsInvul(false);
			text = MessagesData.getInstance().getMessage(player, "admin_player_now_mortal").replace("%i%", player.getName() + "");
		}
		else
		{
			player.setIsInvul(true);
			text = MessagesData.getInstance().getMessage(player, "admin_player_now_invulnerable").replace("%i%", player.getName() + "");
		}
		player.sendAdminMessage(text);
	}
}
