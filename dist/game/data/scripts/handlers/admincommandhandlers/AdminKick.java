/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * Kick command.
 */
public class AdminKick implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_kick",
		"admin_kick_non_gm"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.startsWith("admin_kick"))
		{
			var st = new StringTokenizer(command);
			if (st.countTokens() > 1)
			{
				st.nextToken();
				var pl = L2World.getInstance().getPlayer(st.nextToken());
				if (pl != null)
				{
					pl.logout();
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_kicked_from_game").replace("%i%", pl.getName() + ""));
				}
			}
		}
		if (command.startsWith("admin_kick_non_gm"))
		{
			var counter = 0;
			for (var pl : L2World.getInstance().getPlayers())
			{
				if (!pl.isGM())
				{
					counter++;
					pl.logout();
				}
			}
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_kicked_counter_players").replace("%i%", counter + ""));
		}
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
