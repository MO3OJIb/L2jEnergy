/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.gameserver.data.xml.impl.ChampionData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.L2Attackable;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2ControllableMobInstance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;

/**
 * Kill command.
 */
public class AdminKill implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_kill",
		"admin_kill_monster"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.startsWith("admin_kill"))
		{
			var st = new StringTokenizer(command, " ");
			st.nextToken(); // skip command
			
			if (st.hasMoreTokens())
			{
				var firstParam = st.nextToken();
				var pl = L2World.getInstance().getPlayer(firstParam);
				if (pl != null)
				{
					if (st.hasMoreTokens())
					{
						try
						{
							var radius = Integer.parseInt(st.nextToken());
							for (var knownChar : pl.getKnownList().getKnownCharactersInRadius(radius))
							{
								if ((knownChar instanceof L2ControllableMobInstance) || (knownChar == player))
								{
									continue;
								}
								
								kill(player, knownChar);
							}
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_killed_all_characters_within_unit_radius").replace("%i%", radius + ""));
							return true;
						}
						catch (NumberFormatException ex)
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_invalid_radius"));
							return false;
						}
					}
					kill(player, pl);
				}
				else
				{
					try
					{
						var radius = Integer.parseInt(firstParam);
						
						for (var knownChar : player.getKnownList().getKnownCharactersInRadius(radius))
						{
							if ((knownChar instanceof L2ControllableMobInstance) || (knownChar == player))
							{
								continue;
							}
							kill(player, knownChar);
						}
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_killed_all_characters_within_unit_radius").replace("%i%", radius + ""));
						return true;
					}
					catch (NumberFormatException ex)
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_kill"));
						return false;
					}
				}
			}
			else
			{
				var obj = player.getTarget();
				if ((obj instanceof L2ControllableMobInstance) || !(obj instanceof L2Character))
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
				}
				else
				{
					kill(player, (L2Character) obj);
				}
			}
		}
		return true;
	}
	
	private void kill(L2PcInstance player, L2Character target)
	{
		if (target instanceof L2PcInstance)
		{
			if (!((L2PcInstance) target).isGM())
			{
				target.stopAllEffects(); // e.g. invincibility effect
			}
			target.reduceCurrentHp(target.getMaxHp() + target.getMaxCp() + 1, player, null);
		}
		else if (ChampionData.getInstance().isEnabled() && target.isChampion())
		{
			target.reduceCurrentHp((target.getMaxHp() * ChampionData.getInstance().getHpMultipler((L2Attackable) target)) + 1, player, null);
		}
		else
		{
			var targetIsInvul = false;
			if (target.isInvul())
			{
				targetIsInvul = true;
				target.setIsInvul(false);
			}
			
			target.reduceCurrentHp(target.getMaxHp() + 1, player, null);
			
			if (targetIsInvul)
			{
				target.setIsInvul(true);
			}
		}
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
