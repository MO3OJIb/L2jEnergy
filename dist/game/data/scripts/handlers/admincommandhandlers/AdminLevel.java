/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.gameserver.data.json.ExperienceData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;

/**
 * Level command.
 */
public class AdminLevel implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_add_level",
		"admin_set_level"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var object = player.getTarget();
		var st = new StringTokenizer(command, " ");
		var actualCommand = st.nextToken(); // Get actual command
		
		String val = "";
		if (st.countTokens() >= 1)
		{
			val = st.nextToken();
		}
		
		if (actualCommand.equalsIgnoreCase("admin_add_level"))
		{
			try
			{
				if (object.isPlayer())
				{
					var target = (L2PcInstance) object;
					target.addLevel(Integer.parseInt(val));
				}
			}
			catch (NumberFormatException ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_wrong_number_format"));
			}
		}
		else if (actualCommand.equalsIgnoreCase("admin_set_level"))
		{
			if ((object == null) || !object.isPlayer())
			{
				player.sendPacket(SystemMessageId.TARGET_IS_INCORRECT);
				return false;
			}
			
			var target = object.getActingPlayer();
			
			try
			{
				var oldLevel = target.getLevel();
				var newLevel = Integer.parseInt(val);
				
				if (newLevel < 1)
				{
					newLevel = 1;
				}
				target.setLevel(newLevel);
				target.setExp(ExperienceData.getInstance().getExpForLevel(Math.min(newLevel, target.getMaxExpLevel())));
				target.onLevelChange(newLevel > oldLevel);
				target.broadcastInfo();
			}
			catch (NumberFormatException ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_level_require_number_value"));
				return false;
			}
		}
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
