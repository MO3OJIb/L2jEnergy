/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import com.l2jserver.gameserver.SevenSigns;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.AutoSpawnHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;

/**
 * Mammon command.
 */
public class AdminMammon implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_mammon_find",
		"admin_mammon_respawn",
	};
	
	private final boolean _isSealValidation = SevenSigns.getInstance().isSealValidationPeriod();
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var teleportIndex = -1;
		var blackSpawnInst = AutoSpawnHandler.getInstance().getAutoSpawnInstance(SevenSigns.MAMMON_BLACKSMITH_ID, false);
		var merchSpawnInst = AutoSpawnHandler.getInstance().getAutoSpawnInstance(SevenSigns.MAMMON_MERCHANT_ID, false);
		
		if (command.startsWith("admin_mammon_find"))
		{
			try
			{
				if (command.length() > 17)
				{
					teleportIndex = Integer.parseInt(command.substring(18));
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_mammon"));
				return false;
			}
			
			if (!_isSealValidation)
			{
				player.sendPacket(SystemMessageId.SSQ_COMPETITION_UNDERWAY);
				return false;
			}
			
			if (blackSpawnInst != null)
			{
				var blackInst = blackSpawnInst.getNPCInstanceList().peek();
				if (blackInst != null)
				{
					if (teleportIndex == 1)
					{
						player.teleToLocation(blackInst.getLocation(), true);
					}
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_coord_blacksmith_mammon").replace("%i%", blackInst.getX() + "").replace("%s%", blackInst.getY() + "").replace("%t%", blackInst.getZ() + ""));
				}
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_blacksmith_mammon_isnt_registered_spawn"));
			}
			
			if (merchSpawnInst != null)
			{
				var merchInst = merchSpawnInst.getNPCInstanceList().peek();
				if (merchInst != null)
				{
					if (teleportIndex == 2)
					{
						player.teleToLocation(merchInst.getLocation(), true);
					}
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_coord_merchant_mammon").replace("%i%", merchInst.getX() + "").replace("%s%", merchInst.getY() + "").replace("%t%", merchInst.getZ() + ""));
				}
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_merchant_mammon_isnt_registered_spawn"));
			}
		}
		else if (command.startsWith("admin_mammon_respawn"))
		{
			if (!_isSealValidation)
			{
				player.sendPacket(SystemMessageId.SSQ_COMPETITION_UNDERWAY);
				return true;
			}
			
			if (merchSpawnInst != null)
			{
				var merchRespawn = AutoSpawnHandler.getInstance().getTimeToNextSpawn(merchSpawnInst);
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_merchant_mammon_will_respawn").replace("%i%", (merchRespawn / 60000) + ""));
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_merchant_mammon_isnt_registered_spawn"));
			}
			
			if (blackSpawnInst != null)
			{
				var blackRespawn = AutoSpawnHandler.getInstance().getTimeToNextSpawn(blackSpawnInst);
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_blacksmith_mammon_will_respawn").replace("%i%", (blackRespawn / 60000) + ""));
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_blacksmith_mammon_isnt_registered_spawn"));
			}
		}
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
