/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.instancemanager.CastleManorManager;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * Manor command.
 */
public final class AdminManor implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_manor"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var manor = CastleManorManager.getInstance();
		var html = new NpcHtmlMessage();
		html.setFile(player, "data/html/admin/submenu/manor.htm");
		html.replace("%status%", manor.getCurrentModeName());
		html.replace("%change%", manor.getNextModeChange());
		
		var sb = new StringBuilder(3400);
		for (var c : CastleManager.getInstance().getCastles())
		{
			StringUtil.append(sb, "<tr><td> " + MessagesData.getInstance().getMessage(player, "admin_htm_name") + "</td><td><font color=008000>" + c.getName() + "</font></td></tr>");
			StringUtil.append(sb, "<tr><td>" + MessagesData.getInstance().getMessage(player, "admin_htm_current_period") + "</td><td><font color=FF9900>", StringUtil.formatNumber(manor.getManorCost(c.getResidenceId(), false)), " Adena</font></td></tr>");
			StringUtil.append(sb, "<tr><td>" + MessagesData.getInstance().getMessage(player, "admin_htm_next_period") + "</td><td><font color=FF9900>", StringUtil.formatNumber(manor.getManorCost(c.getResidenceId(), true)), " Adena</font></td></tr>");
			StringUtil.append(sb, "<tr><td><font color=808080>--------------------------</font></td><td><font color=808080>--------------------------</font></td></tr>");
		}
		html.replace("%castleInfo%", sb.toString());
		player.sendPacket(html);
		sb.setLength(0);
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}