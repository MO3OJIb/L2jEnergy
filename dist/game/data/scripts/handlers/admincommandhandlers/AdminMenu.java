/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.gameserver.data.xml.impl.AdminData;
import com.l2jserver.gameserver.data.xml.impl.ChampionData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.AdminCommandHandler;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Attackable;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;

/**
 * Menu command.
 */
public class AdminMenu implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_char_manage",
		"admin_teleport_character_to_menu",
		"admin_recall_char_menu",
		"admin_recall_party_menu",
		"admin_recall_clan_menu",
		"admin_goto_char_menu",
		"admin_kick_menu",
		"admin_kill_menu",
		"admin_ban_menu",
		"admin_unban_menu"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.equals("admin_char_manage"))
		{
			showMainPage(player);
		}
		else if (command.startsWith("admin_teleport_character_to_menu"))
		{
			var data = command.split(" ");
			if (data.length == 5)
			{
				var playerName = data[1];
				var pl = L2World.getInstance().getPlayer(playerName);
				if (pl != null)
				{
					teleportCharacter(pl, new Location(Integer.parseInt(data[2]), Integer.parseInt(data[3]), Integer.parseInt(data[4])), player, "admin_is_teleporting_you");
				}
			}
			showMainPage(player);
		}
		else if (command.startsWith("admin_recall_char_menu"))
		{
			try
			{
				var targetName = command.substring(23);
				var pl = L2World.getInstance().getPlayer(targetName);
				teleportCharacter(pl, player.getLocation(), player, "admin_is_teleporting_you");
			}
			catch (StringIndexOutOfBoundsException e)
			{
			}
		}
		else if (command.startsWith("admin_recall_party_menu"))
		{
			try
			{
				var targetName = command.substring(24);
				var pl = L2World.getInstance().getPlayer(targetName);
				if (pl == null)
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
					return true;
				}
				if (!pl.isInParty())
				{
					player.sendAdminMessage("Player is not in party.");
					teleportCharacter(pl, player.getLocation(), player, "admin_is_teleporting_you");
					return true;
				}
				for (var pm : pl.getParty().getMembers())
				{
					teleportCharacter(pm, player.getLocation(), player, "admin_your_party_being_teleported");
				}
			}
			catch (Exception ex)
			{
				ex.getMessage();
			}
		}
		else if (command.startsWith("admin_recall_clan_menu"))
		{
			try
			{
				var targetName = command.substring(23);
				var pl = L2World.getInstance().getPlayer(targetName);
				if (pl == null)
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
					return true;
				}
				var clan = pl.getClan();
				if (clan == null)
				{
					player.sendAdminMessage("Player is not in a clan.");
					teleportCharacter(pl, player.getLocation(), player, "admin_is_teleporting_you");
					return true;
				}
				
				for (var member : clan.getOnlineMembers(0))
				{
					teleportCharacter(member, player.getLocation(), player, "admin_your_clan_being_teleported");
				}
			}
			catch (Exception ex)
			{
				ex.getMessage();
			}
		}
		else if (command.startsWith("admin_goto_char_menu"))
		{
			try
			{
				var targetName = command.substring(21);
				var pl = L2World.getInstance().getPlayer(targetName);
				player.setInstanceId(pl.getInstanceId());
				teleportToCharacter(player, pl);
			}
			catch (StringIndexOutOfBoundsException e)
			{
			}
		}
		else if (command.equals("admin_kill_menu"))
		{
			handleKill(player);
		}
		else if (command.startsWith("admin_kick_menu"))
		{
			var st = new StringTokenizer(command);
			if (st.countTokens() > 1)
			{
				st.nextToken();
				var pl = L2World.getInstance().getPlayer(st.nextToken());
				String text;
				if (pl != null)
				{
					pl.logout();
					text = ("admin_you_kicked_player_from_game").replace("%i%", pl.getName() + "");
				}
				else
				{
					text = ("admin_player_was_not_found_the_game").replace("%i%", st.nextToken() + "");
				}
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, text));
			}
			showMainPage(player);
		}
		else if (command.startsWith("admin_ban_menu"))
		{
			var st = new StringTokenizer(command);
			if (st.countTokens() > 1)
			{
				var subCommand = "admin_ban_char";
				if (!AdminData.getInstance().hasAccess(subCommand, player.getAccessLevel()))
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_dont_have_access_right_use_this_command"));
					return false;
				}
				var ach = AdminCommandHandler.getInstance().getHandler(subCommand);
				ach.useAdminCommand(subCommand + command.substring(14), player);
			}
			showMainPage(player);
		}
		else if (command.startsWith("admin_unban_menu"))
		{
			var st = new StringTokenizer(command);
			if (st.countTokens() > 1)
			{
				var subCommand = "admin_unban_char";
				if (!AdminData.getInstance().hasAccess(subCommand, player.getAccessLevel()))
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_dont_have_access_right_use_this_command"));
					return false;
				}
				var ach = AdminCommandHandler.getInstance().getHandler(subCommand);
				ach.useAdminCommand(subCommand + command.substring(16), player);
			}
			showMainPage(player);
		}
		return true;
	}
	
	private void handleKill(L2PcInstance player)
	{
		handleKill(player, null);
	}
	
	private void handleKill(L2PcInstance activeChar, String player)
	{
		var obj = activeChar.getTarget();
		var target = (L2Character) obj;
		var filename = "main_menu.htm";
		if (player != null)
		{
			var pl = L2World.getInstance().getPlayer(player);
			if (pl != null)
			{
				target = pl;
				activeChar.sendAdminMessage("You killed " + pl.getName());
			}
		}
		if (target != null)
		{
			if (target instanceof L2PcInstance)
			{
				target.reduceCurrentHp(target.getMaxHp() + target.getMaxCp() + 1, activeChar, null);
				filename = "submenu/charmanage.htm";
			}
			else if (ChampionData.getInstance().isEnabled() && target.isChampion())
			{
				target.reduceCurrentHp((target.getMaxHp() * ChampionData.getInstance().getHpMultipler((L2Attackable) target)) + 1, activeChar, null);
			}
			else
			{
				target.reduceCurrentHp(target.getMaxHp() + 1, activeChar, null);
			}
		}
		else
		{
			activeChar.sendPacket(SystemMessageId.INVALID_TARGET);
		}
		AdminHtml.showAdminHtml(activeChar, filename);
	}
	
	private void teleportCharacter(L2PcInstance target, Location loc, L2PcInstance player, String message)
	{
		if (target != null)
		{
			target.sendMessage(MessagesData.getInstance().getMessage(target, message));
			target.teleToLocation(loc, true);
		}
		showMainPage(player);
	}
	
	private void teleportToCharacter(L2PcInstance player, L2Object object)
	{
		L2PcInstance target = null;
		if (object instanceof L2PcInstance)
		{
			target = (L2PcInstance) object;
		}
		else
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		if (target.getObjectId() == player.getObjectId())
		{
			target.sendPacket(SystemMessageId.YOU_CANNOT_USE_THIS_ON_YOURSELF);
		}
		else
		{
			player.setInstanceId(target.getInstanceId());
			player.teleToLocation(target.getLocation(), true);
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_teleporting_yourself_to_character").replace("%i%", target.getName() + ""));
		}
		showMainPage(player);
	}
	
	private void showMainPage(L2PcInstance player)
	{
		AdminHtml.showAdminHtml(player, "submenu/charmanage.htm");
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
