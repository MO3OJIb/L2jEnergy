/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import com.l2jserver.gameserver.data.xml.impl.NpcData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.MobGroup;
import com.l2jserver.gameserver.model.MobGroupTable;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.MagicSkillUse;
import com.l2jserver.gameserver.network.serverpackets.SetupGauge;
import com.l2jserver.gameserver.util.Broadcast;

/**
 * MobGroup command.
 * @author littlecrow
 */
public class AdminMobGroup implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_mobmenu",
		"admin_mobgroup_list",
		"admin_mobgroup_create",
		"admin_mobgroup_remove",
		"admin_mobgroup_delete",
		"admin_mobgroup_spawn",
		"admin_mobgroup_unspawn",
		"admin_mobgroup_kill",
		"admin_mobgroup_idle",
		"admin_mobgroup_attack",
		"admin_mobgroup_rnd",
		"admin_mobgroup_return",
		"admin_mobgroup_follow",
		"admin_mobgroup_casting",
		"admin_mobgroup_nomove",
		"admin_mobgroup_attackgrp",
		"admin_mobgroup_invul"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.equals("admin_mobmenu"))
		{
			showMainPage(player, command);
			return true;
		}
		else if (command.equals("admin_mobgroup_list"))
		{
			showGroupList(player);
		}
		else if (command.startsWith("admin_mobgroup_create"))
		{
			createGroup(command, player);
		}
		else if (command.startsWith("admin_mobgroup_delete") || command.startsWith("admin_mobgroup_remove"))
		{
			removeGroup(command, player);
		}
		else if (command.startsWith("admin_mobgroup_spawn"))
		{
			spawnGroup(command, player);
		}
		else if (command.startsWith("admin_mobgroup_unspawn"))
		{
			unspawnGroup(command, player);
		}
		else if (command.startsWith("admin_mobgroup_kill"))
		{
			killGroup(command, player);
		}
		else if (command.startsWith("admin_mobgroup_attackgrp"))
		{
			attackGrp(command, player);
		}
		else if (command.startsWith("admin_mobgroup_attack"))
		{
			if (player.getTarget() instanceof L2Character)
			{
				var target = (L2Character) player.getTarget();
				attack(command, player, target);
			}
		}
		else if (command.startsWith("admin_mobgroup_rnd"))
		{
			setNormal(command, player);
		}
		else if (command.startsWith("admin_mobgroup_idle"))
		{
			idle(command, player);
		}
		else if (command.startsWith("admin_mobgroup_return"))
		{
			returnToChar(command, player);
		}
		else if (command.startsWith("admin_mobgroup_follow"))
		{
			follow(command, player, player);
		}
		else if (command.startsWith("admin_mobgroup_casting"))
		{
			setCasting(command, player);
		}
		else if (command.startsWith("admin_mobgroup_nomove"))
		{
			noMove(command, player);
		}
		else if (command.startsWith("admin_mobgroup_invul"))
		{
			invul(command, player);
		}
		else if (command.startsWith("admin_mobgroup_teleport"))
		{
			teleportGroup(command, player);
		}
		showMainPage(player, command);
		return true;
	}
	
	private void showMainPage(L2PcInstance player, String command)
	{
		AdminHtml.showAdminHtml(player, "submenu/mobgroup.htm");
	}
	
	private void returnToChar(String command, L2PcInstance player)
	{
		int groupId;
		try
		{
			groupId = Integer.parseInt(command.split(" ")[1]);
		}
		catch (Exception ex)
		{
			player.sendAdminMessage("Incorrect command arguments.");
			return;
		}
		var group = MobGroupTable.getInstance().getGroup(groupId);
		if (group == null)
		{
			player.sendAdminMessage("Invalid group specified.");
			return;
		}
		group.returnGroup(player);
	}
	
	private void idle(String command, L2PcInstance player)
	{
		int groupId;
		
		try
		{
			groupId = Integer.parseInt(command.split(" ")[1]);
		}
		catch (Exception e)
		{
			player.sendAdminMessage("Incorrect command arguments.");
			return;
		}
		var group = MobGroupTable.getInstance().getGroup(groupId);
		if (group == null)
		{
			player.sendAdminMessage("Invalid group specified.");
			return;
		}
		group.setIdleMode();
	}
	
	private void setNormal(String command, L2PcInstance player)
	{
		int groupId;
		try
		{
			groupId = Integer.parseInt(command.split(" ")[1]);
		}
		catch (Exception e)
		{
			player.sendAdminMessage("Incorrect command arguments.");
			return;
		}
		var group = MobGroupTable.getInstance().getGroup(groupId);
		if (group == null)
		{
			player.sendAdminMessage("Invalid group specified.");
			return;
		}
		group.setAttackRandom();
	}
	
	private void attack(String command, L2PcInstance player, L2Character target)
	{
		int groupId;
		try
		{
			groupId = Integer.parseInt(command.split(" ")[1]);
		}
		catch (Exception e)
		{
			player.sendAdminMessage("Incorrect command arguments.");
			return;
		}
		var group = MobGroupTable.getInstance().getGroup(groupId);
		if (group == null)
		{
			player.sendAdminMessage("Invalid group specified.");
			return;
		}
		group.setAttackTarget(target);
	}
	
	private void follow(String command, L2PcInstance player, L2Character target)
	{
		int groupId;
		try
		{
			groupId = Integer.parseInt(command.split(" ")[1]);
		}
		catch (Exception e)
		{
			player.sendAdminMessage("Incorrect command arguments.");
			return;
		}
		var group = MobGroupTable.getInstance().getGroup(groupId);
		if (group == null)
		{
			player.sendAdminMessage("Invalid group specified.");
			return;
		}
		group.setFollowMode(target);
	}
	
	private void createGroup(String command, L2PcInstance player)
	{
		int groupId;
		int templateId;
		int mobCount;
		
		try
		{
			var cmdParams = command.split(" ");
			
			groupId = Integer.parseInt(cmdParams[1]);
			templateId = Integer.parseInt(cmdParams[2]);
			mobCount = Integer.parseInt(cmdParams[3]);
		}
		catch (Exception ex)
		{
			player.sendAdminMessage("Usage: //mobgroup_create <group> <npcid> <count>");
			return;
		}
		
		if (MobGroupTable.getInstance().getGroup(groupId) != null)
		{
			player.sendAdminMessage("Mob group " + groupId + " already exists.");
			return;
		}
		
		var template = NpcData.getInstance().getTemplate(templateId);
		
		if (template == null)
		{
			player.sendAdminMessage("Invalid NPC ID specified.");
			return;
		}
		
		var group = new MobGroup(groupId, template, mobCount);
		MobGroupTable.getInstance().addGroup(groupId, group);
		
		player.sendAdminMessage("Mob group " + groupId + " created.");
	}
	
	private void removeGroup(String command, L2PcInstance player)
	{
		int groupId;
		
		try
		{
			groupId = Integer.parseInt(command.split(" ")[1]);
		}
		catch (Exception e)
		{
			player.sendAdminMessage("Usage: //mobgroup_remove <groupId>");
			return;
		}
		
		var group = MobGroupTable.getInstance().getGroup(groupId);
		
		if (group == null)
		{
			player.sendAdminMessage("Invalid group specified.");
			return;
		}
		
		doAnimation(player);
		group.unspawnGroup();
		
		if (MobGroupTable.getInstance().removeGroup(groupId))
		{
			player.sendAdminMessage("Mob group " + groupId + " unspawned and removed.");
		}
	}
	
	private void spawnGroup(String command, L2PcInstance player)
	{
		int groupId;
		var topos = false;
		var posx = 0;
		var posy = 0;
		var posz = 0;
		
		try
		{
			var cmdParams = command.split(" ");
			groupId = Integer.parseInt(cmdParams[1]);
			
			try
			{ // we try to get a position
				posx = Integer.parseInt(cmdParams[2]);
				posy = Integer.parseInt(cmdParams[3]);
				posz = Integer.parseInt(cmdParams[4]);
				topos = true;
			}
			catch (Exception ex)
			{
				// no position given
			}
		}
		catch (Exception ex)
		{
			player.sendAdminMessage("Usage: //mobgroup_spawn <group> [ x y z ]");
			return;
		}
		
		var group = MobGroupTable.getInstance().getGroup(groupId);
		if (group == null)
		{
			player.sendAdminMessage("Invalid group specified.");
			return;
		}
		
		doAnimation(player);
		
		if (topos)
		{
			group.spawnGroup(posx, posy, posz);
		}
		else
		{
			group.spawnGroup(player);
		}
		
		player.sendAdminMessage("Mob group " + groupId + " spawned.");
	}
	
	private void unspawnGroup(String command, L2PcInstance player)
	{
		int groupId;
		
		try
		{
			groupId = Integer.parseInt(command.split(" ")[1]);
		}
		catch (Exception e)
		{
			player.sendAdminMessage("Usage: //mobgroup_unspawn <groupId>");
			return;
		}
		
		var group = MobGroupTable.getInstance().getGroup(groupId);
		
		if (group == null)
		{
			player.sendAdminMessage("Invalid group specified.");
			return;
		}
		
		doAnimation(player);
		group.unspawnGroup();
		
		player.sendAdminMessage("Mob group " + groupId + " unspawned.");
	}
	
	private void killGroup(String command, L2PcInstance player)
	{
		int groupId;
		
		try
		{
			groupId = Integer.parseInt(command.split(" ")[1]);
		}
		catch (Exception e)
		{
			player.sendAdminMessage("Usage: //mobgroup_kill <groupId>");
			return;
		}
		
		var group = MobGroupTable.getInstance().getGroup(groupId);
		
		if (group == null)
		{
			player.sendAdminMessage("Invalid group specified.");
			return;
		}
		
		doAnimation(player);
		group.killGroup(player);
	}
	
	private void setCasting(String command, L2PcInstance player)
	{
		int groupId;
		
		try
		{
			groupId = Integer.parseInt(command.split(" ")[1]);
		}
		catch (Exception e)
		{
			player.sendAdminMessage("Usage: //mobgroup_casting <groupId>");
			return;
		}
		
		var group = MobGroupTable.getInstance().getGroup(groupId);
		
		if (group == null)
		{
			player.sendAdminMessage("Invalid group specified.");
			return;
		}
		
		group.setCastMode();
	}
	
	private void noMove(String command, L2PcInstance player)
	{
		int groupId;
		String enabled;
		
		try
		{
			groupId = Integer.parseInt(command.split(" ")[1]);
			enabled = command.split(" ")[2];
		}
		catch (Exception e)
		{
			player.sendAdminMessage("Usage: //mobgroup_nomove <groupId> <on|off>");
			return;
		}
		
		var group = MobGroupTable.getInstance().getGroup(groupId);
		if (group == null)
		{
			player.sendAdminMessage("Invalid group specified.");
			return;
		}
		
		if (enabled.equalsIgnoreCase("on") || enabled.equalsIgnoreCase("true"))
		{
			group.setNoMoveMode(true);
		}
		else if (enabled.equalsIgnoreCase("off") || enabled.equalsIgnoreCase("false"))
		{
			group.setNoMoveMode(false);
		}
		else
		{
			player.sendAdminMessage("Incorrect command arguments.");
		}
	}
	
	private void doAnimation(L2PcInstance player)
	{
		Broadcast.toSelfAndKnownPlayersInRadius(player, new MagicSkillUse(player, 1008, 1, 4000, 0), 1500);
		player.sendPacket(new SetupGauge(0, 4000));
	}
	
	private void attackGrp(String command, L2PcInstance player)
	{
		int groupId;
		int othGroupId;
		
		try
		{
			groupId = Integer.parseInt(command.split(" ")[1]);
			othGroupId = Integer.parseInt(command.split(" ")[2]);
		}
		catch (Exception e)
		{
			player.sendAdminMessage("Usage: //mobgroup_attackgrp <groupId> <TargetGroupId>");
			return;
		}
		
		var group = MobGroupTable.getInstance().getGroup(groupId);
		
		if (group == null)
		{
			player.sendAdminMessage("Invalid group specified.");
			return;
		}
		
		var othGroup = MobGroupTable.getInstance().getGroup(othGroupId);
		
		if (othGroup == null)
		{
			player.sendAdminMessage("Incorrect target group.");
			return;
		}
		
		group.setAttackGroup(othGroup);
	}
	
	private void invul(String command, L2PcInstance player)
	{
		int groupId;
		String enabled;
		
		try
		{
			groupId = Integer.parseInt(command.split(" ")[1]);
			enabled = command.split(" ")[2];
		}
		catch (Exception e)
		{
			player.sendAdminMessage("Usage: //mobgroup_invul <groupId> <on|off>");
			return;
		}
		
		var group = MobGroupTable.getInstance().getGroup(groupId);
		
		if (group == null)
		{
			player.sendAdminMessage("Invalid group specified.");
			return;
		}
		
		if (enabled.equalsIgnoreCase("on") || enabled.equalsIgnoreCase("true"))
		{
			group.setInvul(true);
		}
		else if (enabled.equalsIgnoreCase("off") || enabled.equalsIgnoreCase("false"))
		{
			group.setInvul(false);
		}
		else
		{
			player.sendAdminMessage("Incorrect command arguments.");
		}
	}
	
	private void teleportGroup(String command, L2PcInstance player)
	{
		int groupId;
		String targetPlayerStr = null;
		L2PcInstance targetPlayer = null;
		
		try
		{
			groupId = Integer.parseInt(command.split(" ")[1]);
			targetPlayerStr = command.split(" ")[2];
			
			if (targetPlayerStr != null)
			{
				targetPlayer = L2World.getInstance().getPlayer(targetPlayerStr);
			}
			
			if (targetPlayer == null)
			{
				targetPlayer = player;
			}
		}
		catch (Exception ex)
		{
			player.sendAdminMessage("Usage: //mobgroup_teleport <groupId> [playerName]");
			return;
		}
		
		var group = MobGroupTable.getInstance().getGroup(groupId);
		
		if (group == null)
		{
			player.sendAdminMessage("Invalid group specified.");
			return;
		}
		
		group.teleportGroup(player);
	}
	
	private void showGroupList(L2PcInstance player)
	{
		var mobGroupList = MobGroupTable.getInstance().getGroups();
		
		player.sendAdminMessage("======= <Mob Groups> =======");
		
		for (var mobGroup : mobGroupList)
		{
			player.sendMessage(mobGroup.getGroupId() + ": " + mobGroup.getActiveMobCount() + " alive out of " + mobGroup.getMaxMobCount() + " of NPC ID " + mobGroup.getTemplate().getId() + " (" + mobGroup.getStatus() + ")");
		}
		player.sendPacket(SystemMessageId.FRIEND_LIST_FOOTER);
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}