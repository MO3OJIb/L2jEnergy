/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.Arrays;
import java.util.List;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.enums.audio.Music;
import com.l2jserver.gameserver.enums.audio.Sound;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.games.MonsterRace;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.DeleteObject;
import com.l2jserver.gameserver.network.serverpackets.MonRaceInfo;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * MonsterRace command.
 */
public class AdminMonsterRace implements IAdminCommandHandler
{
	private static final List<List<Integer>> CODES = Arrays.asList(Arrays.asList(-1, 0), Arrays.asList(0, 15322), Arrays.asList(13765, -1), Arrays.asList(-1, 0));
	
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_mons"
	};
	
	protected static int _state = -1;
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.equalsIgnoreCase("admin_mons"))
		{
			handleSendPacket(player);
		}
		return true;
	}
	
	private void handleSendPacket(L2PcInstance player)
	{
		var race = MonsterRace.getInstance();
		
		if (_state == -1)
		{
			_state++;
			race.newRace();
			race.calculateLaneSpeeds();
			MonRaceInfo spk = new MonRaceInfo(CODES.get(_state).get(0), CODES.get(_state).get(1), race.getMonsters(), race.getSpeeds());
			player.sendPacket(spk);
			player.broadcastPacket(spk);
		}
		else if (_state == 0)
		{
			_state++;
			var sm = SystemMessage.getSystemMessage(SystemMessageId.THEY_RE_OFF);
			sm.addInt(0);
			player.sendPacket(sm);
			var SRace = Music.S_RACE.getPacket();
			player.sendPacket(SRace);
			player.broadcastPacket(SRace);
			var SRace2 = Sound.ITEMSOUND2_RACE_START.getPacket();
			player.sendPacket(SRace2);
			player.broadcastPacket(SRace2);
			var spk = new MonRaceInfo(CODES.get(_state).get(0), CODES.get(_state).get(1), race.getMonsters(), race.getSpeeds());
			player.sendPacket(spk);
			player.broadcastPacket(spk);
			ThreadPoolManager.getInstance().scheduleGeneral(new RunRace(CODES, player), 5000);
		}
	}
	
	private class RunRace implements Runnable
	{
		private final List<List<Integer>> _codes;
		private final L2PcInstance _player;
		
		public RunRace(List<List<Integer>> pCodes, L2PcInstance pl)
		{
			_codes = pCodes;
			_player = pl;
		}
		
		@Override
		public void run()
		{
			var spk = new MonRaceInfo(_codes.get(2).get(0), _codes.get(2).get(1), MonsterRace.getInstance().getMonsters(), MonsterRace.getInstance().getSpeeds()); //
			_player.sendPacket(spk);
			_player.broadcastPacket(spk);
			ThreadPoolManager.getInstance().scheduleGeneral(new RunEnd(_player), 30000);
		}
	}
	
	private static class RunEnd implements Runnable
	{
		private final L2PcInstance _player;
		
		public RunEnd(L2PcInstance pl)
		{
			_player = pl;
		}
		
		@Override
		public void run()
		{
			DeleteObject obj = null;
			for (int i = 0; i < 8; i++)
			{
				obj = new DeleteObject(MonsterRace.getInstance().getMonsters()[i]);
				_player.sendPacket(obj);
				_player.broadcastPacket(obj);
			}
			_state = -1;
			
		}
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
