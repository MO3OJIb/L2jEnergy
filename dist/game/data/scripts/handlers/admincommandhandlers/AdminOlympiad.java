/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.Hero;
import com.l2jserver.gameserver.model.olympiad.Olympiad;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * Olympiad command.
 */
public class AdminOlympiad implements IAdminCommandHandler
{
	protected static final String POINTS = "olympiad_points";
	
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_olympiad_info",
		"admin_saveolymp",
		"admin_endolympiad",
		"admin_sethero",
		"admin_givehero"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command, " ");
		var actualCommand = st.nextToken();
		var target = getTarget(player);
		
		if (actualCommand.equals("admin_olympiad_info"))
		{
			if (st.hasMoreTokens())
			{
				var action = st.nextToken();
				if ((target == null) || !st.hasMoreTokens())
				{
					return false;
				}
				
				var stats = Olympiad.getNobleStats(target.getObjectId());
				var oldpoints = Olympiad.getInstance().getNoblePoints(target.getObjectId());
				
				var value = 0;
				try
				{
					value = oldpoints + Integer.parseInt(st.nextToken());
				}
				catch (Exception ex)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_invalid_value"));
					return false;
				}
				
				var points = 0;
				try
				{
					points = oldpoints - Integer.parseInt(st.nextToken());
				}
				catch (Exception ex)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_invalid_value"));
					return false;
				}
				switch (action)
				{
					case "increase" ->
					{
						if (value >= 100)
						{
							
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_cant_set_more_than_100_points"));
							return false;
						}
						stats.set(POINTS, value);
						target.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.C1_HAS_GAINED_S2_OLYMPIAD_POINTS).addString(target.getName()).addInt(value));
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_player_now_has_oly_point").replace("%i%", target.getName() + "").replace("%s%", value + ""));
					}
					case "decrease" ->
					{
						if (points < 1)
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_cant_set_less_than_0_points"));
							return false;
						}
						stats.set(POINTS, points);
						target.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.C1_HAS_LOST_S2_OLYMPIAD_POINTS).addString(target.getName()).addInt(value));
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_player_now_has_oly_point").replace("%i%", target.getName() + "").replace("%s%", value + ""));
					}
				}
			}
		}
		else if (command.startsWith("admin_saveolymp"))
		{
			Olympiad.getInstance().saveOlympiadStatus();
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_save_oly_sys"));
		}
		else if (command.startsWith("admin_endolympiad"))
		{
			try
			{
				Olympiad.getInstance().manualSelectHeroes();
			}
			catch (Exception ex)
			{
				
			}
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_hero_formed"));
		}
		else if (command.startsWith("admin_sethero"))
		{
			if (player.getTarget() == null)
			{
				player.sendPacket(SystemMessageId.INVALID_TARGET);
				return false;
			}
			target.setHero(!target.isHero());
			target.broadcastUserInfo();
		}
		else if (command.startsWith("admin_givehero"))
		{
			if (player.getTarget() == null)
			{
				player.sendPacket(SystemMessageId.INVALID_TARGET);
				return false;
			}
			
			if (Hero.getInstance().isHero(target.getObjectId()))
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_hero_status_already_claimed"));
				return false;
			}
			
			if (!Hero.getInstance().isUnclaimedHero(target.getObjectId()))
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_hero_status_cannot_claimed"));
				return false;
			}
			Hero.getInstance().claimHero(target);
		}
		
		var html = new NpcHtmlMessage(0);
		html.setHtml(HtmCache.getInstance().getHtm(player, "data/html/admin/submenu/olympiad.htm"));
		
		html.replace("%match%", Olympiad.getInstance().getCompetitionDone(target.getObjectId()));
		html.replace("%win%", Olympiad.getInstance().getCompetitionWon(target.getObjectId()));
		html.replace("%defeat%", Olympiad.getInstance().getCompetitionLost(target.getObjectId()));
		html.replace("%points%", Olympiad.getInstance().getNoblePoints(target.getObjectId()));
		html.replace("%targetName%", target.getName());
		player.sendPacket(html);
		return true;
	}
	
	private L2PcInstance getTarget(L2PcInstance player)
	{
		return ((player.getTarget() != null) && (player.getTarget().getActingPlayer() != null)) ? player.getTarget().getActingPlayer() : player;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
