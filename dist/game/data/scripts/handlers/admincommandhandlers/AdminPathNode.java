/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import com.l2jserver.gameserver.configuration.config.GeoDataConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.pathfinding.AbstractNodeLoc;
import com.l2jserver.gameserver.pathfinding.PathFinding;

/**
 * PathNode command.
 */
public class AdminPathNode implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_pn_info",
		"admin_show_path",
		"admin_path_debug",
		"admin_show_pn",
		"admin_find_path",
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.equals("admin_pn_info"))
		{
			var info = PathFinding.getInstance().getStat();
			if (info == null)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_not_supported"));
			}
			else
			{
				for (var msg : info)
				{
					player.sendAdminMessage(msg);
				}
			}
		}
		else if (command.equals("admin_show_path"))
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_not_supported"));
		}
		else if (command.equals("admin_path_debug"))
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_not_supported"));
		}
		else if (command.equals("admin_show_pn"))
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_not_supported"));
		}
		else if (command.equals("admin_find_path"))
		{
			if (GeoDataConfig.PATHFINDING == 0)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_path_finding_disabled"));
				return true;
			}
			if (player.getTarget() != null)
			{
				var path = PathFinding.getInstance().findPath(player.getX(), player.getY(), (short) player.getZ(), player.getTarget().getX(), player.getTarget().getY(), (short) player.getTarget().getZ(), player.getInstanceId(), true);
				if (path == null)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_no_route"));
					return true;
				}
				for (AbstractNodeLoc a : path)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_path_coord").replace("%i%", a.getX() + "").replace("%s%", a.getY() + "").replace("%n%", a.getZ() + ""));
				}
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_no_target"));
			}
		}
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
