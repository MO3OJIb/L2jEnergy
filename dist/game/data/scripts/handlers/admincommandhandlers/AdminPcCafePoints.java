/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.Collection;
import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.events.PcCafeConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * PcCafePoints command.
 */
public final class AdminPcCafePoints implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_pccafepoints"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command, " ");
		var actualCommand = st.nextToken();
		
		if (actualCommand.equals("admin_pccafepoints"))
		{
			if (st.hasMoreTokens())
			{
				var action = st.nextToken();
				
				var target = getTarget(player);
				if ((target == null) || !st.hasMoreTokens())
				{
					return false;
				}
				
				var value = 0;
				try
				{
					value = Integer.parseInt(st.nextToken());
				}
				catch (Exception ex)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_invalid_value"));
					return false;
				}
				
				switch (action)
				{
					case "increase" ->
					{
						if (target.getPcCafeSystem().getPoints() >= PcCafeConfig.MAX_PC_BANG_POINTS)
						{
							player.sendMessage(MessagesData.getInstance().getMessage(player, "target_already_have_max_count_pc_cafe_point").replace("%c%", target.getName() + ""));
							return false;
						}
						player.getPcCafeSystem().addPoints(value);
						target.sendMessage(MessagesData.getInstance().getMessage(target, "target_increase_your_pc_cafe_point").replace("%s%", value + ""));
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_increased_your_pc_cafe_point").replace("%c%", target.getName() + "").replace("%s%", value + ""));
					}
					case "decrease" ->
					{
						if (target.getPcCafeSystem().getPoints() == 0)
						{
							player.sendMessage(MessagesData.getInstance().getMessage(player, "target_already_have_min_count_pc_cafe_point").replace("%c%", target.getName() + ""));
							return false;
						}
						player.getPcCafeSystem().decreasePoints(value);
						target.sendMessage(MessagesData.getInstance().getMessage(target, "target_decreased_your_pc_cafe_point").replace("%s%", value + ""));
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_decreased_your_pc_cafe_point").replace("%c%", target.getName() + "").replace("%s%", value + ""));
					}
					case "rewardOnline" ->
					{
						var range = 0;
						try
						{
							range = Integer.parseInt(st.nextToken());
						}
						catch (Exception ex)
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_invalid_value"));
							return false;
						}
						
						if (range <= 0)
						{
							final int count = increaseForAll(L2World.getInstance().getPlayers(), value);
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_increased_your_pc_cafe_point_all_online").replace("%i%", count + "").replace("%s%", value + ""));
						}
						else if (range > 0)
						{
							final int count = increaseForAll(player.getKnownList().getKnownPlayers().values(), value);
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_increased_your_pc_cafe_point_all_players").replace("%i%", count + "").replace("%r%", range + "").replace("%s%", value + ""));
						}
					}
				}
			}
		}
		var target = getTarget(player);
		var points = target.getPcCafeSystem().getPoints();
		
		var html = new NpcHtmlMessage(0);
		html.setHtml(HtmCache.getInstance().getHtm(player, "data/html/admin/submenu/pccafepoints.htm"));
		
		html.replace("%points%", StringUtil.formatNumber(points));
		html.replace("%targetName%", target.getName());
		player.sendPacket(html);
		return true;
	}
	
	private L2PcInstance getTarget(L2PcInstance player)
	{
		return ((player.getTarget() != null) && (player.getTarget().getActingPlayer() != null)) ? player.getTarget().getActingPlayer() : player;
	}
	
	private int increaseForAll(Collection<L2PcInstance> players, int value)
	{
		var counter = 0;
		for (var player : players)
		{
			if ((player != null) && (player.isOnlineInt() == 1))
			{
				if (player.getPcCafeSystem().getPoints() >= PcCafeConfig.MAX_PC_BANG_POINTS)
				{
					continue;
				}
				player.getPcCafeSystem().addPoints(value);
				player.sendMessage(MessagesData.getInstance().getMessage(player, "admin_increase_your_pc_cafe_point").replace("%s%", value + ""));
				counter++;
			}
		}
		return counter;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}