/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.PcCondOverride;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * PcCondOverride command.
 * @author UnAfraid
 */
public class AdminPcCondOverride implements IAdminCommandHandler
{
	private static final String[] COMMANDS =
	{
		"admin_exceptions",
		"admin_set_exception",
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command);
		if (st.hasMoreTokens())
		{
			switch (st.nextToken())
			// command
			{
				case "admin_exceptions" ->
				{
					var html = new NpcHtmlMessage(0);
					html.setFile(player, "data/html/admin/submenu/cond_override.htm");
					
					var sb = new StringBuilder();
					for (var ex : PcCondOverride.values())
					{
						sb.append("<tr><td fixwidth=\"180\">" + ex.getDescription() + ":</td><td><a action=\"bypass -h admin_set_exception " + ex.ordinal() + "\">"
							+ (player.canOverrideCond(ex) ? "" + MessagesData.getInstance().getMessage(player, "admin_disable_page") + "" : "" + MessagesData.getInstance().getMessage(player, "admin_enable_page") + "") + "</a></td></tr>");
					}
					html.replace("%cond_table%", sb.toString());
					player.sendPacket(html);
				}
				case "admin_set_exception" ->
				{
					if (st.hasMoreTokens())
					{
						var token = st.nextToken();
						if (StringUtil.isDigit(token))
						{
							var ex = PcCondOverride.getCondOverride(Integer.valueOf(token));
							if (ex != null)
							{
								if (player.canOverrideCond(ex))
								{
									player.removeOverridedCond(ex);
									player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_disabled_pc_cond").replace("%i%", ex.getDescription() + ""));
								}
								else
								{
									player.addOverrideCond(ex);
									player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_enabled_pc_cond").replace("%i%", ex.getDescription() + ""));
								}
							}
						}
						else
						{
							switch (token)
							{
								case "enable_all" ->
								{
									for (var ex : PcCondOverride.values())
									{
										if (!player.canOverrideCond(ex))
										{
											player.addOverrideCond(ex);
										}
									}
									player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_all_condition_exceptions_have_enabled"));
								}
								case "disable_all" ->
								{
									for (var ex : PcCondOverride.values())
									{
										if (player.canOverrideCond(ex))
										{
											player.removeOverridedCond(ex);
										}
									}
									player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_all_condition_exceptions_have_disabled"));
								}
							}
						}
						useAdminCommand(COMMANDS[0], player);
					}
				}
			}
		}
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return COMMANDS;
	}
}
