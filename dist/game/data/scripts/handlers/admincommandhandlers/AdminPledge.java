/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.GMViewPledgeInfo;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * Pledge commands.
 */
public class AdminPledge implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_pledge"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var target = getTarget(player);
		if ((target == null))
		{
			return false;
		}
		
		var name = target.getName();
		if (command.startsWith("admin_pledge"))
		{
			String action = null;
			String parameter = null;
			var st = new StringTokenizer(command);
			try
			{
				st.nextToken();
				action = st.nextToken(); // create|info|dismiss|setlevel|rep
				parameter = st.nextToken(); // clanname|nothing|nothing|level|rep_points
			}
			catch (NoSuchElementException nse)
			{
				return false;
			}
			if (action.equals("create"))
			{
				var cet = target.getClanCreateExpiryTime();
				target.setClanCreateExpiryTime(0);
				var clan = ClanTable.getInstance().createClan(target, parameter);
				if (clan != null)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_clan_created_leader_name").replace("%i%", parameter + "").replace("%s%", target.getName() + ""));
				}
				else
				{
					target.setClanCreateExpiryTime(cet);
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_clan_creating_problem"));
				}
			}
			else if (!target.isClanLeader())
			{
				player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.S1_IS_NOT_A_CLAN_LEADER).addString(name));
				showMainPage(player);
				return false;
			}
			else if (action.equals("dismiss"))
			{
				ClanTable.getInstance().destroyClan(target.getClanId());
				var clan = target.getClan();
				if (clan == null)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_clan_disbanded"));
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_clan_was_problem_destroying"));
				}
			}
			else if (action.equals("info"))
			{
				player.sendPacket(new GMViewPledgeInfo(target.getClan(), target));
			}
			else if (parameter == null)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_clan_pledge"));
			}
			else if (action.equals("setlevel"))
			{
				var level = Integer.parseInt(parameter);
				if ((level >= 0) && (level < 12))
				{
					player.getClan().changeLevel(level);
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_clan_set_level").replace("%i%", level + "").replace("%s%", target.getClan().getName() + ""));
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_clan_level_incorrect"));
				}
			}
			else if (action.startsWith("rep"))
			{
				try
				{
					var points = Integer.parseInt(parameter);
					var clan = target.getClan();
					if (clan.getLevel() < 5)
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_clan_only_level_5_receive_reputation_points"));
						showMainPage(player);
						return false;
					}
					clan.incReputation(points, true);
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_clan_you_add_remove_points_reputation").replace("%i%", (points > 0 ? "" + MessagesData.getInstance().getMessage(player, "admin_htm_add") + "" : ""
						+ MessagesData.getInstance().getMessage(player, "admin_htm_remove") + "") + Math.abs(points)
						+ "").replace("%s%", (points > 0 ? "" + MessagesData.getInstance().getMessage(player, "admin_htm_to") + "" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_from") + "") + "").replace("%t%", clan.getName() + "").replace("%m%", clan.getReputationScore() + ""));
				}
				catch (Exception ex)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_clan_pledge_rep"));
				}
			}
		}
		showMainPage(player);
		return true;
	}
	
	private void showMainPage(L2PcInstance player)
	{
		AdminHtml.showAdminHtml(player, "game_menu.htm");
	}
	
	private L2PcInstance getTarget(L2PcInstance player)
	{
		return ((player.getTarget() != null) && (player.getTarget().getActingPlayer() != null)) ? player.getTarget().getActingPlayer() : player;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
