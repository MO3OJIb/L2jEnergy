/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.TransformData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.MagicSkillUse;
import com.l2jserver.gameserver.network.serverpackets.SetupGauge;

/**
 * Polymorph command.
 * @author Zoey76
 */
public class AdminPolymorph implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_polymorph",
		"admin_unpolymorph",
		"admin_transform",
		"admin_untransform",
		"admin_transform_menu",
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.equals("admin_transform_menu"))
		{
			AdminHtml.showAdminHtml(player, "submenu/transform/transform.htm");
			return true;
		}
		else if (command.startsWith("admin_untransform"))
		{
			var target = player.getTarget();
			if (target instanceof L2Character)
			{
				((L2Character) target).stopTransformation(true);
			}
			else
			{
				player.sendPacket(SystemMessageId.INVALID_TARGET);
			}
		}
		else if (command.startsWith("admin_transform"))
		{
			var target = player.getTarget();
			if ((target == null) || !target.isPlayer())
			{
				player.sendPacket(SystemMessageId.INVALID_TARGET);
				return false;
			}
			
			var pl = target.getActingPlayer();
			if (player.isSitting())
			{
				player.sendPacket(SystemMessageId.CANNOT_TRANSFORM_WHILE_SITTING);
				return false;
			}
			
			if (pl.isTransformed() || pl.isInStance())
			{
				if (!command.contains(" "))
				{
					pl.untransform();
					return true;
				}
				player.sendPacket(SystemMessageId.YOU_ALREADY_POLYMORPHED_AND_CANNOT_POLYMORPH_AGAIN);
				return false;
			}
			
			if (pl.isInWater())
			{
				player.sendPacket(SystemMessageId.YOU_CANNOT_POLYMORPH_INTO_THE_DESIRED_FORM_IN_WATER);
				return false;
			}
			
			if (pl.isFlyingMounted() || pl.isMounted())
			{
				player.sendPacket(SystemMessageId.YOU_CANNOT_POLYMORPH_WHILE_RIDING_A_PET);
				return false;
			}
			
			var parts = command.split(" ");
			if ((parts.length != 2) || !StringUtil.isDigit(parts[1]))
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_transform"));
				return false;
			}
			
			var id = Integer.parseInt(parts[1]);
			if (!TransformData.getInstance().transformPlayer(id, pl))
			{
				pl.sendAdminMessage(MessagesData.getInstance().getMessage(pl, "admin_unknown_transformation_id").replace("%s%", id + ""));
				return false;
			}
		}
		if (command.startsWith("admin_polymorph"))
		{
			var parts = command.split(" ");
			if ((parts.length < 2) || !StringUtil.isDigit(parts[1]))
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_polymorph"));
				return false;
			}
			
			if (parts.length > 2)
			{
				doPolymorph(player, player.getTarget(), parts[2], parts[1]);
			}
			else
			{
				doPolymorph(player, player.getTarget(), parts[1], "npc");
			}
		}
		else if (command.equals("admin_unpolymorph"))
		{
			doUnPolymorph(player, player.getTarget());
		}
		return true;
	}
	
	private static void doPolymorph(L2PcInstance player, L2Object target, String id, String type)
	{
		if (target != null)
		{
			target.getPoly().setPolyInfo(type, id);
			// animation
			if (target instanceof L2Character)
			{
				var Char = (L2Character) target;
				var msk = new MagicSkillUse(Char, 1008, 1, 4000, 0);
				Char.broadcastPacket(msk);
				var sg = new SetupGauge(0, 4000);
				Char.sendPacket(sg);
			}
			// end of animation
			target.decayMe();
			target.spawnMe(target.getX(), target.getY(), target.getZ());
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_polymorph_succeed"));
		}
		else
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
		}
	}
	
	private static void doUnPolymorph(L2PcInstance player, L2Object target)
	{
		if (target != null)
		{
			target.getPoly().setPolyInfo(null, "1");
			target.decayMe();
			target.spawnMe(target.getX(), target.getY(), target.getZ());
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unpolymorph_succeed"));
		}
		else
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
		}
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
