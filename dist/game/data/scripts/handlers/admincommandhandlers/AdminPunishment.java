/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.parser.IPConfigDataParser;
import com.l2jserver.gameserver.data.sql.impl.CharNameTable;
import com.l2jserver.gameserver.enums.PunishmentAffect;
import com.l2jserver.gameserver.enums.PunishmentType;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.PunishmentManager;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.punishment.PunishmentTask;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.util.LoggingUtils;
import com.l2jserver.gameserver.util.Util;

/**
 * Punishment commands.
 * @author UnAfraid
 */
public class AdminPunishment implements IAdminCommandHandler
{
	private static final Logger LOG_GM_AUDIT = LoggerFactory.getLogger("gmaudit");
	
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_punishment",
		"admin_punishment_add",
		"admin_punishment_remove",
		"admin_ban_acc",
		"admin_unban_acc",
		"admin_ban_hwid",
		"admin_unban_hwid",
		"admin_ban_chat",
		"admin_unban_chat",
		"admin_ban_char",
		"admin_unban_char",
		"admin_jail",
		"admin_unjail"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance activeChar)
	{
		var st = new StringTokenizer(command, " ");
		if (!st.hasMoreTokens())
		{
			return false;
		}
		var cmd = st.nextToken();
		switch (cmd)
		{
			case "admin_punishment":
			{
				if (!st.hasMoreTokens())
				{
					var html = new NpcHtmlMessage(0);
					html.setHtml(HtmCache.getInstance().getHtm(activeChar, "data/html/admin/submenu/punishment.htm"));
					html.replace("%punishments%", Util.implode(PunishmentType.values(), ";"));
					html.replace("%affects%", Util.implode(PunishmentAffect.values(), ";"));
					activeChar.sendPacket(html);
				}
				else
				{
					var subcmd = st.nextToken();
					switch (subcmd)
					{
						case "info":
						{
							var key = st.hasMoreTokens() ? st.nextToken() : null;
							var af = st.hasMoreTokens() ? st.nextToken() : null;
							var name = key;
							
							if ((key == null) || (af == null))
							{
								activeChar.sendAdminMessage("Not enough data specified!");
								break;
							}
							var affect = PunishmentAffect.getByName(af);
							if (affect == null)
							{
								activeChar.sendAdminMessage("Incorrect value specified for affect type!");
								break;
							}
							
							// Swap the name of the character with it's id.
							if (affect == PunishmentAffect.CHARACTER)
							{
								key = findCharId(key);
							}
							
							var html = new NpcHtmlMessage(0);
							html.setFile(activeChar, "data/html/admin/submenu/punishment/info.htm");
							var sb = new StringBuilder();
							for (var type : PunishmentType.values())
							{
								if (PunishmentManager.getInstance().hasPunishment(key, affect, type))
								{
									var expiration = PunishmentManager.getInstance().getPunishmentExpiration(key, affect, type);
									var expire = "never";
									
									if (expiration > 0)
									{
										// Synchronize date formatter since its not thread safe.
										synchronized (TimeUtils.DATE_TIME_FORMATTER_FIX)
										{
											expire = TimeUtils.DATE_TIME_FORMATTER_FIX.format(new Date(expiration));
										}
									}
									sb.append("<tr><td><font color=\"LEVEL\">" + type + "</font></td><td>" + expire + "</td><td><a action=\"bypass -h admin_punishment_remove " + name + " " + affect + " " + type + "\">Remove</a></td></tr>");
								}
							}
							
							html.replace("%player_name%", name);
							html.replace("%punishments%", sb.toString());
							html.replace("%affects%", Util.implode(PunishmentAffect.values(), ";"));
							html.replace("%affect_type%", affect.name());
							activeChar.sendPacket(html);
							break;
						}
						case "player":
						{
							L2PcInstance target = null;
							if (st.hasMoreTokens())
							{
								var playerName = st.nextToken();
								if (playerName.isEmpty() && ((activeChar.getTarget() == null) || !activeChar.getTarget().isPlayer()))
								{
									return useAdminCommand("admin_punishment", activeChar);
								}
								target = L2World.getInstance().getPlayer(playerName);
							}
							if ((target == null) && ((activeChar.getTarget() == null) || !activeChar.getTarget().isPlayer()))
							{
								activeChar.sendAdminMessage("You must target player!");
								break;
							}
							if (target == null)
							{
								target = activeChar.getTarget().getActingPlayer();
							}
							
							var html = new NpcHtmlMessage(0);
							html.setFile(activeChar, "data/html/admin/submenu/punishment/player.htm");
							html.replace("%player_name%", target.getName());
							html.replace("%punishments%", Util.implode(PunishmentType.values(), ";"));
							html.replace("%acc%", target.getAccountName());
							html.replace("%char%", target.getName());
							html.replace("%ip%", target.getIPAddress());
							html.replace("%hwid%", (target.getClient() == null) || (target.getClient().getHardwareInfo() == null) ? "Unknown" : target.getClient().getHardwareInfo().getMacAddress());
							activeChar.sendPacket(html);
							break;
						}
					}
				}
				break;
			}
			case "admin_punishment_add":
			{
				// Add new punishment
				var key = st.hasMoreTokens() ? st.nextToken() : null;
				var af = st.hasMoreTokens() ? st.nextToken() : null;
				var t = st.hasMoreTokens() ? st.nextToken() : null;
				var exp = st.hasMoreTokens() ? st.nextToken() : null;
				var reason = st.hasMoreTokens() ? st.nextToken() : null;
				
				// Let's grab the other part of the reason if there is..
				if (reason != null)
				{
					while (st.hasMoreTokens())
					{
						reason += " " + st.nextToken();
					}
					if (!reason.isEmpty())
					{
						reason = reason.replaceAll("\\$", "\\\\\\$");
						reason = reason.replaceAll("\r\n", "<br1>");
						reason = reason.replace("<", "&lt;");
						reason = reason.replace(">", "&gt;");
					}
				}
				
				var name = key;
				
				if ((key == null) || (af == null) || (t == null) || (exp == null) || (reason == null))
				{
					activeChar.sendAdminMessage("Please fill all the fields!");
					break;
				}
				if (!StringUtil.isDigit(exp) && !exp.equals("-1"))
				{
					activeChar.sendAdminMessage("Incorrect value specified for expiration time!");
					break;
				}
				
				long expirationTime = Integer.parseInt(exp);
				if (expirationTime > 0)
				{
					expirationTime = System.currentTimeMillis() + (expirationTime * 60 * 1000);
				}
				
				var affect = PunishmentAffect.getByName(af);
				var type = PunishmentType.getByName(t);
				if ((affect == null) || (type == null))
				{
					activeChar.sendAdminMessage("Incorrect value specified for affect/punishment type!");
					break;
				}
				
				// Swap the name of the character with it's id.
				if (affect == PunishmentAffect.CHARACTER)
				{
					key = findCharId(key);
				}
				else if (affect == PunishmentAffect.IP)
				{
					try
					{
						var addr = InetAddress.getByName(key);
						if (addr.isLoopbackAddress())
						{
							throw new UnknownHostException("You cannot ban any local address!");
						}
						else if (IPConfigDataParser.getInstance().getHosts().contains(addr.getHostAddress()))
						{
							throw new UnknownHostException("You cannot ban your gameserver's address!");
						}
					}
					catch (UnknownHostException e)
					{
						activeChar.sendAdminMessage("You've entered an incorrect IP address!");
						activeChar.sendMessage(e.getMessage());
						break;
					}
				}
				
				// Check if we already put the same punishment on that guy ^^
				if (PunishmentManager.getInstance().hasPunishment(key, affect, type))
				{
					activeChar.sendAdminMessage("Target is already affected by that punishment.");
					break;
				}
				
				// Punish him!
				PunishmentManager.getInstance().startPunishment(new PunishmentTask(key, affect, type, expirationTime, reason, activeChar.getName()));
				activeChar.sendAdminMessage("Punishment " + type.name() + " have been applied to: " + affect + " " + name + "!");
				LoggingUtils.logGMAudit(LOG_GM_AUDIT, activeChar.getName() + " [" + activeChar.getObjectId() + "]", cmd, affect.name(), name);
				return useAdminCommand("admin_punishment info " + name + " " + affect.name(), activeChar);
			}
			case "admin_punishment_remove":
			{
				// Remove punishment.
				var key = st.hasMoreTokens() ? st.nextToken() : null;
				var af = st.hasMoreTokens() ? st.nextToken() : null;
				var t = st.hasMoreTokens() ? st.nextToken() : null;
				var name = key;
				
				if ((key == null) || (af == null) || (t == null))
				{
					activeChar.sendAdminMessage("Not enough data specified!");
					break;
				}
				
				var affect = PunishmentAffect.getByName(af);
				var type = PunishmentType.getByName(t);
				if ((affect == null) || (type == null))
				{
					activeChar.sendAdminMessage("Incorrect value specified for affect/punishment type!");
					break;
				}
				
				// Swap the name of the character with it's id.
				if (affect == PunishmentAffect.CHARACTER)
				{
					key = findCharId(key);
				}
				
				if (!PunishmentManager.getInstance().hasPunishment(key, affect, type))
				{
					activeChar.sendAdminMessage("Target is not affected by that punishment!");
					break;
				}
				
				PunishmentManager.getInstance().stopPunishment(key, affect, type);
				activeChar.sendAdminMessage("Punishment " + type.name() + " have been stopped to: " + affect + " " + name + "!");
				LoggingUtils.logGMAudit(LOG_GM_AUDIT, activeChar.getName() + " [" + activeChar.getObjectId() + "]", cmd, affect.name(), name);
				return useAdminCommand("admin_punishment info " + name + " " + affect.name(), activeChar);
			}
			case "admin_ban_char":
			{
				if (st.hasMoreTokens())
				{
					return useAdminCommand(String.format("admin_punishment_add %s %s %s %s %s", st.nextToken(), PunishmentAffect.CHARACTER, PunishmentType.BAN, 0, "Banned by admin"), activeChar);
				}
			}
			case "admin_unban_char":
			{
				if (st.hasMoreTokens())
				{
					return useAdminCommand(String.format("admin_punishment_remove %s %s %s", st.nextToken(), PunishmentAffect.CHARACTER, PunishmentType.BAN), activeChar);
				}
			}
			case "admin_ban_acc":
			{
				if (st.hasMoreTokens())
				{
					return useAdminCommand(String.format("admin_punishment_add %s %s %s %s %s", st.nextToken(), PunishmentAffect.ACCOUNT, PunishmentType.BAN, 0, "Banned by admin"), activeChar);
				}
			}
			case "admin_unban_acc":
			{
				if (st.hasMoreTokens())
				{
					return useAdminCommand(String.format("admin_punishment_remove %s %s %s", st.nextToken(), PunishmentAffect.ACCOUNT, PunishmentType.BAN), activeChar);
				}
			}
			case "admin_ban_hwid":
			{
				if (st.hasMoreTokens())
				{
					return useAdminCommand(String.format("admin_punishment_add %s %s %s %s %s", st.nextToken(), PunishmentAffect.HWID, PunishmentType.BAN, 0, "Banned by admin"), activeChar);
				}
				break;
			}
			case "admin_unban_hwid":
			{
				if (st.hasMoreTokens())
				{
					return useAdminCommand(String.format("admin_punishment_remove %s %s %s", st.nextToken(), PunishmentAffect.HWID, PunishmentType.BAN), activeChar);
				}
				break;
			}
			case "admin_ban_chat":
			{
				if (st.hasMoreTokens())
				{
					return useAdminCommand(String.format("admin_punishment_add %s %s %s %s %s", st.nextToken(), PunishmentAffect.CHARACTER, PunishmentType.CHAT_BAN, 0, "Chat banned by admin"), activeChar);
				}
			}
			case "admin_unban_chat":
			{
				if (st.hasMoreTokens())
				{
					return useAdminCommand(String.format("admin_punishment_remove %s %s %s", st.nextToken(), PunishmentAffect.CHARACTER, PunishmentType.CHAT_BAN), activeChar);
				}
			}
			case "admin_jail":
			{
				if (st.hasMoreTokens())
				{
					return useAdminCommand(String.format("admin_punishment_add %s %s %s %s %s", st.nextToken(), PunishmentAffect.CHARACTER, PunishmentType.JAIL, 0, "Jailed by admin"), activeChar);
				}
			}
			case "admin_unjail":
			{
				if (st.hasMoreTokens())
				{
					return useAdminCommand(String.format("admin_punishment_remove %s %s %s", st.nextToken(), PunishmentAffect.CHARACTER, PunishmentType.JAIL), activeChar);
				}
			}
		}
		return true;
	}
	
	private static final String findCharId(String key)
	{
		var charId = CharNameTable.getInstance().getIdByName(key);
		if (charId > 0) // Yeah its a char name!
		{
			return Integer.toString(charId);
		}
		return key;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}