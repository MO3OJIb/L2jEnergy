/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.io.File;
import java.util.TreeSet;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.configuration.config.ServerConfig;
import com.l2jserver.gameserver.enums.events.EventType;
import com.l2jserver.gameserver.enums.events.ListenerRegisterType;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.QuestManager;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.scripting.L2ScriptEngineManager;

/**
 * Quest commands.
 */
public class AdminQuest implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_quest_reload",
		"admin_script_load",
		"admin_script_unload",
		"admin_show_quests",
		"admin_quest_info"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (player == null)
		{
			return false;
		}
		
		// syntax will either be:
		// //quest_reload <id>
		// //quest_reload <questName>
		// The questName MUST start with a non-numeric character for this to work,
		// regardless which of the two formats is used.
		// Example: //quest_reload orc_occupation_change_1
		// Example: //quest_reload chests
		// Example: //quest_reload SagasSuperclass
		// Example: //quest_reload 12
		if (command.startsWith("admin_quest_reload"))
		{
			var parts = command.split(" ");
			if (parts.length < 2)
			{
				player.sendAdminMessage("Usage: //quest_reload <questFolder>.<questSubFolders...>.questName> or //quest_reload <id>");
			}
			else
			{
				// try the first param as id
				try
				{
					var questId = Integer.parseInt(parts[1]);
					if (QuestManager.getInstance().reload(questId))
					{
						player.sendAdminMessage("Quest Reloaded Successfully.");
					}
					else
					{
						player.sendAdminMessage("Quest Reloaded Failed");
					}
				}
				catch (NumberFormatException e)
				{
					if (QuestManager.getInstance().reload(parts[1]))
					{
						player.sendAdminMessage("Quest Reloaded Successfully.");
					}
					else
					{
						player.sendAdminMessage("Quest Reloaded Failed");
					}
				}
			}
		}
		// script load should NOT be used in place of reload. If a script is already loaded
		// successfully, quest_reload ought to be used. The script_load command should only
		// be used for scripts that failed to load altogether (eg. due to errors) or that
		// did not at all exist during server boot. Using script_load to re-load a previously
		// loaded script may cause unpredictable script flow, minor loss of data, and more.
		// This provides a way to load new scripts without having to reboot the server.
		else if (command.startsWith("admin_script_load"))
		{
			var parts = command.split(" ");
			if (parts.length < 2)
			{
				// activeChar.sendMessage("Example: //script_load <questFolder>/<questSubFolders...>/<filename>.<ext> ");
				player.sendAdminMessage("Example: //script_load quests/SagasSuperclass/__init__.py");
			}
			else
			{
				var file = new File(ServerConfig.DATAPACK_ROOT, "data/scripts/" + parts[1]);
				// Trying to reload by script name.
				if (!file.exists())
				{
					var quest = QuestManager.getInstance().getQuest(parts[1]);
					if (quest != null)
					{
						file = new File(ServerConfig.DATAPACK_ROOT, "data/scripts/" + quest.getClass().getName().replaceAll("\\.", "/") + ".java");
					}
				}
				
				// Reloading by full path
				if (file.isFile())
				{
					try
					{
						L2ScriptEngineManager.getInstance().executeScript(file);
						
						// This part should be called only when the script is successfully loaded.
						player.sendAdminMessage("Script Successfully Loaded.");
					}
					catch (Exception ex)
					{
						player.sendAdminMessage("Failed loading: " + parts[1]);
					}
				}
				else
				{
					player.sendAdminMessage("File Not Found: " + parts[1]);
				}
			}
			
		}
		else if (command.startsWith("admin_script_unload"))
		{
			var parts = command.split(" ");
			if (parts.length < 2)
			{
				player.sendAdminMessage("Example: //script_unload questName/questId");
			}
			else
			{
				var q = StringUtil.isDigit(parts[1]) ? QuestManager.getInstance().getQuest(Integer.parseInt(parts[1])) : QuestManager.getInstance().getQuest(parts[1]);
				
				if (q != null)
				{
					if (q.unload())
					{
						player.sendAdminMessage("Script Successfully Unloaded [" + q.getName() + "/" + q.getId() + "]");
					}
					else
					{
						player.sendAdminMessage("Failed unloading [" + q.getName() + "/" + q.getId() + "].");
					}
				}
				else
				{
					player.sendAdminMessage("The quest [" + parts[1] + "] was not found!.");
				}
			}
		}
		else if (command.startsWith("admin_show_quests"))
		{
			if (player.getTarget() == null)
			{
				player.sendAdminMessage("Get a target first.");
			}
			else if (!player.getTarget().isCharacter())
			{
				player.sendAdminMessage("Invalid Target.");
			}
			else
			{
				var character = (L2Character) player.getTarget();
				var sb = new StringBuilder();
				var questNames = new TreeSet<>();
				for (var type : EventType.values())
				{
					for (var listener : character.getListeners(type))
					{
						if (listener.getOwner() instanceof Quest)
						{
							var quest = (Quest) listener.getOwner();
							if (questNames.contains(quest.getName()))
							{
								continue;
							}
							sb.append("<tr><td colspan=\"4\"><font color=\"LEVEL\"><a action=\"bypass -h admin_quest_info " + quest.getName() + "\">" + quest.getName() + "</a></font></td></tr>");
							questNames.add(quest.getName());
						}
					}
				}
				
				var msg = new NpcHtmlMessage(0);
				msg.setFile(player, "data/html/admin/submenu/npc-quests.htm");
				
				msg.replace("%quests%", sb.toString());
				msg.replace("%objid%", character.getObjectId());
				msg.replace("%questName%", "");
				player.sendPacket(msg);
			}
		}
		else if (command.startsWith("admin_quest_info "))
		{
			var questName = command.substring("admin_quest_info ".length());
			var quest = QuestManager.getInstance().getQuest(questName);
			String events = "", npcs = "", items = "", timers = "";
			var counter = 0;
			if (quest == null)
			{
				player.sendAdminMessage("Couldn't find quest or script with name " + questName + " !");
				return false;
			}
			
			var listenerTypes = new TreeSet<>();
			for (var listener : quest.getListeners())
			{
				if (!listenerTypes.contains(listener.getType()))
				{
					events += ", " + listener.getType().name();
					listenerTypes.add(listener.getType());
					counter++;
				}
				if (counter > 10)
				{
					counter = 0;
					break;
				}
			}
			
			var npcIds = new TreeSet<>(quest.getRegisteredIds(ListenerRegisterType.NPC));
			for (var npcId : npcIds)
			{
				npcs += ", " + npcId;
				counter++;
				if (counter > 50)
				{
					counter = 0;
					break;
				}
			}
			
			if (!events.isEmpty())
			{
				events = listenerTypes.size() + ": " + events.substring(2);
			}
			
			if (!npcs.isEmpty())
			{
				npcs = npcIds.size() + ": " + npcs.substring(2);
			}
			
			if (quest.getRegisteredItemIds() != null)
			{
				for (var itemId : quest.getRegisteredItemIds())
				{
					items += ", " + itemId;
					counter++;
					if (counter > 20)
					{
						counter = 0;
						break;
					}
				}
				items = quest.getRegisteredItemIds().length + ":" + items.substring(2);
			}
			
			for (var list : quest.getQuestTimers().values())
			{
				for (var timer : list)
				{
					timers += "<tr><td colspan=\"4\"><table width=270 border=0 bgcolor=131210><tr><td width=270><font color=\"LEVEL\">" + timer.getName() + ":</font> <font color=00FF00>Active: " + timer.getIsActive() + " Repeatable: " + timer.getIsRepeating() + " Player: " + timer.getPlayer()
						+ " Npc: " + timer.getNpc() + "</font></td></tr></table></td></tr>";
					counter++;
					if (counter > 10)
					{
						break;
					}
				}
			}
			
			var sb = new StringBuilder();
			sb.append("<tr><td colspan=\"4\"><table width=270 border=0 bgcolor=131210><tr><td width=270><font color=\"LEVEL\">ID:</font> <font color=00FF00>" + quest.getId() + "</font></td></tr></table></td></tr>");
			sb.append("<tr><td colspan=\"4\"><table width=270 border=0 bgcolor=131210><tr><td width=270><font color=\"LEVEL\">Name:</font> <font color=00FF00>" + quest.getName() + "</font></td></tr></table></td></tr>");
			sb.append("<tr><td colspan=\"4\"><table width=270 border=0 bgcolor=131210><tr><td width=270><font color=\"LEVEL\">Descr:</font> <font color=00FF00>" + quest.getDescr() + "</font></td></tr></table></td></tr>");
			sb.append("<tr><td colspan=\"4\"><table width=270 border=0 bgcolor=131210><tr><td width=270><font color=\"LEVEL\">Path:</font> <font color=00FF00>" + quest.getClass().getName().substring(0, quest.getClass().getName().lastIndexOf('.')).replaceAll("\\.", "/")
				+ "</font></td></tr></table></td></tr>");
			sb.append("<tr><td colspan=\"4\"><table width=270 border=0 bgcolor=131210><tr><td width=270><font color=\"LEVEL\">Events:</font> <font color=00FF00>" + events + "</font></td></tr></table></td></tr>");
			if (!npcs.isEmpty())
			{
				sb.append("<tr><td colspan=\"4\"><table width=270 border=0 bgcolor=131210><tr><td width=270><font color=\"LEVEL\">NPCs:</font> <font color=00FF00>" + npcs + "</font></td></tr></table></td></tr>");
			}
			if (!items.isEmpty())
			{
				sb.append("<tr><td colspan=\"4\"><table width=270 border=0 bgcolor=131210><tr><td width=270><font color=\"LEVEL\">Items:</font> <font color=00FF00>" + items + "</font></td></tr></table></td></tr>");
			}
			if (!timers.isEmpty())
			{
				sb.append("<tr><td colspan=\"4\"><table width=270 border=0 bgcolor=131210><tr><td width=270><font color=\"LEVEL\">Timers:</font> <font color=00FF00></font></td></tr></table></td></tr>");
				sb.append(timers);
			}
			
			var html = new NpcHtmlMessage(0);
			html.setFile(player, "data/html/admin/submenu/npc-quests.htm");
			html.replace("%quests%", sb.toString());
			html.replace("%questName%", "<table><tr><td width=\"50\" align=\"left\"><a action=\"bypass -h admin_script_load " + quest.getName() + "\">Reload</a></td> <td width=\"150\"  align=\"center\"><a action=\"bypass -h admin_quest_info " + quest.getName() + "\">" + quest.getName()
				+ "</a></td> <td width=\"50\" align=\"right\"><a action=\"bypass -h admin_script_unload " + quest.getName() + "\">Unload</a></tr></td></table>");
			player.sendPacket(html);
		}
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
