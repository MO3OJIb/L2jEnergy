/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2ControllableMobInstance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.taskmanager.DecayTaskManager;

/**
 * Res commands.
 */
public class AdminRes implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_res",
		"admin_res_monster"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.startsWith("admin_res "))
		{
			handleRes(player, command.split(" ")[1]);
		}
		else if (command.equals("admin_res"))
		{
			handleRes(player);
		}
		else if (command.startsWith("admin_res_monster "))
		{
			handleNonPlayerRes(player, command.split(" ")[1]);
		}
		else if (command.equals("admin_res_monster"))
		{
			handleNonPlayerRes(player);
		}
		return true;
	}
	
	private void handleRes(L2PcInstance player)
	{
		handleRes(player, null);
	}
	
	private void handleRes(L2PcInstance player, String resParam)
	{
		var object = player.getTarget();
		
		if (resParam != null)
		{
			// Check if a player name was specified as a param.
			var target = L2World.getInstance().getPlayer(resParam);
			
			if (target != null)
			{
				object = target;
			}
			else
			{
				// Otherwise, check if the param was a radius.
				try
				{
					var radius = Integer.parseInt(resParam);
					
					for (var pls : player.getKnownList().getKnownPlayersInRadius(radius))
					{
						doResurrect(pls);
					}
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_resurrected_all_players_within_radius").replace("%i%", radius + ""));
					return;
				}
				catch (NumberFormatException e)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_enter_invalid_name_or_radius"));
					return;
				}
			}
		}
		
		if (object == null)
		{
			object = player;
		}
		
		if (object instanceof L2ControllableMobInstance)
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		
		doResurrect((L2Character) object);
	}
	
	private void handleNonPlayerRes(L2PcInstance player)
	{
		handleNonPlayerRes(player, "");
	}
	
	private void handleNonPlayerRes(L2PcInstance player, String radiusStr)
	{
		var object = player.getTarget();
		
		try
		{
			var radius = 0;
			
			if (!radiusStr.isEmpty())
			{
				radius = Integer.parseInt(radiusStr);
				
				for (var knownChar : player.getKnownList().getKnownCharactersInRadius(radius))
				{
					if (!(knownChar instanceof L2PcInstance) && !(knownChar instanceof L2ControllableMobInstance))
					{
						doResurrect(knownChar);
					}
				}
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_resurrected_all_non_players_within_radius").replace("%i%", radius + ""));
			}
		}
		catch (NumberFormatException e)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_enter_invalid_radius"));
			return;
		}
		
		if ((object instanceof L2PcInstance) || (object instanceof L2ControllableMobInstance))
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		
		doResurrect((L2Character) object);
	}
	
	private void doResurrect(L2Character target)
	{
		if (!target.isDead())
		{
			return;
		}
		
		// If the target is a player, then restore the XP lost on death.
		if (target instanceof L2PcInstance)
		{
			target.doRevive(100.0);
		}
		else
		{
			DecayTaskManager.getInstance().cancel(target);
			target.doRevive();
		}
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
