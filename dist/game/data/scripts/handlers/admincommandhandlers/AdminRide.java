/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.TransformData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;

/**
 * Ride commands.
 */
public class AdminRide implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_ride_horse",
		"admin_ride_bike",
		"admin_ride_wyvern",
		"admin_ride_strider",
		"admin_unride_wyvern",
		"admin_unride_strider",
		"admin_unride",
		"admin_ride_wolf",
		"admin_unride_wolf",
	};
	private int _petRideId;
	
	private static final int PURPLE_MANED_HORSE_TRANSFORMATION_ID = 106;
	
	private static final int JET_BIKE_TRANSFORMATION_ID = 20001;
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var target = getRideTarget(player);
		if (target == null)
		{
			return false;
		}
		
		if (command.startsWith("admin_ride"))
		{
			if (target.isMounted() || target.hasSummon())
			{
				player.sendAdminMessage("Target already have a summon.");
				return false;
			}
			if (command.startsWith("admin_ride_wyvern"))
			{
				_petRideId = 12621;
			}
			else if (command.startsWith("admin_ride_strider"))
			{
				_petRideId = 12526;
			}
			else if (command.startsWith("admin_ride_wolf"))
			{
				_petRideId = 16041;
			}
			else if (command.startsWith("admin_ride_horse")) // handled using transformation
			{
				if (target.isTransformed() || target.isInStance())
				{
					player.sendPacket(SystemMessageId.YOU_ALREADY_POLYMORPHED_AND_CANNOT_POLYMORPH_AGAIN);
				}
				else
				{
					TransformData.getInstance().transformPlayer(PURPLE_MANED_HORSE_TRANSFORMATION_ID, target);
				}
				
				return true;
			}
			else if (command.startsWith("admin_ride_bike")) // handled using transformation
			{
				if (target.isTransformed() || target.isInStance())
				{
					player.sendPacket(SystemMessageId.YOU_ALREADY_POLYMORPHED_AND_CANNOT_POLYMORPH_AGAIN);
				}
				else
				{
					TransformData.getInstance().transformPlayer(JET_BIKE_TRANSFORMATION_ID, target);
				}
				
				return true;
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_command_not_recognized").replace("%i%", command + ""));
				return false;
			}
			
			target.mount(_petRideId, 0, false);
			
			return false;
		}
		else if (command.startsWith("admin_unride"))
		{
			if (target.getTransformationId() == PURPLE_MANED_HORSE_TRANSFORMATION_ID)
			{
				target.untransform();
			}
			
			if (target.getTransformationId() == JET_BIKE_TRANSFORMATION_ID)
			{
				target.untransform();
			}
			else
			{
				target.dismount();
			}
		}
		return true;
	}
	
	private L2PcInstance getRideTarget(L2PcInstance player)
	{
		L2PcInstance target = null;
		
		if ((player.getTarget() == null) || (player.getTarget().getObjectId() == player.getObjectId()) || !(player.getTarget() instanceof L2PcInstance))
		{
			target = player;
		}
		else
		{
			target = (L2PcInstance) player.getTarget();
		}
		
		return target;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
