/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 *
 * This file is part of L2jEnergy Server.
 *
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.SpawnData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.RaidBossSpawnManager;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * Scan commands.
 * @author NosBit
 */
public class AdminScan implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_scan",
		"admin_deleteNpcByObjectId"
	};
	
	private static final int DEFAULT_RADIUS = 500;
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command, " ");
		var actualCommand = st.nextToken();
		switch (actualCommand.toLowerCase())
		{
			case "admin_scan" ->
			{
				var radius = DEFAULT_RADIUS;
				if (st.hasMoreElements())
				{
					try
					{
						radius = Integer.parseInt(st.nextToken());
					}
					catch (NumberFormatException e)
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_scan_radius"));
						return false;
					}
				}
				sendNpcList(player, radius);
			}
			case "admin_deletenpcbyobjectid" ->
			{
				if (!st.hasMoreElements())
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_deletenpcbyobjectid"));
					return false;
				}
				
				try
				{
					var objectId = Integer.parseInt(st.nextToken());
					
					var target = L2World.getInstance().getObject(objectId);
					var npc = target instanceof L2Npc ? (L2Npc) target : null;
					if (npc == null)
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_npc_does_not_exist_object_id_does_not_belong"));
						return false;
					}
					
					npc.deleteMe();
					
					var spawn = npc.getSpawn();
					if (spawn != null)
					{
						spawn.stopRespawn();
						
						if (RaidBossSpawnManager.getInstance().isDefined(spawn.getId()))
						{
							RaidBossSpawnManager.getInstance().deleteSpawn(spawn, true);
						}
						else
						{
							SpawnData.getInstance().deleteSpawn(spawn, true);
						}
					}
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_object_id_have_been_deleted").replace("%i%", npc.getName() + ""));
				}
				catch (NumberFormatException e)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_object_id_must_be_number"));
					return false;
				}
				sendNpcList(player, DEFAULT_RADIUS);
			}
		}
		return true;
	}
	
	private void sendNpcList(L2PcInstance player, int radius)
	{
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/scan.htm");
		
		var sb = new StringBuilder();
		for (var character : player.getKnownList().getKnownCharactersInRadius(radius))
		{
			if (character instanceof L2Npc)
			{
				sb.append("<tr>");
				sb.append("<td width=\"54\">" + character.getId() + "</td>");
				sb.append("<td width=\"54\">" + character.getName() + "</td>");
				sb.append("<td width=\"54\">" + Math.round(player.calculateDistance(character, false, false)) + "</td>");
				sb.append("<td width=\"54\"><a action=\"bypass -h admin_deleteNpcByObjectId " + character.getObjectId() + "\"><font color=\"LEVEL\">Delete</font></a></td>");
				sb.append("<td width=\"54\"><a action=\"bypass -h admin_move_to " + character.getX() + " " + character.getY() + " " + character.getZ() + "\"><font color=\"LEVEL\">Go to</font></a></td>");
				sb.append("</tr>");
			}
		}
		html.replace("%data%", sb.toString());
		player.sendPacket(html);
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
