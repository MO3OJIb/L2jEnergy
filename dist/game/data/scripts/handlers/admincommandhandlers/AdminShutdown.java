/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.commons.network.ServerType;
import com.l2jserver.commons.util.MemoryUtils;
import com.l2jserver.gameserver.GameTimeController;
import com.l2jserver.gameserver.LoginServerThread;
import com.l2jserver.gameserver.Shutdown;
import com.l2jserver.gameserver.configuration.config.AdminConfig;
import com.l2jserver.gameserver.configuration.config.GeoDataConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.util.DifferentMethods;

/**
 * Shutdown commands.
 */
public class AdminShutdown implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_server_shutdown",
		"admin_server_restart",
		"admin_server_abort",
		"admin_server_gm_only",
		"admin_server_all",
		"admin_server_max_player"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.equals("admin_server_gm_only"))
		{
			gmOnly();
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_server_is_now_gm_only"));
		}
		else if (command.equals("admin_server_all"))
		{
			allowToAll();
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_server_is_not_gm_only_anymore"));
		}
		else if (command.startsWith("admin_server_max_player"))
		{
			var st = new StringTokenizer(command);
			if (st.countTokens() > 1)
			{
				st.nextToken();
				var number = st.nextToken();
				try
				{
					LoginServerThread.getInstance().setMaxPlayer(Integer.parseInt(number));
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_server_set_max_players").replace("%i%", Integer.parseInt(number) + ""));
				}
				catch (NumberFormatException e)
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_server_max_player_must_number"));
					AdminHtml.showAdminHtml(player, "server_menu.htm");
				}
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_server_max_player"));
			}
		}
		else if (command.startsWith("admin_server_shutdown"))
		{
			try
			{
				var val = command.substring(22);
				if (StringUtil.isDigit(val))
				{
					serverShutdown(player, Integer.valueOf(val), false);
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_server_shutdown"));
					AdminHtml.showAdminHtml(player, "server_menu.htm");
				}
			}
			catch (StringIndexOutOfBoundsException e)
			{
				AdminHtml.showAdminHtml(player, "server_menu.htm");
			}
		}
		else if (command.startsWith("admin_server_restart"))
		{
			try
			{
				var val = command.substring(21);
				if (StringUtil.isDigit(val))
				{
					serverShutdown(player, Integer.parseInt(val), true);
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_server_restart"));
				}
			}
			catch (StringIndexOutOfBoundsException e)
			{
				AdminHtml.showAdminHtml(player, "server_menu.htm");
			}
		}
		else if (command.startsWith("admin_server_abort"))
		{
			serverAbort(player);
		}
		
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/server_menu.htm");
		
		html.replace("%onlineAll%", String.valueOf(DifferentMethods.getPlayersCount("ALL")));
		html.replace("%offlineTrade%", String.valueOf(DifferentMethods.getPlayersCount("OFF_TRADE")));
		html.replace("%onlineGM%", String.valueOf(DifferentMethods.getPlayersCount("GM")));
		html.replace("%onlineReal%", String.valueOf(DifferentMethods.getPlayersCount("ALL_REAL")));
		html.replace("%used%", String.valueOf(MemoryUtils.getMemUsedMb()));
		html.replace("%free%", String.valueOf(MemoryUtils.getMemFreeMb()));
		html.replace("%max%", String.valueOf(MemoryUtils.getMemMaxMb()));
		html.replace("%os%", System.getProperty("os.name"));
		html.replace("%gameTime%", GameTimeController.getInstance().getGameHour() + ":" + GameTimeController.getInstance().getGameMinute());
		html.replace("%dayNight%", GameTimeController.getInstance().isNight() ? MessagesData.getInstance().getMessage(player, "admin_game_night") : MessagesData.getInstance().getMessage(player, "admin_game_day"));
		html.replace("%timeserv%", String.valueOf(DifferentMethods.getServerUpTime()));
		html.replace("%maxonline%", String.valueOf(DifferentMethods.getPlayersCount("ALL") + "/" + String.valueOf(LoginServerThread.getInstance().getMaxPlayer())));
		html.replace("%geo%", GeoDataConfig.PATHFINDING > 0 ? MessagesData.getInstance().getMessage(player, "admin_geo_loading") : MessagesData.getInstance().getMessage(player, "admin_geo_disabled"));
		player.sendPacket(html);
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
	
	private void serverShutdown(L2PcInstance activeChar, int seconds, boolean restart)
	{
		Shutdown.getInstance().startShutdown(activeChar, seconds, restart);
		
	}
	
	private void serverAbort(L2PcInstance activeChar)
	{
		Shutdown.getInstance().abort(activeChar);
	}
	
	private void allowToAll()
	{
		LoginServerThread.getInstance().setServerType(ServerType.AUTO);
		AdminConfig.SERVER_GMONLY = false;
	}
	
	private void gmOnly()
	{
		LoginServerThread.getInstance().setServerType(ServerType.GM_ONLY);
		AdminConfig.SERVER_GMONLY = true;
	}
}
