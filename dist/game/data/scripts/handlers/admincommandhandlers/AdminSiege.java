/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.Calendar;
import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.SevenSigns;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.AuctionManager;
import com.l2jserver.gameserver.instancemanager.CHSiegeManager;
import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.instancemanager.ClanHallManager;
import com.l2jserver.gameserver.model.L2Clan;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.Castle;
import com.l2jserver.gameserver.model.entity.ClanHall;
import com.l2jserver.gameserver.model.entity.clanhall.SiegableHall;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * Siege commands.
 * @author Zoey76 (rework)
 */
public class AdminSiege implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		// Castle commands
		"admin_siege",
		"admin_add_attacker",
		"admin_add_defender",
		"admin_add_guard",
		"admin_list_siege_clans",
		"admin_clear_siege_list",
		"admin_move_defenders",
		"admin_spawn_doors",
		"admin_endsiege",
		"admin_startsiege",
		"admin_setsiegetime",
		"admin_setcastle",
		"admin_removecastle",
		// Clan hall commands
		"admin_clanhall",
		"admin_clanhallset",
		"admin_clanhalldel",
		"admin_clanhallopendoors",
		"admin_clanhallclosedoors",
		"admin_clanhallteleportself"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command, " ");
		command = st.nextToken(); // Get actual command
		
		// Get castle
		Castle castle = null;
		ClanHall clanhall = null;
		if (st.hasMoreTokens())
		{
			L2PcInstance target = null;
			if ((player.getTarget() != null) && player.getTarget().isPlayer())
			{
				target = player.getTarget().getActingPlayer();
			}
			
			var val = st.nextToken();
			if (command.startsWith("admin_clanhall"))
			{
				if (StringUtil.isDigit(val))
				{
					clanhall = ClanHallManager.getInstance().getClanHallById(Integer.parseInt(val));
					L2Clan clan = null;
					switch (command)
					{
						case "admin_clanhallset":
							if ((target == null) || (target.getClan() == null))
							{
								player.sendPacket(SystemMessageId.INVALID_TARGET);
								return false;
							}
							
							if (clanhall.getOwnerId() > 0)
							{
								player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_this_clan_hall_not_free"));
								return false;
							}
							
							clan = target.getClan();
							if (clan.getHideoutId() > 0)
							{
								player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_have_already_clan_hall"));
								return false;
							}
							
							if (!clanhall.isSiegableHall())
							{
								ClanHallManager.getInstance().setOwner(clanhall.getId(), clan);
								if (AuctionManager.getInstance().getAuction(clanhall.getId()) != null)
								{
									AuctionManager.getInstance().getAuction(clanhall.getId()).deleteAuctionFromDB();
								}
							}
							else
							{
								clanhall.setOwner(clan);
								clan.setHideoutId(clanhall.getId());
							}
							break;
						case "admin_clanhalldel":
							
							if (!clanhall.isSiegableHall())
							{
								if (!ClanHallManager.getInstance().isFree(clanhall.getId()))
								{
									ClanHallManager.getInstance().setFree(clanhall.getId());
									AuctionManager.getInstance().initNPC(clanhall.getId());
								}
								else
								{
									player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_clan_hall_already_free"));
								}
							}
							else
							{
								var oldOwner = clanhall.getOwnerId();
								if (oldOwner > 0)
								{
									clanhall.free();
									clan = ClanTable.getInstance().getClan(oldOwner);
									if (clan != null)
									{
										clan.setHideoutId(0);
										clan.broadcastClanStatus();
									}
								}
							}
							break;
						case "admin_clanhallopendoors":
							clanhall.openCloseDoors(true);
							break;
						case "admin_clanhallclosedoors":
							clanhall.openCloseDoors(false);
							break;
						case "admin_clanhallteleportself":
							var zone = clanhall.getZone();
							if (zone != null)
							{
								player.teleToLocation(zone.getSpawnLoc(), true);
							}
							break;
						default:
							if (!clanhall.isSiegableHall())
							{
								showClanHallPage(player, clanhall);
							}
							else
							{
								showSiegableHallPage(player, (SiegableHall) clanhall);
							}
							break;
					}
				}
			}
			else
			{
				castle = CastleManager.getInstance().getCastle(val);
				switch (command)
				{
					case "admin_add_attacker":
						if (target == null)
						{
							player.sendPacket(SystemMessageId.INVALID_TARGET);
						}
						else
						{
							castle.getSiege().registerAttacker(target, true);
						}
						break;
					case "admin_add_defender":
						if (target == null)
						{
							player.sendPacket(SystemMessageId.INVALID_TARGET);
						}
						else
						{
							castle.getSiege().registerDefender(target, true);
						}
						break;
					case "admin_add_guard":
						if (st.hasMoreTokens())
						{
							val = st.nextToken();
							if (StringUtil.isDigit(val))
							{
								castle.getSiege().getSiegeGuardManager().addSiegeGuard(player, Integer.parseInt(val));
								break;
							}
						}
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_add_guard"));
						break;
					case "admin_clear_siege_list":
						DAOFactory.getInstance().getSiegeDAO().clearSiegeClan(castle.getSiege());
						break;
					case "admin_endsiege":
						castle.getSiege().endSiege();
						break;
					case "admin_list_siege_clans":
						castle.getSiege().listRegisterClan(player);
						break;
					case "admin_move_defenders":
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_not_implemented_yet"));
						break;
					case "admin_setcastle":
						if ((target == null) || (target.getClan() == null))
						{
							player.sendPacket(SystemMessageId.INVALID_TARGET);
						}
						else
						{
							castle.setOwner(target.getClan());
						}
						break;
					case "admin_removecastle":
						var clan = ClanTable.getInstance().getClan(castle.getOwnerId());
						if (clan != null)
						{
							castle.removeOwner(clan);
						}
						else
						{
							player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unable_remove_castle"));
						}
						break;
					case "admin_setsiegetime":
						if (st.hasMoreTokens())
						{
							var cal = Calendar.getInstance();
							cal.setTimeInMillis(castle.getSiegeDate().getTimeInMillis());
							
							val = st.nextToken();
							
							if ("month".equals(val))
							{
								var month = cal.get(Calendar.MONTH) + Integer.parseInt(st.nextToken());
								if ((cal.getActualMinimum(Calendar.MONTH) > month) || (cal.getActualMaximum(Calendar.MONTH) < month))
								{
									player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unable_change_siege_date_incorrect_month").replace("%i%", cal.getActualMinimum(Calendar.MONTH) + "").replace("%s%", cal.getActualMaximum(Calendar.MONTH) + ""));
									return false;
								}
								cal.set(Calendar.MONTH, month);
							}
							else if ("day".equals(val))
							{
								var day = Integer.parseInt(st.nextToken());
								if ((cal.getActualMinimum(Calendar.DAY_OF_MONTH) > day) || (cal.getActualMaximum(Calendar.DAY_OF_MONTH) < day))
								{
									player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unable_change_siege_date_incorrect_day").replace("%i%", cal.getActualMinimum(Calendar.DAY_OF_MONTH) + "").replace("%s%", cal.getActualMaximum(Calendar.DAY_OF_MONTH) + ""));
									return false;
								}
								cal.set(Calendar.DAY_OF_MONTH, day);
							}
							else if ("hour".equals(val))
							{
								var hour = Integer.parseInt(st.nextToken());
								if ((cal.getActualMinimum(Calendar.HOUR_OF_DAY) > hour) || (cal.getActualMaximum(Calendar.HOUR_OF_DAY) < hour))
								{
									player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unable_change_siege_date_incorrect_hour").replace("%i%", cal.getActualMinimum(Calendar.HOUR_OF_DAY) + "").replace("%s%", cal.getActualMaximum(Calendar.HOUR_OF_DAY) + ""));
									return false;
								}
								cal.set(Calendar.HOUR_OF_DAY, hour);
							}
							else if ("min".equals(val))
							{
								var min = Integer.parseInt(st.nextToken());
								if ((cal.getActualMinimum(Calendar.MINUTE) > min) || (cal.getActualMaximum(Calendar.MINUTE) < min))
								{
									player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unable_change_siege_date_incorrect_minute").replace("%i%", cal.getActualMinimum(Calendar.MINUTE) + "").replace("%s%", cal.getActualMaximum(Calendar.MINUTE) + ""));
									return false;
								}
								cal.set(Calendar.MINUTE, min);
							}
							
							if (cal.getTimeInMillis() < Calendar.getInstance().getTimeInMillis())
							{
								player.sendAdminMessage("Unable to change Siege Date");
							}
							else if (cal.getTimeInMillis() != castle.getSiegeDate().getTimeInMillis())
							{
								castle.getSiegeDate().setTimeInMillis(cal.getTimeInMillis());
								castle.getSiege().saveSiegeDate();
								player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_castle_siege_time_for_castle_has_been_changed").replace("%i%", castle.getName() + ""));
							}
						}
						showSiegeTimePage(player, castle);
						break;
					case "admin_spawn_doors":
						castle.spawnDoor();
						break;
					case "admin_startsiege":
						castle.getSiege().startSiege();
						break;
					default:
						showSiegePage(player, castle.getName());
						break;
				}
			}
		}
		else
		{
			showCastleSelectPage(player);
		}
		return true;
	}
	
	private void showCastleSelectPage(L2PcInstance player)
	{
		var i = 0;
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/castles.htm");
		
		var sb = new StringBuilder(500);
		for (var castle : CastleManager.getInstance().getCastles())
		{
			if (castle != null)
			{
				StringUtil.append(sb, "<td fixwidth=90><a action=\"bypass -h admin_siege ", castle.getName(), "\">", castle.getName(), "</a></td>");
				i++;
			}
			if (i > 2)
			{
				sb.append("</tr><tr>");
				i = 0;
			}
		}
		html.replace("%castles%", sb.toString());
		sb.setLength(0);
		i = 0;
		for (var hall : CHSiegeManager.getInstance().getConquerableHalls().values())
		{
			if (hall != null)
			{
				StringUtil.append(sb, "<td fixwidth=90><a action=\"bypass -h admin_chsiege_siegablehall ", String.valueOf(hall.getId()), "\">", hall.getName(), "</a></td>");
				i++;
			}
			if (i > 1)
			{
				sb.append("</tr><tr>");
				i = 0;
			}
		}
		html.replace("%siegableHalls%", sb.toString());
		sb.setLength(0);
		i = 0;
		for (var clanhall : ClanHallManager.getInstance().getClanHalls().values())
		{
			if (clanhall != null)
			{
				StringUtil.append(sb, "<td fixwidth=134><a action=\"bypass -h admin_clanhall ", String.valueOf(clanhall.getId()), "\">", clanhall.getName(), "</a></td>");
				i++;
			}
			if (i > 1)
			{
				sb.append("</tr><tr>");
				i = 0;
			}
		}
		html.replace("%clanhalls%", sb.toString());
		sb.setLength(0);
		i = 0;
		for (var clanhall : ClanHallManager.getInstance().getFreeClanHalls().values())
		{
			if (clanhall != null)
			{
				StringUtil.append(sb, "<td fixwidth=134><a action=\"bypass -h admin_clanhall ", String.valueOf(clanhall.getId()), "\">", clanhall.getName(), "</a></td>");
				i++;
			}
			if (i > 1)
			{
				sb.append("</tr><tr>");
				i = 0;
			}
		}
		html.replace("%freeclanhalls%", sb.toString());
		player.sendPacket(html);
	}
	
	private void showSiegePage(L2PcInstance player, String castleName)
	{
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/castle.htm");
		
		html.replace("%castleName%", castleName);
		player.sendPacket(html);
	}
	
	private void showSiegeTimePage(L2PcInstance player, Castle castle)
	{
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/castlesiegetime.htm");
		
		html.replace("%castleName%", castle.getName());
		html.replace("%time%", castle.getSiegeDate().getTime().toString());
		var newDay = Calendar.getInstance();
		var isSunday = false;
		if (newDay.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
		{
			isSunday = true;
		}
		else
		{
			newDay.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		}
		
		if (!SevenSigns.getInstance().isDateInSealValidPeriod(newDay))
		{
			newDay.add(Calendar.DAY_OF_MONTH, 7);
		}
		
		if (isSunday)
		{
			html.replace("%sundaylink%", String.valueOf(newDay.get(Calendar.DAY_OF_YEAR)));
			html.replace("%sunday%", String.valueOf(newDay.get(Calendar.MONTH) + "/" + String.valueOf(newDay.get(Calendar.DAY_OF_MONTH))));
			newDay.add(Calendar.DAY_OF_MONTH, 13);
			html.replace("%saturdaylink%", String.valueOf(newDay.get(Calendar.DAY_OF_YEAR)));
			html.replace("%saturday%", String.valueOf(newDay.get(Calendar.MONTH) + "/" + String.valueOf(newDay.get(Calendar.DAY_OF_MONTH))));
		}
		else
		{
			html.replace("%saturdaylink%", String.valueOf(newDay.get(Calendar.DAY_OF_YEAR)));
			html.replace("%saturday%", String.valueOf(newDay.get(Calendar.MONTH) + "/" + String.valueOf(newDay.get(Calendar.DAY_OF_MONTH))));
			newDay.add(Calendar.DAY_OF_MONTH, 1);
			html.replace("%sundaylink%", String.valueOf(newDay.get(Calendar.DAY_OF_YEAR)));
			html.replace("%sunday%", String.valueOf(newDay.get(Calendar.MONTH) + "/" + String.valueOf(newDay.get(Calendar.DAY_OF_MONTH))));
		}
		player.sendPacket(html);
	}
	
	private void showClanHallPage(L2PcInstance player, ClanHall clanhall)
	{
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/clanhall.htm");
		
		html.replace("%clanhallName%", clanhall.getName());
		html.replace("%clanhallId%", String.valueOf(clanhall.getId()));
		var owner = ClanTable.getInstance().getClan(clanhall.getOwnerId());
		html.replace("%clanhallOwner%", (owner == null) ? "None" : owner.getName());
		player.sendPacket(html);
	}
	
	private void showSiegableHallPage(L2PcInstance player, SiegableHall hall)
	{
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/siegablehall.htm");
		
		html.replace("%clanhallId%", String.valueOf(hall.getId()));
		html.replace("%clanhallName%", hall.getName());
		if (hall.getOwnerId() > 0)
		{
			var owner = ClanTable.getInstance().getClan(hall.getOwnerId());
			html.replace("%clanhallOwner%", (owner != null) ? owner.getName() : "" + MessagesData.getInstance().getMessage(player, "admin_no_owner") + "");
		}
		else
		{
			html.replace("%clanhallOwner%", "" + MessagesData.getInstance().getMessage(player, "admin_no_owner") + "");
		}
		player.sendPacket(html);
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
