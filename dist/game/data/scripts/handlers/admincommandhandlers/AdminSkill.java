/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.commons.math.MathUtil;
import com.l2jserver.gameserver.data.xml.impl.ClassListData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.SkillData;
import com.l2jserver.gameserver.data.xml.impl.SkillTreesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.network.serverpackets.PledgeSkillList;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * Skill commands.
 */
public class AdminSkill implements IAdminCommandHandler
{
	private static final int PAGE_LIMIT = 10;
	
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_show_skills",
		"admin_remove_skills",
		"admin_skill_list",
		"admin_skill_index",
		"admin_add_skill",
		"admin_remove_skill",
		"admin_get_skills",
		"admin_reset_skills",
		"admin_give_all_skills",
		"admin_give_all_skills_fs",
		"admin_give_clan_skills",
		"admin_give_all_clan_skills",
		"admin_remove_all_skills",
		"admin_add_clan_skill",
		"admin_setskill"
	};
	
	private static Skill[] ADMIN_SKILLS;
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.equals("admin_show_skills"))
		{
			showMainPage(player);
		}
		else if (command.startsWith("admin_remove_skills"))
		{
			try
			{
				var val = command.substring(20);
				removeSkillsPage(player, Integer.parseInt(val));
			}
			catch (StringIndexOutOfBoundsException e)
			{
			}
		}
		else if (command.startsWith("admin_skill_list"))
		{
			AdminHtml.showAdminHtml(player, "submenu/skills.htm");
		}
		else if (command.startsWith("admin_skill_index"))
		{
			try
			{
				var val = command.substring(18);
				AdminHtml.showAdminHtml(player, "submenu/skills/" + val + ".htm");
			}
			catch (StringIndexOutOfBoundsException e)
			{
			}
		}
		else if (command.startsWith("admin_add_skill"))
		{
			try
			{
				var val = command.substring(15);
				adminAddSkill(player, val);
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_add_skill"));
			}
		}
		else if (command.startsWith("admin_remove_skill"))
		{
			try
			{
				var id = command.substring(19);
				var idval = Integer.parseInt(id);
				adminRemoveSkill(player, idval);
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_remove_skill"));
			}
		}
		else if (command.equals("admin_get_skills"))
		{
			adminGetSkills(player);
		}
		else if (command.equals("admin_reset_skills"))
		{
			adminResetSkills(player);
		}
		else if (command.equals("admin_give_all_skills"))
		{
			adminGiveAllSkills(player, false);
		}
		else if (command.equals("admin_give_all_skills_fs"))
		{
			adminGiveAllSkills(player, true);
		}
		else if (command.equals("admin_give_clan_skills"))
		{
			adminGiveClanSkills(player, false);
		}
		else if (command.equals("admin_give_all_clan_skills"))
		{
			adminGiveClanSkills(player, true);
		}
		else if (command.equals("admin_remove_all_skills"))
		{
			var object = player.getTarget();
			if ((object == null) || !object.isPlayer())
			{
				player.sendPacket(SystemMessageId.INVALID_TARGET);
				return false;
			}
			var target = object.getActingPlayer();
			for (var skill : target.getAllSkills())
			{
				target.removeSkill(skill);
			}
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_have_removed_all_skills_from_target").replace("%i%", target.getName() + ""));
			target.sendMessage(MessagesData.getInstance().getMessage(target, "admin_removed_all_skills_from_you"));
			target.sendSkillList();
			target.broadcastUserInfo();
		}
		else if (command.startsWith("admin_add_clan_skill"))
		{
			try
			{
				var val = command.split(" ");
				adminAddClanSkill(player, Integer.parseInt(val[1]), Integer.parseInt(val[2]));
			}
			catch (Exception e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_add_clan_skill"));
			}
		}
		else if (command.startsWith("admin_setskill"))
		{
			var split = command.split(" ");
			var id = Integer.parseInt(split[1]);
			var lvl = Integer.parseInt(split[2]);
			var skill = SkillData.getInstance().getSkill(id, lvl);
			player.addSkill(skill);
			player.sendSkillList();
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_added_yourself_skill").replace("%i%", skill.getName() + "").replace("%s%", id + "").replace("%n%", lvl + ""));
		}
		return true;
	}
	
	private void adminGiveAllSkills(L2PcInstance player, boolean includedByFs)
	{
		var object = player.getTarget();
		if ((object == null) || !object.isPlayer())
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		var target = object.getActingPlayer();
		player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_gave_skills_target").replace("%i%", target.giveAvailableSkills(includedByFs, true) + "").replace("%s%", target.getName() + ""));
		target.sendSkillList();
	}
	
	private void adminGiveClanSkills(L2PcInstance player, boolean includeSquad)
	{
		var object = player.getTarget();
		if ((object == null) || !object.isPlayer())
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		
		var target = object.getActingPlayer();
		var clan = target.getClan();
		
		if (clan == null)
		{
			player.sendPacket(SystemMessageId.TARGET_MUST_BE_IN_CLAN);
			return;
		}
		
		if (!target.isClanLeader())
		{
			final SystemMessage sm = SystemMessage.getSystemMessage(SystemMessageId.S1_IS_NOT_A_CLAN_LEADER);
			sm.addString(target.getName());
			player.sendPacket(sm);
		}
		
		var skills = SkillTreesData.getInstance().getMaxPledgeSkills(clan, includeSquad);
		for (var s : skills.values())
		{
			clan.addNewSkill(SkillData.getInstance().getSkill(s.getSkillId(), s.getSkillLevel()));
		}
		
		// Notify target and active char
		clan.broadcastToOnlineMembers(new PledgeSkillList(clan));
		for (var member : clan.getOnlineMembers(0))
		{
			member.sendSkillList();
		}
		player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_gave_skills_target_clan").replace("%i%", skills.size() + "").replace("%s%", target.getName() + "").replace("%n%", clan.getName() + ""));
		target.sendMessage(MessagesData.getInstance().getMessage(target, "admin_you_clan_received_skills").replace("%i%", skills.size() + ""));
	}
	
	private static void removeSkillsPage(L2PcInstance player, int page)
	{
		var object = player.getTarget();
		if ((object == null) || !object.isPlayer())
		{
			player.sendPacket(SystemMessageId.TARGET_IS_INCORRECT);
			return;
		}
		
		var target = object.getActingPlayer();
		
		List<Skill> skills = new ArrayList<>(player.getSkills().values());
		
		var max = MathUtil.countPagesNumber(skills.size(), PAGE_LIMIT);
		
		skills = skills.subList((page - 1) * PAGE_LIMIT, Math.min(page * PAGE_LIMIT, skills.size()));
		
		var sb = new StringBuilder(3000);
		StringUtil.append(sb, "<html><body><table width=270><tr><td width=45><button value=\"Main\" action=\"bypass -h admin_admin\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td><td width=180><center>Delete Skills Menu</center></td><td width=45><button value=\"Back\" action=\"bypass -h admin_show_skills\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td></tr></table><br><br><center>Editing <font color=\"LEVEL\">", target.getName(), "</font>, ", ClassListData.getInstance().getClass(target.getClassId()).getClientCode(), " lvl ", target.getLevel(), ".<br><center><table width=270><tr>");
		
		for (var i = 0; i < max; i++)
		{
			var pagenr = i + 1;
			if (page == pagenr)
			{
				StringUtil.append(sb, "<td>", pagenr, "</td>");
			}
			else
			{
				StringUtil.append(sb, "<td><a action=\"bypass -h admin_remove_skills ", pagenr, "\">", pagenr, "</a></td>");
			}
		}
		
		sb.append("</tr></table></center><br><table width=270><tr><td width=80>Name:</td><td width=60>Level:</td><td width=40>Id:</td></tr>");
		
		for (var skill : skills)
		{
			StringUtil.append(sb, "<tr><td width=80><a action=\"bypass -h admin_remove_skill ", skill.getId(), "\">", skill.getName(), "</a></td><td width=60>", skill.getLevel(), "</td><td width=40>", skill.getId(), "</td></tr>");
		}
		
		sb.append("</table><br><center><table width=200><tr><td width=50 align=right>Id: </td><td><edit var=\"id_to_remove\" width=55></td><td width=100><button value=\"Remove skill\" action=\"bypass -h admin_remove_skill $id_to_remove\" width=95 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td></tr><tr><td></td><td></td><td><button value=\"Back to stats\" action=\"bypass -h admin_current_player\" width=95 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td></tr></table></center></body></html>");
		
		var html = new NpcHtmlMessage(0);
		html.setHtml(sb.toString());
		player.sendPacket(html);
	}
	
	private void showMainPage(L2PcInstance player)
	{
		var object = player.getTarget();
		if ((object == null) || !object.isPlayer())
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		var target = object.getActingPlayer();
		
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/charskills.htm");
		
		html.replace("%name%", target.getName());
		html.replace("%level%", String.valueOf(target.getLevel()));
		html.replace("%class%", ClassListData.getInstance().getClass(target.getClassId()).getClientCode());
		player.sendPacket(html);
	}
	
	private void adminGetSkills(L2PcInstance player)
	{
		var object = player.getTarget();
		if ((object == null) || !object.isPlayer())
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		var target = object.getActingPlayer();
		if (target.getName().equals(player.getName()))
		{
			target.sendPacket(SystemMessageId.YOU_CANNOT_USE_THIS_ON_YOURSELF);
		}
		else
		{
			var skills = target.getAllSkills().toArray(new Skill[target.getAllSkills().size()]);
			ADMIN_SKILLS = player.getAllSkills().toArray(new Skill[player.getAllSkills().size()]);
			for (var skill : ADMIN_SKILLS)
			{
				player.removeSkill(skill);
			}
			for (var skill : skills)
			{
				player.addSkill(skill, true);
			}
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_now_have_all_skills_of_player").replace("%i%", target.getName() + ""));
			player.sendSkillList();
		}
		showMainPage(player);
	}
	
	private void adminResetSkills(L2PcInstance player)
	{
		var object = player.getTarget();
		if ((object == null) || !object.isPlayer())
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		var target = object.getActingPlayer();
		if (ADMIN_SKILLS == null)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_must_get_skills_someone_order_this"));
		}
		else
		{
			var skills = target.getAllSkills().toArray(new Skill[target.getAllSkills().size()]);
			for (var skill : skills)
			{
				target.removeSkill(skill);
			}
			for (var skill : player.getAllSkills())
			{
				target.addSkill(skill, true);
			}
			for (var skill : skills)
			{
				player.removeSkill(skill);
			}
			for (var skill : ADMIN_SKILLS)
			{
				player.addSkill(skill, true);
			}
			target.sendAdminMessage(MessagesData.getInstance().getMessage(target, "admin_gm_updated_your_skills").replace("%i%", player.getName() + ""));
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_now_have_all_your_skills_back"));
			ADMIN_SKILLS = null;
			player.sendSkillList();
			target.sendSkillList();
		}
		showMainPage(player);
	}
	
	private void adminAddSkill(L2PcInstance player, String val)
	{
		var object = player.getTarget();
		if ((object == null) || !object.isPlayer())
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			showMainPage(player);
			return;
		}
		var target = object.getActingPlayer();
		var st = new StringTokenizer(val);
		if (st.countTokens() != 2)
		{
			showMainPage(player);
		}
		else
		{
			Skill skill = null;
			try
			{
				var id = st.nextToken();
				var level = st.nextToken();
				var idval = Integer.parseInt(id);
				var levelval = Integer.parseInt(level);
				skill = SkillData.getInstance().getSkill(idval, levelval);
			}
			catch (Exception ex)
			{
				
			}
			
			if (skill != null)
			{
				var name = skill.getName();
				// Player's info.
				target.sendMessage(MessagesData.getInstance().getMessage(target, "admin_gave_you_the_skill").replace("%i%", name + ""));
				target.addSkill(skill, true);
				target.sendSkillList();
				// Admin info.
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_gave_the_skill_to_player").replace("%i%", name + "").replace("%s%", target.getName() + ""));
				player.sendSkillList();
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_error_there_is_such_skill"));
			}
			showMainPage(player); // Back to start
		}
	}
	
	private void adminRemoveSkill(L2PcInstance player, int id)
	{
		var object = player.getTarget();
		if ((object == null) || !object.isPlayer())
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		var target = object.getActingPlayer();
		var skill = SkillData.getInstance().getSkill(id, target.getSkillLevel(id));
		if (skill != null)
		{
			var skillname = skill.getName();
			target.sendMessage(MessagesData.getInstance().getMessage(target, "admin_removed_skill_from_your_skills_list").replace("%i%", skillname + ""));
			target.removeSkill(skill);
			// Admin information
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_removed_skill_from_player").replace("%i%", skillname + "").replace("%s%", target.getName() + ""));
			player.sendSkillList();
		}
		else
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_error_there_is_such_skill"));
		}
		removeSkillsPage(player, 0); // Back to previous page
	}
	
	private void adminAddClanSkill(L2PcInstance player, int id, int level)
	{
		var object = player.getTarget();
		if ((object == null) || !object.isPlayer())
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			showMainPage(player);
			return;
		}
		var target = object.getActingPlayer();
		if (!target.isClanLeader())
		{
			var sm = SystemMessage.getSystemMessage(SystemMessageId.S1_IS_NOT_A_CLAN_LEADER);
			sm.addString(target.getName());
			player.sendPacket(sm);
			showMainPage(player);
			return;
		}
		if ((id < 370) || (id > 391) || (level < 1) || (level > 3))
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_add_clan_skill"));
			showMainPage(player);
			return;
		}
		
		var skill = SkillData.getInstance().getSkill(id, level);
		if (skill == null)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_error_there_is_such_skill"));
			return;
		}
		
		var skillname = skill.getName();
		var sm = SystemMessage.getSystemMessage(SystemMessageId.CLAN_SKILL_S1_ADDED);
		sm.addSkillName(skill);
		target.sendPacket(sm);
		var clan = target.getClan();
		clan.broadcastToOnlineMembers(sm);
		clan.addNewSkill(skill);
		player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_gave_the_clan_skill_to_the_clan").replace("%i%", skillname + "").replace("%s%", clan.getName() + ""));
		
		clan.broadcastToOnlineMembers(new PledgeSkillList(clan));
		for (var member : clan.getOnlineMembers(0))
		{
			member.sendSkillList();
		}
		showMainPage(player);
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
