/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.SevenSigns;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.NpcData;
import com.l2jserver.gameserver.data.xml.impl.SpawnData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.DayNightSpawnManager;
import com.l2jserver.gameserver.instancemanager.InstanceManager;
import com.l2jserver.gameserver.instancemanager.QuestManager;
import com.l2jserver.gameserver.instancemanager.RaidBossSpawnManager;
import com.l2jserver.gameserver.model.AutoSpawnHandler;
import com.l2jserver.gameserver.model.L2Spawn;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.templates.L2NpcTemplate;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.Broadcast;

/**
 * Spawn commands.
 */
public class AdminSpawn implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_show_spawns",
		"admin_spawn",
		"admin_spawn_monster",
		"admin_spawn_index",
		"admin_unspawnall",
		"admin_respawnall",
		"admin_spawn_reload",
		"admin_npc_index",
		"admin_spawn_once",
		"admin_show_npcs",
		"admin_spawnnight",
		"admin_spawnday",
		"admin_instance_spawns",
		"admin_list_spawns",
		"admin_list_positions"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.equals("admin_show_spawns"))
		{
			AdminHtml.showAdminHtml(player, "submenu/spawns.htm");
		}
		else if (command.startsWith("admin_spawn_index"))
		{
			var st = new StringTokenizer(command, " ");
			try
			{
				st.nextToken();
				var level = Integer.parseInt(st.nextToken());
				var from = 0;
				try
				{
					from = Integer.parseInt(st.nextToken());
				}
				catch (NoSuchElementException nsee)
				{
				}
				showMonsters(player, level, from);
			}
			catch (Exception ex)
			{
				AdminHtml.showAdminHtml(player, "submenu/spawns.htm");
			}
		}
		else if (command.equals("admin_show_npcs"))
		{
			AdminHtml.showAdminHtml(player, "submenu/npcs.htm");
		}
		else if (command.startsWith("admin_npc_index"))
		{
			var st = new StringTokenizer(command, " ");
			try
			{
				st.nextToken();
				var letter = st.nextToken();
				var from = 0;
				try
				{
					from = Integer.parseInt(st.nextToken());
				}
				catch (NoSuchElementException nsee)
				{
				}
				showNpcs(player, letter, from);
			}
			catch (Exception ex)
			{
				AdminHtml.showAdminHtml(player, "submenu/npcs.htm");
			}
		}
		else if (command.startsWith("admin_instance_spawns"))
		{
			var st = new StringTokenizer(command, " ");
			try
			{
				st.nextToken();
				var instance = Integer.parseInt(st.nextToken());
				if (instance >= 300000)
				{
					var sb = new StringBuilder(500 + 1000);
					StringUtil.append(sb, "<html><table width=\"100%\"><tr><td width=45><button value=\"Main\" action=\"bypass -h admin_admin\" width=45 height=21 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td><td width=180><center>", "<font color=\"LEVEL\">Spawns for " + instance
						+ "</font>", "</td><td width=45><button value=\"Back\" action=\"bypass -h admin_current_player\" width=45 height=21 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"></td></tr></table><br>", "<table width=\"100%\"><tr><td width=200>NpcName</td><td width=70>Action</td></tr>");
					var counter = 0;
					var skiped = 0;
					var inst = InstanceManager.getInstance().getInstance(instance);
					if (inst != null)
					{
						for (var npc : inst.getNpcs())
						{
							if (!npc.isDead())
							{
								// Only 50 because of client html limitation
								if (counter < 50)
								{
									StringUtil.append(sb, "<tr><td>" + npc.getName() + "</td><td>", "<a action=\"bypass -h admin_move_to " + npc.getX() + " " + npc.getY() + " " + npc.getZ() + "\">Go</a>", "</td></tr>");
									counter++;
								}
								else
								{
									skiped++;
								}
							}
						}
						StringUtil.append(sb, "<tr><td>Skipped:</td><td>" + String.valueOf(skiped) + "</td></tr></table></body></html>");
						
						var html = new NpcHtmlMessage(0);
						html.setHtml(sb.toString());
						player.sendPacket(html);
					}
					else
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_cannot_find_instance").replace("%s%", instance + ""));
					}
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_invalid_instance_number"));
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_instance_spawns"));
			}
		}
		else if (command.startsWith("admin_unspawnall"))
		{
			Broadcast.toAllOnlinePlayers(SystemMessage.getSystemMessage(SystemMessageId.NPC_SERVER_NOT_OPERATING));
			RaidBossSpawnManager.getInstance().cleanUp();
			DayNightSpawnManager.getInstance().cleanUp();
			L2World.getInstance().deleteVisibleNpcSpawns();
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_npc_unspawn_completed"));
		}
		else if (command.startsWith("admin_spawnday"))
		{
			DayNightSpawnManager.getInstance().spawnDayCreatures();
		}
		else if (command.startsWith("admin_spawnnight"))
		{
			DayNightSpawnManager.getInstance().spawnNightCreatures();
		}
		else if (command.startsWith("admin_respawnall") || command.startsWith("admin_spawn_reload"))
		{
			// make sure all spawns are deleted
			RaidBossSpawnManager.getInstance().cleanUp();
			DayNightSpawnManager.getInstance().cleanUp();
			L2World.getInstance().deleteVisibleNpcSpawns();
			// now respawn all
			NpcData.getInstance().load();
			SpawnData.getInstance().load();
			RaidBossSpawnManager.getInstance().load();
			AutoSpawnHandler.getInstance().reload();
			SevenSigns.getInstance().spawnSevenSignsNPC();
			QuestManager.getInstance().reloadAllScripts();
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_npc_respawn_completed"));
		}
		else if (command.startsWith("admin_spawn_monster") || command.startsWith("admin_spawn"))
		{
			var st = new StringTokenizer(command, " ");
			try
			{
				var cmd = st.nextToken();
				var id = st.nextToken();
				var respawnTime = 0;
				var mobCount = 1;
				if (st.hasMoreTokens())
				{
					mobCount = Integer.parseInt(st.nextToken());
				}
				if (st.hasMoreTokens())
				{
					respawnTime = Integer.parseInt(st.nextToken());
				}
				if (cmd.equalsIgnoreCase("admin_spawn_once"))
				{
					spawnMonster(player, id, respawnTime, mobCount, false);
				}
				else
				{
					spawnMonster(player, id, respawnTime, mobCount, true);
				}
			}
			catch (Exception ex)
			{ // Case of wrong or missing monster data
				AdminHtml.showAdminHtml(player, "submenu/spawns.htm");
			}
		}
		else if (command.startsWith("admin_list_spawns") || command.startsWith("admin_list_positions"))
		{
			var npcId = 0;
			var teleportIndex = -1;
			try
			{ // admin_list_spawns x[xxxx] x[xx]
				var params = command.split(" ");
				var pattern = Pattern.compile("[0-9]*");
				var regexp = pattern.matcher(params[1]);
				if (regexp.matches())
				{
					npcId = Integer.parseInt(params[1]);
				}
				else
				{
					params[1] = params[1].replace('_', ' ');
					npcId = NpcData.getInstance().getTemplateByName(params[1]).getId();
				}
				if (params.length > 2)
				{
					teleportIndex = Integer.parseInt(params[2]);
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_list_spawns"));
			}
			if (command.startsWith("admin_list_positions"))
			{
				findNPCInstances(player, npcId, teleportIndex, true);
			}
			else
			{
				findNPCInstances(player, npcId, teleportIndex, false);
			}
		}
		return true;
	}
	
	private void findNPCInstances(L2PcInstance player, int npcId, int teleportIndex, boolean showposition)
	{
		var index = 0;
		for (var spawn : SpawnData.getInstance().getSpawns(npcId))
		{
			index++;
			var npc = spawn.getLastSpawn();
			if (teleportIndex > -1)
			{
				if (teleportIndex == index)
				{
					if (showposition && (npc != null))
					{
						player.teleToLocation(npc.getLocation(), true);
					}
					else
					{
						player.teleToLocation(spawn.getLocation(), true);
					}
				}
			}
			else
			{
				if (showposition && (npc != null))
				{
					player.sendAdminMessage(MessagesData.getInstance()
						.getMessage(player, "admin_npc_instance")
						.replace("%s%", index + "")
						.replace("%i%", spawn.getTemplate().getName() + "")
						.replace("%n%", spawn + "")
						.replace("%t%", npc.getX() + "")
						.replace("%k%", npc.getY()
							+ "")
						.replace("%f%", npc.getZ() + ""));
				}
				else
				{
					player.sendAdminMessage(MessagesData.getInstance()
						.getMessage(player, "admin_npc_instance_spawn")
						.replace("%s%", index + "")
						.replace("%i%", spawn.getTemplate().getName() + "")
						.replace("%n%", spawn + "")
						.replace("%t%", spawn.getX() + "")
						.replace("%k%", spawn.getY()
							+ "")
						.replace("%f%", spawn.getZ() + ""));
				}
			}
		}
		
		if (index == 0)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_no_current_spawns_found").replace("%s%", getClass().getSimpleName() + ""));
		}
	}
	
	private void spawnMonster(L2PcInstance player, String monsterId, int respawnTime, int mobCount, boolean permanent)
	{
		var target = player.getTarget();
		if (target == null)
		{
			target = player;
		}
		
		L2NpcTemplate template;
		if (monsterId.matches("[0-9]*"))
		{
			// First parameter was an ID number
			template = NpcData.getInstance().getTemplate(Integer.parseInt(monsterId));
		}
		else
		{
			// First parameter wasn't just numbers so go by name not ID
			template = NpcData.getInstance().getTemplateByName(monsterId.replace('_', ' '));
		}
		
		try
		{
			var spawn = new L2Spawn(template);
			spawn.setX(target.getX());
			spawn.setY(target.getY());
			spawn.setZ(target.getZ());
			spawn.setAmount(mobCount);
			spawn.setHeading(player.getHeading());
			spawn.setRespawnDelay(respawnTime);
			if (player.getInstanceId() > 0)
			{
				spawn.setInstanceId(player.getInstanceId());
				permanent = false;
			}
			else
			{
				spawn.setInstanceId(0);
			}
			// TODO add checks for GrandBossSpawnManager
			if (RaidBossSpawnManager.getInstance().isDefined(spawn.getId()))
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_cannot_spawn_another_instance").replace("%s%", template.getName() + ""));
			}
			else
			{
				if (template.isType("L2RaidBoss"))
				{
					spawn.setRespawnMinDelay(43200);
					spawn.setRespawnMaxDelay(129600);
					RaidBossSpawnManager.getInstance().addNewSpawn(spawn, 0, template.getBaseHpMax(), template.getBaseMpMax(), permanent);
				}
				else
				{
					SpawnData.getInstance().addNewSpawn(spawn, permanent);
					spawn.init();
				}
				if (!permanent)
				{
					spawn.stopRespawn();
				}
				player.sendAdminMessage("Created " + template.getName() + " on " + target.getObjectId());
			}
		}
		catch (Exception ex)
		{
			player.sendPacket(SystemMessageId.YOU_TARGET_CANNOT_BE_FOUND);
		}
	}
	
	private void showMonsters(L2PcInstance player, int level, int from)
	{
		var mobs = NpcData.getInstance().getAllMonstersOfLevel(level);
		var sb = new StringBuilder(200 + (mobs.size() * 100));
		StringUtil.append(sb, "<html><title>Spawn Monster:</title><body><p> Level : ", level, "<br>Total Npc's : ", mobs.size(), "<br>");
		
		// Loop
		var i = from;
		for (var j = 0; (i < mobs.size()) && (j < 50); i++, j++)
		{
			StringUtil.append(sb, "<a action=\"bypass -h admin_spawn_monster ", mobs.get(i).getId(), "\">", mobs.get(i).getName(), "</a><br1>");
		}
		
		if (i == mobs.size())
		{
			sb.append("<br><center><button value=\"Back\" action=\"bypass -h admin_show_spawns\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></center></body></html>");
		}
		else
		{
			StringUtil
				.append(sb, "<br><center><button value=\"Next\" action=\"bypass -h admin_spawn_index ", level, " ", i, "\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"><button value=\"Back\" action=\"bypass -h admin_show_spawns\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></center></body></html>");
		}
		
		var html = new NpcHtmlMessage(0);
		html.setHtml(sb.toString());
		player.sendPacket(html);
	}
	
	private void showNpcs(L2PcInstance player, String starting, int from)
	{
		var mobs = NpcData.getInstance().getAllNpcStartingWith(starting);
		var sb = new StringBuilder(200 + (mobs.size() * 100));
		StringUtil.append(sb, "<html><title>Spawn Monster:</title><body><p> There are ", mobs.size(), " Npcs whose name starts with ", starting, ":<br>");
		// Loop
		var i = from;
		for (var j = 0; (i < mobs.size()) && (j < 50); i++, j++)
		{
			StringUtil.append(sb, "<a action=\"bypass -h admin_spawn_monster ", mobs.get(i).getId(), "\">", mobs.get(i).getName(), "</a><br1>");
		}
		
		if (i == mobs.size())
		{
			sb.append("<br><center><button value=\"Back\" action=\"bypass -h admin_show_npcs\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></center></body></html>");
		}
		else
		{
			StringUtil
				.append(sb, "<br><center><button value=\"Next\" action=\"bypass -h admin_npc_index ", starting, " ", i, "\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"><button value=\"Back\" action=\"bypass -h admin_show_npcs\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></center></body></html>");
		}
		var html = new NpcHtmlMessage(0);
		html.setHtml(sb.toString());
		player.sendPacket(html);
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
