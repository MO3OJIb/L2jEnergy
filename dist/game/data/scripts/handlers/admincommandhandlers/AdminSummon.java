/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import com.l2jserver.gameserver.data.xml.impl.AdminData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.AdminCommandHandler;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * Summon commands.
 * @author poltomb
 */
public class AdminSummon implements IAdminCommandHandler
{
	public static final String[] ADMIN_COMMANDS =
	{
		"admin_summon"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		int id;
		var count = 1;
		var data = command.split(" ");
		try
		{
			id = Integer.parseInt(data[1]);
			if (data.length > 2)
			{
				count = Integer.parseInt(data[2]);
			}
		}
		catch (NumberFormatException nfe)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_incorrect_format_command_summon"));
			return false;
		}
		
		String subCommand;
		if (id < 1000000)
		{
			subCommand = "admin_create_item";
			if (!AdminData.getInstance().hasAccess(subCommand, player.getAccessLevel()))
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_don_have_access_right_use_command"));
				return false;
			}
			var ach = AdminCommandHandler.getInstance().getHandler(subCommand);
			ach.useAdminCommand(subCommand + " " + id + " " + count, player);
		}
		else
		{
			subCommand = "admin_spawn_once";
			if (!AdminData.getInstance().hasAccess(subCommand, player.getAccessLevel()))
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_don_have_access_right_use_command"));
				return false;
			}
			var ach = AdminCommandHandler.getInstance().getHandler(subCommand);
			
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_this_is_only_temporary_spawn"));
			id -= 1000000;
			ach.useAdminCommand(subCommand + " " + id + " " + count, player);
		}
		return true;
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}