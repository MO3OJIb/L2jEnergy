/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.ai.CtrlIntention;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.SpawnData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.MapRegionManager;
import com.l2jserver.gameserver.instancemanager.RaidBossSpawnManager;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.L2Spawn;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2GrandBossInstance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.instance.L2RaidBossInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * Teleport commands.
 */
public class AdminTeleport implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_show_moves",
		"admin_show_moves_other",
		"admin_show_teleport",
		"admin_teleport_to_character",
		"admin_teleportto",
		"admin_move_to",
		"admin_teleport_character",
		"admin_recall",
		"admin_walk",
		"teleportto",
		"recall",
		"admin_recall_npc",
		"admin_gonorth",
		"admin_gosouth",
		"admin_goeast",
		"admin_gowest",
		"admin_goup",
		"admin_godown",
		"admin_tele",
		"admin_teleto",
		"admin_instant_move",
		"admin_sendhome"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance activeChar)
	{
		if (command.equals("admin_teleto"))
		{
			activeChar.setTeleMode(1);
		}
		if (command.equals("admin_instant_move"))
		{
			activeChar.sendAdminMessage(MessagesData.getInstance().getMessage(activeChar, "admin_instant_move_ready"));
			activeChar.setTeleMode(1);
		}
		if (command.equals("admin_teleto r"))
		{
			activeChar.setTeleMode(2);
		}
		if (command.equals("admin_teleto end"))
		{
			activeChar.setTeleMode(0);
		}
		if (command.equals("admin_show_moves"))
		{
			AdminHtml.showAdminHtml(activeChar, "submenu/teleports.htm");
		}
		if (command.equals("admin_show_moves_other"))
		{
			AdminHtml.showAdminHtml(activeChar, "tele/other.html");
		}
		else if (command.equals("admin_show_teleport"))
		{
			showTeleportCharWindow(activeChar);
		}
		else if (command.equals("admin_recall_npc"))
		{
			recallNPC(activeChar);
		}
		else if (command.equals("admin_teleport_to_character"))
		{
			teleportToCharacter(activeChar, activeChar.getTarget());
		}
		else if (command.startsWith("admin_walk"))
		{
			try
			{
				var val = command.substring(11);
				var st = new StringTokenizer(val);
				var x = Integer.parseInt(st.nextToken());
				var y = Integer.parseInt(st.nextToken());
				var z = Integer.parseInt(st.nextToken());
				activeChar.getAI().setIntention(CtrlIntention.AI_INTENTION_MOVE_TO, new Location(x, y, z, 0));
			}
			catch (Exception ex)
			{
				ex.getMessage();
			}
		}
		else if (command.startsWith("admin_move_to"))
		{
			try
			{
				var val = command.substring(14);
				teleportTo(activeChar, val);
			}
			catch (StringIndexOutOfBoundsException e)
			{
				// Case of empty or missing coordinates
				AdminHtml.showAdminHtml(activeChar, "submenu/teleports.htm");
			}
			catch (NumberFormatException nfe)
			{
				activeChar.sendAdminMessage(MessagesData.getInstance().getMessage(activeChar, "admin_usage_move_to"));
				AdminHtml.showAdminHtml(activeChar, "submenu/teleports.htm");
			}
		}
		else if (command.startsWith("admin_teleport_character"))
		{
			try
			{
				var val = command.substring(25);
				
				teleportCharacter(activeChar, val);
			}
			catch (StringIndexOutOfBoundsException e)
			{
				activeChar.sendAdminMessage(MessagesData.getInstance().getMessage(activeChar, "admin_wrong_or_coordinates_given"));
				showTeleportCharWindow(activeChar); // back to character teleport
			}
		}
		else if (command.startsWith("admin_teleportto "))
		{
			try
			{
				var targetName = command.substring(17);
				var pl = L2World.getInstance().getPlayer(targetName);
				teleportToCharacter(activeChar, pl);
			}
			catch (StringIndexOutOfBoundsException e)
			{
			}
		}
		else if (command.startsWith("admin_recall "))
		{
			try
			{
				var param = command.split(" ");
				if (param.length != 2)
				{
					activeChar.sendAdminMessage(MessagesData.getInstance().getMessage(activeChar, "admin_usage_recall"));
					return false;
				}
				var targetName = param[1];
				var pl = L2World.getInstance().getPlayer(targetName);
				if (pl != null)
				{
					teleportCharacter(pl, activeChar.getLocation(), activeChar);
				}
				else
				{
					changeCharacterPosition(activeChar, targetName);
				}
			}
			catch (StringIndexOutOfBoundsException e)
			{
			}
		}
		else if (command.equals("admin_tele"))
		{
			showTeleportWindow(activeChar);
		}
		else if (command.startsWith("admin_go"))
		{
			var intVal = 150;
			int x = activeChar.getX(), y = activeChar.getY(), z = activeChar.getZ();
			try
			{
				var val = command.substring(8);
				var st = new StringTokenizer(val);
				var dir = st.nextToken();
				if (st.hasMoreTokens())
				{
					intVal = Integer.parseInt(st.nextToken());
				}
				if (dir.equals("east"))
				{
					x += intVal;
				}
				else if (dir.equals("west"))
				{
					x -= intVal;
				}
				else if (dir.equals("north"))
				{
					y -= intVal;
				}
				else if (dir.equals("south"))
				{
					y += intVal;
				}
				else if (dir.equals("up"))
				{
					z += intVal;
				}
				else if (dir.equals("down"))
				{
					z -= intVal;
				}
				activeChar.teleToLocation(new Location(x, y, z));
				showTeleportWindow(activeChar);
			}
			catch (Exception ex)
			{
				activeChar.sendAdminMessage(MessagesData.getInstance().getMessage(activeChar, "admin_usage_go"));
			}
		}
		else if (command.startsWith("admin_sendhome"))
		{
			var st = new StringTokenizer(command, " ");
			st.nextToken(); // Skip command.
			if (st.countTokens() > 1)
			{
				activeChar.sendAdminMessage(MessagesData.getInstance().getMessage(activeChar, "admin_usage_sendhome"));
			}
			else if (st.countTokens() == 1)
			{
				var name = st.nextToken();
				var pl = L2World.getInstance().getPlayer(name);
				if (pl == null)
				{
					activeChar.sendPacket(SystemMessageId.THAT_PLAYER_IS_NOT_ONLINE);
					return false;
				}
				teleportHome(pl);
			}
			else
			{
				var target = activeChar.getTarget();
				if (target instanceof L2PcInstance)
				{
					teleportHome(target.getActingPlayer());
				}
				else
				{
					activeChar.sendPacket(SystemMessageId.INVALID_TARGET);
				}
			}
		}
		return true;
	}
	
	private void teleportHome(L2PcInstance player)
	{
		String regionName;
		switch (player.getRace())
		{
			case ELF:
				regionName = "elf_town";
				break;
			case DARK_ELF:
				regionName = "darkelf_town";
				break;
			case ORC:
				regionName = "orc_town";
				break;
			case DWARF:
				regionName = "dwarf_town";
				break;
			case KAMAEL:
				regionName = "kamael_town";
				break;
			case HUMAN:
			default:
				regionName = "talking_island_town";
		}
		
		player.teleToLocation(MapRegionManager.getInstance().getMapRegionByName(regionName).getSpawnLoc(), true);
		player.setInstanceId(0);
		player.setIsIn7sDungeon(false);
	}
	
	private void teleportTo(L2PcInstance player, String coords)
	{
		try
		{
			var st = new StringTokenizer(coords);
			var x1 = st.nextToken();
			var x = Integer.parseInt(x1);
			var y1 = st.nextToken();
			var y = Integer.parseInt(y1);
			var z1 = st.nextToken();
			var z = Integer.parseInt(z1);
			
			player.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
			player.teleToLocation(x, y, z);
			
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_have_teleported_to").replace("%i%", coords + ""));
		}
		catch (NoSuchElementException nsee)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_wrong_or_coordinates_given"));
		}
	}
	
	private void showTeleportWindow(L2PcInstance player)
	{
		AdminHtml.showAdminHtml(player, "submenu/move.htm");
	}
	
	private void showTeleportCharWindow(L2PcInstance player)
	{
		var object = player.getTarget();
		L2PcInstance target = null;
		if (object instanceof L2PcInstance)
		{
			target = (L2PcInstance) object;
		}
		else
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		var html = new NpcHtmlMessage(0);
		
		html.setHtml("<html><title>Teleport Character</title>" + "<body>" + "The character you will teleport is " + target.getName() + "." + "<br>" + "Co-ordinate x" + "<edit var=\"char_cord_x\" width=110>" + "Co-ordinate y" + "<edit var=\"char_cord_y\" width=110>" + "Co-ordinate z"
			+ "<edit var=\"char_cord_z\" width=110>" + "<button value=\"Teleport\" action=\"bypass -h admin_teleport_character $char_cord_x $char_cord_y $char_cord_z\" width=60 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\">"
			+ "<button value=\"Teleport near you\" action=\"bypass -h admin_teleport_character " + player.getX() + " " + player.getY() + " " + player.getZ() + "\" width=115 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\">" + "<center><button value=\""
			+ MessagesData.getInstance().getMessage(player, "admin_button_back") + "\" action=\"bypass -h admin_current_player\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></center>" + "</body></html>");
		player.sendPacket(html);
	}
	
	private void teleportCharacter(L2PcInstance player, String coords)
	{
		var object = player.getTarget();
		L2PcInstance target = null;
		if (object instanceof L2PcInstance)
		{
			target = (L2PcInstance) object;
		}
		else
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		
		if (target.getObjectId() == player.getObjectId())
		{
			target.sendPacket(SystemMessageId.YOU_CANNOT_USE_THIS_ON_YOURSELF);
		}
		else
		{
			try
			{
				var st = new StringTokenizer(coords);
				var x1 = st.nextToken();
				var x = Integer.parseInt(x1);
				var y1 = st.nextToken();
				var y = Integer.parseInt(y1);
				var z1 = st.nextToken();
				var z = Integer.parseInt(z1);
				teleportCharacter(target, new Location(x, y, z), null);
			}
			catch (NoSuchElementException nsee)
			{
			}
		}
	}
	
	private void teleportCharacter(L2PcInstance target, Location loc, L2PcInstance player)
	{
		if (target != null)
		{
			// Check for jail
			if (target.isJailed())
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_sorry_player_in_jail").replace("%i%", target.getName() + ""));
			}
			else
			{
				// Set player to same instance as GM teleporting.
				if ((player != null) && (player.getInstanceId() >= 0))
				{
					target.setInstanceId(player.getInstanceId());
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_have_recalled_target").replace("%i%", target.getName() + ""));
					
				}
				else
				{
					target.setInstanceId(0);
				}
				target.sendMessage(MessagesData.getInstance().getMessage(target, "admin_is_teleporting_you"));
				target.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
				target.teleToLocation(loc, true);
			}
		}
	}
	
	private void teleportToCharacter(L2PcInstance player, L2Object object)
	{
		if (object == null)
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		
		L2PcInstance target = null;
		if (object instanceof L2PcInstance)
		{
			target = (L2PcInstance) object;
		}
		else
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
			return;
		}
		
		if (target.getObjectId() == player.getObjectId())
		{
			target.sendPacket(SystemMessageId.YOU_CANNOT_USE_THIS_ON_YOURSELF);
		}
		else
		{
			// move to targets instance
			player.setInstanceId(object.getInstanceId());
			
			var x = target.getX();
			var y = target.getY();
			var z = target.getZ();
			
			player.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
			player.teleToLocation(new Location(x, y, z), true);
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_you_have_teleported_character").replace("%i%", target.getName() + ""));
		}
	}
	
	private void changeCharacterPosition(L2PcInstance player, String name)
	{
		var x = player.getX();
		var y = player.getY();
		var z = player.getZ();
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("UPDATE characters SET x=?, y=?, z=? WHERE char_name=?"))
		{
			ps.setInt(1, x);
			ps.setInt(2, y);
			ps.setInt(3, z);
			ps.setString(4, name);
			ps.execute();
			int count = ps.getUpdateCount();
			
			if (count == 0)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_character_not_found_position_unaltered"));
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_players_position_now_set_to").replace("%i%", name + "").replace("%s%", x + "").replace("%n%", y + "").replace("%f%", z + ""));
			}
		}
		catch (Exception ex)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_while_changing_offline_character_position"));
		}
	}
	
	private void recallNPC(L2PcInstance player)
	{
		var object = player.getTarget();
		if ((object instanceof L2Npc) && !((L2Npc) object).isMinion() && !(object instanceof L2RaidBossInstance) && !(object instanceof L2GrandBossInstance))
		{
			var target = (L2Npc) object;
			var spawn = target.getSpawn();
			if (spawn == null)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_incorrect_monster_spawn"));
				return;
			}
			var respawnTime = spawn.getRespawnDelay() / 1000;
			
			target.deleteMe();
			spawn.stopRespawn();
			SpawnData.getInstance().deleteSpawn(spawn, true);
			
			try
			{
				spawn = new L2Spawn(target.getTemplate().getId());
				spawn.setX(player.getX());
				spawn.setY(player.getY());
				spawn.setZ(player.getZ());
				spawn.setAmount(1);
				spawn.setHeading(player.getHeading());
				spawn.setRespawnDelay(respawnTime);
				if (player.getInstanceId() >= 0)
				{
					spawn.setInstanceId(player.getInstanceId());
				}
				else
				{
					spawn.setInstanceId(0);
				}
				SpawnData.getInstance().addNewSpawn(spawn, true);
				spawn.init();
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_created_target_name_or_target").replace("%i%", target.getTemplate().getName() + "").replace("%s%", target.getObjectId() + ""));
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_target_not_game"));
			}
		}
		else if (object instanceof L2RaidBossInstance)
		{
			var target = (L2RaidBossInstance) object;
			var spawn = target.getSpawn();
			var curHP = target.getCurrentHp();
			var curMP = target.getCurrentMp();
			if (spawn == null)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_incorrect_raid_spawn"));
				return;
			}
			RaidBossSpawnManager.getInstance().deleteSpawn(spawn, true);
			try
			{
				final L2Spawn spawnDat = new L2Spawn(target.getId());
				spawnDat.setX(player.getX());
				spawnDat.setY(player.getY());
				spawnDat.setZ(player.getZ());
				spawnDat.setAmount(1);
				spawnDat.setHeading(player.getHeading());
				spawnDat.setRespawnMinDelay(43200);
				spawnDat.setRespawnMaxDelay(129600);
				
				RaidBossSpawnManager.getInstance().addNewSpawn(spawnDat, 0, curHP, curMP, true);
			}
			catch (Exception ex)
			{
				player.sendPacket(SystemMessageId.YOU_TARGET_CANNOT_BE_FOUND);
			}
		}
		else
		{
			player.sendPacket(SystemMessageId.INVALID_TARGET);
		}
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
