/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.Calendar;
import java.util.StringTokenizer;

import com.l2jserver.gameserver.configuration.config.TerritoryWarConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.GlobalVariablesManager;
import com.l2jserver.gameserver.instancemanager.TerritoryWarManager;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * Territory War commands
 * @author Gigiikun
 */
public class AdminTerritoryWar implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_territory_war",
		"admin_territory_war_time",
		"admin_territory_war_start",
		"admin_territory_war_end",
		"admin_territory_wards_list"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command);
		command = st.nextToken();
		
		if (command.equals("admin_territory_war"))
		{
			showMainPage(player);
		}
		else if (command.equalsIgnoreCase("admin_territory_war_time"))
		{
			if (st.hasMoreTokens())
			{
				var cal = Calendar.getInstance();
				cal.setTimeInMillis(TerritoryWarManager.getInstance().getTWStartTimeInMillis());
				
				var val = st.nextToken();
				if ("month".equals(val))
				{
					var month = cal.get(Calendar.MONTH) + Integer.parseInt(st.nextToken());
					if ((cal.getActualMinimum(Calendar.MONTH) > month) || (cal.getActualMaximum(Calendar.MONTH) < month))
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unable_change_siege_date_incorrect_month").replace("%i%", cal.getActualMinimum(Calendar.MONTH) + "").replace("%s%", cal.getActualMaximum(Calendar.MONTH) + ""));
						return false;
					}
					cal.set(Calendar.MONTH, month);
				}
				else if ("day".equals(val))
				{
					var day = Integer.parseInt(st.nextToken());
					if ((cal.getActualMinimum(Calendar.DAY_OF_MONTH) > day) || (cal.getActualMaximum(Calendar.DAY_OF_MONTH) < day))
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unable_change_siege_date_incorrect_day").replace("%i%", cal.getActualMinimum(Calendar.DAY_OF_MONTH) + "").replace("%s%", cal.getActualMaximum(Calendar.DAY_OF_MONTH) + ""));
						return false;
					}
					cal.set(Calendar.DAY_OF_MONTH, day);
				}
				else if ("hour".equals(val))
				{
					var hour = Integer.parseInt(st.nextToken());
					if ((cal.getActualMinimum(Calendar.HOUR_OF_DAY) > hour) || (cal.getActualMaximum(Calendar.HOUR_OF_DAY) < hour))
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unable_change_siege_date_incorrect_hour").replace("%i%", cal.getActualMinimum(Calendar.HOUR_OF_DAY) + "").replace("%s%", cal.getActualMaximum(Calendar.HOUR_OF_DAY) + ""));
						return false;
					}
					cal.set(Calendar.HOUR_OF_DAY, hour);
				}
				else if ("min".equals(val))
				{
					var min = Integer.parseInt(st.nextToken());
					if ((cal.getActualMinimum(Calendar.MINUTE) > min) || (cal.getActualMaximum(Calendar.MINUTE) < min))
					{
						player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unable_change_siege_date_incorrect_minute").replace("%i%", cal.getActualMinimum(Calendar.MINUTE) + "").replace("%s%", cal.getActualMaximum(Calendar.MINUTE) + ""));
						return false;
					}
					cal.set(Calendar.MINUTE, min);
				}
				
				if (cal.getTimeInMillis() < Calendar.getInstance().getTimeInMillis())
				{
					player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_unable_change_tw_date"));
				}
				else if (cal.getTimeInMillis() != TerritoryWarManager.getInstance().getTWStartTimeInMillis())
				{
					TerritoryWarManager.getInstance().setTWStartTimeInMillis(cal.getTimeInMillis());
					GlobalVariablesManager.getInstance().set(TerritoryWarManager.GLOBAL_VARIABLE, cal.getTimeInMillis());
				}
			}
			showSiegeTimePage(player);
		}
		else if (command.equalsIgnoreCase("admin_territory_war_start"))
		{
			TerritoryWarManager.getInstance().setTWStartTimeInMillis(System.currentTimeMillis());
		}
		else if (command.equalsIgnoreCase("admin_territory_war_end"))
		{
			TerritoryWarManager.getInstance().setTWStartTimeInMillis(System.currentTimeMillis() - TerritoryWarConfig.WARLENGTH);
		}
		else if (command.equalsIgnoreCase("admin_territory_wards_list"))
		{
			// build beginning of html page
			var html = new NpcHtmlMessage(0);
			var sb = new StringBuilder();
			sb.append("<html><title>Server Control Panel</title><body><br><center><font color=\"LEVEL\">Active Wards List:</font></center>");
			
			// get,build & send current Wards list
			if (TerritoryWarManager.getInstance().isTWInProgress())
			{
				var territoryWardList = TerritoryWarManager.getInstance().getAllTerritoryWards();
				for (var ward : territoryWardList)
				{
					if (ward.getNpc() != null)
					{
						sb.append("<table width=270><tr>");
						sb.append("<td width=135 ALIGN=\"LEFT\">" + ward.getNpc().getName() + "</td>");
						sb.append("<td width=135 ALIGN=\"RIGHT\"><button value=\"TeleTo\" action=\"bypass -h admin_move_to " + ward.getNpc().getX() + " " + ward.getNpc().getY() + " " + ward.getNpc().getZ() + "\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
						sb.append("</tr></table>");
					}
					else if (ward.getPlayer() != null)
					{
						sb.append("<table width=270><tr>");
						sb.append("<td width=135 ALIGN=\"LEFT\">" + ward.getPlayer().getActiveWeaponInstance().getItemName() + " - " + ward.getPlayer().getName() + "</td>");
						sb.append("<td width=135 ALIGN=\"RIGHT\"><button value=\"TeleTo\" action=\"bypass -h admin_move_to " + ward.getPlayer().getX() + " " + ward.getPlayer().getY() + " " + ward.getPlayer().getZ()
							+ "\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
						sb.append("</tr></table>");
					}
				}
				sb.append("<br><center><button value=\"Back\" action=\"bypass -h admin_territory_war\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></center></body></html>");
				html.setHtml(sb.toString());
				player.sendPacket(html);
			}
			else
			{
				sb.append("<br><br><center>The Ward List is empty!<br>TW has probably NOT started!");
				sb.append("<br><button value=\"Back\" action=\"bypass -h admin_territory_war\" width=45 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></center></body></html>");
				html.setHtml(sb.toString());
				player.sendPacket(html);
			}
		}
		return true;
	}
	
	private void showSiegeTimePage(L2PcInstance player)
	{
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/territorywartime.htm");
		
		html.replace("%time%", TerritoryWarManager.getInstance().getTWStart().getTime().toString());
		player.sendPacket(html);
	}
	
	private void showMainPage(L2PcInstance player)
	{
		AdminHtml.showAdminHtml(player, "submenu/territorywar.htm");
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
