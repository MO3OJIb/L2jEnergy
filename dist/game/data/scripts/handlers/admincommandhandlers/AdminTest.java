/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.SkillData;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.MagicSkillUse;

/**
 * Test commands
 */
public class AdminTest implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_stats",
		"admin_skill_test",
		"admin_known"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (command.equals("admin_stats"))
		{
			for (var line : ThreadPoolManager.getInstance().getStats())
			{
				player.sendMessage(line);
			}
		}
		else if (command.startsWith("admin_skill_test"))
		{
			try
			{
				var st = new StringTokenizer(command);
				st.nextToken();
				var id = Integer.parseInt(st.nextToken());
				if (command.startsWith("admin_skill_test"))
				{
					adminTestSkill(player, id, true);
				}
				else
				{
					adminTestSkill(player, id, false);
				}
			}
			catch (NumberFormatException e)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_skill_test"));
			}
			catch (NoSuchElementException nsee)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_skill_test"));
			}
		}
		else if (command.equals("admin_known on"))
		{
			GeneralConfig.CHECK_KNOWN = true;
		}
		else if (command.equals("admin_known off"))
		{
			GeneralConfig.CHECK_KNOWN = false;
		}
		return true;
	}
	
	/**
	 * @param player
	 * @param id
	 * @param msu
	 */
	private void adminTestSkill(L2PcInstance player, int id, boolean msu)
	{
		L2Character target;
		var object = player.getTarget();
		if (!(object instanceof L2Character))
		{
			target = player;
		}
		else
		{
			target = (L2Character) object;
		}
		
		var skill = SkillData.getInstance().getSkill(id, 1);
		if (skill != null)
		{
			target.setTarget(player);
			if (msu)
			{
				target.broadcastPacket(new MagicSkillUse(target, player, id, 1, skill.getHitTime(), skill.getReuseDelay()));
			}
			else
			{
				target.doCast(skill);
			}
		}
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
