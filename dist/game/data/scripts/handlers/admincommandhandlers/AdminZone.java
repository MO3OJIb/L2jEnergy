/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.admincommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.TeleportWhereType;
import com.l2jserver.gameserver.enums.ZoneId;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.instancemanager.MapRegionManager;
import com.l2jserver.gameserver.instancemanager.ZoneManager;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * Zone commands.
 */
public class AdminZone implements IAdminCommandHandler
{
	private static final String[] ADMIN_COMMANDS =
	{
		"admin_zone_check",
		"admin_zone_visual"
	};
	
	@Override
	public boolean useAdminCommand(String command, L2PcInstance player)
	{
		if (player == null)
		{
			return false;
		}
		
		var st = new StringTokenizer(command, " ");
		var actualCommand = st.nextToken();
		
		if (actualCommand.equalsIgnoreCase("admin_zone_check"))
		{
			showHtml(player);
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_map_region").replace("%i%", MapRegionManager.getInstance().getMapRegionX(player.getX()) + "").replace("%s%", MapRegionManager.getInstance().getMapRegionY(player.getY())
				+ "").replace("%t%", MapRegionManager.getInstance().getMapRegionLocId(player) + ""));
			getGeoRegionXY(player);
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_closest_town").replace("%i%", MapRegionManager.getInstance().getClosestTownName(player) + ""));
			
			Location loc;
			
			loc = MapRegionManager.getInstance().getTeleToLocation(player, TeleportWhereType.CASTLE);
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_teletolocation_castle").replace("%i%", loc.getX() + "").replace("%s%", loc.getY() + "").replace("%t%", loc.getZ() + ""));
			
			loc = MapRegionManager.getInstance().getTeleToLocation(player, TeleportWhereType.CLANHALL);
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_teletolocation_clanhall").replace("%i%", loc.getX() + "").replace("%s%", loc.getY() + "").replace("%t%", loc.getZ() + ""));
			
			loc = MapRegionManager.getInstance().getTeleToLocation(player, TeleportWhereType.SIEGEFLAG);
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_teletolocation_siegeflag").replace("%i%", loc.getX() + "").replace("%s%", loc.getY() + "").replace("%t%", loc.getZ() + ""));
			
			loc = MapRegionManager.getInstance().getTeleToLocation(player, TeleportWhereType.TOWN);
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_teletolocation_town").replace("%i%", loc.getX() + "").replace("%s%", loc.getY() + "").replace("%t%", loc.getZ() + ""));
		}
		else if (actualCommand.equalsIgnoreCase("admin_zone_visual"))
		{
			try
			{
				var next = st.nextToken();
				if (next.equalsIgnoreCase("all"))
				{
					for (var zone : ZoneManager.getInstance().getZones(player))
					{
						zone.visualizeZone(player.getZ());
					}
					
					showHtml(player);
				}
				else if (next.equalsIgnoreCase("clear"))
				{
					ZoneManager.getInstance().clearDebugItems();
					showHtml(player);
				}
				else
				{
					var zoneId = Integer.parseInt(next);
					ZoneManager.getInstance().getZoneById(zoneId).visualizeZone(player.getZ());
				}
			}
			catch (Exception ex)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_usage_zone_visual"));
			}
		}
		return true;
	}
	
	private static void showHtml(L2PcInstance player)
	{
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/admin/submenu/zone.htm");
		
		html.replace("%PEACE%", (player.isInsideZone(ZoneId.PEACE) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%PVP%", (player.isInsideZone(ZoneId.PVP) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%SIEGE%", (player.isInsideZone(ZoneId.SIEGE) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%TOWN%", (player.isInsideZone(ZoneId.TOWN) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%CASTLE%", (player.isInsideZone(ZoneId.CASTLE) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%FORT%", (player.isInsideZone(ZoneId.FORT) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%HQ%", (player.isInsideZone(ZoneId.HQ) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%CLANHALL%", (player.isInsideZone(ZoneId.CLAN_HALL) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%LAND%", (player.isInsideZone(ZoneId.LANDING) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%NOLAND%", (player.isInsideZone(ZoneId.NO_LANDING) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%NOSUMMON%", (player.isInsideZone(ZoneId.NO_SUMMON_FRIEND) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%WATER%", (player.isInsideZone(ZoneId.WATER) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%SWAMP%", (player.isInsideZone(ZoneId.SWAMP) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%DANGER%", (player.isInsideZone(ZoneId.DANGER_AREA) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%NOSTORE%", (player.isInsideZone(ZoneId.NO_STORE) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%SCRIPT%", (player.isInsideZone(ZoneId.SCRIPT) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		html.replace("%SSQ%", (player.isInsideZone(ZoneId.SSQZONE) ? "<font color=\"LEVEL\">" + MessagesData.getInstance().getMessage(player, "admin_htm_yes") + "</font>" : "" + MessagesData.getInstance().getMessage(player, "admin_htm_no") + ""));
		var sb = new StringBuilder(100);
		var region = L2World.getInstance().getRegion(player.getX(), player.getY());
		for (var zone : region.getZones())
		{
			if (zone.isCharacterInZone(player))
			{
				if (zone.getName() != null)
				{
					StringUtil.append(sb, zone.getName() + "<br1>");
					if (zone.getId() < 300000)
					{
						StringUtil.append(sb, "(", zone.getId(), ")");
					}
				}
				else
				{
					StringUtil.append(sb, zone.getId());
				}
				StringUtil.append(sb, " ");
			}
		}
		for (var territory : ZoneManager.getInstance().getSpawnTerritories(player))
		{
			StringUtil.append(sb, territory.getName() + "<br1>");
		}
		html.replace("%LIST%", sb.toString());
		player.sendPacket(html);
	}
	
	private static void getGeoRegionXY(L2PcInstance player)
	{
		var worldX = player.getX();
		var worldY = player.getY();
		var geoX = ((((worldX - (-327680)) >> 4) >> 11) + 10);
		var geoY = ((((worldY - (-262144)) >> 4) >> 11) + 10);
		player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_geo_region").replace("%i%", geoX + "").replace("%s%", geoY + ""));
	}
	
	@Override
	public String[] getAdminCommandList()
	{
		return ADMIN_COMMANDS;
	}
}
