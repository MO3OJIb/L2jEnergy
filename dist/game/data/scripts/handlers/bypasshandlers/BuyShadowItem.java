/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.bypasshandlers;

import com.l2jserver.gameserver.handler.IBypassHandler;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2MerchantInstance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

public class BuyShadowItem implements IBypassHandler
{
	private static final String[] COMMANDS =
	{
		"BuyShadowItem"
	};
	
	@Override
	public boolean useBypass(String command, L2PcInstance player, L2Character target)
	{
		if (!(target instanceof L2MerchantInstance))
		{
			return false;
		}
		
		var html = new NpcHtmlMessage(((L2Npc) target).getObjectId());
		if (player.getLevel() < 40)
		{
			html.setFile(player, "data/html/common/shadow_item-lowlevel.htm");
		}
		else if ((player.getLevel() >= 40) && (player.getLevel() < 46))
		{
			html.setFile(player, "data/html/common/shadow_item_d.htm");
		}
		else if ((player.getLevel() >= 46) && (player.getLevel() < 52))
		{
			html.setFile(player, "data/html/common/shadow_item_c.htm");
		}
		else if (player.getLevel() >= 52)
		{
			html.setFile(player, "data/html/common/shadow_item_b.htm");
		}
		html.replace("%objectId%", String.valueOf(((L2Npc) target).getObjectId()));
		player.sendPacket(html);
		return true;
	}
	
	@Override
	public String[] getBypassList()
	{
		return COMMANDS;
	}
}
