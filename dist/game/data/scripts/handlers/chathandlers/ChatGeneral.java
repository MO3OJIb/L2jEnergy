/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.chathandlers;

import java.util.StringTokenizer;

import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.enums.FloodAction;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.handler.IChatHandler;
import com.l2jserver.gameserver.handler.IVoicedCommandHandler;
import com.l2jserver.gameserver.handler.VoicedCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.friend.BlockList;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.CreatureSay;
import com.l2jserver.gameserver.util.FloodProtectors;

/**
 * A chat handler
 * @author durgus
 */
public class ChatGeneral implements IChatHandler
{
	private static final ChatType[] CHAT_TYPES =
	{
		ChatType.GENERAL,
	};
	
	@Override
	public void handleChat(ChatType type, L2PcInstance player, String params, String text)
	{
		var vcd_used = false;
		if (text.startsWith("."))
		{
			var st = new StringTokenizer(text);
			IVoicedCommandHandler vch;
			var command = "";
			
			if (st.countTokens() > 1)
			{
				command = st.nextToken().substring(1);
				params = text.substring(command.length() + 2);
			}
			else
			{
				command = text.substring(1);
			}
			
			vch = VoicedCommandHandler.getInstance().getHandler(command);
			
			if (vch != null)
			{
				vch.useVoicedCommand(command, player, params);
				vcd_used = true;
			}
			else
			{
				vcd_used = false;
			}
		}
		
		if (!vcd_used)
		{
			if (player.isChatBanned() && GeneralConfig.BAN_CHAT_CHANNELS.contains(type))
			{
				player.sendPacket(SystemMessageId.CHATTING_IS_CURRENTLY_PROHIBITED_IF_YOU_TRY_TO_CHAT_BEFORE_THE_PROHIBITION_IS_REMOVED_THE_PROHIBITION_TIME_WILL_INCREASE_EVEN_FURTHER);
				return;
			}
			
			/**
			 * Match the character "." literally (Exactly 1 time) Match any character that is NOT a . character. Between one and unlimited times as possible, giving back as needed (greedy)
			 */
			if (text.matches("\\.{1}[^\\.]+"))
			{
				player.sendPacket(SystemMessageId.INCORRECT_SYNTAX);
			}
			else
			{
				if (!FloodProtectors.performAction(player.getClient(), FloodAction.GLOBAL_CHAT))
				{
					return;
				}
				
				var cs = new CreatureSay(player.getObjectId(), type, player.getAppearance().getVisibleName(), text);
				var plrs = player.getKnownList().getKnownPlayers().values();
				for (L2PcInstance pl : plrs)
				{
					if ((pl != null) && player.isInsideRadius(pl, 1250, false, true) && !BlockList.isBlocked(pl, player))
					{
						pl.sendPacket(cs);
					}
				}
				player.sendPacket(cs);
			}
		}
	}
	
	@Override
	public ChatType[] getChatTypeList()
	{
		return CHAT_TYPES;
	}
}