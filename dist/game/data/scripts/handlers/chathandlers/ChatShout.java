/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.chathandlers;

import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.configuration.config.protection.BaseProtectionConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.FloodAction;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.handler.IChatHandler;
import com.l2jserver.gameserver.instancemanager.MapRegionManager;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.PcCondOverride;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.friend.BlockList;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.CreatureSay;
import com.l2jserver.gameserver.util.FloodProtectors;

/**
 * Shout chat handler.
 * @author durgus
 */
public class ChatShout implements IChatHandler
{
	private static final ChatType[] CHAT_TYPES =
	{
		ChatType.SHOUT,
	};
	
	@Override
	public void handleChat(ChatType type, L2PcInstance player, String target, String text)
	{
		if (player.isChatBanned() && GeneralConfig.BAN_CHAT_CHANNELS.contains(type))
		{
			player.sendPacket(SystemMessageId.CHATTING_IS_CURRENTLY_PROHIBITED_IF_YOU_TRY_TO_CHAT_BEFORE_THE_PROHIBITION_IS_REMOVED_THE_PROHIBITION_TIME_WILL_INCREASE_EVEN_FURTHER);
			return;
		}
		
		if (BaseProtectionConfig.JAIL_DISABLE_CHAT && player.isJailed() && !player.canOverrideCond(PcCondOverride.CHAT_CONDITIONS))
		{
			player.sendPacket(SystemMessageId.CHATTING_IS_CURRENTLY_PROHIBITED);
			return;
		}
		
		var cs = new CreatureSay(player.getObjectId(), type, player.getName(), text);
		if (GeneralConfig.DEFAULT_GLOBAL_CHAT.equalsIgnoreCase("on") || (GeneralConfig.DEFAULT_GLOBAL_CHAT.equalsIgnoreCase("gm") && player.canOverrideCond(PcCondOverride.CHAT_CONDITIONS)))
		{
			var region = MapRegionManager.getInstance().getMapRegionLocId(player);
			for (var pl : L2World.getInstance().getPlayers())
			{
				if ((region == MapRegionManager.getInstance().getMapRegionLocId(pl)) && !BlockList.isBlocked(pl, player) && (pl.getInstanceId() == player.getInstanceId()) && !BlockList.isBlocked(player, pl))
				{
					pl.sendPacket(cs);
				}
			}
		}
		else if (GeneralConfig.DEFAULT_GLOBAL_CHAT.equalsIgnoreCase("global"))
		{
			if (!player.canOverrideCond(PcCondOverride.CHAT_CONDITIONS) && !FloodProtectors.performAction(player.getClient(), FloodAction.GLOBAL_CHAT))
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "no_spam_channel"));
				return;
			}
			
			for (var pl : L2World.getInstance().getPlayers())
			{
				if (!BlockList.isBlocked(pl, player))
				{
					pl.sendPacket(cs);
				}
			}
		}
	}
	
	@Override
	public ChatType[] getChatTypeList()
	{
		return CHAT_TYPES;
	}
}
