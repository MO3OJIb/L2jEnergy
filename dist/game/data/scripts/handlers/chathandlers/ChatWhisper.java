/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.chathandlers;

import com.l2jserver.gameserver.configuration.config.CharacterConfig;
import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.configuration.config.protection.BaseProtectionConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.handler.IChatHandler;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.PcCondOverride;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.friend.BlockList;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.CreatureSay;

/**
 * Tell chat handler.
 * @author durgus
 */
public class ChatWhisper implements IChatHandler
{
	private static final ChatType[] CHAT_TYPES =
	{
		ChatType.WHISPER
	};
	
	@Override
	public void handleChat(ChatType type, L2PcInstance player, String target, String text)
	{
		if (player.isChatBanned() && GeneralConfig.BAN_CHAT_CHANNELS.contains(type))
		{
			player.sendPacket(SystemMessageId.CHATTING_IS_CURRENTLY_PROHIBITED_IF_YOU_TRY_TO_CHAT_BEFORE_THE_PROHIBITION_IS_REMOVED_THE_PROHIBITION_TIME_WILL_INCREASE_EVEN_FURTHER);
			return;
		}
		
		if (BaseProtectionConfig.JAIL_DISABLE_CHAT && player.isJailed() && !player.canOverrideCond(PcCondOverride.CHAT_CONDITIONS))
		{
			player.sendPacket(SystemMessageId.CHATTING_IS_CURRENTLY_PROHIBITED);
			return;
		}
		
		// Return if no target is set
		if (target == null)
		{
			return;
		}
		
		var pl = L2World.getInstance().getPlayer(target);
		if ((pl != null) && !pl.isSilenceMode(player.getObjectId()))
		{
			if (BaseProtectionConfig.JAIL_DISABLE_CHAT && pl.isJailed() && !player.canOverrideCond(PcCondOverride.CHAT_CONDITIONS))
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "player_jailed"));
				return;
			}
			
			if (pl.isChatBanned())
			{
				player.sendPacket(SystemMessageId.THAT_PERSON_IS_IN_MESSAGE_REFUSAL_MODE);
				return;
			}
			
			if ((pl.getClient() == null) || (pl.getClient().isDetached()))
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "player_offline"));
				return;
			}
			
			if (!BlockList.isBlocked(pl, player))
			{
				// Allow reciever to send PMs to this char, which is in silence mode.
				if (CharacterConfig.SILENCE_MODE_EXCLUDE && player.isSilenceMode())
				{
					player.addSilenceModeExcluded(pl.getObjectId());
				}
				
				pl.sendPacket(new CreatureSay(player.getObjectId(), type, player.getName(), text));
				player.sendPacket(new CreatureSay(player.getObjectId(), type, "->" + pl.getName(), text));
			}
			else
			{
				player.sendPacket(SystemMessageId.THAT_PERSON_IS_IN_MESSAGE_REFUSAL_MODE);
			}
		}
		else
		{
			player.sendPacket(SystemMessageId.THAT_PLAYER_IS_NOT_ONLINE);
		}
	}
	
	@Override
	public ChatType[] getChatTypeList()
	{
		return CHAT_TYPES;
	}
}
