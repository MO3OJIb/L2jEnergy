/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.communityboard;

import java.text.SimpleDateFormat;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * Favorite board.
 * @author Zoey76
 */
public class FavoriteBoard implements IParseBoardHandler
{
	// SQL Queries
	private static final String SELECT_FAVORITES = "SELECT * FROM `bbs_favorites` WHERE `playerId`=? ORDER BY `favAddDate` DESC";
	private static final String DELETE_FAVORITE = "DELETE FROM `bbs_favorites` WHERE `playerId`=? AND `favId`=?";
	private static final String ADD_FAVORITE = "REPLACE INTO `bbs_favorites`(`playerId`, `favTitle`, `favBypass`) VALUES(?, ?, ?)";
	
	private static final String[] COMMANDS =
	{
		"_bbsgetfav",
		"bbs_add_fav",
		"_bbsdelfav_"
	};
	
	@Override
	public String[] getCommunityBoardCommands()
	{
		return COMMANDS;
	}
	
	@Override
	public boolean parseCommunityBoardCommand(String command, L2PcInstance player)
	{
		// None of this commands can be added to favorites.
		if (command.startsWith("_bbsgetfav"))
		{
			// Load Favorite links
			var list = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/bbs_favoritetpl.html");
			var sb = new StringBuilder();
			try (var con = ConnectionFactory.getInstance().getConnection();
				var ps = con.prepareStatement(SELECT_FAVORITES))
			{
				ps.setInt(1, player.getObjectId());
				try (var rs = ps.executeQuery())
				{
					while (rs.next())
					{
						var link = list.replaceAll("%fav_bypass%", String.valueOf(rs.getString("favBypass")));
						link = link.replaceAll("%fav_title%", rs.getString("favTitle"));
						var date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						link = link.replaceAll("%fav_add_date%", date.format(rs.getTimestamp("favAddDate")));
						link = link.replaceAll("%fav_id%", String.valueOf(rs.getInt("favId")));
						sb.append(link);
					}
				}
				var html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/bbs_getfavorite.html");
				html = html.replaceAll("%fav_list%", sb.toString());
				CommunityBoardHandler.separateAndSend(html, player);
			}
			catch (Exception e)
			{
				LOG.warn("{}: Couldn't load favorite links for player {}", FavoriteBoard.class.getSimpleName(), player.getName());
			}
		}
		else if (command.startsWith("bbs_add_fav"))
		{
			var bypass = CommunityBoardHandler.getInstance().removeBypass(player);
			if (bypass != null)
			{
				var parts = bypass.split("&", 2);
				if (parts.length != 2)
				{
					LOG.warn("{}: Couldn't add favorite link, {} it's not a valid bypass!", FavoriteBoard.class.getSimpleName(), bypass);
					return false;
				}
				
				try (var con = ConnectionFactory.getInstance().getConnection();
					var ps = con.prepareStatement(ADD_FAVORITE))
				{
					ps.setInt(1, player.getObjectId());
					ps.setString(2, parts[0].trim());
					ps.setString(3, parts[1].trim());
					ps.execute();
					// Callback
					parseCommunityBoardCommand("_bbsgetfav", player);
				}
				catch (Exception ex)
				{
					LOG.warn("{}: Couldn't add favorite link {} for player {}", FavoriteBoard.class.getSimpleName(), bypass, player.getName());
				}
			}
		}
		else if (command.startsWith("_bbsdelfav_"))
		{
			var favId = command.replaceAll("_bbsdelfav_", "");
			if (!StringUtil.isDigit(favId))
			{
				LOG.warn("{}: Couldn't delete favorite link, {} it's not a valid ID!", FavoriteBoard.class.getSimpleName(), favId);
				return false;
			}
			
			try (var con = ConnectionFactory.getInstance().getConnection();
				var ps = con.prepareStatement(DELETE_FAVORITE))
			{
				ps.setInt(1, player.getObjectId());
				ps.setInt(2, Integer.parseInt(favId));
				ps.execute();
				// Callback
				parseCommunityBoardCommand("_bbsgetfav", player);
			}
			catch (Exception ex)
			{
				LOG.warn("{}: Couldn't delete favorite link ID {} for player {}", FavoriteBoard.class.getSimpleName(), favId, player.getName());
			}
		}
		return true;
	}
}
