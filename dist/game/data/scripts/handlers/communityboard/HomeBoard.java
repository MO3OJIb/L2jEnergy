/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.communityboard;

import java.time.ZonedDateTime;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.RatesConfig;
import com.l2jserver.gameserver.configuration.config.bbs.CBasicConfig;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.util.DifferentMethods;

/**
 * Home board.
 * @author Zoey76
 */
public final class HomeBoard implements IParseBoardHandler
{
	// SQL Queries
	private static final String COUNT_FAVORITES = "SELECT COUNT(*) AS favorites FROM `bbs_favorites` WHERE `playerId`=?";
	
	private static final String[] COMMANDS =
	{
		"_bbshome",
		"_bbstop",
		"_bbsopen"
	};
	
	@Override
	public String[] getCommunityBoardCommands()
	{
		return COMMANDS;
	}
	
	@Override
	public boolean parseCommunityBoardCommand(String command, L2PcInstance player)
	{
		if (command.equals("_bbshome") || command.equals("_bbstop"))
		{
			final String customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
			CommunityBoardHandler.getInstance().addBypass(player, "Home", command);
			
			var time = ZonedDateTime.now().withZoneSameInstant(CBasicConfig.BBS_TIME_ZONE_ID_INFO);
			
			var html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "bbs_top.html");
			html = html.replaceAll("%fav_count%", Integer.toString(getFavoriteCount(player)));
			html = html.replaceAll("%region_count%", Integer.toString(getRegionCount(player)));
			html = html.replaceAll("%clan_count%", Integer.toString(ClanTable.getInstance().getClanCount()));
			
			html = html.replace("<?cb_rate_xp?>", Float.toString(RatesConfig.RATE_XP));
			html = html.replace("<?cb_rate_sp?>", Float.toString(RatesConfig.RATE_SP));
			// html = html.replace("<?cb_rate_adena?>" TODO: need fix
			html = html.replace("<?cb_rate_herb?>", Float.toString(RatesConfig.RATE_HERB_DROP_AMOUNT_MULTIPLIER));
			html = html.replace("<?cb_rate_spoil?>", Float.toString(RatesConfig.RATE_CORPSE_DROP_AMOUNT_MULTIPLIER));
			html = html.replace("<?cb_rate_raid?>", Float.toString(RatesConfig.RATE_RAID_DROP_CHANCE_MULTIPLIER));
			html = html.replace("<?cb_rate_siege?>", Float.toString(RatesConfig.RATE_SIEGE_GUARDS_PRICE));
			html = html.replace("<?cb_rate_quest?>", Float.toString(RatesConfig.RATE_QUEST_REWARD));
			html = html.replace("<?cb_rate_manor?>", Float.toString(RatesConfig.RATE_DROP_MANOR));
			html = html.replace("<?cb_rate_clanrep?>", Float.toString(RatesConfig.RATE_CLAN_REP_SCORE));
			html = html.replace("<?cb_rate_hellbound?>", Float.toString(RatesConfig.RATE_HB_TRUST_INCREASE));
			
			html = html.replace("<?cb_player_name?>", player.getName());
			html = html.replace("<?cb_time?>", time.getHour() + ":" + time.getMinute());
			html = html.replace("<?cb_online_players?>", String.valueOf(DifferentMethods.getPlayersCount("ALL_REAL")));
			html = html.replace("<?cb_offtrade_players?>", String.valueOf(DifferentMethods.getPlayersCount("OFF_TRADE")));
			CommunityBoardHandler.separateAndSend(html, player);
		}
		else if (command.startsWith("_bbsopen"))
		{
			var customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
			var b = command.split(":");
			var folder = b[1];
			var page = b[2];
			var html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + folder + "/" + page + ".html");
			if (html == null)
			{
				return false;
			}
			CommunityBoardHandler.separateAndSend(html, player);
		}
		else if (command.startsWith("_bbstop;"))
		{
			var customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
			var path = command.replace("_bbstop;", "");
			if ((path.length() > 0) && path.endsWith(".html"))
			{
				var html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + path);
				CommunityBoardHandler.separateAndSend(html, player);
			}
		}
		return true;
	}
	
	/**
	 * Gets the Favorite links for the given player.
	 * @param player the player
	 * @return the favorite links count
	 */
	private static int getFavoriteCount(L2PcInstance player)
	{
		int count = 0;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(COUNT_FAVORITES))
		{
			ps.setInt(1, player.getObjectId());
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					count = rs.getInt("favorites");
				}
			}
		}
		catch (Exception e)
		{
			LOG.warn("{}: Coudn't load favorites count for player {}", FavoriteBoard.class.getSimpleName(), player.getName());
		}
		return count;
	}
	
	/**
	 * Gets the registered regions count for the given player.
	 * @param player the player
	 * @return the registered regions count
	 */
	private static int getRegionCount(L2PcInstance player)
	{
		return 0; // TODO: Implement.
	}
}
