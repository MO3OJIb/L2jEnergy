/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.communityboard;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.enums.events.EventType;
import com.l2jserver.gameserver.enums.events.ListenerRegisterType;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IWriteBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.bbs.Mail;
import com.l2jserver.gameserver.model.events.annotations.RegisterEvent;
import com.l2jserver.gameserver.model.events.annotations.RegisterType;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ExMailArrived;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * Mail board.
 * @author Zoey76
 */
public class MailBoard implements IWriteBoardHandler
{
	// SQL Queries
	private static final String SELECT_MAIL = "SELECT * FROM `bbs_mail` WHERE `box_type` = 0 and `read` = 0 and `to_object_id` = ?";
	private static final String DELETE_MAIL = "DELETE FROM `bbs_mail` WHERE `to_object_id` = ? and `post_date` < ?";
	private static final String SELECT_MAIL2 = "SELECT * FROM `bbs_mail` WHERE `box_type` = ? and `to_object_id` = ? ORDER BY post_date DESC";
	
	private static final int MESSAGE_PER_PAGE = 10;
	
	private static final String[] COMMANDS =
	{
		"_maillist_",
		"_mailsearch_",
		"_mailread_",
		"_maildelete_"
	};
	
	@Override
	public String[] getCommunityBoardCommands()
	{
		return COMMANDS;
	}
	
	@Override
	public boolean parseCommunityBoardCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command, "_");
		var cmd = st.nextToken();
		player.setSessionVar("add_fav", null);
		if ("maillist".equals(cmd))
		{
			var type = Integer.parseInt(st.nextToken());
			var page = Integer.parseInt(st.nextToken());
			var byTitle = Integer.parseInt(st.nextToken());
			var search = st.hasMoreTokens() ? st.nextToken() : "";
			
			CommunityBoardHandler.getInstance().addBypass(player, "Mail Command", command);
			var html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/bbs_mail_list.html");
			
			var inbox = 0;
			var send = 0;
			
			try (var con = ConnectionFactory.getInstance().getConnection();
				var ps1 = con.prepareStatement("SELECT count(*) as cnt FROM `bbs_mail` WHERE `box_type` = 0 and `to_object_id` = ?");
				var ps2 = con.prepareStatement("SELECT count(*) as cnt FROM `bbs_mail` WHERE `box_type` = 1 and `from_object_id` = ?"))
			{
				ps1.setInt(1, player.getObjectId());
				try (var rs = ps1.executeQuery())
				{
					if (rs.next())
					{
						inbox = rs.getInt("cnt");
					}
				}
				
				ps2.setInt(1, player.getObjectId());
				try (var rs = ps2.executeQuery())
				{
					if (rs.next())
					{
						send = rs.getInt("cnt");
					}
				}
			}
			catch (Exception e)
			{
			}
			
			List<Mail> mailList = null;
			
			switch (type)
			{
				case 0:
					html = html.replace("%inbox_link%", "[&$917;]");
					html = html.replace("%sentbox_link%", "<a action=\"bypass _maillist_1_1_0_\">[&$918;]</a>");
					html = html.replace("%archive_link%", "<a action=\"bypass _maillist_2_1_0_\">[&$919;]</a>");
					html = html.replace("%temp_archive_link%", "<a action=\"bypass _maillist_3_1_0_\">[&$920;]</a>");
					html = html.replace("%TREE%", "&$917;");
					html = html.replace("%writer_header%", "&$911;");
					mailList = getMailList(player, type, search, byTitle == 1);
					break;
				case 1:
					html = html.replace("%inbox_link%", "<a action=\"bypass _maillist_0_1_0_\">[&$917;]</a>");
					html = html.replace("%sentbox_link%", "[&$918;]");
					html = html.replace("%archive_link%", "<a action=\"bypass _maillist_2_1_0_\">[&$919;]</a>");
					html = html.replace("%temp_archive_link%", "<a action=\"bypass _maillist_3_1_0_\">[&$920;]</a>");
					html = html.replace("%TREE%", "&$918;");
					html = html.replace("%writer_header%", "&$909;");
					mailList = getMailList(player, type, search, byTitle == 1);
					break;
				case 2:
					html = html.replace("%inbox_link%", "<a action=\"bypass _maillist_0_1_0_\">[&$917;]</a>");
					html = html.replace("%sentbox_link%", "<a action=\"bypass _maillist_1_1_0_\">[&$918;]</a>");
					html = html.replace("%archive_link%", "[&$919;]");
					html = html.replace("%temp_archive_link%", "<a action=\"bypass _maillist_3_1_0_\">[&$920;]</a>");
					html = html.replace("%TREE%", "&$919;");
					html = html.replace("%writer_header%", "&$911;");
					break;
				case 3:
					html = html.replace("%inbox_link%", "<a action=\"bypass _maillist_0_1_0_\">[&$917;]</a>");
					html = html.replace("%sentbox_link%", "<a action=\"bypass _maillist_1_1_0_\">[&$918;]</a>");
					html = html.replace("%archive_link%", "<a action=\"bypass _maillist_2_1_0_\">[&$919;]</a>");
					html = html.replace("%temp_archive_link%", "[&$920;]");
					html = html.replace("%TREE%", "&$920;");
					html = html.replace("%writer_header%", "&$909;");
					break;
			}
			
			if (mailList != null)
			{
				var start = (page - 1) * MESSAGE_PER_PAGE;
				var end = Math.min(page * MESSAGE_PER_PAGE, mailList.size());
				
				if (page == 1)
				{
					html = html.replace("%ACTION_GO_LEFT%", "");
					html = html.replace("%GO_LIST%", "");
					html = html.replace("%NPAGE%", "1");
				}
				else
				{
					html = html.replace("%ACTION_GO_LEFT%", "bypass _maillist_" + type + "_" + (page - 1) + "_" + byTitle + "_" + search);
					html = html.replace("%NPAGE%", String.valueOf(page));
					var sb = new StringBuilder("");
					for (var i = page > 10 ? page - 10 : 1; i < page; i++)
					{
						sb.append("<td><a action=\"bypass _maillist_").append(type).append("_").append(i).append("_").append(byTitle).append("_").append(search).append("\"> ").append(i).append(" </a> </td>\n\n");
					}
					
					html = html.replace("%GO_LIST%", sb.toString());
				}
				
				var pages = Math.max(mailList.size() / MESSAGE_PER_PAGE, 1);
				if (mailList.size() > (pages * MESSAGE_PER_PAGE))
				{
					pages++;
				}
				
				if (pages > page)
				{
					html = html.replace("%ACTION_GO_RIGHT%", "bypass _maillist_" + type + "_" + (page + 1) + "_" + byTitle + "_" + search);
					var ep = Math.min(page + 10, pages);
					var sb = new StringBuilder("");
					for (int i = page + 1; i <= ep; i++)
					{
						sb.append("<td><a action=\"bypass _maillist_").append(type).append("_").append(i).append("_").append(byTitle).append("_").append(search).append("\"> ").append(i).append(" </a> </td>\n\n");
					}
					
					html = html.replace("%GO_LIST2%", sb.toString());
				}
				else
				{
					html = html.replace("%ACTION_GO_RIGHT%", "");
					html = html.replace("%GO_LIST2%", "");
				}
				
				var sb = new StringBuilder("");
				var tpl = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/bbs_mailtpl.html");
				for (var i = start; i < end; i++)
				{
					var md = mailList.get(i);
					var mailtpl = tpl;
					mailtpl = mailtpl.replace("%action%", "bypass _mailread_" + md.getMessageId() + "_" + type + "_" + page + "_" + byTitle + "_" + search);
					mailtpl = mailtpl.replace("%writer%", md.getAuthor());
					mailtpl = mailtpl.replace("%title%", md.getTitle());
					mailtpl = mailtpl.replace("%post_date%", md.getPostDate());
					sb.append(mailtpl);
				}
				
				html = html.replace("%MAIL_LIST%", sb.toString());
			}
			else
			{
				html = html.replace("%ACTION_GO_LEFT%", "");
				html = html.replace("%GO_LIST%", "");
				html = html.replace("%NPAGE%", "1");
				html = html.replace("%GO_LIST2%", "");
				html = html.replace("%ACTION_GO_RIGHT%", "");
				html = html.replace("%MAIL_LIST%", "");
			}
			
			html = html.replace("%mailbox_type%", String.valueOf(type));
			html = html.replace("%incomming_mail_no%", String.valueOf(inbox));
			html = html.replace("%sent_mail_no%", String.valueOf(send));
			html = html.replace("%archived_mail_no%", "0");
			html = html.replace("%temp_mail_no%", "0");
			
			CommunityBoardHandler.separateAndSend(html, player);
		}
		else if ("mailread".equals(cmd))
		{
			var messageId = Integer.parseInt(st.nextToken());
			var type = Integer.parseInt(st.nextToken());
			var page = Integer.parseInt(st.nextToken());
			var byTitle = Integer.parseInt(st.nextToken());
			var search = st.hasMoreTokens() ? st.nextToken() : "";
			
			try (var con = ConnectionFactory.getInstance().getConnection();
				var ps1 = con.prepareStatement("SELECT * FROM `bbs_mail` WHERE `message_id` = ? and `box_type` = ? and `to_object_id` = ?");
				var ps2 = con.prepareStatement("UPDATE `bbs_mail` SET `read` = `read` + 1 WHERE message_id = ?"))
			{
				ps1.setInt(1, messageId);
				ps1.setInt(2, type);
				ps1.setInt(3, player.getObjectId());
				try (var rs = ps1.executeQuery())
				{
					if (rs.next())
					{
						var html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/bbs_mail_read.html");
						
						switch (type)
						{
							case 0 -> html = html.replace("%TREE%", "<a action=\"bypass _maillist_0_1_0_\">&$917;</a>");
							case 1 -> html = html.replace("%TREE%", "<a action=\"bypass _maillist_1_1_0__\">&$918;</a>");
							case 2 -> html = html.replace("%TREE%", "<a action=\"bypass _maillist_2_1_0__\">&$919;</a>");
							case 3 -> html = html.replace("%TREE%", "<a action=\"bypass _maillist_3_1_0__\">&$920;</a>");
						}
						
						html = html.replace("%writer%", rs.getString("from_name"));
						html = html.replace("%post_date%", String.format("%1$te-%1$tm-%1$tY", new Date(rs.getInt("post_date") * 1000L)));
						html = html.replace("%del_date%", String.format("%1$te-%1$tm-%1$tY", new Date((rs.getInt("post_date") + (90 * 24 * 60 * 60)) * 1000L)));
						html = html.replace("%char_name%", rs.getString("to_name"));
						html = html.replace("%title%", rs.getString("title"));
						html = html.replace("%CONTENT%", rs.getString("message").replace("\n", "<br1>"));
						html = html.replace("%GOTO_LIST_LINK%", "bypass _maillist_" + type + "_" + page + "_" + byTitle + "_" + search);
						html = html.replace("%message_id%", String.valueOf(messageId));
						html = html.replace("%mailbox_type%", String.valueOf(type));
						player.setSessionVar("add_fav", command + "&" + rs.getString("title"));
						
						ps2.setInt(1, messageId);
						ps2.execute();
						
						CommunityBoardHandler.separateAndSend(html, player);
						return false;
					}
				}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
			parseCommunityBoardCommand("_maillist_" + type + "_" + page + "_" + byTitle + "_" + search, player);
		}
		else if ("maildelete".equals(cmd))
		{
			var type = Integer.parseInt(st.nextToken());
			var messageId = Integer.parseInt(st.nextToken());
			
			try (var con = ConnectionFactory.getInstance().getConnection();
				var ps = con.prepareStatement("DELETE FROM `bbs_mail` WHERE `box_type` = ? and `message_id` = ? and `to_object_id` = ?"))
			{
				ps.setInt(1, type);
				ps.setInt(2, messageId);
				ps.setInt(3, player.getObjectId());
				ps.execute();
			}
			catch (Exception ex)
			{
			}
			parseCommunityBoardCommand("_maillist_" + type + "_1_0_", player);
		}
		return true;
	}
	
	@Override
	public boolean writeCommunityBoardCommand(L2PcInstance activeChar, String arg1, String arg2, String arg3, String arg4, String arg5)
	{
		var st = new StringTokenizer(arg1, "_");
		var cmd = st.nextToken();
		if ("mailsearch".equals(cmd))
		{
			parseCommunityBoardCommand("_maillist_" + st.nextToken() + "_1_" + ("Title".equals(arg3) ? "1_" : "0_") + (arg5 != null ? arg5 : ""), activeChar);
		}
		return false;
	}
	
	@RegisterEvent(EventType.ON_PLAYER_LOGIN)
	@RegisterType(ListenerRegisterType.GLOBAL_PLAYERS)
	public static void OnPlayerEnter(L2PcInstance player)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_MAIL))
		{
			ps.setInt(1, player.getObjectId());
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.NEW_MAIL));
					player.sendPacket(ExMailArrived.STATIC_PACKET);
				}
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	private static List<Mail> getMailList(L2PcInstance player, int type, String search, boolean byTitle)
	{
		List<Mail> list = new ArrayList<>();
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps1 = con.prepareStatement(DELETE_MAIL);
			var ps2 = con.prepareStatement(SELECT_MAIL2))
		{
			ps1.setInt(1, player.getObjectId());
			ps1.setInt(2, (int) (System.currentTimeMillis() / 1000) - (90 * 24 * 60 * 60));
			ps1.execute();
			
			var column_name = type == 0 ? "from_name" : "to_name";
			ps2.setInt(1, type);
			ps2.setInt(2, player.getObjectId());
			try (var rs = ps2.executeQuery())
			{
				while (rs.next())
				{
					if (search.isEmpty())
					{
						list.add(new Mail(rs.getString(column_name), rs.getString("title"), rs.getInt("post_date"), rs.getInt("message_id")));
					}
					else if (byTitle && !search.isEmpty() && rs.getString("title").toLowerCase().contains(search.toLowerCase()))
					{
						list.add(new Mail(rs.getString(column_name), rs.getString("title"), rs.getInt("post_date"), rs.getInt("message_id")));
					}
					else if (!byTitle && !search.isEmpty() && rs.getString(column_name).toLowerCase().contains(search.toLowerCase()))
					{
						list.add(new Mail(rs.getString(column_name), rs.getString("title"), rs.getInt("post_date"), rs.getInt("message_id")));
					}
				}
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return list;
	}
}
