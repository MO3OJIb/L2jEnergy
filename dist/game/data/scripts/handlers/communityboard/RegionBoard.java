/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.communityboard;

import java.util.List;
import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IWriteBoardHandler;
import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.Castle;

/**
 * Region board.
 * @author Zoey76
 */
public class RegionBoard implements IWriteBoardHandler
{
	// Region data
	// @formatter:off
	private static final int[] REGIONS = { 1049, 1052, 1053, 1057, 1060, 1059, 1248, 1247, 1056 };
	// @formatter:on
	private static final String[] COMMANDS =
	{
		"_bbsloc",
		"_bbsregsearch",
		"_bbsreglist_"
	};
	
	@Override
	public String[] getCommunityBoardCommands()
	{
		return COMMANDS;
	}
	
	@Override
	public boolean parseCommunityBoardCommand(String command, L2PcInstance player)
	{
		var st = new StringTokenizer(command, "_");
		var cmd = st.nextToken();
		
		if ("bbsloc".equals(cmd))
		{
			CommunityBoardHandler.getInstance().addBypass(player, "Region", command);
			
			var list = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/bbs_regiontpl.html");
			var sb = new StringBuilder();
			final List<Castle> castles = CastleManager.getInstance().getCastles();
			for (int i = 0; i < REGIONS.length; i++)
			{
				var castle = castles.get(i);
				var clan = ClanTable.getInstance().getClan(castle.getOwnerId());
				var link = list.replaceAll("%region_id%", String.valueOf(i));
				link = link.replace("%region_name%", String.valueOf(REGIONS[i]));
				link = link.replace("%region_owning_clan%", (clan != null ? clan.getName() : "NPC"));
				link = link.replace("%region_owning_clan_alliance%", ((clan != null) && (clan.getAllyName() != null) ? clan.getAllyName() : ""));
				link = link.replace("%region_tax_rate%", String.valueOf(castle.getTaxRate() * 100) + "%");
				sb.append(link);
			}
			
			var html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/bbs_region_list.html");
			html = html.replace("%region_list%", sb.toString());
			CommunityBoardHandler.separateAndSend(html, player);
		}
		else if ("bbsloc".equals(cmd))
		{
			CommunityBoardHandler.getInstance().addBypass(player, "Region>", command);
			
			var id = command.replace("_bbsloc;", "");
			if (!StringUtil.isDigit(id))
			{
				LOG.warn("{}: Player {} sent and invalid region bypass {}!", RegionBoard.class.getSimpleName(), player, command);
				return false;
			}
			// TODO: Implement.
		}
		return true;
	}
	
	@Override
	public boolean writeCommunityBoardCommand(L2PcInstance player, String arg1, String arg2, String arg3, String arg4, String arg5)
	{
		var st = new StringTokenizer(arg1, "_");
		var cmd = st.nextToken();
		if ("bbsregsearch".equals(cmd))
		{
			var townId = Integer.parseInt(st.nextToken());
			var type = Integer.parseInt(st.nextToken());
			var byItem = "Item".equals(arg4) ? "1" : "0";
			if (arg3 == null)
			{
				arg3 = "";
			}
			
			arg3 = arg3.replace("<", "");
			arg3 = arg3.replace(">", "");
			arg3 = arg3.replace("&", "");
			arg3 = arg3.replace("$", "");
			
			if (arg3.length() > 30)
			{
				arg3 = arg3.substring(0, 30);
			}
			
			parseCommunityBoardCommand("_bbsreglist_" + townId + "_" + type + "_1_" + byItem + "_" + arg3, player);
		}
		return false;
	}
}
