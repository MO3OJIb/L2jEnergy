/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.communityboard.custom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.bbs.CBasicConfig;
import com.l2jserver.gameserver.configuration.config.bbs.CBufferConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.xml.impl.BufferBBSData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.SkillData;
import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.enums.ZoneId;
import com.l2jserver.gameserver.enums.events.Team;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.model.actor.L2Playable;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.bbs.BuffTask;
import com.l2jserver.gameserver.model.bbs.PremiumBuff;
import com.l2jserver.gameserver.model.bbs.Scheme;
import com.l2jserver.gameserver.model.holders.BuffBBSHolder;
import com.l2jserver.gameserver.model.olympiad.OlympiadManager;
import com.l2jserver.gameserver.network.serverpackets.MagicSkillUse;
import com.l2jserver.gameserver.taskmanager.AttackStanceTaskManager;
import com.l2jserver.gameserver.util.DifferentMethods;
import com.l2jserver.gameserver.util.Util;

import gnu.trove.list.array.TIntArrayList;

/**
 * Buffer board.
 */
public class BufferBoard implements IParseBoardHandler
{
	private static final TIntArrayList _allowBuff = new TIntArrayList(CBufferConfig.BBS_BUFFER_BUFFS_LIST);
	
	private static List<String> _pageBuffPlayer;
	private static List<String> _pageBuffPet;
	private static List<String> _pagePremiumBuffPlayer;
	private static List<String> _pagePremiumBuffPet;
	
	private static List<PremiumBuff> premiumBuffs;
	
	private static final StringBuilder _buffSchemes = new StringBuilder();
	
	private static final String[] COMMANDS =
	{
		"_bbsbuffer",
		"_bbsplayerbuffer",
		"_bbspetbuffer",
		"_bbscastbuff",
		"_bbscastgroupbuff",
		"_bbsbuffersave",
		"_bbsbufferuse",
		"_bbsbufferdelete",
		"_bbsbufferheal",
		"_bbsbufferremovebuffs",
		"_bbsplayerpremiumbuffer",
		"_bbspetpremiumbuffer"
	};
	
	@Override
	public boolean parseCommunityBoardCommand(String command, L2PcInstance player)
	{
		player.setSessionVar("add_fav", null);
		
		if (!CBufferConfig.ENABLE_BBS_BUFFER)
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
			CommunityBoardHandler.getInstance().handleParseCommand("_bbshome", player);
			return false;
		}
		
		if (!checkCondition(player))
		{
			parseCommunityBoardCommand("_bbsbuffer", player);
			return false;
		}
		
		String html;
		var customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
		
		if (command.equals("_bbsbuffer"))
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "buffer/index.html");
			html = html.replace("%scheme%", _buffSchemes.toString());
			var template = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "buffer/my-sheme.html");
			String block;
			String list = null;
			html = html.replace("{buffTime}", String.valueOf(CBufferConfig.BBS_BUFFER_TIME));
			for (var name : player.getBuffSchemes().keySet())
			{
				block = template;
				block = block.replace("{bypass}", "bypass _bbsbufferuse " + name + " $Who");
				block = block.replace("{name}", name);
				block = block.replace("{delete}", "bypass _bbsbufferdelete " + name);
				list = list + block;
			}
			var priceId = CBufferConfig.BBS_BUFFER_PRICE[0];
			var priceCount = CBufferConfig.BBS_BUFFER_PRICE[1];
			
			if (CBufferConfig.BBS_BUFFER_PRICE_MOD)
			{
				priceCount *= player.getLevel();
			}
			
			if (CBufferConfig.BBS_BUFFER_PA_PRICE_MOD && player.getPcPremiumSystem().isActive())
			{
				priceCount = (int) (priceCount * CBufferConfig.BBS_BUFFER_PA_PRICE);
			}
			
			if (player.getLevel() < CBufferConfig.BBS_BUFFER_PLAYER_FREE_LEVEL)
			{
				priceCount = 0;
			}
			
			html = html.replace("%price%", DifferentMethods.formatPay(player, priceCount, priceId));
			
			if (list != null)
			{
				html = html.replace("%buffgrps%", list);
			}
			else
			{
				html = html.replace("%buffgrps%", HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "buffer/my-sheme-empty.html"));
			}
			onInit();
		}
		else if (command.startsWith("_bbsplayerbuffer"))
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "buffer/scheme.html");
			var st = new StringTokenizer(command, ":");
			st.nextToken();
			var page = Integer.parseInt(st.nextToken());
			if (_pageBuffPlayer.get(page) != null)
			{
				html = html.replace("%content%", _pageBuffPlayer.get(page));
			}
		}
		else if (command.startsWith("_bbsplayerpremiumbuffer"))
		{
			if (player.getPcPremiumSystem().isActive())
			{
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "buffer/scheme.html");
				var st = new StringTokenizer(command, ":");
				st.nextToken();
				var page = Integer.parseInt(st.nextToken());
				if (_pagePremiumBuffPlayer.get(page) != null)
				{
					html = html.replace("%content%", _pagePremiumBuffPlayer.get(page));
				}
			}
			else
			{
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "buffer/scheme.html").replaceFirst("%content%", "You are not a Premium member!");
			}
		}
		else if (command.startsWith("_bbspetbuffer"))
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "buffer/scheme.html");
			var st = new StringTokenizer(command, ":");
			st.nextToken();
			var page = Integer.parseInt(st.nextToken());
			if (_pageBuffPet.get(page) != null)
			{
				html = html.replace("%content%", _pageBuffPet.get(page));
			}
		}
		else if (command.startsWith("_bbspetpremiumbuffer"))
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "buffer/scheme.html");
			var st = new StringTokenizer(command, ":");
			st.nextToken();
			var page = Integer.parseInt(st.nextToken());
			if (_pagePremiumBuffPet.get(page) != null)
			{
				html = html.replace("%content%", _pagePremiumBuffPet.get(page));
			}
		}
		else if (command.startsWith("_bbscastbuff"))
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "buffer/scheme.html");
			var st = new StringTokenizer(command, ":");
			st.nextToken();
			
			var id = Integer.parseInt(st.nextToken());
			var level = Integer.parseInt(st.nextToken());
			var page = Integer.parseInt(st.nextToken());
			var type = st.nextToken();
			var isPremium = st.hasMoreTokens();
			
			L2Playable playable = null;
			if ("Player".equals(type))
			{
				playable = player;
			}
			else if ("Pet".equals(type))
			{
				playable = player.getSummon();
			}
			
			var check = _allowBuff.indexOf(id);
			var priceId = CBufferConfig.BBS_BUFFER_PRICE[0];
			var priceCount = CBufferConfig.BBS_BUFFER_PRICE[1];
			
			if (CBufferConfig.BBS_BUFFER_PRICE_MOD)
			{
				priceCount *= player.getLevel();
			}
			
			if (isPremium && (getPremiumBuff(id, level) != null))
			{
				buffPremium(player, playable, id, level);
			}
			else if ((playable != null) && (check != -1) && (_allowBuff.get(check + 1) <= level))
			{
				if (player.getLevel() < CBufferConfig.BBS_BUFFER_PLAYER_FREE_LEVEL)
				{
					buffList(Collections.singletonList(new BuffBBSHolder(id, level)), playable);
				}
				else if (DifferentMethods.getPay(player, priceId, priceCount))
				{
					buffList(Collections.singletonList(new BuffBBSHolder(id, level)), playable);
				}
			}
			
			if ("Player".equals(type))
			{
				html = html.replace("%content%", isPremium ? _pagePremiumBuffPlayer.get(page) : _pageBuffPlayer.get(page));
			}
			else if ("Pet".equals(type))
			{
				html = html.replace("%content%", isPremium ? _pagePremiumBuffPet.get(page) : _pageBuffPet.get(page));
			}
		}
		else
		{
			if (command.startsWith("_bbscastgroupbuff"))
			{
				var st = new StringTokenizer(command, " ");
				st.nextToken();
				var id = Integer.parseInt(st.nextToken());
				var type = st.nextToken();
				var buffScheme = BufferBBSData.getInstance().getBuffScheme(id);
				var priceId = buffScheme.getPriceId();
				var priceCount = buffScheme.getPriceCount();
				
				L2Playable playable = null;
				if ("Player".equals(type))
				{
					playable = player;
				}
				else if ("Pet".equals(type))
				{
					playable = player.getSummon();
				}
				
				if (playable != null)
				{
					if (player.getLevel() < CBufferConfig.BBS_BUFFER_PLAYER_FREE_LEVEL)
					{
						buffList(buffScheme.getBuffIds(), playable);
					}
					else if (DifferentMethods.getPay(player, priceId, priceCount))
					{
						buffList(buffScheme.getBuffIds(), playable);
					}
				}
				parseCommunityBoardCommand("_bbsbuffer", player);
				return false;
			}
			
			if (command.startsWith("_bbsbuffersave"))
			{
				if (player.getBuffSchemes().size() >= 5)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_can_create_only_scheme"));
					parseCommunityBoardCommand("_bbsbuffer", player);
					return false;
				}
				
				var st = new StringTokenizer(command, " ");
				if (st.countTokens() < 3)
				{
					parseCommunityBoardCommand("_bbsbuffer", player);
					return false;
				}
				st.nextToken();
				
				var name = st.nextToken();
				var type = st.nextToken();
				
				L2Playable playable = null;
				if ("Player".equals(type))
				{
					playable = player;
				}
				else if ("Pet".equals(type))
				{
					playable = player.getSummon();
				}
				
				if (playable == null)
				{
					return false;
				}
				
				if (playable.getEffectList().getEffects().size() == 0)
				{
					parseCommunityBoardCommand("_bbsbuffer", player);
					return false;
				}
				
				if (player.getBuffScheme(name).isPresent())
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_name_already_exists"));
					parseCommunityBoardCommand("_bbsbuffer", player);
					return false;
				}
				
				if (DifferentMethods.getPay(player, CBufferConfig.BBS_BUFFER_SAVED_ID, CBufferConfig.BBS_BUFFER_SAVED_PRICE))
				{
					var sb = new StringBuilder();
					var scheme = new Scheme(name);
					for (var effect : playable.getEffectList().getEffects())
					{
						var skill = effect.getSkill();
						var id = skill.getId();
						var level = skill.getLevel();
						var check = _allowBuff.indexOf(skill.getId());
						level = level > _allowBuff.get(check + 1) ? _allowBuff.get(check + 1) : level;
						if (check != -1)
						{
							sb.append(id).append(",").append(level).append(";");
							scheme.addBuff(id, level);
						}
					}
					DAOFactory.getInstance().getCommunityBufferDAO().insert(player, sb.toString(), scheme);
				}
				parseCommunityBoardCommand("_bbsbuffer", player);
				return false;
			}
			
			if (command.startsWith("_bbsbufferuse"))
			{
				var sb = new StringTokenizer(command, " ");
				sb.nextToken();
				var name = sb.nextToken();
				var type = sb.nextToken();
				
				L2Playable playable = null;
				if ("Player".equals(type))
				{
					playable = player;
				}
				else if ("Pet".equals(type))
				{
					playable = player.getSummon();
				}
				
				var priceId = CBufferConfig.BBS_BUFFER_PRICE[0];
				var priceCount = CBufferConfig.BBS_BUFFER_PRICE[1];
				
				if (CBufferConfig.BBS_BUFFER_PRICE_MOD)
				{
					priceCount *= player.getLevel();
				}
				
				if (CBufferConfig.BBS_BUFFER_PRICE_MOD && player.getPcPremiumSystem().isActive())
				{
					priceCount = (int) (priceCount * CBufferConfig.BBS_BUFFER_PA_PRICE);
				}
				
				if (playable != null)
				{
					final List<BuffBBSHolder> buffs = new ArrayList<>();
					var my = player.getBuffScheme(name);
					if (my.isPresent())
					{
						if (player.getLevel() > CBufferConfig.BBS_BUFFER_PLAYER_FREE_LEVEL)
						{
							priceCount *= my.get().getBuffs().size();
						}
						else
						{
							priceCount = 0;
						}
						
						if (DifferentMethods.getPay(player, priceId, priceCount))
						{
							buffs.addAll(my.get().getBuffs().entrySet().stream().filter(scheme -> _allowBuff.indexOf(scheme.getKey()) != -1).map(scheme -> new BuffBBSHolder(scheme.getKey(), scheme.getValue())).collect(Collectors.toList()));
						}
						buffList(buffs, playable);
					}
				}
				parseCommunityBoardCommand("_bbsbuffer", player);
				return false;
			}
			
			if (command.startsWith("_bbsbufferdelete"))
			{
				var sb = new StringTokenizer(command, " ");
				sb.nextToken();
				var name = sb.nextToken();
				DAOFactory.getInstance().getCommunityBufferDAO().delete(player, name);
				parseCommunityBoardCommand("_bbsbuffer", player);
				return false;
			}
			
			if (command.startsWith("_bbsbufferheal"))
			{
				if (!CBufferConfig.BBS_BUFFER_RECOVER)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
				}
				else if ((CBufferConfig.BBS_BUFFER_RECOVER_PVP_FLAG && (player.getPvpFlag() > 0)) || (CBufferConfig.BBS_BUFFER_RECOVER_BATTLE && player.isInCombat()))
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_combat_enable"));
				}
				else
				{
					var sb = new StringTokenizer(command, " ");
					sb.nextToken();
					var type = sb.nextToken();
					var target = sb.nextToken();
					
					L2Playable playable = null;
					if ("Player".equals(target))
					{
						playable = player;
					}
					else if ("Pet".equals(target))
					{
						playable = player.getSummon();
					}
					
					if (playable != null)
					{
						if ((CBufferConfig.BBS_BUFFER_PEACE_RECOVER) && (!playable.isInsideZone(ZoneId.PEACE)))
						{
							if (playable.isPlayer())
							{
								player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_peaceZone"));
							}
							parseCommunityBoardCommand("_bbsbuffer", player);
							return false;
						}
						
						if (DifferentMethods.getPay(player, CBufferConfig.BBS_BUFFER_RECOVER_PRICE[0], CBufferConfig.BBS_BUFFER_RECOVER_PRICE[1]))
						{
							if ("HP".equals(type))
							{
								if (playable.getCurrentHp() != playable.getMaxHp())
								{
									playable.setCurrentHp(playable.getMaxHp());
									playable.broadcastPacket(new MagicSkillUse(playable, playable, 6696, 1, 1000, 0));
								}
							}
							else if ("MP".equals(type))
							{
								if (playable.getCurrentMp() != playable.getMaxMp())
								{
									playable.setCurrentMp(playable.getMaxMp());
									playable.broadcastPacket(new MagicSkillUse(playable, playable, 6696, 1, 1000, 0));
								}
							}
							else if ("CP".equals(type))
							{
								if (playable.getCurrentCp() != playable.getMaxCp())
								{
									playable.setCurrentCp(playable.getMaxCp());
									playable.broadcastPacket(new MagicSkillUse(playable, playable, 6696, 1, 1000, 0));
								}
							}
						}
					}
				}
				parseCommunityBoardCommand("_bbsbuffer", player);
				return false;
			}
			
			if (command.startsWith("_bbsbufferremovebuffs"))
			{
				if (!CBufferConfig.BBS_BUFFER_CLEAR)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
				}
				else
				{
					var sb = new StringTokenizer(command, " ");
					sb.nextToken();
					var type = sb.nextToken();
					
					L2Playable playable = null;
					if ("Player".equals(type))
					{
						playable = player;
					}
					else if ("Pet".equals(type))
					{
						playable = player.getSummon();
					}
					if (playable != null)
					{
						playable.stopAllEffectsExceptThoseThatLastThroughDeath();
						playable.broadcastPacket(new MagicSkillUse(playable, playable, 6696, 1, 1000, 0));
					}
				}
				parseCommunityBoardCommand("_bbsbuffer", player);
				return false;
			}
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_error"));
			return false;
		}
		CommunityBoardHandler.separateAndSend(html, player);
		return true;
	}
	
	public void writeCommunityBoardCommand(L2PcInstance activeChar, String arg1, String arg2, String arg3, String arg4, String arg5)
	{
		
	}
	
	private void genPage(final List<String> list, final String type)
	{
		genPage(list, type, false);
	}
	
	private void genPage(final List<String> list, final String type, boolean isPremium)
	{
		var sb = new StringBuilder("<table><tr>");
		sb.append("<td width=70>Navigation: </td>"); // TODO MessagesData
		for (var i = 0; i < list.size(); i++)
		{
			sb.append(DifferentMethods.buttonTD(String.valueOf(i + 1), "_bbs" + type + (isPremium ? "premium" : "") + "buffer:" + i, 30, 25, "L2UI_CT1.ListCTRL_DF_Title_Down", "L2UI_CT1.ListCTRL_DF_Title"));
		}
		sb.append("<td>").append(DifferentMethods.button("Back", "_bbsbuffer", 60, 25, "L2UI_CT1.ListCTRL_DF_Title_Down", "L2UI_CT1.ListCTRL_DF_Title")).append("</td></tr></table><br><br>");
		
		for (var i = 0; i < list.size(); i++)
		{
			list.set(i, sb.toString() + list.get(i));
		}
	}
	
	public void genPageBuff(final List<String> list, final int start, final String type)
	{
		var sb = new StringBuilder("<table><tr>");
		var i = start;
		var next = false;
		for (; i < _allowBuff.size(); i += 2)
		{
			if (next && ((i % 12) == 0))
			{
				sb.append("</tr><tr>");
			}
			if (next && ((i % 48) == 0))
			{
				break;
			}
			sb.append("<td>").append(buttonBuff(_allowBuff.get(i), _allowBuff.get(i + 1), list.size(), type)).append("</td>");
			next = true;
		}
		sb.append("</tr></table>");
		list.add(sb.toString());
		if ((i + 2) <= _allowBuff.size())
		{
			genPageBuff(list, i, type);
		}
	}
	
	public void genPremiumPageBuff(final List<String> list, final int start, final String type)
	{
		var sb = new StringBuilder("<table><tr>");
		var i = start;
		var next = false;
		for (; i < premiumBuffs.size(); i++)
		{
			if (next && ((i % 6) == 0))
			{
				sb.append("</tr><tr>");
			}
			if (next && ((i % 18) == 0))
			{
				break;
			}
			var buff = getPremiumBuff(premiumBuffs.get(i).getId(), premiumBuffs.get(i).getLevel());
			if (buff != null)
			{
				sb.append("<td>").append(buttonBuff(premiumBuffs.get(i).getId(), premiumBuffs.get(i).getLevel(), list.size(), type, buff.getItemId(), buff.getItemCount())).append("</td>");
			}
			next = true;
		}
		sb.append("</tr></table>");
		list.add(sb.toString());
		if ((i + 1) <= premiumBuffs.size())
		{
			genPremiumPageBuff(list, i, type);
		}
	}
	
	public String buttonBuff(int id, int level, int page, String type)
	{
		return buttonBuff(id, level, page, type, 0, 0);
	}
	
	public String buttonBuff(int id, int level, int page, String type, int itemId, int itemCount)
	{
		var skillId = Integer.toString(id);
		var sb = new StringBuilder("<table width=100>");
		String icon;
		
		if (skillId.length() < 4)
		{
			icon = 0 + skillId;
		}
		else if (skillId.length() < 3)
		{
			icon = 00 + skillId;
		}
		else if ((id == 4700) || (id == 4699))
		{
			icon = "1331";
		}
		else if ((id == 4702) || (id == 4703))
		{
			icon = "1332";
		}
		else if (id == 1517)
		{
			icon = "1499";
		}
		else if (id == 1518)
		{
			icon = "1502";
		}
		else
		{
			icon = skillId;
		}
		
		var name = SkillData.getInstance().getSkill(id, level).getName();
		name = name.replace("Dance of the", "D.");
		name = name.replace("Dance of", "D.");
		name = name.replace("Song of", "S.");
		name = name.replace("Improved", "I.");
		name = name.replace("Awakening", "A.");
		name = name.replace("Blessing", "Bless.");
		name = name.replace("Protection", "Protect.");
		name = name.replace("Critical", "C.");
		name = name.replace("Condition", "Con.");
		sb.append("<tr><td><center><img src=icon.skill").append(icon).append(" width=32 height=32><br>");
		if (itemId != 0)
		{
			sb.append("<font color=437AD0>Price: </font>" + itemCount + " ").append(ItemTable.getInstance().getTemplate(itemId).getName()).append("<br>");
		}
		sb.append("<font color=F2C202>Level ").append(level).append("</font></center></td></tr>");
		sb.append(DifferentMethods.buttonTR(name, "_bbscastbuff:" + id + ":" + level + ":" + page + ":" + type + (""), 100, 25, "L2UI_CT1.ListCTRL_DF_Title_Down", "L2UI_CT1.ListCTRL_DF_Title"));
		sb.append("</table>");
		return sb.toString();
	}
	
	public void buffList(List<BuffBBSHolder> list, L2Playable playable)
	{
		new BuffTask(list, playable).run();
	}
	
	public boolean checkCondition(L2PcInstance player)
	{
		if (player.isAlikeDead() || player.isFakeDeath())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_cannot_used_death"));
			return false;
		}
		
		if (player.isCursedWeaponEquipped())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_cannot_used_cursed_weapons"));
			return false;
		}
		
		if (player.isJailed())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_cannot_used_jail"));
			return false;
		}
		
		if (OlympiadManager.getInstance().isRegistered(player) || player.isInOlympiadMode())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_olympiad_enable"));
			return false;
		}
		
		if ((!CBufferConfig.BBS_BUFFER_IN_SIEGE && player.isInSiege()) || (player.getSiegeState() != 0) || player.isInsideZone(ZoneId.SIEGE))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_siege_disable"));
			return false;
		}
		
		if ((!CBufferConfig.BBS_BUFFER_IN_PVP && (player.getPvpFlag() > 0)) || (!CBufferConfig.BBS_BUFFER_IN_BATTLE && (player.isInCombat() || player.isMovementDisabled() || AttackStanceTaskManager.getInstance().hasAttackStanceTask(player))))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_combat_enable"));
			return false;
		}
		
		if ((!CBufferConfig.BBS_BUFFER_IN_EVENTS && (player.getTeam() != Team.NONE)) || (player.getBlockCheckerArena() >= 0) || (player.isOnEvent() || player.isCombatFlagEquipped()))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_events"));
			return false;
		}
		
		if (player.isFlying() || player.isFlyingMounted())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_flying"));
			return false;
		}
		
		if (player.getInstanceId() > 0)
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_instance"));
			return false;
		}
		
		if (player.isTransformed())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_transformed"));
			return false;
		}
		
		if (player.isInStoreMode() || player.getTradeRefusal() || player.isInOfflineMode())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_private_store"));
			return false;
		}
		
		if (player.isFishing())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_fishing"));
			return false;
		}
		
		if (!checkLevel(player.getLevel()))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_minMaxLevel").replace("%s%", CBufferConfig.BBS_BUFFER_PLAYER_MIN_LEVEL + "").replace("%c%", CBufferConfig.BBS_BUFFER_PLAYER_MAX_LEVEL + ""));
			return false;
		}
		return !player.isDead();
	}
	
	public boolean checkLevel(int level)
	{
		return (level >= CBufferConfig.BBS_BUFFER_PLAYER_MIN_LEVEL) && (level <= CBufferConfig.BBS_BUFFER_PLAYER_MAX_LEVEL);
	}
	
	public void onInit()
	{
		_pageBuffPlayer = new ArrayList<>();
		_pageBuffPet = new ArrayList<>();
		
		_pagePremiumBuffPlayer = new ArrayList<>();
		_pagePremiumBuffPet = new ArrayList<>();
		
		genPageBuff(_pageBuffPlayer, 0, "Player");
		genPage(_pageBuffPlayer, "player");
		genPageBuff(_pageBuffPet, 0, "Pet");
		genPage(_pageBuffPet, "pet");
		
		if ((CBufferConfig.BBS_BUFFS_PA_LIST != null) && !CBufferConfig.BBS_BUFFS_PA_LIST.equals(""))
		{
			parsePremiumBuffs();
			genPremiumPageBuff(_pagePremiumBuffPlayer, 0, "Player");
			genPage(_pagePremiumBuffPlayer, "player", true);
			genPremiumPageBuff(_pagePremiumBuffPet, 0, "Pet");
			genPage(_pagePremiumBuffPet, "pet", true);
		}
	}
	
	private void parsePremiumBuffs()
	{
		try
		{
			premiumBuffs = Util.parseTemplateConfig(CBufferConfig.BBS_BUFFS_PA_LIST, PremiumBuff.class);
		}
		catch (Exception ex)
		{
			LOG.warn("", ex);
		}
	}
	
	private void buffPremium(L2PcInstance player, L2Playable playable, int id, int lvl)
	{
		var buff = getPremiumBuff(id, lvl);
		if (buff == null)
		{
			return;
		}
		
		if (DifferentMethods.getPay(player, buff.getItemId(), buff.getItemCount()))
		{
			buffList(Collections.singletonList(new BuffBBSHolder(id, lvl)), playable);
		}
	}
	
	private PremiumBuff getPremiumBuff(int id, int lvl)
	{
		for (var buff : premiumBuffs)
		{
			if ((buff.getId() == id) && (buff.getLevel() == lvl))
			{
				return buff;
			}
		}
		return null;
	}
	
	@Override
	public String[] getCommunityBoardCommands()
	{
		return COMMANDS;
	}
}
