/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.communityboard.custom;

import java.util.Date;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.bbs.CBasicConfig;
import com.l2jserver.gameserver.configuration.config.bbs.CCabinetConfig;
import com.l2jserver.gameserver.configuration.config.custom.CustomConfig;
import com.l2jserver.gameserver.configuration.config.custom.PremiumConfig;
import com.l2jserver.gameserver.data.xml.impl.ClassListData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.bbs.Security;
import com.l2jserver.gameserver.util.DifferentMethods;

/**
 * Private Cabinet board.
 */
public class CabinetBoard implements IParseBoardHandler
{
	private static final String[] COMMANDS =
	{
		"_bbscabinet"
	};
	
	@Override
	public boolean parseCommunityBoardCommand(String command, L2PcInstance player)
	{
		if (!CCabinetConfig.ENABLE_BBS_CABINET)
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
			CommunityBoardHandler.getInstance().handleParseCommand("_bbshome", player);
			return false;
		}
		
		String html = "";
		var customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
		var ACTIVED = "<font color=00FF00>" + MessagesData.getInstance().getMessage(player, "html_ON") + "</font>";
		var DESATIVED = "<font color=FF0000>" + MessagesData.getInstance().getMessage(player, "html_OFF") + "</font>";
		
		if (command.startsWith("_bbscabinet"))
		{
			var bypass = command.split(":");
			if (bypass[1].equals("show"))
			{
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "cabinet/page/page-1.html");
				html = html.replace("<?status?>", MessagesData.getInstance().getMessage(player, "html_status"));
				html = html.replace("<?status_sex?>", String.valueOf(CCabinetConfig.BBS_CABINET_ENABLE_SEX_CHANGE ? ACTIVED : DESATIVED));
				html = html.replace("<?status_premium?>", String.valueOf(PremiumConfig.PREMIUM_SYSTEM_ENABLED ? ACTIVED : DESATIVED));
				html = html.replace("<?status_cfg?>", String.valueOf(CustomConfig.ALLOW_MENU_VOICE_COMMAND ? ACTIVED : DESATIVED));
				html = html.replace("<?status_rep?>", String.valueOf(CustomConfig.ALLOW_REPAIR_VOICE_COMMAND ? ACTIVED : DESATIVED));
				html = html.replace("<?status_noble?>", String.valueOf(CCabinetConfig.BBS_CABINET_ENABLE_NOBLESSE_CHANGE ? ACTIVED : DESATIVED));
				html = html.replace("<?status_pk?>", String.valueOf(CCabinetConfig.BBS_CABINET_ENABLE_PK_REDUCE_CHANGE ? ACTIVED : DESATIVED));
				html = html.replace("<?status_title_color?>", String.valueOf(CCabinetConfig.BBS_CABINET_ENABLE_TITLE_COLOR_CHANGE ? ACTIVED : DESATIVED));
				html = html.replace("<?status_nick_name?>", String.valueOf(CCabinetConfig.BBS_CABINET_ENABLE_NICK_NAME_CHANGE ? ACTIVED : DESATIVED));
				html = html.replace("<?status_nick_color?>", String.valueOf(CCabinetConfig.BBS_CABINET_NICK_NAME_COLOR_CHANGE ? ACTIVED : DESATIVED));
				html = html.replace("<?status_add?>", "<font color=FF0000>" + MessagesData.getInstance().getMessage(player, "html_develop") + "</font>");
			}
			else if (bypass[1].equals("show-2"))
			{
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "cabinet/page/page-2.html");
				html = html.replace("<?status?>", MessagesData.getInstance().getMessage(player, "html_status"));
				html = html.replace("<?status_pass?>", String.valueOf(CCabinetConfig.BBS_CABINET_CHANGE_PASSWORD ? ACTIVED : DESATIVED));
				html = html.replace("<?status_add?>", "<font color=FF0000>" + MessagesData.getInstance().getMessage(player, "html_develop") + "</font>");
				// TODO add service
			}
			else if (bypass[1].equals("show-3"))
			{
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "cabinet/page/page-3.html");
				
				html = html.replace("<?status?>", MessagesData.getInstance().getMessage(player, "html_status"));
				html = html.replace("<?status_add?>", "<font color=FF0000>" + MessagesData.getInstance().getMessage(player, "html_develop") + "</font>");
			}
			else if (bypass[1].equals("security"))
			{
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "cabinet/security.html");
				
				var data = getLogAccount(player.getAccountName());
				for (int i = 0; i < 5; i++)
				{
					try
					{
						html = html.replace("{last_ip_" + (i + 1) + "}", data.ip[i]);
						html = html.replace("{last_time_" + (i + 1) + "}", (data.time[i].longValue() == 0L) ? "..." : TimeUtils.DATE_TIME_FORMAT_FIX.format(new Date(data.time[i].longValue() * 1000L)));
					}
					catch (NullPointerException e)
					{
						html = html.replace("{last_ip_" + (i + 1) + "}", "...");
						html = html.replace("{last_time_" + (i + 1) + "}", "...");
					}
				}
				
				html = html.replace("{bypass_ip}", Security.check(player, true, false, false, false));
				html = html.replace("{bypass_hwid}", Security.check(player, false, true, false, false));
				html = html.replace("{status_ip}", Security.check(player, false, false, true, false));
				html = html.replace("{status_hwid}", Security.check(player, false, false, false, true));
			}
			else if (command.startsWith("_bbscabinet:getsecurity"))
			{
				var page = command.split(":");
				
				if (page[1].equals("lockip"))
				{
					Security.lock(player, true, false);
				}
				else if (page[1].equals("unlockip"))
				{
					Security.unlock(player, true, false);
				}
				else if (page[1].equals("lockhwid"))
				{
					Security.lock(player, false, true);
				}
				else if (page[1].equals("unlockhwid"))
				{
					Security.unlock(player, false, true);
				}
				parseCommunityBoardCommand("_bbscabinet:security", player);
				return false;
			}
			else if (bypass[1].equals("games"))
			{
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "cabinet/games/index.html");
			}
			else
			{
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "cabinet/" + bypass[1] + ".html");
			}
			
			html = html.replace("<?player_name?>", String.valueOf(player.getName()));
			html = html.replace("<?player_class?>", String.valueOf(ClassListData.getInstance().getClass(player.getClassId()).getClientCode()));
			html = html.replace("<?player_clan1?>", String.valueOf((player.getClan() != null) ? player.getClan().getName() : "<font color=\"FF0000\">" + MessagesData.getInstance().getMessage(player, "html_no") + "</font>"));
			html = html.replace("<?player_ally?>", String.valueOf(((player.getClan() != null) && (player.getClan().getAllyName() != null)) ? player.getClan().getAllyName() : "<font color=\"FF0000\">" + MessagesData.getInstance().getMessage(player, "html_no") + "</font>"));
			html = html.replace("<?player_level?>", String.valueOf(player.getLevel()));
			html = html.replace("<?player_pvp?>", String.valueOf(player.getPvpKills()));
			html = html.replace("<?player_pk?>", String.valueOf(player.getPkKills()));
			html = html.replace("<?online_time?>", DifferentMethods.getPlayerTime(player, (int) player.getOnlineTime()));
			html = html.replace("<?premium_img?>", String.valueOf(CommunityBoardHandler.images(player)));
		}
		CommunityBoardHandler.separateAndSend(html, player);
		return true;
	}
	
	public AccountLog getLogAccount(String account)
	{
		var data = new AccountLog();
		var number = 0;
		
		// "SELECT * FROM `accounts` WHERE login = ? ORDER BY lastactive DESC LIMIT 0, 5;"
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("SELECT * FROM account_log WHERE login=? ORDER BY time DESC LIMIT 0, 5;")) // var ps = con.prepareStatement("SELECT * FROM " + Config.GAME_SERVER_LOGIN_DATABASE_NAME + ".accounts WHERE login=? ORDER BY lastactive DESC LIMIT 0, 5;");
		{
			ps.setString(1, account);
			
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					if (!rs.getString("login").isEmpty())
					{
						data.ip[number] = rs.getString("ip");
						data.time[number] = Long.valueOf(rs.getLong("time"));
					}
					else
					{
						data.ip[number] = "...";
						data.time[number] = Long.valueOf(0L);
					}
					number++;
				}
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return data;
	}
	
	public class AccountLog
	{
		private final String[] ip = new String[10];
		
		private final Long[] time = new Long[10];
	}
	
	@Override
	public String[] getCommunityBoardCommands()
	{
		return COMMANDS;
	}
}