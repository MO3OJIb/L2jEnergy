/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.communityboard.custom;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.bbs.CBasicConfig;
import com.l2jserver.gameserver.configuration.config.bbs.CCareerConfig;
import com.l2jserver.gameserver.data.xml.impl.ClassListData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.actors.ClassId;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.util.DifferentMethods;

/**
 * Career board.
 */
public class CareerBoard implements IParseBoardHandler
{
	private static final String[] COMMANDS =
	{
		"_bbscareer"
	};
	
	@Override
	public boolean parseCommunityBoardCommand(String command, L2PcInstance player)
	{
		if (!CCareerConfig.ENABLE_BBS_CLASS_MASTERS)
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
			CommunityBoardHandler.getInstance().handleParseCommand("_bbshome", player);
			return false;
		}
		
		if (!DifferentMethods.checkFirstConditions(player))
		{
			return false;
		}
		
		if (command.startsWith("_bbscareer"))
		{
			showClassPage(player);
		}
		else if (command.startsWith("_bbscareer:change_class"))
		{
			var data = command.split(":");
			changeClass(player, Integer.parseInt(data[2]), Integer.parseInt(data[3]), 0);
		}
		return true;
	}
	
	public void showClassPage(final L2PcInstance player)
	{
		var classId = player.getClassId();
		var jobLevel = classId.level();
		var level = player.getLevel();
		var html = "";
		var customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
		
		if (CCareerConfig.BBS_CLASS_MASTERS_LIST.isEmpty() || !CCareerConfig.BBS_CLASS_MASTERS_LIST.contains(jobLevel))
		{
			jobLevel = 4;
		}
		
		var content = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "classMaster/index.html");
		var template = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "classMaster/template.html");
		String block = null;
		
		if (((level >= 20) && (jobLevel == 1)) || ((level >= 40) && (jobLevel == 2)) || (((level >= 76) && (jobLevel == 3)) && (CCareerConfig.BBS_CLASS_MASTERS_LIST.contains(jobLevel))))
		{
			html += ("<table width=755>");
			html += (page(("<center><font color=865D2E>" + MessagesData.getInstance().getMessage(player, "community_board_classMaster_choiceClass") + "</font></center>")));
			html += ("</table>");
			int id = jobLevel - 1;
			
			for (var cid : ClassId.values())
			{
				if (cid == ClassId.INSPECTOR)
				{
					continue;
				}
				
				if (cid.childOf(classId) && (cid.level() == (classId.level() + 1)))
				{
					block = template;
					block = block.replace("{icon}", "icon.etc_royal_membership_i00");
					block = block.replace("{name}", ClassListData.getInstance().getClass(cid.getId()).getClientCode());
					switch (payType(id))
					{
						case 0 ->
						{
							block = block.replace("{action_name}", "<font color=99CC66>"
								+ MessagesData.getInstance().getMessage(player, "community_board_classMaster_cost").replace("%s%", DifferentMethods.formatPay(player, CCareerConfig.BBS_CLASS_MASTERS_PRICE_COUNT[id], CCareerConfig.BBS_CLASS_MASTERS_PRICE_ITEM_ID[id])) + "</font>");
							block = block.replace("{link}", "bypass _bbscareer:change_class:" + cid.getId() + ":" + id + ":0");
						}
						case 1 ->
						{
							block = block.replace("{action_name}", "<font color=99CC66>"
								+ MessagesData.getInstance().getMessage(player, "community_board_classMaster_cost").replace("%s%", DifferentMethods.formatPay(player, CCareerConfig.BBS_CLASS_MASTERS_PRICE_SECOND_COUNT[id], CCareerConfig.BBS_CLASS_MASTERS_PRICE_SECOND_ITEM_ID[id])) + "</font>");
							block = block.replace("{link}", "bypass _bbscareer:change_class:" + cid.getId() + ":" + id + ":1");
						}
						case 2 ->
						{
							block = block.replace("{action_name}", "<font color=99CC66>" + MessagesData.getInstance().getMessage(player, "community_board_classMaster_costChoice") + "</font>");
							block = block.replace("{link}", "bypass _bbsbypass:services.Class:choice " + cid.getId() + " " + id + ";_bbscareer");
						}
					}
					block = block.replace("{value}", MessagesData.getInstance().getMessage(player, "community_board_classMaster_change"));
					html += block;
				}
			}
			content = content.replace("{info}", "");
		}
		else
		{
			var info = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "classMaster/info.html");
			info = info.replace("{current}", ClassListData.getInstance().getClass(player.getClassId()).getClientCode());
			html += ("<table width=755>");
			
			switch (jobLevel)
			{
				case 1 -> info.replace("{info}", MessagesData.getInstance().getMessage(player, "community_board_classMaster_needLevel_20"));
				case 2 -> info.replace("{info}", MessagesData.getInstance().getMessage(player, "community_board_classMaster_needLevel_40"));
				case 3 -> info.replace("{info}", MessagesData.getInstance().getMessage(player, "community_board_classMaster_needLevel_76"));
				case 4 -> info.replace("{info}", MessagesData.getInstance().getMessage(player, "community_board_classMaster_you_have_learned_all_available_professions"));
			}
			content = content.replace("{info}", info);
			html += ("</table>");
		}
		
		if (CCareerConfig.BBS_CLASS_MASTER_ADD_SUB_CLASS)
		{
			block = template;
			block = block.replace("{icon}", "icon.etc_quest_subclass_reward_i00");
			block = block.replace("{name}", MessagesData.getInstance().getMessage(player, "community_board_classMaster_subclass_add_info"));
			block = block.replace("{link}", "bypass _bbsbypass:services.Subclass:page choice;_bbscareer"); // TODO: need add service
			
			block = block.replace("{action_name}", "<font color=99CC66>"
				+ MessagesData.getInstance().getMessage(player, "community_board_classMaster_cost").replace("%s%", DifferentMethods.formatPay(player, CCareerConfig.BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT[0], (int) CCareerConfig.BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT[1])) + "</font>");
			block = block.replace("{value}", MessagesData.getInstance().getMessage(player, "button_add"));
			html += block;
		}
		
		if (CCareerConfig.BBS_CLASS_MASTER_CHANGE_SUB_CLASS)
		{
			block = template;
			block = block.replace("{icon}", "icon.etc_quest_subclass_reward_i00");
			block = block.replace("{name}", MessagesData.getInstance().getMessage(player, "community_board_classMaster_change_info"));
			block = block.replace("{link}", "bypass _bbsbypass:services.Subclass:change;_bbscareer"); // TODO: need add service
			block = block.replace("{action_name}", "<font color=99CC66>"
				+ MessagesData.getInstance().getMessage(player, "community_board_classMaster_cost").replace("%s%", DifferentMethods.formatPay(player, CCareerConfig.BBS_CLASS_MASTER_SUB_CHANGE_PRICE_COUNT[0], (int) CCareerConfig.BBS_CLASS_MASTER_SUB_CHANGE_PRICE_COUNT[1])) + "</font>");
			block = block.replace("{value}", MessagesData.getInstance().getMessage(player, "button_change"));
			html += block;
		}
		
		if (CCareerConfig.BBS_CLASS_MASTER_CANCEL_SUB_CLASS)
		{
			block = template;
			block = block.replace("{icon}", "icon.etc_quest_subclass_reward_i00");
			block = block.replace("{name}", MessagesData.getInstance().getMessage(player, "community_board_classMaster_cancel_info"));
			block = block.replace("{link}", "bypass _bbsbypass:services.Subclass:cancel;_bbscareer"); // TODO: need add service
			block = block.replace("{action_name}", "<font color=99CC66>"
				+ MessagesData.getInstance().getMessage(player, "community_board_classMaster_cost").replace("%s%", DifferentMethods.formatPay(player, CCareerConfig.BBS_CLASS_MASTER_SUB_CANCEL_PRICE_COUNT[0], (int) CCareerConfig.BBS_CLASS_MASTER_SUB_CANCEL_PRICE_COUNT[1])) + "</font>");
			block = block.replace("{value}", MessagesData.getInstance().getMessage(player, "button_cancel"));
			html += block;
		}
		
		content = content.replace("{classmaster}", html.toString());
		CommunityBoardHandler.separateAndSend(content, player);
	}
	
	public void changeClass(final L2PcInstance player, final int classID, final int id, final int pay)
	{
		if (player == null)
		{
			return;
		}
		
		int item = 0;
		long count = -1;
		
		switch (pay)
		{
			case 0 ->
			{
				item = CCareerConfig.BBS_CLASS_MASTERS_PRICE_ITEM_ID[id];
				count = CCareerConfig.BBS_CLASS_MASTERS_PRICE_COUNT[id];
			}
			case 1 ->
			{
				item = CCareerConfig.BBS_CLASS_MASTERS_PRICE_SECOND_ITEM_ID[id];
				count = CCareerConfig.BBS_CLASS_MASTERS_PRICE_SECOND_COUNT[id];
			}
		}
		
		if (DifferentMethods.getPay(player, item, count))
		{
			var RequestClassId = Optional.empty();
			var availClasses = getAvailClasses(player.getClassId());
			for (var _class : availClasses)
			{
				if (_class.getId() == classID)
				{
					RequestClassId = Optional.of(_class);
					break;
				}
			}
			
			if (!RequestClassId.isPresent())
			{
				return;
			}
			
			if (player.getClassId().level() == 3)
			{
				player.sendPacket(SystemMessageId.YOU_HAVE_COMPLETED_THE_QUEST_FOR_3RD_OCCUPATION_CHANGE_AND_MOVED_TO_ANOTHER_CLASS_CONGRATULATIONS);
			}
			else
			{
				player.sendPacket(SystemMessageId.CONGRATULATIONS_YOU_HAVE_COMPLETED_A_CLASS_TRANSFER);
			}
			
			player.setClassId(classID);
			
			if (player.isSubClassActive())
			{
				player.getSubClasses().get(player.getClassIndex()).setClassId(player.getActiveClass());
			}
			else
			{
				player.setBaseClass(player.getActiveClass());
			}
			player.broadcastUserInfo();
		}
		showClassPage(player);
	}
	
	public String page(final String text)
	{
		var sb = new StringBuilder();
		sb.append("<tr>");
		sb.append("<td width=20></td>");
		sb.append("<td width=690 height=15 align=left valign=top>");
		sb.append(text);
		sb.append("</td>");
		sb.append("</tr>");
		return sb.toString();
	}
	
	public int payType(final int id)
	{
		if ((CCareerConfig.BBS_CLASS_MASTERS_PRICE_ITEM_ID[id] != 0) && (CCareerConfig.BBS_CLASS_MASTERS_PRICE_SECOND_ITEM_ID[id] == 0))
		{
			return 0;
		}
		if ((CCareerConfig.BBS_CLASS_MASTERS_PRICE_ITEM_ID[id] == 0) && (CCareerConfig.BBS_CLASS_MASTERS_PRICE_SECOND_ITEM_ID[id] != 0))
		{
			return 1;
		}
		if ((CCareerConfig.BBS_CLASS_MASTERS_PRICE_ITEM_ID[id] != 0) && (CCareerConfig.BBS_CLASS_MASTERS_PRICE_SECOND_ITEM_ID[id] != 0))
		{
			return 2;
		}
		return 0;
	}
	
	public static List<ClassId> getAvailClasses(ClassId playerClass)
	{
		return Stream.of(ClassId.values()).filter(_class -> (_class.level() == (playerClass.level() + 1)) && _class.childOf(playerClass) && (_class != ClassId.INSPECTOR)).collect(Collectors.toList());
	}
	
	public void writeCommunityBoardCommand(L2PcInstance activeChar, String arg1, String arg2, String arg3, String arg4, String arg5)
	{
		
	}
	
	@Override
	public String[] getCommunityBoardCommands()
	{
		return COMMANDS;
	}
}
