/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.communityboard.custom;

import com.l2jserver.gameserver.configuration.config.bbs.CBasicConfig;
import com.l2jserver.gameserver.configuration.config.custom.CustomConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.handler.VoicedCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.ShowBoard;

/**
 * Command board.
 */
public class CommandBoard implements IParseBoardHandler
{
	private static final String[] COMMANDS =
	{
		"_bbscomand"
	};
	
	@Override
	public boolean parseCommunityBoardCommand(String command, L2PcInstance player)
	{
		if (!CBasicConfig.ENABLE_BBS)
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
			CommunityBoardHandler.getInstance().handleParseCommand("_bbshome", player);
			return false;
		}
		
		if (command.startsWith("_bbscomand"))
		{
			var bypass = command.split(":");
			if (bypass[1].equals("cfg"))
			{
				if (!CustomConfig.ALLOW_MENU_VOICE_COMMAND)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
					parseCommunityBoardCommand("_bbscabinet:show", player);
					return false;
				}
				
				var cfg = VoicedCommandHandler.getInstance().getHandler("cfg");
				if (cfg != null)
				{
					player.sendPacket(new ShowBoard());
					cfg.useVoicedCommand("cfg", player, "");
					return false;
				}
			}
			else if (bypass[1].equals("repair"))
			{
				if (!CustomConfig.ALLOW_REPAIR_VOICE_COMMAND)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
					parseCommunityBoardCommand("_bbscabinet:show", player);
					return false;
				}
				
				var repair = VoicedCommandHandler.getInstance().getHandler("repair");
				if (repair != null)
				{
					player.sendPacket(new ShowBoard());
					repair.useVoicedCommand("repair", player, "");
					return false;
				}
			}
		}
		return true;
	}
	
	public void writeCommunityBoardCommand(L2PcInstance activeChar, String arg1, String arg2, String arg3, String arg4, String arg5)
	{
		
	}
	
	@Override
	public String[] getCommunityBoardCommands()
	{
		return COMMANDS;
	}
}
