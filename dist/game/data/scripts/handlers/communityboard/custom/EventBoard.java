/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.communityboard.custom;

import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.bbs.CBasicConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.eventengine.GameEventManager;

/**
 * Event board.
 * @author Мо3олЬ
 */
public class EventBoard implements IParseBoardHandler
{
	private static final String[] COMMANDS =
	{
		"_bbsevents"
	};
	
	@Override
	public boolean parseCommunityBoardCommand(String command, L2PcInstance player)
	{
		if (!CBasicConfig.ENABLE_BBS)
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
			CommunityBoardHandler.getInstance().handleParseCommand("_bbshome", player);
			return false;
		}
		
		String html = null;
		var customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
		
		if (command.equals("_bbsevents"))
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "events/index.html");
		}
		else if (command.startsWith("_bbsevents:page"))
		{
			var path = command.split(":");
			if (path.length > 3)
			{
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "events/" + path[2] + "/" + path[3] + ".html");
			}
			else
			{
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "events/" + path[2] + ".html");
			}
		}
		else if (command.equalsIgnoreCase("_bbsevents:tvt_reg"))
		{
			if (GameEventManager.getInstance().getEvent().register(player))
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_event_you_reg_for_event"));
			}
		}
		else if (command.equalsIgnoreCase("_bbsevents:tvt_unreg"))
		{
			if (GameEventManager.getInstance().getEvent().unRegister(player))
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_event_you_unregistered_for_event"));
			}
		}
		CommunityBoardHandler.separateAndSend(html, player);
		return true;
	}
	
	@Override
	public String[] getCommunityBoardCommands()
	{
		return COMMANDS;
	}
}
