/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.communityboard.custom;

import java.util.HashMap;
import java.util.Map;

import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.bbs.CBasicConfig;
import com.l2jserver.gameserver.configuration.config.bbs.CForgeConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.enums.items.EtcItemType;
import com.l2jserver.gameserver.enums.items.WeaponType;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.bbs.ForgeElement;
import com.l2jserver.gameserver.model.itemcontainer.Inventory;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ExShowVariationCancelWindow;
import com.l2jserver.gameserver.network.serverpackets.ExShowVariationMakeWindow;
import com.l2jserver.gameserver.network.serverpackets.InventoryUpdate;
import com.l2jserver.gameserver.util.DifferentMethods;

/**
 * Forge board. TODO: need test
 */
public class ForgeBoard implements IParseBoardHandler
{
	// --------------------------------------------------
	public static int ENCHANT_BY_ITEM = 4037;
	public static int ENCHANT_PRICE = 1;
	public static int ENCHANT_MAX = 30;
	
	private static final String[] COMMANDS =
	{
		"_bbsforge"
	};
	
	@Override
	public boolean parseCommunityBoardCommand(String command, L2PcInstance player)
	{
		if (!CForgeConfig.BBS_FORGE_ENABLED)
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
			CommunityBoardHandler.getInstance().handleParseCommand("_bbshome", player);
			return false;
		}
		
		String html = null;
		var customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
		if (command.equals("_bbsforge"))
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "forge/index.html");
		}
		else if (command.startsWith("_bbsforge:augment"))
		{
			player.sendPacket(SystemMessageId.SELECT_THE_ITEM_TO_BE_AUGMENTED);
			player.sendPacket(ExShowVariationMakeWindow.STATIC_PACKET);
			parseCommunityBoardCommand("_bbsforge", player);
			return false;
		}
		else if (command.startsWith("_bbsforge:remove:augment"))
		{
			player.sendPacket(SystemMessageId.SELECT_THE_ITEM_FROM_WHICH_YOU_WISH_TO_REMOVE_AUGMENTATION);
			player.sendPacket(ExShowVariationCancelWindow.STATIC_PACKET);
			parseCommunityBoardCommand("_bbsforge", player);
			return false;
		}
		else if (command.equals("_bbsforge:enchant:list"))
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "forge/itemlist.html");
			
			var head = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_HEAD);
			var chest = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_CHEST);
			var legs = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LEGS);
			var gloves = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_GLOVES);
			var feet = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_FEET);
			
			var lhand = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LHAND);
			var rhand = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);
			
			var lfinger = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LFINGER);
			var rfinger = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RFINGER);
			var neck = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_NECK);
			var lear = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LEAR);
			var rear = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_REAR);
			
			Map<Integer, String[]> data = new HashMap<>();
			
			// Armors
			data.put(Inventory.PAPERDOLL_HEAD, ForgeElement.generateEnchant(head, CForgeConfig.BBS_FORGE_ENCHANT_MAX[1], Inventory.PAPERDOLL_HEAD, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_CHEST, ForgeElement.generateEnchant(chest, CForgeConfig.BBS_FORGE_ENCHANT_MAX[1], Inventory.PAPERDOLL_CHEST, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_LEGS, ForgeElement.generateEnchant(legs, CForgeConfig.BBS_FORGE_ENCHANT_MAX[1], Inventory.PAPERDOLL_LEGS, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_GLOVES, ForgeElement.generateEnchant(gloves, CForgeConfig.BBS_FORGE_ENCHANT_MAX[1], Inventory.PAPERDOLL_GLOVES, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_FEET, ForgeElement.generateEnchant(feet, CForgeConfig.BBS_FORGE_ENCHANT_MAX[1], Inventory.PAPERDOLL_FEET, player.getLanguage()));
			
			// Jewels
			data.put(Inventory.PAPERDOLL_LFINGER, ForgeElement.generateEnchant(lfinger, CForgeConfig.BBS_FORGE_ENCHANT_MAX[2], Inventory.PAPERDOLL_LFINGER, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_RFINGER, ForgeElement.generateEnchant(rfinger, CForgeConfig.BBS_FORGE_ENCHANT_MAX[2], Inventory.PAPERDOLL_RFINGER, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_NECK, ForgeElement.generateEnchant(neck, CForgeConfig.BBS_FORGE_ENCHANT_MAX[2], Inventory.PAPERDOLL_NECK, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_LEAR, ForgeElement.generateEnchant(lear, CForgeConfig.BBS_FORGE_ENCHANT_MAX[2], Inventory.PAPERDOLL_LEAR, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_REAR, ForgeElement.generateEnchant(rear, CForgeConfig.BBS_FORGE_ENCHANT_MAX[2], Inventory.PAPERDOLL_REAR, player.getLanguage()));
			
			// Weapons
			data.put(Inventory.PAPERDOLL_RHAND, ForgeElement.generateEnchant(rhand, CForgeConfig.BBS_FORGE_ENCHANT_MAX[0], Inventory.PAPERDOLL_RHAND, player.getLanguage()));
			if ((rhand != null) && ((rhand.getItem().getItemType() == WeaponType.BLUNT) || (rhand.getItem().getItemType() == WeaponType.BOW) || (rhand.getItem().getItemType() == WeaponType.DUALDAGGER) || (rhand.getItem().getItemType() == WeaponType.ANCIENTSWORD)
				|| (rhand.getItem().getItemType() == WeaponType.CROSSBOW) || (rhand.getItem().getItemType() == WeaponType.SWORD) || (rhand.getItem().getItemType() == WeaponType.DUALFIST) || (rhand.getItem().getItemType() == WeaponType.DUAL) || (rhand.getItem().getItemType() == WeaponType.POLE)
				|| (rhand.getItem().getItemType() == WeaponType.FIST)))
			{
				data.put(Inventory.PAPERDOLL_LHAND, new String[]
				{
					rhand.getItem().getIcon(),
					(rhand.getName() + " " + (rhand.getEnchantLevel() > 0 ? "+" + rhand.getEnchantLevel() : "")),
					"<font color=\"FF0000\">...</font>",
					"L2UI_CT1.ItemWindow_DF_SlotBox_Disable"
				});
			}
			else
			{
				data.put(Inventory.PAPERDOLL_LHAND, ForgeElement.generateEnchant(lhand, CForgeConfig.BBS_FORGE_ENCHANT_MAX[0], Inventory.PAPERDOLL_LHAND, player.getLanguage()));
			}
			
			html = html.replace("<?content?>", ForgeElement.page(player));
			
			for (var info : data.entrySet())
			{
				var slot = info.getKey();
				var array = info.getValue();
				html = html.replace("<?" + slot + "_icon?>", array[0]);
				html = html.replace("<?" + slot + "_name?>", array[1]);
				html = html.replace("<?" + slot + "_button?>", array[2]);
				html = html.replace("<?" + slot + "_pic?>", array[3]);
			}
		}
		else if (command.startsWith("_bbsforge:enchant:item:"))
		{
			var array = command.split(":");
			var item = Integer.parseInt(array[3]);
			
			var name = ItemTable.getInstance().getTemplate(CForgeConfig.BBS_FORGE_ENCHANT_ITEM).getName();
			
			if (name.isEmpty())
			{
				name = MessagesData.getInstance().getMessage(player, "communityboard_forge_item_no_name");
			}
			
			if ((item < 1) || (item > 12))
			{
				return false;
			}
			
			var _item = player.getInventory().getPaperdollItem(item);
			if ((_item == null))
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_forge_item_null"));
				parseCommunityBoardCommand("_bbsforge:enchant:list", player);
				return false;
			}
			
			if (_item.getItem().getItemType() == EtcItemType.ARROW)
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_forge_item_arrow"));
				parseCommunityBoardCommand("_bbsforge:enchant:list", player);
				return false;
			}
			
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "forge/enchant.html");
			
			var template = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "forge/enchant_template.html");
			
			template = template.replace("{icon}", _item.getItem().getIcon());
			var _name = _item.getName();
			_name = _name.replace(" {PvP}", "");
			
			if (_name.length() > 30)
			{
				_name = _name.substring(0, 29) + "...";
			}
			
			template = template.replace("{name}", _name);
			template = template.replace("{enchant}", _item.getEnchantLevel() <= 0 ? "" : "+" + _item.getEnchantLevel());
			template = template.replace("{msg}", MessagesData.getInstance().getMessage(player, "communityboard_forge_enchant_select"));
			
			var button_tm = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "forge/enchant_button_template.html");
			String button = null;
			String block = null;
			
			var level = _item.isWeapon() ? CForgeConfig.BBS_FORGE_WEAPON_ENCHANT_LVL : _item.isArmor() ? CForgeConfig.BBS_FORGE_ARMOR_ENCHANT_LVL : CForgeConfig.BBS_FORGE_JEWELS_ENCHANT_LVL;
			for (var i = 0; i < level.length; i++)
			{
				if (_item.getEnchantLevel() < level[i])
				{
					block = button_tm;
					block = block.replace("{link}", String.valueOf("bypass _bbsforge:enchant:" + (i * item) + ":" + item));
					block = block.replace("{value}", "+" + level[i] + " (" + (_item.isWeapon() ? CForgeConfig.BBS_FORGE_ENCHANT_PRICE_WEAPON[i] : _item.isArmor() ? CForgeConfig.BBS_FORGE_ENCHANT_PRICE_ARMOR[i] : CForgeConfig.BBS_FORGE_ENCHANT_PRICE_JEWELS[i]) + " " + name + ")");
					button += block;
				}
			}
			
			template = template.replace("{button}", button == null ? "------" : button);
			html = html.replace("<?content?>", template);
		}
		else if (command.startsWith("_bbsforge:enchant:"))
		{
			var array = command.split(":");
			
			var val = Integer.parseInt(array[2]);
			var item = Integer.parseInt(array[3]);
			
			var conversion = val / item;
			
			var _item = player.getInventory().getPaperdollItem(item);
			
			var level = _item.isWeapon() ? CForgeConfig.BBS_FORGE_WEAPON_ENCHANT_LVL : _item.isArmor() ? CForgeConfig.BBS_FORGE_ARMOR_ENCHANT_LVL : CForgeConfig.BBS_FORGE_JEWELS_ENCHANT_LVL;
			var Value = level[conversion];
			
			var max = _item.isWeapon() ? CForgeConfig.BBS_FORGE_ENCHANT_MAX[0] : _item.isArmor() ? CForgeConfig.BBS_FORGE_ENCHANT_MAX[1] : CForgeConfig.BBS_FORGE_ENCHANT_MAX[2];
			if (Value > max)
			{
				return false;
			}
			
			if (_item.getItem().getItemType() == EtcItemType.ARROW)
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_forge_item_arrow"));
				parseCommunityBoardCommand("_bbsforge:enchant:list", player);
				return false;
			}
			
			var price = _item.isWeapon() ? CForgeConfig.BBS_FORGE_ENCHANT_PRICE_WEAPON[conversion] : _item.isArmor() ? CForgeConfig.BBS_FORGE_ENCHANT_PRICE_ARMOR[conversion] : CForgeConfig.BBS_FORGE_ENCHANT_PRICE_JEWELS[conversion];
			
			if (_item.getItem() != null)
			{
				if (DifferentMethods.getPay(player, CForgeConfig.BBS_FORGE_ENCHANT_ITEM, price))
				{
					player.getInventory().equipItem(_item);
					_item.setEnchantLevel(Value);
					player.getInventory().equipItem(_item);
					
					InventoryUpdate iu = new InventoryUpdate();
					iu.addModifiedItem(_item);
					player.sendPacket(iu);
					player.broadcastUserInfo();
					player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_forge_enchant_success").replace("%s%", _item.getName() + "").replace("%i%", Value + ""));
				}
			}
			parseCommunityBoardCommand("_bbsforge:enchant:list", player);
			return false;
		}
		CommunityBoardHandler.separateAndSend(html, player);
		return true;
	}
	
	@Override
	public String[] getCommunityBoardCommands()
	{
		return COMMANDS;
	}
}
