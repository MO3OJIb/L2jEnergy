/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.communityboard.custom;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.LoginServerThread;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.CharacterConfig;
import com.l2jserver.gameserver.configuration.config.bbs.CBasicConfig;
import com.l2jserver.gameserver.configuration.config.bbs.CCabinetConfig;
import com.l2jserver.gameserver.configuration.config.custom.PremiumConfig;
import com.l2jserver.gameserver.data.sql.impl.CharNameTable;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.NpcData;
import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.enums.ZoneId;
import com.l2jserver.gameserver.enums.actors.ClassRace;
import com.l2jserver.gameserver.enums.actors.PlayerSex;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.instancemanager.PremiumManager;
import com.l2jserver.gameserver.instancemanager.QuestManager;
import com.l2jserver.gameserver.model.actor.appearance.PcAppearance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.ExBrExtraUserInfo;
import com.l2jserver.gameserver.network.serverpackets.SkillList;
import com.l2jserver.gameserver.network.serverpackets.SocialAction;
import com.l2jserver.gameserver.network.serverpackets.UserInfo;
import com.l2jserver.gameserver.util.DifferentMethods;
import com.l2jserver.gameserver.util.Util;

/**
 * Service board.
 * @author Мо3олЬ
 */
public class ServiceBoard implements IParseBoardHandler
{
	private static final int Q00234_FatesWhisper = 234;
	private static final int Q00236_SeedsOfChaos = 236;
	private static final int Q00235_MimirsElixir = 235;
	
	private static final String[] COMMANDS =
	{
		"_bbsservice"
	};
	
	@Override
	public boolean parseCommunityBoardCommand(String command, L2PcInstance player)
	{
		if (!CCabinetConfig.ENABLE_BBS_CABINET)
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
			CommunityBoardHandler.getInstance().handleParseCommand("_bbshome", player);
			return false;
		}
		
		String html = "";
		var customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
		
		if (command.startsWith("_bbsservice"))
		{
			var bypass = command.split(":");
			var set = command.split(" ");
			if (bypass[1].equals("premium"))
			{
				if (!PremiumConfig.PREMIUM_SYSTEM_ENABLED)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
					CommunityBoardHandler.getInstance().handleParseCommand("_bbscabinet:show", player);
					return false;
				}
				
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "cabinet/service/player/premium.html");
				html = html.replace("<?premium_data?>", player.getPcPremiumSystem().isActive() ? "" + MessagesData.getInstance().getMessage(player, "html_premium_activated_for") + " - <font color=F2C202>"
					+ TimeUtils.dateTimeFormat(Instant.ofEpochMilli(PremiumManager.getInstance().getPremiumExpiration(player.getAccountName()))) + "</font>.<br>" : "<font color=F2C202>" + MessagesData.getInstance().getMessage(player, "html_no_premium") + "</font><br>");;
				html = html.replace("<?1day?>", String.valueOf(CCabinetConfig.BBS_CABINET_PREMIUM_1_ITEM_COUNT));
				html = html.replace("<?7day?>", String.valueOf(CCabinetConfig.BBS_CABINET_PREMIUM_7_ITEM_COUNT));
				html = html.replace("<?30day?>", String.valueOf(CCabinetConfig.BBS_CABINET_PREMIUM_30_ITEM_COUNT));
				html = html.replace("<?60day?>", String.valueOf(CCabinetConfig.BBS_CABINET_PREMIUM_60_ITEM_COUNT));
				html = html.replace("<?180day?>", String.valueOf(CCabinetConfig.BBS_CABINET_PREMIUM_180_ITEM_COUNT));
				html = html.replace("<?360day?>", String.valueOf(CCabinetConfig.BBS_CABINET_PREMIUM_360_ITEM_COUNT));
				html = html.replace("<?item_name?>", String.valueOf(DifferentMethods.getItemName(CCabinetConfig.BBS_CABINET_PREMIUM_ITEM_ID)));;
			}
			else if (bypass[1].startsWith("setpremium"))
			{
				var count = Integer.parseInt(set[1]);
				var money = Integer.parseInt(set[2]);
				
				var item = ItemTable.getInstance().getTemplate(CCabinetConfig.BBS_CABINET_PREMIUM_ITEM_ID);
				var pay = player.getInventory().getItemByItemId(item.getId());
				
				if ((pay != null) && (pay.getCount() >= money))
				{
					player.destroyItem("Premium", pay, money, player, true);
					PremiumManager.getInstance().addPremiumTime(player.getAccountName(), count, TimeUnit.DAYS);
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_premium_buy_for_day_activity").replace("%i%", count + ""));
				}
				else
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_no_premium_for_count"));
				}
				parseCommunityBoardCommand("_bbsservice:premium", player);
				return false;
			}
			else if (bypass[1].equals("password"))
			{
				if (!CCabinetConfig.BBS_CABINET_CHANGE_PASSWORD)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
					CommunityBoardHandler.getInstance().handleParseCommand("_bbscabinet:show", player);
					return false;
				}
				
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "cabinet/service/player/password.html");
				
				html = html.replace("<?n1?>", String.valueOf(DifferentMethods.doCaptcha(true, false)));
				html = html.replace("<?n2?>", String.valueOf(DifferentMethods.doCaptcha(false, true)));
			}
			else if (bypass[1].startsWith("changepassword"))
			{
				try
				{
					var old = set[1];
					var newPass1 = set[2];
					var newPass2 = set[3];
					var n1 = set[4];
					var n2 = set[5];
					var captcha = set[6];
					changePassword(player, old, newPass1, newPass2, n1, n2, captcha);
				}
				catch (Exception e)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_password_incorrect_input"));
				}
				parseCommunityBoardCommand("_bbsservice:password", player);
				return false;
			}
			else if (bypass[1].equals("titleColor"))
			{
				if (!CCabinetConfig.BBS_CABINET_ENABLE_TITLE_COLOR_CHANGE)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
					CommunityBoardHandler.getInstance().handleParseCommand("_bbscabinet:show", player);
					return false;
				}
				
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "cabinet/service/player/titlecolor.html");
				
				html = html.replace("<?title_color_count?>", String.valueOf(StringUtil.formatNumber(CCabinetConfig.BBS_CABINET_TITLE_COLOR_CHANGE_COUNT)));
				html = html.replace("<?title_color_item?>", String.valueOf(DifferentMethods.getItemName(CCabinetConfig.BBS_CABINET_TITLE_COLOR_CHANGE_ITEM)));
			}
			else if (bypass[1].startsWith("changeTitleColor"))
			{
				playerSetColor(player, set[1]);
				parseCommunityBoardCommand("_bbsservice:titleColor", player);
				return false;
			}
			else if (bypass[1].startsWith("resetColor"))
			{
				if (player.getAppearance().getTitleColor() != PcAppearance.DEFAULT_TITLE_COLOR)
				{
					player.getAppearance().setTitleColor(Integer.decode("0x" + DifferentMethods.RGBtoBGR("FFFFFF")));
					player.broadcastUserInfo();
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_title_color_successfully_used_service"));
				}
				
				player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_title_color_has_standard_color"));
				parseCommunityBoardCommand("_bbsservice:titleColor", player);
				return false;
			}
			else if (bypass[1].equals("nameColor"))
			{
				if (!CCabinetConfig.BBS_CABINET_NICK_NAME_COLOR_CHANGE)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
					CommunityBoardHandler.getInstance().handleParseCommand("_bbscabinet:show", player);
					return false;
				}
				// TODO: add service
			}
			else if (bypass[1].equals("gender"))
			{
				if (!CCabinetConfig.BBS_CABINET_ENABLE_SEX_CHANGE)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
					CommunityBoardHandler.getInstance().handleParseCommand("_bbscabinet:show", player);
					return false;
				}
				
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "cabinet/service/player/change_sex.html");
				String sex = "";
				
				switch (player.getAppearance().getSex())
				{
					case FEMALE -> sex = MessagesData.getInstance().getMessage(player, "html_female");
					case MALE -> sex = MessagesData.getInstance().getMessage(player, "html_male");
				}
				
				html = html.replace("<?sex?>", sex);
				html = html.replace("<?sex_count?>", String.valueOf(StringUtil.formatNumber(CCabinetConfig.BBS_CABINET_SEX_CHANGE_COUNT)));
				html = html.replace("<?sex_item?>", String.valueOf(DifferentMethods.getItemName(CCabinetConfig.BBS_CABINET_SEX_CHANGE_ITEM)));
			}
			else if (bypass[1].startsWith("changeGender"))
			{
				var male = PlayerSex.MALE;
				var female = PlayerSex.FEMALE;
				
				if (player.getRace() == ClassRace.KAMAEL)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_change_gender_not_available_your_race"));
					return false;
				}
				else if (player.isInSiege() || (player.getSiegeState() != 0) || player.isInsideZone(ZoneId.SIEGE))
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_change_gender_cannot_during_siege"));
					return false;
				}
				else if (!player.isInsideZone(ZoneId.PEACE))
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_change_gender_use_only_peaceful_zone"));
					return false;
				}
				else if (DifferentMethods.getPay(player, CCabinetConfig.BBS_CABINET_SEX_CHANGE_ITEM, CCabinetConfig.BBS_CABINET_SEX_CHANGE_COUNT))
				{
					player.getAppearance().setSex(male != player.getAppearance().getSex() ? male : female);
					player.setTarget(player);
					player.broadcastUserInfo();
					player.logout();
				}
				parseCommunityBoardCommand("_bbsservice:gender", player);
				return false;
			}
			else if (bypass[1].equals("reducePks"))
			{
				if (!CCabinetConfig.BBS_CABINET_ENABLE_PK_REDUCE_CHANGE)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
					CommunityBoardHandler.getInstance().handleParseCommand("_bbscabinet:show", player);
					return false;
				}
				
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "cabinet/service/player/change_pk.html");
				html = html.replace("<?correct_pk?>", String.valueOf(player.getPkKills()));
				html = html.replace("<?pk_count?>", String.valueOf(StringUtil.formatNumber(CCabinetConfig.BBS_CABINET_PK_REDUCE_CHANGE_COUNT)));
				html = html.replace("<?pk_item?>", String.valueOf(DifferentMethods.getItemName(CCabinetConfig.BBS_CABINET_PK_REDUCE_CHANGE_ITEM)));
			}
			else if (bypass[1].startsWith("changereducePks"))
			{
				try
				{
					var count = Integer.parseInt(set[1]);
					changePk(player, count);
				}
				catch (Exception ex)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_reduce_pk_incorrect_value"));
				}
				parseCommunityBoardCommand("_bbsservice:reducePks", player);
				return false;
			}
			else if (bypass[1].equals("noble"))
			{
				if (!CCabinetConfig.BBS_CABINET_ENABLE_NOBLESSE_CHANGE)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
					CommunityBoardHandler.getInstance().handleParseCommand("_bbscabinet:show", player);
					return false;
				}
				
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "cabinet/service/player/noblesse.html");
				html = html.replace("<?noble_count?>", String.valueOf(StringUtil.formatNumber(CCabinetConfig.BBS_CABINET_NOBLESSE_CHANGE_COUNT)));
				html = html.replace("<?noble_item?>", String.valueOf(DifferentMethods.getItemName(CCabinetConfig.BBS_CABINET_NOBLESSE_CHANGE_ITEM)));
			}
			else if (bypass[1].startsWith("changeNoble"))
			{
				if (player.isNoble())
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_you_already_noble"));
					return false;
				}
				else if (player.getLevel() < 75)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_purchase_noble_level_75"));
					return false;
				}
				else if (DifferentMethods.getPay(player, CCabinetConfig.BBS_CABINET_NOBLESSE_CHANGE_ITEM, CCabinetConfig.BBS_CABINET_NOBLESSE_CHANGE_COUNT))
				{
					player.setTarget(player);
					
					// Olympiad.addNoble(player);
					player.setNoble(true);
					// player.updatePledgeClass();
					player.sendPacket(new SkillList());
					player.sendPacket(new UserInfo(player));
					player.sendPacket(new ExBrExtraUserInfo(player));
					player.broadcastPacket(new SocialAction(player.getObjectId(), SocialAction.VICTORY));
					
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_received_status_noble"));
					player.getInventory().addItem("Noblesse Tiara", 7694, 1, player, player.getTarget());
					// Log.add("QUEST\tПолучение дворянства за " + need_item_id + ":" + need_item_count, "service_quests", player);
					// TODO add logger
					makeSubQuests(player);
				}
				parseCommunityBoardCommand("_bbsservice:noble", player);
				return false;
			}
			else if (bypass[1].equals("nickname"))
			{
				if (!CCabinetConfig.BBS_CABINET_ENABLE_NICK_NAME_CHANGE)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
					CommunityBoardHandler.getInstance().handleParseCommand("_bbscabinet:show", player);
					return false;
				}
				
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "cabinet/service/player/change_name.html");
				html = html.replace("<?correct_name?>", String.valueOf(player.getName()));
				html = html.replace("<?nick_name_count?>", String.valueOf(StringUtil.formatNumber(CCabinetConfig.BBS_CABINET_NICK_NAME_CHANGE_COUNT)));
				html = html.replace("<?nick_name_item?>", String.valueOf(DifferentMethods.getItemName(CCabinetConfig.BBS_CABINET_NICK_NAME_CHANGE_ITEM)));
			}
			else if (bypass[1].startsWith("changenickname"))
			{
				try
				{
					var name = set[1];
					if (name == null)
					{
						return false;
					}
					playerSetNickName(player, name);
				}
				catch (Exception ex)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_correct_name"));
				}
				parseCommunityBoardCommand("_bbsservice:nickname", player);
				return false;
			}
		}
		CommunityBoardHandler.separateAndSend(html, player);
		return true;
	}
	
	public void writeCommunityBoardCommand(L2PcInstance player, String arg1, String arg2, String arg3, String arg4, String arg5)
	{
		
	}
	
	// TODO: Rework for load ColorData
	public void playerSetColor(L2PcInstance player, String color)
	{
		var colorh = new String("FFFFFF");
		if (color.equalsIgnoreCase("Green"))
		{
			colorh = "00FF00";
		}
		else if (color.equalsIgnoreCase("Yellow"))
		{
			colorh = "00FFFF";
		}
		else if (color.equalsIgnoreCase("Orange"))
		{
			colorh = "0099FF";
		}
		else if (color.equalsIgnoreCase("Blue"))
		{
			colorh = "FF0000";
		}
		else if (color.equalsIgnoreCase("Black"))
		{
			colorh = "000000";
		}
		else if (color.equalsIgnoreCase("Brown"))
		{
			colorh = "006699";
		}
		else if (color.equalsIgnoreCase("Light-Pink"))
		{
			colorh = "FF66FF";
		}
		else if (color.equalsIgnoreCase("Pink"))
		{
			colorh = "FF00FF";
		}
		else if (color.equalsIgnoreCase("Light-Blue"))
		{
			colorh = "FFFF66";
		}
		else if (color.equalsIgnoreCase("Turquoise"))
		{
			colorh = "999900";
		}
		else if (color.equalsIgnoreCase("Lime"))
		{
			colorh = "99FF99";
		}
		else if (color.equalsIgnoreCase("Gray"))
		{
			colorh = "999999";
		}
		else if (color.equalsIgnoreCase("Dark-Green"))
		{
			colorh = "339900";
		}
		else if (color.equalsIgnoreCase("Purple"))
		{
			colorh = "FF3399";
		}
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("UPDATE characters SET title_color = ? WHERE charId = ?"))
		{
			if (DifferentMethods.getPay(player, CCabinetConfig.BBS_CABINET_TITLE_COLOR_CHANGE_ITEM, CCabinetConfig.BBS_CABINET_TITLE_COLOR_CHANGE_COUNT))
			{
				ps.setInt(1, Integer.decode("0x" + colorh));
				ps.setInt(2, player.getObjectId());
				ps.execute();
				
				player.getAppearance().setTitleColor(Integer.decode("0x" + colorh));
				player.broadcastUserInfo();
				player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_title_color_changed_to").replace("%i%", color + ""));
			}
		}
		catch (Exception ex)
		{
			LOG.error("Error saving new title color!", ex);
		}
	}
	
	public static void changePk(L2PcInstance player, int count)
	{
		if ((count < 1) || (count > 100))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_reduce_pk_enter_value"));
			return;
		}
		
		if (player.getPkKills() > 0)
		{
			if (DifferentMethods.getPay(player, CCabinetConfig.BBS_CABINET_PK_REDUCE_CHANGE_ITEM, CCabinetConfig.BBS_CABINET_PK_REDUCE_CHANGE_COUNT * count))
			{
				player.setPkKills(player.getPkKills() - count);
				player.broadcastUserInfo();
				player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_reduce_pk_you_have_successfully_cleared").replace("%i%", count + ""));
			}
		}
		else if (player.getPkKills() <= 0)
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_reduce_pk_you_do_not_have_pk"));
		}
	}
	
	private static void playerSetNickName(L2PcInstance player, String name)
	{
		if ((name.isEmpty() || (name.length() < 3)) || (name.length() > 16) || CharacterConfig.FORBIDDEN_NAMES.contains(name.toLowerCase()) || !Util.isValidName(name) || (NpcData.getInstance().getTemplateByName(name) != null))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_correct_name"));
		}
		else if (name.equals(player.getName()))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_choose_different_name"));
		}
		else if (CharNameTable.getInstance().doesCharNameExist(name))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_name_already_exists").replace("%i%", name + ""));
		}
		else if (DifferentMethods.getPay(player, CCabinetConfig.BBS_CABINET_NICK_NAME_CHANGE_ITEM, CCabinetConfig.BBS_CABINET_NICK_NAME_CHANGE_COUNT))
		{
			player.setName(name);
			player.broadcastUserInfo();
			player.storeMe();
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_new_name").replace("%i%", name + ""));
		}
	}
	
	public void makeSubQuests(L2PcInstance player)
	{
		if (player == null)
		{
			return;
		}
		
		var q = QuestManager.getInstance().getQuest(Q00234_FatesWhisper);
		var qs = q.getQuestState(player, true);
		if (qs != null)
		{
			qs.exitQuest(true, false);
		}
		q.newQuestState(player);
		
		if (player.getRace() == ClassRace.KAMAEL)
		{
			q = QuestManager.getInstance().getQuest(Q00236_SeedsOfChaos);
			qs = q.getQuestState(player, true);
			if (qs != null)
			{
				qs.exitQuest(true, false);
			}
			q.newQuestState(player);
		}
		else
		{
			q = QuestManager.getInstance().getQuest(Q00235_MimirsElixir);
			qs = q.getQuestState(player, true);
			if (qs != null)
			{
				qs.exitQuest(true, false);
			}
			q.newQuestState(player);
		}
	}
	
	public static void changePassword(L2PcInstance player, String old, String newpass, String newpass1, String number1, String number2, String captcha)
	{
		if (player == null)
		{
			return;
		}
		
		if (old.equals(newpass))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_password_incorrect_newisold") + "");
			return;
		}
		if ((newpass.length() < 3) || (newpass.length() > 30))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_password_incorrect_size") + "");
			return;
		}
		if (!newpass.equals(newpass1))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_password_incorrect_confirmation") + "");
			return;
		}
		if ((Integer.valueOf(number1).intValue() + Integer.valueOf(number2).intValue()) != Integer.valueOf(captcha).intValue())
		{
			int captchaA = Integer.valueOf(number1).intValue() + Integer.valueOf(number2).intValue();
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_cabinet_password_incorrect_captcha").replace("%s%", captchaA + "") + "");
			return;
		}
		LoginServerThread.getInstance().sendChangePassword(player.getAccountName(), player.getName(), old, newpass);
	}
	
	@Override
	public String[] getCommunityBoardCommands()
	{
		return COMMANDS;
	}
}
