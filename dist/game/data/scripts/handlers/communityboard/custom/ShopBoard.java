/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.communityboard.custom;

import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.bbs.CBasicConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.MultisellData;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.util.DifferentMethods;

/**
 * Shop board.
 */
public class ShopBoard implements IParseBoardHandler
{
	private static final String[] COMMANDS =
	{
		"_bbsshop"
	};
	
	@Override
	public boolean parseCommunityBoardCommand(String command, L2PcInstance player)
	{
		player.setSessionVar("add_fav", null);
		
		if (!CBasicConfig.ENABLE_BBS)
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
			CommunityBoardHandler.getInstance().handleParseCommand("_bbshome", player);
			return false;
		}
		
		if (!DifferentMethods.checkFirstConditions(player))
		{
			return false;
		}
		
		final String html;
		var customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
		
		if (command.startsWith("_bbsshop"))
		{
			var link = command.split(":");
			if (link[1].equals("open"))
			{
				var data = command.split(";");
				var listId = Integer.parseInt(link[2].split(";")[0]);
				if (data.length > 1)
				{
					parseCommunityBoardCommand(data[1], player);
				}
				MultisellData.getInstance().separateAndSend(listId, player, null, false);
				return false;
			}
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "shop/" + link[1] + ".html");
		}
		else
		{
			html = "";
		}
		CommunityBoardHandler.separateAndSend(html, player);
		return true;
	}
	
	public void writeCommunityBoardCommand(L2PcInstance player, String arg1, String arg2, String arg3, String arg4, String arg5)
	{
		
	}
	
	@Override
	public String[] getCommunityBoardCommands()
	{
		return COMMANDS;
	}
}
