/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.communityboard.custom;

import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.RatesConfig;
import com.l2jserver.gameserver.configuration.config.bbs.CBasicConfig;
import com.l2jserver.gameserver.configuration.config.bbs.CStatsConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.bbs.Rating;
import com.l2jserver.gameserver.util.DifferentMethods;

/**
 * Statistics board.
 */
public class StatisticsBoard implements IParseBoardHandler
{
	public long update = System.currentTimeMillis() / 1000;
	
	private static final String[] COMMANDS =
	{
		"_bbsrating"
	};
	
	@Override
	public boolean parseCommunityBoardCommand(String command, L2PcInstance player)
	{
		if (!CStatsConfig.BBS_STATISTIC_ALLOW)
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
			CommunityBoardHandler.getInstance().handleParseCommand("_bbshome", player);
			return false;
		}
		
		String html = null;
		var customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
		
		if (command.equals("_bbsrating"))
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "statistic/index_stistic.html");
		}
		else if (command.equals("_bbsrating;info"))
		{
			show(player, 1);
		}
		else if (command.equals("_bbsrating;pk"))
		{
			show(player, 2);
		}
		else if (command.equals("_bbsrating;pvp"))
		{
			show(player, 3);
		}
		else if (command.equals("_bbsrating;online"))
		{
			show(player, 4);
		}
		else if (command.equals("_bbsrating;olympiad"))
		{
			show(player, 5);
		}
		else if (command.equals("_bbsrating;epicboss"))
		{
			show(player, 6);
		}
		else if (command.equals("_bbsrating;castle"))
		{
			show(player, 7);
		}
		else if (command.equals("_bbsrating;rich"))
		{
			show(player, 8);
		}
		else if (command.equals("_bbsrating;pcpoint"))
		{
			show(player, 9);
		}
		else if (command.equals("_bbsrating;clan"))
		{
			show(player, 10);
		}
		
		if ((update + (CStatsConfig.BBS_STATISTIC_UPDATE_TIME * 60)) < (System.currentTimeMillis() / 1000))
		{
			DAOFactory.getInstance().getCommunityStatisticDAO().selectClassesCount();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectHeroCount();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectAccount();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectNobleCount();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectClanCount();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectAllyCount();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectTopPK();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectTopPVP();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectTopOnline();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectTopPcPoint();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectGrandBoss();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectCastleStatus();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectTopClan();
			
			// TODO
			DAOFactory.getInstance().getCommunityStatisticDAO().selectHeroeStatus();
			// DAOFactory.getInstance().getCommunityStatisticDAO().selectTopRich();
			
			update = System.currentTimeMillis() / 1000;
			LOG.info("Full statistics in the commynity board has been updated.");
		}
		CommunityBoardHandler.separateAndSend(html, player);
		return true;
	}
	
	private void show(L2PcInstance player, int page)
	{
		var number = 0;
		var html = "";
		var NO = MessagesData.getInstance().getMessage(player, "html_no");
		var YES = MessagesData.getInstance().getMessage(player, "html_yes");
		var MALE = MessagesData.getInstance().getMessage(player, "html_male");
		var FEMALE = MessagesData.getInstance().getMessage(player, "html_female");
		
		var customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
		
		if (page == 1)
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "statistic/index.html");
			
			html = html.replace("<?account?>", Integer.toString(Rating.Account));
			html = html.replace("<?players?>", Integer.toString(Rating.Players));
			html = html.replace("<?noblesse?>", Integer.toString(Rating.Noble));
			html = html.replace("<?hero?>", Integer.toString(Rating.Hero));
			html = html.replace("<?clan?>", Integer.toString(Rating.Clan));
			html = html.replace("<?ally?>", Integer.toString(Rating.Ally));
			
			var race = Rating.Human + Rating.Elf + Rating.DarkElf + Rating.Orc + Rating.Dwarf + Rating.Kamael;
			
			if (race == 0)
			{
				html = html.replace("<?Human?>", "...");
				html = html.replace("<?Elf?>", "...");
				html = html.replace("<?Dark_Elf?>", "...");
				html = html.replace("<?Orc?>", "...");
				html = html.replace("<?Dwarf?>", "...");
				html = html.replace("<?Kamael?>", "...");
				
				html = html.replace("<?Human_percent?>", "0%");
				html = html.replace("<?Elf_percent?>", "0%");
				html = html.replace("<?Dark_Elf_percent?>", "0%");
				html = html.replace("<?Orc_percent?>", "0%");
				html = html.replace("<?Dwarf_percent?>", "0%");
				html = html.replace("<?Kamael_percent?>", "0%");
			}
			else
			{
				html = html.replace("<?Human?>", Integer.toString(Rating.Human));
				html = html.replace("<?Elf?>", Integer.toString(Rating.Elf));
				html = html.replace("<?Dark_Elf?>", Integer.toString(Rating.DarkElf));
				html = html.replace("<?Orc?>", Integer.toString(Rating.Orc));
				html = html.replace("<?Dwarf?>", Integer.toString(Rating.Dwarf));
				html = html.replace("<?Kamael?>", Integer.toString(Rating.Kamael));
				
				html = html.replace("<?Human_percent?>", Rating.Human == 0 ? "0%" : DifferentMethods.prune(((double) Rating.Human * 100) / race, 2) + "%");
				html = html.replace("<?Elf_percent?>", Rating.Elf == 0 ? "0%" : DifferentMethods.prune(((double) Rating.Elf * 100) / race, 2) + "%");
				html = html.replace("<?Dark_Elf_percent?>", Rating.DarkElf == 0 ? "0%" : DifferentMethods.prune(((double) Rating.DarkElf * 100) / race, 2) + "%");
				html = html.replace("<?Orc_percent?>", Rating.Orc == 0 ? "0%" : DifferentMethods.prune(((double) Rating.Orc * 100) / race, 2) + "%");
				html = html.replace("<?Dwarf_percent?>", Rating.Dwarf == 0 ? "0%" : DifferentMethods.prune(((double) Rating.Dwarf * 100) / race, 2) + "%");
				html = html.replace("<?Kamael_percent?>", Rating.Kamael == 0 ? "0%" : DifferentMethods.prune(((double) Rating.Kamael * 100) / race, 2) + "%");
			}
			
			html = html.replace("<?xp?>", Float.toString(RatesConfig.RATE_XP));
			html = html.replace("<?sp?>", Float.toString(RatesConfig.RATE_SP));
			// html = html.replace("<?adena?>", Double.toString(RateService.getRateDropAdena(player)));
			html = html.replace("<?drop?>", Float.toString(RatesConfig.PLAYER_RATE_DROP_ITEM));
			html = html.replace("<?spoil?>", Float.toString(RatesConfig.RATE_CORPSE_DROP_AMOUNT_MULTIPLIER));
			html = html.replace("<?manor?>", Float.toString(RatesConfig.RATE_DROP_MANOR));
			// html = html.replace("<?epaulettes?>", Float.toString(ConfigValue.RateDropEpaulette));
			html = html.replace("<?quests?>", Float.toString(RatesConfig.RATE_QUEST_REWARD));
			html = html.replace("<?dropRB?>", Float.toString(RatesConfig.RATE_RAID_DROP_CHANCE_MULTIPLIER));
			// html = html.replace("<?fish?>", Float.toString(ConfigValue.RateFishDropCount));
		}
		else if (page == 2)
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "statistic/pk.html");
			while (number < CStatsConfig.BBS_STATISTIC_COUNT)
			{
				if (Rating.getTopPkName()[number] != null)
				{
					html = html.replace("<?name_" + number + "?>", Rating.getTopPkName()[number]);
					html = html.replace("<?clan_" + number + "?>", (Rating.getTopPkClan()[number] == null) ? "<font color=\"B59A75\">" + NO + "</font>" : Rating.getTopPkClan()[number]);
					html = html.replace("<?sex_" + number + "?>", (Rating.getTopPkSex()[number] == 0) ? MALE : FEMALE);
					html = html.replace("<?class_" + number + "?>", String.valueOf(DifferentMethods.className(player, Rating.getTopPkClass()[number])));
					html = html.replace("<?on_" + number + "?>", (Rating.getTopPkOn()[number] == 1) ? "<font color=\"66FF33\">" + YES + "</font>" : "<font color=\"B59A75\">" + NO + "</font>");
					html = html.replace("<?online_" + number + "?>", DifferentMethods.getPlayerTime(player, Rating.getTopPkOnline()[number]));
					html = html.replace("<?pk_count_" + number + "?>", Integer.toString(Rating.getTopPk()[number]));
					html = html.replace("<?pvp_count_" + number + "?>", Integer.toString(Rating.getTopPkPvP()[number]));
				}
				else
				{
					html = html.replace("<?name_" + number + "?>", "...");
					html = html.replace("<?clan_" + number + "?>", "...");
					html = html.replace("<?sex_" + number + "?>", "...");
					html = html.replace("<?class_" + number + "?>", "...");
					html = html.replace("<?on_" + number + "?>", "...");
					html = html.replace("<?online_" + number + "?>", "...");
					html = html.replace("<?pk_count_" + number + "?>", "...");
					html = html.replace("<?pvp_count_" + number + "?>", "...");
				}
				number++;
			}
		}
		else if (page == 3)
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "statistic/pvp.html");
			while (number < CStatsConfig.BBS_STATISTIC_COUNT)
			{
				if (Rating.getTopPvPName()[number] != null)
				{
					html = html.replace("<?name_" + number + "?>", Rating.getTopPvPName()[number]);
					html = html.replace("<?clan_" + number + "?>", (Rating.getTopPvPClan()[number] == null) ? "<font color=\"B59A75\">" + NO + "</font>" : Rating.getTopPvPClan()[number]);
					html = html.replace("<?sex_" + number + "?>", (Rating.getTopPvPSex()[number] == 0) ? MALE : FEMALE);
					html = html.replace("<?class_" + number + "?>", String.valueOf(DifferentMethods.className(player, Rating.getTopPvPClass()[number])));
					html = html.replace("<?on_" + number + "?>", (Rating.getTopPvPOn()[number] == 1) ? "<font color=\"66FF33\">" + YES + "</font>" : "<font color=\"B59A75\">" + NO + "</font>");
					html = html.replace("<?online_" + number + "?>", String.valueOf(DifferentMethods.getPlayerTime(player, Rating.getTopPvPOnline()[number])));
					html = html.replace("<?pk_count_" + number + "?>", Integer.toString(Rating.getTopPvP()[number]));
					html = html.replace("<?pvp_count_" + number + "?>", Integer.toString(Rating.getTopPvPPvP()[number]));
				}
				else
				{
					html = html.replace("<?name_" + number + "?>", "...");
					html = html.replace("<?clan_" + number + "?>", "...");
					html = html.replace("<?sex_" + number + "?>", "...");
					html = html.replace("<?class_" + number + "?>", "...");
					html = html.replace("<?on_" + number + "?>", "...");
					html = html.replace("<?online_" + number + "?>", "...");
					html = html.replace("<?pk_count_" + number + "?>", "...");
					html = html.replace("<?pvp_count_" + number + "?>", "...");
				}
				number++;
			}
		}
		else if (page == 4)
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "statistic/online.html");
			while (number < CStatsConfig.BBS_STATISTIC_COUNT)
			{
				if (Rating.getTopOnlineName()[number] != null)
				{
					html = html.replace("<?name_" + number + "?>", Rating.getTopOnlineName()[number]);
					html = html.replace("<?clan_" + number + "?>", (Rating.getTopOnlineClan()[number] == null) ? "<font color=\"B59A75\">" + NO + "</font>" : Rating.getTopOnlineClan()[number]);
					html = html.replace("<?sex_" + number + "?>", (Rating.getTopOnlineSex()[number] == 0) ? MALE : FEMALE);
					html = html.replace("<?class_" + number + "?>", String.valueOf(DifferentMethods.className(player, Rating.getTopOnlineClass()[number])));
					html = html.replace("<?on_" + number + "?>", (Rating.getTopOnlineOn()[number] == 1) ? "<font color=\"66FF33\">" + YES + "</font>" : "<font color=\"B59A75\">" + NO + "</font>");
					html = html.replace("<?online_" + number + "?>", String.valueOf(DifferentMethods.getPlayerTime(player, Rating.getTopOnlineOnline()[number])));
					html = html.replace("<?pk_count_" + number + "?>", Integer.toString(Rating.getTopOnline()[number]));
					html = html.replace("<?pvp_count_" + number + "?>", Integer.toString(Rating.getTopOnlinePvP()[number]));
				}
				else
				{
					html = html.replace("<?name_" + number + "?>", "...");
					html = html.replace("<?clan_" + number + "?>", "...");
					html = html.replace("<?sex_" + number + "?>", "...");
					html = html.replace("<?class_" + number + "?>", "...");
					html = html.replace("<?on_" + number + "?>", "...");
					html = html.replace("<?online_" + number + "?>", "...");
					html = html.replace("<?pk_count_" + number + "?>", "...");
					html = html.replace("<?pvp_count_" + number + "?>", "...");
				}
				number++;
			}
		}
		else if (page == 5)
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "statistic/olimpiad.html");
			while (number < CStatsConfig.BBS_STATISTIC_COUNT)
			{
				if (Rating.getTopOlympiadName()[number] != null)
				{
					html = html.replace("<?name_" + number + "?>", Rating.getTopOlympiadName()[number]);
					html = html.replace("<?clan_" + number + "?>", (Rating.getTopOlympiadClan()[number] == null) ? "<font color=\"B59A75\">" + NO + "</font>" : Rating.getTopOlympiadClan()[number]);
					html = html.replace("<?on_" + number + "?>", (Rating.getTopOlympiadOn()[number] == 1) ? "<font color=\"66FF33\">" + YES + "</font>" : "<font color=\"B59A75\">" + NO + "</font>");
					html = html.replace("<?sex_" + number + "?>", (Rating.getTopOlympiadSex()[number] == 0) ? MALE : FEMALE);
					html = html.replace("<?class_" + number + "?>", String.valueOf(DifferentMethods.className(player, Rating.getTopOlympiadClass()[number])));
					html = html.replace("<?olymp_count_" + number + "?>", Integer.toString(Rating.getTopOlympiadDone()[number]));
					html = html.replace("<?olymp_win_" + number + "?>", String.valueOf(Rating.getTopOlympiadWin()[number]));
					
					if (Rating.getTopOlympiadDone()[number] > 0)
					{
						html = html.replace("<?olymp_wins_" + number + "?>", String.valueOf((((double) Rating.getTopOlympiadWin()[number] / (double) Rating.getTopOlympiadDone()[number]) * 100D)));
					}
				}
				else
				{
					html = html.replace("<?name_" + number + "?>", "...");
					html = html.replace("<?clan_" + number + "?>", "...");
					html = html.replace("<?on_" + number + "?>", "...");
					html = html.replace("<?sex_" + number + "?>", "...");
					html = html.replace("<?class_" + number + "?>", "...");
					html = html.replace("<?olymp_count_" + number + "?>", "...");
					html = html.replace("<?olymp_win_" + number + "?>", "...");
					html = html.replace("<?olymp_wins_" + number + "?>", "...");
				}
				number++;
			}
		}
		else if (page == 6)
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "statistic/boss.html");
			while (number < CStatsConfig.BBS_STATISTIC_COUNT)
			{
				if (Rating.getRaidNpcName()[number] != null)
				{
					html = html.replace("<?npc_name_" + number + "?>", Rating.getRaidNpcName()[number]);
					html = html.replace("<?rstatus_" + number
						+ "?>", (Rating.getRaidStatus()[number] == true) ? "<font color=\"99FF00\">" + MessagesData.getInstance().getMessage(player, "html_alive") + "</font>" : "<font color=\"CC0000\">" + MessagesData.getInstance().getMessage(player, "html_dead") + "</font>");
				}
				else
				{
					html = html.replace("<?npc_name_" + number + "?>", "...");
					html = html.replace("<?rstatus_" + number + "?>", "...");
				}
				number++;
			}
		}
		else if (page == 7)
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "statistic/castle.html");
			while (number < CStatsConfig.BBS_STATISTIC_COUNT)
			{
				if (Rating.getCastleName()[number] != null)
				{
					html = html.replace("<?castle_name_" + number + "?>", Rating.getCastleName()[number]);
					String Owner = DAOFactory.getInstance().getCastleDAO().getOwnerCastle(Rating.getCastleId()[number]);
					html = html.replace("<?castle_owner_" + number + "?>", (Owner == null) ? "<font color=\"00CC00\">" + MessagesData.getInstance().getMessage(player, "html_no_owned") + "</font>" : "<font color=\"FFFFFF\">" + Owner + "</font>");
					html = html.replace("<?castle_tax_" + number + "?>", (Rating.getCastlePercent()[number]) + "%");
					html = html.replace("<?castle_siege_date_" + number + "?>", Rating.getCastleSiegeDate()[number]);
				}
				else
				{
					html = html.replace("<?castle_name_" + number + "?>", "...");
					html = html.replace("<?castle_owner_" + number + "?>", "...");
					html = html.replace("<?castle_tax_" + number + "?>", "...");
					html = html.replace("<?castle_siege_date_" + number + "?>", "...");
				}
				number++;
			}
		}
		else if (page == 8)
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "statistic/rich.html");
			while (number < CStatsConfig.BBS_STATISTIC_COUNT)
			{
				if (Rating.getTopRichName()[number] != null)
				{
					html = html.replace("<?name_" + number + "?>", Rating.getTopRichName()[number]);
					html = html.replace("<?clan_" + number + "?>", (Rating.getTopRichClan()[number] == null) ? "<font color=\"B59A75\">" + NO + "</font>" : Rating.getTopRichClan()[number]);
					html = html.replace("<?sex_" + number + "?>", (Rating.getTopRichSex()[number] == 0) ? MALE : FEMALE);
					html = html.replace("<?class_" + number + "?>", String.valueOf(DifferentMethods.className(player, Rating.getTopRichClass()[number])));
					html = html.replace("<?on_" + number + "?>", (Rating.getTopRichOn()[number] == 1) ? "<font color=\"66FF33\">" + YES + "</font>" : "<font color=\"B59A75\">" + NO + "</font>");
					html = html.replace("<?online_" + number + "?>", String.valueOf(DifferentMethods.getPlayerTime(player, Rating.getTopRichOnline()[number])));
					html = html.replace("<?adena_count_" + number + "?>", String.valueOf(DifferentMethods.getAdenas(Rating.getTopRichCount()[number])));
				}
				else
				{
					html = html.replace("<?name_" + number + "?>", "...");
					html = html.replace("<?clan_" + number + "?>", "...");
					html = html.replace("<?sex_" + number + "?>", "...");
					html = html.replace("<?class_" + number + "?>", "...");
					html = html.replace("<?on_" + number + "?>", "...");
					html = html.replace("<?online_" + number + "?>", "...");
					html = html.replace("<?adena_count_" + number + "?>", "...");
				}
				number++;
			}
		}
		else if (page == 9)
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "statistic/pc_point.html");
			while (number < CStatsConfig.BBS_STATISTIC_COUNT)
			{
				if (Rating.getTopPcCafeName()[number] != null)
				{
					html = html.replace("<?name_" + number + "?>", Rating.getTopPcCafeName()[number]);
					html = html.replace("<?clan_" + number + "?>", (Rating.getTopPcCafeClan()[number] == null) ? "<font color=\"B59A75\">" + NO + "</font>" : Rating.getTopPcCafeClan()[number]);
					html = html.replace("<?sex_" + number + "?>", (Rating.getTopPcCafeSex()[number] == 0) ? MALE : FEMALE);
					html = html.replace("<?class_" + number + "?>", String.valueOf(DifferentMethods.className(player, Rating.getTopPcCafeClass()[number])));
					html = html.replace("<?on_" + number + "?>", (Rating.getTopPcCafeOn()[number] == 1) ? "<font color=\"66FF33\">" + YES + "</font>" : "<font color=\"B59A75\">" + NO + "</font>");
					html = html.replace("<?online_" + number + "?>", String.valueOf(DifferentMethods.getPlayerTime(player, Rating.getTopPcCafeOnline()[number])));
					html = html.replace("<?pc_count_" + number + "?>", String.valueOf(Rating.getTopPcCafeCount()[number]));
				}
				else
				{
					html = html.replace("<?name_" + number + "?>", "...");
					html = html.replace("<?clan_" + number + "?>", "...");
					html = html.replace("<?sex_" + number + "?>", "...");
					html = html.replace("<?class_" + number + "?>", "...");
					html = html.replace("<?on_" + number + "?>", "...");
					html = html.replace("<?online_" + number + "?>", "...");
					html = html.replace("<?pc_count_" + number + "?>", "...");
				}
				number++;
			}
		}
		else if (page == 10)
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "statistic/clan.html");
			while (number < CStatsConfig.BBS_STATISTIC_COUNT)
			{
				if (Rating.getClanName()[number] != null)
				{
					html = html.replace("<?clan_name_" + number + "?>", Rating.getClanName()[number]);
					html = html.replace("<?clan_ally_name_" + number + "?>", (Rating.getAllyName()[number] == null) ? "<font color=\"B59A75\">" + NO + "</font>" : Rating.getAllyName()[number]);
					html = html.replace("<?clan_leadername_name_" + number + "?>", Rating.getClanLeader()[number]);
					html = html.replace("<?clan_level_" + number + "?>", Integer.toString(Rating.getClanLvl()[number]));
					html = html.replace("<?clan_reputation_" + number + "?>", Integer.toString(Rating.getClanReputation()[number]));
					html = html.replace("<?clan_castlename_" + number + "?>", (Rating.getClanCastleName()[number] == null) ? "<font color=\"B59A75\">" + NO + "</font>" : Rating.getClanCastleName()[number]);
					html = html.replace("<?clan_allystatus_" + number + "?>", (Rating.getAllyStatus()[number] == null) ? "<font color=\"B59A75\">" + NO + "</font>" : Rating.getAllyStatus()[number]);
				}
				else
				{
					html = html.replace("<?clan_name_" + number + "?>", "...");
					html = html.replace("<?clan_ally_name_" + number + "?>", "...");
					html = html.replace("<?clan_leadername_name_" + number + "?>", "...");
					html = html.replace("<?clan_level_" + number + "?>", "...");
					html = html.replace("<?clan_reputation_" + number + "?>", "...");
					html = html.replace("<?clan_castlename_" + number + "?>", "...");
					html = html.replace("<?clan_allystatus_" + number + "?>", "...");
				}
				number++;
			}
		}
		
		html = html.replace("<?update?>", String.valueOf(CStatsConfig.BBS_STATISTIC_UPDATE_TIME));
		html = html.replace("<?last_update?>", String.valueOf(TimeUtils.time(update)));
		CommunityBoardHandler.separateAndSend(html, player);
	}
	
	public void writeCommunityBoardCommand(L2PcInstance activeChar, String arg1, String arg2, String arg3, String arg4, String arg5)
	{
		
	}
	
	@Override
	public String[] getCommunityBoardCommands()
	{
		return COMMANDS;
	}
}