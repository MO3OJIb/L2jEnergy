/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.communityboard.custom;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.bbs.CBasicConfig;
import com.l2jserver.gameserver.configuration.config.bbs.CTeleportConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.TeleportBBSData;
import com.l2jserver.gameserver.enums.ZoneId;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.handler.IParseBoardHandler;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.bbs.TeleportPoint;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ConfirmDlg;
import com.l2jserver.gameserver.network.serverpackets.ShowBoard;
import com.l2jserver.gameserver.util.DifferentMethods;

/**
 * Teleport board.
 */
public class TeleportBoard implements IParseBoardHandler
{
	private static final String[] COMMANDS =
	{
		"_bbsteleport"
	};
	
	@Override
	public boolean parseCommunityBoardCommand(String command, L2PcInstance player)
	{
		if (!CTeleportConfig.ENABLE_BBS_TELEPORTS)
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
			CommunityBoardHandler.getInstance().handleParseCommand("_bbshome", player);
			return false;
		}
		
		String html = null;
		var customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
		
		if (command.equals("_bbsteleport"))
		{
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "teleport/index.html");
		}
		else if (command.startsWith("_bbsteleport:page"))
		{
			var path = command.split(":");
			if (path.length > 3)
			{
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "teleport/" + path[2] + "/" + path[3] + ".html");
			}
			else
			{
				html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "teleport/" + path[2] + ".html");
			}
		}
		else if (command.equals("_bbsteleport:save_page"))
		{
			showTeleportPoint(player);
			return false;
		}
		else if (command.startsWith("_bbsteleport:delete"))
		{
			DAOFactory.getInstance().getCommunityTeleportDAO().delete(player, Integer.parseInt(command.split(":")[2]));
			showTeleportPoint(player);
			return false;
		}
		else if (command.startsWith("_bbsteleport:save"))
		{
			var point = "";
			var next = command.split(" ");
			if (next.length > 1)
			{
				for (int i = 1; i < next.length; i++)
				{
					point += " " + next[i];
				}
			}
			if (point.length() > 0)
			{
				addTeleportPoint(player, point);
			}
			showTeleportPoint(player);
			return false;
		}
		else if (command.startsWith("_bbsteleport:go"))
		{
			var cord = command.split(":");
			var x = Integer.parseInt(cord[2]);
			var y = Integer.parseInt(cord[3]);
			var z = Integer.parseInt(cord[4]);
			goTeleportPoint(player, new Location(x, y, z));
			player.sendPacket(new ShowBoard());
			html = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "teleport/index.html");
		}
		else if (command.startsWith("_bbsteleport:id"))
		{
			var id = Integer.parseInt(command.split(":")[2]);
			var point = TeleportBBSData.getInstance().getTeleportId(id);
			if (point.isPresent())
			{
				goToTeleportID(player, point.get());
			}
			player.sendPacket(new ShowBoard());
		}
		CommunityBoardHandler.separateAndSend(html, player);
		return true;
	}
	
	private void goToTeleportID(final L2PcInstance player, final TeleportPoint teleportPoint)
	{
		var level = player.getLevel();
		var name = teleportPoint.getName();
		var priceId = teleportPoint.getPriceId();
		var count = teleportPoint.getPriceCount();
		var minLevel = teleportPoint.getMinLevel();
		var maxLevel = teleportPoint.getMaxLevel();
		var pk = teleportPoint.isPk();
		var premium = teleportPoint.isPremium();
		var premiumPriceId = teleportPoint.getPremiumPriceId();
		var premiumCount = teleportPoint.getPremiumPriceCount();
		var location = teleportPoint.getLocation();
		var isConfirm = false;
		
		if ((level < minLevel) || (level > maxLevel))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_teleport_point_level_min_max").replace("%s%", minLevel + "").replace("%c%", maxLevel + ""));
			return;
		}
		
		if ((pk) && (player.getKarma() > 0))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_teleport_point_pk_denied"));
			return;
		}
		
		if ((premium) && (!player.getPcPremiumSystem().isActive()))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_teleport_personal_point_only_premium"));
			return;
		}
		
		if (!DifferentMethods.checkFirstConditions(player))
		{
			return;
		}
		
		var item = player.getPcPremiumSystem().isActive() ? premiumPriceId : priceId;
		var price = player.getPcPremiumSystem().isActive() ? premiumCount : count;
		var freeLevel = player.getLevel() <= CTeleportConfig.BBS_TELEPORT_FREE_LEVEL;
		
		if (isConfirm)
		{
			teleportByAsk(player, teleportPoint, item, price);
		}
		else if (DifferentMethods.getPay(player, item, freeLevel ? 0 : price))
		{
			player.teleToLocation(location);
			player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_teleport_point_success_location").replace("%s%", name + ""));
		}
	}
	
	private void goTeleportPoint(final L2PcInstance player, final Location location)
	{
		if (!DifferentMethods.checkFirstConditions(player))
		{
			return;
		}
		player.teleToLocation(location);
	}
	
	// TODO
	private void teleportByAsk(L2PcInstance player, TeleportPoint tp, int priceId, int priceCount)
	{
		var confirm = new ConfirmDlg(SystemMessageId.S1);
		confirm.addTime(30000);
		var itemName = DifferentMethods.getItemName(priceId);
		confirm.addString("Желаете ли вы телепортироваться за " + priceCount + " " + itemName + "?");
		confirm.addRequesterId(player.getObjectId());
		player.sendPacket(confirm);
	}
	
	private void showTeleportPoint(L2PcInstance player)
	{
		if (CTeleportConfig.BBS_TELEPORTS_POINT_FOR_PREMIUM && !player.getPcPremiumSystem().isActive())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_teleport_personal_point_only_premium"));
			parseCommunityBoardCommand("_bbsteleport", player);
			return;
		}
		
		var customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
		var template = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "teleport/template.html");
		
		var points = "";
		String block;
		
		var locationsList = player.getTeleportPoints().values();
		int counter = 0;
		for (var loc : locationsList)
		{
			if ((counter % 2) == 0)
			{
				counter = 0;
				points += "</td></tr><tr><td>";
			}
			else
			{
				points += "</td><td>";
			}
			block = template;
			block = block.replace("{name}", loc.getName());
			block = block.replace("{id}", String.valueOf(loc.getId()));
			block = block.replace("{x}", String.valueOf(loc.getLocation().getX()));
			block = block.replace("{y}", String.valueOf(loc.getLocation().getY()));
			block = block.replace("{z}", String.valueOf(loc.getLocation().getZ()));
			points += block;
			counter++;
		}
		
		var content = HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "teleport/save.html");
		content = content.replace("{points}", points.equals("") ? "<center><font color=\"FF0000\">" + MessagesData.getInstance().getMessage(player, "communityboard_teleport_dont_have_points") + "</font></center>" : points);
		content = content.replace("{all_price}", StringUtil.formatNumber(CTeleportConfig.BBS_TELEPORT_SAVE_PRICE) + " " + String.valueOf(DifferentMethods.getItemName(CTeleportConfig.BBS_TELEPORT_SAVE_ITEM_ID)));
		content = content.replace("{premium_price}", StringUtil.formatNumber(CTeleportConfig.BBS_TELEPORT_PREMIUM_SAVE_PRICE) + " " + String.valueOf(DifferentMethods.getItemName(CTeleportConfig.BBS_TELEPORT_PREMIUM_SAVE_ITEM_ID)));
		content = content.replace("{point_count}", String.valueOf(CTeleportConfig.BBS_TELEPORT_MAX_COUNT));
		CommunityBoardHandler.separateAndSend(content, player);
		return;
	}
	
	private void addTeleportPoint(L2PcInstance player, final String point)
	{
		if (!DifferentMethods.checkFirstConditions(player))
		{
			return;
		}
		
		if (point.equals("") || point.equals(null))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_teleport_persoanl_point_name"));
			return;
		}
		
		if (CTeleportConfig.BBS_TELEPORTS_POINT_FOR_PREMIUM && !player.getPcPremiumSystem().isActive())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_teleport_personal_point_only_premium"));
			return;
		}
		
		if (player.isMovementDisabled() || player.isOutOfControl())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_teleport_persoanl_point_outofcontrol"));
			return;
		}
		
		if (player.isInCombat())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_teleport_persoanl_point_incombat"));
			return;
		}
		
		if (player.isCursedWeaponEquipped() || player.isJailed() || player.isDead() || player.isAlikeDead() || player.isCastingNow() || player.isAttackingNow() || player.isOlympiadStart())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_teleport_persoanl_point_state"));
			return;
		}
		
		if (player.isInsideZone(ZoneId.CASTLE) || player.isInsideZone(ZoneId.CLAN_HALL) || player.isInsideZone(ZoneId.FORT) || player.isInsideZone(ZoneId.NO_SUMMON_FRIEND) || player.isInsideZone(ZoneId.SIEGE) || player.isInsideZone(ZoneId.WATER) || player.isInsideZone(ZoneId.JAIL))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_teleport_persoanl_point_forbidden_zone"));
			return;
		}
		
		var itemId = player.getPcPremiumSystem().isActive() ? CTeleportConfig.BBS_TELEPORT_PREMIUM_SAVE_ITEM_ID : CTeleportConfig.BBS_TELEPORT_SAVE_ITEM_ID;
		var price = player.getPcPremiumSystem().isActive() ? CTeleportConfig.BBS_TELEPORT_PREMIUM_SAVE_PRICE : CTeleportConfig.BBS_TELEPORT_SAVE_PRICE;
		
		if (DifferentMethods.getPay(player, itemId, price))
		{
			DAOFactory.getInstance().getCommunityTeleportDAO().update(player, point);
		}
		else
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "communityboard_teleport_personal_point").replace("%s%", CTeleportConfig.BBS_TELEPORT_MAX_COUNT + ""));
			player.getInventory().addItem("custom", itemId, price, player, player);
		}
	}
	
	public void writeCommunityBoardCommand(L2PcInstance activeChar, String arg1, String arg2, String arg3, String arg4, String arg5)
	{
		
	}
	
	@Override
	public String[] getCommunityBoardCommands()
	{
		return COMMANDS;
	}
}