/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.effecthandlers.instant;

import com.l2jserver.gameserver.GeoData;
import com.l2jserver.gameserver.enums.FlyType;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.effects.AbstractEffect;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.gameserver.network.serverpackets.FlyToLocation;
import com.l2jserver.gameserver.network.serverpackets.ValidateLocation;

/**
 * Fly Self effect implementation.
 */
public final class FlySelf extends AbstractEffect
{
	private final int _flyRadius;
	
	public FlySelf(Condition attachCond, Condition applyCond, StatsSet set, StatsSet params)
	{
		super(attachCond, applyCond, set, params);
		
		_flyRadius = params.getInt("flyRadius", 0);
	}
	
	@Override
	public boolean isInstant()
	{
		return true;
	}
	
	@Override
	public void onStart(BuffInfo info)
	{
		if (info.getEffected().isMovementDisabled())
		{
			return;
		}
		
		var target = info.getEffected();
		var activeChar = info.getEffector();
		
		// Get current position of the L2Character
		var curX = activeChar.getX();
		var curY = activeChar.getY();
		var curZ = activeChar.getZ();
		
		// Calculate distance (dx,dy) between current position and destination
		var dx = target.getX() - curX;
		var dy = target.getY() - curY;
		var dz = target.getZ() - curZ;
		var distance = Math.hypot(dx, dy);
		if (distance > 2000)
		{
			LOG.warn("Invalid coordinates for characters, getEffector: {}, {} and getEffected: {}, {}!", curX, curY, target.getX(), target.getY());
			return;
		}
		
		var offset = Math.max((int) distance - _flyRadius, 30);
		
		// approximation for moving closer when z coordinates are different
		// TODO: handle Z axis movement better
		offset -= Math.abs(dz);
		if (offset < 5)
		{
			offset = 5;
		}
		
		// If no distance
		if ((distance < 1) || ((distance - offset) <= 0))
		{
			return;
		}
		
		// Calculate movement angles needed
		var sin = dy / distance;
		var cos = dx / distance;
		
		// Calculate the new destination with offset included
		var x = curX + (int) ((distance - offset) * cos);
		var y = curY + (int) ((distance - offset) * sin);
		var z = target.getZ();
		
		var destination = GeoData.getInstance().moveCheck(curX, curY, curZ, x, y, z, activeChar.getInstanceId());
		
		activeChar.broadcastPacket(new FlyToLocation(activeChar, destination, FlyType.CHARGE));
		
		// maybe is need force set X,Y,Z
		activeChar.setXYZ(destination);
		activeChar.broadcastPacket(new ValidateLocation(activeChar));
	}
}
