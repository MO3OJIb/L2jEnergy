/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.effecthandlers.instant;

import java.util.concurrent.TimeUnit;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.ai.CtrlIntention;
import com.l2jserver.gameserver.enums.skills.EffectFlag;
import com.l2jserver.gameserver.enums.skills.L2EffectType;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.actor.L2Attackable;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.effects.AbstractEffect;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.gameserver.model.stats.Formulas;

/**
 * Betray instant effect implementation.
 * @author Zoey76
 */
public final class InstantBetray extends AbstractEffect
{
	private final int _chance;
	private final int _time;
	
	public InstantBetray(Condition attachCond, Condition applyCond, StatsSet set, StatsSet params)
	{
		super(attachCond, applyCond, set, params);
		_chance = params.getInt("chance", 0);
		_time = params.getInt("time", 0);
	}
	
	@Override
	public boolean isInstant()
	{
		return true;
	}
	
	@Override
	public void onStart(BuffInfo info)
	{
		var effected = info.getEffected();
		if (effected == null)
		{
			return;
		}
		
		if (effected.isRaid())
		{
			return;
		}
		
		var target = effected.isServitor() || effected.isSummon() ? effected.getActingPlayer() //
			: effected.isRaidMinion() ? ((L2Attackable) effected).getLeader() : null;
		if (target == null)
		{
			return;
		}
		
		if (!Formulas.calcProbability(_chance, info.getEffector(), effected, info.getSkill()))
		{
			return;
		}
		
		var effectedAI = effected.getAI();
		effectedAI.setIntention(CtrlIntention.AI_INTENTION_ATTACK, target);
		ThreadPoolManager.getInstance().scheduleAiAtFixedRate(() -> effectedAI.setIntention(CtrlIntention.AI_INTENTION_IDLE, target), 0, _time, TimeUnit.SECONDS);
	}
	
	@Override
	public int getEffectFlags()
	{
		return EffectFlag.BETRAYED.getMask();
	}
	
	@Override
	public L2EffectType getEffectType()
	{
		return L2EffectType.DEBUFF;
	}
}
