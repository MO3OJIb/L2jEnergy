/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.effecthandlers.instant;

import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.enums.ShotType;
import com.l2jserver.gameserver.enums.actors.Stats;
import com.l2jserver.gameserver.enums.skills.L2EffectType;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.effects.AbstractEffect;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.gameserver.model.stats.Formulas;

/**
 * Magical Attack Range instant effect implementation.
 * @author Zoey76
 */
public final class MagicalAttackRange extends AbstractEffect
{
	private final double power;
	
	private final double shieldDefensePercent;
	
	public MagicalAttackRange(Condition attachCond, Condition applyCond, StatsSet set, StatsSet params)
	{
		super(attachCond, applyCond, set, params);
		
		power = params.getDouble("power", 0);
		
		shieldDefensePercent = params.getDouble("shieldDefensePercent", 0);
	}
	
	@Override
	public L2EffectType getEffectType()
	{
		return L2EffectType.MAGICAL_ATTACK;
	}
	
	@Override
	public boolean isInstant()
	{
		return true;
	}
	
	@Override
	public void onStart(BuffInfo info)
	{
		var target = info.getEffected();
		var activeChar = info.getEffector();
		var skill = info.getSkill();
		
		if (target.isPlayer() && target.getActingPlayer().isFakeDeath())
		{
			target.stopFakeDeath(true);
		}
		
		var sps = skill.useSpiritShot() && activeChar.isChargedShot(ShotType.SPIRITSHOTS);
		var bss = skill.useSpiritShot() && activeChar.isChargedShot(ShotType.BLESSED_SPIRITSHOTS);
		var mcrit = Formulas.calcMCrit(activeChar.getMCriticalHit(target, skill));
		var shld = Formulas.calcShldUse(activeChar, target, skill);
		var damage = Formulas.calcMagicDam(activeChar, target, skill, shld, shieldDefensePercent, sps, bss, mcrit, power);
		
		if (damage > 0)
		{
			// Manage attack or cast break of the target (calculating rate, sending message...)
			if (!target.isRaid() && Formulas.calcAtkBreak(target, damage))
			{
				target.breakAttack();
				target.breakCast();
			}
			
			// Shield Deflect Magic: Reflect all damage on caster.
			if (target.getStat().calcStat(Stats.VENGEANCE_SKILL_MAGIC_DAMAGE, 0, target, skill) > Rnd.get(100))
			{
				activeChar.reduceCurrentHp(damage, target, skill);
				activeChar.notifyDamageReceived(damage, target, skill, mcrit, false, true);
			}
			else
			{
				target.reduceCurrentHp(damage, activeChar, skill);
				target.notifyDamageReceived(damage, activeChar, skill, mcrit, false, false);
				activeChar.sendDamageMessage(target, (int) damage, mcrit, false, false);
			}
		}
	}
}
