/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.effecthandlers.pump;

import com.l2jserver.gameserver.enums.EffectCalculationType;
import com.l2jserver.gameserver.enums.actors.Stats;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.effects.AbstractEffect;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.gameserver.model.stats.functions.FuncAdd;
import com.l2jserver.gameserver.model.stats.functions.FuncMul;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * Max Mp effect implementation.
 * @author Adry_85
 * @since 2.6.0.0
 */
public final class MaxMp extends AbstractEffect
{
	private final double _power;
	private final EffectCalculationType _type;
	private final boolean _heal;
	
	public MaxMp(Condition attachCond, Condition applyCond, StatsSet set, StatsSet params)
	{
		super(attachCond, applyCond, set, params);
		
		_type = params.getEnum("type", EffectCalculationType.class, EffectCalculationType.DIFF);
		switch (_type)
		{
			case DIFF -> _power = params.getDouble("power", 0);
			default -> _power = 1 + (params.getDouble("power", 0) / 100.0);
		}
		_heal = params.getBoolean("heal", false);
		
		if (params.isEmpty())
		{
			LOG.warn("This effect must have parameters!");
		}
	}
	
	@Override
	public void onStart(BuffInfo info)
	{
		var effected = info.getEffected();
		var charStat = effected.getStat();
		var currentMp = effected.getCurrentMp();
		var amount = _power;
		
		synchronized (charStat)
		{
			switch (_type)
			{
				case DIFF ->
				{
					charStat.getActiveChar().addStatFuncs(new FuncAdd(Stats.MAX_MP, 1, this, _power, null));
					if (_heal)
					{
						effected.setCurrentMp((currentMp + _power));
					}
				}
				case PER ->
				{
					final double maxMp = effected.getMaxMp();
					charStat.getActiveChar().addStatFuncs(new FuncMul(Stats.MAX_MP, 1, this, _power, null));
					if (_heal)
					{
						amount = (_power - 1) * maxMp;
						effected.setCurrentMp(currentMp + amount);
					}
				}
			}
		}
		if (_heal)
		{
			effected.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.S1_MP_HAS_BEEN_RESTORED).addInt((int) amount));
		}
	}
	
	@Override
	public void onExit(BuffInfo info)
	{
		var charStat = info.getEffected().getStat();
		synchronized (charStat)
		{
			charStat.getActiveChar().removeStatsOwner(this);
		}
	}
	
}
