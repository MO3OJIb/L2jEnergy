/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.effecthandlers.ticks;

import com.l2jserver.gameserver.enums.EffectCalculationType;
import com.l2jserver.gameserver.enums.skills.AbnormalType;
import com.l2jserver.gameserver.enums.skills.L2EffectType;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.effects.AbstractEffect;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.gameserver.network.serverpackets.ExRegenMax;

/**
 * Tick Hp effect implementation.
 * @author Adry_85
 */
public final class TickHp extends AbstractEffect
{
	private final double _power;
	private final EffectCalculationType _mode;
	
	public TickHp(Condition attachCond, Condition applyCond, StatsSet set, StatsSet params)
	{
		super(attachCond, applyCond, set, params);
		
		_power = params.getDouble("power", 0);
		setTicks(params.getInt("ticks"));
		_mode = params.getEnum("mode", EffectCalculationType.class, EffectCalculationType.DIFF);
	}
	
	@Override
	public L2EffectType getEffectType()
	{
		return L2EffectType.DMG_OVER_TIME;
	}
	
	@Override
	public boolean onActionTime(BuffInfo info)
	{
		if (info.getEffected().isDead())
		{
			return false;
		}
		
		var target = info.getEffected();
		double power = 0;
		var hp = target.getCurrentHp();
		switch (_mode)
		{
			case DIFF -> power = _power * getTicksMultiplier();
			case PER -> power = hp * _power * getTicksMultiplier();
		}
		
		if (power < 0)
		{
			power = Math.abs(power);
			if (power >= (target.getCurrentHp() - 1))
			{
				power = target.getCurrentHp() - 1;
			}
			
			info.getEffected().reduceCurrentHpByDOT(power, info.getEffector(), info.getSkill());
			info.getEffected().notifyDamageReceived(power, info.getEffector(), info.getSkill(), false, true, false);
		}
		else
		{
			var maxHp = target.getMaxRecoverableHp();
			
			// Not needed to set the HP and send update packet if player is already at max HP
			if (hp > maxHp)
			{
				return true;
			}
			
			target.setCurrentHp(Math.min(hp + power, maxHp));
		}
		return false;
	}
	
	@Override
	public void onStart(BuffInfo info)
	{
		if (info.getEffected().isPlayer() && (getTicks() > 0) && (info.getSkill().getAbnormalType() == AbnormalType.HP_RECOVER))
		{
			info.getEffected().sendPacket(new ExRegenMax(info.getAbnormalTime(), getTicks(), _power));
		}
	}
}
