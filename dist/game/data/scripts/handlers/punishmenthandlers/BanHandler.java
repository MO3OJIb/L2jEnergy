/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.punishmenthandlers;

import com.l2jserver.gameserver.LoginServerThread;
import com.l2jserver.gameserver.enums.PunishmentType;
import com.l2jserver.gameserver.handler.IPunishmentHandler;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.punishment.PunishmentTask;

/**
 * This class handles ban punishment.
 * @author UnAfraid
 */
public class BanHandler implements IPunishmentHandler
{
	@Override
	public void onStart(PunishmentTask task)
	{
		switch (task.getAffect())
		{
			case CHARACTER ->
			{
				var objectId = Integer.parseInt(String.valueOf(task.getKey()));
				var player = L2World.getInstance().getPlayer(objectId);
				if (player != null)
				{
					applyToPlayer(player);
				}
			}
			case ACCOUNT ->
			{
				var account = String.valueOf(task.getKey());
				var client = LoginServerThread.getInstance().getClient(account);
				if (client != null)
				{
					var player = client.getActiveChar();
					if (player != null)
					{
						applyToPlayer(player);
					}
					else
					{
						client.closeNow();
					}
				}
			}
			case IP ->
			{
				var ip = String.valueOf(task.getKey());
				for (var player : L2World.getInstance().getPlayers())
				{
					if (player.getIPAddress().equals(ip))
					{
						applyToPlayer(player);
					}
				}
			}
			case HWID ->
			{
				var hwid = String.valueOf(task.getKey());
				for (var player : L2World.getInstance().getPlayers())
				{
					var client = player.getClient();
					if ((client != null) && client.getHardwareInfo().getMacAddress().equals(hwid))
					{
						applyToPlayer(player);
					}
				}
			}
		}
	}
	
	@Override
	public void onEnd(PunishmentTask task)
	{
		
	}
	
	private static void applyToPlayer(L2PcInstance player)
	{
		player.logout();
	}
	
	@Override
	public PunishmentType getType()
	{
		return PunishmentType.BAN;
	}
}
