/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.punishmenthandlers;

import com.l2jserver.gameserver.LoginServerThread;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.PunishmentType;
import com.l2jserver.gameserver.enums.ZoneId;
import com.l2jserver.gameserver.enums.events.EventType;
import com.l2jserver.gameserver.handler.IPunishmentHandler;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.tasks.player.TeleportTask;
import com.l2jserver.gameserver.model.eventengine.GameEventManager;
import com.l2jserver.gameserver.model.events.Containers;
import com.l2jserver.gameserver.model.events.impl.character.player.OnPlayerLogin;
import com.l2jserver.gameserver.model.events.listeners.ConsumerEventListener;
import com.l2jserver.gameserver.model.olympiad.OlympiadManager;
import com.l2jserver.gameserver.model.punishment.PunishmentTask;
import com.l2jserver.gameserver.model.zone.type.L2JailZone;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * This class handles jail punishment.
 * @author UnAfraid
 */
public class JailHandler implements IPunishmentHandler
{
	public JailHandler()
	{
		// Register global listener
		Containers.Global().addListener(new ConsumerEventListener(Containers.Global(), EventType.ON_PLAYER_LOGIN, (OnPlayerLogin event) -> onPlayerLogin(event), this));
	}
	
	public void onPlayerLogin(OnPlayerLogin event)
	{
		var activeChar = event.getActiveChar();
		if (activeChar.isJailed() && !activeChar.isInsideZone(ZoneId.JAIL))
		{
			applyToPlayer(null, activeChar);
		}
		else if (!activeChar.isJailed() && activeChar.isInsideZone(ZoneId.JAIL) && !activeChar.isGM())
		{
			removeFromPlayer(activeChar);
		}
	}
	
	@Override
	public void onStart(PunishmentTask task)
	{
		switch (task.getAffect())
		{
			case CHARACTER ->
			{
				var objectId = Integer.parseInt(String.valueOf(task.getKey()));
				var player = L2World.getInstance().getPlayer(objectId);
				if (player != null)
				{
					applyToPlayer(task, player);
				}
			}
			case ACCOUNT ->
			{
				var account = String.valueOf(task.getKey());
				var client = LoginServerThread.getInstance().getClient(account);
				if (client != null)
				{
					var player = client.getActiveChar();
					if (player != null)
					{
						applyToPlayer(task, player);
					}
				}
			}
			case IP ->
			{
				var ip = String.valueOf(task.getKey());
				for (var player : L2World.getInstance().getPlayers())
				{
					if (player.getIPAddress().equals(ip))
					{
						applyToPlayer(task, player);
					}
				}
			}
			case HWID ->
			{
				var hwid = String.valueOf(task.getKey());
				for (var player : L2World.getInstance().getPlayers())
				{
					var client = player.getClient();
					if ((client != null) && client.getHardwareInfo().getMacAddress().equals(hwid))
					{
						applyToPlayer(task, player);
					}
				}
			}
		}
	}
	
	@Override
	public void onEnd(PunishmentTask task)
	{
		switch (task.getAffect())
		{
			case CHARACTER ->
			{
				var objectId = Integer.parseInt(String.valueOf(task.getKey()));
				var player = L2World.getInstance().getPlayer(objectId);
				if (player != null)
				{
					removeFromPlayer(player);
				}
			}
			case ACCOUNT ->
			{
				var account = String.valueOf(task.getKey());
				var client = LoginServerThread.getInstance().getClient(account);
				if (client != null)
				{
					var player = client.getActiveChar();
					if (player != null)
					{
						removeFromPlayer(player);
					}
				}
			}
			case IP ->
			{
				var ip = String.valueOf(task.getKey());
				for (var player : L2World.getInstance().getPlayers())
				{
					if (player.getIPAddress().equals(ip))
					{
						removeFromPlayer(player);
					}
				}
			}
			case HWID ->
			{
				var hwid = String.valueOf(task.getKey());
				for (var player : L2World.getInstance().getPlayers())
				{
					var client = player.getClient();
					if ((client != null) && client.getHardwareInfo().getMacAddress().equals(hwid))
					{
						removeFromPlayer(player);
					}
				}
			}
		}
	}
	
	private static void applyToPlayer(PunishmentTask task, L2PcInstance player)
	{
		player.setInstanceId(0);
		player.setIsIn7sDungeon(false);
		
		if (!GameEventManager.getInstance().getEvent().isInactive() && GameEventManager.getInstance().getEvent().isParticipant(player.getObjectId()))
		{
			GameEventManager.getInstance().getEvent().removeParticipant(player);
		}
		
		if (OlympiadManager.getInstance().isRegisteredInComp(player))
		{
			OlympiadManager.getInstance().removeDisconnectedCompetitor(player);
		}
		
		ThreadPoolManager.getInstance().scheduleGeneral(new TeleportTask(player, L2JailZone.getLocationIn()), 2000);
		
		// Open a Html message to inform the player
		var html = new NpcHtmlMessage();
		var content = HtmCache.getInstance().getHtm(player, "data/html/jail_in.htm");
		if (content != null)
		{
			content = content.replaceAll("%reason%", task != null ? task.getReason() : "");
			content = content.replaceAll("%punishedBy%", task != null ? task.getPunishedBy() : "");
			html.setHtml(content);
		}
		else
		{
			html.setHtml("<html><body>You have been put in jail by an admin.</body></html>");
		}
		player.sendPacket(html);
		if (task != null)
		{
			var delay = ((task.getExpirationTime() - System.currentTimeMillis()) / 1000);
			if (delay > 0)
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "player_jail_time").replace("%s%", (delay > 60 ? ((delay / 60) + " m.") : delay + " s.")));
			}
			else
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "player_jail_forever"));
			}
		}
	}
	
	/**
	 * Removes any punishment effects from the player.
	 * @param player
	 */
	private static void removeFromPlayer(L2PcInstance player)
	{
		ThreadPoolManager.getInstance().scheduleGeneral(new TeleportTask(player, L2JailZone.getLocationOut()), 2000);
		
		// Open a Html message to inform the player
		var html = new NpcHtmlMessage();
		var content = HtmCache.getInstance().getHtm(player, "data/html/jail_out.htm");
		if (content != null)
		{
			html.setHtml(content);
		}
		else
		{
			html.setHtml("<html><body>You are free for now, respect server rules!</body></html>");
		}
		player.sendPacket(html);
	}
	
	@Override
	public PunishmentType getType()
	{
		return PunishmentType.JAIL;
	}
}
