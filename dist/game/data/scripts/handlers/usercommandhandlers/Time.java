/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.usercommandhandlers;

import java.text.NumberFormat;
import java.time.ZonedDateTime;
import java.util.Locale;

import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.GameTimeController;
import com.l2jserver.gameserver.configuration.config.custom.CustomConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IUserCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * Time user command.
 */
public class Time implements IUserCommandHandler
{
	private static final int[] COMMAND_IDS =
	{
		77
	};
	
	private static final NumberFormat df = NumberFormat.getInstance(Locale.ENGLISH);
	
	static
	{
		df.setMinimumIntegerDigits(2);
	}
	
	@Override
	public boolean useUserCommand(int id, L2PcInstance activeChar)
	{
		if (COMMAND_IDS[0] != id)
		{
			return false;
		}
		
		var h = GameTimeController.getInstance().getGameHour();
		var m = GameTimeController.getInstance().getGameMinute();
		
		var sm = GameTimeController.getInstance().isNight() ? SystemMessage.getSystemMessage(SystemMessageId.TIME_S1_S2_IN_THE_NIGHT) : SystemMessage.getSystemMessage(SystemMessageId.TIME_S1_S2_IN_THE_DAY);
		sm.addString(df.format(h)).addString(df.format(m));
		activeChar.sendPacket(sm);
		
		if (CustomConfig.DISPLAY_SERVER_TIME)
		{
			var time = ZonedDateTime.now();
			activeChar.sendMessage(MessagesData.getInstance().getMessage(activeChar, "dp_handler_time").replace("%s%", TimeUtils.timeFormat(time) + ""));
		}
		return true;
	}
	
	@Override
	public int[] getUserCommandList()
	{
		return COMMAND_IDS;
	}
}
