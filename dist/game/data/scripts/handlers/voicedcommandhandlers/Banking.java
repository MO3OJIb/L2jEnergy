/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.voicedcommandhandlers;

import com.l2jserver.gameserver.configuration.config.custom.CustomConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IVoicedCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * Banking command.
 * @author Ahmed
 */
public class Banking implements IVoicedCommandHandler
{
	private static final String[] VOICED_COMMANDS =
	{
		"bank",
		"withdraw",
		"deposit"
	};
	
	@Override
	public boolean useVoicedCommand(String command, L2PcInstance player, String params)
	{
		if (command.equals("bank"))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "banking_voiced_usage_deposit_withdraw").replace("%s%", CustomConfig.BANKING_SYSTEM_ADENA + "").replace("%i%", CustomConfig.BANKING_SYSTEM_GOLDBARS + ""));
		}
		else if (command.equals("deposit"))
		{
			if (player.getInventory().getInventoryItemCount(57, 0) >= CustomConfig.BANKING_SYSTEM_ADENA)
			{
				if (!player.reduceAdena("Goldbar", CustomConfig.BANKING_SYSTEM_ADENA, player, false))
				{
					return false;
				}
				player.getInventory().addItem("Goldbar", 3470, CustomConfig.BANKING_SYSTEM_GOLDBARS, player, null);
				player.getInventory().updateDatabase();
				player.sendMessage(MessagesData.getInstance().getMessage(player, "banking_voiced_you_now_have_goldbar_and_adena").replace("%i%", CustomConfig.BANKING_SYSTEM_GOLDBARS + "").replace("%s%", CustomConfig.BANKING_SYSTEM_ADENA + ""));
			}
			else
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "banking_voiced_you_not_have_enough_goldbars_convert").replace("%s%", CustomConfig.BANKING_SYSTEM_ADENA + ""));
			}
		}
		else if (command.equals("withdraw"))
		{
			if (player.getInventory().getInventoryItemCount(3470, 0) >= CustomConfig.BANKING_SYSTEM_GOLDBARS)
			{
				if (!player.destroyItemByItemId("Adena", 3470, CustomConfig.BANKING_SYSTEM_GOLDBARS, player, false))
				{
					return false;
				}
				player.getInventory().addAdena("Adena", CustomConfig.BANKING_SYSTEM_ADENA, player, null);
				player.getInventory().updateDatabase();
				player.sendMessage(MessagesData.getInstance().getMessage(player, "banking_voiced_you_now_have_adena_and_goldbar").replace("%s%", CustomConfig.BANKING_SYSTEM_ADENA + "").replace("%i%", CustomConfig.BANKING_SYSTEM_GOLDBARS + ""));
			}
			else
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "banking_voiced_you_not_have_goldbars_into").replace("%s%", CustomConfig.BANKING_SYSTEM_ADENA + ""));
			}
		}
		return true;
	}
	
	@Override
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}
}