/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.voicedcommandhandlers;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IVoicedCommandHandler;
import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.model.actor.instance.L2DoorInstance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;

/**
 * @author Zoey76
 */
public class CastleVCmd implements IVoicedCommandHandler
{
	private static final String[] VOICED_COMMANDS =
	{
		"opendoors",
		"closedoors",
		"ridewyvern"
	};
	
	@Override
	public boolean useVoicedCommand(String command, L2PcInstance player, String params)
	{
		switch (command)
		{
			case "opendoors":
				if (!params.equals("castle"))
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "castle_no_lord_open_door"));
					return false;
				}
				
				if (!player.isClanLeader())
				{
					player.sendPacket(SystemMessageId.ONLY_CLAN_LEADER_CAN_ISSUE_COMMANDS);
					return false;
				}
				
				var door = (L2DoorInstance) player.getTarget();
				if (door == null)
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
					return false;
				}
				
				var castle = CastleManager.getInstance().getCastleById(player.getClan().getCastleId());
				if (castle == null)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "castle_no_clan_own"));
					return false;
				}
				
				if (castle.getSiege().isInProgress())
				{
					player.sendPacket(SystemMessageId.GATES_NOT_OPENED_CLOSED_DURING_SIEGE);
					return false;
				}
				
				if (castle.checkIfInZone(door.getX(), door.getY(), door.getZ()))
				{
					player.sendPacket(SystemMessageId.THE_GATE_IS_BEING_OPENED);
					door.openMe();
				}
				break;
			case "closedoors":
				if (!params.equals("castle"))
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "castle_no_lord_closed_door"));
					return false;
				}
				if (!player.isClanLeader())
				{
					player.sendPacket(SystemMessageId.ONLY_CLAN_LEADER_CAN_ISSUE_COMMANDS);
					return false;
				}
				var door2 = (L2DoorInstance) player.getTarget();
				if (door2 == null)
				{
					player.sendPacket(SystemMessageId.INVALID_TARGET);
					return false;
				}
				var castle2 = CastleManager.getInstance().getCastleById(player.getClan().getCastleId());
				if (castle2 == null)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "castle_no_clan_own"));
					return false;
				}
				
				if (castle2.getSiege().isInProgress())
				{
					player.sendPacket(SystemMessageId.GATES_NOT_OPENED_CLOSED_DURING_SIEGE);
					return false;
				}
				
				if (castle2.checkIfInZone(door2.getX(), door2.getY(), door2.getZ()))
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "castle_gate_closed"));
					door2.closeMe();
				}
				break;
			case "ridewyvern":
				if (player.isClanLeader() && (player.getClan().getCastleId() > 0))
				{
					player.mount(12621, 0, true);
				}
				break;
		}
		return true;
	}
	
	@Override
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}
}
