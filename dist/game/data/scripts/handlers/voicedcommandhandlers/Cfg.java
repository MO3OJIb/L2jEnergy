/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.voicedcommandhandlers;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IVoicedCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * User command.
 * @author Мо3олЬ
 */
public class Cfg implements IVoicedCommandHandler
{
	private static final String ACTIVED = "<font color=00FF00>ON</font>";
	private static final String DESATIVED = "<font color=FF0000>OFF</font>";
	
	private static final String[] VOICED_COMMANDS =
	{
		"cfg",
		"tradeon",
		"tradeoff",
		"pmon",
		"pmoff",
		"xpon",
		"xpoff",
		"herblooton",
		"herblootoff",
		"itemlooton",
		"itemlootoff",
		"autolooton",
		"autolootoff",
		"buffoff",
		"buffon"
	};
	
	@Override
	public boolean useVoicedCommand(String command, L2PcInstance player, String params)
	{
		if (player == null)
		{
			return false;
		}
		
		if (command.equals("cfg"))
		{
			showHtml(player);
		}
		else if (command.equals("tradeon"))
		{
			player.setTradeRefusal(true);
			player.sendMessage(MessagesData.getInstance().getMessage(player, "admin_trade_on"));
			player.getVariables().set("trade_refusal", true);
			showHtml(player);
		}
		else if (command.equals("tradeoff"))
		{
			player.setTradeRefusal(false);
			player.sendMessage(MessagesData.getInstance().getMessage(player, "admin_trade_off"));
			player.getVariables().remove("trade_refusal");
			showHtml(player);
		}
		else if (command.equals("pmon"))
		{
			player.setMessageRefusal(true);
			player.sendPacket(SystemMessageId.MESSAGE_REFUSAL_MODE);
			player.getVariables().set("refusal", true);
			showHtml(player);
		}
		else if (command.equals("pmoff"))
		{
			player.setMessageRefusal(false);
			player.sendPacket(SystemMessageId.MESSAGE_ACCEPTANCE_MODE);
			player.getVariables().remove("refusal");
			showHtml(player);
		}
		else if (command.equals("xpon"))
		{
			player.setGainXpSpEnable(true);
			player.sendMessage(MessagesData.getInstance().getMessage(player, "xpon_voiced_enabled"));
			player.getVariables().set("GainXpSp", true);
			showHtml(player);
		}
		else if (command.equals("xpoff"))
		{
			player.setGainXpSpEnable(false);
			player.sendMessage(MessagesData.getInstance().getMessage(player, "xpon_voiced_disabled"));
			player.getVariables().remove("GainXpSp");
			showHtml(player);
		}
		else if (command.equals("autolooton"))
		{
			player.setAutoLoot(true);
			player.sendMessage(MessagesData.getInstance().getMessage(player, "auto_loot_voiced_enabled"));
			player.getVariables().set("AutoLoot", true);
			
			if (player.isAutoLootItem())
			{
				player.setAutoLootItem(false);
				player.sendMessage(MessagesData.getInstance().getMessage(player, "auto_loot_voiced_item_disabled"));
				player.getVariables().remove("AutoLootItems");
			}
			showHtml(player);
		}
		else if (command.equals("autolootoff"))
		{
			player.setAutoLoot(false);
			player.sendMessage(MessagesData.getInstance().getMessage(player, "auto_loot_voiced_disabled"));
			player.getVariables().remove("AutoLoot");
			showHtml(player);
		}
		else if (command.equals("itemlooton"))
		{
			player.setAutoLootItem(true);
			player.sendMessage(MessagesData.getInstance().getMessage(player, "auto_loot_voiced_item_enabled"));
			player.getVariables().set("AutoLootItems", true);
			
			if (player.isAutoLoot())
			{
				player.setAutoLoot(false);
				player.sendMessage(MessagesData.getInstance().getMessage(player, "auto_loot_voiced_item_is_now_priority"));
				player.getVariables().remove("AutoLoot");
			}
			showHtml(player);
		}
		else if (command.equals("itemlootoff"))
		{
			player.setAutoLootItem(false);
			player.sendMessage(MessagesData.getInstance().getMessage(player, "auto_loot_voiced_item_disabled"));
			player.getVariables().remove("AutoLootItems");
			showHtml(player);
		}
		else if (command.equals("herblooton"))
		{
			player.setAutoLootHerbs(true);
			player.sendMessage(MessagesData.getInstance().getMessage(player, "auto_loot_voiced_herbs_enabled"));
			player.getVariables().set("AutoLootHerbs", true);
			showHtml(player);
		}
		else if (command.equals("herblootoff"))
		{
			player.setAutoLootHerbs(false);
			player.sendMessage(MessagesData.getInstance().getMessage(player, "auto_loot_voiced_herbs_disabled"));
			player.getVariables().remove("AutoLootHerbs");
			showHtml(player);
		}
		else if (command.equals("buffon"))
		{
			player.setPreventedBuffs(true);
			player.sendMessage(MessagesData.getInstance().getMessage(player, "anti_buff_voiced_enabled"));
			player.getVariables().set("PreventedBuffs", true);
			showHtml(player);
		}
		else if (command.equals("buffoff"))
		{
			player.setPreventedBuffs(false);
			player.sendMessage(MessagesData.getInstance().getMessage(player, "anti_buff_voiced_disabled"));
			player.getVariables().remove("PreventedBuffs");
			showHtml(player);
		}
		showHtml(player);
		return true;
	}
	
	private static void showHtml(L2PcInstance player)
	{
		var html = new NpcHtmlMessage(0);
		html.setFile(player, "data/html/mods/cfg.htm");
		html.replace("%lang%", player.getLang());
		html.replace("%pm%", player.getMessageRefusal() ? ACTIVED : DESATIVED);
		html.replace("%trade%", player.getTradeRefusal() ? ACTIVED : DESATIVED);
		html.replace("%exp%", player.isGainXpSpEnable() ? ACTIVED : DESATIVED);
		html.replace("%autoLoot%", player.isAutoLoot() ? ACTIVED : DESATIVED);
		html.replace("%autoLootI%", player.isAutoLootItem() ? ACTIVED : DESATIVED);
		html.replace("%autoLootH%", player.isAutoLootHerb() ? ACTIVED : DESATIVED);
		html.replace("%blockBuff%", player.isPreventedBuffs() ? ACTIVED : DESATIVED);
		player.sendPacket(html);
	}
	
	@Override
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}
}
