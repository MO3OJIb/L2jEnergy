/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.voicedcommandhandlers;

import java.util.StringTokenizer;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.configuration.config.custom.CustomConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IVoicedCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

public class Lang implements IVoicedCommandHandler
{
	private static final String[] VOICED_COMMANDS =
	{
		"lang"
	};
	
	@Override
	public boolean useVoicedCommand(String command, L2PcInstance player, String params)
	{
		if (!CustomConfig.ENABLE_MULTILANG || !CustomConfig.MULTILANG_VOICED_ALLOW)
		{
			return false;
		}
		
		var html = new NpcHtmlMessage();
		if (params == null)
		{
			var sb = new StringBuilder(200);
			for (var lang : CustomConfig.MULTILANG_ALLOWED)
			{
				StringUtil.append(sb, "<button value=\"", lang.toUpperCase(), "\" action=\"bypass -h voice .lang ", lang, "\" width=60 height=21 back=\"L2UI_ct1.button_df\" fore=\"L2UI_ct1.button_df\"><br>");
			}
			
			html.setFile(player, "data/html/mods/Lang/LanguageSelect.htm");
			html.replace("%list%", sb.toString());
			player.sendPacket(html);
			return true;
		}
		
		var st = new StringTokenizer(params);
		if (st.hasMoreTokens())
		{
			var lang = st.nextToken().trim();
			if (player.setLang(lang))
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "lang_voiced_current_successfully") + " " + lang);
				
				html.setFile(player, "data/html/mods/Lang/Ok.htm");
				player.sendPacket(html);
				return true;
			}
			html.setFile(player, "data/html/mods/Lang/Error.htm");
			player.sendPacket(html);
			return true;
		}
		return false;
		
	}
	
	@Override
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}
}