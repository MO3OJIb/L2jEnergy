/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.voicedcommandhandlers;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.FloodAction;
import com.l2jserver.gameserver.handler.IVoicedCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.tasks.player.PingTask;
import com.l2jserver.gameserver.network.serverpackets.NetPing;
import com.l2jserver.gameserver.util.FloodProtectors;

/**
 * Ping command.
 */
public class Ping implements IVoicedCommandHandler
{
	private static final String[] VOICED_COMMANDS =
	{
		"ping"
	};
	
	@Override
	public boolean useVoicedCommand(String command, L2PcInstance player, String target)
	{
		if (command.equals("ping"))
		{
			if (FloodProtectors.performAction(player.getClient(), FloodAction.PING))
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "ping_voiced_request_processing"));
				player.sendPacket(new NetPing(player));
				ThreadPoolManager.getInstance().scheduleGeneral(new PingTask(player), 3000L);
				return false;
			}
			player.sendMessage(MessagesData.getInstance().getMessage(player, "ping_voiced_anti_flood_protection"));
		}
		return false;
	}
	
	@Override
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}
}
