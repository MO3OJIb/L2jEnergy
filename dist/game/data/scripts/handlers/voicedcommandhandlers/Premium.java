/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.voicedcommandhandlers;

import java.time.Instant;

import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.configuration.config.RatesConfig;
import com.l2jserver.gameserver.configuration.config.custom.PremiumConfig;
import com.l2jserver.gameserver.handler.IVoicedCommandHandler;
import com.l2jserver.gameserver.instancemanager.PremiumManager;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * Premium command.
 * @author Мо3олЬ
 */
public class Premium implements IVoicedCommandHandler
{
	private static final String[] VOICED_COMMANDS =
	{
		"premium"
	};
	
	@Override
	public boolean useVoicedCommand(String command, L2PcInstance player, String target)
	{
		if (command.startsWith("premium"))
		{
			if (player.getPcPremiumSystem().isActive())
			{
				showHtmlPremium(player);
			}
			
			showHtmlNoPremium(player);
			return false;
		}
		return true;
	}
	
	private static void showHtmlPremium(L2PcInstance player)
	{
		var html = new NpcHtmlMessage();
		html.setFile(player, "data/html/mods/premium/activ_prem.htm");
		html.replace("%xp%", (RatesConfig.RATE_XP * PremiumConfig.PREMIUM_RATE_XP));
		html.replace("%sp%", (RatesConfig.RATE_SP * PremiumConfig.PREMIUM_RATE_SP));
		html.replace("%dr_chance%", (RatesConfig.RATE_DEATH_DROP_CHANCE_MULTIPLIER * PremiumConfig.PREMIUM_RATE_DROP_CHANCE));
		html.replace("%dr_amount%", (RatesConfig.RATE_DEATH_DROP_AMOUNT_MULTIPLIER * PremiumConfig.PREMIUM_RATE_DROP_AMOUNT));
		html.replace("%dr_manor%", (RatesConfig.RATE_DROP_MANOR * PremiumConfig.PREMIUM_RATE_DROP_MANOR));
		html.replace("%sp_chance%", (RatesConfig.RATE_CORPSE_DROP_CHANCE_MULTIPLIER * PremiumConfig.PREMIUM_RATE_SPOIL_CHANCE));
		html.replace("%sp_amount%", (RatesConfig.RATE_CORPSE_DROP_AMOUNT_MULTIPLIER * PremiumConfig.PREMIUM_RATE_SPOIL_AMMOUNT));
		html.replace("%end_date%", TimeUtils.dateTimeFormat(Instant.ofEpochMilli(PremiumManager.getInstance().getPremiumExpiration(player.getAccountName()))));
		player.sendPacket(html);
	}
	
	private static void showHtmlNoPremium(L2PcInstance player)
	{
		var html = new NpcHtmlMessage();
		html.setFile(player, "data/html/mods/premium/no_prem.htm");
		html.replace("%xp%", RatesConfig.RATE_XP);
		html.replace("%sp%", RatesConfig.RATE_SP);
		html.replace("%dr_chance%", RatesConfig.RATE_DEATH_DROP_CHANCE_MULTIPLIER);
		html.replace("%dr_amount%", RatesConfig.RATE_DEATH_DROP_AMOUNT_MULTIPLIER);
		html.replace("%dr_manor%", RatesConfig.RATE_DROP_MANOR);
		html.replace("%sp_chance%", RatesConfig.RATE_CORPSE_DROP_CHANCE_MULTIPLIER);
		html.replace("%sp_amount%", RatesConfig.RATE_CORPSE_DROP_AMOUNT_MULTIPLIER);
		html.replace("%pr_xp%", (RatesConfig.RATE_XP * PremiumConfig.PREMIUM_RATE_XP));
		html.replace("%pr_sp%", (RatesConfig.RATE_SP * PremiumConfig.PREMIUM_RATE_SP));
		html.replace("%pr_dr_chance%", (RatesConfig.RATE_DEATH_DROP_CHANCE_MULTIPLIER * PremiumConfig.PREMIUM_RATE_DROP_CHANCE));
		html.replace("%pr_dr_amount%", (RatesConfig.RATE_DEATH_DROP_AMOUNT_MULTIPLIER * PremiumConfig.PREMIUM_RATE_DROP_AMOUNT));
		html.replace("%pr_dr_manor%", (RatesConfig.RATE_DROP_MANOR * PremiumConfig.PREMIUM_RATE_DROP_MANOR));
		html.replace("%pr_sp_chance%", (RatesConfig.RATE_CORPSE_DROP_CHANCE_MULTIPLIER * PremiumConfig.PREMIUM_RATE_SPOIL_CHANCE));
		html.replace("%pr_sp_amount%", (RatesConfig.RATE_CORPSE_DROP_AMOUNT_MULTIPLIER * PremiumConfig.PREMIUM_RATE_SPOIL_AMMOUNT));
		player.sendPacket(html);
	}
	
	@Override
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}
}