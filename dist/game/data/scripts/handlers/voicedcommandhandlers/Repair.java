/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.voicedcommandhandlers;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.IVoicedCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * Repair Character Command.
 * @author Sacrifice
 * @author Мо3олЬ
 */
public final class Repair implements IVoicedCommandHandler
{
	private static final Map<Integer, Long> REUSES = new ConcurrentHashMap<>();
	private static final int REUSE = 30 * 60 * 1000; // 30 Min
	
	private static final String[] VOICED_COMMANDS =
	{
		"repair",
		"startrepair"
	};
	
	@Override
	public boolean useVoicedCommand(String command, L2PcInstance player, String params)
	{
		if (player == null)
		{
			return false;
		}
		
		String repair = null;
		
		try
		{
			if (params != null)
			{
				if (params.length() > 1)
				{
					var cmdParams = params.split(" ");
					repair = cmdParams[0];
				}
			}
		}
		catch (Exception ex)
		{
			repair = null;
		}
		
		// Send activeChar HTML page
		if (command.startsWith("repair"))
		{
			var html = new NpcHtmlMessage();
			html.setFile(player, "data/html/mods/repair/repair.htm");
			html.replace("%acc_chars%", DAOFactory.getInstance().getPlayerDAO().getCharList(player));
			player.sendPacket(html);
			return true;
		}
		
		// Command for enter repairFunction from html
		if (command.startsWith("startrepair") && (repair != null))
		{
			var timeStamp = REUSES.get(player.getObjectId());
			if ((timeStamp != null) && ((System.currentTimeMillis() - REUSE) < timeStamp.longValue()))
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "repair_voiced_you_cannot_repair_often"));
				return false;
			}
			REUSES.put(player.getObjectId(), System.currentTimeMillis());
			
			var html = new NpcHtmlMessage(0);
			if (DAOFactory.getInstance().getPlayerDAO().checkAccount(player, repair))
			{
				if (checkChar(player, repair))
				{
					html.setFile(player, "data/html/mods/repair/repair-self.htm");
					player.sendPacket(html);
					return false;
				}
				else if (DAOFactory.getInstance().getPlayerDAO().checkJail(player))
				{
					html.setFile(player, "data/html/mods/repair/repair-jail.htm");
					player.sendPacket(html);
					return false;
				}
				else
				{
					DAOFactory.getInstance().getPlayerDAO().repairBadCharacter(repair);
					html.setFile(player, "data/html/mods/repair/repair-done.htm");
					player.sendPacket(html);
					return true;
				}
			}
			html.setFile(player, "data/html/mods/repair/repair-error.htm");
			player.sendPacket(html);
			return false;
		}
		return false;
	}
	
	private boolean checkChar(L2PcInstance player, String repair)
	{
		var result = false;
		if (player.getName().compareTo(repair) == 0)
		{
			result = true;
		}
		return result;
	}
	
	@Override
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}
}
