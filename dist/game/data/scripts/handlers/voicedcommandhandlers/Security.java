/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.voicedcommandhandlers;

import com.l2jserver.gameserver.LoginServerThread;
import com.l2jserver.gameserver.configuration.config.protection.SecuritySettingConfig;
import com.l2jserver.gameserver.handler.IVoicedCommandHandler;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

/**
 * @author Мо3олЬ
 */
public class Security implements IVoicedCommandHandler
{
	private final String[] VOICED_COMMANDS =
	{
		"lock",
		"unlock",
		"lockIp",
		"lockHwid",
		"unlockIp",
		"unlockHwid"
	};
	
	@Override
	public boolean useVoicedCommand(String command, L2PcInstance activeChar, String target)
	{
		if (command.equalsIgnoreCase("lock"))
		{
			var html = new NpcHtmlMessage(activeChar.getObjectId());
			html.setFile(activeChar, "data/html/mods/lock/lock.htm");
			html.replace("%ip_block%", IpBlockStatus());
			html.replace("%hwid_block%", HwidBlockStatus());
			html.replace("%hwid_val%", HwidBlockBy());
			html.replace("%curIP%", activeChar.getIPAddress());
			activeChar.sendPacket(html);
			return true;
		}
		else if (command.equalsIgnoreCase("lockIp"))
		{
			if (!SecuritySettingConfig.ALLOW_LOCK_IP)
			{
				return true;
			}
			LoginServerThread.getInstance().sendAllowedIP(activeChar.getAccountName(), activeChar.getIPAddress());
			var html = new NpcHtmlMessage(activeChar.getObjectId());
			html.setFile(activeChar, "data/html/mods/lock/lock_ip.htm");
			html.replace("%curIP%", activeChar.getIPAddress());
			activeChar.sendPacket(html);
			return true;
		}
		else if (command.equalsIgnoreCase("lockHwid"))
		{
			if (!SecuritySettingConfig.ALLOW_LOCK_HWID)
			{
				return true;
			}
			LoginServerThread.getInstance().sendAllowedHwid(activeChar.getAccountName(), activeChar.getHWID());
			var html = new NpcHtmlMessage(activeChar.getObjectId());
			html.setFile(activeChar, "data/html/mods/lock/lock_hwid.htm");
			activeChar.sendPacket(html);
			return true;
		}
		else if (command.equalsIgnoreCase("unlockIp"))
		{
			LoginServerThread.getInstance().sendAllowedIP(activeChar.getAccountName(), "*");
			var html = new NpcHtmlMessage(activeChar.getObjectId());
			html.setFile(activeChar, "data/html/mods/lock/unlock_ip.htm");
			html.replace("%curIP", activeChar.getIPAddress());
			activeChar.sendPacket(html);
			return true;
		}
		else if (command.equalsIgnoreCase("unlockHwid"))
		{
			LoginServerThread.getInstance().sendAllowedHwid(activeChar.getAccountName(), "*");
			var html = new NpcHtmlMessage(activeChar.getObjectId());
			html.setFile(activeChar, "data/html/mods/lock/unlock_hwid.htm");
			activeChar.sendPacket(html);
			return true;
		}
		return true;
	}
	
	private String IpBlockStatus()
	{
		if (SecuritySettingConfig.ALLOW_LOCK_IP)
		{
			return "Разрешено";
		}
		return "Запрещено";
	}
	
	private String HwidBlockStatus()
	{
		if (SecuritySettingConfig.ALLOW_LOCK_HWID)
		{
			return "Разрешено";
		}
		return "Запрещено";
	}
	
	private String HwidBlockBy()
	{
		var result = "(CPU/HDD)";
		switch (SecuritySettingConfig.HWID_LOCK_MASK)
		{
			case 2:
				result = "(HDD)";
				break;
			case 4:
				result = "(BIOS)";
				break;
			case 6:
				result = "(BIOS/HDD)";
				break;
			case 8:
				result = "(CPU)";
				break;
			case 10:
				result = "(CPU/HDD)";
				break;
			case 12:
				result = "(CPU/BIOS)";
				break;
			case 14:
				result = "(CPU/HDD/BIOS)";
				break;
			case 1:
			case 3:
			case 5:
			case 7:
			case 9:
			case 11:
			case 13:
			default:
				result = "(unknown)";
		}
		return result;
	}
	
	@Override
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}
}
