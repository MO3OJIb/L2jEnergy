/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.voicedcommandhandlers;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.GameTimeController;
import com.l2jserver.gameserver.SevenSigns;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.ai.CtrlIntention;
import com.l2jserver.gameserver.configuration.config.events.WeddingConfig;
import com.l2jserver.gameserver.data.xml.impl.SkillData;
import com.l2jserver.gameserver.enums.PlayerAction;
import com.l2jserver.gameserver.enums.ZoneId;
import com.l2jserver.gameserver.enums.skills.AbnormalVisualEffect;
import com.l2jserver.gameserver.handler.IVoicedCommandHandler;
import com.l2jserver.gameserver.instancemanager.CoupleManager;
import com.l2jserver.gameserver.instancemanager.GrandBossManager;
import com.l2jserver.gameserver.instancemanager.QuestManager;
import com.l2jserver.gameserver.instancemanager.SiegeManager;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.eventengine.GameEventManager;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.network.serverpackets.ActionFailed;
import com.l2jserver.gameserver.network.serverpackets.ConfirmDlg;
import com.l2jserver.gameserver.network.serverpackets.MagicSkillUse;
import com.l2jserver.gameserver.network.serverpackets.SetupGauge;
import com.l2jserver.gameserver.util.Broadcast;

/**
 * Wedding voiced commands handler.
 * @author evill33t
 */
public class Wedding implements IVoicedCommandHandler
{
	private static final int Q00505_BloodOffering = 505;
	
	private static final String[] VOICED_COMMANDS =
	{
		"divorce",
		"engage",
		"gotolove"
	};
	
	@Override
	public boolean useVoicedCommand(String command, L2PcInstance player, String params)
	{
		if (player == null)
		{
			return false;
		}
		if (command.startsWith("engage"))
		{
			return engage(player);
		}
		else if (command.startsWith("divorce"))
		{
			return divorce(player);
		}
		else if (command.startsWith("gotolove"))
		{
			return goToLove(player);
		}
		return false;
	}
	
	public boolean divorce(L2PcInstance player)
	{
		if (player.getPartnerId() == 0)
		{
			return false;
		}
		
		var partnerId = player.getPartnerId();
		var coupleId = player.getCoupleId();
		long AdenaAmount = 0;
		
		if (player.isMarried())
		{
			player.sendMessage("You are now divorced.");
			AdenaAmount = (player.getAdena() / 100) * WeddingConfig.WEDDING_DIVORCE_COSTS;
			player.getInventory().reduceAdena("Wedding", AdenaAmount, player, null);
		}
		else
		{
			player.sendMessage("You have broken up as a couple.");
		}
		
		var partner = L2World.getInstance().getPlayer(partnerId);
		if (partner != null)
		{
			partner.setPartnerId(0);
			if (partner.isMarried())
			{
				partner.sendMessage("Your spouse has decided to divorce you.");
			}
			else
			{
				partner.sendMessage("Your fiance has decided to break the engagement with you.");
			}
			
			// give adena
			if (AdenaAmount > 0)
			{
				partner.addAdena("WEDDING", AdenaAmount, null, false);
			}
		}
		CoupleManager.getInstance().deleteCouple(coupleId);
		return true;
	}
	
	public boolean engage(L2PcInstance player)
	{
		if (player.getTarget() == null)
		{
			player.sendMessage("You have no one targeted.");
			return false;
		}
		else if (!(player.getTarget() instanceof L2PcInstance))
		{
			player.sendMessage("You can only ask another player to engage you.");
			return false;
		}
		else if (player.getPartnerId() != 0)
		{
			player.sendMessage("You are already engaged.");
			if (WeddingConfig.WEDDING_PUNISH_INFIDELITY)
			{
				player.startAbnormalVisualEffect(true, AbnormalVisualEffect.BIG_HEAD); // give player a Big Head
				// lets recycle the sevensigns debuffs
				int skillId;
				
				var skillLevel = 1;
				
				if (player.getLevel() > 40)
				{
					skillLevel = 2;
				}
				
				if (player.isMageClass())
				{
					skillId = 4362;
				}
				else
				{
					skillId = 4361;
				}
				
				var skill = SkillData.getInstance().getSkill(skillId, skillLevel);
				if (!player.isAffectedBySkill(skillId))
				{
					skill.applyEffects(player, player);
				}
			}
			return false;
		}
		var ptarget = (L2PcInstance) player.getTarget();
		// check if player target himself
		if (ptarget.getObjectId() == player.getObjectId())
		{
			player.sendMessage("Is there something wrong with you, are you trying to go out with youself?");
			return false;
		}
		
		if (ptarget.isMarried())
		{
			player.sendMessage("Player already married.");
			return false;
		}
		
		if (ptarget.isEngageRequest())
		{
			player.sendMessage("Player already asked by someone else.");
			return false;
		}
		
		if (ptarget.getPartnerId() != 0)
		{
			player.sendMessage("Player already engaged with someone else.");
			return false;
		}
		
		if ((ptarget.getAppearance().getSex() == player.getAppearance().getSex()) && !WeddingConfig.WEDDING_SAMESEX)
		{
			player.sendMessage("Gay marriage is not allowed on this server!");
			return false;
		}
		
		// Check if target has player on friend list
		var foundOnFriendList = false;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var statement = con.prepareStatement("SELECT friendId FROM character_friends WHERE charId=?"))
		{
			statement.setInt(1, ptarget.getObjectId());
			try (var rset = statement.executeQuery())
			{
				while (rset.next())
				{
					if (rset.getInt("friendId") == player.getObjectId())
					{
						foundOnFriendList = true;
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not read friend data!", ex);
		}
		
		if (!foundOnFriendList)
		{
			player.sendMessage("The player you want to ask is not on your friends list, you must first be on each others friends list before you choose to engage.");
			return false;
		}
		
		ptarget.setEngageRequest(true, player.getObjectId());
		ptarget.addAction(PlayerAction.USER_ENGAGE);
		
		var cd = new ConfirmDlg(player.getName() + " is asking to engage you. Do you want to start a new relationship?");
		cd.addTime(15 * 1000);
		ptarget.sendPacket(cd);
		return true;
	}
	
	public boolean goToLove(L2PcInstance player)
	{
		if (!player.isMarried())
		{
			player.sendMessage("You're not married.");
			return false;
		}
		
		if (player.getPartnerId() == 0)
		{
			player.sendMessage("Couldn't find your fiance in the Database - Inform a Gamemaster.");
			LOG.info("Married but couldn't find parter for {}!", player.getName());
			return false;
		}
		
		if (GrandBossManager.getInstance().getZone(player) != null)
		{
			player.sendMessage("You are inside a Boss Zone.");
			return false;
		}
		
		if (player.isCombatFlagEquipped())
		{
			player.sendMessage("While you are holding a Combat Flag or Territory Ward you can't go to your love!");
			return false;
		}
		
		if (player.isCursedWeaponEquipped())
		{
			player.sendMessage("While you are holding a Cursed Weapon you can't go to your love!");
			return false;
		}
		
		if (player.isJailed())
		{
			player.sendMessage("You are in Jail!");
			return false;
		}
		
		if (player.isInOlympiadMode())
		{
			player.sendMessage("You are in the Olympiad now.");
			return false;
		}
		
		if (GameEventManager.getInstance().getEvent().isParticipant(player.getObjectId()))
		{
			player.sendMessage("You are in an event.");
			return false;
		}
		
		if (player.isInDuel())
		{
			player.sendMessage("You are in a duel!");
			return false;
		}
		
		if (player.inObserverMode())
		{
			player.sendMessage("You are in the observation.");
			return false;
		}
		
		if ((SiegeManager.getInstance().getSiege(player) != null) && SiegeManager.getInstance().getSiege(player).isInProgress())
		{
			player.sendMessage("You are in a siege, you cannot go to your partner.");
			return false;
		}
		
		final Quest q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
		if (player.hasQuestState(q505.getName()))
		{
			player.sendMessage("You are in a festival.");
			return false;
		}
		
		if (player.isInParty() && player.getParty().isInDimensionalRift())
		{
			player.sendMessage("You are in the dimensional rift.");
			return false;
		}
		
		// Thanks nbd
		if (!GameEventManager.getInstance().getEvent().onEscapeUse(player.getObjectId()))
		{
			player.sendPacket(ActionFailed.STATIC_PACKET);
			return false;
		}
		
		if (player.isInsideZone(ZoneId.NO_SUMMON_FRIEND))
		{
			player.sendMessage("You are in area which blocks summoning.");
			return false;
		}
		
		var partner = L2World.getInstance().getPlayer(player.getPartnerId());
		if ((partner == null) || !partner.isOnline())
		{
			player.sendMessage("Your partner is not online.");
			return false;
		}
		
		if (player.getInstanceId() != partner.getInstanceId())
		{
			player.sendMessage("Your partner is in another World!");
			return false;
		}
		
		if (partner.isJailed())
		{
			player.sendMessage("Your partner is in Jail.");
			return false;
		}
		
		if (partner.isCursedWeaponEquipped())
		{
			player.sendMessage("Your partner is holding a Cursed Weapon and you can't go to your love!");
			return false;
		}
		
		if (GrandBossManager.getInstance().getZone(partner) != null)
		{
			player.sendMessage("Your partner is inside a Boss Zone.");
			return false;
		}
		
		if (partner.isInOlympiadMode())
		{
			player.sendMessage("Your partner is in the Olympiad now.");
			return false;
		}
		
		if (GameEventManager.getInstance().getEvent().isParticipant(partner.getObjectId()))
		{
			player.sendMessage("Your partner is in an event.");
			return false;
		}
		
		if (partner.isInDuel())
		{
			player.sendMessage("Your partner is in a duel.");
			return false;
		}
		
		if (partner.hasQuestState(q505.getName()))
		{
			player.sendMessage("Your partner is in a festival.");
			return false;
		}
		
		if (partner.isInParty() && partner.getParty().isInDimensionalRift())
		{
			player.sendMessage("Your partner is in dimensional rift.");
			return false;
		}
		
		if (partner.inObserverMode())
		{
			player.sendMessage("Your partner is in the observation.");
			return false;
		}
		
		if ((SiegeManager.getInstance().getSiege(partner) != null) && SiegeManager.getInstance().getSiege(partner).isInProgress())
		{
			player.sendMessage("Your partner is in a siege, you cannot go to your partner.");
			return false;
		}
		
		if (partner.isIn7sDungeon() && !player.isIn7sDungeon())
		{
			var playerCabal = SevenSigns.getInstance().getPlayerCabal(player.getObjectId());
			var isSealValidationPeriod = SevenSigns.getInstance().isSealValidationPeriod();
			var compWinner = SevenSigns.getInstance().getCabalHighestScore();
			
			if (isSealValidationPeriod)
			{
				if (playerCabal != compWinner)
				{
					player.sendMessage("Your Partner is in a Seven Signs Dungeon and you are not in the winner Cabal!");
					return false;
				}
			}
			else
			{
				if (playerCabal == SevenSigns.CABAL_NULL)
				{
					player.sendMessage("Your Partner is in a Seven Signs Dungeon and you are not registered!");
					return false;
				}
			}
		}
		
		if (!GameEventManager.getInstance().getEvent().onEscapeUse(partner.getObjectId()))
		{
			player.sendMessage("Your partner is in an event.");
			return false;
		}
		
		if (partner.isInsideZone(ZoneId.NO_SUMMON_FRIEND))
		{
			player.sendMessage("Your partner is in area which blocks summoning.");
			return false;
		}
		
		var teleportTimer = WeddingConfig.WEDDING_TELEPORT_DURATION * 1000;
		player.sendMessage("After " + (teleportTimer / 60000) + " min. you will be teleported to your partner.");
		player.getInventory().reduceAdena("Wedding", WeddingConfig.WEDDING_TELEPORT_PRICE, player, null);
		
		player.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
		// SoE Animation section
		player.setTarget(player);
		player.disableAllSkills();
		
		var msk = new MagicSkillUse(player, 1050, 1, teleportTimer, 0);
		Broadcast.toSelfAndKnownPlayersInRadius(player, msk, 900);
		var sg = new SetupGauge(0, teleportTimer);
		player.sendPacket(sg);
		// End SoE Animation section
		
		var ef = new EscapeFinalizer(player, partner.getLocation(), partner.isIn7sDungeon());
		// continue execution later
		player.setSkillCast(ThreadPoolManager.getInstance().scheduleGeneral(ef, teleportTimer));
		player.forceIsCasting(GameTimeController.getInstance().getGameTicks() + (teleportTimer / GameTimeController.MILLIS_IN_TICK));
		
		return true;
	}
	
	static class EscapeFinalizer implements Runnable
	{
		private final L2PcInstance _player;
		private final Location _partnerLoc;
		private final boolean _to7sDungeon;
		
		EscapeFinalizer(L2PcInstance player, Location loc, boolean to7sDungeon)
		{
			_player = player;
			_partnerLoc = loc;
			_to7sDungeon = to7sDungeon;
		}
		
		@Override
		public void run()
		{
			if (_player.isDead())
			{
				return;
			}
			
			if ((SiegeManager.getInstance().getSiege(_partnerLoc) != null) && SiegeManager.getInstance().getSiege(_partnerLoc).isInProgress())
			{
				_player.sendMessage("Your partner is in siege, you can't go to your partner.");
				return;
			}
			
			_player.setIsIn7sDungeon(_to7sDungeon);
			_player.enableAllSkills();
			_player.setIsCastingNow(false);
			
			try
			{
				_player.teleToLocation(_partnerLoc);
			}
			catch (Exception ex)
			{
				LOG.warn("", ex);
			}
		}
	}
	
	@Override
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}
}
