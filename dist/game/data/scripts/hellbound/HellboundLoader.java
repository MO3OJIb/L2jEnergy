/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package hellbound;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.configuration.config.custom.CustomConfig;
import com.l2jserver.gameserver.handler.AdminCommandHandler;
import com.l2jserver.gameserver.handler.IAdminCommandHandler;
import com.l2jserver.gameserver.handler.IVoicedCommandHandler;
import com.l2jserver.gameserver.handler.VoicedCommandHandler;

import handlers.admincommandhandlers.AdminHellbound;
import handlers.voicedcommandhandlers.Hellbound;
import hellbound.ai.Amaskari;
import hellbound.ai.Chimeras;
import hellbound.ai.DemonPrince;
import hellbound.ai.HellboundCore;
import hellbound.ai.Keltas;
import hellbound.ai.NaiaLock;
import hellbound.ai.OutpostCaptain;
import hellbound.ai.Ranku;
import hellbound.ai.Slaves;
import hellbound.ai.Typhoon;
import hellbound.ai.npc.Bernarde.Bernarde;
import hellbound.ai.npc.Budenka.Budenka;
import hellbound.ai.npc.Buron.Buron;
import hellbound.ai.npc.Deltuva.Deltuva;
import hellbound.ai.npc.Falk.Falk;
import hellbound.ai.npc.Hude.Hude;
import hellbound.ai.npc.Jude.Jude;
import hellbound.ai.npc.Kanaf.Kanaf;
import hellbound.ai.npc.Kief.Kief;
import hellbound.ai.npc.Natives.Natives;
import hellbound.ai.npc.Quarry.Quarry;
import hellbound.ai.npc.Shadai.Shadai;
import hellbound.ai.npc.Solomon.Solomon;
import hellbound.ai.npc.Warpgate.Warpgate;
import hellbound.ai.zones.AnomicFoundry.AnomicFoundry;
import hellbound.ai.zones.BaseTower.BaseTower;
import hellbound.ai.zones.TowerOfInfinitum.TowerOfInfinitum;
import hellbound.ai.zones.TowerOfNaia.TowerOfNaia;
import hellbound.ai.zones.TullyWorkshop.TullyWorkshop;
import hellbound.instances.DemonPrinceFloor.DemonPrinceFloor;
import hellbound.instances.RankuFloor.RankuFloor;
import hellbound.instances.UrbanArea.UrbanArea;

/**
 * Hellbound loader.
 * @author Zoey76
 */
public final class HellboundLoader
{
	private static final Logger LOG = LoggerFactory.getLogger(HellboundLoader.class);
	
	private static final Class<?>[] SCRIPTS =
	{
		// Commands
		AdminHellbound.class,
		Hellbound.class,
		// AIs
		Amaskari.class,
		Chimeras.class,
		DemonPrince.class,
		HellboundCore.class,
		Keltas.class,
		NaiaLock.class,
		OutpostCaptain.class,
		Ranku.class,
		Slaves.class,
		Typhoon.class,
		// NPCs
		Bernarde.class,
		Budenka.class,
		Buron.class,
		Deltuva.class,
		Falk.class,
		Hude.class,
		Jude.class,
		Kanaf.class,
		Kief.class,
		Natives.class,
		Quarry.class,
		Shadai.class,
		Solomon.class,
		Warpgate.class,
		// Zones
		AnomicFoundry.class,
		BaseTower.class,
		TowerOfInfinitum.class,
		TowerOfNaia.class,
		TullyWorkshop.class,
		// Instances
		DemonPrinceFloor.class,
		UrbanArea.class,
		RankuFloor.class
	};
	
	public static void main(String[] args)
	{
		// Data
		HellboundPointData.getInstance();
		HellboundSpawns.getInstance();
		// Engine
		HellboundEngine.getInstance();
		var n = 0;
		for (var clazz : SCRIPTS)
		{
			try
			{
				var script = clazz.getDeclaredConstructor().newInstance();
				if (script instanceof IAdminCommandHandler)
				{
					AdminCommandHandler.getInstance().registerHandler((IAdminCommandHandler) script);
				}
				else if (CustomConfig.HELLBOUND_STATUS && (script instanceof IVoicedCommandHandler))
				{
					VoicedCommandHandler.getInstance().registerHandler((IVoicedCommandHandler) script);
				}
				n++;
			}
			catch (Exception ex)
			{
				LOG.error("Failed loading {}!", clazz.getSimpleName(), ex);
			}
		}
		LOG.info("Loaded {} Hellbound scripts.", n);
	}
}
