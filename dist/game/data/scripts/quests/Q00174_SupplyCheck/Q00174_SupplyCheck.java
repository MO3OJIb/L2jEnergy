/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package quests.Q00174_SupplyCheck;

import com.l2jserver.gameserver.enums.client.Chronicle;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.interfaces.ChronicleCheck;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.QuestState;
import com.l2jserver.gameserver.model.quest.State;
import com.l2jserver.gameserver.network.NpcStringId;

/**
 * Supply Check (174)
 * @author malyelfik
 */
@ChronicleCheck(Chronicle.HIGH_FIVE)
public class Q00174_SupplyCheck extends Quest
{
	// NPCs
	private static final int NIKA = 32167;
	private static final int BENIS = 32170;
	private static final int MARCELA = 32173;
	private final static int ERINU = 32164;
	private final static int CASCA = 32139;
	// Items
	private static final int WAREHOUSE_MANIFEST = 9792;
	private static final int GROCERY_STORE_MANIFEST = 9793;
	private final static int WEAPON_SHOP_MANIFEST = 9794;
	private final static int SUPPLY_REPORT = 9795;
	
	private static final int[] REWARD =
	{
		23, // Wooden Breastplate
		43, // Wooden Helmet
		49, // Gloves
		2386, // Wooden Gaiters
		37, // Leather Shoes
	};
	
	public Q00174_SupplyCheck()
	{
		super(174, Q00174_SupplyCheck.class.getSimpleName(), "Supply Check");
		addStartNpc(MARCELA);
		addTalkId(MARCELA, BENIS, NIKA, ERINU, CASCA);
		registerQuestItems(WAREHOUSE_MANIFEST, GROCERY_STORE_MANIFEST, WEAPON_SHOP_MANIFEST, SUPPLY_REPORT);
		addLevelCheck(2);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		final QuestState st = getQuestState(player, false);
		String htmltext = null;
		if (st == null)
		{
			return htmltext;
		}
		
		switch (event)
		{
			case "32173-03.htm":
			{
				st.startQuest();
				break;
			}
		}
		return htmltext;
	}
	
	@Override
	public String onTalk(L2Npc npc, L2PcInstance player)
	{
		String htmltext = getNoQuestMsg(player);
		final QuestState st = getQuestState(player, true);
		switch (npc.getId())
		{
			case MARCELA:
				switch (st.getState())
				{
					case State.CREATED:
						switch (isAvailableFor(player))
						{
							case LEVEL:
								st.exitQuest(true);
								htmltext = "32173-02.htm";
								break;
							default:
								htmltext = "32173-01.htm";
								break;
						}
						break;
					case State.STARTED:
						switch (st.getCond())
						{
							case 1:
								htmltext = "32173-04.html";
								break;
							case 2:
								st.setCond(3, true);
								st.takeItems(WAREHOUSE_MANIFEST, -1);
								htmltext = "32173-05.html";
								break;
							case 3:
								htmltext = "32173-06.html";
								break;
							case 4:
								st.setCond(5, true);
								st.takeItems(GROCERY_STORE_MANIFEST, -1);
								htmltext = "32173-07.html";
								break;
							case 5:
								htmltext = "32173-08.html";
								break;
							case 6:
								st.setCond(7, true);
								st.takeItems(SUPPLY_REPORT, -1);
								htmltext = "32173-09.html";
								break;
							case 7:
								htmltext = "32173-10.html";
								break;
							case 8:
								for (int itemId : REWARD)
								{
									st.giveItems(itemId, 1);
								}
								st.giveAdena(2466, true);
								st.addExpAndSp(5672, 446);
								st.exitQuest(false, true);
								// Newbie Guide
								showOnScreenMsg(player, NpcStringId.DELIVERY_DUTY_COMPLETE_N_GO_FIND_THE_NEWBIE_GUIDE, 2, 5000);
								htmltext = "32173-11.html";
								break;
						}
						break;
					case State.COMPLETED:
						htmltext = getAlreadyCompletedMsg(player);
						break;
				}
				break;
			case BENIS:
				if (st.isStarted())
				{
					switch (st.getCond())
					{
						case 1:
							st.setCond(2, true);
							st.giveItems(WAREHOUSE_MANIFEST, 1);
							htmltext = "32170-01.html";
							break;
						case 2:
							htmltext = "32170-02.html";
							break;
						default:
							htmltext = "32170-03.html";
							break;
					}
				}
				break;
			case NIKA:
				if (st.isStarted())
				{
					switch (st.getCond())
					{
						case 1:
						case 2:
							htmltext = "32167-01.html";
							break;
						case 3:
							st.setCond(4, true);
							st.giveItems(GROCERY_STORE_MANIFEST, 1);
							htmltext = "32167-02.html";
							break;
						case 4:
						case 5:
						case 6:
						case 7:
						case 8:
							htmltext = "32167-03.html";
							break;
						default:
							htmltext = "32167-04.html";
							break;
					}
				}
				break;
			case ERINU:
				if (st.isStarted())
				{
					switch (st.getCond())
					{
						case 1:
						case 2:
						case 3:
						case 4:
							htmltext = "32164-01.html";
							break;
						case 5:
							st.setCond(6, true);
							st.giveItems(WEAPON_SHOP_MANIFEST, 1);
							htmltext = "32164-02.html";
							break;
						case 6:
							htmltext = "32164-03.html";
							break;
						default:
							htmltext = "32164-04.html";
							break;
					}
				}
				break;
			case CASCA:
				if (st.isStarted())
				{
					switch (st.getCond())
					{
						case 1:
						case 2:
						case 3:
						case 4:
						case 5:
						case 6:
							htmltext = "32139-01.html";
							break;
						case 7:
							st.setCond(8, true);
							st.giveItems(SUPPLY_REPORT, 1);
							htmltext = "32139-02.html";
							break;
						case 8:
							htmltext = "32139-03.html";
							break;
					}
				}
				break;
		}
		return htmltext;
	}
}