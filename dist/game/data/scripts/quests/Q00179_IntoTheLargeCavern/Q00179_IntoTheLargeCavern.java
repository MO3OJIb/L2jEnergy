/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package quests.Q00179_IntoTheLargeCavern;

import com.l2jserver.gameserver.enums.CategoryType;
import com.l2jserver.gameserver.enums.actors.ClassRace;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.QuestState;
import com.l2jserver.gameserver.model.quest.State;

import quests.Q00178_IconicTrinity.Q00178_IconicTrinity;

/**
 * Into the Large Cavern (179)
 * @author Gnacik
 * @version 2010-10-15 Based on official server Naia
 */
public class Q00179_IntoTheLargeCavern extends Quest
{
	// NPCs
	private static final int KEKROPUS = 32138;
	private static final int MENACING_MACHINE = 32258;
	
	public Q00179_IntoTheLargeCavern()
	{
		super(179, Q00179_IntoTheLargeCavern.class.getSimpleName(), "Into The Large Cavern");
		addStartNpc(KEKROPUS);
		addTalkId(KEKROPUS, MENACING_MACHINE);
		addLevelCheck(17, 21);
		addRaceCheck(ClassRace.KAMAEL);
		addQuestCompletedCheck(Q00178_IconicTrinity.class.getSimpleName());
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		String htmltext = event;
		final QuestState st = getQuestState(player, false);
		if (st == null)
		{
			return htmltext;
		}
		
		if (npc.getId() == KEKROPUS)
		{
			if (event.equalsIgnoreCase("32138-03.html"))
			{
				st.startQuest();
			}
		}
		else if (npc.getId() == MENACING_MACHINE)
		{
			if (event.equalsIgnoreCase("32258-08.html"))
			{
				st.giveItems(391, 1);
				st.giveItems(413, 1);
				st.exitQuest(false, true);
			}
			else if (event.equalsIgnoreCase("32258-09.html"))
			{
				st.giveItems(847, 2);
				st.giveItems(890, 2);
				st.giveItems(910, 1);
				st.exitQuest(false, true);
			}
		}
		return htmltext;
	}
	
	@Override
	public String onTalk(L2Npc npc, L2PcInstance player)
	{
		String htmltext = getNoQuestMsg(player);
		final QuestState st = getQuestState(player, true);
		if (npc.getId() == KEKROPUS)
		{
			switch (st.getState())
			{
				case State.CREATED:
				{
					switch (isAvailableFor(player))
					{
						case LEVEL:
							htmltext = "32138-00c.html";
							st.exitQuest(true);
							break;
						case RACE:
							htmltext = "32138-00b.html";
							st.exitQuest(true);
							break;
						case QUEST:
							htmltext = "32138-00.html";
							st.exitQuest(true);
							break;
						default:
							if (player.isInCategory(CategoryType.FIRST_CLASS_GROUP))
							{
								htmltext = "32138-01.htm";
							}
							break;
					}
					break;
				}
				case State.STARTED:
				{
					if (st.isCond(1))
					{
						htmltext = "32138-03.htm";
					}
					break;
				}
				case State.COMPLETED:
				{
					htmltext = getAlreadyCompletedMsg(player);
					break;
				}
			}
		}
		else if ((npc.getId() == MENACING_MACHINE) && (st.getState() == State.STARTED))
		{
			htmltext = "32258-01.html";
		}
		return htmltext;
	}
}
