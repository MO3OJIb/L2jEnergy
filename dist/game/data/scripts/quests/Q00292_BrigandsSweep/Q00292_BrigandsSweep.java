/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 *
 * This file is part of L2jEnergy Server.
 *
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package quests.Q00292_BrigandsSweep;

import com.l2jserver.gameserver.enums.actors.ClassRace;
import com.l2jserver.gameserver.enums.audio.Sound;
import com.l2jserver.gameserver.enums.client.Chronicle;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.QuestItemChanceHolder;
import com.l2jserver.gameserver.model.interfaces.ChronicleCheck;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.QuestDroplist;
import com.l2jserver.gameserver.model.quest.QuestState;
import com.l2jserver.gameserver.model.quest.State;
import com.l2jserver.gameserver.util.Util;

/**
 * Brigands Sweep (292)
 * @author xban1x
 */
@ChronicleCheck(Chronicle.HIGH_FIVE)
public final class Q00292_BrigandsSweep extends Quest
{
	// NPC's
	private static final int SPIRON = 30532;
	private static final int BALANKI = 30533;
	// Items
	private static final int GOBLIN_NECKLACE = 1483;
	private static final int GOBLIN_PENDANT = 1484;
	private static final int GOBLIN_LORD_PENDANT = 1485;
	private static final int SUSPICIOUS_CONTRACT = 1487;
	private static final QuestItemChanceHolder SUSPICIOUS_MEMO = new QuestItemChanceHolder(1486, 3L);
	// Droplist
	private static final QuestDroplist DROPLIST = QuestDroplist.builder().addSingleDrop(20322, GOBLIN_NECKLACE) // Goblin Brigand
		.addSingleDrop(20323, GOBLIN_PENDANT) // Goblin Brigand Leader
		.addSingleDrop(20324, GOBLIN_NECKLACE) // Goblin Brigand Lieutenant
		.addSingleDrop(20327, GOBLIN_NECKLACE) // Goblin Snooper
		.addSingleDrop(20528, GOBLIN_LORD_PENDANT) // Goblin Lord
		.build();
	
	public Q00292_BrigandsSweep()
	{
		super(292, Q00292_BrigandsSweep.class.getSimpleName(), "Brigands Sweep");
		addStartNpc(SPIRON);
		addTalkId(SPIRON, BALANKI);
		addKillId(DROPLIST.getNpcIds());
		registerQuestItems(GOBLIN_NECKLACE, GOBLIN_PENDANT, GOBLIN_LORD_PENDANT, SUSPICIOUS_MEMO.getId(), SUSPICIOUS_CONTRACT);
		addLevelCheck(5, 18);
		addRaceCheck(ClassRace.DWARF);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		final QuestState qs = getQuestState(player, false);
		String html = null;
		if (qs == null)
		{
			return html;
		}
		
		switch (event)
		{
			case "30532-03.htm":
			{
				if (qs.isCreated())
				{
					qs.startQuest();
					html = event;
				}
				break;
			}
			case "30532-06.html":
			{
				if (qs.isStarted())
				{
					qs.exitQuest(true, true);
					html = event;
				}
				break;
			}
			case "30532-07.html":
			{
				if (qs.isStarted())
				{
					html = event;
				}
				break;
			}
		}
		return html;
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance killer, boolean isSummon)
	{
		final QuestState qs = getQuestState(killer, false);
		if ((qs != null) && qs.isStarted() && Util.checkIfInRange(1500, npc, qs.getPlayer(), true))
		{
			final int chance = getRandom(10);
			if (chance > 5)
			{
				giveItemRandomly(qs.getPlayer(), npc, DROPLIST.get(npc), true);
			}
			else if (qs.isCond(1) && (chance > 4) && !hasQuestItems(killer, SUSPICIOUS_CONTRACT))
			{
				final long memos = getQuestItemsCount(killer, SUSPICIOUS_MEMO.getId());
				if (memos < 3)
				{
					if (giveItemRandomly(qs.getPlayer(), npc, SUSPICIOUS_MEMO, false))
					{
						playSound(killer, Sound.ITEMSOUND_QUEST_ITEMGET);
						giveItems(killer, SUSPICIOUS_CONTRACT, 1);
						takeItems(killer, SUSPICIOUS_MEMO.getId(), -1);
						qs.setCond(2, true);
					}
					else
					{
						playSound(killer, Sound.ITEMSOUND_QUEST_ITEMGET);
					}
				}
			}
		}
		return super.onKill(npc, killer, isSummon);
	}
	
	@Override
	public String onTalk(L2Npc npc, L2PcInstance player)
	{
		String htmltext = getNoQuestMsg(player);
		final QuestState st = getQuestState(player, true);
		switch (npc.getId())
		{
			case SPIRON:
			{
				switch (st.getState())
				{
					case State.CREATED:
					{
						switch (isAvailableFor(player))
						{
							case LEVEL:
								htmltext = "30532-01.htm";
								st.exitQuest(true);
								break;
							case RACE:
								htmltext = "30532-00.htm";
								st.exitQuest(true);
								break;
							default:
								htmltext = "30532-02.htm";
								break;
						}
						break;
					}
					case State.STARTED:
					{
						if (!hasAtLeastOneQuestItem(player, getRegisteredItemIds()))
						{
							htmltext = "30532-04.html";
						}
						else
						{
							final long necklaces = getQuestItemsCount(player, GOBLIN_NECKLACE);
							final long pendants = getQuestItemsCount(player, GOBLIN_PENDANT);
							final long lordPendants = getQuestItemsCount(player, GOBLIN_LORD_PENDANT);
							final long sum = necklaces + pendants + lordPendants;
							if (sum > 0)
							{
								giveAdena(player, (necklaces * 6) + (pendants * 8) + (lordPendants * 10) + (sum >= 10 ? 1000 : 0), true);
								takeItems(player, -1, GOBLIN_NECKLACE, GOBLIN_PENDANT, GOBLIN_LORD_PENDANT);
							}
							if ((sum > 0) && !hasAtLeastOneQuestItem(player, SUSPICIOUS_MEMO.getId(), SUSPICIOUS_CONTRACT))
							{
								htmltext = "30532-05.html";
							}
							else
							{
								final long memos = getQuestItemsCount(player, SUSPICIOUS_MEMO.getId());
								if ((memos == 0) && hasQuestItems(player, SUSPICIOUS_CONTRACT))
								{
									giveAdena(player, 100, true);
									takeItems(player, 1, SUSPICIOUS_CONTRACT); // Retail like, reward is given in 2 pieces if both conditions are meet.
									htmltext = "30532-10.html";
								}
								else
								{
									if (memos == 1)
									{
										htmltext = "30532-08.html";
									}
									else if (memos >= 2)
									{
										htmltext = "30532-09.html";
									}
								}
							}
						}
					}
				}
				break;
			}
			case BALANKI:
			{
				if (st.isStarted())
				{
					if (hasQuestItems(player, SUSPICIOUS_CONTRACT))
					{
						giveAdena(player, 620, true);
						takeItems(player, SUSPICIOUS_CONTRACT, 1);
						htmltext = "30533-02.html";
					}
					else
					{
						htmltext = "30533-01.html";
					}
				}
				break;
			}
		}
		return htmltext;
	}
}
