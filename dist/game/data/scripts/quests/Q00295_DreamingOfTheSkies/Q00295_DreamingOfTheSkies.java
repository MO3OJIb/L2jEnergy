/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 *
 * This file is part of L2jEnergy Server.
 *
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package quests.Q00295_DreamingOfTheSkies;

import static com.l2jserver.gameserver.model.quest.QuestDroplist.singleDropItem;

import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.QuestItemChanceHolder;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.State;
import com.l2jserver.gameserver.util.Util;

/**
 * Dreaming of the Skies (295)
 * @author xban1x
 */
public final class Q00295_DreamingOfTheSkies extends Quest
{
	// NPC
	private static final int ARIN = 30536;
	// Monster
	private static final int MAGICAL_WEAVER = 20153;
	// Item
	private static final QuestItemChanceHolder FLOATING_STONE = new QuestItemChanceHolder(1492, 50L);
	// Reward
	private static final int RING_OF_FIREFLY = 1509;
	
	public Q00295_DreamingOfTheSkies()
	{
		super(295, Q00295_DreamingOfTheSkies.class.getSimpleName(), "Dreaming of the Skies");
		addStartNpc(ARIN);
		addKillId(MAGICAL_WEAVER);
		addTalkId(ARIN);
		registerQuestItems(FLOATING_STONE.getId());
		addLevelCheck(11, 15);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		var qs = getQuestState(player, false);
		if ((qs != null) && qs.isCreated() && event.equals("30536-03.html"))
		{
			qs.startQuest();
			return event;
		}
		return super.onAdvEvent(event, npc, player);
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance killer, boolean isSummon)
	{
		var qs = getQuestState(killer, false);
		if ((qs != null) && qs.isCond(1) && Util.checkIfInRange(1500, npc, killer, true))
		{
			if (giveItemRandomly(qs.getPlayer(), npc, singleDropItem(FLOATING_STONE, getRandom(100) > 25 ? 1L : 2L), FLOATING_STONE.getLimit(), true))
			{
				qs.setCond(2);
			}
		}
		return super.onKill(npc, killer, isSummon);
	}
	
	@Override
	public String onTalk(L2Npc npc, L2PcInstance player)
	{
		var qs = getQuestState(player, true);
		var htmltext = getNoQuestMsg(player);
		switch (qs.getState())
		{
			case State.COMPLETED:
				htmltext = getAlreadyCompletedMsg(player);
				break;
			case State.CREATED:
				switch (isAvailableFor(player))
				{
					case LEVEL:
						htmltext = "30536-01.html";
						qs.exitQuest(true);
						break;
					default:
						htmltext = "30536-02.html";
						break;
				}
				break;
			case State.STARTED:
				if (qs.isCond(2))
				{
					if (hasQuestItems(player, RING_OF_FIREFLY))
					{
						giveAdena(player, 2400, true);
						htmltext = "30536-06.html";
					}
					else
					{
						giveItems(player, RING_OF_FIREFLY, 1);
						htmltext = "30536-05.html";
					}
					addExpAndSp(player, 0, 500);
					takeItems(player, FLOATING_STONE.getId(), -1);
					qs.exitQuest(true, true);
				}
				else
				{
					htmltext = "30536-04.html";
				}
				break;
		}
		return htmltext;
	}
}
