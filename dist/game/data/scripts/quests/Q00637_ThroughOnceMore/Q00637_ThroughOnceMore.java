/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package quests.Q00637_ThroughOnceMore;

import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.QuestItemChanceHolder;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.QuestState;
import com.l2jserver.gameserver.model.quest.State;

/**
 * Through the Gate Once More (637)<br>
 * Original Jython script by BiTi! and DrLecter.
 * @author DS
 */
public final class Q00637_ThroughOnceMore extends Quest
{
	// NPCs
	private static final int FLAURON = 32010;
	// Monsters
	private static final int[] MOBS =
	{
		21565,
		21566,
		21567
	};
	// Items
	private static final int VISITOR_MARK = 8064;
	private static final int FADED_MARK = 8065;
	private static final int MARK = 8067;
	private static final QuestItemChanceHolder NECRO_HEART = new QuestItemChanceHolder(8066, 90.0, 10L);
	
	public Q00637_ThroughOnceMore()
	{
		super(637, Q00637_ThroughOnceMore.class.getSimpleName(), "Through the Gate Once More");
		addStartNpc(FLAURON);
		addTalkId(FLAURON);
		addKillId(MOBS);
		registerQuestItems(NECRO_HEART.getId());
		addLevelCheck(73, 80);
	}
	
	@Override
	public final String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		final QuestState st = getQuestState(player, false);
		if (st == null)
		{
			return null;
		}
		
		if ("32010-03.htm".equals(event))
		{
			st.startQuest();
		}
		else if ("32010-10.htm".equals(event))
		{
			st.exitQuest(true);
		}
		return event;
	}
	
	@Override
	public final String onKill(L2Npc npc, L2PcInstance player, boolean isSummon)
	{
		final QuestState st = getQuestState(player, false);
		if ((st != null) && st.isStarted() && giveItemRandomly(st.getPlayer(), npc, NECRO_HEART, true))
		{
			st.setCond(2);
		}
		return null;
	}
	
	@Override
	public final String onTalk(L2Npc npc, L2PcInstance player)
	{
		String htmltext = getNoQuestMsg(player);
		final QuestState st = getQuestState(player, true);
		
		switch (st.getState())
		{
			case State.COMPLETED:
			{
				htmltext = getAlreadyCompletedMsg(player);
				break;
			}
			case State.CREATED:
			{
				switch (isAvailableFor(player))
				{
					case LEVEL:
					{
						htmltext = "32010-01.htm";
						st.exitQuest(true);
						break;
					}
					default:
					{
						if ((st.hasQuestItems(FADED_MARK)) && (st.hasQuestItems(MARK)))
						{
							htmltext = "32010-02.htm";
						}
						else
						{
							if (st.hasQuestItems(VISITOR_MARK))
							{
								htmltext = "32010-01a.htm";
								st.exitQuest(true);
							}
							htmltext = "32010-0.htm";
							st.exitQuest(true);
						}
						break;
					}
				}
				break;
			}
			case State.STARTED:
			{
				if ((st.isCond(2)) && hasItemsAtLimit(st.getPlayer(), NECRO_HEART))
				{
					st.takeItems(NECRO_HEART.getId(), 10);
					st.takeItems(FADED_MARK, 1);
					st.giveItems(MARK, 1);
					st.giveItems(8273, 10);
					st.exitQuest(true, true);
					htmltext = "32010-05.htm";
				}
				htmltext = "32010-04.htm";
				break;
			}
		}
		return htmltext;
	}
}