/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package quests.Q00649_ALooterAndARailroadMan;

import java.util.HashMap;
import java.util.Map;

import com.l2jserver.gameserver.enums.audio.Sound;
import com.l2jserver.gameserver.enums.client.Chronicle;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.interfaces.ChronicleCheck;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.State;
import com.l2jserver.gameserver.util.Util;

/**
 * A Looter And A Railroad Man (649)
 * @author netvirus
 */
@ChronicleCheck(Chronicle.HIGH_FIVE)
public final class Q00649_ALooterAndARailroadMan extends Quest
{
	// Npc
	private static final int RAILMAN_OBI = 32052;
	// Item
	private static final int THIEF_GUILD_MARK = 8099;
	// Monsters
	private static final Map<Integer, Integer> MONSTERS = new HashMap<>();
	
	static
	{
		MONSTERS.put(22017, 529); // Bandit Sweeper
		MONSTERS.put(22018, 452); // Bandit Hound
		MONSTERS.put(22019, 606); // Bandit Watchman
		MONSTERS.put(22021, 615); // Bandit Undertaker
		MONSTERS.put(22022, 721); // Bandit Assassin
		MONSTERS.put(22023, 827); // Bandit Warrior
		MONSTERS.put(22024, 779); // Bandit Inspector
		MONSTERS.put(22026, 1000); // Bandit Captain
	}
	
	public Q00649_ALooterAndARailroadMan()
	{
		super(649, Q00649_ALooterAndARailroadMan.class.getSimpleName(), "A Looter and a Railroad Man");
		addStartNpc(RAILMAN_OBI);
		addTalkId(RAILMAN_OBI);
		addKillId(MONSTERS.keySet());
		registerQuestItems(THIEF_GUILD_MARK);
		addLevelCheck(30);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		var qs = getQuestState(player, false);
		if (qs == null)
		{
			return null;
		}
		
		String htmltext = null;
		switch (event)
		{
			case "32052-03.htm":
			{
				if (qs.isCreated())
				{
					qs.startQuest();
					htmltext = event;
				}
				break;
			}
			case "32052-06.html":
			{
				if (qs.isCond(2) && qs.hasQuestItems(THIEF_GUILD_MARK))
				{
					qs.giveAdena(21698, true);
					qs.exitQuest(true, true);
					htmltext = event;
				}
				else
				{
					htmltext = "32052-07.htm";
				}
			}
		}
		return htmltext;
	}
	
	@Override
	public String onTalk(L2Npc npc, L2PcInstance player)
	{
		var qs = getQuestState(player, true);
		var htmltext = getNoQuestMsg(player);
		switch (npc.getId())
		{
			case RAILMAN_OBI:
			{
				switch (qs.getState())
				{
					case State.COMPLETED:
					{
						if (npc.getId() == RAILMAN_OBI)
						{
							htmltext = getAlreadyCompletedMsg(player);
						}
						break;
					}
					case State.CREATED:
					{
						if (npc.getId() == RAILMAN_OBI)
						{
							switch (isAvailableFor(player))
							{
								case LEVEL:
									htmltext = "32052-02.htm";
									qs.exitQuest(true);
									break;
								default:
									htmltext = "32052-01.htm";
									break;
							}
						}
						break;
					}
					case State.STARTED:
					{
						if (npc.getId() == RAILMAN_OBI)
						{
							htmltext = (qs.getQuestItemsCount(THIEF_GUILD_MARK) == 200) ? "32052-04.html" : "32052-05.html";
						}
						break;
					}
				}
			}
		}
		return htmltext;
	}
	
	@Override
	public String onKill(L2Npc npc, L2PcInstance killer, boolean isSummon)
	{
		var qs = getQuestState(killer, false);
		if ((qs != null) && qs.isCond(1) && Util.checkIfInRange(1500, npc, killer, false) && (getRandom(1000) < MONSTERS.get(npc.getId())))
		{
			qs.giveItems(THIEF_GUILD_MARK, 1);
			if (qs.getQuestItemsCount(THIEF_GUILD_MARK) == 200)
			{
				qs.setCond(2, true);
			}
			else
			{
				qs.playSound(Sound.ITEMSOUND_QUEST_ITEMGET);
			}
		}
		return super.onKill(npc, killer, isSummon);
	}
}
