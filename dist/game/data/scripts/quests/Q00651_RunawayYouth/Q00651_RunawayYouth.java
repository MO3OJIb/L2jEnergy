/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package quests.Q00651_RunawayYouth;

import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.QuestState;
import com.l2jserver.gameserver.model.quest.State;

/**
 * Runaway Youth (651)
 * @author malyelfik
 */
public class Q00651_RunawayYouth extends Quest
{
	// NPC
	private static final int BATIDAE = 31989;
	private static final int IVAN = 32014;
	// Item
	private static final int SOE = 736;
	
	public Q00651_RunawayYouth()
	{
		super(651, Q00651_RunawayYouth.class.getSimpleName(), "Runaway Youth");
		addStartNpc(IVAN);
		addTalkId(BATIDAE, IVAN);
		addLevelCheck(26);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		final QuestState st = getQuestState(player, false);
		if (st == null)
		{
			return null;
		}
		
		String htmltext = null;
		if (event.equals("32014-03.html"))
		{
			htmltext = event;
		}
		else if (event.equals("32014-04.htm"))
		{
			if (!st.hasQuestItems(SOE))
			{
				return "32014-05.htm";
			}
			st.startQuest();
			st.takeItems(SOE, 1);
			npc.deleteMe();
			htmltext = event;
		}
		return htmltext;
	}
	
	@Override
	public String onTalk(L2Npc npc, L2PcInstance player)
	{
		String htmltext = getNoQuestMsg(player);
		final QuestState st = getQuestState(player, true);
		switch (npc.getId())
		{
			case IVAN:
				switch (st.getState())
				{
					case State.CREATED:
						switch (isAvailableFor(player))
						{
							case LEVEL:
								htmltext = "32014-01a.htm";
								st.exitQuest(true);
								break;
							default:
								htmltext = "32014-01.htm";
								break;
						}
						break;
					case State.STARTED:
						htmltext = "32014-02.html";
						break;
				}
				break;
			case BATIDAE:
				if (st.isStarted())
				{
					st.giveAdena(2883, true);
					st.exitQuest(true, true);
					htmltext = "31989-01.html";
				}
				break;
		}
		return htmltext;
	}
}