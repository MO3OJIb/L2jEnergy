/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package quests.Q00710_PathToBecomingALordGiran;

import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.instancemanager.FortManager;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.Castle;
import com.l2jserver.gameserver.model.entity.Fort;
import com.l2jserver.gameserver.model.holders.QuestItemChanceHolder;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.QuestDroplist;
import com.l2jserver.gameserver.model.quest.QuestState;
import com.l2jserver.gameserver.network.NpcStringId;
import com.l2jserver.gameserver.network.serverpackets.NpcSay;

/**
 * Path to Becoming a Lord - Giran (710)
 * @author Sacrifice
 */
public class Q00710_PathToBecomingALordGiran extends Quest
{
	// NPCs
	private static final int SAUL = 35184;
	private static final int GESTO = 30511;
	private static final int FELTON = 30879;
	private static final int CARGO_BOX = 32243;
	// Items
	private static final int FREIGHT_CHESTS_SEAL = 13014;
	private static final QuestItemChanceHolder GESTOS_BOX = new QuestItemChanceHolder(13013, 300L);
	// Droplist
	private static final QuestDroplist DROPLIST = QuestDroplist.builder().addSingleDrop(20832, GESTOS_BOX) // Zaken's Pikeman
		.addSingleDrop(20833, GESTOS_BOX) // Zaken's Archer
		.addSingleDrop(20835, GESTOS_BOX) // Zaken's Seer
		.addSingleDrop(21602, GESTOS_BOX) // Zaken's Pikeman
		.addSingleDrop(21603, GESTOS_BOX) // Zaken's Pikeman
		.addSingleDrop(21604, GESTOS_BOX) // Zaken's Elite Pikeman
		.addSingleDrop(21605, GESTOS_BOX) // Zaken's Archer
		.addSingleDrop(21606, GESTOS_BOX) // Zaken's Archer
		.addSingleDrop(21607, GESTOS_BOX) // Zaken's Elite Archer
		.addSingleDrop(21608, GESTOS_BOX) // Zaken's Watchman
		.addSingleDrop(21609, GESTOS_BOX) // Zaken's Watchman
		.build();
	// Misc
	private static final int GIRAN_CASTLE = 3;
	
	public Q00710_PathToBecomingALordGiran()
	{
		super(710, Q00710_PathToBecomingALordGiran.class.getSimpleName(), "Path To Becoming A Lord Giran");
		addStartNpc(SAUL);
		addTalkId(SAUL, GESTO, FELTON, CARGO_BOX);
		addKillId(DROPLIST.getNpcIds());
		registerQuestItems(FREIGHT_CHESTS_SEAL, GESTOS_BOX.getId());
		addLevelCheck(0);
	}
	
	@Override
	public String onAdvEvent(String event, L2Npc npc, L2PcInstance player)
	{
		final QuestState qs = player.getQuestState(getName());
		final Castle castle = CastleManager.getInstance().getCastleById(GIRAN_CASTLE);
		if (castle.getOwner() == null)
		{
			return "Castle has no lord";
		}
		
		if (event.equals("35184-03.html"))
		{
			qs.startQuest();
		}
		else if (event.equals("30511-03.html"))
		{
			qs.setCond(3);
		}
		else if (event.equals("30879-02.html"))
		{
			qs.setCond(4);
		}
		else if (event.equals("35184-07.html"))
		{
			if (castle.getOwner().getLeader().getPlayerInstance() != null)
			{
				final NpcSay packet = new NpcSay(npc.getObjectId(), ChatType.NPC_SHOUT, npc.getId(), NpcStringId.S1_HAS_BECOME_THE_LORD_OF_THE_TOWN_OF_GIRAN_MAY_THERE_BE_GLORY_IN_THE_TERRITORY_OF_GIRAN);
				packet.addStringParameter(player.getName());
				npc.broadcastPacket(packet);
				// Territory terr = TerritoryWarManager.getInstance().getTerritory(castle.getOwnerId()); //TODO: fix Support for TerritoryWars
				// terr.setLordId(castle.getOwner().getLeader().getObjectId());
				// terr.updateDataInDB();
				// terr.updateState();
				qs.exitQuest(true, true);
			}
		}
		return event;
	}
	
	@Override
	public final String onKill(L2Npc npc, L2PcInstance killer, boolean isSummon)
	{
		final QuestState qs = killer.getQuestState(getName());
		
		if ((qs != null) && qs.isCond(7))
		{
			if (giveItemRandomly(qs.getPlayer(), npc, DROPLIST.get(npc), true))
			{
				qs.setCond(8);
			}
		}
		return super.onKill(npc, killer, isSummon);
	}
	
	@Override
	public String onTalk(L2Npc npc, L2PcInstance talker)
	{
		final QuestState qs = getQuestState(talker, true);
		String htmltext = getNoQuestMsg(talker);
		final Castle castle = CastleManager.getInstance().getCastleById(GIRAN_CASTLE);
		if (castle.getOwner() == null)
		{
			return "Castle has no lord";
		}
		
		final L2PcInstance castleOwner = castle.getOwner().getLeader().getPlayerInstance();
		
		switch (npc.getId())
		{
			case SAUL:
			{
				if (qs.isCond(0))
				{
					if (castleOwner == qs.getPlayer())
					{
						if (!hasFort())
						{
							htmltext = "35184-01.html";
						}
						else
						{
							htmltext = "35184-00.html";
							qs.exitQuest(true);
						}
					}
					else
					{
						htmltext = "35184-00a.html";
						qs.exitQuest(true);
					}
				}
				else if (qs.isCond(1))
				{
					qs.setCond(2);
					htmltext = "35184-04.html";
				}
				else if (qs.isCond(2))
				{
					htmltext = "35184-05.html";
				}
				else if (qs.isCond(9))
				{
					htmltext = "35184-06.html";
				}
				break;
			}
			case GESTO:
			{
				if (qs.isCond(2))
				{
					htmltext = "30511-01.html";
				}
				else if (qs.isCond(3) || qs.isCond(4))
				{
					htmltext = "30511-04.html";
				}
				else if (qs.isCond(5))
				{
					takeItems(talker, FREIGHT_CHESTS_SEAL, -1);
					qs.setCond(7);
					htmltext = "30511-05.html";
				}
				else if (qs.isCond(7))
				{
					htmltext = "30511-06.html";
				}
				else if (qs.isCond(8))
				{
					takeItems(talker, GESTOS_BOX.getId(), -1);
					qs.setCond(9);
					htmltext = "30511-07.html";
				}
				else if (qs.isCond(9))
				{
					htmltext = "30511-07.html";
				}
				break;
			}
			case FELTON:
			{
				if (qs.isCond(3))
				{
					htmltext = "30879-01.html";
				}
				else if (qs.isCond(4))
				{
					htmltext = "30879-03.html";
				}
				break;
			}
			case CARGO_BOX:
			{
				if (qs.isCond(4))
				{
					qs.setCond(5);
					giveItems(talker, FREIGHT_CHESTS_SEAL, 1);
					htmltext = "32243-01.html";
				}
				else if (qs.isCond(5))
				{
					htmltext = "32243-02.html";
				}
				break;
			}
		}
		return htmltext;
	}
	
	private boolean hasFort()
	{
		for (Fort fortress : FortManager.getInstance().getForts())
		{
			if (fortress.getContractedCastleId() == GIRAN_CASTLE)
			{
				return true;
			}
		}
		return false;
	}
}