CREATE TABLE `account_lecture_mark` (
	`account` varchar(255) NOT NULL,
	`lecture_mark` int(11) NOT NULL,
	PRIMARY KEY (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;