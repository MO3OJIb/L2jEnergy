CREATE TABLE IF NOT EXISTS `character_votes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL DEFAULT 'NONE',
  `nick` varchar(50) CHARACTER SET utf8 NOT NULL,
  `charId` int(10) NOT NULL DEFAULT '0',
  `time` bigint(20) NOT NULL DEFAULT '0',
  `reward` int(10) NOT NULL DEFAULT '0',
  `given` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;