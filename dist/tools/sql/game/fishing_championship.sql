CREATE TABLE `fishing_championship` (
  `PlayerName` VARCHAR(35) NOT NULL,
  `fishLength` double(10,3) NOT NULL,
  `rewarded` int(1) NOT NULL,
  PRIMARY KEY (`PlayerName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;