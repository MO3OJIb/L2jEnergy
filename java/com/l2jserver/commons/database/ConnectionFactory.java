/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.commons.database;

import java.sql.Connection;

import javax.sql.DataSource;

import org.mariadb.jdbc.MariaDbPoolDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Connection Factory implementation.
 * @author Zoey76
 */
public class ConnectionFactory
{
	public static final Logger LOG = LoggerFactory.getLogger(ConnectionFactory.class);
	
	private static MariaDbPoolDataSource dataSource;
	
	protected ConnectionFactory(Builder builder)
	{
		try
		{
			dataSource = new MariaDbPoolDataSource();
			
			dataSource.setUrl(builder._url);
			dataSource.setUser(builder._user);
			dataSource.setPassword(builder._password);
			dataSource.setMaxPoolSize(builder._maxPoolSize);
			dataSource.setStaticGlobal(true);
		}
		catch (Exception ex)
		{
			LOG.error("Couldn't initialize connection pool.", ex);
		}
	}
	
	public static ConnectionFactory getInstance()
	{
		return Builder.INSTANCE;
	}
	
	public DataSource getDataSource()
	{
		return dataSource;
	}
	
	public Connection getConnection()
	{
		Connection con = null;
		while (con == null)
		{
			try
			{
				con = getDataSource().getConnection();
			}
			catch (Exception ex)
			{
				LOG.warn("Unable to get a connection!", ex);
			}
		}
		return con;
	}
	
	public void close()
	{
		try
		{
			dataSource.close();
		}
		catch (Exception ex)
		{
			LOG.warn("There has been a problem closing the data source!", ex);
		}
	}
	
	public static Builder builder()
	{
		return new Builder();
	}
	
	public static final class Builder
	{
		protected static volatile ConnectionFactory INSTANCE;
		
		public String _url;
		public String _user;
		public String _password;
		public int _maxPoolSize;
		
		Builder()
		{
		}
		
		public Builder withUrl(String url)
		{
			_url = url;
			return this;
		}
		
		public Builder withUser(String user)
		{
			_user = user;
			return this;
		}
		
		public Builder withPassword(String password)
		{
			_password = password;
			return this;
		}
		
		public Builder withMaxPoolSize(int maxPoolSize)
		{
			_maxPoolSize = maxPoolSize;
			return this;
		}
		
		public void build()
		{
			if (INSTANCE == null)
			{
				synchronized (this)
				{
					if (INSTANCE == null)
					{
						INSTANCE = new ConnectionFactory(this);
					}
					else
					{
						LOG.warn("Trying to build another Connection Factory!");
					}
				}
			}
		}
	}
}
