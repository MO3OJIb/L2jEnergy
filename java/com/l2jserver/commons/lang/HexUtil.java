/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.commons.lang;

public class HexUtil
{
	public static String printData(byte[] data, int len)
	{
		var result = new StringBuilder();
		var counter = 0;
		
		for (var i = 0; i < len; i++)
		{
			if ((counter % 16) == 0)
			{
				result.append(fillHex(i, 4) + ": ");
			}
			
			result.append(fillHex(data[i] & 0xff, 2) + " ");
			counter++;
			if (counter == 16)
			{
				result.append("   ");
				
				var charpoint = i - 15;
				for (var a = 0; a < 16; a++)
				{
					var t1 = data[charpoint++];
					
					if ((t1 > 0x1f) && (t1 < 0x80))
					{
						result.append((char) t1);
					}
					else
					{
						result.append('.');
					}
				}
				result.append("\n");
				counter = 0;
			}
		}
		
		var rest = data.length % 16;
		if (rest > 0)
		{
			for (var i = 0; i < (17 - rest); i++)
			{
				result.append("   ");
			}
			
			var charpoint = data.length - rest;
			for (var a = 0; a < rest; a++)
			{
				var t1 = data[charpoint++];
				
				if ((t1 > 0x1f) && (t1 < 0x80))
				{
					result.append((char) t1);
				}
				else
				{
					result.append('.');
				}
			}
			result.append("\n");
		}
		return result.toString();
	}
	
	public static String fillHex(int data, int digits)
	{
		var number = Integer.toHexString(data);
		
		for (int i = number.length(); i < digits; i++)
		{
			number = "0" + number;
		}
		return number;
	}
	
	public static String printData(byte[] raw)
	{
		return printData(raw, raw.length);
	}
}
