/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.commons.lang;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class StringUtil
{
	private static final Logger LOG = LoggerFactory.getLogger(StringUtil.class);
	
	public static void append(StringBuilder sb, Object... content)
	{
		for (var obj : content)
		{
			sb.append((obj == null) ? null : obj.toString());
		}
	}
	
	public static boolean isDigit(String text)
	{
		if ((text == null) || text.isEmpty())
		{
			return false;
		}
		for (var chars : text.toCharArray())
		{
			if (!Character.isDigit(chars))
			{
				return false;
			}
		}
		return true;
	}
	
	public static boolean isAlphaNumeric(String text)
	{
		if ((text == null) || text.isEmpty())
		{
			return false;
		}
		for (var chars : text.toCharArray())
		{
			if (!Character.isLetterOrDigit(chars))
			{
				return false;
			}
		}
		return true;
	}
	
	public static String formatNumber(long value)
	{
		return NumberFormat.getInstance(Locale.ENGLISH).format(value);
	}
	
	/**
	 * @param str - the string whose first letter to capitalize
	 * @return a string with the first letter of the {@code str} capitalized
	 */
	public static String capitalizeFirst(String str)
	{
		if ((str == null) || str.isEmpty())
		{
			return str;
		}
		var arr = str.toCharArray();
		var c = arr[0];
		
		if (Character.isLetter(c))
		{
			arr[0] = Character.toUpperCase(c);
		}
		return new String(arr);
	}
	
	public static int getLength(final Iterable<String> strings)
	{
		var length = 0;
		for (var string : strings)
		{
			length += (string == null) ? 4 : string.length();
		}
		return length;
	}
	
	public static String getTraceString(StackTraceElement[] trace)
	{
		var sbString = new StringBuilder();
		for (var element : trace)
		{
			sbString.append(element.toString()).append(System.lineSeparator());
		}
		return sbString.toString();
	}
	
	public static boolean isEmpty(String... strings)
	{
		for (var str : strings)
		{
			if ((str == null) || str.isEmpty())
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Verify if the given text matches with the regex pattern.
	 * @param text : the text to test.
	 * @param regex : the regex pattern to make test with.
	 * @return true if matching.
	 */
	public static boolean isValidString(String text, String regex)
	{
		Pattern pattern;
		try
		{
			pattern = Pattern.compile(regex);
		}
		catch (PatternSyntaxException e) // case of illegal pattern
		{
			pattern = Pattern.compile(".*");
		}
		
		var regexp = pattern.matcher(text);
		return regexp.matches();
	}
	
	public static void printSection(String text)
	{
		var sb = new StringBuilder(60);
		for (var i = 0; i < (54 - text.length()); i++)
		{
			sb.append("-");
		}
		StringUtil.append(sb, "=[ ", text, " ]");
		LOG.info(sb.toString());
	}
}