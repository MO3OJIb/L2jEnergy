/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.commons.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

/**
 * @author Java-man
 */
public class TimeUtils
{
	private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yy");
	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
	
	public static final DateFormat TIME_FORMAT_FIX = new SimpleDateFormat("HH:mm");
	public static final DateFormat DATE_TIME_FORMAT_FIX = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	public static final SimpleDateFormat DATE_TIME_FORMATTER_FIX = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	public static final SimpleDateFormat DATE_FORMAT_FIX = new SimpleDateFormat("yy/MM/dd");
	
	public static String time(long time)
	{
		return TIME_FORMAT_FIX.format(new Date(time * 1000L));
	}
	
	public static String dateTimeFormat(Date dateTime)
	{
		return DATE_TIME_FORMATTER_FIX.format(dateTime);
	}
	
	public static String timeFormat(final TemporalAccessor time)
	{
		return TIME_FORMATTER.format(time);
	}
	
	public static String dateFormat(final TemporalAccessor dateTime)
	{
		return DATE_FORMATTER.format(dateTime);
	}
	
	public static String dateFormat(final Instant instant)
	{
		var dateTime = ZonedDateTime.ofInstant(instant, ZoneId.systemDefault());
		return dateFormat(dateTime);
	}
	
	public static String dateTimeFormat(final TemporalAccessor dateTime)
	{
		return DATE_TIME_FORMATTER.format(dateTime);
	}
	
	public static String dateTimeFormat(final Instant instant)
	{
		var dateTime = ZonedDateTime.ofInstant(instant, ZoneId.systemDefault());
		return dateTimeFormat(dateTime);
	}
}
