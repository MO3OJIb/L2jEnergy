/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.commons.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.random.Rnd;

public final class Util
{
	private static final Logger LOG = LoggerFactory.getLogger(Util.class);
	
	private static final char[] ILLEGAL_CHARACTERS =
	{
		'/',
		'\n',
		'\r',
		'\t',
		'\0',
		'\f',
		'`',
		'?',
		'*',
		'\\',
		'<',
		'>',
		'|',
		'\"',
		':'
	};
	
	/**
	 * Method to get the stack trace of a Throwable into a String
	 * @param t Throwable to get the stacktrace from
	 * @return stack trace from Throwable as String
	 */
	public static String getStackTrace(Throwable t)
	{
		var sw = new StringWriter();
		t.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}
	
	/**
	 * Replaces most invalid characters for the given string with an underscore.
	 * @param str the string that may contain invalid characters
	 * @return the string with invalid character replaced by underscores
	 */
	public static String replaceIllegalCharacters(String str)
	{
		var valid = str;
		for (var c : ILLEGAL_CHARACTERS)
		{
			valid = valid.replace(c, '_');
		}
		return valid;
	}
	
	/**
	 * Verify if a file name is valid.
	 * @param name the name of the file
	 * @return {@code true} if the file name is valid, {@code false} otherwise
	 */
	public static boolean isValidFileName(String name)
	{
		var f = new File(name);
		try
		{
			f.getCanonicalPath();
			return true;
		}
		catch (IOException e)
		{
			return false;
		}
	}
	
	/**
	 * Split words with a space.
	 * @param input the string to split
	 * @return the split string
	 */
	public static String splitWords(String input)
	{
		return input.replaceAll("(\\p{Ll})(\\p{Lu})", "$1 $2");
	}
	
	/**
	 * Gets the next or same closest date from the specified days in {@code daysOfWeek Array} at specified {@code hour} and {@code min}.
	 * @param daysOfWeek the days of week
	 * @param hour the hour
	 * @param min the min
	 * @return the next or same date from the days of week at specified time
	 * @throws IllegalArgumentException if the {@code daysOfWeek Array} is empty.
	 */
	public static LocalDateTime getNextClosestDateTime(DayOfWeek[] daysOfWeek, int hour, int min) throws IllegalArgumentException
	{
		return getNextClosestDateTime(Arrays.asList(daysOfWeek), hour, min);
	}
	
	/**
	 * Gets the next or same closest date from the specified days in {@code daysOfWeek List} at specified {@code hour} and {@code min}.
	 * @param daysOfWeek the days of week
	 * @param hour the hour
	 * @param min the min
	 * @return the next or same date from the days of week at specified time
	 * @throws IllegalArgumentException if the {@code daysOfWeek List} is empty.
	 */
	public static LocalDateTime getNextClosestDateTime(List<DayOfWeek> daysOfWeek, int hour, int min) throws IllegalArgumentException
	{
		if (daysOfWeek.isEmpty())
		{
			throw new IllegalArgumentException("daysOfWeek should not be empty.");
		}
		
		var dateNow = LocalDateTime.now();
		var dateNowWithDifferentTime = dateNow.withHour(hour).withMinute(min).withSecond(0);
		
		// @formatter:off
		return daysOfWeek.stream()
			.map(d -> dateNowWithDifferentTime.with(TemporalAdjusters.nextOrSame(d)))
			.filter(d -> d.isAfter(dateNow))
			.min(Comparator.naturalOrder())
			.orElse(dateNowWithDifferentTime.with(TemporalAdjusters.next(daysOfWeek.get(0))));
		// @formatter:on
	}
	
	/**
	 * This method translates map to function
	 * @param <K> key type of the map and argument of the function
	 * @param <V> value type of the map and return type of the function
	 * @param map the input map
	 * @return a function which returns map.get(arg)
	 */
	public static <K, V> Function<K, V> mapToFunction(Map<K, V> map)
	{
		return key -> map.get(key);
	}
	
	/**
	 * Parses a given argument.
	 * @param args the Java program arguments
	 * @param arg the argument to parse
	 * @param hasArgValue if {@code true} will look for the argument value
	 * @return the argument if hasArgValue is {@code false}, the argument value if hasArgValue is {@code true}, null if the argument is not present
	 */
	public static String parseArg(String[] args, String arg, boolean hasArgValue)
	{
		if ((args == null) || (arg == null) || arg.isEmpty())
		{
			return null;
		}
		
		try
		{
			for (var i = 0; i < args.length; i++)
			{
				if (arg.equals(args[i]))
				{
					return hasArgValue ? args[i + 1] : arg;
				}
			}
			return null;
		}
		catch (Exception ex)
		{
			throw new IllegalArgumentException("Illegal arguments " + Arrays.toString(args) + " and argument " + arg + "!", ex);
		}
	}
	
	public static String randomPassword(int length)
	{
		var lowerChar = "qwertyuiopasdfghjklzxcvbnm";
		var upperChar = "QWERTYUIOPASDFGHJKLZXCVBNM";
		var digits = "1234567890";
		var sb = new StringBuilder(length);
		
		for (var i = 0; i < length; i++)
		{
			var charSet = Rnd.nextInt(3);
			switch (charSet)
			{
				case 0:
					sb.append(lowerChar.charAt(Rnd.nextInt(lowerChar.length() - 1)));
					break;
				case 1:
					sb.append(upperChar.charAt(Rnd.nextInt(upperChar.length() - 1)));
					break;
				case 2:
					sb.append(digits.charAt(Rnd.nextInt(digits.length() - 1)));
					break;
			}
		}
		return sb.toString();
	}
	
	public static byte[] generateHex(int size)
	{
		var array = new byte[size];
		Rnd.nextBytes(array);
		return array;
	}
	
	public static void saveHexid(int serverId, String hexId)
	{
		saveHexid(serverId, hexId, "hexid.txt");
	}
	
	public static void saveHexid(int serverId, String hexId, String fileName)
	{
		try
		{
			var hexSetting = new Properties();
			var file = new File(fileName);
			// Create a new empty file only if it doesn't exist
			file.createNewFile();
			try (var out = new FileOutputStream(file))
			{
				hexSetting.setProperty("ServerID", String.valueOf(serverId));
				hexSetting.setProperty("HexID", hexId);
				hexSetting.store(out, "the hexID to auth into login");
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Failed to save hex id to {} file!", fileName, ex);
		}
	}
}
