/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.commons.versioning;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

public final class Locator
{
	private Locator()
	{
	}
	
	public static File getClassSource(final Class<?> c)
	{
		var classResource = c.getName().replace('.', '/') + ".class";
		return getResourceSource(c.getClassLoader(), classResource);
	}
	
	public static File getResourceSource(ClassLoader c, final String resource)
	{
		if (c == null)
		{
			c = Locator.class.getClassLoader();
		}
		final URL url;
		if (c == null)
		{
			url = ClassLoader.getSystemResource(resource);
		}
		else
		{
			url = c.getResource(resource);
		}
		if (url != null)
		{
			var u = url.toString();
			if (u.startsWith("jar:file:"))
			{
				var pling = u.indexOf('!');
				var jarName = u.substring(4, pling);
				return new File(fromURI(jarName));
			}
			else if (u.startsWith("file:"))
			{
				var tail = u.indexOf(resource);
				var dirName = u.substring(0, tail);
				return new File(fromURI(dirName));
			}
		}
		return null;
	}
	
	public static String fromURI(String uri)
	{
		URL url = null;
		try
		{
			url = URI.create(uri).toURL();
			
		}
		catch (MalformedURLException emYouEarlEx)
		{
			// Ignore malformed exception
		}
		if ((url == null) || !"file".equals(url.getProtocol()))
		{
			throw new IllegalArgumentException("Can only handle valid file: URIs");
		}
		var buf = new StringBuilder(url.getHost());
		if (buf.length() > 0)
		{
			buf.insert(0, File.separatorChar).insert(0, File.separatorChar);
		}
		var file = url.getFile();
		var queryPos = file.indexOf('?');
		buf.append(queryPos < 0 ? file : file.substring(0, queryPos));
		
		uri = buf.toString().replace('/', File.separatorChar);
		
		if ((File.pathSeparatorChar == ';') && uri.startsWith("\\") && (uri.length() > 2) && Character.isLetter(uri.charAt(1)) && (uri.lastIndexOf(':') > -1))
		{
			uri = uri.substring(1);
		}
		return decodeUri(uri);
	}
	
	private static String decodeUri(final String uri)
	{
		if (uri.indexOf('%') == -1)
		{
			return uri;
		}
		var sb = new StringBuilder();
		var iter = new StringCharacterIterator(uri);
		for (var c = iter.first(); c != CharacterIterator.DONE; c = iter.next())
		{
			if (c == '%')
			{
				var c1 = iter.next();
				if (c1 != CharacterIterator.DONE)
				{
					var i1 = Character.digit(c1, 16);
					var c2 = iter.next();
					if (c2 != CharacterIterator.DONE)
					{
						var i2 = Character.digit(c2, 16);
						sb.append((char) ((i1 << 4) + i2));
					}
				}
			}
			else
			{
				sb.append(c);
			}
		}
		return sb.toString();
	}
}
