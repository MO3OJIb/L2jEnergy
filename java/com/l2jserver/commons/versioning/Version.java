/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.commons.versioning;

import java.io.IOException;
import java.util.jar.Attributes;
import java.util.jar.JarFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Version
{
	private static final Logger LOG = LoggerFactory.getLogger(Version.class);
	
	private String _revisionNumber = "exported";
	private String _versionNumber = "-1";
	private String _buildDate = "";
	private String _buildJdk = "";
	
	public Version(final Class<?> c)
	{
		final var jarName = Locator.getClassSource(c);
		try (var jarFile = new JarFile(jarName))
		{
			final var attrs = jarFile.getManifest().getMainAttributes();
			
			setBuildJdk(attrs);
			
			setBuildDate(attrs);
			
			setRevisionNumber(attrs);
			
			setVersionNumber(attrs);
		}
		catch (IOException ex)
		{
			LOG.error("Unable to get soft information\nFile name '{}' isn't a valid jar", (jarName == null ? "null" : jarName.getAbsolutePath()));
		}
	}
	
	public String getRevisionNumber()
	{
		return _revisionNumber;
	}
	
	private void setRevisionNumber(final Attributes attrs)
	{
		final var revisionNumber = attrs.getValue("Implementation-Build");
		if (revisionNumber != null)
		{
			_revisionNumber = revisionNumber;
		}
		else
		{
			_revisionNumber = "-1";
		}
	}
	
	public String getVersionNumber()
	{
		return _versionNumber;
	}
	
	private void setVersionNumber(final Attributes attrs)
	{
		final var versionNumber = attrs.getValue("Implementation-Version");
		if (versionNumber != null)
		{
			_versionNumber = versionNumber;
		}
		else
		{
			_versionNumber = "-1";
		}
	}
	
	public String getBuildDate()
	{
		return _buildDate;
	}
	
	private void setBuildDate(final Attributes attrs)
	{
		final var buildDate = attrs.getValue("Build-Date");
		if (buildDate != null)
		{
			_buildDate = buildDate;
		}
		else
		{
			_buildDate = "-1";
		}
	}
	
	public String getBuildJdk()
	{
		return _buildJdk;
	}
	
	private void setBuildJdk(final Attributes attrs)
	{
		var buildJdk = attrs.getValue("Build-Jdk");
		if (buildJdk != null)
		{
			_buildJdk = buildJdk;
		}
		else
		{
			buildJdk = attrs.getValue("Created-By");
			if (buildJdk != null)
			{
				_buildJdk = buildJdk;
			}
			else
			{
				_buildJdk = "-1";
			}
		}
	}
}
