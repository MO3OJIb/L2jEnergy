/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.configuration.config.ServerConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.CreatureSay;

/**
 * @author Bluur
 */
public class AutoRestartServer
{
	private static final Logger LOG = LoggerFactory.getLogger(AutoRestartServer.class);
	
	private String nextRestartTime = "unknown";
	
	final Calendar currentTime = Calendar.getInstance();
	final Calendar restartTime = Calendar.getInstance();
	Calendar lastRestart = null;
	
	protected AutoRestartServer()
	{
		load();
	}
	
	public void load()
	{
		long delay = 0;
		long lastDelay = 0;
		
		for (var scheduledTime : ServerConfig.AUTO_GAMESERVER_RESTART_INTERVAL)
		{
			var splitTime = scheduledTime.trim().split(":");
			restartTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(splitTime[0]));
			restartTime.set(Calendar.MINUTE, Integer.parseInt(splitTime[1]));
			restartTime.set(Calendar.SECOND, 00);
			
			if (restartTime.getTimeInMillis() < currentTime.getTimeInMillis())
			{
				restartTime.add(Calendar.DAY_OF_WEEK, 1);
			}
			
			if (!ServerConfig.AUTO_GAMESERVER_RESTART_DAYS.isEmpty())
			{
				while (!ServerConfig.AUTO_GAMESERVER_RESTART_DAYS.contains(restartTime.get(Calendar.DAY_OF_WEEK)))
				{
					restartTime.add(Calendar.DAY_OF_WEEK, 1);
				}
			}
			
			delay = restartTime.getTimeInMillis() - currentTime.getTimeInMillis();
			if ((lastDelay == 0) || (delay < lastDelay))
			{
				lastDelay = delay;
				lastRestart = restartTime;
			}
		}
		
		if (lastRestart != null)
		{
			if (ServerConfig.AUTO_GAMESERVER_RESTART_DAYS.isEmpty() || (ServerConfig.AUTO_GAMESERVER_RESTART_DAYS.size() == 7))
			{
				nextRestartTime = TimeUtils.dateTimeFormat(lastRestart.toInstant());
			}
			else
			{
				nextRestartTime = TimeUtils.dateTimeFormat(lastRestart.toInstant());
			}
			
			ThreadPoolManager.getInstance().scheduleGeneral(() ->
			{
				Shutdown.getInstance().startShutdown(null, ServerConfig.AUTO_GAMESERVER_RESTART_SECONDS, true);
			}, lastDelay - (ServerConfig.AUTO_GAMESERVER_RESTART_SECONDS * 1000));
			
			LOG.info("Next auto restart in {}.", lastRestart.getTime());
		}
	}
	
	public String getNextRestartTime()
	{
		return nextRestartTime;
	}
	
	public void getLoginMessage(L2PcInstance player)
	{
		player.sendPacket(new CreatureSay(0, ChatType.BATTLEFIELD, MessagesData.getInstance().getMessage(player, "player_message_server"), MessagesData.getInstance().getMessage(player, "player_message_server_restart").replace("%s%", nextRestartTime + "")));
	}
	
	public static AutoRestartServer getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final AutoRestartServer INSTANCE = new AutoRestartServer();
	}
}
