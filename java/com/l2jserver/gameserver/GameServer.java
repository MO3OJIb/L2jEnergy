/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver;

import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.UPnPService;
import com.l2jserver.commons.dao.ServerNameDAO;
import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.commons.util.IPv4Filter;
import com.l2jserver.commons.util.MemoryUtils;
import com.l2jserver.commons.util.Util;
import com.l2jserver.commons.versioning.Version;
import com.l2jserver.gameserver.bbs.ForumsBBSManager;
import com.l2jserver.gameserver.bbs.StatisticsBBSManager;
import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.AdminConfig;
import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.configuration.config.GeoDataConfig;
import com.l2jserver.gameserver.configuration.config.MMOConfig;
import com.l2jserver.gameserver.configuration.config.ServerConfig;
import com.l2jserver.gameserver.configuration.config.bbs.CBasicConfig;
import com.l2jserver.gameserver.configuration.config.custom.OfflineConfig;
import com.l2jserver.gameserver.configuration.config.custom.PremiumConfig;
import com.l2jserver.gameserver.configuration.config.events.KrateisCubeConfig;
import com.l2jserver.gameserver.configuration.loader.ConfigLoader;
import com.l2jserver.gameserver.configuration.parser.hexid.HexidConfigParser;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.json.ExperienceData;
import com.l2jserver.gameserver.data.json.PvpRewardData;
import com.l2jserver.gameserver.data.sql.impl.AnnouncementsTable;
import com.l2jserver.gameserver.data.sql.impl.CharNameTable;
import com.l2jserver.gameserver.data.sql.impl.CharSummonTable;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.data.sql.impl.CrestTable;
import com.l2jserver.gameserver.data.sql.impl.SummonSkillsTable;
import com.l2jserver.gameserver.data.xml.impl.AdminData;
import com.l2jserver.gameserver.data.xml.impl.ArmorSetsData;
import com.l2jserver.gameserver.data.xml.impl.BotReportData;
import com.l2jserver.gameserver.data.xml.impl.BufferBBSData;
import com.l2jserver.gameserver.data.xml.impl.BuyListData;
import com.l2jserver.gameserver.data.xml.impl.CategoryData;
import com.l2jserver.gameserver.data.xml.impl.ClassListData;
import com.l2jserver.gameserver.data.xml.impl.ColorData;
import com.l2jserver.gameserver.data.xml.impl.ColosseumFenceData;
import com.l2jserver.gameserver.data.xml.impl.DimensionalRiftData;
import com.l2jserver.gameserver.data.xml.impl.DoorData;
import com.l2jserver.gameserver.data.xml.impl.EnchantItemData;
import com.l2jserver.gameserver.data.xml.impl.EnchantItemGroupsData;
import com.l2jserver.gameserver.data.xml.impl.EnchantItemHPBonusData;
import com.l2jserver.gameserver.data.xml.impl.EnchantItemOptionsData;
import com.l2jserver.gameserver.data.xml.impl.EnchantSkillGroupsData;
import com.l2jserver.gameserver.data.xml.impl.EventDroplistData;
import com.l2jserver.gameserver.data.xml.impl.FishData;
import com.l2jserver.gameserver.data.xml.impl.FishingMonstersData;
import com.l2jserver.gameserver.data.xml.impl.FishingRodsData;
import com.l2jserver.gameserver.data.xml.impl.HennaData;
import com.l2jserver.gameserver.data.xml.impl.HitConditionBonusData;
import com.l2jserver.gameserver.data.xml.impl.InitialEquipmentData;
import com.l2jserver.gameserver.data.xml.impl.InitialShortcutData;
import com.l2jserver.gameserver.data.xml.impl.KarmaData;
import com.l2jserver.gameserver.data.xml.impl.LevelUpCrystalData;
import com.l2jserver.gameserver.data.xml.impl.MerchantPriceConfigData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.MultisellData;
import com.l2jserver.gameserver.data.xml.impl.NpcBufferData;
import com.l2jserver.gameserver.data.xml.impl.NpcData;
import com.l2jserver.gameserver.data.xml.impl.OptionData;
import com.l2jserver.gameserver.data.xml.impl.PetDataTable;
import com.l2jserver.gameserver.data.xml.impl.PlayerCreationPointData;
import com.l2jserver.gameserver.data.xml.impl.PlayerTemplateData;
import com.l2jserver.gameserver.data.xml.impl.PlayerXpPercentLostData;
import com.l2jserver.gameserver.data.xml.impl.ProductItemData;
import com.l2jserver.gameserver.data.xml.impl.RecipeData;
import com.l2jserver.gameserver.data.xml.impl.SecondaryAuthData;
import com.l2jserver.gameserver.data.xml.impl.SiegeScheduleData;
import com.l2jserver.gameserver.data.xml.impl.SkillData;
import com.l2jserver.gameserver.data.xml.impl.SkillLearnData;
import com.l2jserver.gameserver.data.xml.impl.SkillTreesData;
import com.l2jserver.gameserver.data.xml.impl.SpawnData;
import com.l2jserver.gameserver.data.xml.impl.StaticObjectData;
import com.l2jserver.gameserver.data.xml.impl.TeleportBBSData;
import com.l2jserver.gameserver.data.xml.impl.TeleportLocationData;
import com.l2jserver.gameserver.data.xml.impl.TransformData;
import com.l2jserver.gameserver.data.xml.impl.UIData;
import com.l2jserver.gameserver.datatables.AugmentationData;
import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.handler.EffectHandler;
import com.l2jserver.gameserver.idfactory.IdFactory;
import com.l2jserver.gameserver.instancemanager.AirShipManager;
import com.l2jserver.gameserver.instancemanager.AntiFeedManager;
import com.l2jserver.gameserver.instancemanager.AuctionManager;
import com.l2jserver.gameserver.instancemanager.BoatManager;
import com.l2jserver.gameserver.instancemanager.CHSiegeManager;
import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.instancemanager.CastleManorManager;
import com.l2jserver.gameserver.instancemanager.ClanHallManager;
import com.l2jserver.gameserver.instancemanager.CoupleManager;
import com.l2jserver.gameserver.instancemanager.CursedWeaponsManager;
import com.l2jserver.gameserver.instancemanager.DayNightSpawnManager;
import com.l2jserver.gameserver.instancemanager.FortManager;
import com.l2jserver.gameserver.instancemanager.FortSiegeManager;
import com.l2jserver.gameserver.instancemanager.FourSepulchersManager;
import com.l2jserver.gameserver.instancemanager.GlobalVariablesManager;
import com.l2jserver.gameserver.instancemanager.GraciaSeedsManager;
import com.l2jserver.gameserver.instancemanager.GrandBossManager;
import com.l2jserver.gameserver.instancemanager.InstanceManager;
import com.l2jserver.gameserver.instancemanager.ItemAuctionManager;
import com.l2jserver.gameserver.instancemanager.ItemsOnGroundManager;
import com.l2jserver.gameserver.instancemanager.MailManager;
import com.l2jserver.gameserver.instancemanager.MapRegionManager;
import com.l2jserver.gameserver.instancemanager.MercTicketManager;
import com.l2jserver.gameserver.instancemanager.PetitionManager;
import com.l2jserver.gameserver.instancemanager.PremiumManager;
import com.l2jserver.gameserver.instancemanager.PunishmentManager;
import com.l2jserver.gameserver.instancemanager.QuestManager;
import com.l2jserver.gameserver.instancemanager.RaidBossPointsManager;
import com.l2jserver.gameserver.instancemanager.RaidBossSpawnManager;
import com.l2jserver.gameserver.instancemanager.SiegeManager;
import com.l2jserver.gameserver.instancemanager.TerritoryWarManager;
import com.l2jserver.gameserver.instancemanager.WalkingManager;
import com.l2jserver.gameserver.instancemanager.ZoneManager;
import com.l2jserver.gameserver.instancemanager.games.FishingChampionshipManager;
import com.l2jserver.gameserver.instancemanager.games.KrateisCubeManager;
import com.l2jserver.gameserver.instancemanager.games.Lottery;
import com.l2jserver.gameserver.instancemanager.games.MiniGameScoreManager;
import com.l2jserver.gameserver.instancemanager.games.MonsterRace;
import com.l2jserver.gameserver.instancemanager.games.UndergroundColiseum;
import com.l2jserver.gameserver.instancemanager.vote.TopManager;
import com.l2jserver.gameserver.model.AutoSpawnHandler;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.PartyMatchRoomList;
import com.l2jserver.gameserver.model.PartyMatchWaitingList;
import com.l2jserver.gameserver.model.entity.Hero;
import com.l2jserver.gameserver.model.eventengine.GameEventAntiAfk;
import com.l2jserver.gameserver.model.eventengine.GameEventLocation;
import com.l2jserver.gameserver.model.eventengine.GameEventManager;
import com.l2jserver.gameserver.model.events.EventDispatcher;
import com.l2jserver.gameserver.model.olympiad.Olympiad;
import com.l2jserver.gameserver.model.vehicles.BoatGiranTalkingTask;
import com.l2jserver.gameserver.model.vehicles.BoatGludinRuneTask;
import com.l2jserver.gameserver.model.vehicles.BoatInnadrilTourTask;
import com.l2jserver.gameserver.model.vehicles.BoatRunePrimevalTask;
import com.l2jserver.gameserver.model.vehicles.BoatTalkingGludinTask;
import com.l2jserver.gameserver.network.L2GameClient;
import com.l2jserver.gameserver.network.L2GamePacketHandler;
import com.l2jserver.gameserver.pathfinding.PathFinding;
import com.l2jserver.gameserver.scripting.L2ScriptEngineManager;
import com.l2jserver.gameserver.taskmanager.KnownListUpdateTaskManager;
import com.l2jserver.gameserver.taskmanager.RandomAnimationTaskManager;
import com.l2jserver.gameserver.taskmanager.TaskManager;
import com.l2jserver.gameserver.util.DeadLockDetector;
import com.l2jserver.mmocore.SelectorConfig;
import com.l2jserver.mmocore.SelectorThread;

public final class GameServer
{
	private static final Logger LOG = LoggerFactory.getLogger(GameServer.class);
	
	private static final String DATAPACK = "-dp";
	private static final String GEODATA = "-gd";
	
	private final Version _version;
	public static long _upTime = 0L;
	public long update = System.currentTimeMillis() / 1000;
	private final SelectorThread<L2GameClient> _selectorThread;
	private final L2GamePacketHandler _gamePacketHandler;
	private final DeadLockDetector _deadDetectThread;
	private static GameServer INSTANCE;
	public static final Calendar dateTimeServerStarted = Calendar.getInstance();
	
	public GameServer() throws Exception
	{
		var serverLoadStart = Instant.now();
		_version = new Version(GameServer.class);
		
		if (!IdFactory.getInstance().isInitialized())
		{
			LOG.error("Could not read object IDs from database. Please check your configuration.");
			throw new Exception("Could not initialize the ID factory!");
		}
		
		ThreadPoolManager.getInstance();
		EventDispatcher.getInstance();
		
		new File("logs/game").mkdirs();
		
		// load script engines
		L2ScriptEngineManager.getInstance();
		
		StringUtil.printSection("World");
		// start game time control early
		GameTimeController.init();
		InstanceManager.getInstance();
		L2World.getInstance();
		MapRegionManager.getInstance();
		AnnouncementsTable.getInstance();
		GlobalVariablesManager.getInstance();
		
		StringUtil.printSection("Data");
		CategoryData.getInstance();
		SecondaryAuthData.getInstance();
		
		StringUtil.printSection("Effects");
		EffectHandler.getInstance().executeScript();
		
		StringUtil.printSection("Enchant Skill Groups");
		EnchantSkillGroupsData.getInstance();
		
		StringUtil.printSection("Skill Trees");
		SkillTreesData.getInstance();
		
		StringUtil.printSection("Skills");
		SkillData.getInstance();
		SummonSkillsTable.getInstance();
		
		StringUtil.printSection("Items");
		ItemTable.getInstance();
		EnchantItemGroupsData.getInstance();
		EnchantItemData.getInstance();
		EnchantItemOptionsData.getInstance();
		OptionData.getInstance();
		EnchantItemHPBonusData.getInstance();
		MerchantPriceConfigData.getInstance().loadInstances();
		BuyListData.getInstance();
		ProductItemData.getInstance();
		MultisellData.getInstance();
		RecipeData.getInstance();
		ArmorSetsData.getInstance();
		FishData.getInstance();
		FishingMonstersData.getInstance();
		FishingRodsData.getInstance();
		HennaData.getInstance();
		LevelUpCrystalData.getInstance();
		PvpRewardData.getInstance();
		
		StringUtil.printSection("Characters");
		ClassListData.getInstance();
		InitialEquipmentData.getInstance();
		InitialShortcutData.getInstance();
		ExperienceData.getInstance();
		PlayerXpPercentLostData.getInstance();
		KarmaData.getInstance();
		HitConditionBonusData.getInstance();
		PlayerTemplateData.getInstance();
		PlayerCreationPointData.getInstance();
		CharNameTable.getInstance();
		AdminData.getInstance();
		RaidBossPointsManager.getInstance();
		PetDataTable.getInstance();
		CharSummonTable.getInstance().init();
		
		// Multi-Language System
		StringUtil.printSection("Messages");
		MessagesData.getInstance();
		
		StringUtil.printSection("Clans");
		ClanTable.getInstance();
		CHSiegeManager.getInstance();
		ClanHallManager.getInstance();
		AuctionManager.getInstance();
		
		StringUtil.printSection("Geodata");
		
		long geodataMemory = MemoryUtils.getMemUsed();
		GeoData.getInstance();
		
		if (GeoDataConfig.PATHFINDING > 0)
		{
			PathFinding.getInstance();
		}
		
		geodataMemory = MemoryUtils.getMemUsed() - geodataMemory;
		if (geodataMemory < 0)
		{
			geodataMemory = 0;
		}
		
		StringUtil.printSection("NPCs");
		SkillLearnData.getInstance();
		NpcData.getInstance();
		WalkingManager.getInstance();
		StaticObjectData.getInstance();
		ZoneManager.getInstance();
		DoorData.getInstance();
		ColosseumFenceData.getInstance();
		CastleManager.getInstance().loadInstances();
		NpcBufferData.getInstance();
		GrandBossManager.getInstance().initZones();
		EventDroplistData.getInstance();
		
		StringUtil.printSection("Auction Manager");
		ItemAuctionManager.getInstance();
		
		StringUtil.printSection("Olympiad");
		Olympiad.getInstance();
		Hero.getInstance();
		
		StringUtil.printSection("Seven Signs");
		SevenSigns.getInstance();
		
		// Call to load caches
		StringUtil.printSection("Cache");
		HtmCache.getInstance();
		CrestTable.getInstance();
		TeleportLocationData.getInstance();
		UIData.getInstance();
		PartyMatchWaitingList.getInstance();
		PartyMatchRoomList.getInstance();
		PetitionManager.getInstance();
		AugmentationData.getInstance();
		CursedWeaponsManager.getInstance();
		TransformData.getInstance();
		BotReportData.getInstance();
		QuestManager.getInstance();
		AirShipManager.getInstance();
		GraciaSeedsManager.getInstance();
		Lottery.getInstance();
		
		StringUtil.printSection("Bonus Tops");
		TopManager.getInstance();
		
		if (CBasicConfig.ENABLE_BBS)
		{
			StringUtil.printSection("BBS");
			ForumsBBSManager.getInstance().load();
			StatisticsBBSManager.getInstance().load();
			BufferBBSData.getInstance();
			TeleportBBSData.getInstance();
			ColorData.getInstance();
		}
		
		StringUtil.printSection("Handlers");
		L2ScriptEngineManager.getInstance().executeScript(new File(L2ScriptEngineManager.SCRIPT_FOLDER, "handlers/MasterHandler.java"));
		
		StringUtil.printSection("AI");
		L2ScriptEngineManager.getInstance().executeScript(new File(L2ScriptEngineManager.SCRIPT_FOLDER, "ai/AILoader.java"));
		
		StringUtil.printSection("Instances");
		L2ScriptEngineManager.getInstance().executeScript(new File(L2ScriptEngineManager.SCRIPT_FOLDER, "instances/InstanceLoader.java"));
		
		StringUtil.printSection("Gracia");
		L2ScriptEngineManager.getInstance().executeScript(new File(L2ScriptEngineManager.SCRIPT_FOLDER, "gracia/GraciaLoader.java"));
		
		StringUtil.printSection("Hellbound");
		L2ScriptEngineManager.getInstance().executeScript(new File(L2ScriptEngineManager.SCRIPT_FOLDER, "hellbound/HellboundLoader.java"));
		
		StringUtil.printSection("Quests");
		L2ScriptEngineManager.getInstance().executeScript(new File(L2ScriptEngineManager.SCRIPT_FOLDER, "quests/QuestLoader.java"));
		L2ScriptEngineManager.getInstance().executeScript(new File(L2ScriptEngineManager.SCRIPT_FOLDER, "quests/TerritoryWarScripts/TerritoryWarSuperClass.java"));
		
		StringUtil.printSection("Scripts");
		L2ScriptEngineManager.getInstance().executeScript(new File(L2ScriptEngineManager.SCRIPT_FOLDER, "conquerablehalls/CHLoader.java"));
		L2ScriptEngineManager.getInstance().executeScript(new File(L2ScriptEngineManager.SCRIPT_FOLDER, "custom/CustomLoader.java"));
		
		if (GeneralConfig.ALLOW_BOAT)
		{
			BoatManager.getInstance();
			BoatGiranTalkingTask.load();
			BoatGludinRuneTask.load();
			BoatInnadrilTourTask.load();
			BoatRunePrimevalTask.load();
			BoatTalkingGludinTask.load();
		}
		
		StringUtil.printSection("Spawns");
		SpawnData.getInstance().load();
		DayNightSpawnManager.getInstance().trim().notifyChangeMode();
		FourSepulchersManager.getInstance().init();
		DimensionalRiftData.getInstance();
		RaidBossSpawnManager.getInstance();
		
		StringUtil.printSection("Siege");
		SiegeManager.getInstance().getSieges();
		CastleManager.getInstance().activateInstances();
		FortManager.getInstance().loadInstances();
		FortManager.getInstance().activateInstances();
		FortSiegeManager.getInstance();
		SiegeScheduleData.getInstance();
		
		MerchantPriceConfigData.getInstance().updateReferences();
		TerritoryWarManager.getInstance();
		CastleManorManager.getInstance();
		MercTicketManager.getInstance();
		
		if (PremiumConfig.PREMIUM_SYSTEM_ENABLED)
		{
			PremiumManager.getInstance();
		}
		
		if (GeneralConfig.SAVE_DROPPED_ITEM)
		{
			ItemsOnGroundManager.getInstance();
		}
		
		if ((GeneralConfig.AUTODESTROY_ITEM_AFTER > 0) || (GeneralConfig.HERB_AUTO_DESTROY_TIME > 0))
		{
			ItemsAutoDestroy.getInstance();
		}
		
		MonsterRace.getInstance();
		FishingChampionshipManager.getInstance();
		
		if (KrateisCubeConfig.KRATEIS_CUBE_EVENT_ENABLED)
		{
			KrateisCubeManager.getInstance();
		}
		UndergroundColiseum.getInstance();
		
		SevenSigns.getInstance().spawnSevenSignsNPC();
		SevenSignsFestival.getInstance();
		AutoSpawnHandler.getInstance();
		
		LOG.info("Loaded {} AutoSpawnHandler in total.", AutoSpawnHandler.getInstance().size());
		
		CoupleManager.getInstance();
		
		if (GeneralConfig.EX_JAPAN_MINIGAME)
		{
			MiniGameScoreManager.getInstance();
		}
		
		TaskManager.getInstance();
		AntiFeedManager.getInstance().registerEvent(AntiFeedManager.GAME_ID);
		
		if (GeneralConfig.ALLOW_MAIL)
		{
			MailManager.getInstance();
		}
		
		PunishmentManager.getInstance();
		
		Runtime.getRuntime().addShutdownHook(Shutdown.getInstance());
		
		LOG.info("Free ObjectID's remaining: {}", IdFactory.getInstance().size());
		
		StringUtil.printSection("Events");
		L2ScriptEngineManager.getInstance().executeScript(new File(L2ScriptEngineManager.SCRIPT_FOLDER, "events/EventsLoader.java"));
		
		GameEventManager.getInstance();
		GameEventAntiAfk.getInstance();
		GameEventLocation.getInstance();
		KnownListUpdateTaskManager.getInstance();
		RandomAnimationTaskManager.getInstance();
		
		if ((OfflineConfig.OFFLINE_TRADE_ENABLE || OfflineConfig.OFFLINE_CRAFT_ENABLE) && OfflineConfig.RESTORE_OFFLINERS)
		{
			StringUtil.printSection("Offline trade");
			DAOFactory.getInstance().getPlayerOfflineTradersDAO().restoreOfflineTraders();
		}
		
		if (AdminConfig.DEADLOCK_DETECTOR)
		{
			_deadDetectThread = new DeadLockDetector();
			_deadDetectThread.setDaemon(true);
			_deadDetectThread.start();
		}
		else
		{
			_deadDetectThread = null;
		}
		System.gc();
		Toolkit.getDefaultToolkit().beep();
		
		StringUtil.printSection("Server Info");
		LOG.info("Maximum numbers of connected players: {}", ServerConfig.MAXIMUM_ONLINE_USERS);
		LOG.info("Started, free memory {} of {}", MemoryUtils.getMemFreeMb(), MemoryUtils.getMemMaxMb());
		LOG.info("Used memory: {}", MemoryUtils.getMemUsedMb());
		LOG.info("Geodata use {} MB of memory", geodataMemory);
		LOG.info("Revision: ................ {}", getVersion().getRevisionNumber());
		LOG.info("Builded: ................. {}", getVersion().getBuildDate());
		LOG.info("Compiler version: ........ {}", getVersion().getBuildJdk());
		LOG.info("Chronicle version: ....... {}", ServerConfig.CHRONICLE_VERSION);
		LOG.info("Forum: ................... L2jEnergy.ru");
		LOG.info("Server {} loaded in {} seconds.", ServerNameDAO.getServer(HexidConfigParser.SERVER_ID), serverLoadStart.until(Instant.now(), ChronoUnit.SECONDS));
		if (ServerConfig.AUTO_GAMESERVER_RESTART_ENABLE)
		{
			AutoRestartServer.getInstance();
		}
		_upTime = System.currentTimeMillis();
		
		if (ServerConfig.ENABLE_UPNP)
		{
			StringUtil.printSection("UPnP");
			UPnPService.getInstance().load(ServerConfig.PORT_GAME, "L2J Game Server");
		}
		
		StringUtil.printSection("Login");
		LoginServerThread.getInstance().start();
		
		final SelectorConfig sc = new SelectorConfig();
		sc.MAX_READ_PER_PASS = MMOConfig.MMO_MAX_READ_PER_PASS;
		sc.MAX_SEND_PER_PASS = MMOConfig.MMO_MAX_SEND_PER_PASS;
		sc.SLEEP_TIME = MMOConfig.MMO_SELECTOR_SLEEP_TIME;
		sc.HELPER_BUFFER_COUNT = MMOConfig.MMO_HELPER_BUFFER_COUNT;
		sc.TCP_NODELAY = MMOConfig.MMO_TCP_NODELAY;
		
		_gamePacketHandler = new L2GamePacketHandler();
		_selectorThread = new SelectorThread<>(sc, _gamePacketHandler, _gamePacketHandler, _gamePacketHandler, new IPv4Filter());
		
		InetAddress bindAddress = null;
		if (!ServerConfig.GAMESERVER_HOSTNAME.equals("*"))
		{
			try
			{
				bindAddress = InetAddress.getByName(ServerConfig.GAMESERVER_HOSTNAME);
			}
			catch (UnknownHostException ex)
			{
				LOG.error("Bind address is invalid, using all available IPs!", ex);
			}
		}
		
		try
		{
			_selectorThread.openServerSocket(bindAddress, ServerConfig.PORT_GAME);
			_selectorThread.start();
			LOG.info("Now listening on {}:{}", ServerConfig.GAMESERVER_HOSTNAME, ServerConfig.PORT_GAME);
		}
		catch (IOException ex)
		{
			LOG.error("Failed to open server socket!", ex);
			System.exit(1);
		}
	}
	
	public static void main(String[] args) throws Exception
	{
		StringUtil.printSection("Config");
		// Initialize configurations.
		ConfigLoader.loading();
		
		final String dp = Util.parseArg(args, DATAPACK, true);
		if (dp != null)
		{
			ServerConfig.DATAPACK_ROOT = new File(dp);
		}
		
		final String gd = Util.parseArg(args, GEODATA, true);
		if (gd != null)
		{
			GeoDataConfig.GEODATA_PATH = Paths.get(gd);
		}
		
		final String LOG_FOLDER = "./logs"; // Name of folder for log file
		// Create log folder
		File logFolder = new File(LOG_FOLDER);
		logFolder.mkdir();
		
		StringUtil.printSection("Database");
		DAOFactory.getInstance();
		
		ConnectionFactory.builder() //
			.withUrl(ServerConfig.DATABASE_URL) //
			.withUser(ServerConfig.DATABASE_LOGIN) //
			.withPassword(ServerConfig.DATABASE_PASSWORD) //
			.withMaxPoolSize(ServerConfig.DATABASE_MAX_CONNECTIONS) //
			.build();
		
		INSTANCE = new GameServer();
	}
	
	public SelectorThread<L2GameClient> getSelectorThread()
	{
		return _selectorThread;
	}
	
	public L2GamePacketHandler getL2GamePacketHandler()
	{
		return _gamePacketHandler;
	}
	
	public DeadLockDetector getDeadLockDetectorThread()
	{
		return _deadDetectThread;
	}
	
	public Version getVersion()
	{
		return _version;
	}
	
	public static GameServer getInstance()
	{
		return INSTANCE;
	}
}
