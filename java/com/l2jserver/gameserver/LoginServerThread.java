/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAKeyGenParameterSpec;
import java.security.spec.RSAPublicKeySpec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.network.AttributeType;
import com.l2jserver.commons.network.BaseSendablePacket;
import com.l2jserver.commons.network.ServerType;
import com.l2jserver.commons.random.Rnd;
import com.l2jserver.commons.security.crypt.NewCrypt;
import com.l2jserver.commons.util.Util;
import com.l2jserver.gameserver.configuration.config.AdminConfig;
import com.l2jserver.gameserver.configuration.config.ServerConfig;
import com.l2jserver.gameserver.configuration.parser.IPConfigDataParser;
import com.l2jserver.gameserver.configuration.parser.hexid.HexidConfigParser;
import com.l2jserver.gameserver.enums.network.FailReason;
import com.l2jserver.gameserver.enums.network.GameClientState;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.L2GameClient;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.gameserverpackets.AuthRequest;
import com.l2jserver.gameserver.network.gameserverpackets.BlowFishKey;
import com.l2jserver.gameserver.network.gameserverpackets.ChangeAccessLevel;
import com.l2jserver.gameserver.network.gameserverpackets.ChangeHwidAddres;
import com.l2jserver.gameserver.network.gameserverpackets.ChangeIpAddres;
import com.l2jserver.gameserver.network.gameserverpackets.ChangePassword;
import com.l2jserver.gameserver.network.gameserverpackets.PlayerAuthRequest;
import com.l2jserver.gameserver.network.gameserverpackets.PlayerInGame;
import com.l2jserver.gameserver.network.gameserverpackets.PlayerLogout;
import com.l2jserver.gameserver.network.gameserverpackets.PlayerTracert;
import com.l2jserver.gameserver.network.gameserverpackets.ReplyCharacters;
import com.l2jserver.gameserver.network.gameserverpackets.SendMail;
import com.l2jserver.gameserver.network.gameserverpackets.ServerStatus;
import com.l2jserver.gameserver.network.gameserverpackets.TempBan;
import com.l2jserver.gameserver.network.loginserverpackets.AuthResponse;
import com.l2jserver.gameserver.network.loginserverpackets.ChangePasswordResponse;
import com.l2jserver.gameserver.network.loginserverpackets.InitLS;
import com.l2jserver.gameserver.network.loginserverpackets.KickPlayer;
import com.l2jserver.gameserver.network.loginserverpackets.LoginServerFail;
import com.l2jserver.gameserver.network.loginserverpackets.PlayerAuthResponse;
import com.l2jserver.gameserver.network.loginserverpackets.RequestCharacters;
import com.l2jserver.gameserver.network.serverpackets.CharacterSelectionInfo;
import com.l2jserver.gameserver.network.serverpackets.LoginFail;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

public class LoginServerThread extends Thread
{
	protected static final Logger LOG = LoggerFactory.getLogger(LoginServerThread.class);
	protected static final Logger LOG_ACCOUNTING = LoggerFactory.getLogger("accounting");
	
	private final Map<String, L2GameClient> _clients = new ConcurrentHashMap<>();
	
	private int _serverId;
	private String _serverName;
	
	private Socket _loginSocket;
	private InputStream _in;
	private OutputStream _out;
	
	private NewCrypt _blowfish;
	private byte[] _blowfishKey;
	private RSAPublicKey _publicKey;
	
	private byte[] _hexId;
	
	private final int _requestId;
	private int _maxPlayer;
	private final List<String> _subnets;
	private final List<String> _hosts;
	private ServerType _type = ServerType.AUTO;
	
	private final List<WaitingClient> _waitingClients;
	
	protected LoginServerThread()
	{
		super("LoginServerThread");
		
		_hexId = HexidConfigParser.HEX_ID;
		if (_hexId == null)
		{
			_requestId = ServerConfig.REQUEST_ID;
			_hexId = Util.generateHex(16);
		}
		else
		{
			_requestId = HexidConfigParser.SERVER_ID;
		}
		
		_subnets = IPConfigDataParser.getInstance().getSubnets();
		_hosts = IPConfigDataParser.getInstance().getHosts();
		_waitingClients = new CopyOnWriteArrayList<>();
		_maxPlayer = ServerConfig.MAXIMUM_ONLINE_USERS;
	}
	
	@Override
	public void run()
	{
		while (!isInterrupted())
		{
			boolean checksumOk = false;
			try
			{
				// Connection
				LOG.info("Connecting to login on {}:{}", ServerConfig.GAME_SERVER_LOGIN_HOST, ServerConfig.GAME_SERVER_LOGIN_PORT);
				
				_loginSocket = new Socket(ServerConfig.GAME_SERVER_LOGIN_HOST, ServerConfig.GAME_SERVER_LOGIN_PORT);
				_in = _loginSocket.getInputStream();
				_out = new BufferedOutputStream(_loginSocket.getOutputStream());
				
				// init Blowfish
				_blowfishKey = Util.generateHex(40);
				// Protect the new blowfish key what cannot begin with zero
				if (_blowfishKey[0] == 0)
				{
					_blowfishKey[0] = (byte) Rnd.get(32, 64);
				}
				_blowfish = new NewCrypt("_;v.]05-31!|+-%xT!^[$\00");
				while (!isInterrupted())
				{
					int lengthLo = _in.read();
					int lengthHi = _in.read();
					int length = (lengthHi * 256) + lengthLo;
					
					if (lengthHi < 0)
					{
						break;
					}
					
					byte[] incoming = new byte[length - 2];
					
					int receivedBytes = 0;
					int newBytes = 0;
					int left = length - 2;
					while ((newBytes != -1) && (receivedBytes < (length - 2)))
					{
						newBytes = _in.read(incoming, receivedBytes, left);
						receivedBytes = receivedBytes + newBytes;
						left -= newBytes;
					}
					
					if (receivedBytes != (length - 2))
					{
						LOG.warn("Incomplete packet is sent to the server, closing connection.");
						break;
					}
					
					// decrypt if we have a key
					_blowfish.decrypt(incoming, 0, incoming.length);
					checksumOk = NewCrypt.verifyChecksum(incoming);
					
					if (!checksumOk)
					{
						LOG.warn("Incorrect packet checksum, ignoring packet.");
						break;
					}
					
					int packetType = incoming[0] & 0xff;
					
					// send the blowfish key through the rsa encryption
					// now, only accept packet with the new encryption
					// login will close the connection here
					switch (packetType)
					{
						case 0x00:
							final InitLS init = new InitLS(incoming);
							
							try
							{
								KeyFactory kfac = KeyFactory.getInstance("RSA");
								BigInteger modulus = new BigInteger(init.getRSAKey());
								RSAPublicKeySpec kspec1 = new RSAPublicKeySpec(modulus, RSAKeyGenParameterSpec.F4);
								
								_publicKey = (RSAPublicKey) kfac.generatePublic(kspec1);
							}
							catch (GeneralSecurityException e)
							{
								LOG.warn("Trouble while init the public key send by login");
								break;
							}
							
							// send the blowfish key through the rsa encryption
							sendPacket(new BlowFishKey(_blowfishKey, _publicKey));
							
							// now, only accept packet with the new encryption
							_blowfish = new NewCrypt(_blowfishKey);
							sendPacket(new AuthRequest(_requestId, ServerConfig.ACCEPT_ALTERNATE_ID, _hexId, ServerConfig.PORT_GAME, ServerConfig.RESERVE_HOST_ON_LOGIN, _maxPlayer, _subnets, _hosts));
							break;
						case 0x01:
							// login will close the connection here
							final LoginServerFail lsf = new LoginServerFail(incoming);
							LOG.info("LoginServer registration failed: {}.", lsf.getReasonString());
							break;
						case 0x02:
							final AuthResponse aresp = new AuthResponse(incoming);
							
							_serverId = aresp.getServerId();
							_serverName = aresp.getServerName();
							
							Util.saveHexid(_serverId, new BigInteger(_hexId).toString(16), HexidConfigParser.HEXID_FILE);
							LOG.info("Registered on login as Server [{}] {}.", _serverId, _serverName);
							
							final ServerStatus ss = new ServerStatus();
							ss.addAttribute(AttributeType.STATUS, (AdminConfig.SERVER_GMONLY) ? ServerType.GM_ONLY.getId() : ServerType.AUTO.getId());
							ss.addAttribute(AttributeType.TYPE, AdminConfig.SERVER_LIST_TYPE);
							ss.addAttribute(AttributeType.BRACKETS, AdminConfig.SERVER_LIST_BRACKETS);
							ss.addAttribute(AttributeType.AGE_LIMIT, AdminConfig.SERVER_LIST_AGE);
							sendPacket(ss);
							
							final List<String> playerList = L2World.getInstance().getPlayers().stream().filter(player -> !player.isInOfflineMode()).map(L2PcInstance::getAccountName).collect(Collectors.toList());
							if (!playerList.isEmpty())
							{
								sendPacket(new PlayerInGame(playerList));
							}
							break;
						case 0x03:
							PlayerAuthResponse par = new PlayerAuthResponse(incoming);
							String account = par.getAccount();
							WaitingClient wcToRemove = null;
							synchronized (_waitingClients)
							{
								for (WaitingClient wc : _waitingClients)
								{
									if (wc.account.equals(account))
									{
										wcToRemove = wc;
									}
								}
							}
							if (wcToRemove != null)
							{
								if (par.isAuthed())
								{
									sendPacket(new PlayerInGame(par.getAccount()));
									
									wcToRemove.gameClient.setState(GameClientState.AUTHED);
									wcToRemove.gameClient.setSessionId(wcToRemove.session);
									CharacterSelectionInfo cl = new CharacterSelectionInfo(wcToRemove.account, wcToRemove.gameClient.getSessionId().playOkID1);
									wcToRemove.gameClient.getConnection().sendPacket(cl);
									wcToRemove.gameClient.setCharSelection(cl.getCharInfo());
								}
								else
								{
									LOG.warn("Session key is not correct. Closing connection for account {}.", wcToRemove.account);
									// wcToRemove.gameClient.getConnection().sendPacket(new LoginFail(LoginFail.SYSTEM_ERROR_LOGIN_LATER));
									wcToRemove.gameClient.close(new LoginFail(FailReason.SYSTEM_ERROR_LOGIN_LATER));
									_clients.remove(wcToRemove.account);
								}
								_waitingClients.remove(wcToRemove);
							}
							break;
						case 0x04:
							KickPlayer kp = new KickPlayer(incoming);
							kickPlayer(kp.getAccount());
							break;
						case 0x05:
							RequestCharacters rc = new RequestCharacters(incoming);
							getCharsOnServer(rc.getAccount());
							break;
						case 0x06:
							new ChangePasswordResponse(incoming);
							break;
					}
				}
			}
			catch (UnknownHostException e)
			{
				LOG.warn("Unknown host!", e);
			}
			catch (SocketException e)
			{
				LOG.warn("LoginServer not avaible, trying to reconnect...");
			}
			catch (IOException e)
			{
				LOG.warn("Disconnected from Login, Trying to reconnect!", e);
			}
			finally
			{
				try
				{
					_loginSocket.close();
					if (isInterrupted())
					{
						return;
					}
				}
				catch (Exception e)
				{
				}
			}
			
			try
			{
				Thread.sleep(10000); // 10 seconds tempo.
			}
			catch (InterruptedException e)
			{
				return; // never swallow an interrupt!
			}
		}
	}
	
	/**
	 * Adds the waiting client and send request.
	 * @param acc the account
	 * @param client the game client
	 * @param key the session key
	 */
	public void addWaitingClientAndSendRequest(String acc, L2GameClient client, SessionKey key)
	{
		WaitingClient wc = new WaitingClient(acc, client, key);
		synchronized (_waitingClients)
		{
			_waitingClients.add(wc);
		}
		PlayerAuthRequest par = new PlayerAuthRequest(acc, key);
		try
		{
			sendPacket(par);
		}
		catch (IOException e)
		{
			LOG.warn("Error while sending player auth request!");
		}
	}
	
	/**
	 * Removes the waiting client.
	 * @param client the client
	 */
	public void removeWaitingClient(L2GameClient client)
	{
		WaitingClient toRemove = null;
		synchronized (_waitingClients)
		{
			for (WaitingClient c : _waitingClients)
			{
				if (c.gameClient == client)
				{
					toRemove = c;
				}
			}
			if (toRemove != null)
			{
				_waitingClients.remove(toRemove);
			}
		}
	}
	
	public void sendLogout(String account)
	{
		if (account == null)
		{
			return;
		}
		
		try
		{
			sendPacket(new PlayerLogout(account));
		}
		catch (IOException ex)
		{
			LOG.warn("Error while sending logout packet to login!");
		}
		finally
		{
			_clients.remove(account);
		}
	}
	
	public boolean addGameServerLogin(String account, L2GameClient client)
	{
		return _clients.putIfAbsent(account, client) == null;
	}
	
	public void sendAccessLevel(String account, int level)
	{
		try
		{
			sendPacket(new ChangeAccessLevel(account, level));
		}
		catch (IOException e)
		{
		}
	}
	
	public void sendClientTracert(String account, String[] address)
	{
		try
		{
			sendPacket(new PlayerTracert(account, address[0], address[1], address[2], address[3], address[4]));
		}
		catch (IOException e)
		{
		}
	}
	
	public void sendMail(String account, String mailId, String... args)
	{
		try
		{
			sendPacket(new SendMail(account, mailId, args));
		}
		catch (IOException e)
		{
		}
	}
	
	public void sendTempBan(String account, String ip, long time)
	{
		try
		{
			sendPacket(new TempBan(account, ip, time));
		}
		catch (IOException e)
		{
		}
	}
	
	public void kickPlayer(String account)
	{
		final L2GameClient client = _clients.get(account);
		if (client != null)
		{
			LOG_ACCOUNTING.warn("Kicked by login: {}", client);
			client.setAdditionalClosePacket(SystemMessage.getSystemMessage(SystemMessageId.ANOTHER_LOGIN_WITH_ACCOUNT));
			client.closeNow();
		}
	}
	
	private void getCharsOnServer(String account)
	{
		
		int chars = 0;
		List<Long> charToDel = new ArrayList<>();
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT deletetime FROM characters WHERE account_name=?"))
		{
			ps.setString(1, account);
			try (ResultSet rs = ps.executeQuery())
			{
				while (rs.next())
				{
					chars++;
					long delTime = rs.getLong("deletetime");
					if (delTime != 0)
					{
						charToDel.add(delTime);
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Exception: getCharsOnServer!", ex);
		}
		
		try
		{
			sendPacket(new ReplyCharacters(account, chars, charToDel));
		}
		catch (IOException e)
		{
		}
	}
	
	private void sendPacket(BaseSendablePacket sl) throws IOException
	{
		byte[] data = sl.getContent();
		NewCrypt.appendChecksum(data);
		_blowfish.crypt(data, 0, data.length);
		
		int len = data.length + 2;
		synchronized (_out) // avoids tow threads writing in the mean time
		{
			_out.write(len & 0xff);
			_out.write((len >> 8) & 0xff);
			_out.write(data);
			_out.flush();
		}
	}
	
	public void setMaxPlayer(int maxPlayer)
	{
		sendServerStatus(AttributeType.MAX_PLAYERS, maxPlayer);
		_maxPlayer = maxPlayer;
	}
	
	public int getMaxPlayer()
	{
		return _maxPlayer;
	}
	
	public void sendServerStatus(AttributeType type, int value)
	{
		try
		{
			final ServerStatus ss = new ServerStatus();
			ss.addAttribute(type, value);
			
			sendPacket(ss);
		}
		catch (IOException e)
		{
		}
	}
	
	public String getServerName()
	{
		return _serverName;
	}
	
	public ServerType getServerType()
	{
		return _type;
	}
	
	public void setServerType(ServerType type)
	{
		sendServerStatus(AttributeType.STATUS, type.getId());
		
		_type = type;
	}
	
	public void sendChangePassword(String account, String character, String oldpass, String newpass)
	{
		try
		{
			sendPacket(new ChangePassword(account, character, oldpass, newpass));
		}
		catch (IOException e)
		{
		}
	}
	
	public void sendAllowedIP(String account, String ip)
	{
		try
		{
			sendPacket(new ChangeIpAddres(account, ip));
		}
		catch (IOException e)
		{
		}
	}
	
	public void sendAllowedHwid(String account, String hwid)
	{
		try
		{
			sendPacket(new ChangeHwidAddres(account, hwid));
		}
		catch (IOException e)
		{
		}
	}
	
	public L2GameClient getClient(String name)
	{
		return name != null ? _clients.get(name) : null;
	}
	
	public static class SessionKey
	{
		public int playOkID1;
		public int playOkID2;
		public int loginOkID1;
		public int loginOkID2;
		
		/**
		 * Instantiates a new session key.
		 * @param loginOK1 the login o k1
		 * @param loginOK2 the login o k2
		 * @param playOK1 the play o k1
		 * @param playOK2 the play o k2
		 */
		public SessionKey(int loginOK1, int loginOK2, int playOK1, int playOK2)
		{
			playOkID1 = playOK1;
			playOkID2 = playOK2;
			loginOkID1 = loginOK1;
			loginOkID2 = loginOK2;
		}
		
		@Override
		public String toString()
		{
			return "PlayOk: " + playOkID1 + " " + playOkID2 + " LoginOk:" + loginOkID1 + " " + loginOkID2;
		}
	}
	
	private static class WaitingClient
	{
		public String account;
		public L2GameClient gameClient;
		public SessionKey session;
		
		/**
		 * Instantiates a new waiting client.
		 * @param acc the acc
		 * @param client the client
		 * @param key the key
		 */
		public WaitingClient(String acc, L2GameClient client, SessionKey key)
		{
			account = acc;
			gameClient = client;
			session = key;
		}
	}
	
	public static LoginServerThread getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final LoginServerThread INSTANCE = new LoginServerThread();
	}
}
