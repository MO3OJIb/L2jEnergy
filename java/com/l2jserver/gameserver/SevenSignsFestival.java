/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.ai.CtrlIntention;
import com.l2jserver.gameserver.configuration.config.FeatureConfig;
import com.l2jserver.gameserver.configuration.config.RatesConfig;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.data.xml.impl.SpawnData;
import com.l2jserver.gameserver.model.FestivalSpawn;
import com.l2jserver.gameserver.model.L2Party;
import com.l2jserver.gameserver.model.L2Spawn;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.actor.instance.L2FestivalMonsterInstance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.Util;

/**
 * @author Tempy
 */
public class SevenSignsFestival
{
	protected static final Logger LOG = LoggerFactory.getLogger(SevenSignsFestival.class);
	
	private static final String GET_CLAN_NAME = "SELECT clan_name FROM clan_data WHERE clan_id = (SELECT clanid FROM characters WHERE char_name = ?)";
	
	private L2PcInstance _player;
	
	// Key Constants \\
	private static final int FESTIVAL_DEFAULT_RESPAWN = 60; // Specify in seconds!
	
	public static final int FESTIVAL_COUNT = 5; // do not change without correcting db and SevenSigns itself !
	public static final int FESTIVAL_LEVEL_MAX_31 = 0;
	public static final int FESTIVAL_LEVEL_MAX_42 = 1;
	public static final int FESTIVAL_LEVEL_MAX_53 = 2;
	public static final int FESTIVAL_LEVEL_MAX_64 = 3;
	public static final int FESTIVAL_LEVEL_MAX_NONE = 4;
	// 500 maximum possible score
	public static final int[] FESTIVAL_LEVEL_SCORES =
	{
		60,
		70,
		100,
		120,
		150
	};
	
	public static final int FESTIVAL_OFFERING_ID = 5901;
	private static final int MAX_DISTANCE = 1500;
	
	/**
	 * The following contains all the necessary spawn data for:<br>
	 * <ul>
	 * <li>Player Start Locations
	 * <li>
	 * <li>Monsters
	 * <li>
	 * <li>Chests</li>
	 * </ul>
	 * All data is given by: X, Y, Z (coords), Heading, NPC ID (if necessary).<br>
	 * This may be moved externally in time, but the data should not change.<br>
	 */
	// @formatter:off
	public static final int[][] FESTIVAL_DAWN_PLAYER_SPAWNS =
	{
		{ -79187, 113186, -4895 }, // 31 and below
		{ -75918, 110137, -4895 }, // 42 and below
		{ -73835, 111969, -4895 }, // 53 and below
		{ -76170, 113804, -4895 }, // 64 and below
		{ -78927, 109528, -4895 }  // No level limit
	};
	
	public static final int[][] FESTIVAL_DUSK_PLAYER_SPAWNS = 
	{
		{ -77200, 88966, -5151 }, // 31 and below
		{ -76941, 85307, -5151 }, // 42 and below
		{ -74855, 87135, -5151 }, // 53 and below
		{ -80208, 88222, -5151 }, // 64 and below
		{ -79954, 84697, -5151 }  // No level limit
	};

	protected static final int[][][] FESTIVAL_DAWN_PRIMARY_SPAWNS =
	{
		{
			/* Level 31 and Below - Offering of the Branded */
			{ -78537, 113839, -4895, -1, 18009 },
			{ -78466, 113852, -4895, -1, 18010 },
			{ -78509, 113899, -4895, -1, 18010 },
			
			{ -78481, 112557, -4895, -1, 18009 },
			{ -78559, 112504, -4895, -1, 18010 },
			{ -78489, 112494, -4895, -1, 18010 },
			
			{ -79803, 112543, -4895, -1, 18012 },
			{ -79854, 112492, -4895, -1, 18013 },
			{ -79886, 112557, -4895, -1, 18014 },
			
			{ -79821, 113811, -4895, -1, 18015 },
			{ -79857, 113896, -4895, -1, 18017 },
			{ -79878, 113816, -4895, -1, 18018 },
			
			// Archers and Marksmen \\
			{ -79190, 113660, -4895, -1, 18011 },
			{ -78710, 113188, -4895, -1, 18011 },
			{ -79190, 112730, -4895, -1, 18016 },
			{ -79656, 113188, -4895, -1, 18016 }
		},
		{
			/* Level 42 and Below - Apostate Offering */
			{ -76558, 110784, -4895, -1, 18019 },
			{ -76607, 110815, -4895, -1, 18020 }, // South West
			{ -76559, 110820, -4895, -1, 18020 },
			
			{ -75277, 110792, -4895, -1, 18019 },
			{ -75225, 110801, -4895, -1, 18020 }, // South East
			{ -75262, 110832, -4895, -1, 18020 },
			
			{ -75249, 109441, -4895, -1, 18022 },
			{ -75278, 109495, -4895, -1, 18023 }, // North East
			{ -75223, 109489, -4895, -1, 18024 },
			
			{ -76556, 109490, -4895, -1, 18025 },
			{ -76607, 109469, -4895, -1, 18027 }, // North West
			{ -76561, 109450, -4895, -1, 18028 },
			// Archers and Marksmen \\
			{ -76399, 110144, -4895, -1, 18021 },
			{ -75912, 110606, -4895, -1, 18021 },
			{ -75444, 110144, -4895, -1, 18026 },
			{ -75930, 109665, -4895, -1, 18026 }
		},
		{
			/* Level 53 and Below - Witch's Offering */
			{ -73184, 111319, -4895, -1, 18029 },
			{ -73135, 111294, -4895, -1, 18030 }, // South West
			{ -73185, 111281, -4895, -1, 18030 },
			
			{ -74477, 111321, -4895, -1, 18029 },
			{ -74523, 111293, -4895, -1, 18030 }, // South East
			{ -74481, 111280, -4895, -1, 18030 },
			
			{ -74489, 112604, -4895, -1, 18032 },
			{ -74491, 112660, -4895, -1, 18033 }, // North East
			{ -74527, 112629, -4895, -1, 18034 },
			
			{ -73197, 112621, -4895, -1, 18035 },
			{ -73142, 112631, -4895, -1, 18037 }, // North West
			{ -73182, 112656, -4895, -1, 18038 },
			
			// Archers and Marksmen \\
			{ -73834, 112430, -4895, -1, 18031 },
			{ -74299, 111959, -4895, -1, 18031 },
			{ -73841, 111491, -4895, -1, 18036 },
			{ -73363, 111959, -4895, -1, 18036 }
		},
		{
			/* Level 64 and Below - Dark Omen Offering */
			{ -75543, 114461, -4895, -1, 18039 },
			{ -75514, 114493, -4895, -1, 18040 }, // South West
			{ -75488, 114456, -4895, -1, 18040 },
			
			{ -75521, 113158, -4895, -1, 18039 },
			{ -75504, 113110, -4895, -1, 18040 }, // South East
			{ -75489, 113142, -4895, -1, 18040 },
			
			{ -76809, 113143, -4895, -1, 18042 },
			{ -76860, 113138, -4895, -1, 18043 }, // North East
			{ -76831, 113112, -4895, -1, 18044 },
			
			{ -76831, 114441, -4895, -1, 18045 },
			{ -76840, 114490, -4895, -1, 18047 }, // North West
			{ -76864, 114455, -4895, -1, 18048 },
			
			// Archers and Marksmen \\
			{ -75703, 113797, -4895, -1, 18041 },
			{ -76180, 114263, -4895, -1, 18041 },
			{ -76639, 113797, -4895, -1, 18046 },
			{ -76180, 113337, -4895, -1, 18046 }
		},
		{
			/* No Level Limit - Offering of Forbidden Path */
			{ -79576, 108881, -4895, -1, 18049 },
			{ -79592, 108835, -4895, -1, 18050 }, // South West
			{ -79614, 108871, -4895, -1, 18050 },
			
			{ -79586, 110171, -4895, -1, 18049 },
			{ -79589, 110216, -4895, -1, 18050 }, // South East
			{ -79620, 110177, -4895, -1, 18050 },
			
			{ -78825, 110182, -4895, -1, 18052 },
			{ -78238, 110182, -4895, -1, 18053 }, // North East
			{ -78266, 110218, -4895, -1, 18054 },
			
			{ -78275, 108883, -4895, -1, 18055 },
			{ -78267, 108839, -4895, -1, 18057 }, // North West
			{ -78241, 108871, -4895, -1, 18058 },
			
			// Archers and Marksmen \\
			{ -79394, 109538, -4895, -1, 18051 },
			{ -78929, 109992, -4895, -1, 18051 },
			{ -78454, 109538, -4895, -1, 18056 },
			{ -78929, 109053, -4895, -1, 18056 }
		}
	};
	
	protected static final int[][][] FESTIVAL_DUSK_PRIMARY_SPAWNS =
	{
		{
			/* Level 31 and Below - Offering of the Branded */
			{ -76542, 89653, -5151, -1, 18009 },
			{ -76509, 89637, -5151, -1, 18010 },
			{ -76548, 89614, -5151, -1, 18010 },
			
			{ -76539, 88326, -5151, -1, 18009 },
			{ -76512, 88289, -5151, -1, 18010 },
			{ -76546, 88287, -5151, -1, 18010 },
			
			{ -77879, 88308, -5151, -1, 18012 },
			{ -77886, 88310, -5151, -1, 18013 },
			{ -77879, 88278, -5151, -1, 18014 },
			
			{ -77857, 89605, -5151, -1, 18015 },
			{ -77858, 89658, -5151, -1, 18017 },
			{ -77891, 89633, -5151, -1, 18018 },
			
			// Archers and Marksmen \\
			{ -76728, 88962, -5151, -1, 18011 },
			{ -77194, 88494, -5151, -1, 18011 },
			{ -77660, 88896, -5151, -1, 18016 },
			{ -77195, 89438, -5151, -1, 18016 }
		},
		{
			/* Level 42 and Below - Apostate's Offering */
			{ -77585, 84650, -5151, -1, 18019 },
			{ -77628, 84643, -5151, -1, 18020 },
			{ -77607, 84613, -5151, -1, 18020 },
			
			{ -76603, 85946, -5151, -1, 18019 },
			{ -77606, 85994, -5151, -1, 18020 },
			{ -77638, 85959, -5151, -1, 18020 },
			
			{ -76301, 85960, -5151, -1, 18022 },
			{ -76257, 85972, -5151, -1, 18023 },
			{ -76286, 85992, -5151, -1, 18024 },
			
			{ -76281, 84667, -5151, -1, 18025 },
			{ -76291, 84611, -5151, -1, 18027 },
			{ -76257, 84616, -5151, -1, 18028 },
			
			// Archers and Marksmen \\
			{ -77419, 85307, -5151, -1, 18021 },
			{ -76952, 85768, -5151, -1, 18021 },
			{ -76477, 85312, -5151, -1, 18026 },
			{ -76942, 84832, -5151, -1, 18026 }
		},
		{
			/* Level 53 and Below - Witch's Offering */
			{ -74211, 86494, -5151, -1, 18029 },
			{ -74200, 86449, -5151, -1, 18030 },
			{ -74167, 86464, -5151, -1, 18030 },
			
			{ -75495, 86482, -5151, -1, 18029 },
			{ -75540, 86473, -5151, -1, 18030 },
			{ -75509, 86445, -5151, -1, 18030 },
			
			{ -75509, 87775, -5151, -1, 18032 },
			{ -75518, 87826, -5151, -1, 18033 },
			{ -75542, 87780, -5151, -1, 18034 },
			
			{ -74214, 87789, -5151, -1, 18035 },
			{ -74169, 87801, -5151, -1, 18037 },
			{ -74198, 87827, -5151, -1, 18038 },
			
			// Archers and Marksmen \\
			{ -75324, 87135, -5151, -1, 18031 },
			{ -74852, 87606, -5151, -1, 18031 },
			{ -74388, 87146, -5151, -1, 18036 },
			{ -74856, 86663, -5151, -1, 18036 }
		},
		{
			/* Level 64 and Below - Dark Omen Offering */
			{ -79560, 89007, -5151, -1, 18039 },
			{ -79521, 89016, -5151, -1, 18040 },
			{ -79544, 89047, -5151, -1, 18040 },
			
			{ -79552, 87717, -5151, -1, 18039 },
			{ -79552, 87673, -5151, -1, 18040 },
			{ -79510, 87702, -5151, -1, 18040 },
			
			{ -80866, 87719, -5151, -1, 18042 },
			{ -80897, 87689, -5151, -1, 18043 },
			{ -80850, 87685, -5151, -1, 18044 },
			
			{ -80848, 89013, -5151, -1, 18045 },
			{ -80887, 89051, -5151, -1, 18047 },
			{ -80891, 89004, -5151, -1, 18048 },
			
			// Archers and Marksmen \\
			{ -80205, 87895, -5151, -1, 18041 },
			{ -80674, 88350, -5151, -1, 18041 },
			{ -80209, 88833, -5151, -1, 18046 },
			{ -79743, 88364, -5151, -1, 18046 }
		},
		{
			/* No Level Limit - Offering of Forbidden Path */
			{ -80624, 84060, -5151, -1, 18049 },
			{ -80621, 84007, -5151, -1, 18050 },
			{ -80590, 84039, -5151, -1, 18050 },
			
			{ -80605, 85349, -5151, -1, 18049 },
			{ -80639, 85363, -5151, -1, 18050 },
			{ -80611, 85385, -5151, -1, 18050 },
			
			{ -79311, 85353, -5151, -1, 18052 },
			{ -79277, 85384, -5151, -1, 18053 },
			{ -79273, 85539, -5151, -1, 18054 },
			
			{ -79297, 84054, -5151, -1, 18055 },
			{ -79285, 84006, -5151, -1, 18057 },
			{ -79260, 84040, -5151, -1, 18058 },
			
			// Archers and Marksmen \\
			{ -79945, 85171, -5151, -1, 18051 },
			{ -79489, 84707, -5151, -1, 18051 },
			{ -79952, 84222, -5151, -1, 18056 },
			{ -80423, 84703, -5151, -1, 18056 }
		}
	};
	
	protected static final int[][][] FESTIVAL_DAWN_SECONDARY_SPAWNS =
	{
		{
			/* 31 and Below */
			{ -78757, 112834, -4895, -1, 18016 },
			{ -78581, 112834, -4895, -1, 18016 },
			{ -78822, 112526, -4895, -1, 18011 },
			
			{ -78822, 113702, -4895, -1, 18011 },
			{ -78822, 113874, -4895, -1, 18011 },
			{ -79524, 113546, -4895, -1, 18011 },
			
			{ -79693, 113546, -4895, -1, 18011 },
			{ -79858, 113546, -4895, -1, 18011 },
			{ -79545, 112757, -4895, -1, 18016 },
			{ -79545, 112586, -4895, -1, 18016 },
		},
		{
			/* 42 and Below */
			{ -75565, 110580, -4895, -1, 18026 },
			{ -75565, 110740, -4895, -1, 18026 },
			{ -75577, 109776, -4895, -1, 18021 },
			
			{ -75413, 109776, -4895, -1, 18021 },
			{ -75237, 109776, -4895, -1, 18021 },
			{ -76274, 109468, -4895, -1, 18021 },
			
			{ -76274, 109635, -4895, -1, 18021 },
			{ -76274, 109795, -4895, -1, 18021 },
			{ -76351, 110500, -4895, -1, 18056 },
			{ -76528, 110500, -4895, -1, 18056 },
		},
		{
			/* 53 and Below */
			{ -74191, 111527, -4895, -1, 18036 },
			{ -74191, 111362, -4895, -1, 18036 },
			{ -73495, 111611, -4895, -1, 18031 },
			
			{ -73327, 111611, -4895, -1, 18031 },
			{ -73154, 111611, -4895, -1, 18031 },
			{ -73473, 112301, -4895, -1, 18031 },
			
			{ -73473, 112475, -4895, -1, 18031 },
			{ -73473, 112649, -4895, -1, 18031 },
			{ -74270, 112326, -4895, -1, 18036 },
			{ -74443, 112326, -4895, -1, 18036 },
		},
		{
			/* 64 and Below */
			{ -75738, 113439, -4895, -1, 18046 },
			{ -75571, 113439, -4895, -1, 18046 },
			{ -75824, 114141, -4895, -1, 18041 },
			
			{ -75824, 114309, -4895, -1, 18041 },
			{ -75824, 114477, -4895, -1, 18041 },
			{ -76513, 114158, -4895, -1, 18041 },
			
			{ -76683, 114158, -4895, -1, 18041 },
			{ -76857, 114158, -4895, -1, 18041 },
			{ -76535, 113357, -4895, -1, 18056 },
			{ -76535, 113190, -4895, -1, 18056 },
		},
		{
			/* No Level Limit */
			{ -79350, 109894, -4895, -1, 18056 },
			{ -79534, 109894, -4895, -1, 18056 },
			{ -79285, 109187, -4895, -1, 18051 },
			
			{ -79285, 109019, -4895, -1, 18051 },
			{ -79285, 108860, -4895, -1, 18051 },
			{ -78587, 109172, -4895, -1, 18051 },
			
			{ -78415, 109172, -4895, -1, 18051 },
			{ -78249, 109172, -4895, -1, 18051 },
			{ -78575, 109961, -4895, -1, 18056 },
			{ -78575, 110130, -4895, -1, 18056 },
		}
	};
	
	protected static final int[][][] FESTIVAL_DUSK_SECONDARY_SPAWNS =
	{
		{
			/* 31 and Below */
			{ -76844, 89304, -5151, -1, 18011 },
			{ -76844, 89479, -5151, -1, 18011 },
			{ -76844, 89649, -5151, -1, 18011 },
			
			{ -77544, 89326, -5151, -1, 18011 },
			{ -77716, 89326, -5151, -1, 18011 },
			{ -77881, 89326, -5151, -1, 18011 },
			
			{ -77561, 88530, -5151, -1, 18016 },
			{ -77561, 88364, -5151, -1, 18016 },
			{ -76762, 88615, -5151, -1, 18016 },
			{ -76594, 88615, -5151, -1, 18016 },
		},
		{
			/* 42 and Below */
			{ -77307, 84969, -5151, -1, 18021 },
			{ -77307, 84795, -5151, -1, 18021 },
			{ -77307, 84623, -5151, -1, 18021 },
			
			{ -76614, 84944, -5151, -1, 18021 },
			{ -76433, 84944, -5151, -1, 18021 },
			{ -76251, 84944, -5151, -1, 18021 },
			
			{ -76594, 85745, -5151, -1, 18026 },
			{ -76594, 85910, -5151, -1, 18026 },
			{ -77384, 85660, -5151, -1, 18026 },
			{ -77555, 85660, -5151, -1, 18026 },
		},
		{
			/* 53 and Below */
			{ -74517, 86782, -5151, -1, 18031 },
			{ -74344, 86782, -5151, -1, 18031 },
			{ -74185, 86782, -5151, -1, 18031 },
			
			{ -74496, 87464, -5151, -1, 18031 },
			{ -74496, 87636, -5151, -1, 18031 },
			{ -74496, 87815, -5151, -1, 18031 },
			
			{ -75298, 87497, -5151, -1, 18036 },
			{ -75460, 87497, -5151, -1, 18036 },
			{ -75219, 86712, -5151, -1, 18036 },
			{ -75219, 86531, -5151, -1, 18036 },
		},
		{
			/* 64 and Below */
			{ -79851, 88703, -5151, -1, 18041 },
			{ -79851, 88868, -5151, -1, 18041 },
			{ -79851, 89040, -5151, -1, 18041 },
			
			{ -80548, 88722, -5151, -1, 18041 },
			{ -80711, 88722, -5151, -1, 18041 },
			{ -80883, 88722, -5151, -1, 18041 },
			
			{ -80565, 87916, -5151, -1, 18046 },
			{ -80565, 87752, -5151, -1, 18046 },
			{ -79779, 87996, -5151, -1, 18046 },
			{ -79613, 87996, -5151, -1, 18046 },
		},
		{
			/* No Level Limit */
			{ -79271, 84330, -5151, -1, 18051 },
			{ -79448, 84330, -5151, -1, 18051 },
			{ -79601, 84330, -5151, -1, 18051 },
			
			{ -80311, 84367, -5151, -1, 18051 },
			{ -80311, 84196, -5151, -1, 18051 },
			{ -80311, 84015, -5151, -1, 18051 },
			
			{ -80556, 85049, -5151, -1, 18056 },
			{ -80384, 85049, -5151, -1, 18056 },
			{ -79598, 85127, -5151, -1, 18056 },
			{ -79598, 85303, -5151, -1, 18056 },
		}
	};
	
	protected static final int[][][] FESTIVAL_DAWN_CHEST_SPAWNS =
	{
		{
			/* Level 31 and Below */
			{ -78999, 112957, -4927, -1, 18109 },
			{ -79153, 112873, -4927, -1, 18109 },
			{ -79256, 112873, -4927, -1, 18109 },
			
			{ -79368, 112957, -4927, -1, 18109 },
			{ -79481, 113124, -4927, -1, 18109 },
			{ -79481, 113275, -4927, -1, 18109 },
			
			{ -79364, 113398, -4927, -1, 18109 },
			{ -79213, 113500, -4927, -1, 18109 },
			{ -79099, 113500, -4927, -1, 18109 },
			
			{ -78960, 113398, -4927, -1, 18109 },
			{ -78882, 113235, -4927, -1, 18109 },
			{ -78882, 113099, -4927, -1, 18109 },
		},
		{
			/* Level 42 and Below */
			{ -76119, 110383, -4927, -1, 18110 },
			{ -75980, 110442, -4927, -1, 18110 },
			{ -75848, 110442, -4927, -1, 18110 },
			
			{ -75720, 110383, -4927, -1, 18110 },
			{ -75625, 110195, -4927, -1, 18110 },
			{ -75625, 110063, -4927, -1, 18110 },
			
			{ -75722, 109908, -4927, -1, 18110 },
			{ -75863, 109832, -4927, -1, 18110 },
			{ -75989, 109832, -4927, -1, 18110 },
			
			{ -76130, 109908, -4927, -1, 18110 },
			{ -76230, 110079, -4927, -1, 18110 },
			{ -76230, 110215, -4927, -1, 18110 },
		},
		{
			/* Level 53 and Below */
			{ -74055, 111781, -4927, -1, 18111 },
			{ -74144, 111938, -4927, -1, 18111 },
			{ -74144, 112075, -4927, -1, 18111 },
			
			{ -74055, 112173, -4927, -1, 18111 },
			{ -73885, 112289, -4927, -1, 18111 },
			{ -73756, 112289, -4927, -1, 18111 },
			
			{ -73574, 112141, -4927, -1, 18111 },
			{ -73511, 112040, -4927, -1, 18111 },
			{ -73511, 111912, -4927, -1, 18111 },
			
			{ -73574, 111772, -4927, -1, 18111 },
			{ -73767, 111669, -4927, -1, 18111 },
			{ -73899, 111669, -4927, -1, 18111 },
		},
		{
			/* Level 64 and Below */
			{ -76008, 113566, -4927, -1, 18112 },
			{ -76159, 113485, -4927, -1, 18112 },
			{ -76267, 113485, -4927, -1, 18112 },
			
			{ -76386, 113566, -4927, -1, 18112 },
			{ -76482, 113748, -4927, -1, 18112 },
			{ -76482, 113885, -4927, -1, 18112 },
			
			{ -76371, 114029, -4927, -1, 18112 },
			{ -76220, 114118, -4927, -1, 18112 },
			{ -76092, 114118, -4927, -1, 18112 },
			
			{ -75975, 114029, -4927, -1, 18112 },
			{ -75861, 113851, -4927, -1, 18112 },
			{ -75861, 113713, -4927, -1, 18112 },
		},
		{
			/* No Level Limit */
			{ -79100, 109782, -4927, -1, 18113 },
			{ -78962, 109853, -4927, -1, 18113 },
			{ -78851, 109853, -4927, -1, 18113 },
			
			{ -78721, 109782, -4927, -1, 18113 },
			{ -78615, 109596, -4927, -1, 18113 },
			{ -78615, 109453, -4927, -1, 18113 },
			
			{ -78746, 109300, -4927, -1, 18113 },
			{ -78881, 109203, -4927, -1, 18113 },
			{ -79027, 109203, -4927, -1, 18113 },
			
			{ -79159, 109300, -4927, -1, 18113 },
			{ -79240, 109480, -4927, -1, 18113 },
			{ -79240, 109615, -4927, -1, 18113 },
		}
	};
	
	protected static final int[][][] FESTIVAL_DUSK_CHEST_SPAWNS =
	{
		{
			/* Level 31 and Below */
			{ -77016, 88726, -5183, -1, 18114 },
			{ -77136, 88646, -5183, -1, 18114 },
			{ -77247, 88646, -5183, -1, 18114 },
			
			{ -77380, 88726, -5183, -1, 18114 },
			{ -77512, 88883, -5183, -1, 18114 },
			{ -77512, 89053, -5183, -1, 18114 },
			
			{ -77378, 89287, -5183, -1, 18114 },
			{ -77254, 89238, -5183, -1, 18114 },
			{ -77095, 89238, -5183, -1, 18114 },
			
			{ -76996, 89287, -5183, -1, 18114 },
			{ -76901, 89025, -5183, -1, 18114 },
			{ -76901, 88891, -5183, -1, 18114 },
		},
		{
			/* Level 42 and Below */
			{ -77128, 85553, -5183, -1, 18115 },
			{ -77036, 85594, -5183, -1, 18115 },
			{ -76919, 85594, -5183, -1, 18115 },
			
			{ -76755, 85553, -5183, -1, 18115 },
			{ -76635, 85392, -5183, -1, 18115 },
			{ -76635, 85216, -5183, -1, 18115 },
			
			{ -76761, 85025, -5183, -1, 18115 },
			{ -76908, 85004, -5183, -1, 18115 },
			{ -77041, 85004, -5183, -1, 18115 },
			
			{ -77138, 85025, -5183, -1, 18115 },
			{ -77268, 85219, -5183, -1, 18115 },
			{ -77268, 85410, -5183, -1, 18115 },
		},
		{
			/* Level 53 and Below */
			{ -75150, 87303, -5183, -1, 18116 },
			{ -75150, 87175, -5183, -1, 18116 },
			{ -75150, 87175, -5183, -1, 18116 },
			
			{ -75150, 87303, -5183, -1, 18116 },
			{ -74943, 87433, -5183, -1, 18116 },
			{ -74767, 87433, -5183, -1, 18116 },
			
			{ -74556, 87306, -5183, -1, 18116 },
			{ -74556, 87184, -5183, -1, 18116 },
			{ -74556, 87184, -5183, -1, 18116 },
			
			{ -74556, 87306, -5183, -1, 18116 },
			{ -74757, 86830, -5183, -1, 18116 },
			{ -74927, 86830, -5183, -1, 18116 },
		},
		{
			/* Level 64 and Below */
			{ -80010, 88128, -5183, -1, 18117 },
			{ -80113, 88066, -5183, -1, 18117 },
			{ -80220, 88066, -5183, -1, 18117 },
			
			{ -80359, 88128, -5183, -1, 18117 },
			{ -80467, 88267, -5183, -1, 18117 },
			{ -80467, 88436, -5183, -1, 18117 },
			
			{ -80381, 88639, -5183, -1, 18117 },
			{ -80278, 88577, -5183, -1, 18117 },
			{ -80142, 88577, -5183, -1, 18117 },
			
			{ -80028, 88639, -5183, -1, 18117 },
			{ -79915, 88466, -5183, -1, 18117 },
			{ -79915, 88322, -5183, -1, 18117 },
		},
		{
			/* No Level Limit */
			{ -80153, 84947, -5183, -1, 18118 },
			{ -80003, 84962, -5183, -1, 18118 },
			{ -79848, 84962, -5183, -1, 18118 },
			
			{ -79742, 84947, -5183, -1, 18118 },
			{ -79668, 84772, -5183, -1, 18118 },
			{ -79668, 84619, -5183, -1, 18118 },
			
			{ -79772, 84471, -5183, -1, 18118 },
			{ -79888, 84414, -5183, -1, 18118 },
			{ -80023, 84414, -5183, -1, 18118 },
			
			{ -80166, 84471, -5183, -1, 18118 },
			{ -80253, 84600, -5183, -1, 18118 },
			{ -80253, 84780, -5183, -1, 18118 },
		}
	};
	// @formatter:on
	
	private FestivalManager _managerInstance;
	protected ScheduledFuture<?> _managerScheduledTask;
	
	protected int _signsCycle = SevenSigns.getInstance().getCurrentCycle();
	protected boolean _festivalInitialized;
	protected boolean _festivalInProgress;
	
	protected List<Integer> _accumulatedBonuses = new ArrayList<>(); // The total bonus available (in Ancient Adena)
	
	private final Map<Integer, Long> _dawnFestivalScores = new HashMap<>();
	private final Map<Integer, Long> _duskFestivalScores = new HashMap<>();
	
	private final Map<Integer, L2Party> _festivalRooms = new HashMap<>();
	
	private final Map<Integer, Map<Integer, StatsSet>> _festivalData = new HashMap<>();
	
	protected SevenSignsFestival()
	{
		restoreFestivalData();
		
		if (SevenSigns.getInstance().isSealValidationPeriod())
		{
			LOG.info("Loaded SevenSignsFestival bypassed due to Seal Validation in effect.");
			return;
		}
		startFestivalManager();
	}
	
	/**
	 * Returns true if the monster ID given is of an archer/marksman type.
	 * @param npcId
	 * @return boolean isArcher
	 */
	protected static boolean isFestivalArcher(int npcId)
	{
		if ((npcId < 18009) || (npcId > 18108))
		{
			return false;
		}
		
		var identifier = npcId % 10;
		return ((identifier == 4) || (identifier == 9));
	}
	
	/**
	 * Returns true if the monster ID given is a festival chest.
	 * @param npcId
	 * @return boolean isChest
	 */
	protected static boolean isFestivalChest(int npcId)
	{
		return ((npcId < 18109) || (npcId > 18118));
	}
	
	/**
	 * Primarily used to terminate the Festival Manager, when the Seven Signs period changes.
	 * @return ScheduledFuture festManagerScheduler
	 */
	protected final ScheduledFuture<?> getFestivalManagerSchedule()
	{
		if (_managerScheduledTask == null)
		{
			startFestivalManager();
		}
		
		return _managerScheduledTask;
	}
	
	/**
	 * Used to start the Festival Manager, if the current period is not Seal Validation.
	 */
	protected void startFestivalManager()
	{
		// Start the Festival Manager for the first time after the server has started
		// at the specified time, then invoke it automatically after every cycle.
		if (_managerInstance != null)
		{
			return; // already started
		}
		_managerInstance = new FestivalManager();
		_managerScheduledTask = ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(_managerInstance, getStartTime(), 1200000);
		
		LOG.info("The next Festival of Darkness game begins at {}", getStartDate(getStartTime()));
	}
	
	private long getStartTime()
	{
		var cal = Calendar.getInstance();
		var i0 = cal.get(Calendar.MINUTE);
		
		int i1;
		if (i0 > 39)
		{
			i1 = 60 - i0;
		}
		else
		{
			if (i0 > 19)
			{
				i1 = 40 - i0;
			}
			else
			{
				i1 = 20 - i0;
			}
		}
		
		cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE) + (i1));
		cal.set(Calendar.SECOND, 0);
		
		return cal.getTimeInMillis() - System.currentTimeMillis();
	}
	
	private String getStartDate(long millis)
	{
		var obj = new SimpleDateFormat("HH:mm:ss");
		var cal = Calendar.getInstance();
		cal.setTimeInMillis(millis + System.currentTimeMillis());
		return obj.format(cal.getTime());
	}
	
	/**
	 * Restores saved festival data, basic settings from the properties file and past high score data from the database.
	 */
	protected void restoreFestivalData()
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var s = con.createStatement();
			var rs = s.executeQuery("SELECT festivalId, cabal, cycle, date, score, members, members_reward_flags FROM seven_signs_festival"))
		{
			while (rs.next())
			{
				var ssqCycle = rs.getInt("cycle");
				var festivalId = rs.getInt("festivalId");
				var cabal = rs.getString("cabal");
				
				var festivalDat = new StatsSet();
				festivalDat.set("festivalId", festivalId);
				festivalDat.set("cabal", cabal);
				festivalDat.set("cycle", ssqCycle);
				festivalDat.set("date", rs.getString("date"));
				festivalDat.set("score", rs.getInt("score"));
				festivalDat.set("members", rs.getString("members"));
				festivalDat.set("members_reward_flags", rs.getString("members_reward_flags"));
				
				if (cabal.equals("dawn"))
				{
					festivalId += FESTIVAL_COUNT;
				}
				
				var tempData = _festivalData.getOrDefault(ssqCycle, new HashMap<>());
				tempData.put(festivalId, festivalDat);
				_festivalData.put(ssqCycle, tempData);
			}
		}
		catch (Exception ex)
		{
			LOG.error("Failed to load configuration!", ex);
		}
		
		var sb = new StringBuilder("SELECT ");
		
		for (int i = 0; i < (FESTIVAL_COUNT - 1); i++)
		{
			StringUtil.append(sb, "accumulated_bonus", i, ", ");
		}
		StringUtil.append(sb, "accumulated_bonus", FESTIVAL_COUNT - 1, " ", "FROM seven_signs_status WHERE id=0");
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var s = con.createStatement();
			var rs = s.executeQuery(sb.toString()))
		{
			while (rs.next())
			{
				for (int i = 0; i < FESTIVAL_COUNT; i++)
				{
					_accumulatedBonuses.add(i, rs.getInt("accumulated_bonus" + i));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.error("Failed to load configuration!", ex);
		}
	}
	
	public void saveFestivalData(boolean updateSettings)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("REPLACE INTO seven_signs_festival (festivalId, cabal, cycle, date, score, members, members_reward_flags) VALUES (?,?,?,?,?,?,?)"))
		{
			for (var currCycleData : _festivalData.values())
			{
				for (var festivalDat : currCycleData.values())
				{
					ps.setInt(1, festivalDat.getInt("festivalId"));
					ps.setString(2, festivalDat.getString("cabal"));
					ps.setInt(3, festivalDat.getInt("cycle"));
					ps.setLong(4, Long.parseLong(festivalDat.getString("date")));
					ps.setInt(5, festivalDat.getInt("score"));
					ps.setString(6, festivalDat.getString("members"));
					ps.setString(7, festivalDat.getString("members_reward_flags"));
					ps.addBatch();
				}
			}
			ps.executeBatch();
		}
		catch (Exception ex)
		{
			LOG.error("Failed to save configuration!", ex);
		}
		
		// Updates Seven Signs DB data also, so call only if really necessary.
		if (updateSettings)
		{
			SevenSigns.getInstance().saveSevenSignsStatus();
		}
	}
	
	/**
	 * If a clan member is a member of the highest-ranked party in the Festival of Darkness, 100 points are added per member
	 */
	protected void rewardHighestRanked()
	{
		String[] partyMembers;
		var overallData = getOverallHighestScoreData(FESTIVAL_LEVEL_MAX_31);
		if (overallData != null)
		{
			partyMembers = overallData.getString("members").split(",");
			for (var partyMemberName : partyMembers)
			{
				addReputationPointsForPartyMemberClan(partyMemberName);
			}
		}
		
		overallData = getOverallHighestScoreData(FESTIVAL_LEVEL_MAX_42);
		if (overallData != null)
		{
			partyMembers = overallData.getString("members").split(",");
			for (var partyMemberName : partyMembers)
			{
				addReputationPointsForPartyMemberClan(partyMemberName);
			}
		}
		
		overallData = getOverallHighestScoreData(FESTIVAL_LEVEL_MAX_53);
		if (overallData != null)
		{
			partyMembers = overallData.getString("members").split(",");
			for (var partyMemberName : partyMembers)
			{
				addReputationPointsForPartyMemberClan(partyMemberName);
			}
		}
		
		overallData = getOverallHighestScoreData(FESTIVAL_LEVEL_MAX_64);
		if (overallData != null)
		{
			partyMembers = overallData.getString("members").split(",");
			for (var partyMemberName : partyMembers)
			{
				addReputationPointsForPartyMemberClan(partyMemberName);
			}
		}
		
		overallData = getOverallHighestScoreData(FESTIVAL_LEVEL_MAX_NONE);
		if (overallData != null)
		{
			partyMembers = overallData.getString("members").split(",");
			for (var partyMemberName : partyMembers)
			{
				addReputationPointsForPartyMemberClan(partyMemberName);
			}
		}
	}
	
	private void addReputationPointsForPartyMemberClan(String partyMemberName)
	{
		var player = L2World.getInstance().getPlayer(partyMemberName);
		if (player != null)
		{
			if (player.getClan() != null)
			{
				player.getClan().incReputation(FeatureConfig.FESTIVAL_WIN_POINTS, true);
				var sm = SystemMessage.getSystemMessage(SystemMessageId.CLAN_MEMBER_C1_WAS_IN_HIGHEST_RANKED_PARTY_IN_FESTIVAL_OF_DARKNESS_AND_GAINED_S2_REPUTATION);
				sm.addString(partyMemberName);
				sm.addInt((Math.round(FeatureConfig.FESTIVAL_WIN_POINTS * RatesConfig.RATE_CLAN_REP_SCORE)));
				player.getClan().broadcastToOnlineMembers(sm);
			}
		}
		else
		{
			try (var con = ConnectionFactory.getInstance().getConnection();
				var ps = con.prepareStatement(GET_CLAN_NAME))
			{
				ps.setString(1, partyMemberName);
				try (var rs = ps.executeQuery())
				{
					if (rs.next())
					{
						var clanName = rs.getString("clan_name");
						if (clanName != null)
						{
							var clan = ClanTable.getInstance().getClanByName(clanName);
							if (clan != null)
							{
								clan.incReputation(FeatureConfig.FESTIVAL_WIN_POINTS, true);
								var sm = SystemMessage.getSystemMessage(SystemMessageId.CLAN_MEMBER_C1_WAS_IN_HIGHEST_RANKED_PARTY_IN_FESTIVAL_OF_DARKNESS_AND_GAINED_S2_REPUTATION);
								sm.addString(partyMemberName);
								sm.addInt((Math.round(FeatureConfig.FESTIVAL_WIN_POINTS * RatesConfig.RATE_CLAN_REP_SCORE)));
								clan.broadcastToOnlineMembers(sm);
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				LOG.warn("Could not get clan name of {}!", partyMemberName, ex);
			}
		}
	}
	
	/**
	 * Used to reset all festival data at the beginning of a new quest event period.
	 * @param updateSettings
	 */
	protected void resetFestivalData(boolean updateSettings)
	{
		_signsCycle = SevenSigns.getInstance().getCurrentCycle();
		
		// Set all accumulated bonuses back to 0.
		for (int i = 0; i < FESTIVAL_COUNT; i++)
		{
			_accumulatedBonuses.set(i, 0);
		}
		
		_festivalRooms.clear();
		_dawnFestivalScores.clear();
		_duskFestivalScores.clear();
		
		// Set up a new data set for the current cycle of festivals
		Map<Integer, StatsSet> newData = new HashMap<>();
		
		for (var i = 0; i < (FESTIVAL_COUNT * 2); i++)
		{
			var festivalId = i;
			
			if (i >= FESTIVAL_COUNT)
			{
				festivalId -= FESTIVAL_COUNT;
			}
			
			// Create a new StatsSet with "default" data for Dusk
			var tempStats = new StatsSet();
			tempStats.set("festivalId", festivalId);
			tempStats.set("cycle", _signsCycle);
			tempStats.set("date", "0");
			tempStats.set("score", 0);
			tempStats.set("members", "");
			tempStats.set("members_reward_flags", "");
			
			if (i >= FESTIVAL_COUNT)
			{
				tempStats.set("cabal", SevenSigns.getCabalShortName(SevenSigns.CABAL_DAWN));
			}
			else
			{
				tempStats.set("cabal", SevenSigns.getCabalShortName(SevenSigns.CABAL_DUSK));
			}
			
			newData.put(i, tempStats);
		}
		
		// Add the newly created cycle data to the existing festival data, and
		// subsequently save it to the database.
		_festivalData.put(_signsCycle, newData);
		
		saveFestivalData(updateSettings);
		
		LOG.info("Reinitialized engine for next competition period.");
	}
	
	public final boolean isFestivalInitialized()
	{
		return _festivalInitialized;
	}
	
	public final boolean isFestivalInProgress()
	{
		return _festivalInProgress;
	}
	
	public boolean isWinnerOfTimeAttackEvent(L2PcInstance player, int roomId)
	{
		var data = getHighestScoreData(SevenSigns.getInstance().getPlayerCabal(player.getObjectId()), roomId);
		var members = data.getString("members").split(",");
		
		for (var name : members)
		{
			if (name.equals(player.getName()))
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean getTimeAttackRewardFlag(L2PcInstance player, int roomId)
	{
		var data = getHighestScoreData(SevenSigns.getInstance().getPlayerCabal(player.getObjectId()), roomId);
		var members = data.getString("members_reward_flags").split(",");
		
		for (var name : members)
		{
			if (name.equals(player.getName()))
			{
				return true;
			}
		}
		return false;
	}
	
	public void giveTimeAttackReward(L2PcInstance player, int roomId, int amount)
	{
		var data = getHighestScoreData(SevenSigns.getInstance().getPlayerCabal(player.getObjectId()), roomId);
		var members = data.getString("members_reward_flags").split(",");
		var count = data.getString("members_reward_flags").isEmpty() ? 0 : members.length;
		
		List<String> list = new ArrayList<>(count);
		if (count > 0)
		{
			for (var member : members)
			{
				list.add(member);
			}
		}
		list.add(player.getName());
		
		data.set("members_reward_flags", String.join(",", list));
		
		player.addAncientAdena("SevenSigns", amount, player, true);
		
		saveFestivalData(true);
	}
	
	public void addPartyToEventRoom(int type, int room, L2Party party)
	{
		var loc = type == SevenSigns.CABAL_DUSK ? FESTIVAL_DUSK_PLAYER_SPAWNS[room] : FESTIVAL_DAWN_PLAYER_SPAWNS[room];
		var leaderLoc = party.getLeader().getLocation();
		for (var pc : party.getMembers())
		{
			var distance = Util.calculateDistance(leaderLoc, pc, true, false);
			if (distance <= MAX_DISTANCE)
			{
				pc.teleToLocation(new Location(loc[0], loc[1], loc[2]));
			}
		}
		_festivalRooms.put(type == SevenSigns.CABAL_DUSK ? 10 + room : 20 + room, party);
	}
	
	public boolean checkEventRoom(int type, int room)
	{
		room += (type == SevenSigns.CABAL_DUSK) ? 10 : 20;
		if (_festivalRooms.get(room) != null)
		{
			return false;
		}
		return true;
	}
	
	public final int getHighestScore(int oracle, int festivalId)
	{
		return getHighestScoreData(oracle, festivalId).getInt("score");
	}
	
	/**
	 * Returns a stats set containing the highest score <b>this cycle</b> for the the specified cabal and associated festival ID.
	 * @param oracle
	 * @param festivalId
	 * @return StatsSet festivalDat
	 */
	public final StatsSet getHighestScoreData(int oracle, int festivalId)
	{
		var offsetId = festivalId;
		
		if (oracle == SevenSigns.CABAL_DAWN)
		{
			offsetId += 5;
		}
		
		// Attempt to retrieve existing score data (if found), otherwise create a
		// new blank data set and display a console warning.
		StatsSet currData = null;
		
		try
		{
			currData = _festivalData.get(_signsCycle).get(offsetId);
		}
		catch (Exception e)
		{
			currData = new StatsSet();
			currData.set("score", 0);
			currData.set("members", "");
			currData.set("members_reward_flags", "");
		}
		return currData;
	}
	
	/**
	 * Returns a stats set containing the highest ever recorded score data for the specified festival.
	 * @param festivalId
	 * @return StatsSet result
	 */
	public final StatsSet getOverallHighestScoreData(int festivalId)
	{
		StatsSet result = null;
		var highestScore = 0;
		
		for (var currCycleData : _festivalData.values())
		{
			for (var currFestData : currCycleData.values())
			{
				var currFestID = currFestData.getInt("festivalId");
				var festivalScore = currFestData.getInt("score");
				
				if (currFestID != festivalId)
				{
					continue;
				}
				
				if (festivalScore > highestScore)
				{
					highestScore = festivalScore;
					result = currFestData;
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Set the final score details for the last participants of the specified festival data. Returns <b>true</b> if the score is higher than that previously recorded <b>this cycle</b>.
	 * @param player
	 * @param oracle
	 * @param festivalId
	 * @param offeringScore
	 * @return boolean isHighestScore
	 */
	public boolean addTimeAttackRecord(int oracle, int festivalId, L2Party party, long offeringScore)
	{
		var currDawnHighScore = getHighestScore(SevenSigns.CABAL_DAWN, festivalId);
		var currDuskHighScore = getHighestScore(SevenSigns.CABAL_DUSK, festivalId);
		
		var thisCabalHighScore = 0;
		var otherCabalHighScore = 0;
		
		if (oracle == SevenSigns.CABAL_DAWN)
		{
			thisCabalHighScore = currDawnHighScore;
			otherCabalHighScore = currDuskHighScore;
			
			_dawnFestivalScores.put(festivalId, offeringScore);
		}
		else
		{
			thisCabalHighScore = currDuskHighScore;
			otherCabalHighScore = currDawnHighScore;
			
			_duskFestivalScores.put(festivalId, offeringScore);
		}
		
		var currFestData = getHighestScoreData(oracle, festivalId);
		
		// Check if this is the highest score for this level range so far for the player's cabal.
		if (offeringScore > thisCabalHighScore)
		{
			List<String> partyMembers = new ArrayList<>(party.getMemberCount());
			
			// Record a string list of the party members involved.
			for (var pc : party.getMembers())
			{
				partyMembers.add(pc.getName());
			}
			
			// Update the highest scores and party list.
			currFestData.set("date", String.valueOf(System.currentTimeMillis()));
			currFestData.set("score", offeringScore);
			currFestData.set("members", String.join(",", partyMembers));
			
			// Only add the score to the cabal's overall if it's higher than the other cabal's score.
			if (offeringScore > otherCabalHighScore)
			{
				var contribPoints = FESTIVAL_LEVEL_SCORES[festivalId];
				
				// Give this cabal the festival points, while deducting them from the other.
				SevenSigns.getInstance().addFestivalScore(oracle, contribPoints);
			}
			saveFestivalData(true);
			return true;
		}
		return false;
	}
	
	public final int getAccumulatedBonus(int festivalId)
	{
		return _accumulatedBonuses.get(festivalId);
	}
	
	public void addTimeAttackFee(int festivalId, int amount)
	{
		var newTotalBonus = _accumulatedBonuses.get(festivalId) + amount;
		_accumulatedBonuses.set(festivalId, newTotalBonus);
	}
	
	/**
	 * Basically a wrapper-call to signal to increase the challenge of the specified festival.
	 * @param oracle
	 * @param festivalId
	 * @return boolean isChalIncreased
	 */
	public final boolean increaseChallenge(int oracle, int festivalId)
	{
		var festivalInst = _managerInstance.getFestivalInstance(oracle, festivalId);
		
		return festivalInst.increaseChallenge();
	}
	
	/**
	 * The FestivalManager class is the main runner of all the festivals. It is used for easier integration and management of all running festivals.
	 * @author Tempy
	 */
	public class FestivalManager implements Runnable
	{
		protected Map<Integer, L2DarknessFestival> _festivalInstances;
		
		public FestivalManager()
		{
			_festivalInstances = new HashMap<>();
		}
		
		@Override
		public synchronized void run()
		{
			try
			{
				// The manager shouldn't be running if Seal Validation is in effect.
				if (SevenSigns.getInstance().isSealValidationPeriod())
				{
					return;
				}
				
				// If the next period is due to start before the end of this
				// festival cycle, then don't run it.
				if (SevenSigns.getInstance().getMilliToPeriodChange() < FeatureConfig.ALT_FESTIVAL_CYCLE_LENGTH)
				{
					return;
				}
				
				// Get rid of random monsters that avoided deletion after last festival
				for (var festivalInst : _festivalInstances.values())
				{
					festivalInst.unspawnMobs();
				}
				
				// Create the instances for the festivals in both Oracles
				for (var i = 0; i < FESTIVAL_COUNT; i++)
				{
					_festivalInstances.put(10 + i, new L2DarknessFestival(SevenSigns.CABAL_DUSK, i));
					_festivalInstances.put(20 + i, new L2DarknessFestival(SevenSigns.CABAL_DAWN, i));
				}
				
				// Prevent future signups while festival is in progress.
				_festivalInitialized = true;
				
				// Set the festival timer to 0, as it is just beginning.
				var elapsedTime = 0;
				
				// Participants can now opt to increase the challenge, if desired.
				_festivalInProgress = true;
				
				/* PROPOGATION */
				// Sequentially set all festivals to begin
				for (var festivalInst : _festivalInstances.values())
				{
					festivalInst.festivalStart();
				}
				
				// After a short time period, move all idle spawns to the center of the arena.
				try
				{
					wait(FeatureConfig.ALT_FESTIVAL_FIRST_SWARM - FeatureConfig.ALT_FESTIVAL_FIRST_SPAWN);
				}
				catch (InterruptedException e)
				{
				}
				
				elapsedTime += FeatureConfig.ALT_FESTIVAL_FIRST_SWARM - FeatureConfig.ALT_FESTIVAL_FIRST_SPAWN;
				
				for (var festivalInst : _festivalInstances.values())
				{
					festivalInst.moveMonstersToCenter();
				}
				
				// Stand by until the time comes for the second spawn.
				try
				{
					wait(FeatureConfig.ALT_FESTIVAL_SECOND_SPAWN - FeatureConfig.ALT_FESTIVAL_FIRST_SWARM);
				}
				catch (InterruptedException e)
				{
				}
				
				// Spawn an extra set of monsters (archers) on the free platforms with
				// a faster respawn when killed.
				for (var festivalInst : _festivalInstances.values())
				{
					festivalInst.spawnFestivalMonsters(FESTIVAL_DEFAULT_RESPAWN / 2, 2);
				}
				
				elapsedTime += FeatureConfig.ALT_FESTIVAL_SECOND_SPAWN - FeatureConfig.ALT_FESTIVAL_FIRST_SWARM;
				
				// After another short time period, again move all idle spawns to the center of the arena.
				try
				{
					wait(FeatureConfig.ALT_FESTIVAL_SECOND_SWARM - FeatureConfig.ALT_FESTIVAL_SECOND_SPAWN);
				}
				catch (InterruptedException e)
				{
				}
				
				for (var festivalInst : _festivalInstances.values())
				{
					festivalInst.moveMonstersToCenter();
				}
				
				elapsedTime += FeatureConfig.ALT_FESTIVAL_SECOND_SWARM - FeatureConfig.ALT_FESTIVAL_SECOND_SPAWN;
				
				// Stand by until the time comes for the chests to be spawned.
				try
				{
					wait(FeatureConfig.ALT_FESTIVAL_CHEST_SPAWN - FeatureConfig.ALT_FESTIVAL_SECOND_SWARM);
				}
				catch (InterruptedException e)
				{
				}
				
				// Spawn the festival chests, which enable the team to gain greater rewards
				// for each chest they kill.
				for (var festivalInst : _festivalInstances.values())
				{
					festivalInst.spawnFestivalMonsters(FESTIVAL_DEFAULT_RESPAWN, 3);
				}
				
				elapsedTime += FeatureConfig.ALT_FESTIVAL_CHEST_SPAWN - FeatureConfig.ALT_FESTIVAL_SECOND_SWARM;
				
				// Stand by and wait until it's time to end the festival.
				try
				{
					wait(FeatureConfig.ALT_FESTIVAL_LENGTH - elapsedTime);
				}
				catch (InterruptedException e)
				{
				}
				
				// Participants can no longer opt to increase the challenge, as the festival will soon close.
				_festivalInProgress = false;
				// Allow signups for the next festival cycle.
				_festivalInitialized = false;
				
				// wait for the next game to start
				try
				{
					wait(FeatureConfig.ALT_FESTIVAL_MANAGER_START);
				}
				catch (InterruptedException e)
				{
				}
			}
			catch (Exception e)
			{
				LOG.warn("Could not run featival task!", e);
			}
		}
		
		/**
		 * Returns the running instance of a festival for the given Oracle and festivalID. <BR>
		 * A <B>null</B> value is returned if there are no participants in that festival.
		 * @param oracle
		 * @param festivalId
		 * @return L2DarknessFestival festivalInst
		 */
		public final L2DarknessFestival getFestivalInstance(int oracle, int festivalId)
		{
			festivalId += (oracle == SevenSigns.CABAL_DUSK) ? 10 : 20; // Compute the offset if a Dusk instance is required. ID: 0 1 2 3 4 Dusk 1:10,11,12,13,14 Dawn 2:20,21,22,23,24
			return _festivalInstances.get(festivalId);
		}
	}
	
	/**
	 * Each running festival is represented by an L2DarknessFestival class. It contains all the spawn information and data for the running festival. All festivals are managed by the FestivalManager class, which must be initialized first.
	 * @author Tempy
	 */
	public class L2DarknessFestival
	{
		protected final int _cabal;
		protected final int _levelRange;
		protected boolean _challengeIncreased;
		
		private FestivalSpawn _startLocation;
		List<L2FestivalMonsterInstance> _npcInsts = new ArrayList<>();
		
		protected L2DarknessFestival(int cabal, int levelRange)
		{
			_cabal = cabal;
			_levelRange = levelRange;
			
			if (cabal == SevenSigns.CABAL_DAWN)
			{
				_startLocation = new FestivalSpawn(FESTIVAL_DAWN_PLAYER_SPAWNS[levelRange]);
			}
			else
			{
				_startLocation = new FestivalSpawn(FESTIVAL_DUSK_PLAYER_SPAWNS[levelRange]);
			}
		}
		
		protected void festivalStart()
		{
			spawnFestivalMonsters(FESTIVAL_DEFAULT_RESPAWN, 0);
		}
		
		protected void moveMonstersToCenter()
		{
			for (var festivalMob : _npcInsts)
			{
				if (festivalMob.isDead())
				{
					continue;
				}
				
				// Only move monsters that are idle or doing their usual functions.
				var currIntention = festivalMob.getAI().getIntention();
				
				if ((currIntention != CtrlIntention.AI_INTENTION_IDLE) && (currIntention != CtrlIntention.AI_INTENTION_ACTIVE))
				{
					continue;
				}
				
				var x = _startLocation.getX();
				var y = _startLocation.getY();
				
				festivalMob.setRunning();
				festivalMob.getAI().setIntention(CtrlIntention.AI_INTENTION_MOVE_TO, new Location(x, y, _startLocation.getZ(), Rnd.nextInt(65536)));
			}
		}
		
		/**
		 * Used to spawn monsters unique to the festival. <BR>
		 * Valid SpawnTypes:<BR>
		 * 0 - All Primary Monsters (starting monsters) <BR>
		 * 1 - Same as 0, but without archers/marksmen. (used for challenge increase) <BR>
		 * 2 - Secondary Monsters (archers) <BR>
		 * 3 - Festival Chests
		 * @param respawnDelay
		 * @param spawnType
		 */
		protected void spawnFestivalMonsters(int respawnDelay, int spawnType)
		{
			int[][] _npcSpawns = null;
			
			switch (spawnType)
			{
				case 0:
				case 1:
					_npcSpawns = (_cabal == SevenSigns.CABAL_DAWN) ? FESTIVAL_DAWN_PRIMARY_SPAWNS[_levelRange] : FESTIVAL_DUSK_PRIMARY_SPAWNS[_levelRange];
					break;
				case 2:
					_npcSpawns = (_cabal == SevenSigns.CABAL_DAWN) ? FESTIVAL_DAWN_SECONDARY_SPAWNS[_levelRange] : FESTIVAL_DUSK_SECONDARY_SPAWNS[_levelRange];
					break;
				case 3:
					_npcSpawns = (_cabal == SevenSigns.CABAL_DAWN) ? FESTIVAL_DAWN_CHEST_SPAWNS[_levelRange] : FESTIVAL_DUSK_CHEST_SPAWNS[_levelRange];
					break;
				default:
					return;
			}
			
			for (var _npcSpawn : _npcSpawns)
			{
				var currSpawn = new FestivalSpawn(_npcSpawn);
				
				// Only spawn archers/marksmen if specified to do so.
				if ((spawnType == 1) && isFestivalArcher(currSpawn.getNpcId()))
				{
					continue;
				}
				
				try
				{
					var npcSpawn = new L2Spawn(currSpawn.getNpcId());
					
					npcSpawn.setX(currSpawn.getX());
					npcSpawn.setY(currSpawn.getY());
					npcSpawn.setZ(currSpawn.getZ());
					npcSpawn.setHeading(Rnd.nextInt(65536));
					npcSpawn.setAmount(1);
					npcSpawn.setRespawnDelay(respawnDelay);
					
					// Needed as doSpawn() is required to be called also for the NpcInstance it returns.
					npcSpawn.startRespawn();
					
					SpawnData.getInstance().addNewSpawn(npcSpawn, false);
					var festivalMob = (L2FestivalMonsterInstance) npcSpawn.doSpawn();
					
					_npcInsts.add(festivalMob);
				}
				catch (Exception ex)
				{
					LOG.warn("Error while spawning NPC ID {}!", currSpawn.getNpcId(), ex);
				}
			}
		}
		
		protected boolean increaseChallenge()
		{
			if (_challengeIncreased)
			{
				return false;
			}
			
			// Set this flag to true to make sure that this can only be done once.
			_challengeIncreased = true;
			
			// Spawn more festival monsters, but this time with a twist.
			spawnFestivalMonsters(FESTIVAL_DEFAULT_RESPAWN, 1);
			return true;
		}
		
		public void festivalEnd()
		{
			_festivalRooms.remove(_cabal == SevenSigns.CABAL_DUSK ? 10 + _levelRange : 20 + _levelRange);
			
			unspawnMobs();
		}
		
		protected void unspawnMobs()
		{
			for (var monsterInst : _npcInsts)
			{
				if (monsterInst != null)
				{
					monsterInst.getSpawn().stopRespawn();
					monsterInst.deleteMe();
					SpawnData.getInstance().deleteSpawn(monsterInst.getSpawn(), false);
				}
			}
		}
	}
	
	public L2PcInstance getPlayer()
	{
		return _player;
	}
	
	public FestivalManager getManagerInstance()
	{
		return _managerInstance;
	}
	
	public static SevenSignsFestival getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final SevenSignsFestival INSTANCE = new SevenSignsFestival();
	}
}
