/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.UPnPService;
import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.commons.network.ServerType;
import com.l2jserver.gameserver.configuration.config.AdminConfig;
import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.configuration.config.custom.OfflineConfig;
import com.l2jserver.gameserver.configuration.config.events.FishingConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.instancemanager.CHSiegeManager;
import com.l2jserver.gameserver.instancemanager.CastleManorManager;
import com.l2jserver.gameserver.instancemanager.CursedWeaponsManager;
import com.l2jserver.gameserver.instancemanager.GlobalVariablesManager;
import com.l2jserver.gameserver.instancemanager.GrandBossManager;
import com.l2jserver.gameserver.instancemanager.ItemAuctionManager;
import com.l2jserver.gameserver.instancemanager.ItemsOnGroundManager;
import com.l2jserver.gameserver.instancemanager.QuestManager;
import com.l2jserver.gameserver.instancemanager.RaidBossSpawnManager;
import com.l2jserver.gameserver.instancemanager.games.FishingChampionshipManager;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.Hero;
import com.l2jserver.gameserver.model.olympiad.Olympiad;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ServerClose;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.Broadcast;

public class Shutdown extends Thread
{
	private static final Logger LOG = LoggerFactory.getLogger(Shutdown.class);
	
	private static Shutdown _counterInstance = null;
	
	private int _secondsShut;
	private int _shutdownMode;
	
	private static final int SIGTERM = 0;
	private static final int GM_SHUTDOWN = 1;
	private static final int GM_RESTART = 2;
	private static final int ABORT = 3;
	
	private static final String[] MODE_TEXT =
	{
		"SIGTERM",
		"shutting down",
		"restarting",
		"aborting"
	};
	
	protected Shutdown()
	{
		_secondsShut = -1;
		_shutdownMode = SIGTERM;
	}
	
	public Shutdown(int seconds, boolean restart)
	{
		_secondsShut = Math.max(0, seconds);
		_shutdownMode = (restart) ? GM_RESTART : GM_SHUTDOWN;
	}
	
	@Override
	public void run()
	{
		if (this == getInstance())
		{
			StringUtil.printSection("Under " + MODE_TEXT[_shutdownMode] + " process");
			
			try
			{
				UPnPService.getInstance().removeAllPorts();
				LOG.info("UPnP Service: All ports mappings deleted.");
			}
			catch (Exception ex)
			{
				LOG.warn("Error while removing UPnP port mappings!", ex);
			}
			
			try
			{
				if ((OfflineConfig.OFFLINE_TRADE_ENABLE || OfflineConfig.OFFLINE_CRAFT_ENABLE) && OfflineConfig.RESTORE_OFFLINERS)
				{
					DAOFactory.getInstance().getPlayerOfflineTradersDAO().storeOffliners();
					LOG.info("Offline Traders: Offline shops stored.");
				}
			}
			catch (Exception ex)
			{
				LOG.warn("Error saving offline shops!", ex);
			}
			
			try
			{
				disconnectAllCharacters();
				LOG.info("All players disconnected and saved.");
			}
			catch (Exception e)
			{
				// ignore
			}
			
			// ensure all services are stopped
			try
			{
				GameTimeController.getInstance().stopTimer();
				LOG.info("Game Time Controller: Timer stopped.");
			}
			catch (Exception e)
			{
				// ignore
			}
			
			// stop all thread pools
			try
			{
				ThreadPoolManager.getInstance().shutdown();
				LOG.info("Thread Pool Manager: Manager has been shut down.");
			}
			catch (Exception e)
			{
				// ignore
			}
			
			try
			{
				LoginServerThread.getInstance().interrupt();
				LOG.info("Login Server Thread: Thread interruped.");
			}
			catch (Exception e)
			{
				// ignore
			}
			
			// last byebye, save all data and quit this server
			saveData();
			
			// saveData sends messages to exit players, so shutdown selector after it
			try
			{
				GameServer.getInstance().getSelectorThread().shutdown();
				LOG.info("Game Server: Selector thread has been shut down.");
			}
			catch (Exception e)
			{
				// ignore
			}
			
			// commit data, last chance
			ConnectionFactory.getInstance().close();
			LOG.info("ConnectionFactory: Database connection has been shutdown.");
			
			// server will quit, when this function ends.
			Runtime.getRuntime().halt((getInstance()._shutdownMode == GM_RESTART) ? 2 : 0);
			LOG.info("The server has been successfully shut down.");
		}
		else
		{
			// gm shutdown: send warnings and then call exit to start shutdown sequence
			countdown();
			// last point where logging is operational :(
			LOG.warn("GM shutdown countdown is over. {} NOW!", MODE_TEXT[_shutdownMode]);
			switch (_shutdownMode)
			{
				case GM_SHUTDOWN ->
				{
					getInstance().setMode(GM_SHUTDOWN);
					System.exit(0);
				}
				case GM_RESTART ->
				{
					getInstance().setMode(GM_RESTART);
					System.exit(2);
				}
				case ABORT -> LoginServerThread.getInstance().setServerType(ServerType.AUTO);
			}
		}
	}
	
	public void startShutdown(L2PcInstance player, int seconds, boolean restart)
	{
		_shutdownMode = (restart) ? GM_RESTART : GM_SHUTDOWN;
		
		if (player != null)
		{
			LOG.warn("GM: {}({}) issued shutdown command. {} in {} seconds!", player.getName(), player.getObjectId(), MODE_TEXT[_shutdownMode], seconds);
		}
		else
		{
			LOG.warn("Automated restart activated!");
		}
		
		if (_shutdownMode > 0)
		{
			switch (seconds)
			{
				case 540:
				case 480:
				case 420:
				case 360:
				case 300:
				case 240:
				case 180:
				case 120:
				case 60:
				case 30:
				case 10:
				case 5:
				case 4:
				case 3:
				case 2:
				case 1:
					break;
				default:
					sendServerQuit(seconds);
			}
		}
		
		if (_counterInstance != null)
		{
			_counterInstance.setMode(ABORT);
		}
		_counterInstance = new Shutdown(seconds, restart);
		_counterInstance.start();
	}
	
	public void abort(L2PcInstance player)
	{
		if (_counterInstance != null)
		{
			LOG.info("GM: {}({}) aborted {} process.", player.getName(), player.getObjectId(), MODE_TEXT[_shutdownMode]);
			_counterInstance.setMode(ABORT);
			
			Broadcast.announceToOnlinePlayers("Server aborts " + MODE_TEXT[_shutdownMode] + " and continues normal operation!", false);
		}
	}
	
	private void setMode(int mode)
	{
		_shutdownMode = mode;
	}
	
	private void countdown()
	{
		try
		{
			while (_secondsShut > 0)
			{
				// Rehabilitate previous server status if shutdown is aborted.
				if (_shutdownMode == ABORT)
				{
					if (LoginServerThread.getInstance().getServerType() == ServerType.DOWN)
					{
						LoginServerThread.getInstance().setServerType((AdminConfig.SERVER_GMONLY) ? ServerType.GM_ONLY : ServerType.AUTO);
					}
					break;
				}
				
				switch (_secondsShut)
				{
					case 540:
					case 480:
					case 420:
					case 360:
					case 300:
					case 240:
					case 180:
					case 120:
					case 60:
					case 30:
					case 10:
					case 5:
					case 4:
					case 3:
					case 2:
					case 1:
						sendServerQuit(_secondsShut);
						break;
				}
				
				// avoids new players from logging in
				if ((_secondsShut <= 60) && (LoginServerThread.getInstance().getServerType() != ServerType.DOWN))
				{
					LoginServerThread.getInstance().setServerType(ServerType.DOWN);
				}
				
				_secondsShut--;
				
				Thread.sleep(1000);
				
				if (_shutdownMode == ABORT)
				{
					break;
				}
			}
		}
		catch (InterruptedException e)
		{
			// this will never happen
		}
	}
	
	private void saveData()
	{
		switch (_shutdownMode)
		{
			case SIGTERM -> LOG.info("SIGTERM received. Shutting down NOW!");
			case GM_SHUTDOWN -> LOG.info("GM shutdown received. Shutting down NOW!");
			case GM_RESTART -> LOG.info("GM restart received. Restarting NOW!");
		}
		
		// Seven Signs data is now saved along with Festival data.
		if (!SevenSigns.getInstance().isSealValidationPeriod())
		{
			SevenSignsFestival.getInstance().saveFestivalData(false);
		}
		// Save Seven Signs data before closing. :)
		SevenSigns.getInstance().saveSevenSignsData();
		SevenSigns.getInstance().saveSevenSignsStatus();
		LOG.info("Seven Signs Festival, general data && status have been saved.");
		
		// Save all raidboss and GrandBoss status ^_^
		RaidBossSpawnManager.getInstance().cleanUp();
		LOG.info("Raid Bosses data has been saved.");
		// Save grandbosses status
		GrandBossManager.getInstance().cleanUp();
		LOG.info("World Bosses data has been saved.");
		
		ItemAuctionManager.getInstance().shutdown();
		LOG.info("Item Auction Manager data has been saved.");
		
		Olympiad.getInstance().saveOlympiadStatus();
		LOG.info("Olympiad data has been saved.");
		
		Hero.getInstance().shutdown();
		LOG.info("Hero data has been saved.");
		
		ClanTable.getInstance().storeClanScore();
		LOG.info("Clan data has been saved.");
		
		CursedWeaponsManager.getInstance().saveData();
		LOG.info("Cursed Weapons data has been saved.");
		
		CastleManorManager.getInstance().storeMe();
		LOG.info("Manor data has been saved.");
		
		CHSiegeManager.getInstance().onServerShutDown();
		LOG.info("Siegable hall attacker lists saved!");
		
		// Save all global (non-player specific) Quest data that needs to persist after reboot
		QuestManager.getInstance().save();
		LOG.info("Quest data has been saved.");
		
		// Save all global variables data
		GlobalVariablesManager.getInstance().storeMe();
		LOG.info("Global Variables data has been saved.");
		
		// Save Fishing tournament data
		if (FishingConfig.ALT_FISH_CHAMPIONSHIP_ENABLED)
		{
			FishingChampionshipManager.getInstance().shutdown();
			LOG.info("Fishing Championship data has been saved.");
		}
		
		// Save items on ground before closing
		if (GeneralConfig.SAVE_DROPPED_ITEM)
		{
			ItemsOnGroundManager.getInstance().saveInDb();
			ItemsOnGroundManager.getInstance().cleanUp();
			LOG.info("Items On Ground data has cleaned up and saved.");
		}
		
		// Save bot reports to database
		if (GeneralConfig.BOTREPORT_ENABLE)
		{
			DAOFactory.getInstance().getBotReportDAO().save();
			LOG.info("Bot Report data has been saved.");
		}
		
		try
		{
			Thread.sleep(5000);
		}
		catch (InterruptedException e)
		{
			// never happens :p
		}
	}
	
	private static void sendServerQuit(int seconds)
	{
		Broadcast.toAllOnlinePlayers(SystemMessage.getSystemMessage(SystemMessageId.THE_SERVER_WILL_BE_COMING_DOWN_IN_S1_SECONDS__PLEASE_FIND_A_SAFE_PLACE_TO_LOG_OUT).addInt(seconds));
	}
	
	private static void disconnectAllCharacters()
	{
		for (var player : L2World.getInstance().getPlayers())
		{
			var client = player.getClient();
			if ((client != null) && !client.isDetached())
			{
				client.close(ServerClose.STATIC_PACKET);
				client.setActiveChar(null);
				player.setClient(null);
			}
			player.deleteMe();
		}
	}
	
	public static Shutdown getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final Shutdown INSTANCE = new Shutdown();
	}
}
