/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.bbs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.configuration.config.bbs.CStatsConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;

/**
 * @author Мо3олЬ
 */
public class StatisticsBBSManager
{
	private static final Logger LOG = LoggerFactory.getLogger(StatisticsBBSManager.class);
	
	protected StatisticsBBSManager()
	{
		// Do nothing.
	}
	
	public void load()
	{
		if (CStatsConfig.BBS_STATISTIC_ALLOW)
		{
			DAOFactory.getInstance().getCommunityStatisticDAO().selectClassesCount();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectHeroCount();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectAccount();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectNobleCount();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectClanCount();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectAllyCount();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectTopPK();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectTopPVP();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectTopOnline();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectTopPcPoint();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectGrandBoss();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectCastleStatus();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectTopClan();
			DAOFactory.getInstance().getCommunityStatisticDAO().selectHeroeStatus();
			// TODO
			// DAOFactory.getInstance().getCommunityStatisticDAO().selectTopRich();
			LOG.info("Loaded BBS Full statistics.");
		}
	}
	
	public static StatisticsBBSManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final StatisticsBBSManager INSTANCE = new StatisticsBBSManager();
	}
}
