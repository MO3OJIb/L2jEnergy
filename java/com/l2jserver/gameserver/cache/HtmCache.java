/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.cache;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.util.filter.HTMLFilter;
import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.configuration.config.ServerConfig;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * HTML Cache.
 * @author Layane
 * @author Zoey76
 */
public class HtmCache
{
	private static final Logger LOG = LoggerFactory.getLogger(HtmCache.class);
	
	private static final HTMLFilter HTML_FILTER = new HTMLFilter();
	
	private static final Map<String, String> HTML_CACHE = GeneralConfig.LAZY_CACHE ? new ConcurrentHashMap<>() : new HashMap<>();
	
	private int _loadedFiles;
	private long _bytesBuffLen;
	
	protected HtmCache()
	{
		reload();
	}
	
	public void reload()
	{
		reload(ServerConfig.DATAPACK_ROOT);
	}
	
	public void reload(File f)
	{
		if (!GeneralConfig.LAZY_CACHE)
		{
			LOG.info("Html cache start...");
			parseDir(f);
			LOG.info(String.format("%.3f", getMemoryUsage()) + " megabytes on " + getLoadedFiles() + " files loaded");
		}
		else
		{
			HTML_CACHE.clear();
			_loadedFiles = 0;
			_bytesBuffLen = 0;
			LOG.info("Loaded lazy cache.");
		}
	}
	
	public void reloadPath(File f)
	{
		parseDir(f);
		LOG.info("Reloaded specified path.");
	}
	
	public double getMemoryUsage()
	{
		return ((float) _bytesBuffLen / 1048576);
	}
	
	public int getLoadedFiles()
	{
		return _loadedFiles;
	}
	
	private void parseDir(File dir)
	{
		var files = dir.listFiles();
		if (files != null)
		{
			for (var file : files)
			{
				if (!file.isDirectory())
				{
					loadFile(file);
				}
				else
				{
					parseDir(file);
				}
			}
		}
	}
	
	public String loadFile(File file)
	{
		if (!HTML_FILTER.accept(file))
		{
			return null;
		}
		
		String content = null;
		try (var fis = new FileInputStream(file);
			var bis = new BufferedInputStream(fis))
		{
			var bytes = bis.available();
			var raw = new byte[bytes];
			
			bis.read(raw);
			content = new String(raw, StandardCharsets.UTF_8);
			content = content.replaceAll("(?s)<!--.*?-->", ""); // Remove html comments
			
			var oldContent = HTML_CACHE.put(file.getCanonicalPath(), content);
			if (oldContent == null)
			{
				_bytesBuffLen += bytes;
				_loadedFiles++;
			}
			else
			{
				_bytesBuffLen = (_bytesBuffLen - oldContent.length()) + bytes;
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Problem with htm file {}!", file, ex);
		}
		return content;
	}
	
	public String getHtm(L2PcInstance player, String path)
	{
		var prefix = player != null ? player.getHtmlPrefix() : "";
		var newPath = Objects.requireNonNullElse(prefix, "") + path;
		var content = HTML_CACHE.get(newPath);
		if (GeneralConfig.LAZY_CACHE && (content == null))
		{
			content = loadFile(new File(ServerConfig.DATAPACK_ROOT, newPath));
			if (content == null)
			{
				content = loadFile(new File(ServerConfig.DATAPACK_ROOT, newPath));
			}
			
			// If multilanguage content is not present, try default location.
			if (!prefix.contentEquals("") && (content == null))
			{
				content = loadFile(new File(ServerConfig.DATAPACK_ROOT, path));
				if (content == null)
				{
					content = loadFile(new File(ServerConfig.DATAPACK_ROOT, path));
				}
			}
		}
		
		if ((player != null) && player.isGM())
		{
			player.sendHTMLMessage(newPath.substring(5));
		}
		return content;
	}
	
	public boolean contains(String path)
	{
		return HTML_CACHE.containsKey(path);
	}
	
	public boolean isLoadable(String path)
	{
		return HTML_FILTER.accept(new File(ServerConfig.DATAPACK_ROOT, path));
	}
	
	public static HtmCache getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final HtmCache INSTANCE = new HtmCache();
	}
}