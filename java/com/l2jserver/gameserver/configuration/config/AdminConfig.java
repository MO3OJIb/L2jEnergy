/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;
import com.l2jserver.gameserver.enums.network.ServerType;

/**
 * Character Configuration.
 * @author Мо3олЬ
 */
@Configuration("admin.json")
public class AdminConfig
{
	@Setting(name = "EverybodyHasAdminRights")
	public static boolean EVERYBODY_HAS_ADMIN_RIGHTS;
	
	@Setting(name = "ServerListBrackets")
	public static boolean SERVER_LIST_BRACKETS;
	
	@Setting(name = "ServerListType", method = "serverType")
	public static int SERVER_LIST_TYPE;
	
	@Setting(name = "ServerListAge")
	public static int SERVER_LIST_AGE;
	
	@Setting(name = "ServerGMOnly")
	public static boolean SERVER_GMONLY;
	
	@Setting(name = "GMHeroAura")
	public static boolean GM_HERO_AURA;
	
	@Setting(name = "GMStartupInvulnerable")
	public static boolean GM_STARTUP_INVULNERABLE;
	
	@Setting(name = "GMStartupInvisible")
	public static boolean GM_STARTUP_INVISIBLE;
	
	@Setting(name = "GMStartupSilence")
	public static boolean GM_STARTUP_SILENCE;
	
	@Setting(name = "GMStartupAutoList")
	public static boolean GM_STARTUP_AUTO_LIST;
	
	@Setting(name = "GMStartupDietMode")
	public static boolean GM_STARTUP_DIET_MODE;
	
	@Setting(name = "GMItemRestriction")
	public static boolean GM_ITEM_RESTRICTION;
	
	@Setting(name = "GMSkillRestriction")
	public static boolean GM_SKILL_RESTRICTION;
	
	@Setting(name = "GMRestartFighting")
	public static boolean GM_RESTART_FIGHTING;
	
	@Setting(name = "GMShowAnnouncerName")
	public static boolean GM_ANNOUNCER_NAME;
	
	@Setting(name = "GMShowCritAnnouncerName")
	public static boolean GM_CRITANNOUNCER_NAME;
	
	@Setting(name = "GMGiveSpecialSkills")
	public static boolean GM_GIVE_SPECIAL_SKILLS;
	
	@Setting(name = "GMGiveSpecialAuraSkills")
	public static boolean GM_GIVE_SPECIAL_AURA_SKILLS;
	
	@Setting(name = "GameGuardEnforce")
	public static boolean GAMEGUARD_ENFORCE;
	
	@Setting(name = "GameGuardProhibitAction")
	public static boolean GAMEGUARD_PROHIBITACTION;
	
	@Setting(name = "LogChat")
	public static boolean LOG_CHAT;
	
	@Setting(name = "LogItems")
	public static boolean LOG_ITEMS;
	
	@Setting(name = "LogItemsSmallLog")
	public static boolean LOG_ITEMS_SMALL_LOG;
	
	@Setting(name = "LogItemEnchants")
	public static boolean LOG_ITEM_ENCHANTS;
	
	@Setting(name = "LogSkillEnchants")
	public static boolean LOG_SKILL_ENCHANTS;
	
	@Setting(name = "GMAudit")
	public static boolean GMAUDIT;
	
	@Setting(name = "SkillCheckEnable")
	public static boolean SKILL_CHECK_ENABLE;
	
	@Setting(name = "SkillCheckRemove")
	public static boolean SKILL_CHECK_REMOVE;
	
	@Setting(name = "SkillCheckGM")
	public static boolean SKILL_CHECK_GM;
	
	@Setting(name = "ScheduledThreadCorePoolSizeForAI")
	public static int SCHEDULED_THREAD_CORE_POOL_SIZE_AI;
	
	@Setting(name = "ScheduledThreadCorePoolSizeForEffects")
	public static int SCHEDULED_THREAD_CORE_POOL_SIZE_EFFECTS;
	
	@Setting(name = "ScheduledThreadCorePoolSizeForEvents")
	public static int SCHEDULED_THREAD_CORE_POOL_SIZE_EVENTS;
	
	@Setting(name = "ScheduledThreadCorePoolSizeForGeneral")
	public static int SCHEDULED_THREAD_CORE_POOL_SIZE_GENERAL;
	
	@Setting(name = "ThreadCorePoolSizeForEvents")
	public static int THREAD_CORE_POOL_SIZE_EVENT;
	
	@Setting(name = "ThreadCorePoolSizeForGeneral")
	public static int THREAD_CORE_POOL_SIZE_GENERAL;
	
	@Setting(name = "ThreadCorePoolSizeForGeneralPackets")
	public static int THREAD_CORE_POOL_SIZE_GENERAL_PACKETS;
	
	@Setting(name = "ThreadCorePoolSizeForIOPackets")
	public static int THREAD_CORE_POOL_SIZE_IO_PACKETS;
	
	@Setting(name = "DeadLockDetector")
	public static boolean DEADLOCK_DETECTOR;
	
	@Setting(name = "DeadLockCheckInterval", increase = 1000)
	public static int DEADLOCK_CHECK_INTERVAL;
	
	@Setting(name = "RestartOnDeadlock")
	public static boolean RESTART_ON_DEADLOCK;
	
	@Setting(name = "ClientPacketQueueSize", method = "clientpacketqueuesize")
	public static int CLIENT_PACKET_QUEUE_SIZE;
	
	@Setting(name = "ClientPacketQueueMaxBurstSize", method = "clientpacketqueuemaxburstsize")
	public static int CLIENT_PACKET_QUEUE_MAX_BURST_SIZE;
	
	@Setting(name = "ClientPacketQueueMaxPacketsPerSecond")
	public static int CLIENT_PACKET_QUEUE_MAX_PACKETS_PER_SECOND;
	
	@Setting(name = "ClientPacketQueueMeasureInterval")
	public static int CLIENT_PACKET_QUEUE_MEASURE_INTERVAL;
	
	@Setting(name = "ClientPacketQueueMaxAveragePacketsPerSecond")
	public static int CLIENT_PACKET_QUEUE_MAX_AVERAGE_PACKETS_PER_SECOND;
	
	@Setting(name = "ClientPacketQueueMaxFloodsPerMin")
	public static int CLIENT_PACKET_QUEUE_MAX_FLOODS_PER_MIN;
	
	@Setting(name = "ClientPacketQueueMaxOverflowsPerMin")
	public static int CLIENT_PACKET_QUEUE_MAX_OVERFLOWS_PER_MIN;
	
	@Setting(name = "ClientPacketQueueMaxUnderflowsPerMin")
	public static int CLIENT_PACKET_QUEUE_MAX_UNDERFLOWS_PER_MIN;
	
	@Setting(name = "ClientPacketQueueMaxUnknownPerMin")
	public static int CLIENT_PACKET_QUEUE_MAX_UNKNOWN_PER_MIN;
	
	public void serverType(final String value)
	{
		final ServerType t = ServerType.valueOf(value.toUpperCase());
		SERVER_LIST_TYPE |= t.getId();
	}
	
	public void clientpacketqueuesize(String value)
	{
		CLIENT_PACKET_QUEUE_SIZE = Integer.parseInt(value);
		if (CLIENT_PACKET_QUEUE_SIZE == 0)
		{
			CLIENT_PACKET_QUEUE_SIZE = 12 + 2;
		}
	}
	
	public void clientpacketqueuemaxburstsize(String value)
	{
		CLIENT_PACKET_QUEUE_MAX_BURST_SIZE = Integer.parseInt(value);
		if (CLIENT_PACKET_QUEUE_MAX_BURST_SIZE == 0)
		{
			CLIENT_PACKET_QUEUE_MAX_BURST_SIZE = 12 + 1;
		}
	}
}