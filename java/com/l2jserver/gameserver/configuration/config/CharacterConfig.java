/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;
import com.l2jserver.gameserver.util.ClassMasterSettings;
import com.l2jserver.gameserver.util.Util;

/**
 * Character Configuration.
 * @author Мо3олЬ
 */
@Configuration("character.json")
public class CharacterConfig
{
	@Setting(name = "Delevel")
	public static boolean ALT_GAME_DELEVEL;
	
	@Setting(name = "DecreaseSkillOnDelevel")
	public static boolean DECREASE_SKILL_LEVEL;
	
	@Setting(name = "AltWeightLimit")
	public static double ALT_WEIGHT_LIMIT;
	
	@Setting(name = "RunSpeedBoost")
	public static int RUN_SPD_BOOST;
	
	@Setting(name = "DeathPenaltyChance")
	public static int DEATH_PENALTY_CHANCE;
	
	@Setting(name = "RespawnRestoreCP")
	public static double RESPAWN_RESTORE_CP;
	
	@Setting(name = "RespawnRestoreHP")
	public static double RESPAWN_RESTORE_HP;
	
	@Setting(name = "RespawnRestoreMP")
	public static double RESPAWN_RESTORE_MP;
	
	@Setting(name = "HpRegenMultiplier")
	public static double HP_REGEN_MULTIPLIER;
	
	@Setting(name = "MpRegenMultiplier")
	public static double MP_REGEN_MULTIPLIER;
	
	@Setting(name = "CpRegenMultiplier")
	public static double CP_REGEN_MULTIPLIER;
	
	@Setting(name = "EnableModifySkillDuration")
	public static boolean ENABLE_MODIFY_SKILL_DURATION;
	
	@Setting(name = "SkillDurationList", method = "skillListDuration", canNull = true)
	public static Map<Integer, Integer> SKILL_DURATION_LIST;
	
	@Setting(name = "EnableModifySkillReuse")
	public static boolean ENABLE_MODIFY_SKILL_REUSE;
	
	@Setting(name = "SkillReuseList", method = "skillListReuse", canNull = true)
	public static Map<Integer, Integer> SKILL_REUSE_LIST;
	
	@Setting(name = "AutoLearnSkills")
	public static boolean AUTO_LEARN_SKILLS;
	
	@Setting(name = "AutoLearnForgottenScrollSkills")
	public static boolean AUTO_LEARN_FS_SKILLS;
	
	@Setting(name = "AutoLootHerbs")
	public static boolean AUTO_LOOT_HERBS;
	
	@Setting(name = "MaxBuffAmount")
	public static byte BUFFS_MAX_AMOUNT;
	
	@Setting(name = "MaxTriggeredBuffAmount")
	public static byte TRIGGERED_BUFFS_MAX_AMOUNT;
	
	@Setting(name = "MaxDanceAmount")
	public static byte DANCES_MAX_AMOUNT;
	
	@Setting(name = "DanceCancelBuff")
	public static boolean DANCE_CANCEL_BUFF;
	
	@Setting(name = "DanceConsumeAdditionalMP")
	public static boolean DANCE_CONSUME_ADDITIONAL_MP;
	
	@Setting(name = "AltStoreDances")
	public static boolean ALT_STORE_DANCES;
	
	@Setting(name = "AutoLearnDivineInspiration")
	public static boolean AUTO_LEARN_DIVINE_INSPIRATION;
	
	@Setting(name = "AltGameCancelByHit")
	public static boolean ALT_GAME_CANCEL_BOW;
	
	@Setting(name = "AltGameCancelByHit")
	public static boolean ALT_GAME_CANCEL_CAST;
	
	@Setting(name = "MagicFailures")
	public static boolean ALT_GAME_MAGICFAILURES;
	
	@Setting(name = "PlayerFakeDeathUpProtection")
	public static int PLAYER_FAKEDEATH_UP_PROTECTION;
	
	@Setting(name = "StoreSkillCooltime")
	public static boolean STORE_SKILL_COOLTIME;
	
	@Setting(name = "SubclassStoreSkillCooltime")
	public static boolean SUBCLASS_STORE_SKILL_COOLTIME;
	
	@Setting(name = "AltShieldBlocks")
	public static boolean ALT_GAME_SHIELD_BLOCKS;
	
	@Setting(name = "AltPerfectShieldBlockRate")
	public static int ALT_PERFECT_SHLD_BLOCK;
	
	@Setting(name = "EffectTickRatio")
	public static long EFFECT_TICK_RATIO;
	
	@Setting(name = "AllowClassMasters")
	public static boolean ALLOW_CLASS_MASTERS;
	
	@Setting(name = "ConfigClassMaster", method = "configClassMaster")
	public static ClassMasterSettings CLASS_MASTER_SETTINGS;
	
	@Setting(name = "AllowEntireTree")
	public static boolean ALLOW_ENTIRE_TREE;
	
	@Setting(name = "AlternateClassMaster")
	public static boolean ALTERNATE_CLASS_MASTER;
	
	@Setting(name = "LifeCrystalNeeded")
	public static boolean LIFE_CRYSTAL_NEEDED;
	
	@Setting(name = "EnchantSkillSpBookNeeded")
	public static boolean ES_SP_BOOK_NEEDED;
	
	@Setting(name = "DivineInspirationSpBookNeeded")
	public static boolean DIVINE_SP_BOOK_NEEDED;
	
	@Setting(name = "AltGameSkillLearn")
	public static boolean ALT_GAME_SKILL_LEARN;
	
	@Setting(name = "AltSubClassWithoutQuests")
	public static boolean ALT_GAME_SUBCLASS_WITHOUT_QUESTS;
	
	@Setting(name = "AltSubclassEverywhere")
	public static boolean ALT_GAME_SUBCLASS_EVERYWHERE;
	
	@Setting(name = "AltTransformationWithoutQuest")
	public static boolean ALLOW_TRANSFORM_WITHOUT_QUEST;
	
	@Setting(name = "FeeDeleteTransferSkills")
	public static int FEE_DELETE_TRANSFER_SKILLS;
	
	@Setting(name = "FeeDeleteSubClassSkills")
	public static int FEE_DELETE_SUBCLASS_SKILLS;
	
	@Setting(name = "SummonStoreSkillCooltime")
	public static boolean SUMMON_STORE_SKILL_COOLTIME;
	
	@Setting(name = "RestoreServitorOnReconnect")
	public static boolean RESTORE_SERVITOR_ON_RECONNECT;
	
	@Setting(name = "RestorePetOnReconnect")
	public static boolean RESTORE_PET_ON_RECONNECT;
	
	@Setting(name = "EnableVitality")
	public static boolean ENABLE_VITALITY;
	
	@Setting(name = "RecoverVitalityOnReconnect")
	public static boolean RECOVER_VITALITY_ON_RECONNECT;
	
	@Setting(name = "StartingVitalityPoints")
	public static int STARTING_VITALITY_POINTS;
	
	@Setting(name = "EnchantChanceElementStone")
	public static double ENCHANT_CHANCE_ELEMENT_STONE;
	
	@Setting(name = "EnchantChanceElementCrystal")
	public static double ENCHANT_CHANCE_ELEMENT_CRYSTAL;
	
	@Setting(name = "EnchantChanceElementJewel")
	public static double ENCHANT_CHANCE_ELEMENT_JEWEL;
	
	@Setting(name = "EnchantChanceElementEnergy")
	public static double ENCHANT_CHANCE_ELEMENT_ENERGY;
	
	@Setting(name = "EnchantBlackList", splitter = ",")
	public static int[] ENCHANT_BLACKLIST;;
	
	@Setting(name = "AugmentationNGSkillChance")
	public static int AUGMENTATION_NG_SKILL_CHANCE;
	
	@Setting(name = "AugmentationNGGlowChance")
	public static int AUGMENTATION_NG_GLOW_CHANCE;
	
	@Setting(name = "AugmentationMidSkillChance")
	public static int AUGMENTATION_MID_SKILL_CHANCE;
	
	@Setting(name = "AugmentationMidGlowChance")
	public static int AUGMENTATION_MID_GLOW_CHANCE;
	
	@Setting(name = "AugmentationHighSkillChance")
	public static int AUGMENTATION_HIGH_SKILL_CHANCE;
	
	@Setting(name = "AugmentationHighGlowChance")
	public static int AUGMENTATION_HIGH_GLOW_CHANCE;
	
	@Setting(name = "AugmentationTopSkillChance")
	public static int AUGMENTATION_TOP_SKILL_CHANCE;
	
	@Setting(name = "AugmentationTopGlowChance")
	public static int AUGMENTATION_TOP_GLOW_CHANCE;
	
	@Setting(name = "AugmentationBaseStatChance")
	public static int AUGMENTATION_BASESTAT_CHANCE;
	
	@Setting(name = "AugmentationAccSkillChance")
	public static int AUGMENTATION_ACC_SKILL_CHANCE;
	
	@Setting(name = "RetailLikeAugmentation")
	public static boolean RETAIL_LIKE_AUGMENTATION;
	
	@Setting(name = "RetailLikeAugmentationNoGradeChance", splitter = ",")
	public static int[] RETAIL_LIKE_AUGMENTATION_NG_CHANCE;
	
	@Setting(name = "RetailLikeAugmentationMidGradeChance", splitter = ",")
	public static int[] RETAIL_LIKE_AUGMENTATION_MID_CHANCE;
	
	@Setting(name = "RetailLikeAugmentationHighGradeChance", splitter = ",")
	public static int[] RETAIL_LIKE_AUGMENTATION_HIGH_CHANCE;
	
	@Setting(name = "RetailLikeAugmentationTopGradeChance", splitter = ",")
	public static int[] RETAIL_LIKE_AUGMENTATION_TOP_CHANCE;
	
	@Setting(name = "RetailLikeAugmentationAccessory")
	public static boolean RETAIL_LIKE_AUGMENTATION_ACCESSORY;
	
	@Setting(name = "AugmentationBlackList", splitter = ",")
	public static int[] AUGMENTATION_BLACKLIST;
	
	@Setting(name = "AltAllowAugmentPvPItems")
	public static boolean ALT_ALLOW_AUGMENT_PVP_ITEMS;
	
	@Setting(name = "AltKarmaPlayerCanBeKilledInPeaceZone")
	public static boolean ALT_GAME_KARMA_PLAYER_CAN_BE_KILLED_IN_PEACEZONE;
	
	@Setting(name = "AltKarmaPlayerCanShop")
	public static boolean ALT_GAME_KARMA_PLAYER_CAN_SHOP;
	
	@Setting(name = "AltKarmaPlayerCanTeleport")
	public static boolean ALT_GAME_KARMA_PLAYER_CAN_TELEPORT;
	
	@Setting(name = "AltKarmaPlayerCanUseGK")
	public static boolean ALT_GAME_KARMA_PLAYER_CAN_USE_GK;
	
	@Setting(name = "AltKarmaPlayerCanTrade")
	public static boolean ALT_GAME_KARMA_PLAYER_CAN_TRADE;
	
	@Setting(name = "AltKarmaPlayerCanUseWareHouse")
	public static boolean ALT_GAME_KARMA_PLAYER_CAN_USE_WAREHOUSE;
	
	@Setting(name = "MaxPersonalFamePoints")
	public static int MAX_PERSONAL_FAME_POINTS;
	
	@Setting(name = "FortressZoneFameTaskFrequency")
	public static int FORTRESS_ZONE_FAME_TASK_FREQUENCY;
	
	@Setting(name = "FortressZoneFameAquirePoints")
	public static int FORTRESS_ZONE_FAME_AQUIRE_POINTS;
	
	@Setting(name = "CastleZoneFameTaskFrequency")
	public static int CASTLE_ZONE_FAME_TASK_FREQUENCY;
	
	@Setting(name = "CastleZoneFameAquirePoints")
	public static int CASTLE_ZONE_FAME_AQUIRE_POINTS;
	
	@Setting(name = "FameForDeadPlayers")
	public static boolean FAME_FOR_DEAD_PLAYERS;
	
	@Setting(name = "CraftingEnabled")
	public static boolean IS_CRAFTING_ENABLED;
	
	@Setting(name = "CraftMasterwork")
	public static boolean CRAFT_MASTERWORK;
	
	@Setting(name = "DwarfRecipeLimit")
	public static int DWARF_RECIPE_LIMIT;
	
	@Setting(name = "CommonRecipeLimit")
	public static int COMMON_RECIPE_LIMIT;
	
	@Setting(name = "AltGameCreation")
	public static boolean ALT_GAME_CREATION;
	
	@Setting(name = "AltGameCreationSpeed")
	public static double ALT_GAME_CREATION_SPEED;
	
	@Setting(name = "AltGameCreationXpRate")
	public static double ALT_GAME_CREATION_XP_RATE;
	
	@Setting(name = "AltGameCreationSpRate")
	public static double ALT_GAME_CREATION_SP_RATE;
	
	@Setting(name = "AltGameCreationRareXpSpRate")
	public static double ALT_GAME_CREATION_RARE_XPSP_RATE;
	
	@Setting(name = "AltBlacksmithUseRecipes")
	public static boolean ALT_BLACKSMITH_USE_RECIPES;
	
	@Setting(name = "StoreRecipeShopList")
	public static boolean STORE_RECIPE_SHOPLIST;
	
	@Setting(name = "AltPartyRange")
	public static int ALT_PARTY_RANGE;
	
	@Setting(name = "AltPartyRange2")
	public static int ALT_PARTY_RANGE2;
	
	@Setting(name = "PartyEvenlyDistributeAllStackableItems")
	public static boolean PARTY_EVENTLY_DISTRIBUTE_ALL_STACKABLE_ITEMS;
	
	@Setting(name = "PartyEvenlyDistributeAllOtherItems")
	public static boolean PARTY_EVENTLY_DISTRIBUTE_ALL_OTHER_ITEMS;
	
	@Setting(name = "PartyEvenlyDistributeItems", splitter = ",", method = "partyEvenly")
	public static List<Integer> PARTY_EVENTLY_DISTRIBUTE_ITEMS = new ArrayList<>();
	
	@Setting(name = "AltLeavePartyLeader")
	public static boolean ALT_LEAVE_PARTY_LEADER;
	
	@Setting(name = "InitialEquipmentEvent")
	public static boolean INITIAL_EQUIPMENT_EVENT;
	
	@Setting(name = "StartingAdena")
	public static long STARTING_ADENA;
	
	@Setting(name = "StartingLevel")
	public static int STARTING_LEVEL;
	
	@Setting(name = "StartingSP")
	public static int STARTING_SP;
	
	@Setting(name = "MaxAdena")
	public static long MAX_ADENA;
	
	@Setting(name = "AutoLoot")
	public static boolean AUTO_LOOT;
	
	@Setting(name = "AutoLootRaids")
	public static boolean AUTO_LOOT_RAIDS;
	
	@Setting(name = "RaidLootRightsInterval", increase = 1000)
	public static int LOOT_RAIDS_PRIVILEGE_INTERVAL;
	
	@Setting(name = "RaidLootRightsCCSize")
	public static int LOOT_RAIDS_PRIVILEGE_CC_SIZE;
	
	@Setting(name = "UnstuckInterval")
	public static int UNSTUCK_INTERVAL;
	
	@Setting(name = "TeleportWatchdogTimeout")
	public static int TELEPORT_WATCHDOG_TIMEOUT;
	
	@Setting(name = "PlayerSpawnProtection")
	public static int PLAYER_SPAWN_PROTECTION;
	
	@Setting(name = "PlayerSpawnProtectionAllowedItems", splitter = ",")
	public static int[] SPAWN_PROTECTION_ALLOWED_ITEMS;
	
	@Setting(name = "PlayerTeleportProtection")
	public static int PLAYER_TELEPORT_PROTECTION;
	
	@Setting(name = "RandomRespawnInTownEnabled")
	public static boolean RANDOM_RESPAWN_IN_TOWN_ENABLED;
	
	@Setting(name = "OffsetOnTeleportEnabled")
	public static boolean OFFSET_ON_TELEPORT_ENABLED;
	
	@Setting(name = "MaxOffsetOnTeleport")
	public static int MAX_OFFSET_ON_TELEPORT;
	
	@Setting(name = "PetitioningAllowed")
	public static boolean PETITIONING_ALLOWED;
	
	@Setting(name = "MaxPetitionsPerPlayer")
	public static int MAX_PETITIONS_PER_PLAYER;
	
	@Setting(name = "MaxPetitionsPending")
	public static int MAX_PETITIONS_PENDING;
	
	@Setting(name = "AltFreeTeleporting")
	public static boolean ALT_GAME_FREE_TELEPORT;
	
	@Setting(name = "DeleteCharAfterDays")
	public static int DELETE_DAYS;
	
	@Setting(name = "AltGameExponentXp")
	public static float ALT_GAME_EXPONENT_XP;
	
	@Setting(name = "AltGameExponentSp")
	public static float ALT_GAME_EXPONENT_SP;
	
	@Setting(name = "PartyXpCutoffMethod")
	public static String PARTY_XP_CUTOFF_METHOD;
	
	@Setting(name = "PartyXpCutoffPercent")
	public static double PARTY_XP_CUTOFF_PERCENT;
	
	@Setting(name = "PartyXpCutoffLevel")
	public static int PARTY_XP_CUTOFF_LEVEL;
	
	@Setting(name = "PartyXpCutoffGaps", method = "partyXpCutoffGaps")
	public static int[][] PARTY_XP_CUTOFF_GAPS;
	
	@Setting(name = "PartyXpCutoffGapPercent", splitter = ";")
	public static int[] PARTY_XP_CUTOFF_GAP_PERCENTS;
	
	@Setting(name = "DisableTutorial")
	public static boolean DISABLE_TUTORIAL;
	
	@Setting(name = "ExpertisePenalty")
	public static boolean EXPERTISE_PENALTY;
	
	@Setting(name = "StoreCharUiSettings")
	public static boolean STORE_UI_SETTINGS;
	
	@Setting(name = "ForbiddenNames", method = "forbidNames")
	public static Set<String> FORBIDDEN_NAMES;
	
	@Setting(name = "SilenceModeExclude")
	public static boolean SILENCE_MODE_EXCLUDE;
	
	@Setting(name = "AltValidateTriggerSkills")
	public static boolean ALT_VALIDATE_TRIGGER_SKILLS;
	
	public void forbidNames(String value)
	{
		FORBIDDEN_NAMES = new HashSet<>(Arrays.asList(value.split(",")));
	}
	
	public void partyEvenly(String value)
	{
		PARTY_EVENTLY_DISTRIBUTE_ITEMS.add(Integer.parseInt(value));
	}
	
	public void partyXpCutoffGaps(String value)
	{
		final String[] gaps = value.split(";");
		PARTY_XP_CUTOFF_GAPS = new int[gaps.length][2];
		for (int i = 0; i < gaps.length; i++)
		{
			PARTY_XP_CUTOFF_GAPS[i] = new int[]
			{
				Integer.parseInt(gaps[i].split(",")[0]),
				Integer.parseInt(gaps[i].split(",")[1])
			};
		}
	}
	
	public void skillListDuration(String value)
	{
		SKILL_DURATION_LIST = Util.parseConfigMap(value, Integer.class, Integer.class);
	}
	
	public void skillListReuse(String value)
	{
		SKILL_REUSE_LIST = Util.parseConfigMap(value, Integer.class, Integer.class);
	}
	
	public void configClassMaster(String value)
	{
		if (ALLOW_CLASS_MASTERS || ALTERNATE_CLASS_MASTER)
		{
			CLASS_MASTER_SETTINGS = new ClassMasterSettings(value);
		}
	}
}