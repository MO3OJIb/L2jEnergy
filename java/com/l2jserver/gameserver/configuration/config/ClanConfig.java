/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;

/**
 * Clan Configuration.
 * @author Мо3олЬ
 */
@Configuration("clan.json")
public class ClanConfig
{
	@Setting(name = "AltClanLeaderDateChange", minValue = 1, maxValue = 7)
	public static int ALT_CLAN_LEADER_DATE_CHANGE;
	
	@Setting(name = "AltClanLeaderHourChange")
	public static String ALT_CLAN_LEADER_HOUR_CHANGE;
	
	@Setting(name = "AltClanLeaderInstantActivation")
	public static boolean ALT_CLAN_LEADER_INSTANT_ACTIVATION;
	
	@Setting(name = "DaysBeforeJoinAClan")
	public static int ALT_CLAN_JOIN_DAYS;
	
	@Setting(name = "DaysBeforeCreateAClan")
	public static int ALT_CLAN_CREATE_DAYS;
	
	@Setting(name = "DaysToPassToDissolveAClan")
	public static int ALT_CLAN_DISSOLVE_DAYS;
	
	@Setting(name = "DaysBeforeJoinAllyWhenLeaved")
	public static int ALT_ALLY_JOIN_DAYS_WHEN_LEAVED;
	
	@Setting(name = "DaysBeforeJoinAllyWhenDismissed")
	public static int ALT_ALLY_JOIN_DAYS_WHEN_DISMISSED;
	
	@Setting(name = "DaysBeforeAcceptNewClanWhenDismissed")
	public static int ALT_ACCEPT_CLAN_DAYS_WHEN_DISMISSED;
	
	@Setting(name = "DaysBeforeCreateNewAllyWhenDissolved")
	public static int ALT_CREATE_ALLY_DAYS_WHEN_DISSOLVED;
	
	@Setting(name = "AltMaxNumOfClansInAlly")
	public static int ALT_MAX_NUM_OF_CLANS_IN_ALLY;
	
	@Setting(name = "AltMembersCanWithdrawFromClanWH")
	public static boolean ALT_MEMBERS_CAN_WITHDRAW_FROM_CLANWH;
	
	@Setting(name = "RemoveCastleCirclets")
	public static boolean REMOVE_CASTLE_CIRCLETS;
	
	@Setting(name = "AltClanMembersForWar")
	public static int ALT_CLAN_MEMBERS_FOR_WAR;
}
