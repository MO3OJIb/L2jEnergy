/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;

/**
 * Developer Configuration.
 * @author Мо3олЬ
 */
@Configuration("developer.json")
public class DeveloperConfig
{
	@Setting(name = "Debug")
	public static boolean DEBUG;
	
	@Setting(name = "InstanceDebug")
	public static boolean DEBUG_INSTANCES;
	
	@Setting(name = "HtmlActionCacheDebug")
	public static boolean HTML_ACTION_CACHE_DEBUG;
	
	@Setting(name = "PacketHandlerDebug")
	public static boolean PACKET_HANDLER_DEBUG;
	
	@Setting(name = "Developer")
	public static boolean DEVELOPER;
	
	@Setting(name = "AltDevNoHandlers")
	public static boolean ALT_DEV_NO_HANDLERS;
	
	@Setting(name = "AltDevNoQuests")
	public static boolean ALT_DEV_NO_QUESTS;
	
	@Setting(name = "AltDevNoSpawns")
	public static boolean ALT_DEV_NO_SPAWNS;
	
	@Setting(name = "AltDevShowQuestsLoadInLogs")
	public static boolean ALT_DEV_SHOW_QUESTS_LOAD_IN_LOGS;
	
	@Setting(name = "AltDevShowScriptsLoadInLogs")
	public static boolean ALT_DEV_SHOW_SCRIPTS_LOAD_IN_LOGS;
}
