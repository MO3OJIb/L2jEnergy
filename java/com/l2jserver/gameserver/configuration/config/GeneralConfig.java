/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config;

import java.util.HashSet;
import java.util.Set;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;
import com.l2jserver.gameserver.enums.network.ChatType;

/**
 * General Configuration.
 * @author Мо3олЬ
 */
@Configuration("general.json")
public class GeneralConfig
{
	@Setting(ignore = true)
	public static boolean CHECK_KNOWN;
	
	@Setting(name = "AllowDiscardItem")
	public static boolean ALLOW_DISCARDITEM;
	
	@Setting(name = "AutoDestroyDroppedItemAfter")
	public static int AUTODESTROY_ITEM_AFTER;
	
	@Setting(name = "AutoDestroyHerbTime", increase = 1000)
	public static int HERB_AUTO_DESTROY_TIME;
	
	@Setting(name = "ListOfProtectedItems", splitter = ",")
	public static int[] LIST_PROTECTED_ITEMS;
	
	@Setting(name = "DatabaseCleanUp")
	public static boolean DATABASE_CLEAN_UP;
	
	@Setting(name = "ConnectionCloseTime")
	public static long CONNECTION_CLOSE_TIME;
	
	@Setting(name = "CharacterDataStoreInterval")
	public static int CHAR_STORE_INTERVAL;
	
	@Setting(name = "LazyItemsUpdate")
	public static boolean LAZY_ITEMS_UPDATE;
	
	@Setting(name = "UpdateItemsOnCharStore")
	public static boolean UPDATE_ITEMS_ON_CHAR_STORE;
	
	@Setting(name = "DestroyPlayerDroppedItem")
	public static boolean DESTROY_DROPPED_PLAYER_ITEM;
	
	@Setting(name = "DestroyEquipableItem")
	public static boolean DESTROY_EQUIPABLE_PLAYER_ITEM;
	
	@Setting(name = "SaveDroppedItem")
	public static boolean SAVE_DROPPED_ITEM;
	
	@Setting(name = "EmptyDroppedItemTableAfterLoad")
	public static boolean EMPTY_DROPPED_ITEM_TABLE_AFTER_LOAD;
	
	@Setting(name = "SaveDroppedItemInterval", increase = 60000)
	public static int SAVE_DROPPED_ITEM_INTERVAL;
	
	@Setting(name = "ClearDroppedItemTable")
	public static boolean CLEAR_DROPPED_ITEM_TABLE;
	
	@Setting(name = "AutoDeleteInvalidQuestData")
	public static boolean AUTODELETE_INVALID_QUEST_DATA;
	
	@Setting(name = "PreciseDropCalculation")
	public static boolean PRECISE_DROP_CALCULATION;
	
	@Setting(name = "PreciseDropMultipleGroupRolls")
	public static boolean RECISE_DROP_MULTIPLE_GROUP_ROLLS;
	
	@Setting(name = "PreciseDropMultipleRollsAggregateDrops")
	public static boolean RECISE_DROP_MULTIPLE_ROLLS_AGGREGATE_DROPS;
	
	@Setting(name = "MultipleItemDrop")
	public static boolean MULTIPLE_ITEM_DROP;
	
	@Setting(name = "ForceInventoryUpdate")
	public static boolean FORCE_INVENTORY_UPDATE;
	
	@Setting(name = "LazyCache")
	public static boolean LAZY_CACHE;
	
	@Setting(name = "CacheCharNames")
	public static boolean CACHE_CHAR_NAMES;
	
	@Setting(name = "MinNPCAnimation")
	public static int MIN_NPC_ANIMATION;
	
	@Setting(name = "MaxNPCAnimation")
	public static int MAX_NPC_ANIMATION;
	
	@Setting(name = "MinMonsterAnimation")
	public static int MIN_MONSTER_ANIMATION;
	
	@Setting(name = "MaxMonsterAnimation")
	public static int MAX_MONSTER_ANIMATION;
	
	@Setting(name = "EnableFallingDamage")
	public static boolean ENABLE_FALLING_DAMAGE;
	
	@Setting(name = "JapanMinigame")
	public static boolean EX_JAPAN_MINIGAME;
	
	@Setting(name = "LectureMark")
	public static boolean EX_LECTURE_MARK;
	
	@Setting(name = "EnableItemMall")
	public static boolean ENABLE_ITEM_MALL;
	
	@Setting(name = "GridsAlwaysOn")
	public static boolean GRIDS_ALWAYS_ON;
	
	@Setting(name = "GridNeighborTurnOnTime")
	public static int GRID_NEIGHBOR_TURNON_TIME;
	
	@Setting(name = "GridNeighborTurnOffTime")
	public static int GRID_NEIGHBOR_TURNOFF_TIME;
	
	@Setting(name = "MoveBasedKnownlist")
	public static boolean MOVE_BASED_KNOWNLIST;
	
	@Setting(name = "KnownListUpdateInterval")
	public static long KNOWNLIST_UPDATE_INTERVAL;
	
	@Setting(name = "PeaceZoneMode")
	public static int PEACE_ZONE_MODE;
	
	@Setting(name = "GlobalChat")
	public static String DEFAULT_GLOBAL_CHAT;
	
	@Setting(name = "TradeChat")
	public static String DEFAULT_TRADE_CHAT;
	
	@Setting(name = "AllowWarehouse")
	public static boolean ALLOW_WAREHOUSE;
	
	@Setting(name = "WarehouseCache")
	public static boolean WAREHOUSE_CACHE;
	
	@Setting(name = "WarehouseCacheTime")
	public static int WAREHOUSE_CACHE_TIME;
	
	@Setting(name = "AllowRefund")
	public static boolean ALLOW_REFUND;
	
	@Setting(name = "AllowMail")
	public static boolean ALLOW_MAIL;
	
	@Setting(name = "AllowAttachments")
	public static boolean ALLOW_ATTACHMENTS;
	
	@Setting(name = "AllowWear")
	public static boolean ALLOW_WEAR;
	
	@Setting(name = "WearDelay")
	public static int WEAR_DELAY;
	
	@Setting(name = "WearPrice")
	public static int WEAR_PRICE;
	
	@Setting(name = "DefaultFinishTime")
	public static int INSTANCE_FINISH_TIME;
	
	@Setting(name = "RestorePlayerInstance")
	public static boolean RESTORE_PLAYER_INSTANCE;
	
	@Setting(name = "AllowSummonInInstance")
	public static boolean ALLOW_SUMMON_IN_INSTANCE;
	
	@Setting(name = "EjectDeadPlayerTime")
	public static int EJECT_DEAD_PLAYER_TIME;
	
	@Setting(name = "AllowWater")
	public static boolean ALLOW_WATER;
	
	@Setting(name = "AllowRentPet")
	public static boolean ALLOW_RENTPET;
	
	@Setting(name = "AllowFishing")
	public static boolean ALLOWFISHING;
	
	@Setting(name = "AllowBoat")
	public static boolean ALLOW_BOAT;
	
	@Setting(name = "BoatBroadcastRadius")
	public static int BOAT_BROADCAST_RADIUS;
	
	@Setting(name = "AllowCursedWeapons")
	public static boolean ALLOW_CURSED_WEAPONS;
	
	@Setting(name = "AllowPetWalkers")
	public static boolean ALLOW_PET_WALKERS;
	
	@Setting(name = "ShowServerNews")
	public static boolean SERVER_NEWS;
	
	@Setting(name = "UseChatFilter")
	public static boolean USE_SAY_FILTER;
	
	@Setting(name = "ChatFilterChars")
	public static String CHAT_FILTER_CHARS;
	
	@Setting(name = "BanChatChannels", method = "banChat")
	public static Set<ChatType> BAN_CHAT_CHANNELS;
	
	@Setting(name = "AltItemAuctionEnabled")
	public static boolean ALT_ITEM_AUCTION_ENABLED;
	
	@Setting(name = "AltItemAuctionExpiredAfter")
	public static int ALT_ITEM_AUCTION_EXPIRED_AFTER;
	
	@Setting(name = "AltItemAuctionTimeExtendsOnBid", increase = 1000)
	public static long ALT_ITEM_AUCTION_TIME_EXTENDS_ON_BID;
	
	@Setting(name = "TimeOfAttack", minValue = 0, maxValue = 50)
	public static int FS_TIME_ATTACK;
	
	@Setting(name = "TimeOfCoolDown", minValue = 0, maxValue = 5)
	public static int FS_TIME_COOLDOWN;
	
	@Setting(name = "TimeOfEntry", minValue = 0, maxValue = 3)
	public static int FS_TIME_ENTRY;
	
	@Setting(name = "TimeOfWarmUp")
	public static int FS_TIME_WARMUP;
	
	@Setting(name = "NumberOfNecessaryPartyMembers")
	public static int FS_PARTY_MEMBER_COUNT;
	
	@Setting(name = "RiftMinPartySize")
	public static int RIFT_MIN_PARTY_SIZE;
	
	@Setting(name = "RiftSpawnDelay")
	public static int RIFT_SPAWN_DELAY;
	
	@Setting(name = "MaxRiftJumps")
	public static int RIFT_MAX_JUMPS;
	
	@Setting(name = "AutoJumpsDelayMin")
	public static int RIFT_AUTO_JUMPS_TIME_MIN;
	
	@Setting(name = "AutoJumpsDelayMax")
	public static int RIFT_AUTO_JUMPS_TIME_MAX;
	
	@Setting(name = "BossRoomTimeMultiply")
	public static float RIFT_BOSS_ROOM_TIME_MUTIPLY;
	
	@Setting(name = "RecruitCost")
	public static int RIFT_ENTER_COST_RECRUIT;
	
	@Setting(name = "SoldierCost")
	public static int RIFT_ENTER_COST_SOLDIER;
	
	@Setting(name = "OfficerCost")
	public static int RIFT_ENTER_COST_OFFICER;
	
	@Setting(name = "CaptainCost")
	public static int RIFT_ENTER_COST_CAPTAIN;
	
	@Setting(name = "CommanderCost")
	public static int RIFT_ENTER_COST_COMMANDER;
	
	@Setting(name = "HeroCost")
	public static int RIFT_ENTER_COST_HERO;
	
	@Setting(name = "AltBirthdayGift")
	public static int ALT_BIRTHDAY_GIFT;
	
	@Setting(name = "HellboundWithoutQuest")
	public static boolean HELLBOUND_WITHOUT_QUEST;
	
	@Setting(name = "NormalEnchantCostMultipiler")
	public static int NORMAL_ENCHANT_COST_MULTIPLIER;
	
	@Setting(name = "SafeEnchantCostMultipiler")
	public static int SAFE_ENCHANT_COST_MULTIPLIER;
	
	@Setting(name = "EnableBotReportButton")
	public static boolean BOTREPORT_ENABLE;
	
	@Setting(name = "BotReportPointsResetHour", splitter = ":")
	public static String[] BOTREPORT_RESETPOINT_HOUR;
	
	@Setting(name = "BotReportDelay", increase = 60000)
	public static long BOTREPORT_REPORT_DELAY;
	
	@Setting(name = "AllowReportsFromSameClanMembers")
	public static boolean BOTREPORT_ALLOW_REPORTS_FROM_SAME_CLAN_MEMBERS;
	
	public void banChat(String value)
	{
		final String[] chats = value.split(";");
		BAN_CHAT_CHANNELS = new HashSet<>();
		for (String chat : chats)
		{
			BAN_CHAT_CHANNELS.add(Enum.valueOf(ChatType.class, chat));
		}
	}
}