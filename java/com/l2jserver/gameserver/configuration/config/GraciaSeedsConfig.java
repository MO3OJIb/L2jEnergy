/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;

/**
 * Gracia Seeds Configuration.
 * @author Мо3олЬ
 */
@Configuration("graciaSeeds.json")
public class GraciaSeedsConfig
{
	@Setting(name = "TiatKillCountForNextState")
	public static int SOD_TIAT_KILL_COUNT;
	
	@Setting(name = "Stage2Length", increase = 60000)
	public static long SOD_STAGE_2_LENGTH;
	
	@Setting(name = "TwinKillCountForNextStage")
	public static int SOI_TWIN_KILL_COUNT;
	
	@Setting(name = "CohemenesKillCountForNextStage")
	public static int SOI_COHEMENES_KILL_COUNT;
	
	@Setting(name = "EkimusKillCountForNextStage")
	public static int SOI_EKIMUS_KILL_COUNT;
}