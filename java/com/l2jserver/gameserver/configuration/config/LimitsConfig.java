/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;

/**
 * Limits Configuration.
 * @author Мо3олЬ
 */
@Configuration("limits.json")
public class LimitsConfig
{
	@Setting(name = "MaxExpBonus")
	public static double MAX_BONUS_EXP;
	
	@Setting(name = "MaxSpBonus")
	public static double MAX_BONUS_SP;
	
	@Setting(name = "MaxRunSpeed")
	public static int MAX_RUN_SPEED;
	
	@Setting(name = "MaxPCritRate")
	public static int MAX_PCRIT_RATE;
	
	@Setting(name = "MaxMCritRate")
	public static int MAX_MCRIT_RATE;
	
	@Setting(name = "MaxPAtkSpeed")
	public static int MAX_PATK_SPEED;
	
	@Setting(name = "MaxMAtkSpeed")
	public static int MAX_MATK_SPEED;
	
	@Setting(name = "MaxEvasion")
	public static int MAX_EVASION;
	
	@Setting(name = "MinAbnormalStateSuccessRate")
	public static int MIN_ABNORMAL_STATE_SUCCESS_RATE;
	
	@Setting(name = "MaxAbnormalStateSuccessRate")
	public static int MAX_ABNORMAL_STATE_SUCCESS_RATE;
	
	@Setting(name = "MaxPlayerLevel")
	public static int MAX_PLAYER_LEVEL;
	
	@Setting(name = "MaxPetLevel")
	public static int MAX_PET_LEVEL;
	
	@Setting(name = "MaxSubclass")
	public static byte MAX_SUBCLASS;
	
	@Setting(name = "BaseSubclassLevel")
	public static int BASE_SUBCLASS_LEVEL;
	
	@Setting(name = "MaxSubclassLevel")
	public static int MAX_SUBCLASS_LEVEL;
	
	@Setting(name = "MaxPvtStoreSellSlotsDwarf")
	public static int MAX_PVTSTORESELL_SLOTS_DWARF;
	
	@Setting(name = "MaxPvtStoreSellSlotsOther")
	public static int MAX_PVTSTORESELL_SLOTS_OTHER;
	
	@Setting(name = "MaxPvtStoreBuySlotsDwarf")
	public static int MAX_PVTSTOREBUY_SLOTS_DWARF;
	
	@Setting(name = "MaxPvtStoreBuySlotsOther")
	public static int MAX_PVTSTOREBUY_SLOTS_OTHER;
	
	@Setting(name = "MaximumSlotsForNoDwarf")
	public static int INVENTORY_MAXIMUM_NO_DWARF;
	
	@Setting(name = "MaximumSlotsForDwarf")
	public static int INVENTORY_MAXIMUM_DWARF;
	
	@Setting(name = "MaximumSlotsForGMPlayer")
	public static int INVENTORY_MAXIMUM_GM;
	
	@Setting(name = "MaximumSlotsForQuestItems")
	public static int INVENTORY_MAXIMUM_QUEST_ITEMS;
	
	@Setting(name = "MaximumWarehouseSlotsForDwarf")
	public static int WAREHOUSE_SLOTS_DWARF;
	
	@Setting(name = "MaximumWarehouseSlotsForNoDwarf")
	public static int WAREHOUSE_SLOTS_NO_DWARF;
	
	@Setting(name = "MaximumWarehouseSlotsForClan")
	public static int WAREHOUSE_SLOTS_CLAN;
	
	@Setting(name = "MaximumFreightSlots")
	public static int ALT_FREIGHT_SLOTS;
	
	@Setting(name = "FreightPrice")
	public static int ALT_FREIGHT_PRICE;
	
	@Setting(name = "NpcTalkBlockingTime", increase = 1000)
	public static int PLAYER_MOVEMENT_BLOCK_TIME;
	
	@Setting(name = "FriendListLimit")
	public static int FRIEND_LIST_LIMIT;
	
	@Setting(name = "BlockListLimit")
	public static int BLOCK_LIST_LIMIT;
}
