/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config.bbs;

/**
 * Community Buffer Configuration.
 * @author Мо3олЬ
 */
import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;

@Configuration("bbs/buffer.json")
public class CBufferConfig
{
	@Setting(name = "EnableCommunityBuffer")
	public static boolean ENABLE_BBS_BUFFER;
	
	@Setting(name = "CommunityBufferPrice", splitter = ";")
	public static int[] BBS_BUFFER_PRICE;
	
	@Setting(name = "CommunityBufferPriceMod")
	public static boolean BBS_BUFFER_PRICE_MOD;
	
	@Setting(name = "CommunityBufferSaveId")
	public static int BBS_BUFFER_SAVED_ID;
	
	@Setting(name = "CommunityBufferSavePrice")
	public static long BBS_BUFFER_SAVED_PRICE;
	
	@Setting(name = "CommunityBufferTime")
	public static int BBS_BUFFER_TIME;
	
	@Setting(name = "CommunityBufferPlayerMinLevel")
	public static int BBS_BUFFER_PLAYER_MIN_LEVEL;
	
	@Setting(name = "CommunityBufferPlayerMaxLevel")
	public static int BBS_BUFFER_PLAYER_MAX_LEVEL;
	
	@Setting(name = "CommunityBufferPlayerFreeLevel")
	public static int BBS_BUFFER_PLAYER_FREE_LEVEL;
	
	@Setting(name = "CommunityBufferList", splitter = ",")
	public static int[] BBS_BUFFER_BUFFS_LIST;
	
	@Setting(name = "CommunityBufferRecover")
	public static boolean BBS_BUFFER_RECOVER;
	
	@Setting(name = "CommunityBufferClear")
	public static boolean BBS_BUFFER_CLEAR;
	
	@Setting(name = "CommunityBufferPeaceRecover")
	public static boolean BBS_BUFFER_PEACE_RECOVER;
	
	@Setting(name = "CommunityBufferRecoverPrice", splitter = ";")
	public static int[] BBS_BUFFER_RECOVER_PRICE;
	
	@Setting(name = "CommunityBufferPremiumPriceMod")
	public static boolean BBS_BUFFER_PA_PRICE_MOD;
	
	@Setting(name = "CommunityBufferPremiumPrice")
	public static double BBS_BUFFER_PA_PRICE;
	
	@Setting(name = "CommunityBufferPremiumList")
	public static String BBS_BUFFS_PA_LIST;
	
	@Setting(name = "CommunityBufferInSiege")
	public static boolean BBS_BUFFER_IN_SIEGE;
	
	@Setting(name = "CommunityBufferInPvP")
	public static boolean BBS_BUFFER_IN_PVP;
	
	@Setting(name = "CommunityBufferInBattle")
	public static boolean BBS_BUFFER_IN_BATTLE;
	
	@Setting(name = "CommunityBufferInEvents")
	public static boolean BBS_BUFFER_IN_EVENTS;
	
	@Setting(name = "CommunityBufferRecoverPvPFlag")
	public static boolean BBS_BUFFER_RECOVER_PVP_FLAG;
	
	@Setting(name = "CommunityBufferRecoverBattle")
	public static boolean BBS_BUFFER_RECOVER_BATTLE;
}
