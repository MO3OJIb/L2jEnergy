/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config.bbs;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;

/**
 * Community Cabinet Configuration.
 * @author Мо3олЬ
 */
@Configuration("bbs/cabinet.json")
public class CCabinetConfig
{
	@Setting(name = "EnableCommunityCabinet")
	public static boolean ENABLE_BBS_CABINET;
	
	@Setting(name = "PremiumItemId")
	public static int BBS_CABINET_PREMIUM_ITEM_ID;
	
	@Setting(name = "Premium1ItemCount")
	public static int BBS_CABINET_PREMIUM_1_ITEM_COUNT;
	
	@Setting(name = "Premium7ItemCount")
	public static int BBS_CABINET_PREMIUM_7_ITEM_COUNT;
	
	@Setting(name = "Premium30ItemCount")
	public static int BBS_CABINET_PREMIUM_30_ITEM_COUNT;
	
	@Setting(name = "Premium60ItemCount")
	public static int BBS_CABINET_PREMIUM_60_ITEM_COUNT;
	
	@Setting(name = "Premium180ItemCount")
	public static int BBS_CABINET_PREMIUM_180_ITEM_COUNT;
	
	@Setting(name = "Premium360ItemCount")
	public static int BBS_CABINET_PREMIUM_360_ITEM_COUNT;
	
	@Setting(name = "ChangePassword")
	public static boolean BBS_CABINET_CHANGE_PASSWORD;
	
	@Setting(name = "EnableServiceTitleColorChange")
	public static boolean BBS_CABINET_ENABLE_TITLE_COLOR_CHANGE;
	
	@Setting(name = "ServiceTitleColorChangeItem")
	public static int BBS_CABINET_TITLE_COLOR_CHANGE_ITEM;
	
	@Setting(name = "ServiceTitleColorChangeCount")
	public static int BBS_CABINET_TITLE_COLOR_CHANGE_COUNT;
	
	@Setting(name = "EnableServiceNickNameColorChange")
	public static boolean BBS_CABINET_NICK_NAME_COLOR_CHANGE;
	
	@Setting(name = "EnableServiceSexChange")
	public static boolean BBS_CABINET_ENABLE_SEX_CHANGE;
	
	@Setting(name = "ServiceSexChangeItem")
	public static int BBS_CABINET_SEX_CHANGE_ITEM;
	
	@Setting(name = "ServiceSexChangeCount")
	public static int BBS_CABINET_SEX_CHANGE_COUNT;
	
	@Setting(name = "EnableServicePkReduceChange")
	public static boolean BBS_CABINET_ENABLE_PK_REDUCE_CHANGE;
	
	@Setting(name = "ServicePkReduceChangeItem")
	public static int BBS_CABINET_PK_REDUCE_CHANGE_ITEM;
	
	@Setting(name = "ServicePkReduceChangeCount")
	public static long BBS_CABINET_PK_REDUCE_CHANGE_COUNT;
	
	@Setting(name = "EnableServiceNoblesseChange")
	public static boolean BBS_CABINET_ENABLE_NOBLESSE_CHANGE;
	
	@Setting(name = "ServiceNoblesseChangeItem")
	public static int BBS_CABINET_NOBLESSE_CHANGE_ITEM;
	
	@Setting(name = "ServiceNoblesseChangeCount")
	public static long BBS_CABINET_NOBLESSE_CHANGE_COUNT;
	
	@Setting(name = "EnableServiceNickNameChange")
	public static boolean BBS_CABINET_ENABLE_NICK_NAME_CHANGE;
	
	@Setting(name = "ServiceNickNameChangeItem")
	public static int BBS_CABINET_NICK_NAME_CHANGE_ITEM;
	
	@Setting(name = "ServiceNickNameChangeCount")
	public static long BBS_CABINET_NICK_NAME_CHANGE_COUNT;
}
