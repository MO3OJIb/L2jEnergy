/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config.bbs;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;

/**
 * Community Forge Configuration.
 * @author Мо3олЬ
 */
@Configuration("bbs/forge.json")
public class CForgeConfig
{
	@Setting(name = "EnableCommunityForge")
	public static boolean BBS_FORGE_ENABLED;
	
	@Setting(name = "CommunityForgeItemId")
	public static int BBS_FORGE_ENCHANT_ITEM;
	
	@Setting(name = "CommunityForgeMaxEnchant", splitter = ",")
	public static int[] BBS_FORGE_ENCHANT_MAX;
	
	@Setting(name = "CommunityForgeWeaponValue", splitter = ",")
	public static int[] BBS_FORGE_WEAPON_ENCHANT_LVL;
	
	@Setting(name = "CommunityForgeArmorValue", splitter = ",")
	public static int[] BBS_FORGE_ARMOR_ENCHANT_LVL;
	
	@Setting(name = "CommunityForgeJewelsValue", splitter = ",")
	public static int[] BBS_FORGE_JEWELS_ENCHANT_LVL;
	
	@Setting(name = "CommunityForgeeWeaponPrice", splitter = ",")
	public static int[] BBS_FORGE_ENCHANT_PRICE_WEAPON;
	
	@Setting(name = "CommunityForgeArmorPrice", splitter = ",")
	public static int[] BBS_FORGE_ENCHANT_PRICE_ARMOR;
	
	@Setting(name = "CommunityForgeJewelsPrice", splitter = ",")
	public static int[] BBS_FORGE_ENCHANT_PRICE_JEWELS;
}
