/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config.custom;

import java.util.ArrayList;
import java.util.List;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;

/**
 * Custom Configuration.
 * @author Мо3олЬ
 */
@Configuration("custom/custom.json")
public class CustomConfig
{
	@Setting(name = "EnableWarehouseSortingClan")
	public static boolean ENABLE_WAREHOUSESORTING_CLAN;
	
	@Setting(name = "EnableWarehouseSortingPrivate")
	public static boolean ENABLE_WAREHOUSESORTING_PRIVATE;
	
	@Setting(name = "MultiLangEnable")
	public static boolean ENABLE_MULTILANG;
	
	@Setting(name = "MultiLangDefault")
	public static String MULTILANG_DEFAULT;
	
	@Setting(name = "MultiLangAllowed", method = "listlang")
	public static List<String> MULTILANG_ALLOWED;
	
	@Setting(name = "MultiLangVoiceCommand")
	public static boolean MULTILANG_VOICED_ALLOW;
	
	@Setting(name = "MultiLangSystemMessageEnable")
	public static boolean MULTILANG_SM_ENABLE;
	
	@Setting(name = "MultiLangSystemMessageAllowed", splitter = ";", method = "listlangSM", canNull = true)
	public static List<String> MULTILANG_SM_ALLOWED = new ArrayList<>();
	
	@Setting(name = "MultiLangNpcStringEnable")
	public static boolean MULTILANG_NS_ENABLE;
	
	@Setting(name = "MultiLangNpcStringAllowed", splitter = ";", method = "listlangNS", canNull = true)
	public static List<String> MULTILANG_NS_ALLOWED = new ArrayList<>();
	
	@Setting(name = "ClientLanguageCheck")
	public static boolean ALT_ENABLE_CLIENT_LANGUAGE_CHECK;
	
	@Setting(name = "BankingEnabled")
	public static boolean BANKING_SYSTEM_ENABLED;
	
	@Setting(name = "BankingGoldbarCount")
	public static int BANKING_SYSTEM_GOLDBARS;
	
	@Setting(name = "BankingAdenaCount")
	public static int BANKING_SYSTEM_ADENA;
	
	@Setting(name = "HellboundStatus")
	public static boolean HELLBOUND_STATUS;
	
	@Setting(name = "AllowChangePassword")
	public static boolean ALLOW_CHANGE_PASSWORD;
	
	@Setting(name = "AllowRepairVoiceCommand")
	public static boolean ALLOW_REPAIR_VOICE_COMMAND;
	
	@Setting(name = "AllowHelpVoiceCommand")
	public static boolean ALLOW_HELP_VOICE_COMMAND;
	
	@Setting(name = "AllowDebugVoiceCommand")
	public static boolean ALLOW_DEBUG_VOICE_COMMAND;
	
	@Setting(name = "AllowMenuVoiceCommand")
	public static boolean ALLOW_MENU_VOICE_COMMAND;
	
	@Setting(name = "AutoLootHerbsList", splitter = ",")
	public static int[] AUTO_LOOT_HERBS_LIST;
	
	@Setting(name = "AutoLootItemsList", splitter = ",")
	public static int[] AUTO_LOOT_ITEMS_LIST;
	
	@Setting(name = "PreventedBuffsList", splitter = ",")
	public static int[] PREVENTED_BUFFS_LIST;
	
	@Setting(name = "AllowPingVoiceCommand")
	public static boolean ALLOW_PING_COMMAND;
	
	@Setting(name = "EnableManaPotionSupport")
	public static boolean ENABLE_MANA_POTIONS_SUPPORT;
	
	@Setting(name = "DisplayServerTime")
	public static boolean DISPLAY_SERVER_TIME;
	
	@Setting(name = "ChatAdmin")
	public static boolean CHAT_ADMIN;
	
	@Setting(name = "ScreenWelcomeMessageEnable")
	public static boolean WELCOME_MESSAGE_ENABLED;
	
	@Setting(name = "ScreenWelcomeMessageTime", increase = 1000)
	public static int WELCOME_MESSAGE_TIME;
	
	@Setting(name = "SendStatusTradeJustOffline")
	public static boolean SENDSTATUS_TRADE_JUST_OFFLINE;
	
	@Setting(name = "SendStatusTradeMod")
	public static double SENDSTATUS_TRADE_MOD;
	
	@Setting(name = "ShowOfflineTradeInOnline")
	public static boolean SHOW_OFFLINE_MODE_IN_ONLINE;
	
	@Setting(name = "RemoteWhoLog")
	public static boolean RWHO_LOG;
	
	@Setting(name = "RemoteWhoForceInc")
	public static int RWHO_FORCE_INC;
	
	@Setting(name = "RemoteOnlineKeepStat")
	public static int RWHO_KEEP_STAT;
	
	@Setting(name = "RemoteWhoMaxOnline")
	public static int RWHO_MAX_ONLINE;
	
	@Setting(name = "RemoteWhoSendTrash")
	public static boolean RWHO_SEND_TRASH;
	
	@Setting(name = "RemoteOnlineIncrement")
	public static int RWHO_ONLINE_INCREMENT;
	
	@Setting(name = "RemotePrivStoreFactor")
	public static float RWHO_PRIV_STORE_FACTOR;
	
	@Setting(name = "CustomNpcData")
	public static boolean CUSTOM_NPC_DATA;
	
	@Setting(name = "CustomSkillsLoad")
	public static boolean CUSTOM_SKILLS_LOAD;
	
	@Setting(name = "CustomItemsLoad")
	public static boolean CUSTOM_ITEMS_LOAD;
	
	@Setting(name = "CustomMultisellLoad")
	public static boolean CUSTOM_MULTISELL_LOAD;
	
	@Setting(name = "CustomBuyListLoad")
	public static boolean CUSTOM_BUYLIST_LOAD;
	
	public void listlang(final String value)
	{
		final String[] listlan = value.split(";");
		MULTILANG_ALLOWED = new ArrayList<>(listlan.length);
		
		for (final String type : listlan)
		{
			MULTILANG_ALLOWED.add(type);
		}
		MULTILANG_ALLOWED.contains(MULTILANG_DEFAULT);
	}
	
	public void listlangSM(final String value)
	{
		if (!value.isEmpty())
		{
			MULTILANG_SM_ALLOWED.add(value);
		}
	}
	
	public void listlangNS(final String value)
	{
		if (!value.isEmpty())
		{
			MULTILANG_NS_ALLOWED.add(value);
		}
	}
}
