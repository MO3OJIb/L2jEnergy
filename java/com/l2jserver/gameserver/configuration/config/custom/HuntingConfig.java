/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config.custom;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;

/**
 * Hunting System Configuration.
 * @author Мо3олЬ
 */
@Configuration("custom/huntingSystem.json")
public class HuntingConfig
{
	@Setting(name = "HuntingSystemMaxTime")
	public static int HUNTING_SYSTEM_MAX_TIME;
	
	@Setting(name = "HuntingSystemEffectTime")
	public static int HUNTING_SYSTEM_EFFECT_TIME;
	
	@Setting(name = "HuntingSystemMaxPoints")
	public static int HUNTING_SYSTEM_MAX_POINTS;
	
	@Setting(name = "HuntingSystemVitalityPoints")
	public static int HUNTING_SYSTEM_VITALITY_POINTS;
	
	@Setting(name = "HuntingSystemRegularPoints")
	public static int HUNTING_SYSTEM_REGULAR_POINTS;
	
	@Setting(name = "HuntingSystemRegularPoints2")
	public static int HUNTING_SYSTEM_REGULAR_POINTS_2;
	
	@Setting(name = "HuntingSystemNormalPoints")
	public static int HUNTING_SYSTEM_NORMAL_POINTS;
	
	@Setting(name = "HuntingSystemLevelPoints")
	public static int HUNTING_SYSTEM_LEVEL_POINTS;
	
	@Setting(name = "HuntingSystemLevelPoints2")
	public static int HUNTING_SYSTEM_LEVEL_POINTS_2;
	
	@Setting(name = "HuntingSystemExtraPoints")
	public static boolean HUNTING_SYSTEM_EXTRA_POINTS;
	
	@Setting(name = "HuntingSystemExtraPointsAllTime")
	public static boolean HUNTING_SYSTEM_EXTRA_POINTS_ALL_TIME;
	
	@Setting(name = "HuntingSystemLimit")
	public static boolean HUNTING_SYSTEM_LIMIT;
}
