/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config.custom;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;

/**
 * PvP Reward Configuration.
 * @author Мо3олЬ
 */
@Configuration("custom/pvpReward.json")
public class PvPRewardConfig
{
	@Setting(name = "EnablePvpReward")
	public static boolean ENABLE_PVP_REWARD_SYSTEM;
	
	@Setting(name = "RewardMinPlayerLevel")
	public static int PVP_REWARD_MIN_LEVEL;
	
	@Setting(name = "RewardMinProff")
	public static int PVP_REWARD_MIN_PROFF;
	
	@Setting(name = "RewardMinPlayerUptimeMinutes")
	public static int PVP_REWARD_MIN_UPTIME_MINUTE;
	
	@Setting(name = "RewardPK")
	public static boolean PVP_REWARD_PK_GIVE;
	
	@Setting(name = "RewardOnlyIfNoble")
	public static boolean PVP_REWARD_ONLY_NOBLE_GIVE;
	
	@Setting(name = "SpecialAntiTwinkCharCreateDelay")
	public static boolean PVP_REWARD_SPECIAL_ANTI_TWINK_TIMER;
	
	@Setting(name = "SpecialAntiTwinkDelayInHours")
	public static int PVP_REWARD_HR_NEW_CHAR_BEFORE_GET_ITEM;
	
	@Setting(name = "EquipCheck")
	public static boolean PVP_REWARD_CHECK_EQUIP;
	
	@Setting(name = "MinimumGradeToCheck")
	public static int PVP_REWARD_WEAPON_GRADE_TO_CHECK;
}
