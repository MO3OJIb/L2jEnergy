/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config.custom;

import java.util.Map;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;
import com.l2jserver.gameserver.util.Util;

/**
 * Vote Tops Configuration.
 * @author Мо3олЬ
 */
@Configuration("custom/voteBonus.json")
public class TopsConfig
{
	@Setting(name = "EnableVoteSystem")
	public static boolean ENABLE_VOTE_SYSTEM;
	
	@Setting(name = "TopServerAddress")
	public static String TOP_SERVER_ADDRESS;
	
	@Setting(name = "TopSaveDays")
	public static int TOP_SAVE_DAYS;
	
	@Setting(name = "HopzoneEnable")
	public static boolean ALLOW_HOPZONE_VOTE_REWARD;
	
	@Setting(name = "HopzoneAddress", canNull = true)
	public static String HOPZONE_SERVER_LINK;
	
	@Setting(name = "HopzoneRewardCheckTime")
	public static int HOPZONE_REWARD_CHECK_TIME;
	
	@Setting(name = "HopzoneReward", method = "hopzoneTopReward")
	public static Map<Integer, Integer> HOPZONE_REWARD;
	
	@Setting(name = "L2TopEnabled")
	public static boolean ALLOW_L2TOP_VOTE_REWARD;
	
	@Setting(name = "L2TopAddress", canNull = true)
	public static String L2TOP_SERVER_LINK;
	
	@Setting(name = "L2TopSmsAddress", canNull = true)
	public static String L2TOP_SMS_ADDRESS;
	
	@Setting(name = "L2TopServerPrefix")
	public static String L2TOP_SERVER_PREFIX;
	
	@Setting(name = "L2TopRewardCheckTime")
	public static int L2TOP_REWARD_CHECK_TIME;
	
	@Setting(name = "L2TopReward", method = "l2TopReward")
	public static Map<Integer, Integer> L2TOP_REWARD;
	
	@Setting(name = "MMOTopEnable")
	public static boolean ALLOW_MMO_TOP_VOTE_REWARD;
	
	@Setting(name = "MMOTopAddress", canNull = true)
	public static String MMOTOP_SERVER_LINK;
	
	@Setting(name = "MMOTopRewardCheckTime")
	public static int MMOTOP_REWARD_CHECK_TIME;
	
	@Setting(name = "MMOTopReward", method = "mmoTopReward")
	public static Map<Integer, Integer> MMOTOP_REWARD;
	
	@Setting(name = "NetworkEnable")
	public static boolean ALLOW_NETWORK_VOTE_REWARD;
	
	@Setting(name = "NetworkAddress", canNull = true)
	public static String NETWORK_SERVER_LINK;
	
	@Setting(name = "NetworkRewardCheckTime")
	public static int NETWORK_REWARD_CHECK_TIME;
	
	@Setting(name = "NetworkReward", method = "networkTopReward")
	public static Map<Integer, Integer> NETWORK_REWARD;
	
	@Setting(name = "TopZonekEnable")
	public static boolean ALLOW_TOPZONE_VOTE_REWARD;
	
	@Setting(name = "TopZonekAddress", canNull = true)
	public static String TOPZONE_SERVER_LINK;
	
	@Setting(name = "TopZoneRewardCheckTime")
	public static int TOPZONE_REWARD_CHECK_TIME;
	
	@Setting(name = "TopZoneReward", method = "topzoneReward")
	public static Map<Integer, Integer> TOPZONE_REWARD;
	
	public void hopzoneTopReward(String value)
	{
		HOPZONE_REWARD = Util.parseConfigMap(value, Integer.class, Integer.class);
	}
	
	public void l2TopReward(String value)
	{
		L2TOP_REWARD = Util.parseConfigMap(value, Integer.class, Integer.class);
	}
	
	public void mmoTopReward(String value)
	{
		MMOTOP_REWARD = Util.parseConfigMap(value, Integer.class, Integer.class);
	}
	
	public void networkTopReward(String value)
	{
		NETWORK_REWARD = Util.parseConfigMap(value, Integer.class, Integer.class);
	}
	
	public void topzoneReward(String value)
	{
		TOPZONE_REWARD = Util.parseConfigMap(value, Integer.class, Integer.class);
	}
}
