/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config.events;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;

/**
 * Handy Block Events Configuration.
 * @author Мо3олЬ
 */
@Configuration("events/handyBlock.json")
public class HandyBlockConfig
{
	@Setting(name = "EnableBlockCheckerEvent")
	public static boolean BLOCK_CHECKER_EVENT_ENABLE;
	
	@Setting(name = "BlockCheckerMinTeamMembers", minValue = 1, maxValue = 6)
	public static int BLOCK_CHECKER_EVENT_MIN_TEAM_MEMBERS;
	
	@Setting(name = "BlockCheckerFairPlay")
	public static boolean BLOCK_CHECKER_EVENT_FAIR_PLAY;
}
