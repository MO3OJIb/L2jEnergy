/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config.events;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;

/**
 * Krateis Cube Events Configuration.
 * @author Мо3олЬ
 */
@Configuration("events/krateisCube.json")
public class KrateisCubeConfig
{
	@Setting(name = "KrateisCubeEventEnabled")
	public static boolean KRATEIS_CUBE_EVENT_ENABLED;
	
	@Setting(name = "KrateisCubeEventRegistrationTime")
	public static int KRATEIS_CUBE_REGISTRATION_TIME;
	
	@Setting(name = "KrateisCubeEventRunningTime")
	public static int KRATEIS_CUBE_RUNNING_TIME;
	
	@Setting(name = "KrateisCubeEventTimeToRevive")
	public static int KRATEIS_CUBE_TIME_TO_REVIVE;
	
	@Setting(name = "KrateisCubeEventTimeOfProtection")
	public static int KRATEIS_CUBE_TIME_OF_PROTECTION;
	
	@Setting(name = "KrateisCubeEventMinPlayer")
	public static int KRATEIS_CUBE_MIN_PLAYER;
	
	@Setting(name = "KrateisCubeEventMaxPlayer")
	public static int KRATEIS_CUBE_MAX_PLAYER;
	
	@Setting(name = "KrateisCubeEventRewardToKillPlayer")
	public static int KRATEIS_REWARD_TO_KILL_PLAYER;
	
	@Setting(name = "KrateisCubeEventRewardToKillMob")
	public static int KRATEIS_REWARD_TO_KILL_MOB;
}
