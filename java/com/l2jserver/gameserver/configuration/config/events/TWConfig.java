/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config.events;

/**
 * TW Event Configuration.
 * @author Мо3олЬ
 */
public class TWConfig
{
	public int TW_TOWN_ID;
	public static String TW_TOWN_NAME;
	public boolean TW_ALL_TOWNS;
	public boolean TW_AUTO_EVENT;
	public String[] TW_INTERVAL;
	public int TW_TIME_BEFORE_START;
	public int TW_RUNNING_TIME;
	public int TW_ITEM_ID;
	public int TW_ITEM_AMOUNT;
	public boolean TW_GIVE_PVP_AND_PK_POINTS;
	public boolean TW_ALLOW_KARMA;
	public boolean TW_DISABLE_GK;
	public boolean TW_RESS_ON_DEATH;
	public boolean TW_LOSE_BUFFS_ON_DEATH;
	
	/*
	 * TW_TOWN_ID = Integer.parseInt(TownWarFiles.getProperty("TownWarTownId", "9")); TW_TOWN_NAME = TownWarFiles.getProperty("TownWarTownName", "Giran Town"); TW_ALL_TOWNS = Boolean.parseBoolean(TownWarFiles.getProperty("TownWarAllTowns", "False")); TW_AUTO_EVENT =
	 * Boolean.parseBoolean(TownWarFiles.getProperty("TownWarAutoEvent", "false")); TW_INTERVAL = TownWarFiles.getProperty("TownWarInterval", "20:00").split(","); TW_TIME_BEFORE_START = Integer.parseInt(TownWarFiles.getProperty("TownWarTimeBeforeStart", "3600")); TW_RUNNING_TIME =
	 * Integer.parseInt(TownWarFiles.getProperty("TownWarRunningTime", "1800")); TW_ITEM_ID = Integer.parseInt(TownWarFiles.getProperty("TownWarItemId", "57")); TW_ITEM_AMOUNT = Integer.parseInt(TownWarFiles.getProperty("TownWarItemAmount", "5000")); TW_GIVE_PVP_AND_PK_POINTS =
	 * Boolean.parseBoolean(TownWarFiles.getProperty("TownWarGivePvPAndPkPoints", "False")); TW_ALLOW_KARMA = Boolean.parseBoolean(TownWarFiles.getProperty("TownWarAllowKarma", "False")); TW_DISABLE_GK = Boolean.parseBoolean(TownWarFiles.getProperty("TownWarDisableGK", "True")); TW_RESS_ON_DEATH =
	 * Boolean.parseBoolean(TownWarFiles.getProperty("TownWarRessOnDeath", "True")); TW_LOSE_BUFFS_ON_DEATH = Boolean.parseBoolean(TownWarFiles.getProperty("TownWarLoseBuffsOnDeath", "False"));
	 */
	public static TWConfig getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final TWConfig INSTANCE = new TWConfig();
	}
}
