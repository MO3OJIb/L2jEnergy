/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config.events;

import java.util.ArrayList;
import java.util.List;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;
import com.l2jserver.commons.lang.StringUtil;

/**
 * Underground Coliseum Configuration.
 * @author Мо3олЬ
 */
@Configuration("events/undergroundColiseum.json")
public class UCConfig
{
	@Setting(name = "DayBattle", splitter = ",", method = "dayBattle")
	public static List<Integer> UC_BATTLE_DAY = new ArrayList<>();
	
	@Setting(name = "HourStartBattle")
	public static int UC_HOUR_START;
	
	@Setting(name = "HourStopBattle")
	public static int UC_HOUR_STOP;
	
	@Setting(name = "RoundTime")
	public static int UC_ROUND_TIME;
	
	@Setting(name = "PartyLimit")
	public static int UC_PARTY_LIMIT;
	
	@Setting(name = "ReputationReward1")
	public static int UC_REWARD_1;
	
	@Setting(name = "ReputationReward2")
	public static int UC_REWARD_2;
	
	@Setting(name = "ReputationReward3")
	public static int UC_REWARD_3;
	
	@Setting(name = "ReputationReward4")
	public static int UC_REWARD_4;
	
	@Setting(name = "ReputationReward5")
	public static int UC_REWARD_5;
	
	@Setting(name = "ReputationReward6")
	public static int UC_REWARD_6;
	
	@Setting(name = "ReputationReward7")
	public static int UC_REWARD_7;
	
	@Setting(name = "ReputationReward8")
	public static int UC_REWARD_8;
	
	@Setting(name = "ReputationReward9")
	public static int UC_REWARD_9;
	
	@Setting(name = "ReputationReward10")
	public static int UC_REWARD_10;
	
	@Setting(name = "ReputationRewardBonus")
	public static int UC_REWARD_BONUS;
	
	public void dayBattle(String value)
	{
		if (StringUtil.isDigit(value))
		{
			UC_BATTLE_DAY.add(Integer.parseInt(value));
		}
	}
}
