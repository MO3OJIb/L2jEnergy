/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config.protection;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;
import com.l2jserver.gameserver.enums.IllegalActionPunishmentType;

/**
 * Base Protection Configuration.
 * @author Мо3олЬ
 */
@Configuration("protection/baseSetting.json")
public class BaseProtectionConfig
{
	@Setting(name = "AntiFeedEnable")
	public static boolean ANTIFEED_ENABLE;
	
	@Setting(name = "AntiFeedDualbox")
	public static boolean ANTIFEED_DUALBOX;
	
	@Setting(name = "AntiFeedDisconnectedAsDualbox")
	public static boolean ANTIFEED_DISCONNECTED_AS_DUALBOX;
	
	@Setting(name = "AntiFeedInterval", increase = 1000)
	public static int ANTIFEED_INTERVAL;
	
	@Setting(name = "L2WalkerProtection")
	public static boolean L2WALKER_PROTECTION;
	
	@Setting(name = "DualboxCheckMaxPlayersPerIP")
	public static int DUALBOX_CHECK_MAX_PLAYERS_PER_IP;
	
	@Setting(name = "DualboxCheckMaxOlympiadParticipantsPerIP")
	public static int DUALBOX_CHECK_MAX_OLYMPIAD_PARTICIPANTS_PER_IP;
	
	@Setting(name = "DualboxCheckWhitelist", method = "whitelist")
	public static Map<Integer, Integer> DUALBOX_CHECK_WHITELIST;
	
	@Setting(name = "DefaultPunish", method = "legalAction")
	public static IllegalActionPunishmentType DEFAULT_PUNISH;
	
	@Setting(name = "DefaultPunishParam")
	public static int DEFAULT_PUNISH_PARAM;
	
	@Setting(name = "OnlyGMItemsFree")
	public static boolean ONLY_GM_ITEMS_FREE;
	
	@Setting(name = "JailIsPvp")
	public static boolean JAIL_IS_PVP;
	
	@Setting(name = "JailDisableChat")
	public static boolean JAIL_DISABLE_CHAT;
	
	@Setting(name = "JailDisableTransaction")
	public static boolean JAIL_DISABLE_TRANSACTION;
	
	public void whitelist(final String value)
	{
		final String[] dualboxCheckWhiteList = value.split(";");
		DUALBOX_CHECK_WHITELIST = new HashMap<>(dualboxCheckWhiteList.length);
		
		for (String entry : dualboxCheckWhiteList)
		{
			String[] entrySplit = entry.split(",");
			if (entrySplit.length != 2)
			{
				//
			}
			else
			{
				try
				{
					int num = Integer.parseInt(entrySplit[1]);
					num = (num == 0) ? -1 : num;
					DUALBOX_CHECK_WHITELIST.put(InetAddress.getByName(entrySplit[0]).hashCode(), num);
				}
				catch (UnknownHostException e)
				{
					// LOG.warn("DualboxCheck[BaseProtectionConfig.load()]: invalid address -> DualboxCheckWhitelist {}", entrySplit[0]);
				}
				catch (NumberFormatException e)
				{
					// LOG.warn("DualboxCheck[BaseProtectionConfig.load()]: invalid number -> DualboxCheckWhitelist {}", entrySplit[1]);
				}
			}
		}
	}
	
	public void legalAction(String value)
	{
		DEFAULT_PUNISH = IllegalActionPunishmentType.findByName(value);
	}
}
