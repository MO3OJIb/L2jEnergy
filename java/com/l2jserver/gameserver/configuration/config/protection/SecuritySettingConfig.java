/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.config.protection;

import com.l2jserver.commons.configuration.annotations.Configuration;
import com.l2jserver.commons.configuration.annotations.Setting;

/**
 * @author Мо3олЬ
 */
@Configuration("protection/securitySetting.json")
public class SecuritySettingConfig
{
	@Setting(name = "EnableLockSecurity")
	public static boolean ENABLE_LOCK_SECURITY;
	
	@Setting(name = "AllowLockIP")
	public static boolean ALLOW_LOCK_IP;
	
	@Setting(name = "AllowLockHwid")
	public static boolean ALLOW_LOCK_HWID;
	
	@Setting(name = "HwidLockMask")
	public static int HWID_LOCK_MASK;
}
