/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.configuration.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet6Address;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.data.IXmlReader;

/**
 * IP Config Configuration.
 * @author Мо3олЬ
 */
public class IPConfigDataParser implements IXmlReader
{
	private final String IP_CONFIG_FILE = "configuration/ipconfig.xml";
	
	private static final List<String> _subnets = new ArrayList<>(5);
	private static final List<String> _hosts = new ArrayList<>(5);
	
	public IPConfigDataParser()
	{
		load();
	}
	
	@Override
	public void load()
	{
		StringUtil.printSection("Network Configuration");
		var f = new File(IP_CONFIG_FILE);
		if (f.exists())
		{
			LOG.info("Loaded existing ipconfig.xml.");
			parseFile(new File(IP_CONFIG_FILE));
		}
		else
		{
			LOG.info("Loaded automatic network configuration.");
			autoIpConfig();
		}
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		NamedNodeMap attrs;
		for (var n = doc.getFirstChild(); n != null; n = n.getNextSibling())
		{
			if ("gameserver".equalsIgnoreCase(n.getNodeName()))
			{
				for (var d = n.getFirstChild(); d != null; d = d.getNextSibling())
				{
					if ("define".equalsIgnoreCase(d.getNodeName()))
					{
						attrs = d.getAttributes();
						_subnets.add(attrs.getNamedItem("subnet").getNodeValue());
						_hosts.add(attrs.getNamedItem("address").getNodeValue());
						
						if (_hosts.size() != _subnets.size())
						{
							LOG.warn("Failed to load {} file - subnets does not match server addresses.", IP_CONFIG_FILE);
						}
					}
				}
				
				var att = n.getAttributes().getNamedItem("address");
				if (att == null)
				{
					LOG.warn("Failed to load {} file - default server address is missing.", IP_CONFIG_FILE);
					_hosts.add("127.0.0.1");
				}
				else
				{
					_hosts.add(att.getNodeValue());
				}
				_subnets.add("0.0.0.0/0");
			}
		}
	}
	
	protected void autoIpConfig()
	{
		String externalIp;
		try
		{
			var autoIp = URI.create("http://ip1.dynupdate.no-ip.com:8245/").toURL();
			try (var in = new BufferedReader(new InputStreamReader(autoIp.openStream())))
			{
				externalIp = in.readLine();
			}
		}
		catch (IOException e)
		{
			LOG.warn("Failed to connect to aip1.dynupdate.no-ip.com:8245 please check your internet connection using 127.0.0.1!");
			externalIp = "127.0.0.1";
		}
		
		try
		{
			var niList = NetworkInterface.getNetworkInterfaces();
			
			while (niList.hasMoreElements())
			{
				var ni = niList.nextElement();
				
				if (!ni.isUp() || ni.isVirtual())
				{
					continue;
				}
				
				if (!ni.isLoopback() && ((ni.getHardwareAddress() == null) || (ni.getHardwareAddress().length != 6)))
				{
					continue;
				}
				
				for (var ia : ni.getInterfaceAddresses())
				{
					if (ia.getAddress() instanceof Inet6Address)
					{
						continue;
					}
					
					var hostAddress = ia.getAddress().getHostAddress();
					var subnetPrefixLength = ia.getNetworkPrefixLength();
					var subnetMaskInt = IntStream.rangeClosed(1, subnetPrefixLength).reduce((r, e) -> (r << 1) + 1).orElse(0) << (32 - subnetPrefixLength);
					var hostAddressInt = Arrays.stream(hostAddress.split("\\.")).mapToInt(Integer::parseInt).reduce((r, e) -> (r << 8) + e).orElse(0);
					var subnetAddressInt = hostAddressInt & subnetMaskInt;
					var subnetAddress = ((subnetAddressInt >> 24) & 0xFF) + "." + ((subnetAddressInt >> 16) & 0xFF) + "." + ((subnetAddressInt >> 8) & 0xFF) + "." + (subnetAddressInt & 0xFF);
					var subnet = subnetAddress + '/' + subnetPrefixLength;
					if (!_subnets.contains(subnet) && !subnet.equals("0.0.0.0/0"))
					{
						_subnets.add(subnet);
						_hosts.add(hostAddress);
						LOG.info("Loaded new subnet: " + subnet + " address: " + hostAddress);
					}
				}
			}
			
			// External host and subnet
			_hosts.add(externalIp);
			_subnets.add("0.0.0.0/0");
			LOG.info("Loaded new subnet: 0.0.0.0/0 address: {}", externalIp);
		}
		catch (SocketException e)
		{
			LOG.error("Configuration failed please manually configure ipconfig.xml", e);
			System.exit(0);
		}
	}
	
	public List<String> getSubnets()
	{
		if (_subnets.isEmpty())
		{
			return Arrays.asList("0.0.0.0/0");
		}
		return _subnets;
	}
	
	public List<String> getHosts()
	{
		if (_hosts.isEmpty())
		{
			return Arrays.asList("127.0.0.1");
		}
		return _hosts;
	}
	
	public static IPConfigDataParser getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final IPConfigDataParser INSTANCE = new IPConfigDataParser();
	}
}
