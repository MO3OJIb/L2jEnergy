/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao;

import java.util.List;
import java.util.Map;

import com.l2jserver.gameserver.model.L2Clan;
import com.l2jserver.gameserver.model.TowerSpawn;
import com.l2jserver.gameserver.model.entity.Castle;
import com.l2jserver.gameserver.model.entity.Castle.CastleFunction;

/**
 * Castle DAO interface.
 * @author Мо3олЬ
 */
public interface CastleDAO
{
	void loadDoorUpgrade(Castle castle, int residenceId);
	
	void setDoorUpgrade(int doorId, int ratio, int residenceId);
	
	void removeDoorUpgrade(int residenceId);
	
	Map<Integer, CastleFunction> loadFunctions(int residenceId);
	
	void setFunction(int residenceId, int type, int lvl, int lease, long rate, long endDate);
	
	void removeFunction(int residenceId, int functionType);
	
	boolean addTreasury(long treasury, int residenceId);
	
	void updateTaxPercent(int taxPercent, int residenceId);
	
	void updateTicket(int ticketBuyCount, int residenceId);
	
	void updateShowNpcCrest(boolean showNpcCrest, int residenceId, String name);
	
	String getOwnerCastle(int castleId);
	
	void updateOwner(L2Clan clan, int residenceId, int ownerId);
	
	Map<Integer, List<TowerSpawn>> loadTrapUpgrade(int castleId);
	
	void setTrapUpgrade(int residenceId, int towerIndex, int level);
	
	void removeTrap(int residenceId);
}
