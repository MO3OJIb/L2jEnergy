/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao;

import java.util.Map;

import com.l2jserver.gameserver.model.L2Clan;

/**
 * Clan DAO interface.
 * @author Мо3олЬ
 * @author Zoey76
 */
public interface ClanDAO
{
	void storeClan(int clanId, String name, int level, int castleId, int bloodAllianceCount, int bloodOathCount, int allyId, String allyName, int leader, int crestId, int crestLargeId, int allyCrestId, int newLeaderId);
	
	void updateClan(int leader, int allyId, String allyName, int reputationScore, long allyPenaltyExpiryTime, int allyPenaltyType, long charPenaltyExpiryTime, long dissolvingExpiryTime, int newLeaderId, int clanId);
	
	void restoreClan(L2Clan clan); // Переделать
	
	void updateSubPledge(L2Clan clan, int pledgeType); // Переделать
	
	void changeLevel(int level, int clan);
	
	void changeClanCrest(int crestId, int clan);
	
	void changeLargeCrest(int crestId, int clan);
	
	void removeMember(int playerId, long clanJoinExpiryTime, long clanCreateExpiryTime);
	
	void setNotice(int clan, String notice, boolean enabled);
	
	void updateClanScore(int reputationScore, int clan);
	
	void updateBloodOathCount(int bloodOath, int clan);
	
	void updateBloodAllianceCount(int bloodAlliance, int clan);
	
	void updateClanPrivsOld(int leader);
	
	void updateClanPrivsNew(int leader);
	
	Map<Integer, Integer> getPrivileges(int clanId);
	
	void storePrivileges(int clanId, int rank, int privileges);
	
	void updatePledgeType(int pledgeType, int charId);
	
	void updatePowerGrade(int powerGrade, int charId);
	
	void saveApprenticeAndSponsor(int apprentice, int sponsor, int charId);
}
