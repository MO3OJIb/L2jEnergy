/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao;

/**
 * Lottery DAO interface.
 * @author Мо3олЬ
 */
public interface LotteryDAO
{
	void add(int lotteryId, long endTime, long prize);
	
	void update(int lotteryId, long prize, long newPrize, long prize1, long prize2, long prize3, int enchant, int type2);
	
	void select(int lotteryId, int count1, int count2, int count3, int count4, int enchant, int type2);
	
	void updatePrize(int lotteryId, long newPrize);
	
	long[] checkTicket(int id, int enchant, int type2);
}
