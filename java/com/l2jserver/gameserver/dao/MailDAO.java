/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao;

import java.util.Map;

import com.l2jserver.gameserver.model.entity.Message;

/**
 * Mail DAO interface.
 * @author Мо3олЬ
 */
public interface MailDAO
{
	Map<Integer, Message> load();
	
	void markAsRead(int message);
	
	void markAsDeletedBySender(int message);
	
	void markAsDeletedByReceiver(int message);
	
	void removeAttachments(int message);
	
	void send(int id, int senderId, int receiverId, String subject, String content, long expiration, long reqAdena, boolean hasAttachments, boolean unread, boolean deletedBySender, boolean deletedByReceiver, int sendBySystem, boolean returned);
	
	void delete(int message);
}
