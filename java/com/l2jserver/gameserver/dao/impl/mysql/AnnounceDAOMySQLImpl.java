/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.AnnounceDAO;
import com.l2jserver.gameserver.enums.AnnouncementType;

/**
 * @author Мо3олЬ
 */
public class AnnounceDAOMySQLImpl implements AnnounceDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(AnnounceDAOMySQLImpl.class);
	
	private static final String INSERT_AUTO = "INSERT INTO announcements (`type`, `content`, `author`, `initial`, `delay`, `repeat`) VALUES (?, ?, ?, ?, ?, ?)";
	private static final String UPDATE_AUTO = "UPDATE announcements SET `type` = ?, `content` = ?, `author` = ?, `initial` = ?, `delay` = ?, `repeat` = ? WHERE id = ?";
	private static final String INSERT_QUERY = "INSERT INTO announcements (type, content, author) VALUES (?, ?, ?)";
	private static final String UPDATE_QUERY = "UPDATE announcements SET type = ?, content = ?, author = ? WHERE id = ?";
	private static final String DELETE_QUERY = "DELETE FROM announcements WHERE id = ?";
	
	@Override
	public void store(AnnouncementType type, String content, String author, long initial, long delay, int repeat, int id)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT_AUTO, Statement.RETURN_GENERATED_KEYS))
		{
			ps.setInt(1, type.ordinal());
			ps.setString(2, content);
			ps.setString(3, author);
			ps.setLong(4, initial);
			ps.setLong(5, delay);
			ps.setInt(6, repeat);
			ps.execute();
			try (var rset = ps.getGeneratedKeys())
			{
				if (rset.next())
				{
					id = rset.getInt(1);
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Couldn't store announcement!", ex);
		}
	}
	
	@Override
	public void storeMe(AnnouncementType type, String content, String author, int id)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS))
		{
			ps.setInt(1, type.ordinal());
			ps.setString(2, content);
			ps.setString(3, author);
			ps.execute();
			try (var rset = ps.getGeneratedKeys())
			{
				if (rset.next())
				{
					id = rset.getInt(1);
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not store announcement!", ex);
		}
	}
	
	@Override
	public void updateMe(AnnouncementType type, String content, String author, int id)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_QUERY))
		{
			ps.setInt(1, type.ordinal());
			ps.setString(2, content);
			ps.setString(3, author);
			ps.setInt(4, id);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Couldn't store announcement!", ex);
		}
	}
	
	@Override
	public void update(AnnouncementType type, String content, String author, long initial, long delay, int repeat, int id)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_AUTO))
		{
			ps.setInt(1, type.ordinal());
			ps.setString(2, content);
			ps.setString(3, author);
			ps.setLong(4, initial);
			ps.setLong(5, delay);
			ps.setLong(6, repeat);
			ps.setLong(7, id);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Couldn't update announcement!", ex);
		}
	}
	
	@Override
	public void deleteMe(int id)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_QUERY))
		{
			ps.setInt(1, id);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Couldn't remove announcement!", ex);
		}
	}
}
