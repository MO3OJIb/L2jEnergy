/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.dao.BotReportDAO;
import com.l2jserver.gameserver.model.reported.ReportedCharData;
import com.l2jserver.gameserver.model.reported.ReporterCharData;

/**
 * BotReport DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class BotReportDAOMySQLImpl implements BotReportDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(BotReportDAOMySQLImpl.class);
	
	private static final int COLUMN_BOT_ID = 1;
	private static final int COLUMN_REPORTER_ID = 2;
	private static final int COLUMN_REPORT_TIME = 3;
	
	private static final String LOAD_REPORTED = "SELECT * FROM bot_reported_char_data";
	private static final String INSERT_REPORTED = "INSERT INTO bot_reported_char_data VALUES (?,?,?)";
	private static final String CLEAR_REPORTED = "DELETE FROM bot_reported_char_data";
	
	final Map<Integer, ReporterCharData> _charRegistry = new ConcurrentHashMap<>();
	
	@Override
	public Map<Integer, ReportedCharData> load()
	{
		final Map<Integer, ReportedCharData> reports = new ConcurrentHashMap<>();
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var st = con.createStatement();
			var rs = st.executeQuery(LOAD_REPORTED))
		{
			long lastResetTime = 0;
			try
			{
				var hour = GeneralConfig.BOTREPORT_RESETPOINT_HOUR;
				var c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour[0]));
				c.set(Calendar.MINUTE, Integer.parseInt(hour[1]));
				
				if (System.currentTimeMillis() < c.getTimeInMillis())
				{
					c.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR) - 1);
				}
				lastResetTime = c.getTimeInMillis();
			}
			catch (Exception ex)
			{
				
			}
			
			while (rs.next())
			{
				int botId = rs.getInt(COLUMN_BOT_ID);
				int reporter = rs.getInt(COLUMN_REPORTER_ID);
				long date = rs.getLong(COLUMN_REPORT_TIME);
				if (reports.containsKey(botId))
				{
					reports.get(botId).addReporter(reporter, date);
				}
				else
				{
					var rcd = new ReportedCharData();
					rcd.addReporter(reporter, date);
					reports.put(rs.getInt(COLUMN_BOT_ID), rcd);
				}
				
				if (date > lastResetTime)
				{
					var rcd = _charRegistry.get(reporter);
					if (rcd != null)
					{
						rcd.setPoints(rcd.getPointsLeft() - 1);
					}
					else
					{
						rcd = new ReporterCharData();
						rcd.setPoints(6);
						_charRegistry.put(reporter, rcd);
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not load reported char data!", ex);
		}
		return reports;
	}
	
	@Override
	public Map<Integer, ReportedCharData> save()
	{
		final Map<Integer, ReportedCharData> reports = new ConcurrentHashMap<>();
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var st = con.createStatement();
			var ps = con.prepareStatement(INSERT_REPORTED))
		{
			st.execute(CLEAR_REPORTED);
			
			for (var entrySet : reports.entrySet())
			{
				var reportTable = entrySet.getValue().getReporters();
				for (var reporterId : reportTable.keySet())
				{
					ps.setInt(1, entrySet.getKey());
					ps.setInt(2, reporterId);
					ps.setLong(3, reportTable.get(reporterId));
					ps.execute();
				}
			}
		}
		catch (Exception ex)
		{
			LOG.error("Could not update reported char data in database!", ex);
		}
		return reports;
	}
}
