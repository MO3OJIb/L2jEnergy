/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.CastleDAO;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.enums.audio.Music;
import com.l2jserver.gameserver.model.L2Clan;
import com.l2jserver.gameserver.model.TowerSpawn;
import com.l2jserver.gameserver.model.entity.Castle;
import com.l2jserver.gameserver.model.entity.Castle.CastleFunction;
import com.l2jserver.gameserver.network.serverpackets.PledgeShowInfoUpdate;

/**
 * Castle DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class CastleDAOMySQLImpl implements CastleDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(CastleDAOMySQLImpl.class);
	
	private static final String INTO_CASTLE_DOOR_UPGARADE = "REPLACE INTO castle_doorupgrade (doorId, ratio, castleId) values (?,?,?)";
	private static final String INTO_CASTLE_TRAPUPGRADE = "REPLACE INTO castle_trapupgrade (castleId, towerIndex, level) values (?,?,?)";
	private static final String INTO_CASTLE_FUNCTIONS = "REPLACE INTO castle_functions (castle_id, type, lvl, lease, rate, endTime) VALUES (?,?,?,?,?,?)";
	private static final String SELECT_CASTLE_DOOR_UPGARADE = "SELECT * FROM castle_doorupgrade WHERE castleId=?";
	private static final String SELECT_CASTLE_TRAPUPGRADE = "SELECT * FROM castle_trapupgrade WHERE castleId=?";
	private static final String SELECT_CASTLE_FUNCTIONS = "SELECT * FROM castle_functions WHERE castle_id = ?";
	private static final String SELECT_OWNER_CASTLE = "SELECT clan_id FROM clan_data WHERE hasCastle = ? LIMIT 1";
	private static final String UPDATE_CASTLE_CREST = "UPDATE castle SET showNpcCrest = ? WHERE id = ?";
	private static final String UPDATE_CASTLE_TREASURY = "UPDATE castle SET treasury = ? WHERE id = ?";
	private static final String UPDATE_CASTLE_TAX_PERCENT = "UPDATE castle SET taxPercent = ? WHERE id = ?";
	private static final String UPDATE_CASTLE_TICKET_BUY_COUNT = "UPDATE castle SET ticketBuyCount = ? WHERE id = ?";
	private static final String UPDATE_CASTLE = "UPDATE clan_data SET hasCastle = 0 WHERE hasCastle = ?";
	private static final String UPDATE_CASTLE_ID = "UPDATE clan_data SET hasCastle = ? WHERE clan_id = ?";
	private static final String DELETE_CASTLE_DOOR_UPGARADE = "DELETE FROM castle_doorupgrade WHERE castleId=?";
	private static final String DELETE_CASTLE_TRAPUPGRADE = "DELETE FROM castle_trapupgrade WHERE castleId=?";
	private static final String DELETE_CASTLE_FUNCTIONS = "DELETE FROM castle_functions WHERE castle_id=? AND type=?";
	
	@Override
	public void loadDoorUpgrade(Castle castle, int residenceId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_CASTLE_DOOR_UPGARADE))
		{
			ps.setInt(1, residenceId);
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					castle.setDoorUpgrade(rs.getInt("doorId"), rs.getInt("ratio"), false);
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not load castle door upgrade!", ex);
		}
	}
	
	@Override
	public void setDoorUpgrade(int doorId, int ratio, int residenceId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INTO_CASTLE_DOOR_UPGARADE))
		{
			ps.setInt(1, doorId);
			ps.setInt(2, ratio);
			ps.setInt(3, residenceId);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not set door upgrade!", ex);
		}
	}
	
	@Override
	public void removeDoorUpgrade(int residenceId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_CASTLE_DOOR_UPGARADE))
		{
			ps.setInt(1, residenceId);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not remove door upgrade!", ex);
		}
	}
	
	// TODO; fix
	@Override
	public Map<Integer, CastleFunction> loadFunctions(int residenceId)
	{
		Map<Integer, CastleFunction> function = new ConcurrentHashMap<>();
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_CASTLE_FUNCTIONS))
		{
			ps.setInt(1, residenceId);
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					// function.getFunction().put(rs.getInt("type"), new CastleFunction(rs.getInt("type"), rs.getInt("lvl"), rs.getInt("lease"), 0, rs.getLong("rate"), rs.getLong("endTime"), true));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Exception: Castle.loadFunctions()", ex);
		}
		return function;
	}
	
	@Override
	public void setFunction(int residenceId, int type, int lvl, int lease, long rate, long endDate)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INTO_CASTLE_FUNCTIONS))
		{
			ps.setInt(1, residenceId);
			ps.setInt(2, type);
			ps.setInt(3, lvl);
			ps.setInt(4, lease);
			ps.setLong(5, rate);
			ps.setLong(6, endDate);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not update functions castle.", ex);
		}
	}
	
	@Override
	public void removeFunction(int residenceId, int functionType)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_CASTLE_FUNCTIONS))
		{
			ps.setInt(1, residenceId);
			ps.setInt(2, functionType);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not castle remove functions castle!", ex);
		}
	}
	
	@Override
	public boolean addTreasury(long treasury, int residenceId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_CASTLE_TREASURY))
		{
			ps.setLong(1, treasury);
			ps.setInt(2, residenceId);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("", ex);
		}
		return true;
	}
	
	@Override
	public void updateTaxPercent(int taxPercent, int residenceId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_CASTLE_TAX_PERCENT))
		{
			ps.setInt(1, taxPercent);
			ps.setInt(2, residenceId);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("", ex);
		}
	}
	
	@Override
	public void updateTicket(int ticketBuyCount, int residenceId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_CASTLE_TICKET_BUY_COUNT))
		{
			ps.setInt(1, ticketBuyCount);
			ps.setInt(2, residenceId);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not update ticket!", ex);
		}
	}
	
	@Override
	public void updateShowNpcCrest(boolean showNpcCrest, int residenceId, String name)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_CASTLE_CREST))
		{
			ps.setString(1, String.valueOf(showNpcCrest));
			ps.setInt(2, residenceId);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not saving show npc crest for castle {}!", name, ex);
		}
	}
	
	@Override
	public String getOwnerCastle(int castleId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_OWNER_CASTLE))
		{
			ps.setInt(1, castleId);
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					return ClanTable.getInstance().getClan(rs.getInt("clan_id")).getName();
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("", ex);
		}
		return null;
	}
	
	@Override
	public void updateOwner(L2Clan clan, int residenceId, int ownerId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection())
		{
			// Need to remove has castle flag from clan_data, should be checked from castle table.
			try (var ps = con.prepareStatement(UPDATE_CASTLE))
			{
				ps.setInt(1, residenceId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(UPDATE_CASTLE_ID))
			{
				ps.setInt(1, residenceId);
				ps.setInt(2, ownerId);
				ps.execute();
			}
			
			// Announce to clan members
			if (clan != null)
			{
				clan.setCastleId(residenceId); // Set has castle flag for new owner
				clan.broadcastToOnlineMembers(new PledgeShowInfoUpdate(clan));
				clan.broadcastToOnlineMembers(Music.SIEGE_VICTORY.getPacket());
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not update Owner clan!", ex);
		}
	}
	
	@Override
	public Map<Integer, List<TowerSpawn>> loadTrapUpgrade(int castleId)
	{
		Map<Integer, List<TowerSpawn>> flameTowers = new HashMap<>();
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_CASTLE_TRAPUPGRADE))
		{
			ps.setInt(1, castleId);
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					flameTowers.get(castleId).get(rs.getInt("towerIndex")).setUpgradeLevel(rs.getInt("level"));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not load trap upgrade!", ex);
		}
		return flameTowers;
	}
	
	@Override
	public void setTrapUpgrade(int residenceId, int towerIndex, int level)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INTO_CASTLE_TRAPUPGRADE))
		{
			ps.setInt(1, residenceId);
			ps.setInt(2, towerIndex);
			ps.setInt(3, level);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not set trap upgrade Level!", ex);
		}
	}
	
	@Override
	public void removeTrap(int residenceId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_CASTLE_TRAPUPGRADE))
		{
			ps.setInt(1, residenceId);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not remove door upgrade!", ex);
		}
	}
}