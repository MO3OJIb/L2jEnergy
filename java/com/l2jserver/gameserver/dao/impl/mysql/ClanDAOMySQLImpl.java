/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.util.EnumIntBitmask;
import com.l2jserver.gameserver.configuration.config.ClanConfig;
import com.l2jserver.gameserver.dao.ClanDAO;
import com.l2jserver.gameserver.enums.ClanPrivilege;
import com.l2jserver.gameserver.model.L2Clan;
import com.l2jserver.gameserver.model.L2ClanMember;

/**
 * Clan DAO MySQL implementation.
 * @author Zoey76
 * @author Мо3олЬ
 */
public class ClanDAOMySQLImpl implements ClanDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(ClanDAOMySQLImpl.class);
	
	private static final String INSERT_CLAN_DATA = "INSERT INTO `clan_data` (`clan_id`, `clan_name`, `clan_level`, `hasCastle`, `blood_alliance_count`, `blood_oath_count`, `ally_id`, `ally_name`, `leader_id`, `crest_id`, `crest_large_id`, `ally_crest_id`, `new_leader_id`) values (?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String INSERT_CLAN_PRIVILEGES = "INSERT INTO `clan_privs` (`clan_id`, `rank`, `party`, `privs`) VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE `privs`=?";
	private static final String UPDATE_CLAN_DATA = "UPDATE `clan_data` SET `leader_id` = ?, `ally_id` = ?, `ally_name` = ?, `reputation_score` = ?, `ally_penalty_expiry_time` = ?,`ally_penalty_type` = ?, `char_penalty_expiry_time` = ?, `dissolving_expiry_time` = ?, `new_leader_id` = ? WHERE `clan_id` = ?";
	private static final String SELECT_CLAN_DATA = "SELECT * FROM `clan_data` where `clan_id` = ?";
	private static final String SELECT_CLAN_NOTICE = "SELECT `enabled`, `notice` FROM `clan_notices` WHERE `clan_id` = ?";
	private static final String SELECT_CLAN_PRIVILEGES = "SELECT `privs`, `rank`, `party` FROM `clan_privs` WHERE clan_id=?";
	private static final String UPDATE_CLAN_LEVEL = "UPDATE `clan_data` SET `clan_level` = ? WHERE `clan_id` = ?";
	private static final String UPDATE_CLAN_CREST = "UPDATE `clan_data` SET `crest_id` = ? WHERE `clan_id` = ?";
	private static final String UPDATE_CLAN_CREST_LARGE = "UPDATE `clan_data` SET `crest_large_id` = ? WHERE `clan_id` = ?";
	private static final String UPDATE_CLAN_SUBPLEDGES = "UPDATE `clan_subpledges` SET `leader_id` = ?, `name` = ? WHERE `clan_id` = ? AND `sub_pledge_id` = ?";
	private static final String UPDATE_CLAN_SCORE = "UPDATE `clan_data` SET `reputation_score` = ? WHERE `clan_id` = ?";
	private static final String UPDATE_BLOOD_OATH = "UPDATE `clan_data` SET `blood_oath_count` = ? WHERE `clan_id` = ?";
	private static final String UPDATE_BLOOD_ALLIANCE = "UPDATE `clan_data` SET `blood_alliance_count` = ? WHERE `clan_id` = ?";
	private static final String UPDATE_CLAN_PRIVS = "UPDATE `characters` SET `clan_privs` = ? WHERE `charId` = ?";
	private static final String UPDATE_CLAN_CHAR = "UPDATE `characters` SET `clanid` = 0, `title` = ?, `clan_join_expiry_time` = ?, `clan_create_expiry_time` = ?, `clan_privs` = 0, `wantspeace` = 0, `subpledge` = 0, `lvl_joined_academy` = 0, `apprentice` = 0, `sponsor` = 0 WHERE `charId` = ?";
	private static final String UPDATE_CLAN_CHAR_APPRENTICE = "UPDATE `characters` SET `apprentice` = 0 WHERE `apprentice` = ?";
	private static final String UPDATE_CLAN_CHAR_SPONSOR = "UPDATE `characters` SET `sponsor` = 0 WHERE `sponsor` = ?";
	private static final String INSERT_CLAN_NOTICE = "INSERT INTO `clan_notices` (`clan_id`, `notice`, `enabled`) values (?, ?, ?) ON DUPLICATE KEY UPDATE `notice` = ?, `enabled` = ?";
	private static final String UPDATE_SUBPLEDGE = "UPDATE characters SET subpledge=? WHERE charId=?";
	private static final String UPDATE_POWER_GRADE = "UPDATE characters SET power_grade=? WHERE charId=?";
	private static final String UPDATE_APPRENTICE_SPONSOR = "UPDATE characters SET apprentice=?,sponsor=? WHERE charId=?";
	
	@Override
	public void storeClan(int clanId, String name, int level, int castleId, int bloodAllianceCount, int bloodOathCount, int allyId, String allyName, int leader, int crestId, int crestLargeId, int allyCrestId, int newLeaderId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT_CLAN_DATA))
		{
			ps.setInt(1, clanId);
			ps.setString(2, name);
			ps.setInt(3, level);
			ps.setInt(4, castleId);
			ps.setInt(5, bloodAllianceCount);
			ps.setInt(6, bloodOathCount);
			ps.setInt(7, allyId);
			ps.setString(8, allyName);
			ps.setInt(9, leader);
			ps.setInt(10, crestId);
			ps.setInt(11, crestLargeId);
			ps.setInt(12, allyCrestId);
			ps.setInt(13, newLeaderId);
			ps.execute();
			ps.close();
		}
		catch (Exception ex)
		{
			LOG.error("Error saving new clan!", ex);
		}
	}
	
	@Override
	public void updateClan(int leader, int allyId, String allyName, int reputationScore, long allyPenaltyExpiryTime, int allyPenaltyType, long charPenaltyExpiryTime, long dissolvingExpiryTime, int newLeaderId, int clanId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_CLAN_DATA))
		{
			ps.setInt(1, leader);
			ps.setInt(2, allyId);
			ps.setString(3, allyName);
			ps.setInt(4, reputationScore);
			ps.setLong(5, allyPenaltyExpiryTime);
			ps.setInt(6, allyPenaltyType);
			ps.setLong(7, charPenaltyExpiryTime);
			ps.setLong(8, dissolvingExpiryTime);
			ps.setInt(9, newLeaderId);
			ps.setInt(10, clanId);
			ps.execute();
			ps.close();
		}
		catch (Exception ex)
		{
			LOG.error("Could not saving clan!", ex);
		}
	}
	
	@Override
	public void restoreClan(L2Clan clan)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_CLAN_DATA))
		{
			ps.setInt(1, clan.getId());
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					clan.setName(rs.getString("clan_name"));
					clan.setLevel(rs.getInt("clan_level"));
					clan.setCastleId(rs.getInt("hasCastle"));
					clan.setBloodAllianceCount(rs.getInt("blood_alliance_count"));
					clan.setBloodOathCount(rs.getInt("blood_oath_count"));
					clan.setAllyId(rs.getInt("ally_id"));
					clan.setAllyName(rs.getString("ally_name"));
					clan.setAllyPenaltyExpiryTime(rs.getLong("ally_penalty_expiry_time"), rs.getInt("ally_penalty_type"));
					
					if (clan.getAllyPenaltyExpiryTime() < System.currentTimeMillis())
					{
						clan.setAllyPenaltyExpiryTime(0, 0);
					}
					clan.setCharPenaltyExpiryTime(rs.getLong("char_penalty_expiry_time"));
					
					if ((clan.getCharPenaltyExpiryTime() + (ClanConfig.ALT_CLAN_JOIN_DAYS * 86400000L)) < System.currentTimeMillis()) // 24*60*60*1000 = 86400000
					{
						clan.setCharPenaltyExpiryTime(0);
					}
					clan.setDissolvingExpiryTime(rs.getLong("dissolving_expiry_time"));
					
					clan.setCrestId(rs.getInt("crest_id"));
					clan.setCrestLargeId(rs.getInt("crest_large_id"));
					clan.setAllyCrestId(rs.getInt("ally_crest_id"));
					
					clan.setReputationScore(rs.getInt("reputation_score"));
					clan.setAuctionBiddedAt(rs.getInt("auction_bid_at"), false);
					clan.setNewLeaderId(rs.getInt("new_leader_id"), false);
					
					var leaderId = (rs.getInt("leader_id"));
					
					ps.clearParameters();
					
					try (var ps1 = con.prepareStatement("SELECT `char_name`, `level`, `classid`, `charId`, `title`, `power_grade`, `subpledge`, `apprentice`, `sponsor`, `sex`, `race` FROM `characters` WHERE `clanid` = ?"))
					{
						ps1.setInt(1, clan.getId());
						try (var rs1 = ps1.executeQuery())
						{
							L2ClanMember member = null;
							while (rs1.next())
							{
								member = new L2ClanMember(clan, rs1);
								if (member.getObjectId() == leaderId)
								{
									clan.setLeader(member);
								}
								else
								{
									clan.addClanMember(member);
								}
							}
						}
					}
				}
			}
			clan.restoreSubPledges();
			clan.restoreRankPrivs();
			clan.restoreSkills();
			restoreNotice(clan);
		}
		catch (Exception ex)
		{
			LOG.error("Could not restoring clan data!", ex);
		}
	}
	
	public void restoreNotice(L2Clan clan)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_CLAN_NOTICE))
		{
			ps.setInt(1, clan.getId());
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					clan.setNoticeEnabled(rs.getBoolean("enabled"));
					clan.setNotice(rs.getString("notice"));
				}
				rs.close();
			}
			ps.close();
		}
		catch (Exception ex)
		{
			LOG.error("Error restoring clan notice!", ex);
		}
	}
	
	@Override
	public void changeLevel(int level, int clan)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_CLAN_LEVEL))
		{
			ps.setInt(1, level);
			ps.setInt(2, clan);
			ps.execute();
			ps.close();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not increase clan level!", ex);
		}
	}
	
	@Override
	public void changeClanCrest(int crestId, int clan)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_CLAN_CREST))
		{
			ps.setInt(1, crestId);
			ps.setInt(2, clan);
			ps.executeUpdate();
			ps.close();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not update crest for clan {}!", clan, ex);
		}
	}
	
	@Override
	public void changeLargeCrest(int crestId, int clan)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_CLAN_CREST_LARGE))
		{
			ps.setInt(1, crestId);
			ps.setInt(2, clan);
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not update large crest for clan {}!", clan, ex);
		}
	}
	
	@Override
	public void removeMember(int playerId, long clanJoinExpiryTime, long clanCreateExpiryTime)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps1 = con.prepareStatement(UPDATE_CLAN_CHAR);
			var ps2 = con.prepareStatement(UPDATE_CLAN_CHAR_APPRENTICE);
			var ps3 = con.prepareStatement(UPDATE_CLAN_CHAR_SPONSOR))
		{
			// Remove clan member.
			ps1.setString(1, "");
			ps1.setLong(2, clanJoinExpiryTime);
			ps1.setLong(3, clanCreateExpiryTime);
			ps1.setInt(4, playerId);
			ps1.execute();
			ps1.close();
			// Remove apprentice.
			ps2.setInt(1, playerId);
			ps2.execute();
			ps2.close();
			// Remove sponsor.
			ps3.setInt(1, playerId);
			ps3.execute();
			ps3.close();
		}
		catch (Exception ex)
		{
			LOG.error("Could not removing clan member!", ex);
		}
	}
	
	@Override
	public void updateSubPledge(L2Clan clan, int pledgeType)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_CLAN_SUBPLEDGES))
		{
			ps.setInt(1, clan.getSubPledge(pledgeType).getLeaderId());
			ps.setString(2, clan.getSubPledge(pledgeType).getName());
			ps.setInt(3, clan.getId());
			ps.setInt(4, pledgeType);
			ps.execute();
			ps.close();
		}
		catch (Exception ex)
		{
			LOG.error("Could not updating subpledge!", ex);
		}
	}
	
	@Override
	public void updateClanScore(int reputationScore, int clan)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_CLAN_SCORE))
		{
			ps.setInt(1, reputationScore);
			ps.setInt(2, clan);
			ps.execute();
			ps.close();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not update clan score!", ex);
		}
	}
	
	@Override
	public void updateBloodOathCount(int bloodOath, int clan)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_BLOOD_OATH))
		{
			ps.setInt(1, bloodOath);
			ps.setInt(2, clan);
			ps.execute();
			ps.close();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not update blood oath count!", ex);
		}
	}
	
	@Override
	public void updateBloodAllianceCount(int bloodAlliance, int clan)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_BLOOD_ALLIANCE))
		{
			ps.setInt(1, bloodAlliance);
			ps.setInt(2, clan);
			ps.execute();
			ps.close();
		}
		catch (Exception ex)
		{
			LOG.warn("CCould not update blood alliance count!", ex);
		}
	}
	
	@Override
	public void updateClanPrivsOld(int leader)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_CLAN_PRIVS))
		{
			ps.setInt(1, 0);
			ps.setInt(2, leader);
			ps.execute();
			ps.close();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not update clan privs for old clan leader!", ex);
		}
	}
	
	@Override
	public void updateClanPrivsNew(int leader)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_CLAN_PRIVS))
		{
			ps.setInt(1, EnumIntBitmask.getAllBitmask(ClanPrivilege.class));
			ps.setInt(2, leader);
			ps.execute();
			ps.close();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not update clan privs for new clan leader!", ex);
		}
	}
	
	@Override
	public void setNotice(int clan, String notice, boolean enabled)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT_CLAN_NOTICE))
		{
			ps.setInt(1, clan);
			ps.setString(2, notice);
			if (enabled)
			{
				ps.setString(3, "true");
			}
			else
			{
				ps.setString(3, "false");
			}
			ps.setString(4, notice);
			if (enabled)
			{
				ps.setString(5, "true");
			}
			else
			{
				ps.setString(5, "false");
			}
			ps.execute();
			ps.close();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not store clan notice!", ex);
		}
	}
	
	@Override
	public Map<Integer, Integer> getPrivileges(int clanId)
	{
		final Map<Integer, Integer> result = new HashMap<>();
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_CLAN_PRIVILEGES))
		{
			ps.setInt(1, clanId);
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					var rank = rs.getInt("rank");
					if (rank == -1)
					{
						continue;
					}
					result.put(rank, rs.getInt("privs"));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.error("Unable to restore clan privileges for clan Id {}!", clanId, ex);
		}
		return result;
	}
	
	@Override
	public void storePrivileges(int clanId, int rank, int privileges)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT_CLAN_PRIVILEGES))
		{
			ps.setInt(1, clanId);
			ps.setInt(2, rank);
			ps.setInt(3, 0);
			ps.setInt(4, privileges);
			ps.setInt(5, privileges);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.error("Unable to store clan privileges for clan Id {}!", clanId, ex);
		}
	}
	
	@Override
	public void updatePledgeType(int pledgeType, int charId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_SUBPLEDGE))
		{
			ps.setLong(1, pledgeType);
			ps.setInt(2, charId);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not update pledge type!", ex);
		}
	}
	
	@Override
	public void updatePowerGrade(int powerGrade, int charId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_POWER_GRADE))
		{
			ps.setLong(1, powerGrade);
			ps.setInt(2, charId);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not update power grade!", ex);
		}
	}
	
	@Override
	public void saveApprenticeAndSponsor(int apprentice, int sponsor, int charId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_APPRENTICE_SPONSOR))
		{
			ps.setInt(1, apprentice);
			ps.setInt(2, sponsor);
			ps.setInt(3, charId);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not save apprentice/sponsor!", ex);
		}
	}
}
