/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.CommunityBufferDAO;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.bbs.Scheme;

/**
 * Community Buffer DAO Factory implementation.
 * @author Мо3олЬ
 */
public class CommunityBufferDAOMySQLImpl implements CommunityBufferDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(CommunityBufferDAOMySQLImpl.class);
	
	public static final String SELECT = "SELECT * FROM bbs_buffer WHERE charId=?";
	public static final String INSERT = "REPLACE INTO bbs_buffer (name, charId, buffs) VALUES (?,?,?)";
	public static final String DELETE = "DELETE FROM bbs_buffer WHERE charId=? AND name=?";
	
	@Override
	public void select(L2PcInstance player)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT))
		{
			ps.setInt(1, player.getObjectId());
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					var buffs = rs.getString("buffs");
					var name = rs.getString("name");
					if (!buffs.isEmpty())
					{
						var scheme = new Scheme(name);
						for (var str : buffs.split(";"))
						{
							var arrayOfString2 = str.split(",");
							var id = Integer.parseInt(arrayOfString2[0]);
							var lvl = Integer.parseInt(arrayOfString2[1]);
							scheme.addBuff(id, lvl);
						}
						
						if (scheme.getBuffs().size() > 1)
						{
							player.addBuffScheme(scheme);
						}
					}
					else
					{
						player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_buffer_schema_has_been_removed_doesn_have_buff").replace("%i%", name + ""));
						delete(player, name);
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not restoring community buff scheme for {}!", player, ex);
		}
	}
	
	@Override
	public void insert(L2PcInstance player, String buffs, Scheme scheme)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT))
		{
			ps.setString(1, scheme.getName());
			ps.setInt(2, player.getObjectId());
			ps.setString(3, buffs);
			ps.execute();
			
			player.addBuffScheme(scheme);
		}
		catch (Exception ex)
		{
			LOG.warn("Could not add community buff scheme for {}!", player, ex);
		}
	}
	
	@Override
	public void delete(L2PcInstance player, String name)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE))
		{
			ps.setInt(1, player.getObjectId());
			ps.setString(2, name);
			ps.execute();
			
			player.deleteBuffScheme(name);
		}
		catch (Exception ex)
		{
			LOG.warn("Could not delete community buff scheme for {}!", player, ex);
		}
	}
}
