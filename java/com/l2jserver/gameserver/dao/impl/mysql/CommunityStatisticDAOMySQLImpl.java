/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.configuration.config.bbs.CStatsConfig;
import com.l2jserver.gameserver.dao.CommunityStatisticDAO;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.data.xml.impl.NpcData;
import com.l2jserver.gameserver.model.bbs.Rating;

/**
 * Community Statistic DAO Factory implementation.
 * @author Мо3олЬ
 */
public class CommunityStatisticDAOMySQLImpl implements CommunityStatisticDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(CommunityStatisticDAOMySQLImpl.class);
	
	public static final String SELECT_CLASSES_COUNT = "SELECT base_class FROM characters";
	public static final String SELECT_HEROES = "SELECT charId FROM heroes";
	public static final String SELECT_ACCOUNT = "SELECT login FROM accounts";
	public static final String SELECT_NOBLES = "SELECT charId FROM olympiad_nobles";
	public static final String SELECT_CLAN = "SELECT clan_id FROM clan_data";
	public static final String SELECT_ALLY = "SELECT ally_id FROM clan_data";
	public static final String SELECT_TOP_PK = "SELECT * FROM characters WHERE accesslevel = '0' ORDER BY pkkills DESC LIMIT " + CStatsConfig.BBS_STATISTIC_COUNT;
	public static final String SELECT_TOP_PVP = "SELECT * FROM characters WHERE accesslevel = '0' ORDER BY pvpkills DESC LIMIT " + CStatsConfig.BBS_STATISTIC_COUNT;
	public static final String SELECT_TOP_ONLINE = "SELECT * FROM characters WHERE accesslevel = '0' ORDER BY onlinetime DESC LIMIT " + CStatsConfig.BBS_STATISTIC_COUNT;
	public static final String SELECT_PC_CAFE_POINTS = "SELECT * FROM characters WHERE accesslevel = '0' ORDER BY pccafe_points DESC LIMIT " + CStatsConfig.BBS_STATISTIC_COUNT;
	public static final String SELECT_GRANDBOSS = "SELECT boss_id, status FROM grandboss_data";
	public static final String SELECT_CASTLE = "SELECT * FROM castle ORDER BY name DESC LIMIT " + CStatsConfig.BBS_STATISTIC_COUNT;
	public static final String SELECT_OLYMPIAD_POINTS = "SELECT c.char_name, c.clanid, c.sex, c.base_class, c.online, o.class_id, o.competitions_done, o.competitions_won " + "FROM olympiad_nobles o " + "JOIN characters c ON o.charId = c.charId " + "ORDER BY o.competitions_won DESC LIMIT "
		+ CStatsConfig.BBS_STATISTIC_COUNT;
	
	public int number = 0;
	
	@Override
	public void selectClassesCount()
	{
		Rating.Human = 0;
		Rating.Elf = 0;
		Rating.DarkElf = 0;
		Rating.Orc = 0;
		Rating.Dwarf = 0;
		Rating.Kamael = 0;
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_CLASSES_COUNT))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					if (((rs.getInt("base_class") >= 0) && (rs.getInt("base_class") <= 17)) || ((rs.getInt("base_class") >= 88) && (rs.getInt("base_class") <= 98)))
					{
						Rating.Human++;
					}
					if (((rs.getInt("base_class") >= 18) && (rs.getInt("base_class") <= 30)) || ((rs.getInt("base_class") >= 99) && (rs.getInt("base_class") <= 105)))
					{
						Rating.Elf++;
					}
					if (((rs.getInt("base_class") >= 31) && (rs.getInt("base_class") <= 43)) || ((rs.getInt("base_class") >= 106) && (rs.getInt("base_class") <= 112)))
					{
						Rating.DarkElf++;
					}
					if (((rs.getInt("base_class") >= 44) && (rs.getInt("base_class") <= 52)) || ((rs.getInt("base_class") >= 113) && (rs.getInt("base_class") <= 116)))
					{
						Rating.Orc++;
					}
					if (((rs.getInt("base_class") >= 53) && (rs.getInt("base_class") <= 57)) || ((rs.getInt("base_class") >= 117) && (rs.getInt("base_class") <= 118)))
					{
						Rating.Dwarf++;
					}
					if ((rs.getInt("base_class") >= 123) && (rs.getInt("base_class") <= 136))
					{
						Rating.Kamael++;
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select base class!", ex);
		}
	}
	
	@Override
	public void selectHeroCount()
	{
		Rating.Hero = 0;
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_HEROES))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					Rating.Hero++;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select charId for heroes!", ex);
		}
	}
	
	@Override
	public void selectAccount()
	{
		Rating.Account = 0;
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_ACCOUNT))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					Rating.Account++;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select login for accounts!", ex);
		}
	}
	
	@Override
	public void selectNobleCount()
	{
		Rating.Noble = 0;
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_NOBLES))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					Rating.Noble++;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select olympiad nobles for charId!", ex);
		}
	}
	
	@Override
	public void selectClanCount()
	{
		Rating.Clan = 0;
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_CLAN))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					Rating.Clan++;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select clan data for clan_id!", ex);
		}
	}
	
	@Override
	public void selectAllyCount()
	{
		Rating.Ally = 0;
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_ALLY))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					Rating.Ally++;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select clan data for ally_id!", ex);
		}
	}
	
	@Override
	public void selectTopPVP()
	{
		number = 0;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_TOP_PVP))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					if (!rs.getString("char_name").isEmpty())
					{
						Rating.getTopPvPName()[number] = rs.getString("char_name");
						var clan_id = rs.getInt("clanid");
						var clan = (clan_id == 0) ? null : ClanTable.getInstance().getClan(clan_id);
						Rating.getTopPvPClan()[number] = (clan == null) ? null : clan.getName();
						Rating.getTopPvPSex()[number] = rs.getInt("sex");
						Rating.getTopPvPClass()[number] = rs.getInt("base_class");
						Rating.getTopPvPOn()[number] = rs.getInt("online");
						Rating.getTopPvPOnline()[number] = rs.getInt("onlinetime");
						Rating.getTopPvP()[number] = rs.getInt("pkkills");
						Rating.getTopPvPPvP()[number] = rs.getInt("pvpkills");
					}
					else
					{
						Rating.getTopPvPName()[number] = null;
					}
					number++;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select pvpkills!", ex);
		}
	}
	
	@Override
	public void selectTopPK()
	{
		number = 0;
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_TOP_PK))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					if (!rs.getString("char_name").isEmpty())
					{
						Rating.getTopPkName()[number] = rs.getString("char_name");
						var clan_id = rs.getInt("clanid");
						var clan = (clan_id == 0) ? null : ClanTable.getInstance().getClan(clan_id);
						Rating.getTopPkClan()[number] = (clan == null) ? null : clan.getName();
						Rating.getTopPkSex()[number] = rs.getInt("sex");
						Rating.getTopPkClass()[number] = rs.getInt("base_class");
						Rating.getTopPkOn()[number] = rs.getInt("online");
						Rating.getTopPkOnline()[number] = rs.getInt("onlinetime");
						Rating.getTopPk()[number] = rs.getInt("pkkills");
						Rating.getTopPkPvP()[number] = rs.getInt("pvpkills");
					}
					else
					{
						Rating.getTopPkName()[number] = null;
					}
					number++;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select pkkills!", ex);
		}
	}
	
	@Override
	public void selectTopOnline()
	{
		number = 0;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_TOP_ONLINE))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					if (!rs.getString("char_name").isEmpty())
					{
						Rating.getTopOnlineName()[number] = rs.getString("char_name");
						var clan_id = rs.getInt("clanid");
						var clan = (clan_id == 0) ? null : ClanTable.getInstance().getClan(clan_id);
						Rating.getTopOnlineClan()[number] = (clan == null) ? null : clan.getName();
						Rating.getTopOnlineSex()[number] = rs.getInt("sex");
						Rating.getTopOnlineClass()[number] = rs.getInt("base_class");
						Rating.getTopOnlineOn()[number] = rs.getInt("online");
						Rating.getTopOnlineOnline()[number] = rs.getInt("onlinetime");
						Rating.getTopOnline()[number] = rs.getInt("pkkills");
						Rating.getTopOnlinePvP()[number] = rs.getInt("pvpkills");
					}
					else
					{
						Rating.getTopOnlineName()[number] = null;
					}
					number++;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select online!", ex);
		}
	}
	
	@Override
	public void selectHeroeStatus()
	{
		number = 0;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_OLYMPIAD_POINTS))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					if (!rs.getString("char_name").isEmpty())
					{
						Rating.getTopOlympiadName()[number] = rs.getString("char_name");
						var clan_id = rs.getInt("clanid");
						var clan = (clan_id == 0) ? null : ClanTable.getInstance().getClan(clan_id);
						Rating.getTopOlympiadClan()[number] = (clan == null) ? null : clan.getName();
						Rating.getTopOlympiadOn()[number] = rs.getInt("online");
						Rating.getTopOlympiadSex()[number] = rs.getInt("sex");
						Rating.getTopOlympiadClass()[number] = rs.getInt("base_class");
						Rating.getTopOlympiadWin()[number] = rs.getInt("competitions_won");
						Rating.getTopOlympiadDone()[number] = rs.getInt("competitions_done");
					}
					else
					{
						Rating.getTopOlympiadName()[number] = null;
					}
					number++;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select hero list!", ex);
		}
	}
	
	@Override
	public void selectGrandBoss()
	{
		number = 0;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_GRANDBOSS))
		{
			try (var rs = ps.executeQuery())
			{
				nextnpc:
				while (rs.next())
				{
					if (!rs.getString("boss_id").isEmpty())
					{
						var npcid = rs.getInt("boss_id");
						var status = rs.getInt("status");
						if (npcid == 29062)
						{
							continue nextnpc;
						}
						
						var rstatus = false;
						if (status == 0)
						{
							rstatus = true;
						}
						
						Rating.getRaidNpcName()[number] = NpcData.getInstance().getTemplate(npcid).getName();
						Rating.getRaidStatus()[number] = rstatus;
					}
					else
					{
						Rating.getRaidNpcName()[number] = null;
					}
					number++;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select GrandBoss list!", ex);
		}
	}
	
	@Override
	public void selectCastleStatus()
	{
		number = 0;
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_CASTLE))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					if (!rs.getString("name").isEmpty())
					{
						Rating.getCastleId()[number] = rs.getInt("Id");
						Rating.getCastleName()[number] = rs.getString("name");
						Rating.getCastlePercent()[number] = rs.getString("taxPercent");
						Rating.getCastleSiegeDate()[number] = TimeUtils.dateTimeFormat(new Date(rs.getLong("siegeDate")));
					}
					else
					{
						Rating.getCastleName()[number] = null;
					}
					number++;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select Castle status!", ex);
		}
	}
	
	@Override
	public void selectTopRich()
	{
		number = 0;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(("SELECT SUM(it.count), ch.char_name, ch.pkkills, ch.pvpkills, ch.onlinetime, ch.base_class, ch.online FROM characters ch LEFT OUTER JOIN items it ON ch.charId=it.owner_id WHERE item_id=57 GROUP BY ch.charId ORDER BY SUM(it.count) DESC LIMIT "
				+ CStatsConfig.BBS_STATISTIC_COUNT)))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					if (!rs.getString("char_name").isEmpty())
					{
						Rating.getTopRichName()[number] = rs.getString("char_name");
						var clan_id = rs.getInt("clanid");
						var clan = (clan_id == 0) ? null : ClanTable.getInstance().getClan(clan_id);
						Rating.getTopRichClan()[number] = (clan == null) ? null : clan.getName();
						Rating.getTopRichSex()[number] = rs.getInt("sex");
						Rating.getTopRichClass()[number] = rs.getInt("base_class");
						Rating.getTopRichOn()[number] = rs.getInt("online");
						Rating.getTopRichOnline()[number] = rs.getInt("onlinetime");
						Rating.getTopRichCount()[number] = rs.getInt("count");
					}
					else
					{
						Rating.getTopRichName()[number] = null;
					}
					number++;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select Rich list!", ex);
		}
	}
	
	@Override
	public void selectTopPcPoint()
	{
		number = 0;
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_PC_CAFE_POINTS))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					if (!rs.getString("char_name").isEmpty())
					{
						Rating.getTopPcCafeName()[number] = rs.getString("char_name");
						var clan_id = rs.getInt("clanid");
						var clan = (clan_id == 0) ? null : ClanTable.getInstance().getClan(clan_id);
						Rating.getTopPcCafeClan()[number] = (clan == null) ? null : clan.getName();
						Rating.getTopPcCafeSex()[number] = rs.getInt("sex");
						Rating.getTopPcCafeClass()[number] = rs.getInt("base_class");
						Rating.getTopPcCafeOn()[number] = rs.getInt("online");
						Rating.getTopPcCafeOnline()[number] = rs.getInt("onlinetime");
						Rating.getTopPcCafeCount()[number] = rs.getInt("pccafe_points");
					}
					else
					{
						Rating.getTopPcCafeName()[number] = null;
					}
					number++;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select pc cafe points!", ex);
		}
	}
	
	@Override
	public void selectTopClan()
	{
		number = 0;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(("SELECT clan_id, clan_name, ally_name, leader_id, clan_level, reputation_score, hasCastle, ally_id FROM clan_data ORDER BY `clan_level` DESC LIMIT " + CStatsConfig.BBS_STATISTIC_COUNT)))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					if (!rs.getString("clan_name").isEmpty())
					{
						var clanid = rs.getInt("clan_id");
						Rating.getClanName()[number] = rs.getString("clan_name");
						Rating.getAllyName()[number] = rs.getString("ally_name");
						var clanleader = rs.getInt("leader_id");
						Rating.getClanLvl()[number] = rs.getInt("clan_level");
						Rating.getClanReputation()[number] = rs.getInt("reputation_score");
						var hascastle = rs.getInt("hasCastle");
						var allyid = rs.getInt("ally_id");
						if (allyid != 0)
						{
							if (allyid == clanid)
							{
								Rating.getAllyStatus()[number] = "Alliance Leader";
							}
							Rating.getAllyStatus()[number] = "Affiliated Clan";
						}
						
						if (hascastle != 0)
						{
							var ps2 = con.prepareStatement("SELECT name FROM castle WHERE id=" + hascastle);
							var rs2 = ps2.executeQuery();
							if (rs2.next())
							{
								Rating.getClanCastleName()[number] = rs2.getString("name");
							}
						}
						
						var ps3 = con.prepareStatement("SELECT char_name FROM characters WHERE charId=" + clanleader);
						var rs3 = ps3.executeQuery();
						
						if (rs3.next())
						{
							Rating.getClanLeader()[number] = rs3.getString("char_name");
						}
					}
					else
					{
						Rating.getClanName()[number] = null;
					}
					number++;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select Clan list!", ex);
		}
	}
}
