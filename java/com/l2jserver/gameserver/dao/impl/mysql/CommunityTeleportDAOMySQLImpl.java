/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.configuration.config.bbs.CTeleportConfig;
import com.l2jserver.gameserver.dao.CommunityTeleportDAO;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.bbs.TeleportPoint;

/**
 * Community Teleport DAO Factory implementation.
 * @author Мо3олЬ
 */
public class CommunityTeleportDAOMySQLImpl implements CommunityTeleportDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(CommunityTeleportDAOMySQLImpl.class);
	
	private static final String DELETE_TELEPORT_ID = "DELETE FROM bbs_teleport WHERE charId=? AND id=?;";
	private static final String SELECT_TELEPORT_ID = "SELECT * FROM bbs_teleport WHERE charId=?;";
	private static final String SELECT_TELEPORT_COUNT = "SELECT COUNT(*) FROM bbs_teleport WHERE charId=?";
	private static final String SELECT_TELEPORT_NAME_COUNT = "SELECT COUNT(*) FROM bbs_teleport WHERE charId=? AND name=?";
	private static final String INSERT_TELEPORT = "INSERT INTO bbs_teleport (charId, x, y, z, name) VALUES(?,?,?,?,?)";
	private static final String UPDATE_TELEPORT = "UPDATE bbs_teleport SET x=?, y=?, z=? WHERE charId=? AND name=?;";
	
	@Override
	public void select(L2PcInstance player)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_TELEPORT_ID))
		{
			ps.setInt(1, player.getObjectId());
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					player.addTeleportId(new TeleportPoint(rs.getInt("id"), rs.getString("name"), 57, 0, 1, 85, false, false, 57, 0, new Location(rs.getInt("x"), rs.getInt("y"), rs.getInt("z"))));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not restoring community teleport id for {}!", player, ex);
		}
	}
	
	@Override
	public void delete(L2PcInstance player, int id)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_TELEPORT_ID))
		{
			ps.setInt(1, player.getObjectId());
			ps.setInt(2, id);
			ps.execute();
			
			player.deleteTeleportId(id);
		}
		catch (Exception ex)
		{
			LOG.warn("Could not delete community teleport id for {}!", player, ex);
		}
	}
	
	@Override
	public void update(L2PcInstance player, String namePoint)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_TELEPORT_COUNT);
			var ps1 = con.prepareStatement(SELECT_TELEPORT_NAME_COUNT);
			var ps2 = con.prepareStatement(INSERT_TELEPORT);
			var ps3 = con.prepareStatement(UPDATE_TELEPORT))
		{
			ps.setInt(1, player.getObjectId());
			
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					ps1.setInt(1, player.getObjectId());
					ps1.setString(2, namePoint);
					ps1.executeUpdate();
					
					if (rs.getInt(1) < CTeleportConfig.BBS_TELEPORT_MAX_COUNT)
					{
						ps2.setInt(1, player.getObjectId());
						ps2.setInt(2, player.getX());
						ps2.setInt(3, player.getY());
						ps2.setInt(4, player.getZ());
						ps2.setString(5, namePoint);
						ps2.execute();
						
						player.addTeleportId(new TeleportPoint(rs.getInt(1), namePoint, 57, 0, 1, 85, false, false, 57, 0, new Location(player.getX(), player.getY(), player.getZ())));
					}
					else if (rs.getInt(1) > CTeleportConfig.BBS_TELEPORT_MAX_COUNT)
					{
						ps3.setInt(1, player.getX());
						ps3.setInt(2, player.getY());
						ps3.setInt(3, player.getZ());
						ps3.setInt(4, player.getObjectId());
						ps3.setString(5, namePoint);
						ps3.execute();
						
						var teleportPoint = player.getTeleportPoints().values().stream().filter(teleport -> Objects.equals(teleport.getName(), namePoint)).findFirst();
						if (teleportPoint.isPresent())
						{
							player.addTeleportId(new TeleportPoint(teleportPoint.get().getId(), namePoint, 57, 0, 1, 85, false, false, 57, 0, new Location(player.getX(), player.getY(), player.getZ())));
						}
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not add/update community teleport id for {}!", player, ex);
		}
	}
}
