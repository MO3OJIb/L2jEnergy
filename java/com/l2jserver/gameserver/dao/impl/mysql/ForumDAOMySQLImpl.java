/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.bbs.ForumsBBSManager;
import com.l2jserver.gameserver.dao.ForumDAO;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.enums.ForumType;
import com.l2jserver.gameserver.enums.ForumVisibility;
import com.l2jserver.gameserver.model.bbs.Forum;

/**
 * Forum DAO Factory implementation.
 * @author Zoey76
 */
public class ForumDAOMySQLImpl implements ForumDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(ForumDAOMySQLImpl.class);
	
	private static final String SELECT_FORUMS = "SELECT forum_id, forum_name, forum_post, forum_type, forum_perm, forum_owner_id FROM forums WHERE forum_type = 0";
	private static final String SELECT_FORUM_CHILDREN = "SELECT forum_id, forum_name, forum_post, forum_type, forum_perm, forum_owner_id FROM forums WHERE forum_parent=?";
	private static final String INSERT_FORUM = "INSERT INTO forums (forum_id,forum_name,forum_parent,forum_post,forum_type,forum_perm,forum_owner_id) VALUES (?,?,?,?,?,?,?)";
	
	@Override
	public Map<String, Forum> getForums()
	{
		final HashMap<String, Forum> forums = new HashMap<>();
		try (var con = ConnectionFactory.getInstance().getConnection();
			var s = con.createStatement();
			var rs = s.executeQuery(SELECT_FORUMS))
		{
			while (rs.next())
			{
				var forum = new Forum(rs.getInt("forum_id"), //
					rs.getString("forum_name"), //
					null, //
					ForumType.values()[rs.getInt("forum_type")], //
					ForumVisibility.values()[rs.getInt("forum_perm")], //
					rs.getInt("forum_owner_id"));
				forums.put(forum.getName(), forum);
				
				// Load topic
				DAOFactory.getInstance().getTopicDAO().load(forum);
				
				// Load children
				loadChildren(forum);
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Data error on Forum (root)!", ex);
		}
		return forums;
	}
	
	private void loadChildren(Forum parent)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_FORUM_CHILDREN))
		{
			ps.setInt(1, parent.getId());
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					ForumsBBSManager.getInstance().load(rs.getInt("forum_id"), //
						rs.getString("forum_name"), //
						parent, //
						ForumType.values()[rs.getInt("forum_type")], //
						ForumVisibility.values()[rs.getInt("forum_perm")], //
						rs.getInt("forum_owner_id"));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Error loading child forums for forum Id {}!", parent.getId(), ex);
		}
	}
	
	@Override
	public void save(Forum forum)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT_FORUM, RETURN_GENERATED_KEYS))
		{
			ps.setString(1, forum.getName());
			ps.setInt(2, forum.getParent().getId());
			ps.setInt(3, forum.getPost());
			ps.setInt(4, forum.getType().ordinal());
			ps.setInt(5, forum.getVisibility().ordinal());
			ps.setInt(6, forum.getOwnerId());
			ps.executeUpdate();
			
			try (var rs = ps.getGeneratedKeys())
			{
				if (rs.next())
				{
					forum.setId(rs.getInt(1));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.error("Error saving forum Id {} in database!", forum.getId(), ex);
		}
	}
}
