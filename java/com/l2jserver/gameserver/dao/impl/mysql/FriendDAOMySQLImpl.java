/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.FriendDAO;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.L2Friend;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * Friend DAO MySQL implementation.
 * @author Zoey76
 * @author Мо3олЬ
 */
public class FriendDAOMySQLImpl implements FriendDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(FriendDAOMySQLImpl.class);
	
	private static final String INSERT = "INSERT INTO character_friends (charId, friendId, relation) VALUES (?, ?, 1)";
	private static final String SELECT = "SELECT friendId FROM character_friends WHERE charId=? AND relation=0 AND friendId<>charId";
	private static final String SELECT_LIST = "SELECT friendId FROM character_friends WHERE charId=? AND relation=1";
	private static final String DELETE = "DELETE FROM character_friends WHERE charId=? AND friendId=? AND relation=1";
	private static final String ADD_FRIEND = "INSERT INTO character_friends (charId, friendId) VALUES (?, ?), (?, ?)";
	private static final String DELETE_FRIEND = "DELETE FROM character_friends WHERE (charId=? AND friendId=?) OR (charId=? AND friendId=?)";
	
	@Override
	public void load(L2PcInstance player)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT))
		{
			ps.setInt(1, player.getObjectId());
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					player.addFriend(rs.getInt("friendId"));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.error("Error found in {} friend list!", player, ex);
		}
	}
	
	@Override
	public List<Integer> loadList(int objectId)
	{
		List<Integer> list = new ArrayList<>();
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_LIST))
		{
			ps.setInt(1, objectId);
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					var friendId = rs.getInt("friendId");
					if (friendId == objectId)
					{
						continue;
					}
					list.add(friendId);
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Error found in {} friend list while loading block list!", objectId, ex);
		}
		return list;
	}
	
	@Override
	public void remove(int objectId, int target)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE))
		{
			ps.setInt(1, objectId);
			ps.setInt(2, target);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not remove blocked {}!", objectId, ex);
		}
	}
	
	@Override
	public void addFriends(L2PcInstance requestor, L2PcInstance player)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(ADD_FRIEND))
		{
			ps.setInt(1, requestor.getObjectId());
			ps.setInt(2, player.getObjectId());
			ps.setInt(3, player.getObjectId());
			ps.setInt(4, requestor.getObjectId());
			ps.execute();
			
			requestor.sendPacket(SystemMessageId.YOU_HAVE_SUCCEEDED_INVITING_FRIEND);
			
			// Player added to your friend list
			var msg = SystemMessage.getSystemMessage(SystemMessageId.S1_ADDED_TO_FRIENDS);
			msg.addString(player.getName());
			requestor.sendPacket(msg);
			requestor.addFriend(player.getObjectId());
			
			// has joined as friend.
			msg = SystemMessage.getSystemMessage(SystemMessageId.S1_JOINED_AS_FRIEND);
			msg.addString(requestor.getName());
			player.sendPacket(msg);
			player.addFriend(requestor.getObjectId());
			
			// Send notifications for both player in order to show them online
			player.sendPacket(new L2Friend(true, requestor.getObjectId()));
			requestor.sendPacket(new L2Friend(true, player.getObjectId()));
		}
		catch (Exception ex)
		{
			LOG.warn("Could not add friend objectid!", ex);
		}
	}
	
	@Override
	public void deleteFriends(int id, L2PcInstance player, String name)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_FRIEND))
		{
			ps.setInt(1, player.getObjectId());
			ps.setInt(2, id);
			ps.setInt(3, id);
			ps.setInt(4, player.getObjectId());
			ps.execute();
			
			// Player deleted from your friend list
			var sm = SystemMessage.getSystemMessage(SystemMessageId.S1_HAS_BEEN_DELETED_FROM_YOUR_FRIENDS_LIST);
			sm.addString(name);
			player.sendPacket(sm);
			
			player.removeFriend(id);
			player.sendPacket(new L2Friend(false, id));
			
			var friend = L2World.getInstance().getPlayer(name);
			if (friend != null)
			{
				friend.removeFriend(player.getObjectId());
				friend.sendPacket(new L2Friend(false, player.getObjectId()));
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not del friend objectId!", ex);
		}
	}
	
	@Override
	public void save(int objectId, int target)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT))
		{
			ps.setInt(1, objectId);
			ps.setInt(2, target);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not add blocked {}!", objectId, ex);
		}
	}
}
