/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.HeroDAO;

/**
 * Hero DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class HeroDAOMySQLImpl implements HeroDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(HeroDAOMySQLImpl.class);
	
	private static final String INSERT_HEROES_DIARY = "INSERT INTO heroes_diary (charId, time, action, param) values(?,?,?,?)";
	private static final String UPDATE_HEROES_MESSAGE = "UPDATE heroes SET message=? WHERE charId=?;";
	private static final String DELETE_HERO_ITEMS = "DELETE FROM items WHERE item_id IN (6842, 6611, 6612, 6613, 6614, 6615, 6616, 6617, 6618, 6619, 6620, 6621, 9388, 9389, 9390) AND owner_id NOT IN (SELECT charId FROM characters WHERE accesslevel > 0)";
	
	@Override
	public void setDiaryData(int charId, int action, int param)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT_HEROES_DIARY))
		{
			ps.setInt(1, charId);
			ps.setLong(2, System.currentTimeMillis());
			ps.setInt(3, action);
			ps.setInt(4, param);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.error("Could not saving diary!", ex);
		}
	}
	
	@Override
	public void setUpdateMessage(int charId)
	{
		Map<Integer, String> heroMessage = new ConcurrentHashMap<>();
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_HEROES_MESSAGE))
		{
			ps.setString(1, heroMessage.get(charId));
			ps.setInt(2, charId);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not saving hero message!", ex);
		}
	}
	
	@Override
	public void deleteItems()
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var s = con.createStatement())
		{
			s.executeUpdate(DELETE_HERO_ITEMS);
		}
		catch (Exception ex)
		{
			LOG.warn("", ex);
		}
	}
}
