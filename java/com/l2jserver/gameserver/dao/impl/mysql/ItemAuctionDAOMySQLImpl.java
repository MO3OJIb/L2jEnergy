/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.ItemAuctionDAO;
import com.l2jserver.gameserver.model.itemauction.ItemAuctionBid;

/**
 * Item Auction DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class ItemAuctionDAOMySQLImpl implements ItemAuctionDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(ItemAuctionDAOMySQLImpl.class);
	
	private static final String SELECT_ITEM_AUCTION_ID = "SELECT auctionId FROM item_auction ORDER BY auctionId DESC LIMIT 0, 1";
	private static final String DELETE_ITEM_AUCTION_BID = "DELETE FROM item_auction_bid WHERE auctionId = ? AND playerObjId = ?";
	private static final String INSERT_ITEM_AUCTION_BID = "INSERT INTO item_auction_bid (auctionId, playerObjId, playerBid) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE playerBid = ?";
	private static final String DELETE_ITEM_AUCTION_ID = "DELETE FROM item_auction WHERE auctionId=?";
	private static final String DELETE_ITEM_AUCTION_BID_ID = "DELETE FROM item_auction_bid WHERE auctionId=?";
	
	@Override
	public AtomicInteger select()
	{
		final AtomicInteger auctionIds = new AtomicInteger(1);
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.createStatement();
			var rs = ps.executeQuery(SELECT_ITEM_AUCTION_ID))
		{
			if (rs.next())
			{
				auctionIds.set(rs.getInt(1) + 1);
			}
		}
		catch (Exception ex)
		{
			LOG.error("Failed loading auctions!", ex);
		}
		return auctionIds;
	}
	
	@Override
	public void delete(int auctionId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps1 = con.prepareStatement(DELETE_ITEM_AUCTION_ID);
			var ps2 = con.prepareStatement(DELETE_ITEM_AUCTION_BID_ID))
		{
			ps1.setInt(1, auctionId);
			ps1.execute();
			
			ps2.setInt(1, auctionId);
			ps2.execute();
		}
		catch (Exception ex)
		{
			LOG.error("Failed deleting auction ID {}!", auctionId, ex);
		}
	}
	
	@Override
	public void update(int auctionId, ItemAuctionBid bid, boolean delete)
	{
		var query = delete ? DELETE_ITEM_AUCTION_BID : INSERT_ITEM_AUCTION_BID;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(query))
		{
			ps.setInt(1, auctionId);
			ps.setInt(2, bid.getPlayerObjId());
			if (!delete)
			{
				ps.setLong(3, bid.getLastBid());
				ps.setLong(4, bid.getLastBid());
			}
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Error saving update player bid in database!", ex);
		}
	}
}
