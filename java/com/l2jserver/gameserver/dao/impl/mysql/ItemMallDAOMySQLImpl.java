/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.ItemMallDAO;
import com.l2jserver.gameserver.data.xml.impl.ProductItemData;
import com.l2jserver.gameserver.model.primeshop.PrimeShopGroup;

/**
 * Item Mall DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class ItemMallDAOMySQLImpl implements ItemMallDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(ItemMallDAOMySQLImpl.class);
	
	private static final String INSERT_ITEM_MALL_PRODUCT = "INSERT INTO character_item_mall_transactions (charId, productId, quantity, maxStock) values (?,?,?,?)";
	private static final String SELECT_ITEM_MALL_PRODUCT = "SELECT productId FROM character_item_mall_transactions WHERE charId=? ORDER BY transactionTime DESC";
	private static final String SELECT_ITEM_MALL_MAX_STOCK = "SELECT maxStock FROM character_item_mall_transactions WHERE productId=?";
	private static final String SELECT_ITEM_MALL_ACTUAL_STOCK = "SELECT quantity FROM character_item_mall_transactions WHERE productId=?";
	
	@Override
	public void addPoduct(int charId, int productId, int quantity, int maxStock)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT_ITEM_MALL_PRODUCT))
		{
			ps.setInt(1, charId);
			ps.setInt(2, productId);
			ps.setInt(3, quantity);
			ps.setInt(4, maxStock);
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.error("Could not save Item Mall transaction!", ex);
		}
	}
	
	@Override
	public List<PrimeShopGroup> selectPoducts(int charId)
	{
		final List<PrimeShopGroup> itemsList = new ArrayList<>();
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_ITEM_MALL_PRODUCT))
		{
			ps.setInt(1, charId);
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					var product = ProductItemData.getInstance().getProduct(rs.getInt("productId"));
					if ((product != null) && !itemsList.contains(product))
					{
						itemsList.add(product);
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.error("Could not restore Item Mall transaction!", ex);
		}
		return itemsList;
	}
	
	// TODO
	@Override
	public int setMaxStock(int productId)
	{
		int maxStock = 0;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_ITEM_MALL_MAX_STOCK))
		{
			ps.setInt(1, productId);
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					return maxStock = rs.getInt("maxStock");
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not get max stock of products in Item Mall!", ex);
		}
		return maxStock;
	}
	
	// TODO
	@Override
	public int setCurrentStock(int productId)
	{
		int currentStock = 0;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_ITEM_MALL_ACTUAL_STOCK))
		{
			ps.setInt(1, productId);
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					return currentStock = rs.getInt("quantity");
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not get actual stock of products in Item Mall!", ex);
		}
		return currentStock;
	}
}
