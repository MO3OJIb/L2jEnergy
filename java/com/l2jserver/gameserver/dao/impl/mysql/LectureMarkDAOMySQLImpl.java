/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.LectureMarkDAO;

/**
 * LectureMark DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class LectureMarkDAOMySQLImpl implements LectureMarkDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(LectureMarkDAOMySQLImpl.class);
	
	public static final String INSERT = "REPLACE INTO account_lecture_mark(account, lecture_mark) VALUES (?,?)";
	public static final String SELECT = "SELECT lecture_mark FROM account_lecture_mark WHERE account=?";
	
	@Override
	public int select(String account)
	{
		var val = -1;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT))
		{
			ps.setString(1, account);
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					val = rs.getInt("lecture_mark");
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("", ex);
		}
		return val;
	}
	
	@Override
	public void replace(String account, int mark)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT))
		{
			ps.setString(1, account);
			ps.setInt(2, mark);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("", ex);
		}
	}
}
