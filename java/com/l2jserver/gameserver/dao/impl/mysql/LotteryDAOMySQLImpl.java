/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.configuration.config.events.LotteryConfig;
import com.l2jserver.gameserver.dao.LotteryDAO;

/**
 * Lottery DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class LotteryDAOMySQLImpl implements LotteryDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(LotteryDAOMySQLImpl.class);
	
	private static final String INSERT = "INSERT INTO games(id, idnr, enddate, prize, newprize) VALUES (?, ?, ?, ?, ?)";
	private static final String UPDATE = "UPDATE games SET finished=1, prize=?, newprize=?, number1=?, number2=?, prize1=?, prize2=?, prize3=? WHERE id=1 AND idnr=?";
	private static final String UPDATE_PRICE = "UPDATE games SET prize=?, newprize=? WHERE id = 1 AND idnr = ?";
	private static final String SELECT_LOTTERY_ITEM = "SELECT enchant_level, custom_type2 FROM items WHERE item_id = 4442 AND custom_type1 = ?";
	
	@Override
	public void add(int lotteryId, long endTime, long prize)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT))
		{
			ps.setInt(1, 1);
			ps.setInt(2, lotteryId);
			ps.setLong(3, endTime);
			ps.setLong(4, prize);
			ps.setLong(5, prize);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not store new lottery data!", ex);
		}
	}
	
	@Override
	public void update(int lotteryId, long prize, long newPrize, long prize1, long prize2, long prize3, int enchant, int type2)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE))
		{
			ps.setLong(1, prize);
			ps.setLong(2, newPrize);
			ps.setInt(3, enchant);
			ps.setInt(4, type2);
			ps.setLong(5, prize1);
			ps.setLong(6, prize2);
			ps.setLong(7, prize3);
			ps.setInt(8, lotteryId);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not store finished lottery data!", ex);
		}
	}
	
	@Override
	public void select(int lotteryId, int count1, int count2, int count3, int count4, int enchant, int type2)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_LOTTERY_ITEM))
		{
			ps.setInt(1, lotteryId);
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					var curenchant = rs.getInt("enchant_level") & enchant;
					var curtype2 = rs.getInt("custom_type2") & type2;
					
					if ((curenchant == 0) && (curtype2 == 0))
					{
						continue;
					}
					
					var count = 0;
					
					for (var i = 1; i <= 16; i++)
					{
						var val = curenchant / 2;
						
						if (val != Math.round((double) curenchant / 2))
						{
							count++;
						}
						
						var val2 = curtype2 / 2;
						
						if (val2 != ((double) curtype2 / 2))
						{
							count++;
						}
						
						curenchant = val;
						curtype2 = val2;
					}
					
					if (count == 5)
					{
						count1++;
					}
					else if (count == 4)
					{
						count2++;
					}
					else if (count == 3)
					{
						count3++;
					}
					else if (count > 0)
					{
						count4++;
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not restore lottery data!", ex);
		}
	}
	
	private static final String SELECT_LOTTERY_TICKET = "SELECT number1, number2, prize1, prize2, prize3 FROM games WHERE id = 1 and idnr = ?";
	
	@Override
	public long[] checkTicket(int id, int enchant, int type2)
	{
		long res[] =
		{
			0,
			0
		};
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_LOTTERY_TICKET))
		{
			ps.setInt(1, id);
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					var curenchant = rs.getInt("number1") & enchant;
					var curtype2 = rs.getInt("number2") & type2;
					
					if ((curenchant == 0) && (curtype2 == 0))
					{
						return res;
					}
					
					var count = 0;
					
					for (var i = 1; i <= 16; i++)
					{
						var val = curenchant / 2;
						if (val != Math.round((double) curenchant / 2))
						{
							count++;
						}
						var val2 = curtype2 / 2;
						if (val2 != ((double) curtype2 / 2))
						{
							count++;
						}
						curenchant = val;
						curtype2 = val2;
					}
					
					switch (count)
					{
						case 0:
							break;
						case 5:
							res[0] = 1;
							res[1] = rs.getLong("prize1");
							break;
						case 4:
							res[0] = 2;
							res[1] = rs.getLong("prize2");
							break;
						case 3:
							res[0] = 3;
							res[1] = rs.getLong("prize3");
							break;
						default:
							res[0] = 4;
							res[1] = LotteryConfig.ALT_LOTTERY_2_AND_1_NUMBER_PRIZE;
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not check lottery ticket #{}!", id, ex);
		}
		return res;
	}
	
	@Override
	public void updatePrize(int lotteryId, long newPrize)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_PRICE))
		{
			ps.setLong(1, newPrize);
			ps.setLong(2, newPrize);
			ps.setInt(3, lotteryId);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not increase current lottery prize!", ex);
		}
	}
}
