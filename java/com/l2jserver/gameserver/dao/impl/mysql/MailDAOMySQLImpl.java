/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.dao.MailDAO;
import com.l2jserver.gameserver.instancemanager.tasks.MessageDeletionTask;
import com.l2jserver.gameserver.model.entity.Message;

/**
 * Mail DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class MailDAOMySQLImpl implements MailDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(MailDAOMySQLImpl.class);
	
	private static final String INSERT_MESSAGES = "INSERT INTO messages (messageId, senderId, receiverId, subject, content, expiration, reqAdena, hasAttachments, isUnread, isDeletedBySender, isDeletedByReceiver, sendBySystem, isReturned) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String UPDATE_MESSAGES = "UPDATE messages SET isUnread = 'false' WHERE messageId = ?";
	private static final String UPDATE_SENDER = "UPDATE messages SET isDeletedBySender = 'true' WHERE messageId = ?";
	private static final String UPDATE_RECEIVER = "UPDATE messages SET isDeletedByReceiver = 'true' WHERE messageId = ?";
	private static final String UPDATE_ATTACHMENTS = "UPDATE messages SET hasAttachments = 'false' WHERE messageId = ?";
	private static final String DELETE = "DELETE FROM messages WHERE messageId = ?";
	
	@Override
	public Map<Integer, Message> load()
	{
		final Map<Integer, Message> messages = new ConcurrentHashMap<>();
		
		int count = 0;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.createStatement();
			var rs = ps.executeQuery("SELECT * FROM messages ORDER BY expiration"))
		{
			while (rs.next())
			{
				var msgId = rs.getInt("messageId");
				var msg = new Message(msgId, rs.getInt("senderId"), rs.getInt("receiverId"), rs.getString("subject"), rs.getString("content"), rs.getLong("expiration"), rs.getLong("reqAdena"), rs.getBoolean("hasAttachments"), rs.getBoolean("isUnread"), rs.getBoolean("isDeletedBySender"), rs
					.getBoolean("isDeletedByReceiver"), rs.getInt("sendBySystem"), rs.getBoolean("isReturned"));
				messages.put(msgId, msg);
				count++;
				
				var expiration = msg.getExpiration();
				
				if (expiration < System.currentTimeMillis())
				{
					ThreadPoolManager.getInstance().scheduleGeneral(new MessageDeletionTask(msgId), 10000);
				}
				else
				{
					ThreadPoolManager.getInstance().scheduleGeneral(new MessageDeletionTask(msgId), expiration - System.currentTimeMillis());
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Error loading from database!", ex);
		}
		LOG.info("Loaded {} mail messages.", count);
		return messages;
		
	}
	
	@Override
	public void markAsRead(int message)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_MESSAGES))
		{
			ps.setInt(1, message);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not marking as read message!", ex);
		}
	}
	
	@Override
	public void markAsDeletedBySender(int message)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_SENDER))
		{
			ps.setInt(1, message);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not marking as deleted by sender message!", ex);
		}
	}
	
	@Override
	public void markAsDeletedByReceiver(int message)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_RECEIVER))
		{
			ps.setInt(1, message);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not marking as deleted by receiver message!", ex);
		}
	}
	
	@Override
	public void removeAttachments(int message)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_ATTACHMENTS))
		{
			ps.setInt(1, message);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not removing attachments in message!", ex);
		}
	}
	
	@Override
	public void send(int id, int senderId, int receiverId, String subject, String content, long expiration, long reqAdena, boolean hasAttachments, boolean unread, boolean deletedBySender, boolean deletedByReceiver, int sendBySystem, boolean returned)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT_MESSAGES))
		{
			ps.setInt(1, id);
			ps.setInt(2, senderId);
			ps.setInt(3, receiverId);
			ps.setString(4, subject);
			ps.setString(5, content);
			ps.setLong(6, expiration);
			ps.setLong(7, reqAdena);
			ps.setString(8, String.valueOf(hasAttachments));
			ps.setString(9, String.valueOf(unread));
			ps.setString(10, String.valueOf(deletedBySender));
			ps.setString(11, String.valueOf(deletedByReceiver));
			ps.setString(12, String.valueOf(sendBySystem));
			ps.setString(13, String.valueOf(returned));
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Error saving message!", ex);
		}
	}
	
	@Override
	public void delete(int message)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE))
		{
			ps.setInt(1, message);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Error deleting message!", ex);
		}
	}
}
