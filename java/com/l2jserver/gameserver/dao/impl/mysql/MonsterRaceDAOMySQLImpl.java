/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.MonsterRaceDAO;
import com.l2jserver.gameserver.instancemanager.games.MonsterRace;
import com.l2jserver.gameserver.model.HistoryInfo;

/**
 * Monster Race DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class MonsterRaceDAOMySQLImpl implements MonsterRaceDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(MonsterRaceDAOMySQLImpl.class);
	
	private static final String SAVE_MONSTER_RACE_BETS = "REPLACE INTO mdt_bets (lane_id, bet) VALUES (?,?)";
	private static final String SELECT_MONSTER_RACE_BETS = "SELECT * FROM mdt_bets";
	private static final String CLEAR_MONSTER_RACE_BETS = "UPDATE mdt_bets SET bet = 0";
	private static final String INSERT_HISTORY = "INSERT INTO mdt_history (race_id, first, second, odd_rate, winner) VALUES (?,?,?,?,?)";
	private static final String UPDATE_HISTORY = "UPDATE mdt_history SET first=?, second=?, odd_rate=?, winner=? WHERE race_id=?";
	private static final String LOAD_HISTORY_MONSTER_RACE = "SELECT * FROM mdt_history";
	
	@Override
	public void loadBets(MonsterRace race)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_MONSTER_RACE_BETS))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					race.setBetOnLane(rs.getInt("lane_id"), rs.getLong("bet"), false);
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not load bets!", ex);
		}
	}
	
	@Override
	public void saveBet(int lane, long sum)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SAVE_MONSTER_RACE_BETS))
		{
			ps.setInt(1, lane);
			ps.setLong(2, sum);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not save bet!", ex);
		}
	}
	
	@Override
	public void clearBets()
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(CLEAR_MONSTER_RACE_BETS))
		{
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not clear bets!", ex);
		}
	}
	
	@Override
	public void loadHistory(MonsterRace race)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(LOAD_HISTORY_MONSTER_RACE))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					// Calculate the current race number.
					var savedRaceNumber = rs.getInt("race_id");
					var historyInfo = new HistoryInfo(savedRaceNumber, rs.getInt("first"), rs.getInt("second"), rs.getDouble("odd_rate"), rs.getString("winner"));
					race.getHistory().put(savedRaceNumber, historyInfo);
					if (race.getRaceNumber() <= savedRaceNumber)
					{
						race.setRaceNumber(savedRaceNumber + 1);
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not load history!", ex);
		}
	}
	
	@Override
	public void saveHistory(HistoryInfo history)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_HISTORY))
		{
			ps.setInt(1, history.getFirst());
			ps.setInt(2, history.getSecond());
			ps.setDouble(3, history.getOddRate());
			ps.setString(4, history.getWinner());
			ps.setInt(5, history.getRaceId());
			int update = ps.executeUpdate();
			if (update == 0)
			{
				insertNewHistory(history);
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not save history!", ex);
		}
	}
	
	private void insertNewHistory(HistoryInfo history)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT_HISTORY))
		{
			ps.setInt(1, history.getRaceId());
			ps.setInt(2, history.getFirst());
			ps.setInt(3, history.getSecond());
			ps.setDouble(4, history.getOddRate());
			ps.setString(5, history.getWinner());
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not insert new history!", ex);
		}
	}
}
