/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.configuration.config.events.PcCafeConfig;
import com.l2jserver.gameserver.dao.PcCafeDAO;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.variables.PlayerVariables;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.Util;

/**
 * Pc Cafe DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class PcCafeDAOMySQLImpl implements PcCafeDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(PcCafeDAOMySQLImpl.class);
	
	private final Lock _lock = new ReentrantLock();
	
	private static final String SELECT_PCCAFE_CODE = "SELECT type, value, used_by FROM pccafe_coupons WHERE serial_code=?";
	private static final String UPDATE_PCCAFE_CODE = "UPDATE pccafe_coupons SET used_by=? WHERE serial_code=?";
	
	@Override
	public boolean requestEnterCode(L2PcInstance player, String couponCode)
	{
		_lock.lock();
		try
		{
			couponCode = couponCode.toLowerCase();
			if (!Util.isValidPcBangCouponName(couponCode))
			{
				onWrongCode(player);
				return false;
			}
			
			try (var con = ConnectionFactory.getInstance().getConnection();
				var ps = con.prepareStatement(SELECT_PCCAFE_CODE))
			{
				ps.setString(1, couponCode);
				try (var rs = ps.executeQuery())
				{
					if (rs.next())
					{
						int type = rs.getInt("type");
						var value = rs.getString("value");
						int used_by = rs.getInt("used_by");
						
						if (used_by > 0)
						{
							if (used_by == player.getObjectId())
							{
								player.sendPacket(SystemMessageId.SINCE_YOU_HAVE_ALREADY_USED_THIS_COUPON_YOU_MAY_NOT_USE_THIS_SERIAL_NUMBER);
							}
							else
							{
								player.sendPacket(SystemMessageId.THIS_SERIAL_NUMBER_HAS_ALREADY_BEEN_USED);
							}
							return false;
						}
						return useCoupon(player, couponCode, type, value);
					}
					onWrongCode(player);
				}
			}
			catch (Exception ex)
			{
				LOG.error("Error while reading coupon code.", ex);
			}
		}
		finally
		{
			_lock.unlock();
		}
		return false;
	}
	
	private boolean useCoupon(L2PcInstance player, String couponCode, int type, String value)
	{
		_lock.lock();
		try
		{
			couponCode = couponCode.toLowerCase();
			try (var con = ConnectionFactory.getInstance().getConnection();
				var ps = con.prepareStatement(UPDATE_PCCAFE_CODE))
			{
				ps.setInt(1, player.getObjectId());
				ps.setString(2, couponCode);
				ps.execute();
				
				player.getVariables().remove(PlayerVariables.PCC_CODE_ATTEMPTS); // hasVariable
			}
			catch (Exception ex)
			{
				LOG.error("Error while use coupon code.", ex);
			}
		}
		finally
		{
			_lock.unlock();
		}
		return true;
	}
	
	private void onWrongCode(L2PcInstance player)
	{
		long pcCodeAttempts = Math.min(player.getVariables().getInt(PlayerVariables.PCC_CODE_ATTEMPTS, 0) + 1, System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(PcCafeConfig.ALT_PCBANG_POINTS_BAN_TIME));
		long leftTime = player.getVariables().getLong(PlayerVariables.PCC_CODE_ATTEMPTS, 1) - System.currentTimeMillis();
		
		if (pcCodeAttempts > PcCafeConfig.ALT_PCBANG_POINTS_MAX_CODE_ENTER_ATTEMPTS)
		{
			if (leftTime > 0)
			{
				player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.THIS_SERIAL_NUMBER_CANNOT_BE_ENTERED_PLEASE_TRY_AGAIN_IN_S1_MINUTES).addLong(TimeUnit.MILLISECONDS.toMinutes(leftTime)));
				return;
			}
			pcCodeAttempts = 0;
		}
		
		if (pcCodeAttempts == PcCafeConfig.ALT_PCBANG_POINTS_MAX_CODE_ENTER_ATTEMPTS)
		{
			player.getVariables().set(PlayerVariables.PCC_CODE_ATTEMPTS, pcCodeAttempts);
			player.sendPacket(SystemMessageId.INVALID_SERIAL_NUMBER_YOUR_ATTEMPT_TO_ENTER_THE_NUMBER_HAS_FAILED_5_TIMES_PLEASE_TRY_AGAIN_IN_4_HOURS);
		}
		else
		{
			player.getVariables().set(PlayerVariables.PCC_CODE_ATTEMPTS, pcCodeAttempts);
			player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.INVALID_SERIAL_NUMBER__YOUR_ATTEMPT_TO_ENTER_THE_NUMBER_HAS_FAILED_S1_TIMES_YOU_WILL_BE_ALLOWED_TO_MAKE_S2_MORE_ATTEMPTS).addLong(pcCodeAttempts).addLong(PcCafeConfig.ALT_PCBANG_POINTS_MAX_CODE_ENTER_ATTEMPTS
				- pcCodeAttempts));
		}
	}
}
