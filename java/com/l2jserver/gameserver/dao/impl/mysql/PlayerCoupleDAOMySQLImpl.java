/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.PlayerCoupleDAO;
import com.l2jserver.gameserver.idfactory.IdFactory;
import com.l2jserver.gameserver.model.entity.Couple;

/**
 * Player Couple DAO Factory implementation.
 * @author Мо3олЬ
 */
public class PlayerCoupleDAOMySQLImpl implements PlayerCoupleDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(PlayerCoupleDAOMySQLImpl.class);
	
	private static final String SELECT_COUPLES_ID = "SELECT id FROM couples ORDER BY id";
	private static final String INSERT_COUPLES = "INSERT INTO couples (id, player1Id, player2Id, married, affianceDate, weddingDate) VALUES (?, ?, ?, ?, ?, ?)";
	private static final String SELECT_COUPLES = "SELECT * FROM couples WHERE id = ?";
	private static final String DELETE_COUPLES = "DELETE FROM couples WHERE id=?";
	private static final String UPDATE_COUPLES = "UPDATE couples set married = ?, weddingDate = ? where id = ?";
	
	@Override
	public List<Couple> load()
	{
		List<Couple> couples = new CopyOnWriteArrayList<>();
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.createStatement();
			var rs = ps.executeQuery(SELECT_COUPLES_ID))
		{
			while (rs.next())
			{
				couples.add(new Couple(rs.getInt("id")));
			}
		}
		catch (Exception ex)
		{
			LOG.error("Could not loading couples id!", ex);
		}
		return couples;
	}
	
	@Override
	public void add(int id, int player1Id, int player2Id, Calendar affiancedDate, Calendar weddingDate)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT_COUPLES))
		{
			id = IdFactory.getInstance().getNextId();
			ps.setInt(1, id);
			ps.setInt(2, player1Id);
			ps.setInt(3, player2Id);
			ps.setBoolean(4, false);
			ps.setLong(5, affiancedDate.getTimeInMillis());
			ps.setLong(6, weddingDate.getTimeInMillis());
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not create couple!", ex);
		}
	}
	
	@Override
	public void select(int id, int player1Id, int player2Id, boolean maried, Calendar affiancedDate, Calendar weddingDate)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_COUPLES))
		{
			ps.setInt(1, id);
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					player1Id = rs.getInt("player1Id");
					player2Id = rs.getInt("player2Id");
					maried = rs.getBoolean("married");
					
					affiancedDate = Calendar.getInstance();
					affiancedDate.setTimeInMillis(rs.getLong("affianceDate"));
					
					weddingDate = Calendar.getInstance();
					weddingDate.setTimeInMillis(rs.getLong("weddingDate"));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not load couple!", ex);
		}
	}
	
	@Override
	public void divorce(int id)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_COUPLES))
		{
			ps.setInt(1, id);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not delete couple!", ex);
		}
	}
	
	@Override
	public void marry(Calendar weddingDate, int id, boolean maried)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_COUPLES))
		{
			ps.setBoolean(1, true);
			weddingDate = Calendar.getInstance();
			ps.setLong(2, weddingDate.getTimeInMillis());
			ps.setInt(3, id);
			ps.execute();
			maried = true;
		}
		catch (Exception ex)
		{
			LOG.warn("Could not marry!", ex);
		}
	}
}
