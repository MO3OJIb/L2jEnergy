/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.PlayerCursedWeaponDAO;

/**
 * Player Cursed Weapon DAO Factory implementation.
 * @author Мо3олЬ
 */
public class PlayerCursedWeaponDAOMySQLImpl implements PlayerCursedWeaponDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(PlayerCursedWeaponDAOMySQLImpl.class);
	
	private static final String INSERT_CURSED_WEAPONS = "INSERT INTO cursed_weapons (itemId, charId, playerKarma, playerPkKills, nbKills, endTime) VALUES (?, ?, ?, ?, ?, ?)";
	private static final String DELETE_CURSED_WEAPONS_ITEM_ID = "DELETE FROM cursed_weapons WHERE itemId = ?";
	private static final String SELECT_CHARACTER_ITEM_ID = "SELECT owner_id FROM items WHERE item_id=?";
	private static final String DELETE_CHARACTER_ITEM_ID = "DELETE FROM items WHERE owner_id=? AND item_id=?";
	private static final String UPDATE_CHARACTER_KARMA_PK_KILLS = "UPDATE characters SET karma=?, pkkills=? WHERE charId=?";
	
	@Override
	public void controlPlayers(int itemId, boolean isActivated, int playerKarma, int playerPkKills)
	{
		// TODO: See comments below...
		// This entire for loop should NOT be necessary, since it is already handled by
		// CursedWeapon.endOfLife(). However, if we indeed *need* to duplicate it for safety,
		// then we'd better make sure that it FULLY cleans up inactive cursed weapons!
		// Undesired effects result otherwise, such as player with no zariche but with karma
		// or a lost-child entry in the cursed weapons table, without a corresponding one in items...
		
		// Retrieve the L2PcInstance from the characters table of the database
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps1 = con.prepareStatement(SELECT_CHARACTER_ITEM_ID))
		{
			if (isActivated)
			{
				ps1.setInt(1, itemId);
				try (var rs = ps1.executeQuery())
				{
					if (rs.next())
					{
						// A player has the cursed weapon in his inventory ...
						var playerId = rs.getInt("owner_id");
						LOG.info("PROBLEM : Player {} owns the cursed weapon {} but he shouldn't.", playerId, itemId);
						
						// Delete the item
						try (var ps2 = con.prepareStatement(DELETE_CHARACTER_ITEM_ID))
						{
							ps2.setInt(1, playerId);
							ps2.setInt(2, itemId);
							if (ps2.executeUpdate() != 1)
							{
								LOG.warn("Error while deleting cursed weapon {} from userId {}!", itemId, playerId);
							}
						}
						
						// Restore the player's old karma and pk count
						try (var ps3 = con.prepareStatement(UPDATE_CHARACTER_KARMA_PK_KILLS))
						{
							ps3.setInt(1, playerKarma);
							ps3.setInt(2, playerPkKills);
							ps3.setInt(3, playerId);
							if (ps3.executeUpdate() != 1)
							{
								LOG.warn("Error while updating karma & pkkills for userId {}!", playerId);
							}
						}
						// clean up the cursed weapons table.
						remove(itemId);
					}
				}
			}
			ps1.clearParameters();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not check CursedWeapons data!", ex);
		}
	}
	
	@Override
	public void save(int itemId, boolean isActivated, int playerId, int playerKarma, int playerPkKills, int nbKills, long endTime)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_CURSED_WEAPONS_ITEM_ID);
			var ps1 = con.prepareStatement(INSERT_CURSED_WEAPONS))
		{
			// Delete previous datas
			ps.setInt(1, itemId);
			ps.executeUpdate();
			
			if (isActivated)
			{
				ps1.setInt(1, itemId);
				ps1.setInt(2, playerId);
				ps1.setInt(3, playerKarma);
				ps1.setInt(4, playerPkKills);
				ps1.setInt(5, nbKills);
				ps1.setLong(6, endTime);
				ps1.executeUpdate();
			}
		}
		catch (Exception ex)
		{
			LOG.error("Failed to save data.", ex);
		}
	}
	
	@Override
	public void remove(int itemId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_CURSED_WEAPONS_ITEM_ID))
		{
			ps.setInt(1, itemId);
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.error("Failed to remove data!", ex);
		}
	}
	
	@Override
	public void removedOffline(int playerId, int itemId, int playerKarma, int playerPkKills)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var del = con.prepareStatement(DELETE_CHARACTER_ITEM_ID);
			var ps = con.prepareStatement(UPDATE_CHARACTER_KARMA_PK_KILLS))
		{
			// Delete the item
			del.setInt(1, playerId);
			del.setInt(2, itemId);
			if (del.executeUpdate() != 1)
			{
				LOG.warn("Error while deleting itemId {} from userId {}", itemId, playerId);
			}
			
			// Restore the karma
			ps.setInt(1, playerKarma);
			ps.setInt(2, playerPkKills);
			ps.setInt(3, playerId);
			if (ps.executeUpdate() != 1)
			{
				LOG.warn("Error while updating karma & pkkills for userId {}", playerId);
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not delete!", ex);
		}
	}
}
