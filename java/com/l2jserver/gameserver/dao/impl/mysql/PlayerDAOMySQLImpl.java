/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.sql.Timestamp;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.configuration.config.events.WeddingConfig;
import com.l2jserver.gameserver.dao.PlayerDAO;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.Tops;
import com.l2jserver.gameserver.enums.actors.PlayerSex;
import com.l2jserver.gameserver.instancemanager.CursedWeaponsManager;
import com.l2jserver.gameserver.model.L2ClanMember;
import com.l2jserver.gameserver.model.actor.appearance.PcAppearance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.base.SubClass;
import com.l2jserver.gameserver.model.entity.Hero;

/**
 * Player DAO MySQL implementation.
 * @author Zoey76
 */
public class PlayerDAOMySQLImpl implements PlayerDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(PlayerDAOMySQLImpl.class);
	
	private static final String INSERT = "INSERT INTO characters (account_name,charId,char_name,level,maxHp,curHp,maxCp,curCp,maxMp,curMp,face,hairStyle,hairColor,sex,exp,sp,karma,fame,pvpkills,pkkills,clanid,race,classid,deletetime,cancraft,title,title_color,accesslevel,online,isin7sdungeon,clan_privs,wantspeace,base_class,newbie,nobless,power_grade,createDate) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String INSERT_FEEDBACK = "INSERT INTO petition_feedback VALUES (?,?,?,?,?)";
	private static final String INSERT_TOP_DATA = "INSERT INTO character_votes (type,nick,charId,time,reward) VALUES (?,?,?,?,?)";
	private static final String SELECT = "SELECT * FROM characters WHERE charId=?";
	private static final String SELECT_CHARACTERS = "SELECT charId, char_name FROM characters WHERE account_name=? AND charId<>?";
	private static final String SELECT_CHARACTERS_CHAR_ID = "SELECT charId FROM characters WHERE char_name=?";
	private static final String SELECT_CHARACTER_TOP_DATA = "SELECT * FROM character_votes WHERE charId=? AND type=? AND time=? AND reward=?";
	private static final String SELECT_REPAIR_CHARACTERS = "SELECT account_name FROM characters WHERE char_name=?";
	private static final String SELECT_REPAIR_CHARACTERS_ACCOUNT = "SELECT char_name FROM characters WHERE account_name=?";
	private static final String SELECT_REPAIR_CHARACTERS_JAIL = "SELECT key, expiration FROM punishments WHERE key=? AND type=JAIL";
	private static final String UPDATE = "UPDATE characters SET level=?,maxHp=?,curHp=?,maxCp=?,curCp=?,maxMp=?,curMp=?,face=?,hairStyle=?,hairColor=?,sex=?,heading=?,x=?,y=?,z=?,exp=?,expBeforeDeath=?,sp=?,karma=?,fame=?,pvpkills=?,pkkills=?,clanid=?,race=?,classid=?,deletetime=?,title=?,title_color=?,accesslevel=?,online=?,isin7sdungeon=?,clan_privs=?,wantspeace=?,base_class=?,onlinetime=?,newbie=?,nobless=?,power_grade=?,subpledge=?,lvl_joined_academy=?,apprentice=?,sponsor=?,clan_join_expiry_time=?,clan_create_expiry_time=?,char_name=?,death_penalty_level=?,bookmarkslot=?,vitality_points=?,pccafe_points=?,language=? WHERE charId=?";
	private static final String UPDATE_ONLINE = "UPDATE characters SET online=?, lastAccess=? WHERE charId=?";
	private static final String UPDATE_REPAIR_CHARACTERS = "UPDATE characters SET x=17867, y=170259, z=-3503 WHERE charId=?";
	private static final String UPDATE_REPAIR_CHARACTERS_ID = "UPDATE items SET loc=WAREHOUSE WHERE owner_id=? AND loc=PAPERDOLL";
	private static final String UPDATE_ACCESS_LEVEL = "UPDATE characters SET accesslevel=? WHERE char_name=?";
	private static final String DELETE_TOP_DATA = "DELETE FROM character_votes WHERE time<?";
	private static final String DELETE_REPAIR_CHARACTERS = "DELETE FROM character_shortcuts WHERE charId=?";
	private static final String DELETE_CHARACTERS_POST_FRIENDS = "DELETE FROM character_post_friends WHERE charId=? OR post_friend=?";
	private static final String DELETE_CHARACTERS_FRIENDS = "DELETE FROM character_friends WHERE charId=? OR friendId=?";
	private static final String DELETE_CHARACTERS_HENNAS = "DELETE FROM character_hennas WHERE charId=?";
	private static final String DELETE_CHARACTERS_MACROSES = "DELETE FROM character_macroses WHERE charId=?";
	private static final String DELETE_CHARACTERS_MINIGAME_SCORE = "DELETE FROM character_minigame_score WHERE charId=?";
	private static final String DELETE_CHARACTERS_QUESTS = "DELETE FROM character_quests WHERE charId=?";
	private static final String DELETE_CHARACTERS_GLOBAL_DATA = "DELETE FROM character_quest_global_data WHERE charId=?";
	private static final String DELETE_CHARACTERS_RECIPEBOOK = "DELETE FROM character_recipebook WHERE charId=?";
	private static final String DELETE_CHARACTERS_SHORTCUTS = "DELETE FROM character_shortcuts WHERE charId=?";
	private static final String DELETE_CHARACTERS_SKILLS = "DELETE FROM character_skills WHERE charId=?";
	private static final String DELETE_CHARACTERS_SKILLS_SAVE = "DELETE FROM character_skills_save WHERE charId=?";
	private static final String DELETE_CHARACTERS_SUBCLASSES = "DELETE FROM character_subclasses WHERE charId=?";
	private static final String DELETE_CHARACTERS_ITEM_MALL_TRANSACTIONS = "DELETE FROM character_item_mall_transactions WHERE charId=?";
	private static final String DELETE_HEROES = "DELETE FROM heroes WHERE charId=?";
	private static final String DELETE_OLYMPIAD_NOBLES = "DELETE FROM olympiad_nobles WHERE charId=?";
	private static final String DELETE_SEVEN_SIGNS = "DELETE FROM seven_signs WHERE charId=?";
	private static final String DELETE_PETS = "DELETE FROM pets WHERE item_obj_id IN (SELECT object_id FROM items WHERE items.owner_id=?)";
	private static final String DELETE_ITEM_ATTRIBUTES = "DELETE FROM item_attributes WHERE itemId IN (SELECT object_id FROM items WHERE items.owner_id=?)";
	private static final String DELETE_ITEMS = "DELETE FROM items WHERE owner_id=?";
	private static final String DELETE_MERCHANT_LEASE = "DELETE FROM merchant_lease WHERE player_id=?";
	private static final String DELETE_CHARACTERS_RAID_POINT = "DELETE FROM character_raid_points WHERE charId=?";
	private static final String DELETE_CHARACTERS_RECO_BONUS = "DELETE FROM character_reco_bonus WHERE charId=?";
	private static final String DELETE_CHARACTERS_INSTANCE_TIME = "DELETE FROM character_instance_time WHERE charId=?";
	private static final String DELETE_CHARACTERS_VARIABLES = "DELETE FROM character_variables WHERE charId=?";
	private static final String DELETE_CHARACTERS = "DELETE FROM characters WHERE charId=?";
	private static final String DELETE_COUPLES = "DELETE FROM couples WHERE player1Id = ? OR player2Id = ?";
	
	@Override
	public L2PcInstance load(int objectId)
	{
		L2PcInstance player = null;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT))
		{
			// Retrieve the L2PcInstance from the characters table of the database
			ps.setInt(1, objectId);
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					final int activeClassId = rs.getInt("classid");
					var app = new PcAppearance(rs.getByte("face"), rs.getByte("hairColor"), rs.getByte("hairStyle"), PlayerSex.values()[rs.getInt("sex")]);
					
					player = new L2PcInstance(objectId, activeClassId, rs.getString("account_name"), app);
					player.setName(rs.getString("char_name"));
					player.setLastAccess(rs.getLong("lastAccess"));
					player.setExp(rs.getLong("exp"));
					player.setExpBeforeDeath(rs.getLong("expBeforeDeath"));
					player.setLevel(rs.getInt("level"));
					player.setSp(rs.getInt("sp"));
					player.setWantsPeace(rs.getInt("wantspeace"));
					player.setHeading(rs.getInt("heading"));
					player.setKarma(rs.getInt("karma"));
					player.setFame(rs.getInt("fame"));
					player.setPvpKills(rs.getInt("pvpkills"));
					player.setPkKills(rs.getInt("pkkills"));
					player.setOnlineTime(rs.getLong("onlinetime"));
					player.setNewbie(rs.getInt("newbie"));
					player.setNoble(rs.getInt("nobless") == 1);
					
					player.setClanJoinExpiryTime(rs.getLong("clan_join_expiry_time"));
					if (player.getClanJoinExpiryTime() < System.currentTimeMillis())
					{
						player.setClanJoinExpiryTime(0);
					}
					player.setClanCreateExpiryTime(rs.getLong("clan_create_expiry_time"));
					if (player.getClanCreateExpiryTime() < System.currentTimeMillis())
					{
						player.setClanCreateExpiryTime(0);
					}
					
					player.setPowerGrade(rs.getInt("power_grade"));
					player.setPledgeType(rs.getInt("subpledge"));
					// player.setApprentice(rset.getInt("apprentice"));
					
					player.setDeleteTimer(rs.getLong("deletetime"));
					player.setTitle(rs.getString("title"));
					player.setAccessLevel(rs.getInt("accesslevel"));
					player.getAppearance().setTitleColor(rs.getInt("title_color"));
					player.setFistsWeaponItem(player.findFistsWeaponItem(activeClassId));
					player.setUptime(System.currentTimeMillis());
					
					player.setCurrentCp(rs.getDouble("curCp"));
					player.setCurrentHp(rs.getDouble("curHp"));
					player.setCurrentMp(rs.getDouble("curMp"));
					player.setClassIndex(0);
					player.setBaseClass(rs.getInt("base_class"));
					
					// Restore Subclass Data (cannot be done earlier in function)
					DAOFactory.getInstance().getSubclassDAO().load(player);
					
					if (activeClassId != player.getBaseClass())
					{
						for (SubClass subClass : player.getSubClasses().values())
						{
							if (subClass.getClassId() == activeClassId)
							{
								player.setClassIndex(subClass.getClassIndex());
							}
						}
					}
					
					if ((player.getClassIndex() == 0) && (activeClassId != player.getBaseClass()))
					{
						// Subclass in use but doesn't exist in DB -
						// a possible restart-while-modify-subclass cheat has been attempted.
						// Switching to use base class
						player.setClassId(player.getBaseClass());
						LOG.warn("{} reverted to base class. Possibly has tried a relogin exploit while subclassing.", player);
					}
					else
					{
						player.setActiveClass(activeClassId);
					}
					
					player.setApprentice(rs.getInt("apprentice"));
					player.setSponsor(rs.getInt("sponsor"));
					player.setLvlJoinedAcademy(rs.getInt("lvl_joined_academy"));
					player.setIsIn7sDungeon(rs.getInt("isin7sdungeon") == 1);
					
					CursedWeaponsManager.getInstance().checkPlayer(player);
					
					player.setDeathPenaltyBuffLevel(rs.getInt("death_penalty_level"));
					
					player.setVitalityPoints(rs.getInt("vitality_points"), true);
					
					player.getPcCafeSystem().setPoints(rs.getInt("pccafe_points"));
					
					// Set the x,y,z position of the L2PcInstance and make it invisible
					player.setXYZInvisible(rs.getInt("x"), rs.getInt("y"), rs.getInt("z"));
					
					// Set Teleport Bookmark Slot
					player.setBookMarkSlot(rs.getInt("BookmarkSlot"));
					
					// character creation Time
					player.getCreateDate().setTimeInMillis(rs.getTimestamp("createDate").getTime());
					
					// Language
					player.setLang(rs.getString("language"));
					
					// Set Hero status if it applies
					player.setHero(Hero.getInstance().isHero(objectId));
					
					player.setClan(ClanTable.getInstance().getClan(rs.getInt("clanid")));
					
					if (player.getClan() != null)
					{
						if (player.getClan().getLeaderId() != player.getObjectId())
						{
							if (player.getPowerGrade() == 0)
							{
								player.setPowerGrade(5);
							}
							player.setClanPrivileges(player.getClan().getRankPrivs(player.getPowerGrade()));
						}
						else
						{
							player.getClanPrivileges().setAll();
							player.setPowerGrade(1);
						}
						player.setPledgeClass(L2ClanMember.calculatePledgeClass(player));
					}
					else
					{
						if (player.isNoble())
						{
							player.setPledgeClass(5);
						}
						
						if (player.isHero())
						{
							player.setPledgeClass(8);
						}
						
						player.getClanPrivileges().clear();
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Failed loading character!", ex);
		}
		return player;
	}
	
	@Override
	public void loadCharacters(L2PcInstance player)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_CHARACTERS))
		{
			ps.setString(1, player.getAccountName());
			ps.setInt(2, player.getObjectId());
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					player.getAccountChars().put(rs.getInt("charId"), rs.getString("char_name"));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.error("Failed to load {} characters!", player, ex);
		}
	}
	
	@Override
	public boolean insert(L2PcInstance player)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT))
		{
			ps.setString(1, player.getAccountName());
			ps.setInt(2, player.getObjectId());
			ps.setString(3, player.getName());
			ps.setInt(4, player.getBaseLevel());
			ps.setInt(5, player.getMaxHp());
			ps.setDouble(6, player.getCurrentHp());
			ps.setInt(7, player.getMaxCp());
			ps.setDouble(8, player.getCurrentCp());
			ps.setInt(9, player.getMaxMp());
			ps.setDouble(10, player.getCurrentMp());
			ps.setInt(11, player.getAppearance().getFace());
			ps.setInt(12, player.getAppearance().getHairStyle());
			ps.setInt(13, player.getAppearance().getHairColor());
			ps.setInt(14, player.getAppearance().getSex().ordinal());
			ps.setLong(15, player.getBaseExp());
			ps.setInt(16, player.getBaseSp());
			ps.setInt(17, player.getKarma());
			ps.setInt(18, player.getFame());
			ps.setInt(19, player.getPvpKills());
			ps.setInt(20, player.getPkKills());
			ps.setInt(21, player.getClanId());
			ps.setInt(22, player.getRace().ordinal());
			ps.setInt(23, player.getClassId().getId());
			ps.setLong(24, player.getDeleteTimer());
			ps.setInt(25, player.hasDwarvenCraft() ? 1 : 0);
			ps.setString(26, player.getTitle());
			ps.setInt(27, player.getAppearance().getTitleColor());
			ps.setInt(28, player.getAccessLevel().getLevel());
			ps.setInt(29, player.isOnlineInt());
			ps.setInt(30, player.isIn7sDungeon() ? 1 : 0);
			ps.setInt(31, player.getClanPrivileges().getBitmask());
			ps.setInt(32, player.getWantsPeace());
			ps.setInt(33, player.getBaseClass());
			ps.setInt(34, player.getNewbie());
			ps.setInt(35, player.isNoble() ? 1 : 0);
			ps.setLong(36, 0);
			ps.setTimestamp(37, new Timestamp(player.getCreateDate().getTimeInMillis()));
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not insert player data!", ex);
			return false;
		}
		return true;
	}
	
	@Override
	public void storeCharBase(L2PcInstance player)
	{
		var totalOnlineTime = player.getOnlineTime();
		if (player.getOnlineBeginTime() > 0)
		{
			totalOnlineTime += (System.currentTimeMillis() - player.getOnlineBeginTime()) / 1000;
		}
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE))
		{
			ps.setInt(1, player.getBaseLevel());
			ps.setInt(2, player.getMaxHp());
			ps.setDouble(3, player.getCurrentHp());
			ps.setInt(4, player.getMaxCp());
			ps.setDouble(5, player.getCurrentCp());
			ps.setInt(6, player.getMaxMp());
			ps.setDouble(7, player.getCurrentMp());
			ps.setInt(8, player.getAppearance().getFace());
			ps.setInt(9, player.getAppearance().getHairStyle());
			ps.setInt(10, player.getAppearance().getHairColor());
			ps.setInt(11, player.getAppearance().getSex().ordinal());
			ps.setInt(12, player.getHeading());
			ps.setInt(13, player.inObserverMode() ? player.getLastLocation().getX() : player.getX());
			ps.setInt(14, player.inObserverMode() ? player.getLastLocation().getY() : player.getY());
			ps.setInt(15, player.inObserverMode() ? player.getLastLocation().getZ() : player.getZ());
			ps.setLong(16, player.getBaseExp());
			ps.setLong(17, player.getExpBeforeDeath());
			ps.setInt(18, player.getBaseSp());
			ps.setInt(19, player.getKarma());
			ps.setInt(20, player.getFame());
			ps.setInt(21, player.getPvpKills());
			ps.setInt(22, player.getPkKills());
			ps.setInt(23, player.getClanId());
			ps.setInt(24, player.getRace().ordinal());
			ps.setInt(25, player.getClassId().getId());
			ps.setLong(26, player.getDeleteTimer());
			ps.setString(27, player.getTitle());
			ps.setInt(28, player.getAppearance().getTitleColor());
			ps.setInt(29, player.getAccessLevel().getLevel());
			ps.setInt(30, player.isOnlineInt());
			ps.setInt(31, player.isIn7sDungeon() ? 1 : 0);
			ps.setInt(32, player.getClanPrivileges().getBitmask());
			ps.setInt(33, player.getWantsPeace());
			ps.setInt(34, player.getBaseClass());
			ps.setLong(35, totalOnlineTime);
			ps.setInt(36, player.getNewbie());
			ps.setInt(37, player.isNoble() ? 1 : 0);
			ps.setInt(38, player.getPowerGrade());
			ps.setInt(39, player.getPledgeType());
			ps.setInt(40, player.getLvlJoinedAcademy());
			ps.setLong(41, player.getApprentice());
			ps.setLong(42, player.getSponsor());
			ps.setLong(43, player.getClanJoinExpiryTime());
			ps.setLong(44, player.getClanCreateExpiryTime());
			ps.setString(45, player.getName());
			ps.setLong(46, player.getDeathPenaltyBuffLevel());
			ps.setInt(47, player.getBookMarkSlot());
			ps.setInt(48, player.getVitalityPoints());
			ps.setInt(49, player.getPcCafeSystem().getPoints());
			ps.setString(50, player.getLang());
			ps.setInt(51, player.getObjectId());
			
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not store {} base data!", player, ex);
		}
	}
	
	@Override
	public void updateOnlineStatus(L2PcInstance player)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_ONLINE))
		{
			ps.setInt(1, player.isOnlineInt());
			ps.setLong(2, System.currentTimeMillis());
			ps.setInt(3, player.getObjectId());
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Failed updating player online status!", ex);
		}
	}
	
	@Override
	public void checkAndSave(Tops type, String nick, long time, int reward)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_CHARACTERS_CHAR_ID);
			var ps1 = con.prepareStatement(SELECT_CHARACTER_TOP_DATA);
			var ps2 = con.prepareStatement(INSERT_TOP_DATA))
		{
			var charId = 0;
			ps.setString(1, nick);
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					charId = rs.getInt("charId");
				}
				
				if (charId > 0)
				{
					ps1.setInt(1, charId);
					ps1.setString(2, type.getLink());
					ps1.setLong(3, time);
					ps1.setInt(4, reward);
					
					try (var rs1 = ps1.executeQuery())
					{
						if (!rs1.next())
						{
							ps2.setString(1, type.getLink());
							ps2.setString(2, nick);
							ps2.setInt(3, charId);
							ps2.setLong(4, time);
							ps2.setInt(5, reward);
							ps2.execute();
						}
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not check and save top data!", ex);
		}
	}
	
	@Override
	public void clean()
	{
		var calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_YEAR, -30);
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_TOP_DATA))
		{
			ps.setLong(1, calendar.getTimeInMillis() / 1000);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not clean top data!", ex);
		}
	}
	
	@Override
	public String getCharList(L2PcInstance player)
	{
		var result = "";
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_REPAIR_CHARACTERS_ACCOUNT))
		{
			ps.setString(1, player.getAccountName());
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					if (player.getName().compareTo(rs.getString(1)) != 0)
					{
						result += rs.getString(1) + ";";
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not output result for searching characters on account: {}", result, ex);
			return result;
		}
		return result;
	}
	
	@Override
	public boolean checkAccount(L2PcInstance player, String repairplayer)
	{
		boolean result = false;
		var repairCharAccount = "";
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_REPAIR_CHARACTERS))
		{
			ps.setString(1, repairplayer);
			
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					repairCharAccount = rs.getString(1);
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not repair character: {}!", repairplayer, ex);
			return result;
		}
		
		if (player.getAccountName().compareTo(repairCharAccount) == 0)
		{
			result = true;
		}
		return result;
	}
	
	@Override
	public boolean checkJail(L2PcInstance player)
	{
		boolean result = false;
		int key = 0;
		long expiration = 0;
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_REPAIR_CHARACTERS_JAIL))
		{
			ps.setInt(1, key);
			
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					key = rs.getInt(1);
					expiration = rs.getLong(2);
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not repair character from jail!", ex);
			return result;
		}
		
		if ((player.getId() == key) && (expiration >= System.currentTimeMillis()))
		{
			result = true;
		}
		return result;
	}
	
	@Override
	public void repairBadCharacter(String name)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_CHARACTERS_CHAR_ID);
			var ps2 = con.prepareStatement(UPDATE_REPAIR_CHARACTERS);
			var ps3 = con.prepareStatement(DELETE_REPAIR_CHARACTERS);
			var ps4 = con.prepareStatement(UPDATE_REPAIR_CHARACTERS_ID))
		{
			ps.setString(1, name);
			
			try (var rs = ps.executeQuery())
			{
				var objId = 0;
				if (rs.next())
				{
					objId = rs.getInt(1);
				}
				
				if (objId == 0)
				{
					return;
				}
				
				ps2.setInt(1, objId);
				ps2.execute();
				
				ps3.setInt(1, objId);
				ps3.execute();
				
				ps4.setInt(1, objId);
				ps4.execute();
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not repair character: {}!", name, ex);
		}
	}
	
	@Override
	public void deleteCharByCharId(int charId)
	{
		try (var con = ConnectionFactory.getInstance().getConnection())
		{
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_POST_FRIENDS))
			{
				ps.setInt(1, charId);
				ps.setInt(2, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_FRIENDS))
			{
				ps.setInt(1, charId);
				ps.setInt(2, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_HENNAS))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_MACROSES))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_MINIGAME_SCORE))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_QUESTS))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_GLOBAL_DATA))
			{
				ps.setInt(1, charId);
				ps.executeUpdate();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_RECIPEBOOK))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_SHORTCUTS))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_SKILLS))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_SKILLS_SAVE))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_SUBCLASSES))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_ITEM_MALL_TRANSACTIONS))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_HEROES))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_OLYMPIAD_NOBLES))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_SEVEN_SIGNS))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_PETS))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_ITEM_ATTRIBUTES))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_ITEMS))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_MERCHANT_LEASE))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_RAID_POINT))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_RECO_BONUS))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_INSTANCE_TIME))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS_VARIABLES))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			try (var ps = con.prepareStatement(DELETE_CHARACTERS))
			{
				ps.setInt(1, charId);
				ps.execute();
			}
			
			if (WeddingConfig.ALLOW_WEDDING)
			{
				try (var ps = con.prepareStatement(DELETE_COUPLES))
				{
					ps.setInt(1, charId);
					ps.setInt(2, charId);
					ps.execute();
				}
			}
		}
		catch (Exception ex)
		{
			LOG.error("Error deleting character!", ex);
		}
	}
	
	@Override
	public void addPetition(L2PcInstance player, int rate, String message)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT_FEEDBACK))
		{
			ps.setString(1, player.getName());
			ps.setString(2, player.getLastPetitionGmName());
			ps.setInt(3, rate);
			ps.setString(4, message);
			ps.setLong(5, System.currentTimeMillis());
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.error("Error while saving petition feedback!", ex);
		}
	}
	
	@Override
	public void getChangeAccessLevel(int lvl, String name, L2PcInstance player)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_ACCESS_LEVEL))
		{
			ps.setInt(1, lvl);
			ps.setString(2, name);
			ps.execute();
			
			if (ps.getUpdateCount() == 0)
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_character_not_found_access_level"));
			}
			else
			{
				player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_character_access_level_now_set").replace("%s%", lvl + ""));
			}
		}
		catch (Exception ex)
		{
			player.sendAdminMessage(MessagesData.getInstance().getMessage(player, "admin_changing_sqlexception_character_access_level"));
		}
	}
}
