/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.PlayerKamalokaDAO;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * Player Kamaloka DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class PlayerKamalokaDAOMySQLImpl implements PlayerKamalokaDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(PlayerKamalokaDAOMySQLImpl.class);
	
	private static final String SELECT = "SELECT * FROM `rim_kamaloka` ORDER BY `score` DESC";
	private static final String SELECT_SCORE = "SELECT `score` FROM `rim_kamaloka` WHERE `playerName` = ?";
	private static final String REPLACE = "REPLACE INTO `rim_kamaloka` (`playerName`, `score`) VALUES (?, ?)";
	private static final String DELETE = "DELETE FROM `rim_kamaloka`";
	
	@Override
	public Map<String, Integer> getRatingList()
	{
		final Map<String, Integer> map = new LinkedHashMap<>();
		var playerName = "";
		var score = 0;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT))
		{
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					playerName = rs.getString("playerName");
					score = rs.getInt("score");
					map.put(playerName, score);
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select information in rating for rim kamaloka!", ex);
		}
		return map;
	}
	
	@Override
	public int getScore(L2PcInstance player)
	{
		var score = 0;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_SCORE))
		{
			ps.setString(1, player.getName());
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					score = rs.getInt("score");
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select score in rating for rim kamaloka!", ex);
		}
		return score;
	}
	
	@Override
	public void storeRating(L2PcInstance player, int score)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(REPLACE))
		{
			ps.setString(1, player.getName());
			ps.setInt(2, score);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not store rating for rim kamaloka!", ex);
		}
	}
	
	@Override
	public void resetRating()
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE))
		{
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not removed rating for rim kamaloka!", ex);
		}
		LOG.info("Loaded removed {} rating rim kamaloka entries!", getRatingList().size());
	}
}
