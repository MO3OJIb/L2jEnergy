/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.dao.PlayerMacrosesDAO;
import com.l2jserver.gameserver.enums.MacroType;
import com.l2jserver.gameserver.model.Macro;
import com.l2jserver.gameserver.model.MacroCmd;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * Player Macroses DAO Factory implementation.
 * @author Мо3олЬ
 */
public class PlayerMacrosesDAOMySQLImpl implements PlayerMacrosesDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(PlayerMacrosesDAOMySQLImpl.class);
	
	private static final String INSERT_CHARACTER_MACRO = "INSERT INTO character_macroses (charId,id,icon,name,descr,acronym,commands) values(?,?,?,?,?,?,?)";
	private static final String DELETE_CHARACTER_MACRO = "DELETE FROM character_macroses WHERE charId=? AND id=?";
	private static final String SELECT_CHARACTER_MACRO = "SELECT charId, id, icon, name, descr, acronym, commands FROM character_macroses WHERE charId=?";
	
	@Override
	public void registerMacro(L2PcInstance player, Macro macro)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT_CHARACTER_MACRO))
		{
			ps.setInt(1, player.getObjectId());
			ps.setInt(2, macro.getId());
			ps.setInt(3, macro.getIcon());
			ps.setString(4, macro.getName());
			ps.setString(5, macro.getDescr());
			ps.setString(6, macro.getAcronym());
			var sb = new StringBuilder(300);
			for (var cmd : macro.getCommands())
			{
				StringUtil.append(sb, String.valueOf(cmd.getType().ordinal()), ",", String.valueOf(cmd.getD1()), ",", String.valueOf(cmd.getD2()));
				if ((cmd.getCmd() != null) && (cmd.getCmd().length() > 0))
				{
					StringUtil.append(sb, ",", cmd.getCmd());
				}
				sb.append(';');
			}
			
			if (sb.length() > 255)
			{
				sb.setLength(255);
			}
			
			ps.setString(7, sb.toString());
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not store {} macroses!", player.getObjectId(), ex);
		}
	}
	
	@Override
	public void deleteMacro(L2PcInstance player, Macro macro)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_CHARACTER_MACRO))
		{
			ps.setInt(1, player.getObjectId());
			ps.setInt(2, macro.getId());
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not delete {} macroses!", player.getObjectId(), ex);
		}
	}
	
	@Override
	public boolean restoreMe(L2PcInstance player, Map<Integer, Macro> macroses)
	{
		macroses.clear();
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_CHARACTER_MACRO))
		{
			ps.setInt(1, player.getObjectId());
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					var id = rs.getInt("id");
					var icon = rs.getInt("icon");
					var name = rs.getString("name");
					var descr = rs.getString("descr");
					var acronym = rs.getString("acronym");
					List<MacroCmd> commands = new ArrayList<>();
					var st1 = new StringTokenizer(rs.getString("commands"), ";");
					while (st1.hasMoreTokens())
					{
						var st = new StringTokenizer(st1.nextToken(), ",");
						if (st.countTokens() < 3)
						{
							continue;
						}
						var type = MacroType.values()[Integer.parseInt(st.nextToken())];
						var d1 = Integer.parseInt(st.nextToken());
						var d2 = Integer.parseInt(st.nextToken());
						var cmd = "";
						if (st.hasMoreTokens())
						{
							cmd = st.nextToken();
						}
						commands.add(new MacroCmd(commands.size(), type, d1, d2, cmd));
					}
					macroses.put(id, new Macro(id, icon, name, descr, acronym, commands));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not store {} macroses!", player.getObjectId(), ex);
		}
		return true;
	}
}
