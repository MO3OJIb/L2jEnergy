/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.LoginServerThread;
import com.l2jserver.gameserver.configuration.config.custom.OfflineConfig;
import com.l2jserver.gameserver.dao.PlayerOfflineTradersDAO;
import com.l2jserver.gameserver.enums.PrivateStoreType;
import com.l2jserver.gameserver.enums.network.GameClientState;
import com.l2jserver.gameserver.model.L2ManufactureItem;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.L2GameClient;

/**
 * Player Offline Trader DAO Factory implementation.
 * @author Мо3олЬ
 */
public class PlayerOfflineTradersDAOMySQLImpl implements PlayerOfflineTradersDAO
{
	private static Logger LOG = LoggerFactory.getLogger(PlayerOfflineTradersDAOMySQLImpl.class);
	
	// SQL DEFINITIONS
	private static final String SAVE_OFFLINE_STATUS = "INSERT INTO character_offline_trade (`charId`,`time`,`type`,`title`) VALUES (?,?,?,?)";
	private static final String SAVE_ITEMS = "INSERT INTO character_offline_trade_items (`charId`,`item`,`count`,`price`) VALUES (?,?,?,?)";
	private static final String CLEAR_OFFLINE_TABLE = "DELETE FROM character_offline_trade";
	private static final String CLEAR_OFFLINE_TABLE_ITEMS = "DELETE FROM character_offline_trade_items";
	private static final String LOAD_OFFLINE_STATUS = "SELECT * FROM character_offline_trade";
	private static final String LOAD_OFFLINE_ITEMS = "SELECT * FROM character_offline_trade_items WHERE charId = ?";
	
	@Override
	public void storeOffliners()
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps1 = con.prepareStatement(CLEAR_OFFLINE_TABLE);
			var ps2 = con.prepareStatement(CLEAR_OFFLINE_TABLE_ITEMS);
			var ps3 = con.prepareStatement(SAVE_OFFLINE_STATUS);
			var ps4 = con.prepareStatement(SAVE_ITEMS))
		{
			ps1.execute();
			ps2.execute();
			con.setAutoCommit(false); // avoid halfway done
			
			for (var pc : L2World.getInstance().getPlayers())
			{
				try
				{
					if ((pc.getPrivateStoreType() != PrivateStoreType.NONE) && pc.isInOfflineMode())
					{
						ps3.setInt(1, pc.getObjectId()); // Char Id
						ps3.setLong(2, pc.getOfflineStartTime());
						ps3.setInt(3, pc.getPrivateStoreType().getId()); // store type
						String title = null;
						
						switch (pc.getPrivateStoreType())
						{
							case BUY ->
							{
								if (!OfflineConfig.OFFLINE_TRADE_ENABLE)
								{
									continue;
								}
								title = pc.getBuyList().getTitle();
								for (var i : pc.getBuyList().getItems())
								{
									ps4.setInt(1, pc.getObjectId());
									ps4.setInt(2, i.getItem().getId());
									ps4.setLong(3, i.getCount());
									ps4.setLong(4, i.getPrice());
									ps4.executeUpdate();
									ps4.clearParameters();
								}
							}
							case SELL, PACKAGE_SELL ->
							{
								if (!OfflineConfig.OFFLINE_TRADE_ENABLE)
								{
									continue;
								}
								title = pc.getSellList().getTitle();
								for (var i : pc.getSellList().getItems())
								{
									ps4.setInt(1, pc.getObjectId());
									ps4.setInt(2, i.getObjectId());
									ps4.setLong(3, i.getCount());
									ps4.setLong(4, i.getPrice());
									ps4.executeUpdate();
									ps4.clearParameters();
								}
							}
							case MANUFACTURE ->
							{
								if (!OfflineConfig.OFFLINE_CRAFT_ENABLE)
								{
									continue;
								}
								title = pc.getStoreName();
								for (var i : pc.getManufactureItems().values())
								{
									ps4.setInt(1, pc.getObjectId());
									ps4.setInt(2, i.getRecipeId());
									ps4.setLong(3, 0);
									ps4.setLong(4, i.getCost());
									ps4.executeUpdate();
									ps4.clearParameters();
								}
							}
						}
						ps3.setString(4, title);
						ps3.executeUpdate();
						ps3.clearParameters();
						con.commit(); // flush
					}
				}
				catch (Exception ex)
				{
					LOG.warn("Error while saving offline trader: {}!", pc.getObjectId(), ex);
				}
			}
			LOG.info("Offline traders stored.");
		}
		catch (Exception ex)
		{
			LOG.warn("Error while saving offline traders!", ex);
		}
	}
	
	@Override
	public void restoreOfflineTraders()
	{
		LOG.info("Loaded offline traders...");
		var nTraders = 0;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.createStatement();
			var rs = ps.executeQuery(LOAD_OFFLINE_STATUS))
		{
			while (rs.next())
			{
				var time = rs.getLong("time");
				if (OfflineConfig.OFFLINE_MAX_DAYS > 0)
				{
					var cal = Calendar.getInstance();
					cal.setTimeInMillis(time);
					cal.add(Calendar.DAY_OF_YEAR, OfflineConfig.OFFLINE_MAX_DAYS);
					if (cal.getTimeInMillis() <= System.currentTimeMillis())
					{
						continue;
					}
				}
				
				var type = PrivateStoreType.findById(rs.getInt("type"));
				if (type == null)
				{
					LOG.warn("PrivateStoreType with id {} could not be found!", rs.getInt("type"));
					continue;
				}
				
				if (type == PrivateStoreType.NONE)
				{
					continue;
				}
				
				L2PcInstance player = null;
				
				try
				{
					var client = new L2GameClient(null);
					client.setDetached(true);
					player = L2PcInstance.load(rs.getInt("charId"));
					client.setActiveChar(player);
					player.setOnlineStatus(true, false);
					client.setAccountName(player.getAccountNamePlayer());
					L2World.getInstance().addPlayer(player);
					client.setState(GameClientState.IN_GAME);
					player.setClient(client);
					player.setOfflineStartTime(time);
					player.spawnMe(player.getX(), player.getY(), player.getZ());
					LoginServerThread.getInstance().addGameServerLogin(player.getAccountName(), client);
					try (var ps1 = con.prepareStatement(LOAD_OFFLINE_ITEMS))
					{
						ps1.setInt(1, player.getObjectId());
						try (var rs1 = ps1.executeQuery())
						{
							switch (type)
							{
								case BUY ->
								{
									while (rs1.next())
									{
										if (player.getBuyList().addItemByItemId(rs1.getInt(2), rs1.getLong(3), rs1.getLong(4)) == null)
										{
											throw new NullPointerException();
										}
									}
									player.getBuyList().setTitle(rs.getString("title"));
								}
								case SELL, PACKAGE_SELL ->
								{
									while (rs1.next())
									{
										if (player.getSellList().addItem(rs1.getInt(2), rs1.getLong(3), rs1.getLong(4)) == null)
										{
											throw new NullPointerException();
										}
									}
									player.getSellList().setTitle(rs.getString("title"));
									player.getSellList().setPackaged(type == PrivateStoreType.PACKAGE_SELL);
								}
								case MANUFACTURE ->
								{
									while (rs1.next())
									{
										player.getManufactureItems().put(rs1.getInt(2), new L2ManufactureItem(rs1.getInt(2), rs1.getLong(4)));
									}
									player.setStoreName(rs.getString("title"));
								}
							}
						}
					}
					player.sitDown();
					if (OfflineConfig.OFFLINE_SET_NAME_COLOR)
					{
						player.getAppearance().setNameColor(OfflineConfig.OFFLINE_NAME_COLOR);
					}
					
					if (!OfflineConfig.OFFLINE_ABNORMAL_EFFECTS.isEmpty())
					{
						player.startAbnormalVisualEffect(true, OfflineConfig.OFFLINE_ABNORMAL_EFFECTS.get(Rnd.get(OfflineConfig.OFFLINE_ABNORMAL_EFFECTS.size())));
					}
					player.setPrivateStoreType(type);
					player.setOnlineStatus(true, true);
					player.restoreEffects();
					player.broadcastUserInfo();
					nTraders++;
				}
				catch (Exception ex)
				{
					LOG.warn("Error loading trader: {}!", player, ex);
					if (player != null)
					{
						player.deleteMe();
					}
				}
			}
			
			LOG.info("Loaded {} Offline trader(s)", nTraders);
			
			try (var s1 = con.createStatement())
			{
				s1.execute(CLEAR_OFFLINE_TABLE);
				s1.execute(CLEAR_OFFLINE_TABLE_ITEMS);
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Error while loaded offline traders!", ex);
		}
	}
}
