/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.PlayerPostFriendDAO;
import com.l2jserver.gameserver.data.sql.impl.CharNameTable;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * Player Post Friend DAO Factory implementation.
 * @author Мо3олЬ
 */
public class PlayerPostFriendDAOMySQLImpl implements PlayerPostFriendDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(PlayerPostFriendDAOMySQLImpl.class);
	
	private static final String INSERT = "INSERT INTO character_post_friends (charId, post_friend) VALUES (?, ?)";
	private static final String DELETE = "DELETE FROM character_post_friends WHERE charId = ? and post_friend = ?";
	private static final String SELECT = "SELECT post_friend FROM character_post_friends WHERE charId = ?";
	
	@Override
	public List<String> select(L2PcInstance player)
	{
		final List<String> contacts = new CopyOnWriteArrayList<>();
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT))
		{
			ps.setInt(1, player.getObjectId());
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					var contactId = rs.getInt(1);
					var name = CharNameTable.getInstance().getNameById(contactId);
					if ((name == null) || name.equals(player.getName()) || (contactId == player.getObjectId()))
					{
						continue;
					}
					contacts.add(name);
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Error found in {}'s post friends list!", player.getName(), ex);
		}
		return contacts;
	}
	
	@Override
	public List<String> insert(L2PcInstance player, int contactId, String name)
	{
		final List<String> contacts = new CopyOnWriteArrayList<>();
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT))
		{
			ps.setInt(1, player.getObjectId());
			ps.setInt(2, contactId);
			ps.execute();
			
			contacts.add(name);
			
			var sm = SystemMessage.getSystemMessage(SystemMessageId.S1_WAS_SUCCESSFULLY_ADDED_TO_YOUR_CONTACT_LIST);
			sm.addString(name);
			player.sendPacket(sm);
		}
		catch (Exception ex)
		{
			LOG.warn("Error found in {}'s post friends list!", player.getName(), ex);
		}
		return contacts;
	}
	
	@Override
	public void remove(L2PcInstance player, int contactId, String name)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE))
		{
			ps.setInt(1, player.getObjectId());
			ps.setInt(2, contactId);
			ps.execute();
			
			var sm = SystemMessage.getSystemMessage(SystemMessageId.S1_WAS_SUCCESSFULLY_DELETED_FROM_YOUR_CONTACT_LIST);
			sm.addString(name);
			player.sendPacket(sm);
		}
		catch (Exception ex)
		{
			LOG.warn("Error found in {}'s post friends list!", player.getName(), ex);
		}
	}
}
