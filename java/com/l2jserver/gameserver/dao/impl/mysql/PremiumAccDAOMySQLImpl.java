/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.PremiumAccDAO;

/**
 * Premium Account DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class PremiumAccDAOMySQLImpl implements PremiumAccDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(PremiumAccDAOMySQLImpl.class);
	
	private static final String SELECT_ACCOUNT_NAME = "SELECT account_name, enddate FROM account_premium WHERE account_name = ?";
	private static final String UPDATE_ACCOUNT_NAME = "REPLACE INTO account_premium (enddate,account_name) VALUE (?,?)";
	private static final String DELETE_ACCOUNT_NAME = "DELETE FROM account_premium WHERE account_name = ?";
	
	@Override
	public Map<String, Long> load(String account)
	{
		final Map<String, Long> premiumData = new HashMap<>();
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_ACCOUNT_NAME))
		{
			ps.setString(1, account);
			try (var rs = ps.executeQuery();)
			{
				if (rs.next())
				{
					premiumData.put(rs.getString("account_name"), rs.getLong("enddate")); // 1.2
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Error loading premium account {}!", account, ex);
		}
		return premiumData;
	}
	
	@Override
	public void update(long data, String account)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_ACCOUNT_NAME))
		{
			ps.setLong(1, data);
			ps.setString(2, account);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not update premium account for {}!", account, ex);
		}
	}
	
	@Override
	public void remove(String account)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_ACCOUNT_NAME))
		{
			ps.setString(1, account);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not delete premium account for {}!", account, ex);
		}
	}
}
