/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.sql.Statement;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.PunishmentDAO;
import com.l2jserver.gameserver.enums.PunishmentAffect;
import com.l2jserver.gameserver.enums.PunishmentType;
import com.l2jserver.gameserver.model.holders.PunishmentHolder;
import com.l2jserver.gameserver.model.punishment.PunishmentTask;

/**
 * Punishment DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class PunishmentDAOMySQLImpl implements PunishmentDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(PunishmentDAOMySQLImpl.class);
	
	private static final String INSERT = "INSERT INTO punishments (`key`, `affect`, `type`, `expiration`, `reason`, `punishedBy`) VALUES (?, ?, ?, ?, ?, ?)";
	private static final String UPDATE = "UPDATE punishments SET expiration = ? WHERE id = ?";
	private static final String SELECT = "SELECT * FROM punishments";
	
	@Override
	public void onStart(String key, PunishmentAffect affect, PunishmentType type, long expirationTime, String reason, String punishedBy, int id, boolean isStored)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS))
		{
			ps.setString(1, key);
			ps.setString(2, affect.name());
			ps.setString(3, type.name());
			ps.setLong(4, expirationTime);
			ps.setString(5, reason);
			ps.setString(6, punishedBy);
			ps.execute();
			
			try (var rs = ps.getGeneratedKeys())
			{
				if (rs.next())
				{
					id = rs.getInt(1);
				}
			}
			isStored = true;
		}
		catch (Exception ex)
		{
			LOG.warn("Couldn't store punishment task for: {} {}!", affect, key, ex);
		}
	}
	
	@Override
	public void onEnd(int id)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE))
		{
			ps.setLong(1, System.currentTimeMillis());
			ps.setLong(2, id);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Couldn't update punishment!", ex);
		}
	}
	
	@Override
	public Map<PunishmentAffect, PunishmentHolder> load()
	{
		Map<PunishmentAffect, PunishmentHolder> tasks = new ConcurrentHashMap<>();
		var initiated = 0;
		var expired = 0;
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var st = con.createStatement();
			var rs = st.executeQuery(SELECT))
		{
			while (rs.next())
			{
				var id = rs.getInt("id");
				var key = rs.getString("key");
				var affect = PunishmentAffect.getByName(rs.getString("affect"));
				var type = PunishmentType.getByName(rs.getString("type"));
				var expirationTime = rs.getLong("expiration");
				var reason = rs.getString("reason");
				var punishedBy = rs.getString("punishedBy");
				if ((type != null) && (affect != null))
				{
					if ((expirationTime > 0) && (System.currentTimeMillis() > expirationTime)) // expired task.
					{
						expired++;
					}
					else
					{
						initiated++;
						tasks.get(affect).addPunishment(new PunishmentTask(id, key, affect, type, expirationTime, reason, punishedBy, true));
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Error while loading punishments", getClass().getSimpleName(), ex);
		}
		LOG.info("Loaded {} active and {} expired punishments.", initiated, expired);
		return tasks;
	}
}
