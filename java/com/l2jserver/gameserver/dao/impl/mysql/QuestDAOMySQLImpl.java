/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.dao.QuestDAO;
import com.l2jserver.gameserver.instancemanager.QuestManager;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.model.quest.QuestState;
import com.l2jserver.gameserver.model.quest.State;

/**
 * Quest DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class QuestDAOMySQLImpl implements QuestDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(QuestDAOMySQLImpl.class);
	
	private static final String INSERT_QUEST = "INSERT INTO character_quests (charId,name,var,value) VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE value=?";
	private static final String UPDATE_QUEST = "UPDATE character_quests SET value=? WHERE charId=? AND name=? AND var = ?";
	private static final String DELETE_QUEST = "DELETE FROM character_quests WHERE charId=? AND name=?";
	
	private static final String DELETE_QUEST_VAR = "DELETE FROM character_quests WHERE charId=? AND name=? AND var=?";
	private static final String DELETE_QUEST_NON_REPEATABLE = "DELETE FROM character_quests WHERE charId=? AND name=? AND var!=?";
	
	private static final String DELETE_QUEST_CHAR_ID = "DELETE FROM character_quests WHERE charId = ? AND name = ?";
	private static final String DELETE_QUEST_CHAR = "DELETE FROM character_quests WHERE charId = ? AND name = ?";
	private static final String SELECT_QUEST_CHAR = "SELECT name, value FROM character_quests WHERE charId = ? AND var = ?";
	private static final String SELECT_QUEST_CHAR_ID = "SELECT name, var, value FROM character_quests WHERE charId = ? AND var <> ?";
	
	@Override
	public void playerEnter(L2PcInstance player)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var invalidQuestData = con.prepareStatement(DELETE_QUEST_CHAR_ID);
			var invalidQuestDataVar = con.prepareStatement(DELETE_QUEST_CHAR);
			var ps1 = con.prepareStatement(SELECT_QUEST_CHAR))
		{
			// Get list of quests owned by the player from database
			
			ps1.setInt(1, player.getObjectId());
			ps1.setString(2, "<state>");
			try (var rs = ps1.executeQuery())
			{
				while (rs.next())
				{
					// Get the ID of the quest and its state
					var questId = rs.getString("name");
					var statename = rs.getString("value");
					
					// Search quest associated with the ID
					var q = QuestManager.getInstance().getQuest(questId);
					if (q == null)
					{
						LOG.warn("Unknown quest {} for player {}", questId, player.getName());
						if (GeneralConfig.AUTODELETE_INVALID_QUEST_DATA)
						{
							invalidQuestData.setInt(1, player.getObjectId());
							invalidQuestData.setString(2, questId);
							invalidQuestData.executeUpdate();
						}
						continue;
					}
					
					// Create a new QuestState for the player that will be added to the player's list of quests
					new QuestState(q, player, State.getStateId(statename));
				}
			}
			
			// Get list of quests owned by the player from the DB in order to add variables used in the quest.
			try (var ps2 = con.prepareStatement(SELECT_QUEST_CHAR_ID))
			{
				ps2.setInt(1, player.getObjectId());
				ps2.setString(2, "<state>");
				try (var rs = ps2.executeQuery())
				{
					while (rs.next())
					{
						var questId = rs.getString("name");
						var var = rs.getString("var");
						var value = rs.getString("value");
						// Get the QuestState saved in the loop before
						var qs = player.getQuestState(questId);
						if (qs == null)
						{
							LOG.info("Lost variable {} in quest {} for player {}", var, questId, player.getName());
							if (GeneralConfig.AUTODELETE_INVALID_QUEST_DATA)
							{
								invalidQuestDataVar.setInt(1, player.getObjectId());
								invalidQuestDataVar.setString(2, questId);
								invalidQuestDataVar.setString(3, var);
								invalidQuestDataVar.executeUpdate();
							}
							continue;
						}
						// Add parameter to the quest
						qs.setInternal(var, value);
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not insert char quest!", ex);
		}
		
		// events
		for (var name : QuestManager.getInstance().getScripts().keySet())
		{
			player.processQuestEvent(name, "enter");
		}
	}
	
	@Override
	public void createQuest(QuestState qs, String key, String value)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INSERT_QUEST))
		{
			ps.setInt(1, qs.getPlayer().getObjectId());
			ps.setString(2, qs.getQuestName());
			ps.setString(3, key);
			ps.setString(4, value);
			ps.setString(5, value);
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not insert player quest!", ex);
		}
	}
	
	@Override
	public void updateQuest(QuestState qs, String key, String value)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(UPDATE_QUEST))
		{
			ps.setString(1, value);
			ps.setInt(2, qs.getPlayer().getObjectId());
			ps.setString(3, qs.getQuestName());
			ps.setString(4, key);
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not update player quest!", ex);
		}
	}
	
	@Override
	public void deleteQuest(QuestState qs, String key)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_QUEST_VAR))
		{
			ps.setInt(1, qs.getPlayer().getObjectId());
			ps.setString(2, qs.getQuestName());
			ps.setString(3, key);
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not delete player quest!", ex);
		}
	}
	
	public void saveGlobalQuestVar(Quest qs, String var, String value)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("REPLACE INTO quest_global_data (quest_name,var,value) VALUES (?,?,?)"))
		{
			ps.setString(1, qs.getName());
			ps.setString(2, var);
			ps.setString(3, value);
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not insert global quest variable!", ex);
		}
	}
	
	@Override
	public void delete(QuestState qs, boolean repeatable)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(repeatable ? DELETE_QUEST : DELETE_QUEST_NON_REPEATABLE))
		{
			ps.setInt(1, qs.getPlayer().getObjectId());
			ps.setString(2, qs.getQuestName());
			if (!repeatable)
			{
				ps.setString(3, "<state>");
			}
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.warn("Unable to delete char quest!", ex);
		}
	}
}
