/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.ShortcutDAO;
import com.l2jserver.gameserver.enums.ShortcutType;
import com.l2jserver.gameserver.model.ShortCuts;
import com.l2jserver.gameserver.model.Shortcut;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * Shortcut DAO MySQL implementation.
 * @author Zoey76
 */
public class ShortcutDAOMySQLImpl implements ShortcutDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(ShortcutDAOMySQLImpl.class);
	
	private static final String INTO = "REPLACE INTO character_shortcuts (charId,slot,page,type,shortcut_id,level,class_index) values(?,?,?,?,?,?,?)";
	private static final String SELECT = "SELECT charId, slot, page, type, shortcut_id, level FROM character_shortcuts WHERE charId=? AND class_index=?";
	private static final String DELETE = "DELETE FROM character_shortcuts WHERE charId=? AND class_index=?";
	private static final String DELETE_SHORT = "DELETE FROM character_shortcuts WHERE charId=? AND slot=? AND page=? AND class_index=?";
	
	@Override
	public void registerShortCut(int objectId, Shortcut shortcut, Shortcut oldShortCut, int classIndex)
	{
		if (oldShortCut != null)
		{
			delete(objectId, oldShortCut, classIndex);
		}
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(INTO))
		{
			ps.setInt(1, objectId);
			ps.setInt(2, shortcut.getSlot());
			ps.setInt(3, shortcut.getPage());
			ps.setInt(4, shortcut.getType().ordinal());
			ps.setInt(5, shortcut.getId());
			ps.setInt(6, shortcut.getLevel());
			ps.setInt(7, classIndex);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not store character shortcut!", ex);
		}
	}
	
	@Override
	public void delete(int objectId, Shortcut shortcut, int classIndex)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_SHORT))
		{
			ps.setInt(1, objectId);
			ps.setInt(2, shortcut.getSlot());
			ps.setInt(3, shortcut.getPage());
			ps.setInt(4, classIndex);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not delete character shortcut!", ex);
		}
	}
	
	@Override
	public boolean restore(L2PcInstance player, Map<Integer, Shortcut> shortCuts)
	{
		shortCuts.clear();
		try (var con = ConnectionFactory.getInstance().getConnection();
			var st = con.prepareStatement(SELECT))
		{
			st.setInt(1, player.getObjectId());
			st.setInt(2, player.getClassIndex());
			
			try (var rs = st.executeQuery())
			{
				while (rs.next())
				{
					var slot = rs.getInt("slot");
					var page = rs.getInt("page");
					var type = rs.getInt("type");
					var id = rs.getInt("shortcut_id");
					var level = rs.getInt("level");
					
					shortCuts.put(slot + (page * ShortCuts.MAX_SHORTCUTS_PER_BAR), new Shortcut(slot, page, ShortcutType.values()[type], id, level, 1));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not restore character shortcuts!", ex);
		}
		return false;
	}
	
	@Override
	public boolean delete(L2PcInstance player, int classIndex)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE))
		{
			ps.setInt(1, player.getObjectId());
			ps.setInt(2, classIndex);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.error("Could not modify sub class for {} to class index {}!", player, classIndex, ex);
			return false;
		}
		return true;
	}
}
