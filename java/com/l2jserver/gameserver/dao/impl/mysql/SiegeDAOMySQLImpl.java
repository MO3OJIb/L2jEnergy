/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.SiegeDAO;
import com.l2jserver.gameserver.model.L2Clan;
import com.l2jserver.gameserver.model.L2SiegeClan;
import com.l2jserver.gameserver.model.entity.Siege;

/**
 * Siege DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class SiegeDAOMySQLImpl implements SiegeDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(SiegeDAOMySQLImpl.class);
	
	private static final String SELECT_SIEGE_CLANS = "SELECT clan_id FROM siege_clans where clan_id=? and castle_id=?";
	private static final String DELETE_SIEGE_CLANS_CASTLE_ID = "DELETE FROM siege_clans WHERE castle_id=?";
	private static final String DELETE_SIEGE_CLANS_CLAN_ID = "DELETE FROM siege_clans WHERE clan_id=?";
	private static final String DELETE_SIEGE_CLANS_CLEAR_SIEGE_WAITING = "DELETE FROM siege_clans WHERE castle_id=? and type = 2";
	
	@Override
	public boolean checkIsRegistered(L2Clan clan, int castleid)
	{
		if (clan == null)
		{
			return false;
		}
		
		if (clan.getCastleId() > 0)
		{
			return true;
		}
		
		var register = false;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_SIEGE_CLANS))
		{
			ps.setInt(1, clan.getId());
			ps.setInt(2, castleid);
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					register = true;
					break;
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not check is registered!", ex);
		}
		return register;
	}
	
	@Override
	public void clearSiegeClan(Siege clan)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_SIEGE_CLANS_CASTLE_ID))
		{
			ps.setInt(1, clan.getCastle().getResidenceId());
			ps.execute();
			
			if (clan.getCastle().getOwnerId() > 0)
			{
				try (var delete = con.prepareStatement(DELETE_SIEGE_CLANS_CLAN_ID))
				{
					delete.setInt(1, clan.getCastle().getOwnerId());
					delete.execute();
				}
			}
			
			clan.getAttackerClans().clear();
			clan.getDefenderClans().clear();
			clan.getDefenderWaitingClans().clear();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not clear siege clan!", ex);
		}
	}
	
	@Override
	public List<L2SiegeClan> clearWaiting(int residenceId)
	{
		List<L2SiegeClan> defenderWaitingClans = new CopyOnWriteArrayList<>();
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_SIEGE_CLANS_CLEAR_SIEGE_WAITING))
		{
			ps.setInt(1, residenceId);
			ps.execute();
			defenderWaitingClans.clear();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not clear siege waiting clan!", ex);
		}
		return defenderWaitingClans;
	}
}
