/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.SiegeGuardDAO;
import com.l2jserver.gameserver.model.entity.Castle;

/**
 * Siege Guard DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class SiegeGuardDAOMySQLImpl implements SiegeGuardDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(SiegeGuardDAOMySQLImpl.class);
	
	@Override
	public void removeMerc(int npcId, int x, int y, int z)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("DELETE FROM castle_siege_guards WHERE npcId = ? and x = ? and y = ? and z = ? and isHired = 1"))
		{
			ps.setInt(1, npcId);
			ps.setInt(2, x);
			ps.setInt(3, y);
			ps.setInt(4, z);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Error deleting hired siege guard at {}, {}, {}!", x, y, z, ex);
		}
	}
	
	@Override
	public void removeMercs(Castle castle)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("DELETE FROM castle_siege_guards WHERE castleId = ? and isHired = 1"))
		{
			ps.setInt(1, castle.getResidenceId());
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Error deleting hired siege guard for castle {}!", castle.getName(), ex);
		}
	}
	
	@Override
	public void saveSiegeGuard(Castle castle, int x, int y, int z, int heading, int npcId, int isHire)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("INSERT INTO castle_siege_guards (castleId, npcId, x, y, z, heading, respawnDelay, isHired) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"))
		{
			ps.setInt(1, castle.getResidenceId());
			ps.setInt(2, npcId);
			ps.setInt(3, x);
			ps.setInt(4, y);
			ps.setInt(5, z);
			ps.setInt(6, heading);
			ps.setInt(7, (isHire == 1 ? 0 : 600));
			ps.setInt(8, isHire);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Error adding siege guard for castle {}!", castle.getName(), ex);
		}
	}
}
