/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.UIKeysDAO;
import com.l2jserver.gameserver.data.xml.impl.UIData;
import com.l2jserver.gameserver.model.ActionKey;

/**
 * UIKeys DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class UIKeysDAOMySQLImpl implements UIKeysDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(UIKeysDAOMySQLImpl.class);
	
	@Override
	public Map<Integer, List<Integer>> getCats(int objectId)
	{
		Map<Integer, List<Integer>> storedCategories = new HashMap<>();
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("SELECT * FROM character_ui_categories WHERE `charId` = ? ORDER BY `catId`, `order`"))
		{
			ps.setInt(1, objectId);
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					UIData.addCategory(storedCategories, rs.getInt("catId"), rs.getInt("cmdId"));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select get cats!", ex);
		}
		
		if (storedCategories.isEmpty())
		{
			storedCategories = UIData.getInstance().getCategories();
		}
		return storedCategories;
	}
	
	@Override
	public Map<Integer, List<ActionKey>> getKeys(int objectId)
	{
		Map<Integer, List<ActionKey>> storedKeys = new HashMap<>();
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("SELECT * FROM character_ui_actions WHERE `charId` = ? ORDER BY `cat`, `order`"))
		{
			ps.setInt(1, objectId);
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					var cat = rs.getInt("cat");
					var cmd = rs.getInt("cmd");
					var key = rs.getInt("key");
					var tgKey1 = rs.getInt("tgKey1");
					var tgKey2 = rs.getInt("tgKey2");
					var show = rs.getInt("show");
					UIData.addKey(storedKeys, cat, new ActionKey(cat, cmd, key, tgKey1, tgKey2, show));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not select get keys!", ex);
		}
		
		if (storedKeys.isEmpty())
		{
			storedKeys = UIData.getInstance().getKeys();
		}
		return storedKeys;
	}
}
