/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.json;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.l2jserver.gameserver.configuration.config.ServerConfig;
import com.l2jserver.gameserver.configuration.config.custom.PvPRewardConfig;
import com.l2jserver.gameserver.model.PvpReward;

/**
 * @author Мо3олЬ
 */
public class PvpRewardData
{
	private static final Logger LOG = LoggerFactory.getLogger(PvpRewardData.class);
	
	private static final Gson GSON = new Gson();
	private static final Type TYPE_LIST_PVP_REWARD = new TypeToken<List<PvpReward>>()
	{
	}.getType();
	
	private final List<PvpReward> _pvpRewards = new ArrayList<>();
	
	public PvpRewardData()
	{
		load();
	}
	
	public void load()
	{
		if (!PvPRewardConfig.ENABLE_PVP_REWARD_SYSTEM)
		{
			return;
		}
		
		_pvpRewards.clear();
		
		try (var reader = new JsonReader(new FileReader(new File(ServerConfig.DATAPACK_ROOT, "data/json/pvpReward.json"))))
		{
			_pvpRewards.addAll(GSON.fromJson(reader, TYPE_LIST_PVP_REWARD));
		}
		catch (FileNotFoundException ex)
		{
			LOG.warn("data/json/pvpReward.json not found!", ex);
		}
		catch (IOException ex)
		{
			LOG.warn("Failed to load pvpReward.json for: ", ex);
		}
		LOG.info("Loaded {} reward's for PvP.", _pvpRewards.size());
	}
	
	public List<PvpReward> getPvpRewards()
	{
		return _pvpRewards;
	}
	
	public static PvpRewardData getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final PvpRewardData INSTANCE = new PvpRewardData();
	}
}
