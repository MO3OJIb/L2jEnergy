/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.sql.impl;

import java.io.File;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.util.filter.BMPFilter;
import com.l2jserver.gameserver.configuration.config.ServerConfig;
import com.l2jserver.gameserver.enums.CrestType;
import com.l2jserver.gameserver.model.L2Crest;

/**
 * Loads and saves crests from database.
 * @author NosBit
 */
public final class CrestTable
{
	private static final Logger LOG = LoggerFactory.getLogger(CrestTable.class);
	
	private final Map<Integer, L2Crest> _crests = new ConcurrentHashMap<>();
	private final AtomicInteger _nextId = new AtomicInteger(1);
	
	protected CrestTable()
	{
		load();
	}
	
	public synchronized void load()
	{
		_crests.clear();
		Set<Integer> crestsInUse = new HashSet<>();
		for (var clan : ClanTable.getInstance().getClans())
		{
			if (clan.getCrestId() != 0)
			{
				crestsInUse.add(clan.getCrestId());
			}
			
			if (clan.getCrestLargeId() != 0)
			{
				crestsInUse.add(clan.getCrestLargeId());
			}
			
			if (clan.getAllyCrestId() != 0)
			{
				crestsInUse.add(clan.getAllyCrestId());
			}
		}
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("SELECT `crest_id`, `data`, `type` FROM `crests` ORDER BY `crest_id` DESC");
			var rs = ps.executeQuery())
		{
			while (rs.next())
			{
				var id = rs.getInt("crest_id");
				
				if (_nextId.get() <= id)
				{
					_nextId.set(id + 1);
				}
				
				// delete all unused crests except the last one we dont want to reuse
				// a crest id because client will display wrong crest if its reused
				if (!crestsInUse.contains(id) && (id != (_nextId.get() - 1)))
				{
					rs.deleteRow();
					continue;
				}
				
				var data = rs.getBytes("data");
				var crestType = CrestType.getById(rs.getInt("type"));
				if (crestType != null)
				{
					_crests.put(id, new L2Crest(id, data, crestType));
				}
				else
				{
					LOG.warn("Unknown crest type found in database. Type:{}!", rs.getInt("type"));
				}
			}
			
		}
		catch (Exception ex)
		{
			LOG.warn("There was an error while loading crests from database!", ex);
		}
		
		moveOldCrestsToDb(crestsInUse);
		
		LOG.info("Loaded {} Crests.", _crests.size());
		
		for (var clan : ClanTable.getInstance().getClans())
		{
			if (clan.getCrestId() != 0)
			{
				if (getCrest(clan.getCrestId()) == null)
				{
					LOG.info("Removing non-existent crest for clan {} [{}], crestId: {}", clan.getName(), clan.getId(), clan.getCrestId());
					clan.setCrestId(0);
					clan.changeClanCrest(0);
				}
			}
			
			if (clan.getCrestLargeId() != 0)
			{
				if (getCrest(clan.getCrestLargeId()) == null)
				{
					LOG.info("Removing non-existent large crest for clan {} [{}], crestLargeId: {}", clan.getName(), clan.getId(), clan.getCrestLargeId());
					clan.setCrestLargeId(0);
					clan.changeLargeCrest(0);
				}
			}
			
			if (clan.getAllyCrestId() != 0)
			{
				if (getCrest(clan.getAllyCrestId()) == null)
				{
					LOG.info("Removing non-existent ally crest for clan {} [{}], allyCrestId: {}", clan.getName(), clan.getId(), clan.getAllyCrestId());
					clan.setAllyCrestId(0);
					clan.changeAllyCrest(0, true);
				}
			}
		}
	}
	
	/**
	 * Moves old crests from data/crests folder to database and deletes crest folder<br>
	 * <b>TODO:</b> remove it after some time
	 * @param crestsInUse the set of crests in use
	 */
	private void moveOldCrestsToDb(Set<Integer> crestsInUse)
	{
		var crestDir = new File(ServerConfig.DATAPACK_ROOT, "data/crests/");
		if (crestDir.exists())
		{
			var files = crestDir.listFiles(new BMPFilter());
			if (files == null)
			{
				return;
			}
			
			for (var file : files)
			{
				try
				{
					var data = Files.readAllBytes(file.toPath());
					if (file.getName().startsWith("Crest_Large_"))
					{
						var crestId = Integer.parseInt(file.getName().substring(12, file.getName().length() - 4));
						if (crestsInUse.contains(crestId))
						{
							var crest = createCrest(data, CrestType.PLEDGE_LARGE);
							if (crest != null)
							{
								for (var clan : ClanTable.getInstance().getClans())
								{
									if (clan.getCrestLargeId() == crestId)
									{
										clan.setCrestLargeId(0);
										clan.changeLargeCrest(crest.getId());
									}
								}
							}
						}
					}
					else if (file.getName().startsWith("Crest_"))
					{
						var crestId = Integer.parseInt(file.getName().substring(6, file.getName().length() - 4));
						if (crestsInUse.contains(crestId))
						{
							var crest = createCrest(data, CrestType.PLEDGE);
							if (crest != null)
							{
								for (var clan : ClanTable.getInstance().getClans())
								{
									if (clan.getCrestId() == crestId)
									{
										clan.setCrestId(0);
										clan.changeClanCrest(crest.getId());
									}
								}
							}
						}
					}
					else if (file.getName().startsWith("AllyCrest_"))
					{
						var crestId = Integer.parseInt(file.getName().substring(10, file.getName().length() - 4));
						if (crestsInUse.contains(crestId))
						{
							var crest = createCrest(data, CrestType.ALLY);
							if (crest != null)
							{
								for (var clan : ClanTable.getInstance().getClans())
								{
									if (clan.getAllyCrestId() == crestId)
									{
										clan.setAllyCrestId(0);
										clan.changeAllyCrest(crest.getId(), false);
									}
								}
							}
						}
					}
					file.delete();
				}
				catch (Exception ex)
				{
					LOG.warn("There was an error while moving crest file {} to database!", file.getName(), ex);
				}
			}
			crestDir.delete();
		}
	}
	
	/**
	 * @param crestId The crest id
	 * @return {@code L2Crest} if crest is found, {@code null} if crest was not found.
	 */
	public L2Crest getCrest(int crestId)
	{
		return _crests.get(crestId);
	}
	
	/**
	 * Creates a {@code L2Crest} object and inserts it in database and cache.
	 * @param data
	 * @param crestType
	 * @return {@code L2Crest} on success, {@code null} on failure.
	 */
	public L2Crest createCrest(byte[] data, CrestType crestType)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("INSERT INTO `crests`(`crest_id`, `data`, `type`) VALUES(?, ?, ?)"))
		{
			var crest = new L2Crest(getNextId(), data, crestType);
			ps.setInt(1, crest.getId());
			ps.setBytes(2, crest.getData());
			ps.setInt(3, crest.getType().getId());
			ps.executeUpdate();
			_crests.put(crest.getId(), crest);
			return crest;
		}
		catch (Exception ex)
		{
			LOG.warn("There was an error while saving crest in database!", ex);
		}
		return null;
	}
	
	/**
	 * Removes crest from database and cache.
	 * @param crestId the id of crest to be removed.
	 */
	public void removeCrest(int crestId)
	{
		_crests.remove(crestId);
		
		// avoid removing last crest id we dont want to lose index...
		// because client will display wrong crest if its reused
		if (crestId == (_nextId.get() - 1))
		{
			return;
		}
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("DELETE FROM `crests` WHERE `crest_id` = ?"))
		{
			ps.setInt(1, crestId);
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.warn("There was an error while deleting crest from database!", ex);
		}
	}
	
	public int getNextId()
	{
		return _nextId.getAndIncrement();
	}
	
	public static CrestTable getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final CrestTable INSTANCE = new CrestTable();
	}
}
