/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.sql.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.data.xml.impl.SkillData;
import com.l2jserver.gameserver.model.L2PetSkillLearn;
import com.l2jserver.gameserver.model.actor.L2Summon;

public class SummonSkillsTable
{
	private static Logger LOG = LoggerFactory.getLogger(SummonSkillsTable.class);
	
	private final Map<Integer, Map<Integer, L2PetSkillLearn>> _skillTrees = new HashMap<>();
	
	protected SummonSkillsTable()
	{
		load();
	}
	
	public void load()
	{
		_skillTrees.clear();
		var count = 0;
		try (var con = ConnectionFactory.getInstance().getConnection();
			var s = con.createStatement();
			var rs = s.executeQuery("SELECT templateId, minLvl, skillId, skillLvl FROM pets_skills"))
		{
			while (rs.next())
			{
				var npcId = rs.getInt("templateId");
				var skillTree = _skillTrees.computeIfAbsent(npcId, k -> new HashMap<>());
				
				var id = rs.getInt("skillId");
				var lvl = rs.getInt("skillLvl");
				skillTree.put(SkillData.getSkillHashCode(id, lvl + 1), new L2PetSkillLearn(id, lvl, rs.getInt("minLvl")));
				count++;
			}
		}
		catch (Exception ex)
		{
			LOG.error("Error while loading pet skill tree!", ex);
		}
		LOG.info("Loaded {} Pet skills.", count);
	}
	
	public int getAvailableLevel(L2Summon cha, int skillId)
	{
		var lvl = 0;
		if (!_skillTrees.containsKey(cha.getId()))
		{
			LOG.warn("Pet id {} does not have any skills assigned!", cha.getId());
			return lvl;
		}
		var skills = _skillTrees.get(cha.getId()).values();
		for (var temp : skills)
		{
			if (temp.getId() != skillId)
			{
				continue;
			}
			if (temp.getLevel() == 0)
			{
				if (cha.getLevel() < 70)
				{
					lvl = (cha.getLevel() / 10);
					if (lvl <= 0)
					{
						lvl = 1;
					}
				}
				else
				{
					lvl = (7 + ((cha.getLevel() - 70) / 5));
				}
				
				// formula usable for skill that have 10 or more skill levels
				var maxLvl = SkillData.getInstance().getMaxLevel(temp.getId());
				if (lvl > maxLvl)
				{
					lvl = maxLvl;
				}
				break;
			}
			else if (temp.getMinLevel() <= cha.getLevel())
			{
				if (temp.getLevel() > lvl)
				{
					lvl = temp.getLevel();
				}
			}
		}
		return lvl;
	}
	
	public List<Integer> getAvailableSkills(L2Summon cha)
	{
		List<Integer> skillIds = new ArrayList<>();
		if (!_skillTrees.containsKey(cha.getId()))
		{
			LOG.warn("Pet id {} does not have any skills assigned!", cha.getId());
			return skillIds;
		}
		var skills = _skillTrees.get(cha.getId()).values();
		for (var temp : skills)
		{
			if (skillIds.contains(temp.getId()))
			{
				continue;
			}
			skillIds.add(temp.getId());
		}
		return skillIds;
	}
	
	public static SummonSkillsTable getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final SummonSkillsTable INSTANCE = new SummonSkillsTable();
	}
}