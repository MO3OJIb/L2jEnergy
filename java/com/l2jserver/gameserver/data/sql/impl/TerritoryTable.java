/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.sql.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.model.L2Territory;
import com.l2jserver.gameserver.model.Location;

/**
 * @author Balancer, Mr
 */
public class TerritoryTable
{
	private static final Logger LOG = LoggerFactory.getLogger(TerritoryTable.class);
	
	private static final Map<Integer, L2Territory> _territory = new HashMap<>();
	
	protected TerritoryTable()
	{
		load();
	}
	
	public Location getRandomPoint(int terr)
	{
		return _territory.get(terr).getRandomPoint();
	}
	
	public int getProcMax(int terr)
	{
		return _territory.get(terr).getProcMax();
	}
	
	public void load()
	{
		_territory.clear();
		try (var con = ConnectionFactory.getInstance().getConnection();
			var s = con.createStatement();
			var rs = s.executeQuery("SELECT * FROM locations WHERE loc_id>0"))
		{
			while (rs.next())
			{
				var terrId = rs.getInt("loc_id");
				var terr = _territory.get(terrId);
				if (terr == null)
				{
					terr = new L2Territory(terrId);
					_territory.put(terrId, terr);
				}
				terr.add(rs.getInt("loc_x"), rs.getInt("loc_y"), rs.getInt("loc_zmin"), rs.getInt("loc_zmax"), rs.getInt("proc"));
			}
			LOG.info("Loaded {} territories from database.", _territory.size());
		}
		catch (Exception ex)
		{
			LOG.error("Failed to load territories from database!", ex);
		}
	}
	
	public static TerritoryTable getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final TerritoryTable INSTANCE = new TerritoryTable();
	}
}
