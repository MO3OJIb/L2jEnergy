/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.xml.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.w3c.dom.Document;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.IXmlReader;
import com.l2jserver.gameserver.enums.ZoneId;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.PunishHolder;
import com.l2jserver.gameserver.model.reported.ReportedCharData;
import com.l2jserver.gameserver.model.reported.ReporterCharData;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * @author BiggBoss
 */
public final class BotReportData implements IXmlReader
{
	public static final int ATTACK_ACTION_BLOCK_ID = -1;
	public static final int TRADE_ACTION_BLOCK_ID = -2;
	public static final int PARTY_ACTION_BLOCK_ID = -3;
	public static final int ACTION_BLOCK_ID = -4;
	public static final int CHAT_BLOCK_ID = -5;
	
	private final Map<Integer, Long> _ipRegistry = new HashMap<>();
	private final Map<Integer, ReporterCharData> _charRegistry = new ConcurrentHashMap<>();
	private final Map<Integer, ReportedCharData> _reports = new ConcurrentHashMap<>();
	private final Map<Integer, PunishHolder> _punishments = new ConcurrentHashMap<>();
	
	protected BotReportData()
	{
		if (GeneralConfig.BOTREPORT_ENABLE)
		{
			load();
			DAOFactory.getInstance().getBotReportDAO().load();
			scheduleResetPointTask();
			LOG.info("Loaded {} bot reports.", _reports.size());
		}
	}
	
	@Override
	public void load()
	{
		parseDatapackFile("configuration/botreport_punishments.xml");
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		for (var n = doc.getFirstChild(); n != null; n = n.getNextSibling())
		{
			if ("list".equalsIgnoreCase(n.getNodeName()))
			{
				for (var d = n.getFirstChild(); d != null; d = d.getNextSibling())
				{
					var attrs = d.getAttributes();
					
					if ("punishment".equalsIgnoreCase(d.getNodeName()))
					{
						int reportCount = -1, skillId = -1, skillLevel = 1, sysMessage = -1;
						
						var attr = attrs.getNamedItem("neededReportCount");
						reportCount = parseInteger(attr);
						attr = attrs.getNamedItem("skillId");
						skillId = parseInteger(attr);
						attr = attrs.getNamedItem("skillLevel");
						var level = attr.getNodeValue();
						attr = attrs.getNamedItem("sysMessageId");
						var systemMessageId = (attr != null) ? attr.getNodeValue() : null;
						
						if (level != null)
						{
							skillLevel = Integer.parseInt(level);
						}
						
						if (systemMessageId != null)
						{
							sysMessage = Integer.parseInt(systemMessageId);
						}
						addPunishment(reportCount, skillId, skillLevel, sysMessage);
					}
				}
			}
		}
	}
	
	public boolean reportBot(L2PcInstance reporter)
	{
		var target = reporter.getTarget();
		
		if (target == null)
		{
			return false;
		}
		
		var bot = target.getActingPlayer();
		
		if ((bot == null) || (target.getObjectId() == reporter.getObjectId()))
		{
			return false;
		}
		
		if (bot.isInsideZone(ZoneId.PEACE) || bot.isInsideZone(ZoneId.PVP))
		{
			reporter.sendPacket(SystemMessageId.YOU_CANNOT_REPORT_CHARACTER_IN_PEACE_OR_BATTLE_ZONE);
			return false;
		}
		
		if (bot.isInOlympiadMode())
		{
			reporter.sendPacket(SystemMessageId.TARGET_NOT_REPORT_CANNOT_REPORT_PEACE_PVP_ZONE_OR_OLYMPIAD_OR_CLAN_WAR_ENEMY);
			return false;
		}
		
		if ((bot.getClan() != null) && bot.getClan().isAtWarWith(reporter.getClan()))
		{
			reporter.sendPacket(SystemMessageId.YOU_CANNOT_REPORT_CLAN_WAR_ENEMY);
			return false;
		}
		
		if (bot.getExp() == bot.getStat().getStartingExp())
		{
			reporter.sendPacket(SystemMessageId.YOU_CANNOT_REPORT_CHAR_WHO_ACQUIRED_XP);
			return false;
		}
		
		var rcd = _reports.get(bot.getObjectId());
		var rcdRep = _charRegistry.get(reporter.getObjectId());
		var reporterId = reporter.getObjectId();
		
		synchronized (this)
		{
			if (_reports.containsKey(reporterId))
			{
				reporter.sendPacket(SystemMessageId.YOU_HAVE_BEEN_REPORTED_AND_CANNOT_REPORT);
				return false;
			}
			
			var ip = hashIp(reporter);
			if (!timeHasPassed(_ipRegistry, ip))
			{
				reporter.sendPacket(SystemMessageId.CANNOT_REPORT_TARGET_ALREDY_REPORTED_BY_CLAN_ALLY_MEMBER_OR_SAME_IP);
				return false;
			}
			
			if (rcd != null)
			{
				if (rcd.alredyReportedBy(reporterId))
				{
					reporter.sendPacket(SystemMessageId.YOU_CANNOT_REPORT_CHAR_AT_THIS_TIME_1);
					return false;
				}
				
				if (!GeneralConfig.BOTREPORT_ALLOW_REPORTS_FROM_SAME_CLAN_MEMBERS && rcd.reportedBySameClan(reporter.getClan()))
				{
					reporter.sendPacket(SystemMessageId.CANNOT_REPORT_TARGET_ALREDY_REPORTED_BY_CLAN_ALLY_MEMBER_OR_SAME_IP);
					return false;
				}
			}
			
			if (rcdRep != null)
			{
				if (rcdRep.getPointsLeft() == 0)
				{
					reporter.sendPacket(SystemMessageId.YOU_HAVE_USED_ALL_POINTS_POINTS_ARE_RESET_AT_NOON);
					return false;
				}
				
				var reuse = (System.currentTimeMillis() - rcdRep.getLastReporTime());
				if (reuse < GeneralConfig.BOTREPORT_REPORT_DELAY)
				{
					var sm = SystemMessage.getSystemMessage(SystemMessageId.YOU_CAN_REPORT_IN_S1_MINS_YOU_HAVE_S2_POINTS_LEFT);
					sm.addInt((int) (reuse / 60000));
					sm.addInt(rcdRep.getPointsLeft());
					reporter.sendPacket(sm);
					return false;
				}
			}
			
			var curTime = System.currentTimeMillis();
			
			if (rcd == null)
			{
				rcd = new ReportedCharData();
				_reports.put(bot.getObjectId(), rcd);
			}
			rcd.addReporter(reporterId, curTime);
			
			if (rcdRep == null)
			{
				rcdRep = new ReporterCharData();
			}
			rcdRep.registerReport(curTime);
			
			_ipRegistry.put(ip, curTime);
			_charRegistry.put(reporterId, rcdRep);
		}
		
		var sm = SystemMessage.getSystemMessage(SystemMessageId.C1_WAS_REPORTED_AS_BOT);
		sm.addCharName(bot);
		reporter.sendPacket(sm);
		
		sm = SystemMessage.getSystemMessage(SystemMessageId.YOU_HAVE_USED_REPORT_POINT_ON_C1_YOU_HAVE_C2_POINTS_LEFT);
		sm.addCharName(bot);
		sm.addInt(rcdRep.getPointsLeft());
		reporter.sendPacket(sm);
		handleReport(bot, rcd);
		return true;
	}
	
	/**
	 * Find the punishs to apply to the given bot and triggers the punish method.
	 * @param bot (L2PcInstance to be punished)
	 * @param rcd (RepotedCharData linked to this bot)
	 */
	private void handleReport(L2PcInstance bot, final ReportedCharData rcd)
	{
		// Report count punishment
		punishBot(bot, _punishments.get(rcd.getReportCount()));
		
		// Range punishments
		for (var key : _punishments.keySet())
		{
			if ((key < 0) && (Math.abs(key) <= rcd.getReportCount()))
			{
				punishBot(bot, _punishments.get(key));
			}
		}
	}
	
	/**
	 * Applies the given punish to the bot if the action is secure
	 * @param bot (L2PcInstance to punish)
	 * @param ph (PunishHolder containing the debuff and a possible system message to send)
	 */
	private void punishBot(L2PcInstance bot, PunishHolder ph)
	{
		if (ph != null)
		{
			ph.getPunish().applyEffects(bot, bot);
			if (ph.getSysMsgId() > -1)
			{
				var id = SystemMessageId.getSystemMessageId(ph.getSysMsgId());
				if (id != null)
				{
					bot.sendPacket(id);
				}
			}
		}
	}
	
	/**
	 * Adds a debuff punishment into the punishments record. If skill does not exist, will log it and return
	 * @param neededReports (report count to trigger this debuff)
	 * @param skillId
	 * @param skillLevel
	 * @param sysMsg (id of a system message to send when applying the punish)
	 */
	void addPunishment(int neededReports, int skillId, int skillLevel, int sysMsg)
	{
		var sk = SkillData.getInstance().getSkill(skillId, skillLevel);
		if (sk != null)
		{
			_punishments.put(neededReports, new PunishHolder(sk, sysMsg));
		}
		else
		{
			LOG.warn("Could not add punishment for {} report(s): Skill {}-{} does not exist!", neededReports, skillId, skillLevel);
		}
	}
	
	void resetPointsAndSchedule()
	{
		synchronized (_charRegistry)
		{
			for (var rcd : _charRegistry.values())
			{
				rcd.setPoints(7);
			}
		}
		scheduleResetPointTask();
	}
	
	private void scheduleResetPointTask()
	{
		try
		{
			var hour = GeneralConfig.BOTREPORT_RESETPOINT_HOUR;
			var c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour[0]));
			c.set(Calendar.MINUTE, Integer.parseInt(hour[1]));
			
			if (System.currentTimeMillis() > c.getTimeInMillis())
			{
				c.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR) + 1);
			}
			ThreadPoolManager.getInstance().scheduleGeneral(new ResetPointTask(), c.getTimeInMillis() - System.currentTimeMillis());
		}
		catch (Exception ex)
		{
			ThreadPoolManager.getInstance().scheduleGeneral(new ResetPointTask(), 24 * 3600 * 1000);
			LOG.warn("Could not properly schedule bot report points reset task. Scheduled in 24 hours.", ex);
		}
	}
	
	private static int hashIp(L2PcInstance player)
	{
		var con = player.getClient().getConnection().getInetAddress().getHostAddress();
		var rawByte = con.split("\\.");
		var rawIp = new int[4];
		for (var i = 0; i < 4; i++)
		{
			rawIp[i] = Integer.parseInt(rawByte[i]);
		}
		return rawIp[0] | (rawIp[1] << 8) | (rawIp[2] << 16) | (rawIp[3] << 24);
	}
	
	private static boolean timeHasPassed(Map<Integer, Long> map, int objectId)
	{
		if (map.containsKey(objectId))
		{
			return (System.currentTimeMillis() - map.get(objectId)) > GeneralConfig.BOTREPORT_REPORT_DELAY;
		}
		return true;
	}
	
	class ResetPointTask implements Runnable
	{
		@Override
		public void run()
		{
			resetPointsAndSchedule();
		}
	}
	
	public static BotReportData getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static final class SingletonHolder
	{
		static final BotReportData INSTANCE = new BotReportData();
	}
}
