/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.xml.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.l2jserver.gameserver.data.IXmlReader;
import com.l2jserver.gameserver.model.bbs.ColorTemplate;

/**
 * Loads the the list of colors for bbs. (TODO)
 * @author Мо3олЬ
 */
public class ColorData implements IXmlReader
{
	private static Map<String, List<ColorTemplate>> _colors = new HashMap<>();
	
	protected ColorData()
	{
		load();
	}
	
	@Override
	public void load()
	{
		_colors.clear();
		parseDatapackDirectory("data/xml/community/service", false);
		LOG.info("Loaded {} colors.", _colors.size());
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		for (Node list = doc.getFirstChild(); list != null; list = list.getNextSibling())
		{
			if ("list".equalsIgnoreCase(list.getNodeName()))
			{
				for (Node colors = list.getFirstChild(); colors != null; colors = colors.getNextSibling())
				{
					if ("color".equalsIgnoreCase(colors.getNodeName()))
					{
						final String color = colors.getAttributes().getNamedItem("rgb").getNodeValue();
						final String nameRu = colors.getAttributes().getNamedItem("nameRu").getNodeValue();
						final String nameEn = colors.getAttributes().getNamedItem("nameEn").getNodeValue();
						
						addColorTemplate(new ColorTemplate(color, nameEn, nameRu));
					}
				}
			}
		}
	}
	
	public void addColorTemplate(final ColorTemplate template)
	{
		final List<ColorTemplate> list = new ArrayList<>();
		list.add(template);
		_colors.put(template.getColor(), list);
	}
	
	public static ColorData getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final ColorData INSTANCE = new ColorData();
	}
}
