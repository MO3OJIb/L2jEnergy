/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.xml.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.l2jserver.gameserver.data.IXmlReader;
import com.l2jserver.gameserver.enums.FenceState;
import com.l2jserver.gameserver.instancemanager.InstanceManager;
import com.l2jserver.gameserver.instancemanager.MapRegionManager;
import com.l2jserver.gameserver.model.actor.instance.L2ColosseumFence;

/**
 * @author FBIagent
 * @author Мо3олЬ
 */
public final class ColosseumFenceData implements IXmlReader
{
	private final Map<Integer, List<L2ColosseumFence>> _static = new HashMap<>();
	private final Map<Integer, List<L2ColosseumFence>> _dynamic = new ConcurrentHashMap<>();
	
	protected ColosseumFenceData()
	{
		load();
	}
	
	@Override
	public void load()
	{
		_static.clear();
		parseDatapackFile("data/xml/colosseum_fences.xml");
		LOG.info("Loaded {} spawn colosseum fence data.", _static.size());
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		for (Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
		{
			if ("list".equals(n.getNodeName()))
			{
				for (Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
				{
					if ("fence".equals(d.getNodeName()))
					{
						int x = Integer.parseInt(d.getAttributes().getNamedItem("x").getNodeValue());
						int y = Integer.parseInt(d.getAttributes().getNamedItem("y").getNodeValue());
						int z = Integer.parseInt(d.getAttributes().getNamedItem("z").getNodeValue());
						int minZ = Integer.parseInt(d.getAttributes().getNamedItem("min_z").getNodeValue());
						int maxZ = Integer.parseInt(d.getAttributes().getNamedItem("max_z").getNodeValue());
						int width = Integer.parseInt(d.getAttributes().getNamedItem("width").getNodeValue());
						int height = Integer.parseInt(d.getAttributes().getNamedItem("height").getNodeValue());
						
						L2ColosseumFence fenceInstance = new L2ColosseumFence(0, x, y, z, minZ, maxZ, width, height, FenceState.CLOSED);
						Integer region = MapRegionManager.getInstance().getMapRegionLocId(fenceInstance);
						if (!_static.containsKey(region))
						{
							_static.put(region, new ArrayList<>());
						}
						_static.get(region).add(fenceInstance);
						fenceInstance.spawnMe();
					}
				}
			}
		}
	}
	
	public L2ColosseumFence addDynamic(int x, int y, int z, int minZ, int maxZ, int width, int height)
	{
		L2ColosseumFence fence = new L2ColosseumFence(0, x, y, z, minZ, maxZ, width, height, FenceState.CLOSED);
		Integer region = MapRegionManager.getInstance().getMapRegionLocId(fence);
		if (!_dynamic.containsKey(region))
		{
			_dynamic.put(region, new ArrayList<>());
		}
		_dynamic.get(region).add(fence);
		fence.spawnMe(x, y, z);
		return fence;
	}
	
	public boolean checkIfFencesBetween(int x, int y, int z, int tx, int ty, int tz, int instanceId)
	{
		final Collection<L2ColosseumFence> allFences;
		if ((instanceId > 0) && (InstanceManager.getInstance().getInstance(instanceId) != null))
		{
			allFences = InstanceManager.getInstance().getInstance(instanceId).getFences();
		}
		else
		{
			allFences = new ArrayList<>();
			final int mapRegionId = MapRegionManager.getInstance().getMapRegionLocId(x, y);
			if (_static.containsKey(mapRegionId))
			{
				allFences.addAll(_static.get(mapRegionId));
			}
			if (_dynamic.containsKey(mapRegionId))
			{
				allFences.addAll(_dynamic.get(mapRegionId));
			}
		}
		
		for (L2ColosseumFence fence : allFences)
		{
			if ((fence.getFenceState() == FenceState.CLOSED) && (fence.isInsideFence(x, y, z) != fence.isInsideFence(tx, ty, tz)))
			{
				return true;
			}
		}
		return false;
	}
	
	public static ColosseumFenceData getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final ColosseumFenceData INSTANCE = new ColosseumFenceData();
	}
}
