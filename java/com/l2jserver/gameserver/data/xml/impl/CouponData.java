/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.xml.impl;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.l2jserver.gameserver.data.IXmlReader;
import com.l2jserver.gameserver.model.holders.IntLongHolder;

/**
 * @author Мо3олЬ
 */
public class CouponData implements IXmlReader
{
	private static int COUPON_LENGTH;
	private static final List<IntLongHolder> _REWARDS = new ArrayList<>();
	
	protected CouponData()
	{
		load();
	}
	
	@Override
	public void load()
	{
		_REWARDS.clear();
		parseDatapackFile("data/xml/сoupons.xml");
		LOG.info("Loaded {} PcCafe Coupons.", _REWARDS.size());
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		for (Node list = doc.getFirstChild(); list != null; list = list.getNextSibling())
		{
			if ("list".equalsIgnoreCase(list.getNodeName()))
			{
				for (Node config = list.getFirstChild(); config != null; config = config.getNextSibling())
				{
					if ("config".equalsIgnoreCase(config.getNodeName()))
					{
						final int length = Integer.parseInt(config.getAttributes().getNamedItem("code_length").getNodeValue());
						setLength(length);
					}
				}
				
				for (Node r = list.getFirstChild(); r != null; r = r.getNextSibling())
				{
					if ("rewards".equalsIgnoreCase(r.getNodeName()))
					{
						for (Node t1 = r.getFirstChild(); t1 != null; t1 = t1.getNextSibling())
						{
							if ("item".equalsIgnoreCase(t1.getNodeName()))
							{
								int itemId = Integer.parseInt(t1.getAttributes().getNamedItem("id").getNodeValue());
								long count = Long.parseLong(t1.getAttributes().getNamedItem("count").getNodeValue());
								
								addItem(new IntLongHolder(itemId, count));
							}
						}
					}
				}
			}
		}
	}
	
	public void addItem(IntLongHolder item)
	{
		_REWARDS.add(item);
	}
	
	public final List<IntLongHolder> getRewards()
	{
		return _REWARDS;
	}
	
	public void setLength(int i)
	{
		COUPON_LENGTH = i;
	}
	
	public final int getLength()
	{
		return COUPON_LENGTH;
	}
	
	public static CouponData getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final CouponData INSTANCE = new CouponData();
	}
}
