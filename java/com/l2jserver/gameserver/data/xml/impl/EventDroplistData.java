/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.xml.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.l2jserver.gameserver.model.holders.EventDateDropHolder;
import com.l2jserver.gameserver.script.DateRange;
import com.l2jserver.gameserver.script.EventDrop;

/**
 * This class manage drop of Special Events created by GM for a defined period.<br>
 * During a Special Event all L2Attackable can drop extra Items.<br>
 * Those extra Items are defined in the table <b>allNpcDateDrops</b>.<br>
 * Each Special Event has a start and end date to stop to drop extra Items automatically.
 */
public class EventDroplistData
{
	private static final List<EventDateDropHolder> ALL_NPC_DATE_DROPS = new ArrayList<>();
	
	/**
	 * Create and Init a new DateDrop then add it to the allNpcDateDrops of EventDroplist .
	 * @param itemIdList The table containing all item identifier of this DateDrop
	 * @param count The table containing min and max value of this DateDrop
	 * @param chance The chance to obtain this drop
	 * @param dateRange The DateRange object to add to this DateDrop
	 */
	public void addGlobalDrop(int[] itemIdList, int[] count, int chance, DateRange dateRange)
	{
		ALL_NPC_DATE_DROPS.add(new EventDateDropHolder(dateRange, new EventDrop(itemIdList, count[0], count[1], chance)));
	}
	
	/**
	 * @param itemId the item Id for the drop
	 * @param min the minimum drop count
	 * @param max the maximum drop count
	 * @param chance the drop chance
	 * @param dateRange the event drop rate range
	 */
	public void addGlobalDrop(int itemId, long min, long max, int chance, DateRange dateRange)
	{
		ALL_NPC_DATE_DROPS.add(new EventDateDropHolder(dateRange, new EventDrop(itemId, min, max, chance)));
	}
	
	/**
	 * Adds an event drop for a given date range.
	 * @param dateRange the date range.
	 * @param eventDrop the event drop.
	 */
	public void addGlobalDrop(DateRange dateRange, EventDrop eventDrop)
	{
		ALL_NPC_DATE_DROPS.add(new EventDateDropHolder(dateRange, eventDrop));
	}
	
	public List<EventDateDropHolder> getAllDrops()
	{
		final List<EventDateDropHolder> list = new ArrayList<>();
		final Date currentDate = new Date();
		for (EventDateDropHolder drop : ALL_NPC_DATE_DROPS)
		{
			if (drop.getDateRange().isWithinRange(currentDate))
			{
				list.add(drop);
			}
		}
		return list;
	}
	
	public static EventDroplistData getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final EventDroplistData INSTANCE = new EventDroplistData();
	}
}
