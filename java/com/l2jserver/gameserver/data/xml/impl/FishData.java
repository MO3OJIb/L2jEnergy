/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.xml.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;

import com.l2jserver.gameserver.data.IXmlReader;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.fishing.L2Fish;

/**
 * This class holds the Fish information.
 * @author nonom
 */
public final class FishData implements IXmlReader
{
	private final Map<Integer, L2Fish> _fishNormal = new HashMap<>();
	private final Map<Integer, L2Fish> _fishEasy = new HashMap<>();
	private final Map<Integer, L2Fish> _fishHard = new HashMap<>();
	
	protected FishData()
	{
		load();
	}
	
	@Override
	public void load()
	{
		_fishEasy.clear();
		_fishNormal.clear();
		_fishHard.clear();
		parseDatapackFile("data/xml/stats/fishing/fishes.xml");
		LOG.info("Loaded {} Fish.", (_fishEasy.size() + _fishNormal.size() + _fishHard.size()));
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		for (var n = doc.getFirstChild(); n != null; n = n.getNextSibling())
		{
			if ("list".equalsIgnoreCase(n.getNodeName()))
			{
				for (var d = n.getFirstChild(); d != null; d = d.getNextSibling())
				{
					if ("fish".equalsIgnoreCase(d.getNodeName()))
					{
						var attrs = d.getAttributes();
						
						var set = new StatsSet();
						for (var i = 0; i < attrs.getLength(); i++)
						{
							var att = attrs.item(i);
							set.set(att.getNodeName(), att.getNodeValue());
						}
						
						var fish = new L2Fish(set);
						switch (fish.getFishGrade())
						{
							case 0 -> _fishEasy.put(fish.getFishId(), fish);
							case 1 -> _fishNormal.put(fish.getFishId(), fish);
							case 2 -> _fishHard.put(fish.getFishId(), fish);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Gets the fish.
	 * @param level the fish Level
	 * @param group the fish Group
	 * @param grade the fish Grade
	 * @return List of Fish that can be fished
	 */
	public List<L2Fish> getFish(int level, int group, int grade)
	{
		final ArrayList<L2Fish> result = new ArrayList<>();
		Map<Integer, L2Fish> fish = null;
		switch (grade)
		{
			case 0 -> fish = _fishEasy;
			case 1 -> fish = _fishNormal;
			case 2 -> fish = _fishHard;
			default ->
			{
				LOG.warn("{}: Unmanaged fish grade!", getClass().getSimpleName());
				return result;
			}
		}
		
		for (var f : fish.values())
		{
			if ((f.getFishLevel() != level) || (f.getFishGroup() != group))
			{
				continue;
			}
			result.add(f);
		}
		
		if (result.isEmpty())
		{
			LOG.warn("{}: Cannot find any fish for level: {} group: {} and grade: {}!", getClass().getSimpleName(), level, group, grade);
		}
		return result;
	}
	
	public static FishData getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final FishData INSTANCE = new FishData();
	}
}
