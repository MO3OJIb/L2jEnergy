/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.xml.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.l2jserver.gameserver.data.IXmlReader;
import com.l2jserver.gameserver.enums.AbsorbCrystalType;
import com.l2jserver.gameserver.model.LevelingInfo;
import com.l2jserver.gameserver.model.SoulCrystal;

/**
 * @author Мо3олЬ
 */
public class LevelUpCrystalData implements IXmlReader
{
	private final Map<Integer, SoulCrystal> _soulCrystal = new HashMap<>();
	private final Map<Integer, Map<Integer, LevelingInfo>> _npcLevelInfo = new HashMap<>();
	
	public LevelUpCrystalData()
	{
		load();
	}
	
	@Override
	public synchronized void load()
	{
		_soulCrystal.clear();
		_npcLevelInfo.clear();
		
		parseDatapackFile("data/xml/levelUpCrystalData.xml");
		
		LOG.info("Loaded {} Soul Crystal data.", _soulCrystal.size());
		LOG.info("Loaded {} Npc Leveling info data.", _npcLevelInfo.size());
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		for (Node first = doc.getFirstChild(); first != null; first = first.getNextSibling())
		{
			if ("list".equalsIgnoreCase(first.getNodeName()))
			{
				for (Node n = first.getFirstChild(); n != null; n = n.getNextSibling())
				{
					if ("crystal".equalsIgnoreCase(n.getNodeName()))
					{
						for (Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
						{
							if ("item".equalsIgnoreCase(d.getNodeName()))
							{
								NamedNodeMap attrs = d.getAttributes();
								Node att = attrs.getNamedItem("itemId");
								if (att == null)
								{
									LOG.error("Missing itemId in Crystal List, skipping");
									continue;
								}
								int itemId = Integer.parseInt(attrs.getNamedItem("itemId").getNodeValue());
								
								att = attrs.getNamedItem("level");
								if (att == null)
								{
									LOG.error("Missing level in Crystal List itemId: {}, skipping", itemId);
									continue;
								}
								int level = Integer.parseInt(attrs.getNamedItem("level").getNodeValue());
								
								att = attrs.getNamedItem("leveledItemId");
								if (att == null)
								{
									LOG.error("Missing leveledItemId in Crystal List itemId: {}, skipping", itemId);
									continue;
								}
								int leveledItemId = Integer.parseInt(attrs.getNamedItem("leveledItemId").getNodeValue());
								_soulCrystal.put(itemId, new SoulCrystal(level, itemId, leveledItemId));
							}
						}
					}
					else if ("npc".equalsIgnoreCase(n.getNodeName()))
					{
						for (Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
						{
							if ("item".equalsIgnoreCase(d.getNodeName()))
							{
								NamedNodeMap attrs = d.getAttributes();
								Node att = attrs.getNamedItem("npcId");
								if (att == null)
								{
									LOG.error("Missing npcId in NPC List, skipping");
									continue;
								}
								
								int npcId = Integer.parseInt(att.getNodeValue());
								Map<Integer, LevelingInfo> temp = new HashMap<>();
								for (Node cd = d.getFirstChild(); cd != null; cd = cd.getNextSibling())
								{
									boolean isSkillNeeded = false;
									int chance = 5;
									AbsorbCrystalType absorbType = AbsorbCrystalType.LAST_HIT;
									
									if ("detail".equalsIgnoreCase(cd.getNodeName()))
									{
										attrs = cd.getAttributes();
										
										att = attrs.getNamedItem("absorbType");
										if (att != null)
										{
											absorbType = Enum.valueOf(AbsorbCrystalType.class, att.getNodeValue());
										}
										
										att = attrs.getNamedItem("chance");
										if (att != null)
										{
											chance = Integer.parseInt(att.getNodeValue());
										}
										
										att = attrs.getNamedItem("skill");
										if (att != null)
										{
											isSkillNeeded = Boolean.parseBoolean(att.getNodeValue());
										}
										
										Node att1 = attrs.getNamedItem("maxLevel");
										Node att2 = attrs.getNamedItem("levelList");
										if ((att1 == null) && (att2 == null))
										{
											LOG.error("Missing maxlevel/levelList in NPC List npcId: {}, skipping", npcId);
											continue;
										}
										LevelingInfo info = new LevelingInfo(absorbType, isSkillNeeded, chance);
										if (att1 != null)
										{
											int maxLevel = Integer.parseInt(att1.getNodeValue());
											for (int i = 0; i <= maxLevel; i++)
											{
												temp.put(i, info);
											}
										}
										else if (att2 != null)
										{
											StringTokenizer st = new StringTokenizer(att2.getNodeValue(), ",");
											int tokenCount = st.countTokens();
											for (int i = 0; i < tokenCount; i++)
											{
												Integer value = Integer.decode(st.nextToken().trim());
												if (value == null)
												{
													LOG.error("Bad Level value!! npcId: {} token: {}", npcId, i);
													value = 0;
												}
												temp.put(value, info);
											}
										}
									}
								}
								
								if (temp.isEmpty())
								{
									LOG.error("No leveling info for npcId: {}, skipping", npcId);
									continue;
								}
								_npcLevelInfo.put(npcId, temp);
							}
						}
					}
				}
			}
		}
	}
	
	public Map<Integer, SoulCrystal> getSoulCrystal()
	{
		return _soulCrystal;
	}
	
	public Map<Integer, Map<Integer, LevelingInfo>> getNpcLevelInfo()
	{
		return _npcLevelInfo;
	}
	
	public static LevelUpCrystalData getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final LevelUpCrystalData INSTANCE = new LevelUpCrystalData();
	}
}
