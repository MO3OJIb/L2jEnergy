/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.xml.impl;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.l2jserver.gameserver.InstanceListManager;
import com.l2jserver.gameserver.data.IXmlReader;
import com.l2jserver.gameserver.model.MerchantPriceData;
import com.l2jserver.gameserver.model.actor.instance.L2MerchantInstance;

/**
 * @author KenM
 */
public class MerchantPriceConfigData implements InstanceListManager, IXmlReader
{
	private final Map<Integer, MerchantPriceData> _mpcs = new HashMap<>();
	private MerchantPriceData _defaultMpc;
	
	@Override
	public void load()
	{
		_mpcs.clear();
		parseDatapackFile("data/xml/MerchantPriceConfig.xml");
		LOG.info("Loaded {} Merchant price configs.", _mpcs.size());
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		int defaultPriceConfigId;
		
		Node n = doc.getDocumentElement();
		Node dpcNode = n.getAttributes().getNamedItem("defaultPriceConfig");
		if (dpcNode == null)
		{
			throw new IllegalStateException("merchantPriceConfig must define an 'defaultPriceConfig'");
		}
		defaultPriceConfigId = Integer.parseInt(dpcNode.getNodeValue());
		
		MerchantPriceData mpc;
		for (n = n.getFirstChild(); n != null; n = n.getNextSibling())
		{
			mpc = parseMerchantPriceConfig(n);
			if (mpc != null)
			{
				_mpcs.put(mpc.getId(), mpc);
			}
		}
		
		MerchantPriceData defaultMpc = this.getMerchantPriceConfig(defaultPriceConfigId);
		if (defaultMpc == null)
		{
			throw new IllegalStateException("'defaultPriceConfig' points to an non-loaded priceConfig");
		}
		_defaultMpc = defaultMpc;
	}
	
	public MerchantPriceData getMerchantPriceConfig(L2MerchantInstance npc)
	{
		for (MerchantPriceData mpc : _mpcs.values())
		{
			if ((npc.getWorldRegion() != null) && npc.getWorldRegion().containsZone(mpc.getZoneId()))
			{
				return mpc;
			}
		}
		return _defaultMpc;
	}
	
	public MerchantPriceData getMerchantPriceConfig(int id)
	{
		return _mpcs.get(id);
	}
	
	private MerchantPriceData parseMerchantPriceConfig(Node n)
	{
		if (n.getNodeName().equals("priceConfig"))
		{
			final int id;
			final int baseTax;
			int castleId = -1;
			int zoneId = -1;
			final String name;
			
			Node node = n.getAttributes().getNamedItem("id");
			if (node == null)
			{
				throw new IllegalStateException("Must define the priceConfig 'id'");
			}
			id = Integer.parseInt(node.getNodeValue());
			
			node = n.getAttributes().getNamedItem("name");
			if (node == null)
			{
				throw new IllegalStateException("Must define the priceConfig 'name'");
			}
			name = node.getNodeValue();
			
			node = n.getAttributes().getNamedItem("baseTax");
			if (node == null)
			{
				throw new IllegalStateException("Must define the priceConfig 'baseTax'");
			}
			baseTax = Integer.parseInt(node.getNodeValue());
			
			node = n.getAttributes().getNamedItem("castleId");
			if (node != null)
			{
				castleId = Integer.parseInt(node.getNodeValue());
			}
			
			node = n.getAttributes().getNamedItem("zoneId");
			if (node != null)
			{
				zoneId = Integer.parseInt(node.getNodeValue());
			}
			
			return new MerchantPriceData(id, name, baseTax, castleId, zoneId);
		}
		return null;
	}
	
	@Override
	public void loadInstances()
	{
		load();
	}
	
	@Override
	public void updateReferences()
	{
		for (final MerchantPriceData mpc : _mpcs.values())
		{
			mpc.updateReferences();
		}
	}
	
	@Override
	public void activateInstances()
	{
	}
	
	public static MerchantPriceConfigData getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final MerchantPriceConfigData INSTANCE = new MerchantPriceConfigData();
	}
}
