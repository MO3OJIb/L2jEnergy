/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.xml.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.l2jserver.gameserver.data.IXmlReader;
import com.l2jserver.gameserver.enums.client.Language;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * @author Мо3олЬ
 */
public class MessagesData implements IXmlReader
{
	private final Map<Language, Map<String, String>> _message = new HashMap<>();
	
	protected MessagesData()
	{
		load();
	}
	
	@Override
	public void load()
	{
		_message.clear();
		parseDatapackDirectory("data/xml/translation", false);
		
		for (Map.Entry<Language, Map<String, String>> entry : _message.entrySet())
		{
			LOG.info("Loaded messages {} for lang: {}.", entry.getValue().size(), entry.getKey());
		}
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		for (Node d = doc.getFirstChild(); d != null; d = d.getNextSibling())
		{
			if ("list".equalsIgnoreCase(d.getNodeName()))
			{
				for (Node h = d.getFirstChild(); h != null; h = h.getNextSibling())
				{
					if ("message".equalsIgnoreCase(h.getNodeName()))
					{
						final String id = h.getAttributes().getNamedItem("id").getNodeValue();
						for (Node n1 = h.getFirstChild(); n1 != null; n1 = n1.getNextSibling())
						{
							if ("set".equalsIgnoreCase(n1.getNodeName()))
							{
								addMessage(id, n1.getAttributes().getNamedItem("lang").getNodeValue(), n1.getAttributes().getNamedItem("val").getNodeValue());
							}
						}
					}
				}
			}
		}
	}
	
	public String getMessage(L2PcInstance player, String name)
	{
		Language lang = player == null ? Language.ENGLISH : player.getLanguage();
		
		String text = getMessage(name, lang);
		if ((text == null) && (player != null))
		{
			text = "Not find message: " + name + "; for lang: " + lang;
			_message.get(lang).put(name, text);
		}
		return text;
	}
	
	public String getMessage(String id, Language lang)
	{
		Map<String, String> strings = _message.get(lang);
		return strings.get(id);
	}
	
	public void addMessage(String id, String lang, String text)
	{
		Language language = null;
		for (Language value : Language.VALUES)
		{
			if (value.getShortName().equals(lang))
			{
				language = value;
				break;
			}
		}
		
		if (language == null)
		{
			LOG.error("Unknown language: {}. Id message: {}", lang, id);
			return;
		}
		
		Map<String, String> map = _message.get(language);
		if (map == null)
		{
			_message.put(language, map = new LinkedHashMap<>());
		}
		map.put(id, text);
	}
	
	public static MessagesData getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final MessagesData INSTANCE = new MessagesData();
	}
}