/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.xml.impl;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.l2jserver.gameserver.data.IXmlReader;
import com.l2jserver.gameserver.model.NpcBufferSkills;
import com.l2jserver.gameserver.model.holders.NpcBufferHolder;

public class NpcBufferData implements IXmlReader
{
	private final Map<Integer, NpcBufferSkills> _buffers = new HashMap<>();
	
	private int skillCount = 0;
	private int lastNpcId = 0;
	
	protected NpcBufferData()
	{
		load();
	}
	
	@Override
	public void load()
	{
		_buffers.clear();
		parseDatapackFile("data/xml/npcBuffer.xml");
		LOG.info("Loaded {} buffers and {} skills.", _buffers.size(), skillCount);
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		NpcBufferSkills skills = null;
		for (Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
		{
			if ("list".equals(n.getNodeName()))
			{
				for (Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
				{
					
					final NamedNodeMap attrs = d.getAttributes();
					if ("npc".equals(d.getNodeName()))
					{
						Node attr = attrs.getNamedItem("npcId");
						final int npcId = Integer.parseInt(attr.getNodeValue());
						attr = attrs.getNamedItem("skillId");
						final int skillId = Integer.parseInt(attr.getNodeValue());
						attr = attrs.getNamedItem("skillLevel");
						final int skillLevel = Integer.parseInt(attr.getNodeValue());
						attr = attrs.getNamedItem("skillFeeId");
						final int skillFeeId = Integer.parseInt(attr.getNodeValue());
						attr = attrs.getNamedItem("skillFeeAmount");
						final int skillFeeAmount = Integer.parseInt(attr.getNodeValue());
						attr = attrs.getNamedItem("buffGroup");
						final int buffGroup = Integer.parseInt(attr.getNodeValue());
						
						if (npcId != lastNpcId)
						{
							if (lastNpcId != 0)
							{
								_buffers.put(lastNpcId, skills);
							}
							
							skills = new NpcBufferSkills(npcId);
							skills.addSkill(skillId, skillLevel, skillFeeId, skillFeeAmount, buffGroup);
						}
						else if (skills != null)
						{
							skills.addSkill(skillId, skillLevel, skillFeeId, skillFeeAmount, buffGroup);
						}
						lastNpcId = npcId;
						skillCount++;
					}
					
					if (lastNpcId != 0)
					{
						_buffers.put(lastNpcId, skills);
					}
				}
			}
		}
	}
	
	public NpcBufferHolder getSkillInfo(int npcId, int buffGroup)
	{
		final NpcBufferSkills skills = _buffers.get(npcId);
		if (skills != null)
		{
			return skills.getSkillGroupInfo(buffGroup);
		}
		return null;
	}
	
	public static NpcBufferData getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final NpcBufferData INSTANCE = new NpcBufferData();
	}
}