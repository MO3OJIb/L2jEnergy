/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.xml.impl;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.IXmlReader;
import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.enums.ItemMallFlag;
import com.l2jserver.gameserver.enums.network.ExBrProductReplyType;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.items.L2Item;
import com.l2jserver.gameserver.model.primeshop.PrimeShopGroup;
import com.l2jserver.gameserver.model.primeshop.PrimeShopItem;
import com.l2jserver.gameserver.network.serverpackets.ExBR_BuyProduct;
import com.l2jserver.gameserver.network.serverpackets.ExBR_GamePoint;
import com.l2jserver.gameserver.network.serverpackets.StatusUpdate;
import com.l2jserver.gameserver.util.LoggingUtils;
import com.l2jserver.gameserver.util.Util;

/**
 * @author Мо3олЬ
 */
public class ProductItemData implements IXmlReader
{
	private static final Logger LOG_ITEM_MALL = LoggerFactory.getLogger("itemMall");
	
	private final Map<Integer, PrimeShopGroup> _primeItems = new HashMap<>();
	private final ConcurrentHashMap<Integer, List<PrimeShopGroup>> _recentList = new ConcurrentHashMap<>();
	
	protected ProductItemData()
	{
		load();
	}
	
	@Override
	public void load()
	{
		if (!GeneralConfig.ENABLE_ITEM_MALL)
		{
			return;
		}
		_primeItems.clear();
		parseDatapackDirectory("data/xml/itemMall", false);
		LOG.info("Loaded {} Products item mall", _primeItems.size());
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		for (Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
		{
			if ("list".equalsIgnoreCase(n.getNodeName()))
			{
				for (Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
				{
					if ("product".equalsIgnoreCase(d.getNodeName()))
					{
						Node onSaleNode = d.getAttributes().getNamedItem("on_sale");
						final boolean onSale = (onSaleNode != null) && Boolean.parseBoolean(onSaleNode.getNodeValue());
						if (!onSale)
						{
							continue;
						}
						
						final int productId = Integer.parseInt(d.getAttributes().getNamedItem("id").getNodeValue());
						
						Node categoryNode = d.getAttributes().getNamedItem("category");
						final int category = categoryNode != null ? Integer.parseInt(categoryNode.getNodeValue()) : 5;
						
						Node priceNode = d.getAttributes().getNamedItem("price");
						final int price = priceNode != null ? Integer.parseInt(priceNode.getNodeValue()) : 0;
						
						Node dayWeekNode = d.getAttributes().getNamedItem("day_of_week");
						final int daysOfWeek = dayWeekNode != null ? Integer.parseInt(dayWeekNode.getNodeValue()) : Byte.MAX_VALUE;
						
						Node isEventNode = d.getAttributes().getNamedItem("is_event");
						final boolean isEvent = isEventNode != null ? Boolean.parseBoolean(isEventNode.getNodeValue()) : PrimeShopGroup.DEFAULT_IS_EVENT;
						
						Node startTimeNode = d.getAttributes().getNamedItem("start_sale_date");
						final ZonedDateTime startTimeSale = startTimeNode != null ? getMillisecondsFromString(startTimeNode.getNodeValue()) : PrimeShopGroup.DEFAULT_START_SALE_DATE;
						
						Node endTimeNode = d.getAttributes().getNamedItem("end_sale_date");
						final ZonedDateTime endTimeSale = endTimeNode != null ? getMillisecondsFromString(endTimeNode.getNodeValue()) : PrimeShopGroup.DEFAULT_END_SALE_DATE;
						if (endTimeSale.isBefore(ZonedDateTime.now()))
						{
							continue;
						}
						
						Node isStockNode = d.getAttributes().getNamedItem("stock");
						int isStock = isStockNode != null ? Integer.parseInt(isStockNode.getNodeValue()) : PrimeShopGroup.DEFAULT_CURRENT_STOCK;
						
						Node isMaxStockNode = d.getAttributes().getNamedItem("max_stock");
						int isMaxStock = isMaxStockNode != null ? Integer.parseInt(isMaxStockNode.getNodeValue()) : PrimeShopGroup.DEFAULT_MAX_STOCK;
						
						if (isMaxStock > 0)
						{
							isStock = 0;
							
						}
						
						final ItemMallFlag event = isEvent ? ItemMallFlag.EVENT : ItemMallFlag.NONE;
						
						final PrimeShopGroup product = new PrimeShopGroup(productId, category, price, event, startTimeSale, endTimeSale, daysOfWeek, isStock, isMaxStock);
						
						final List<PrimeShopItem> items = new ArrayList<>();
						for (Node t1 = d.getFirstChild(); t1 != null; t1 = t1.getNextSibling())
						{
							if ("item".equalsIgnoreCase(t1.getNodeName()))
							{
								int itemId = Integer.parseInt(t1.getAttributes().getNamedItem("id").getNodeValue());
								int count = Integer.parseInt(t1.getAttributes().getNamedItem("count").getNodeValue());
								
								final L2Item item = ItemTable.getInstance().getTemplate(itemId);
								if (item == null)
								{
									LOG.error("Item template null for itemId: {} brId: {}!", itemId, productId);
									return;
								}
								items.add(new PrimeShopItem(itemId, count, item.getWeight(), item.isTradeable() ? 1 : 0));
							}
						}
						product.setComponents(items);
						_primeItems.put(productId, product);
					}
				}
			}
		}
	}
	
	private static ZonedDateTime getMillisecondsFromString(final String datetime)
	{
		try
		{
			return TimeUtils.DATE_TIME_FORMATTER.withZone(ZoneId.systemDefault()).parse(datetime, ZonedDateTime::from);
			
		}
		catch (Exception ex)
		{
			LOG.warn("", ex);
		}
		return null;
	}
	
	public boolean calcStartEndTime(int productId)
	{
		if (_primeItems.get(productId) == null)
		{
			return false;
		}
		if ((_primeItems.get(productId).getStartSaleDate() * 1000) >= System.currentTimeMillis())
		{
			return false;
		}
		if ((_primeItems.get(productId).getEndSaleDate() * 1000) <= System.currentTimeMillis())
		{
			return false;
		}
		return true;
	}
	
	public Collection<PrimeShopGroup> getProductValues()
	{
		return _primeItems.values();
	}
	
	public PrimeShopGroup getProduct(int id)
	{
		return _primeItems.get(id);
	}
	
	public void requestBuyItem(L2PcInstance player, int brId, int count)
	{
		if (player.isOutOfControl())
		{
			player.sendPacket(new ExBR_BuyProduct(ExBrProductReplyType.INVALID_USER));
			return;
		}
		
		final PrimeShopGroup item = ProductItemData.getInstance().getProduct(brId);
		
		if (Util.isValidPrimeShop(item, count, player))
		{
			final long price = (item.getPrice() * count);
			
			if (price < 0)
			{
				player.sendPacket(new ExBR_BuyProduct(ExBrProductReplyType.LACK_OF_POINT));
				return;
			}
			
			if (price > player.getPrimePointSystem().getPoints())
			{
				player.sendPacket(new ExBR_BuyProduct(ExBrProductReplyType.LACK_OF_POINT));
				return;
			}
			
			player.getPrimePointSystem().setPoints(player.getPrimePointSystem().getPoints() - price);
			
			if (item.isLimited())
			{
				synchronized (item)
				{
					item._currentStock += count;
				}
			}
			item._price += count;
			
			if (_recentList.get(player.getObjectId()) == null)
			{
				List<PrimeShopGroup> charList = new ArrayList<>();
				charList.add(item);
				_recentList.put(player.getObjectId(), charList);
			}
			else
			{
				_recentList.get(player.getObjectId()).add(item);
			}
			
			for (PrimeShopItem subItem : item.getItems())
			{
				player.addItem("PrimeShop", subItem.getId(), subItem.getValue() * count, player, true);
				LoggingUtils.logItemMall(LOG_ITEM_MALL, "Buy (Item Mall) item ", player, subItem.getId(), subItem.getValue() * count);
			}
			
			StatusUpdate su = new StatusUpdate(player);
			su.addAttribute(StatusUpdate.CUR_LOAD, player.getCurrentLoad());
			player.sendPacket(su);
			player.sendPacket(new ExBR_GamePoint(player));
			player.sendPacket(new ExBR_BuyProduct(ExBrProductReplyType.SUCCESS));
			player.broadcastUserInfo();
			
			DAOFactory.getInstance().getItemMallDAO().addPoduct(player.getObjectId(), item.getProductId(), count, item.getMaxStock());
		}
	}
	
	public static ProductItemData getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final ProductItemData INSTANCE = new ProductItemData();
	}
}
