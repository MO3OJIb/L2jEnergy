/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.data.xml.impl;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.l2jserver.gameserver.data.IXmlReader;
import com.l2jserver.gameserver.model.TeleportLocation;

public class TeleportLocationData implements IXmlReader
{
	private final Map<Integer, TeleportLocation> _teleports = new HashMap<>();
	
	protected TeleportLocationData()
	{
		load();
	}
	
	@Override
	public void load()
	{
		_teleports.clear();
		parseDatapackFile("data/xml/teleportLocations.xml");
		LOG.info("Loaded {} teleport locations.", _teleports.size());
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		for (Node n = doc.getFirstChild(); n != null; n = n.getNextSibling())
		{
			if ("list".equalsIgnoreCase(n.getNodeName()))
			{
				for (Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
				{
					if ("teleport".equalsIgnoreCase(d.getNodeName()))
					{
						int id = Integer.parseInt(d.getAttributes().getNamedItem("id").getNodeValue());
						int x = Integer.parseInt(d.getAttributes().getNamedItem("x").getNodeValue());
						int y = Integer.parseInt(d.getAttributes().getNamedItem("y").getNodeValue());
						int z = Integer.parseInt(d.getAttributes().getNamedItem("z").getNodeValue());
						int price = Integer.parseInt(d.getAttributes().getNamedItem("price").getNodeValue());
						boolean isNoble = Boolean.parseBoolean(d.getAttributes().getNamedItem("isNoble").getNodeValue());
						int itemId = Integer.parseInt(d.getAttributes().getNamedItem("itemId").getNodeValue());
						
						TeleportLocation data = new TeleportLocation(id, x, y, z, price, isNoble, itemId);
						_teleports.put(Integer.valueOf(id), data);
					}
				}
			}
		}
	}
	
	public TeleportLocation getTeleportLocation(int id)
	{
		return _teleports.get(id);
	}
	
	public static TeleportLocationData getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final TeleportLocationData INSTANCE = new TeleportLocationData();
	}
}
