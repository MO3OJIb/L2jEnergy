/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.enums;

import com.l2jserver.gameserver.configuration.config.protection.FloodConfig;

/**
 * @author Мо3олЬ
 */
public enum FloodAction
{
	USE_ITEM(FloodConfig.USE_ITEM_TIME),
	ROLL_DICE(FloodConfig.ROLL_DICE_TIME),
	FIRE_WORK(FloodConfig.FIREWORK_TIME),
	ITEM_PET_SUMMON(FloodConfig.ITEM_PET_SUMMON_TIME),
	HERO_VOICE(FloodConfig.HERO_VOICE_TIME),
	GLOBAL_CHAT(FloodConfig.GLOBAL_CHAT_TIME),
	TRADE_CHAT(FloodConfig.TRADE_CHAT_TIME),
	SUBCLASS(FloodConfig.SUBCLASS_TIME),
	DROP_ITEM(FloodConfig.DROP_ITEM_TIME),
	SERVER_BYPASS(FloodConfig.SERVER_BYPASS_TIME),
	MULTISELL(FloodConfig.MULTISELL_TIME),
	TRANSACTION(FloodConfig.TRANSACTION_TIME),
	MANUFACTURE(FloodConfig.MANUFACTURE_TIME),
	MANOR(FloodConfig.MANOR_TIME),
	SEND_MAIL(FloodConfig.SENDMAIL_TIME),
	CHARACTER_SELECT(FloodConfig.CHARACTER_SELECT_TIME),
	ITEM_AUCTION(FloodConfig.ITEM_AUCTION_TIME),
	PING(FloodConfig.PING_TIME),
	ENCHANT_ITEM(FloodConfig.ENCHANT_ITEM);
	
	private final int _reuseDelay;
	
	private FloodAction(int reuseDelay)
	{
		_reuseDelay = reuseDelay;
	}
	
	public int getReuseDelay()
	{
		return _reuseDelay;
	}
	
	public static final int VALUES_LENGTH = FloodAction.values().length;
}
