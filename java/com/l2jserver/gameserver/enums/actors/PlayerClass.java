/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.enums.actors;

import static com.l2jserver.gameserver.enums.actors.ClassLevel.FIRST;
import static com.l2jserver.gameserver.enums.actors.ClassLevel.FOURTH;
import static com.l2jserver.gameserver.enums.actors.ClassLevel.SECOND;
import static com.l2jserver.gameserver.enums.actors.ClassLevel.THIRD;
import static com.l2jserver.gameserver.enums.actors.ClassType.FIGHTER;
import static com.l2jserver.gameserver.enums.actors.ClassType.MYSTIC;
import static com.l2jserver.gameserver.enums.actors.ClassType.PRIEST;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Set;

import com.l2jserver.gameserver.configuration.config.LimitsConfig;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * @author luisantonioa
 */
public enum PlayerClass
{
	HUMAN_FIGHTER(ClassRace.HUMAN, FIGHTER, FIRST),
	WARRIOR(ClassRace.HUMAN, FIGHTER, SECOND),
	GLADIATOR(ClassRace.HUMAN, FIGHTER, THIRD),
	WARLORD(ClassRace.HUMAN, FIGHTER, THIRD),
	KNIGHT(ClassRace.HUMAN, FIGHTER, SECOND),
	PALADIN(ClassRace.HUMAN, FIGHTER, THIRD),
	DARK_AVENGER(ClassRace.HUMAN, FIGHTER, THIRD),
	ROGUE(ClassRace.HUMAN, FIGHTER, SECOND),
	TREASURE_HUNTER(ClassRace.HUMAN, FIGHTER, THIRD),
	HAWKEYE(ClassRace.HUMAN, FIGHTER, THIRD),
	
	HUMAN_MYSTIC(ClassRace.HUMAN, MYSTIC, FIRST),
	HUMAN_WIZARD(ClassRace.HUMAN, MYSTIC, SECOND),
	SORCERER(ClassRace.HUMAN, MYSTIC, THIRD),
	NECROMANCER(ClassRace.HUMAN, MYSTIC, THIRD),
	WARLOCK(ClassRace.HUMAN, MYSTIC, THIRD),
	CLERIC(ClassRace.HUMAN, PRIEST, SECOND),
	BISHOP(ClassRace.HUMAN, PRIEST, THIRD),
	PROPHET(ClassRace.HUMAN, PRIEST, THIRD),
	
	ELVEN_FIGHTER(ClassRace.ELF, FIGHTER, FIRST),
	ELVEN_KNIGHT(ClassRace.ELF, FIGHTER, SECOND),
	TEMPLE_KNIGHT(ClassRace.ELF, FIGHTER, THIRD),
	SWORD_SINGER(ClassRace.ELF, FIGHTER, THIRD),
	ELVEN_SCOUT(ClassRace.ELF, FIGHTER, SECOND),
	PLAINS_WALKER(ClassRace.ELF, FIGHTER, THIRD),
	SILVER_RANGER(ClassRace.ELF, FIGHTER, THIRD),
	
	ELVEN_MYSTIC(ClassRace.ELF, MYSTIC, FIRST),
	ELVEN_WIZARD(ClassRace.ELF, MYSTIC, SECOND),
	SPELLSINGER(ClassRace.ELF, MYSTIC, THIRD),
	ELEMENTAL_SUMMONER(ClassRace.ELF, MYSTIC, THIRD),
	ELVEN_ORACLE(ClassRace.ELF, PRIEST, SECOND),
	ELVEN_ELDER(ClassRace.ELF, PRIEST, THIRD),
	
	DARK_FIGHTER(ClassRace.DARK_ELF, FIGHTER, FIRST),
	PALUS_KNIGHT(ClassRace.DARK_ELF, FIGHTER, SECOND),
	SHILLIEN_KNIGHT(ClassRace.DARK_ELF, FIGHTER, THIRD),
	BLADEDANCER(ClassRace.DARK_ELF, FIGHTER, THIRD),
	ASSASSIN(ClassRace.DARK_ELF, FIGHTER, SECOND),
	ABYSS_WALKER(ClassRace.DARK_ELF, FIGHTER, THIRD),
	PHANTOM_RANGER(ClassRace.DARK_ELF, FIGHTER, THIRD),
	
	DARK_MYSTIC(ClassRace.DARK_ELF, MYSTIC, FIRST),
	DARK_WIZARD(ClassRace.DARK_ELF, MYSTIC, SECOND),
	SPELLHOWLER(ClassRace.DARK_ELF, MYSTIC, THIRD),
	PHANTOM_SUMMONER(ClassRace.DARK_ELF, MYSTIC, THIRD),
	SHILLIEN_ORACLE(ClassRace.DARK_ELF, PRIEST, SECOND),
	SHILLIEN_ELDER(ClassRace.DARK_ELF, PRIEST, THIRD),
	
	ORC_FIGHTER(ClassRace.ORC, FIGHTER, FIRST),
	ORC_RAIDER(ClassRace.ORC, FIGHTER, SECOND),
	DESTROYER(ClassRace.ORC, FIGHTER, THIRD),
	MONK(ClassRace.ORC, FIGHTER, SECOND),
	TYRANT(ClassRace.ORC, FIGHTER, THIRD),
	
	ORC_MYSTIC(ClassRace.ORC, MYSTIC, FIRST),
	ORC_SHAMAN(ClassRace.ORC, MYSTIC, SECOND),
	OVERLORD(ClassRace.ORC, MYSTIC, THIRD),
	WARCRYER(ClassRace.ORC, MYSTIC, THIRD),
	
	DWARVEN_FIGHTER(ClassRace.DWARF, FIGHTER, FIRST),
	SCAVENGER(ClassRace.DWARF, FIGHTER, SECOND),
	BOUNTY_HUNTER(ClassRace.DWARF, FIGHTER, THIRD),
	ARTISAN(ClassRace.DWARF, FIGHTER, SECOND),
	WARSMITH(ClassRace.DWARF, FIGHTER, THIRD),
	
	dummyEntry1(null, null, null),
	dummyEntry2(null, null, null),
	dummyEntry3(null, null, null),
	dummyEntry4(null, null, null),
	dummyEntry5(null, null, null),
	dummyEntry6(null, null, null),
	dummyEntry7(null, null, null),
	dummyEntry8(null, null, null),
	dummyEntry9(null, null, null),
	dummyEntry10(null, null, null),
	dummyEntry11(null, null, null),
	dummyEntry12(null, null, null),
	dummyEntry13(null, null, null),
	dummyEntry14(null, null, null),
	dummyEntry15(null, null, null),
	dummyEntry16(null, null, null),
	dummyEntry17(null, null, null),
	dummyEntry18(null, null, null),
	dummyEntry19(null, null, null),
	dummyEntry20(null, null, null),
	dummyEntry21(null, null, null),
	dummyEntry22(null, null, null),
	dummyEntry23(null, null, null),
	dummyEntry24(null, null, null),
	dummyEntry25(null, null, null),
	dummyEntry26(null, null, null),
	dummyEntry27(null, null, null),
	dummyEntry28(null, null, null),
	dummyEntry29(null, null, null),
	dummyEntry30(null, null, null),
	/*
	 * (3rd classes)
	 */
	DUELIST(ClassRace.HUMAN, FIGHTER, FOURTH),
	DREADNOUGHT(ClassRace.HUMAN, FIGHTER, FOURTH),
	PHOENIX_KNIGHT(ClassRace.HUMAN, FIGHTER, FOURTH),
	HELL_KNIGHT(ClassRace.HUMAN, FIGHTER, FOURTH),
	SAGGITARIUS(ClassRace.HUMAN, FIGHTER, FOURTH),
	ADVENTURER(ClassRace.HUMAN, FIGHTER, FOURTH),
	ARCHMAGE(ClassRace.HUMAN, MYSTIC, FOURTH),
	SOULTAKER(ClassRace.HUMAN, MYSTIC, FOURTH),
	ARCANA_LORD(ClassRace.HUMAN, MYSTIC, FOURTH),
	CARDINAL(ClassRace.HUMAN, PRIEST, FOURTH),
	HIEROPHANT(ClassRace.HUMAN, PRIEST, FOURTH),
	
	EVAS_TEMPLAR(ClassRace.ELF, FIGHTER, FOURTH),
	SWORD_MUSE(ClassRace.ELF, FIGHTER, FOURTH),
	WIND_RIDER(ClassRace.ELF, FIGHTER, FOURTH),
	MOONLIGHT_SENTINEL(ClassRace.ELF, FIGHTER, FOURTH),
	MYSTIC_MUSE(ClassRace.ELF, MYSTIC, FOURTH),
	ELEMENTAL_MASTER(ClassRace.ELF, MYSTIC, FOURTH),
	EVAS_SAINT(ClassRace.ELF, PRIEST, FOURTH),
	
	SHILLIEN_TEMPLAR(ClassRace.DARK_ELF, FIGHTER, FOURTH),
	SPECTRAL_DANCER(ClassRace.DARK_ELF, FIGHTER, FOURTH),
	GHOST_HUNTER(ClassRace.DARK_ELF, FIGHTER, FOURTH),
	GHOST_SENTINEL(ClassRace.DARK_ELF, FIGHTER, FOURTH),
	STORM_SCREAMER(ClassRace.DARK_ELF, MYSTIC, FOURTH),
	SPECTRAL_MASTER(ClassRace.DARK_ELF, MYSTIC, FOURTH),
	SHILLIEN_SAINT(ClassRace.DARK_ELF, PRIEST, FOURTH),
	
	TITAN(ClassRace.ORC, FIGHTER, FOURTH),
	GRAND_KHAVATARI(ClassRace.ORC, FIGHTER, FOURTH),
	DOMINATOR(ClassRace.ORC, MYSTIC, FOURTH),
	DOOMCRYER(ClassRace.ORC, MYSTIC, FOURTH),
	
	FORTUNE_SEEKER(ClassRace.DWARF, FIGHTER, FOURTH),
	MAESTRO(ClassRace.DWARF, FIGHTER, FOURTH),
	
	dummyEntry31(null, null, null),
	dummyEntry32(null, null, null),
	dummyEntry33(null, null, null),
	dummyEntry34(null, null, null),
	
	MALE_SOLDIER(ClassRace.KAMAEL, FIGHTER, FIRST),
	FEMALE_SOLDIER(ClassRace.KAMAEL, FIGHTER, FIRST),
	TROOPER(ClassRace.KAMAEL, FIGHTER, SECOND),
	WARDER(ClassRace.KAMAEL, FIGHTER, SECOND),
	BERSERKER(ClassRace.KAMAEL, FIGHTER, THIRD),
	MALE_SOULBREAKER(ClassRace.KAMAEL, FIGHTER, THIRD),
	FEMALE_SOULBREAKER(ClassRace.KAMAEL, FIGHTER, THIRD),
	ARBALESTER(ClassRace.KAMAEL, FIGHTER, THIRD),
	DOOMBRINGER(ClassRace.KAMAEL, FIGHTER, FOURTH),
	MALE_SOUL_HOUND(ClassRace.KAMAEL, FIGHTER, FOURTH),
	FEMALE_SOUL_HOUND(ClassRace.KAMAEL, FIGHTER, FOURTH),
	TRICKSTER(ClassRace.KAMAEL, FIGHTER, FOURTH),
	INSPECTOR(ClassRace.KAMAEL, FIGHTER, THIRD),
	JUDICATOR(ClassRace.KAMAEL, FIGHTER, FOURTH);
	
	private final ClassRace _race;
	private final ClassLevel _level;
	private final ClassType _type;
	
	private static final Set<PlayerClass> mainSubclassSet;
	private static final Set<PlayerClass> neverSubclassed = EnumSet.of(OVERLORD, WARSMITH);
	
	private static final Set<PlayerClass> subclasseSet1 = EnumSet.of(DARK_AVENGER, PALADIN, TEMPLE_KNIGHT, SHILLIEN_KNIGHT);
	private static final Set<PlayerClass> subclasseSet2 = EnumSet.of(TREASURE_HUNTER, ABYSS_WALKER, PLAINS_WALKER);
	private static final Set<PlayerClass> subclasseSet3 = EnumSet.of(HAWKEYE, SILVER_RANGER, PHANTOM_RANGER);
	private static final Set<PlayerClass> subclasseSet4 = EnumSet.of(WARLOCK, ELEMENTAL_SUMMONER, PHANTOM_SUMMONER);
	private static final Set<PlayerClass> subclasseSet5 = EnumSet.of(SORCERER, SPELLSINGER, SPELLHOWLER);
	
	private static final EnumMap<PlayerClass, Set<PlayerClass>> subclassSetMap = new EnumMap<>(PlayerClass.class);
	
	static
	{
		Set<PlayerClass> subclasses = getSet(null, THIRD);
		subclasses.removeAll(neverSubclassed);
		
		mainSubclassSet = subclasses;
		
		subclassSetMap.put(DARK_AVENGER, subclasseSet1);
		subclassSetMap.put(PALADIN, subclasseSet1);
		subclassSetMap.put(TEMPLE_KNIGHT, subclasseSet1);
		subclassSetMap.put(SHILLIEN_KNIGHT, subclasseSet1);
		
		subclassSetMap.put(TREASURE_HUNTER, subclasseSet2);
		subclassSetMap.put(ABYSS_WALKER, subclasseSet2);
		subclassSetMap.put(PLAINS_WALKER, subclasseSet2);
		
		subclassSetMap.put(HAWKEYE, subclasseSet3);
		subclassSetMap.put(SILVER_RANGER, subclasseSet3);
		subclassSetMap.put(PHANTOM_RANGER, subclasseSet3);
		
		subclassSetMap.put(WARLOCK, subclasseSet4);
		subclassSetMap.put(ELEMENTAL_SUMMONER, subclasseSet4);
		subclassSetMap.put(PHANTOM_SUMMONER, subclasseSet4);
		
		subclassSetMap.put(SORCERER, subclasseSet5);
		subclassSetMap.put(SPELLSINGER, subclasseSet5);
		subclassSetMap.put(SPELLHOWLER, subclasseSet5);
	}
	
	PlayerClass(ClassRace race, ClassType pType, ClassLevel pLevel)
	{
		_race = race;
		_level = pLevel;
		_type = pType;
	}
	
	public final Set<PlayerClass> getAvailableSubclasses(L2PcInstance player)
	{
		Set<PlayerClass> subclasses = null;
		
		if (_level == THIRD)
		{
			if (player.getRace() != ClassRace.KAMAEL)
			{
				subclasses = EnumSet.copyOf(mainSubclassSet);
				
				subclasses.remove(this);
				
				switch (player.getRace())
				{
					case ELF:
						subclasses.removeAll(getSet(ClassRace.DARK_ELF, THIRD));
						break;
					case DARK_ELF:
						subclasses.removeAll(getSet(ClassRace.ELF, THIRD));
						break;
				}
				
				subclasses.removeAll(getSet(ClassRace.KAMAEL, THIRD));
				
				Set<PlayerClass> unavailableClasses = subclassSetMap.get(this);
				
				if (unavailableClasses != null)
				{
					subclasses.removeAll(unavailableClasses);
				}
				
			}
			else
			{
				subclasses = getSet(ClassRace.KAMAEL, THIRD);
				subclasses.remove(this);
				// Check sex, male subclasses female and vice versa
				// If server owner set MaxSubclass > 3 some kamael's cannot take 4 sub
				// So, in that situation we must skip sex check
				if (LimitsConfig.MAX_SUBCLASS <= 3)
				{
					if (player.getAppearance().getSex() != null)
					{
						subclasses.removeAll(EnumSet.of(FEMALE_SOULBREAKER));
					}
					else
					{
						subclasses.removeAll(EnumSet.of(MALE_SOULBREAKER));
					}
				}
				if (!player.getSubClasses().containsKey(2) || (player.getSubClasses().get(2).getLevel() < 75))
				{
					subclasses.removeAll(EnumSet.of(INSPECTOR));
				}
			}
		}
		return subclasses;
	}
	
	public static final EnumSet<PlayerClass> getSet(ClassRace race, ClassLevel level)
	{
		EnumSet<PlayerClass> allOf = EnumSet.noneOf(PlayerClass.class);
		
		for (PlayerClass playerClass : EnumSet.allOf(PlayerClass.class))
		{
			if ((race == null) || playerClass.isOfRace(race))
			{
				if ((level == null) || playerClass.isOfLevel(level))
				{
					allOf.add(playerClass);
				}
			}
		}
		return allOf;
	}
	
	public final boolean isOfRace(ClassRace pRace)
	{
		return _race == pRace;
	}
	
	public final boolean isOfType(ClassType pType)
	{
		return _type == pType;
	}
	
	public final boolean isOfLevel(ClassLevel pLevel)
	{
		return _level == pLevel;
	}
	
	public final ClassLevel getLevel()
	{
		return _level;
	}
}
