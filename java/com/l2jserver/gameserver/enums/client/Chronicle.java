/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.enums.client;

/**
 * @author VISTALL
 */
public enum Chronicle
{
	FREYA(207, 214, 215, 216), // ct2.5
	HIGH_FIVE(241, 243, 247, 251, 252, 253, 267, 268, 271, 273); // ct2.6 part 1-6
	
	private int[] _protocols;
	
	Chronicle(int obfuscationLength, int... range)
	{
		_protocols = range;
	}
	
	public int[] getProtocols()
	{
		return _protocols;
	}
}
