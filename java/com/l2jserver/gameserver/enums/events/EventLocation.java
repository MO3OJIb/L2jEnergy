/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.enums.events;

import com.l2jserver.commons.random.Rnd;

/**
 * @author Мо3олЬ
 */
public enum EventLocation
{
	ADEN("Aden", 146558, 148341, 26622, 28560, -2200),
	DION("Dion", 18564, 19200, 144377, 145782, -3081),
	GLUDIN("Gludin", -84040, -81420, 150257, 151175, -3125),
	HV("Hunters Village", 116094, 117141, 75776, 77072, -2700),
	OREN("Oren", 82048, 82940, 53240, 54126, -1490);
	
	private final String _name;
	private final int _minX;
	private final int _maxX;
	private final int _minY;
	private final int _maxY;
	private final int _z;
	
	private EventLocation(String name, int minX, int maxX, int minY, int maxY, int z)
	{
		_name = name;
		_minX = minX;
		_maxX = maxX;
		_minY = minY;
		_maxY = maxY;
		_z = z;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public int getRandomX()
	{
		return Rnd.get(_minX, _maxX);
	}
	
	public int getRandomY()
	{
		return Rnd.get(_minY, _maxY);
	}
	
	public int getZ()
	{
		return _z;
	}
}
