/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.enums.items;

import com.l2jserver.gameserver.model.Elementals;

public enum Elemental
{
	FIRE_STONE(Elementals.FIRE, 9546, ElementalType.STONE),
	WATER_STONE(Elementals.WATER, 9547, ElementalType.STONE),
	WIND_STONE(Elementals.WIND, 9549, ElementalType.STONE),
	EARTH_STONE(Elementals.EARTH, 9548, ElementalType.STONE),
	DIVINE_STONE(Elementals.HOLY, 9551, ElementalType.STONE),
	DARK_STONE(Elementals.DARK, 9550, ElementalType.STONE),
	
	FIRE_ROUGHORE(Elementals.FIRE, 10521, ElementalType.ROUGHORE),
	WATER_ROUGHORE(Elementals.WATER, 10522, ElementalType.ROUGHORE),
	WIND_ROUGHORE(Elementals.WIND, 10524, ElementalType.ROUGHORE),
	EARTH_ROUGHORE(Elementals.EARTH, 10523, ElementalType.ROUGHORE),
	DIVINE_ROUGHORE(Elementals.HOLY, 10526, ElementalType.ROUGHORE),
	DARK_ROUGHORE(Elementals.DARK, 10525, ElementalType.ROUGHORE),
	
	FIRE_CRYSTAL(Elementals.FIRE, 9552, ElementalType.CRYSTAL),
	WATER_CRYSTAL(Elementals.WATER, 9553, ElementalType.CRYSTAL),
	WIND_CRYSTAL(Elementals.WIND, 9555, ElementalType.CRYSTAL),
	EARTH_CRYSTAL(Elementals.EARTH, 9554, ElementalType.CRYSTAL),
	DIVINE_CRYSTAL(Elementals.HOLY, 9557, ElementalType.CRYSTAL),
	DARK_CRYSTAL(Elementals.DARK, 9556, ElementalType.CRYSTAL),
	
	FIRE_JEWEL(Elementals.FIRE, 9558, ElementalType.JEWEL),
	WATER_JEWEL(Elementals.WATER, 9559, ElementalType.JEWEL),
	WIND_JEWEL(Elementals.WIND, 9561, ElementalType.JEWEL),
	EARTH_JEWEL(Elementals.EARTH, 9560, ElementalType.JEWEL),
	DIVINE_JEWEL(Elementals.HOLY, 9563, ElementalType.JEWEL),
	DARK_JEWEL(Elementals.DARK, 9562, ElementalType.JEWEL),
	
	// not yet supported by client (Freya pts)
	FIRE_ENERGY(Elementals.FIRE, 9564, ElementalType.ENERGY),
	WATER_ENERGY(Elementals.WATER, 9565, ElementalType.ENERGY),
	WIND_ENERGY(Elementals.WIND, 9567, ElementalType.ENERGY),
	EARTH_ENERGY(Elementals.EARTH, 9566, ElementalType.ENERGY),
	DIVINE_ENERGY(Elementals.HOLY, 9569, ElementalType.ENERGY),
	DARK_ENERGY(Elementals.DARK, 9568, ElementalType.ENERGY);
	
	private final byte _element;
	private final int _itemId;
	private final ElementalType _type;
	
	private Elemental(byte element, int itemId, ElementalType type)
	{
		_element = element;
		_itemId = itemId;
		_type = type;
	}
	
	public ElementalType getType()
	{
		return _type;
	}
	
	public int getItemId()
	{
		return _itemId;
	}
	
	public byte getElement()
	{
		return _element;
	}
}
