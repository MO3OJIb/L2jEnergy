/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.instancemanager;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import com.l2jserver.gameserver.configuration.config.protection.BaseProtectionConfig;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.L2GameClient;

public final class AntiFeedManager
{
	public static final int GAME_ID = 0;
	public static final int OLYMPIAD_ID = 1;
	public static final int CUSTOM_EVENT_ID = 2;
	
	private final Map<Integer, Long> _lastDeathTimes = new ConcurrentHashMap<>();
	private final Map<Integer, Map<Integer, AtomicInteger>> _eventIPs = new ConcurrentHashMap<>();
	
	protected AntiFeedManager()
	{
	}
	
	public final void setLastDeathTime(int objectId)
	{
		_lastDeathTimes.put(objectId, System.currentTimeMillis());
	}
	
	public final boolean check(L2Character attacker, L2Character target)
	{
		if (!BaseProtectionConfig.ANTIFEED_ENABLE)
		{
			return true;
		}
		
		if (target == null)
		{
			return false;
		}
		
		var targetPlayer = target.getActingPlayer();
		if (targetPlayer == null)
		{
			return false;
		}
		
		if ((BaseProtectionConfig.ANTIFEED_INTERVAL > 0) && _lastDeathTimes.containsKey(targetPlayer.getObjectId()))
		{
			if ((System.currentTimeMillis() - _lastDeathTimes.get(targetPlayer.getObjectId())) < BaseProtectionConfig.ANTIFEED_INTERVAL)
			{
				return false;
			}
		}
		
		if (BaseProtectionConfig.ANTIFEED_DUALBOX && (attacker != null))
		{
			var attackerPlayer = attacker.getActingPlayer();
			if (attackerPlayer == null)
			{
				return false;
			}
			
			var targetClient = targetPlayer.getClient();
			var attackerClient = attackerPlayer.getClient();
			if ((targetClient == null) || (attackerClient == null) || targetClient.isDetached() || attackerClient.isDetached())
			{
				// unable to check ip address
				return !BaseProtectionConfig.ANTIFEED_DISCONNECTED_AS_DUALBOX;
			}
			
			return !targetClient.getConnectionAddress().equals(attackerClient.getConnectionAddress());
		}
		
		return true;
	}
	
	public final void clear()
	{
		_lastDeathTimes.clear();
	}
	
	public final void registerEvent(int eventId)
	{
		_eventIPs.putIfAbsent(eventId, new ConcurrentHashMap<>());
	}
	
	public final boolean tryAddPlayer(int eventId, L2PcInstance player, int max)
	{
		return tryAddClient(eventId, player.getClient(), max);
	}
	
	public final boolean tryAddClient(int eventId, L2GameClient client, int max)
	{
		if (client == null)
		{
			return false; // unable to determine IP address
		}
		
		var event = _eventIPs.get(eventId);
		if (event == null)
		{
			return false; // no such event registered
		}
		
		var addrHash = Integer.valueOf(client.getConnectionAddress().hashCode());
		
		var connectionCount = event.computeIfAbsent(addrHash, k -> new AtomicInteger());
		var whiteListCount = BaseProtectionConfig.DUALBOX_CHECK_WHITELIST.getOrDefault(addrHash, 0);
		if ((whiteListCount < 0) || ((connectionCount.get() + 1) <= (max + whiteListCount)))
		{
			connectionCount.incrementAndGet();
			return true;
		}
		return false;
	}
	
	public final boolean removePlayer(int eventId, L2PcInstance player)
	{
		return removeClient(eventId, player.getClient());
	}
	
	public final boolean removeClient(int eventId, L2GameClient client)
	{
		if (client == null)
		{
			return false; // unable to determine IP address
		}
		
		var event = _eventIPs.get(eventId);
		if (event == null)
		{
			return false; // no such event registered
		}
		
		var addrHash = Integer.valueOf(client.getConnectionAddress().hashCode());
		
		return event.computeIfPresent(addrHash, (k, v) ->
		{
			if ((v == null) || (v.decrementAndGet() == 0))
			{
				return null;
			}
			return v;
		}) != null;
	}
	
	public final void onDisconnect(L2GameClient client)
	{
		if (client == null)
		{
			return;
		}
		
		_eventIPs.forEach((k, v) ->
		{
			removeClient(k, client);
		});
	}
	
	public final void clear(int eventId)
	{
		var event = _eventIPs.get(eventId);
		if (event != null)
		{
			event.clear();
		}
	}
	
	public final int getLimit(L2PcInstance player, int max)
	{
		return getLimit(player.getClient(), max);
	}
	
	public final int getLimit(L2GameClient client, int max)
	{
		if (client == null)
		{
			return max;
		}
		
		var addrHash = Integer.valueOf(client.getConnectionAddress().hashCode());
		int limit = max;
		if (BaseProtectionConfig.DUALBOX_CHECK_WHITELIST.containsKey(addrHash))
		{
			limit += BaseProtectionConfig.DUALBOX_CHECK_WHITELIST.get(addrHash);
		}
		return limit;
	}
	
	public static final AntiFeedManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final AntiFeedManager INSTANCE = new AntiFeedManager();
	}
}