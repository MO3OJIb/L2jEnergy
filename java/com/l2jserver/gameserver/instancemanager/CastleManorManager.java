/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.instancemanager;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.random.Rnd;
import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.DeveloperConfig;
import com.l2jserver.gameserver.configuration.config.ManorConfig;
import com.l2jserver.gameserver.data.IXmlReader;
import com.l2jserver.gameserver.enums.ManorStatus;
import com.l2jserver.gameserver.model.CropProcure;
import com.l2jserver.gameserver.model.L2Seed;
import com.l2jserver.gameserver.model.SeedProduction;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.interfaces.IStorable;
import com.l2jserver.gameserver.network.SystemMessageId;

/**
 * Castle manor system.
 * @author malyelfik
 */
public final class CastleManorManager implements IXmlReader, IStorable
{
	// SQL queries
	private static final String INSERT_PRODUCT = "INSERT INTO castle_manor_production VALUES (?, ?, ?, ?, ?, ?)";
	private static final String INSERT_CROP = "INSERT INTO castle_manor_procure VALUES (?, ?, ?, ?, ?, ?, ?)";
	private static final String DELETE_PRODUCT = "DELETE FROM castle_manor_production";
	private static final String DELETE_CROP_2 = "DELETE FROM castle_manor_procure";
	
	// Current manor status
	private ManorStatus _mode = ManorStatus.APPROVED;
	// Temporary date
	private ZonedDateTime _nextModeChange = null;
	private final ZonedDateTime _currentTime = ZonedDateTime.now();
	// Seeds holder
	private static final Map<Integer, L2Seed> _seeds = new HashMap<>();
	// Manor period settings
	private final Map<Integer, List<CropProcure>> _procure = new HashMap<>();
	private final Map<Integer, List<CropProcure>> _procureNext = new HashMap<>();
	private final Map<Integer, List<SeedProduction>> _production = new HashMap<>();
	private final Map<Integer, List<SeedProduction>> _productionNext = new HashMap<>();
	
	public CastleManorManager()
	{
		if (!ManorConfig.ALLOW_MANOR)
		{
			_mode = ManorStatus.DISABLED;
			LOG.info("Loaded Manor system is deactivated.");
			return;
		}
		
		load(); // Load seed data (XML)
		loadDb(); // Load castle manor data (DB)
		
		// Set mode and start timer
		var time = _currentTime.toLocalTime();
		var refreshTime = LocalTime.of(ManorConfig.ALT_MANOR_REFRESH_TIME, ManorConfig.ALT_MANOR_REFRESH_MIN);
		var approveTime = LocalTime.of(ManorConfig.ALT_MANOR_APPROVE_TIME, ManorConfig.ALT_MANOR_APPROVE_MIN);
		var maintenanceTime = refreshTime.plusMinutes(ManorConfig.ALT_MANOR_MAINTENANCE_MIN);
		
		if ((time.isAfter(refreshTime) && time.isAfter(maintenanceTime)) || time.isBefore(approveTime) ||
			(time.equals(refreshTime) && time.isBefore(maintenanceTime)))
		{
			_mode = ManorStatus.MODIFIABLE;
		}
		else if (time.equals(refreshTime) && time.isAfter(maintenanceTime))
		{
			_mode = ManorStatus.MAINTENANCE;
		}
		
		// Schedule mode change
		scheduleModeChange();
		ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(this::storeMe, ManorConfig.ALT_MANOR_SAVE_PERIOD_RATE, ManorConfig.ALT_MANOR_SAVE_PERIOD_RATE, TimeUnit.HOURS);
		if (DeveloperConfig.DEBUG)
		{
			LOG.debug("Current Manor mode is: {}.", _mode.toString());
		}
	}
	
	@Override
	public final void load()
	{
		parseDatapackFile("data/xml/seeds.xml");
		LOG.info("Loaded {} seeds.", _seeds.size());
	}
	
	@Override
	public final void parseDocument(Document doc)
	{
		StatsSet set;
		NamedNodeMap attrs;
		Node att;
		for (var n = doc.getFirstChild(); n != null; n = n.getNextSibling())
		{
			if ("list".equalsIgnoreCase(n.getNodeName()))
			{
				for (var d = n.getFirstChild(); d != null; d = d.getNextSibling())
				{
					if ("castle".equalsIgnoreCase(d.getNodeName()))
					{
						var castleId = parseInteger(d.getAttributes(), "id");
						for (var c = d.getFirstChild(); c != null; c = c.getNextSibling())
						{
							if ("crop".equalsIgnoreCase(c.getNodeName()))
							{
								set = new StatsSet();
								set.set("castleId", castleId);
								
								attrs = c.getAttributes();
								for (var i = 0; i < attrs.getLength(); i++)
								{
									att = attrs.item(i);
									set.set(att.getNodeName(), att.getNodeValue());
								}
								_seeds.put(set.getInt("seedId"), new L2Seed(set));
							}
						}
					}
				}
			}
		}
	}
	
	private final void loadDb()
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps1 = con.prepareStatement("SELECT * FROM castle_manor_production WHERE castle_id=?");
			var ps2 = con.prepareStatement("SELECT * FROM castle_manor_procure WHERE castle_id=?"))
		{
			for (var castle : CastleManager.getInstance().getCastles())
			{
				var castleId = castle.getResidenceId();
				
				// Clear params
				ps1.clearParameters();
				ps2.clearParameters();
				
				// Seed production
				final List<SeedProduction> pCurrent = new ArrayList<>();
				final List<SeedProduction> pNext = new ArrayList<>();
				ps1.setInt(1, castleId);
				try (var rs = ps1.executeQuery())
				{
					while (rs.next())
					{
						var seedId = rs.getInt("seed_id");
						if (_seeds.containsKey(seedId)) // Don't load unknown seeds
						{
							var sp = new SeedProduction(seedId, rs.getLong("amount"), rs.getLong("price"), rs.getInt("start_amount"));
							if (rs.getBoolean("next_period"))
							{
								pNext.add(sp);
							}
							else
							{
								pCurrent.add(sp);
							}
						}
						else
						{
							LOG.warn("Unknown seed id: {}!", seedId);
						}
					}
				}
				_production.put(castleId, pCurrent);
				_productionNext.put(castleId, pNext);
				
				// Seed procure
				final List<CropProcure> current = new ArrayList<>();
				final List<CropProcure> next = new ArrayList<>();
				ps2.setInt(1, castleId);
				try (var rs = ps2.executeQuery())
				{
					var cropIds = getCropIds();
					while (rs.next())
					{
						var cropId = rs.getInt("crop_id");
						if (cropIds.contains(cropId)) // Don't load unknown crops
						{
							var cp = new CropProcure(cropId, rs.getLong("amount"), rs.getInt("reward_type"), rs.getLong("start_amount"), rs.getLong("price"));
							if (rs.getBoolean("next_period"))
							{
								next.add(cp);
							}
							else
							{
								current.add(cp);
							}
						}
						else
						{
							LOG.warn("Unknown crop id: {}!", cropId);
						}
					}
				}
				_procure.put(castleId, current);
				_procureNext.put(castleId, next);
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Error restoring manor data!", ex);
		}
	}
	
	// -------------------------------------------------------
	// Manor methods
	// -------------------------------------------------------
	private final void scheduleModeChange()
	{
		// Calculate next mode change
		switch (_mode)
		{
			case MODIFIABLE:
				_nextModeChange = ZonedDateTime.of(_currentTime.plusDays(1).toLocalDate(), LocalTime.of(ManorConfig.ALT_MANOR_APPROVE_TIME, ManorConfig.ALT_MANOR_APPROVE_MIN), _currentTime.getZone());
				break;
			case MAINTENANCE:
				_nextModeChange = ZonedDateTime.of(_currentTime.toLocalDate(), LocalTime.of(ManorConfig.ALT_MANOR_REFRESH_TIME, ManorConfig.ALT_MANOR_REFRESH_MIN + ManorConfig.ALT_MANOR_MAINTENANCE_MIN), _currentTime.getZone());
				break;
			case APPROVED:
				_nextModeChange = ZonedDateTime.of(_currentTime.toLocalDate(), LocalTime.of(ManorConfig.ALT_MANOR_REFRESH_TIME, ManorConfig.ALT_MANOR_REFRESH_MIN), _currentTime.getZone());
				break;
		}
		if (_nextModeChange.isBefore(_currentTime))
		{
			_nextModeChange = _nextModeChange.plusDays(1);
		}
		long delay = _currentTime.until(_nextModeChange, ChronoUnit.MILLIS);
		// Schedule mode change
		ThreadPoolManager.getInstance().scheduleGeneral(this::changeMode, delay, TimeUnit.MILLISECONDS);
	}
	
	public final void changeMode()
	{
		switch (_mode)
		{
			case APPROVED:
			{
				// Change mode
				_mode = ManorStatus.MAINTENANCE;
				
				// Update manor period
				for (var castle : CastleManager.getInstance().getCastles())
				{
					var owner = castle.getOwner();
					if (owner == null)
					{
						continue;
					}
					
					var castleId = castle.getResidenceId();
					var cwh = owner.getWarehouse();
					for (var crop : _procure.get(castleId))
					{
						if (crop.getStartAmount() > 0)
						{
							// Adding bought crops to clan warehouse
							if (crop.getStartAmount() != crop.getAmount())
							{
								var count = (long) ((crop.getStartAmount() - crop.getAmount()) * 0.9);
								if ((count < 1) && (Rnd.nextInt(99) < 90))
								{
									count = 1;
								}
								
								if (count > 0)
								{
									cwh.addItem("Manor", getSeedByCrop(crop.getId()).getMatureId(), count, null, null);
								}
							}
							// Reserved and not used money giving back to treasury
							if (crop.getAmount() > 0)
							{
								castle.addToTreasuryNoTax(crop.getAmount() * crop.getPrice());
							}
						}
					}
					
					// Change next period to current and prepare next period data
					final List<SeedProduction> _nextProduction = _productionNext.get(castleId);
					final List<CropProcure> _nextProcure = _procureNext.get(castleId);
					
					_production.put(castleId, _nextProduction);
					_procure.put(castleId, _nextProcure);
					
					if (castle.getTreasury() < getManorCost(castleId, false))
					{
						_productionNext.put(castleId, Collections.emptyList());
						_procureNext.put(castleId, Collections.emptyList());
					}
					else
					{
						final List<SeedProduction> production = new ArrayList<>(_nextProduction);
						for (var s : production)
						{
							s.setAmount(s.getStartAmount());
						}
						_productionNext.put(castleId, production);
						
						final List<CropProcure> procure = new ArrayList<>(_nextProcure);
						for (var cr : procure)
						{
							cr.setAmount(cr.getStartAmount());
						}
						_procureNext.put(castleId, procure);
					}
				}
				
				// Save changes
				storeMe();
				break;
			}
			case MAINTENANCE:
			{
				// Notify clan leader about manor mode change
				for (var castle : CastleManager.getInstance().getCastles())
				{
					var owner = castle.getOwner();
					if (owner != null)
					{
						var clanLeader = owner.getLeader();
						if ((clanLeader != null) && clanLeader.isOnline())
						{
							clanLeader.getPlayerInstance().sendPacket(SystemMessageId.THE_MANOR_INFORMATION_HAS_BEEN_UPDATED);
						}
					}
				}
				_mode = ManorStatus.MODIFIABLE;
				break;
			}
			case MODIFIABLE:
			{
				_mode = ManorStatus.APPROVED;
				
				for (var castle : CastleManager.getInstance().getCastles())
				{
					var owner = castle.getOwner();
					if (owner == null)
					{
						continue;
					}
					
					var slots = 0;
					var castleId = castle.getResidenceId();
					var cwh = owner.getWarehouse();
					for (var crop : _procureNext.get(castleId))
					{
						if ((crop.getStartAmount() > 0) && (cwh.getItemsByItemId(getSeedByCrop(crop.getId()).getMatureId()) == null))
						{
							slots++;
						}
					}
					
					var manorCost = getManorCost(castleId, true);
					if (!cwh.validateCapacity(slots) && (castle.getTreasury() < manorCost))
					{
						_productionNext.get(castleId).clear();
						_procureNext.get(castleId).clear();
						
						// Notify clan leader
						var clanLeader = owner.getLeader();
						if ((clanLeader != null) && clanLeader.isOnline())
						{
							clanLeader.getPlayerInstance().sendPacket(SystemMessageId.THE_AMOUNT_IS_NOT_SUFFICIENT_AND_SO_THE_MANOR_IS_NOT_IN_OPERATION);
						}
					}
					else
					{
						castle.addToTreasuryNoTax(-manorCost);
					}
				}
				break;
			}
		}
		scheduleModeChange();
		LOG.debug("Manor mode changed to: {}.", _mode.toString());
	}
	
	public final void setNextSeedProduction(List<SeedProduction> list, int castleId)
	{
		_productionNext.put(castleId, list);
	}
	
	public final void setNextCropProcure(List<CropProcure> list, int castleId)
	{
		_procureNext.put(castleId, list);
	}
	
	public final List<SeedProduction> getSeedProduction(int castleId, boolean nextPeriod)
	{
		return (nextPeriod) ? _productionNext.get(castleId) : _production.get(castleId);
	}
	
	public final SeedProduction getSeedProduct(int castleId, int seedId, boolean nextPeriod)
	{
		for (var sp : getSeedProduction(castleId, nextPeriod))
		{
			if (sp.getId() == seedId)
			{
				return sp;
			}
		}
		return null;
	}
	
	public final List<CropProcure> getCropProcure(int castleId, boolean nextPeriod)
	{
		return (nextPeriod) ? _procureNext.get(castleId) : _procure.get(castleId);
	}
	
	public final CropProcure getCropProcure(int castleId, int cropId, boolean nextPeriod)
	{
		for (var cp : getCropProcure(castleId, nextPeriod))
		{
			if (cp.getId() == cropId)
			{
				return cp;
			}
		}
		return null;
	}
	
	public final long getManorCost(int castleId, boolean nextPeriod)
	{
		var procure = getCropProcure(castleId, nextPeriod);
		var production = getSeedProduction(castleId, nextPeriod);
		
		var total = 0;
		for (var seed : production)
		{
			var s = getSeed(seed.getId());
			total += (s == null) ? 1 : (s.getSeedReferencePrice() * seed.getStartAmount());
		}
		for (var crop : procure)
		{
			total += (crop.getPrice() * crop.getStartAmount());
		}
		return total;
	}
	
	@Override
	public final boolean storeMe()
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps1 = con.prepareStatement(DELETE_PRODUCT);
			var ps2 = con.prepareStatement(INSERT_PRODUCT);
			var ps3 = con.prepareStatement(DELETE_CROP_2);
			var ps4 = con.prepareStatement(INSERT_CROP))
		{
			// Delete old seeds
			ps1.executeUpdate();
			
			// Current production
			for (var entry : _production.entrySet())
			{
				for (var sp : entry.getValue())
				{
					ps2.setInt(1, entry.getKey());
					ps2.setInt(2, sp.getId());
					ps2.setLong(3, sp.getAmount());
					ps2.setLong(4, sp.getStartAmount());
					ps2.setLong(5, sp.getPrice());
					ps2.setBoolean(6, false);
					ps2.addBatch();
				}
			}
			
			// Next production
			for (var entry : _productionNext.entrySet())
			{
				for (var sp : entry.getValue())
				{
					ps2.setInt(1, entry.getKey());
					ps2.setInt(2, sp.getId());
					ps2.setLong(3, sp.getAmount());
					ps2.setLong(4, sp.getStartAmount());
					ps2.setLong(5, sp.getPrice());
					ps2.setBoolean(6, true);
					ps2.addBatch();
				}
			}
			
			// Execute production batch
			ps2.executeBatch();
			
			// Delete old procure
			ps3.executeUpdate();
			
			// Current procure
			for (var entry : _procure.entrySet())
			{
				for (var cp : entry.getValue())
				{
					ps4.setInt(1, entry.getKey());
					ps4.setInt(2, cp.getId());
					ps4.setLong(3, cp.getAmount());
					ps4.setLong(4, cp.getStartAmount());
					ps4.setLong(5, cp.getPrice());
					ps4.setInt(6, cp.getReward());
					ps4.setBoolean(7, false);
					ps4.addBatch();
				}
			}
			
			// Next procure
			for (var entry : _procureNext.entrySet())
			{
				for (var cp : entry.getValue())
				{
					ps4.setInt(1, entry.getKey());
					ps4.setInt(2, cp.getId());
					ps4.setLong(3, cp.getAmount());
					ps4.setLong(4, cp.getStartAmount());
					ps4.setLong(5, cp.getPrice());
					ps4.setInt(6, cp.getReward());
					ps4.setBoolean(7, true);
					ps4.addBatch();
				}
			}
			// Execute procure batch
			ps4.executeBatch();
			return true;
		}
		catch (Exception ex)
		{
			LOG.error("Unable to store manor data!", ex);
			return false;
		}
	}
	
	public final void resetManorData(int castleId)
	{
		if (_mode == ManorStatus.DISABLED)
		{
			return;
		}
		
		_procure.get(castleId).clear();
		_procureNext.get(castleId).clear();
		_production.get(castleId).clear();
		_productionNext.get(castleId).clear();
	}
	
	public final boolean isUnderMaintenance()
	{
		return _mode.equals(ManorStatus.MAINTENANCE);
	}
	
	public final boolean isManorApproved()
	{
		return _mode.equals(ManorStatus.APPROVED);
	}
	
	public final boolean isModifiablePeriod()
	{
		return _mode.equals(ManorStatus.MODIFIABLE);
	}
	
	public final String getCurrentModeName()
	{
		return _mode.toString();
	}
	
	public final String getNextModeChange()
	{
		return TimeUtils.dateTimeFormat(_nextModeChange); // TODO : TEst and
		
		// DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM HH:mm:ss");
		// String formattedNextModeChange = _nextModeChange.format(formatter);
		// return formattedNextModeChange;
	}
	
	public final List<L2Seed> getCrops()
	{
		final List<L2Seed> seeds = new ArrayList<>();
		final List<Integer> cropIds = new ArrayList<>();
		for (var seed : _seeds.values())
		{
			if (!cropIds.contains(seed.getCropId()))
			{
				seeds.add(seed);
				cropIds.add(seed.getCropId());
			}
		}
		cropIds.clear();
		return seeds;
	}
	
	public final Set<L2Seed> getSeedsForCastle(int castleId)
	{
		return _seeds.values().stream().filter(s -> s.getCastleId() == castleId).collect(Collectors.toSet());
	}
	
	public final Set<Integer> getSeedIds()
	{
		return _seeds.keySet();
	}
	
	public final Set<Integer> getCropIds()
	{
		return _seeds.values().stream().map(L2Seed::getCropId).collect(Collectors.toSet());
	}
	
	public final L2Seed getSeed(int seedId)
	{
		return _seeds.get(seedId);
	}
	
	public final L2Seed getSeedByCrop(int cropId, int castleId)
	{
		return _seeds.values().stream().filter(s -> (s.getCastleId() == castleId) && (s.getCropId() == cropId)).findFirst().orElse(null);
	}
	
	public final L2Seed getSeedByCrop(int cropId)
	{
		return _seeds.values().stream().filter(s -> s.getCropId() == cropId).findFirst().orElse(null);
	}
	
	public static final CastleManorManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final CastleManorManager INSTANCE = new CastleManorManager();
	}
}