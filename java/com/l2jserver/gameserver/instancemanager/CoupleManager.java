/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.instancemanager;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.configuration.config.events.WeddingConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.idfactory.IdFactory;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.Couple;

/**
 * @author evill33t
 */
public final class CoupleManager
{
	private static final Logger LOG = LoggerFactory.getLogger(CoupleManager.class);
	
	private List<Couple> _couples = new CopyOnWriteArrayList<>();
	
	protected CoupleManager()
	{
		if (!WeddingConfig.ALLOW_WEDDING)
		{
			LOG.info("Loaded Couples(s) disabled!");
			return;
		}
		_couples = DAOFactory.getInstance().getPlayerCoupleDAO().load();
		
		LOG.info("Loaded: {} couples.", _couples.size());
	}
	
	public void reload()
	{
		_couples.clear();
		_couples = DAOFactory.getInstance().getPlayerCoupleDAO().load();
	}
	
	public final Couple getCouple(int coupleId)
	{
		var index = getCoupleIndex(coupleId);
		if (index >= 0)
		{
			return getCouples().get(index);
		}
		return null;
	}
	
	public void createCouple(L2PcInstance player1, L2PcInstance player2)
	{
		if ((player1 != null) && (player2 != null))
		{
			if ((player1.getPartnerId() == 0) && (player2.getPartnerId() == 0))
			{
				var player1id = player1.getObjectId();
				var player2id = player2.getObjectId();
				
				var couple = new Couple(player1, player2);
				getCouples().add(couple);
				player1.setPartnerId(player2id);
				player2.setPartnerId(player1id);
				player1.setCoupleId(couple.getId());
				player2.setCoupleId(couple.getId());
			}
		}
	}
	
	public void deleteCouple(int coupleId)
	{
		var index = getCoupleIndex(coupleId);
		var couple = getCouples().get(index);
		if (couple != null)
		{
			var player1 = L2World.getInstance().getPlayer(couple.getPlayer1Id());
			var player2 = L2World.getInstance().getPlayer(couple.getPlayer2Id());
			if (player1 != null)
			{
				player1.setPartnerId(0);
				player1.setMarried(false);
				player1.setCoupleId(0);
				
			}
			if (player2 != null)
			{
				player2.setPartnerId(0);
				player2.setMarried(false);
				player2.setCoupleId(0);
				
			}
			DAOFactory.getInstance().getPlayerCoupleDAO().divorce(couple.getId());
			getCouples().remove(index);
		}
		// Release the couple id.
		IdFactory.getInstance().releaseId(coupleId);
	}
	
	public final int getCoupleIndex(int coupleId)
	{
		var i = 0;
		for (var temp : getCouples())
		{
			if ((temp != null) && (temp.getId() == coupleId))
			{
				return i;
			}
			i++;
		}
		return -1;
	}
	
	public final List<Couple> getCouples()
	{
		return _couples;
	}
	
	public static final CoupleManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final CoupleManager INSTANCE = new CoupleManager();
	}
}
