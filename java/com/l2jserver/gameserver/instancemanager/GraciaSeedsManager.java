/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.instancemanager;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.GraciaSeedsConfig;
import com.l2jserver.gameserver.instancemanager.tasks.UpdateSoDStateTask;
import com.l2jserver.gameserver.instancemanager.tasks.UpdateSoIStateTask;
import com.l2jserver.gameserver.model.L2Spawn;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;

public final class GraciaSeedsManager
{
	private static final Logger LOG = LoggerFactory.getLogger(GraciaSeedsManager.class);
	
	public static String ENERGY_SEEDS = "EnergySeeds";
	public static String SOD_DEFENCE = "Defence";
	
	// Types for save data
	private final byte SODTYPE = 1;
	private final byte SOITYPE = 2;
	private final byte SOATYPE = 3;
	
	// Seed of Destruction
	private static final int EDRIC = 32527;
	private static final Location EDRIC_SPAWN_LOCATION = new Location(-248525, 250048, 4307, 24576);
	private L2Npc edricSpawn = null;
	private int _SoDTiatKilled = 0;
	private int _SoDState = 1;
	private final Calendar _SoDLastStateChangeDate;
	
	// Seed of Infinity
	private int _SoITwinKilled = 0;
	private int _SoICohemenesKilled = 0;
	private int _SoIEkimusKilled = 0;
	private int _SoIState = 1;
	private final Calendar _SoILastStateChangeDate;
	private static long _timeStepMin = 48 * 60 * 60 * 1000; // 48 hours
	private static long _timeStepMax = 72 * 60 * 60 * 1000; // 72 hours
	
	protected GraciaSeedsManager()
	{
		_SoDLastStateChangeDate = Calendar.getInstance();
		_SoILastStateChangeDate = Calendar.getInstance();
		loadData();
		handleSodStages();
		handleSoIStages();
	}
	
	public void saveData(byte seedType)
	{
		switch (seedType)
		{
			case SODTYPE:
				// Seed of Destruction
				GlobalVariablesManager.getInstance().set("SoDState", _SoDState);
				GlobalVariablesManager.getInstance().set("SoDTiatKilled", _SoDTiatKilled);
				GlobalVariablesManager.getInstance().set("SoDLSCDate", _SoDLastStateChangeDate.getTimeInMillis());
				break;
			case SOITYPE:
				// Seed of Infinity
				GlobalVariablesManager.getInstance().set("SoIState", _SoIState);
				GlobalVariablesManager.getInstance().set("SoITwinKilled", _SoITwinKilled);
				GlobalVariablesManager.getInstance().set("SoICohemenesKilled", _SoICohemenesKilled);
				GlobalVariablesManager.getInstance().set("SoIEkimusKilled", _SoIEkimusKilled);
				GlobalVariablesManager.getInstance().set("SoILSCDate", _SoILastStateChangeDate.getTimeInMillis());
				break;
			case SOATYPE:
				// Seed of Annihilation
				break;
			default:
				LOG.warn("Unknown SeedType in SaveData: {}", seedType);
				break;
		}
	}
	
	public void loadData()
	{
		// Seed of Destruction variables
		if (GlobalVariablesManager.getInstance().hasVariable("SoDState"))
		{
			_SoDState = GlobalVariablesManager.getInstance().getInt("SoDState");
			_SoDTiatKilled = GlobalVariablesManager.getInstance().getInt("SoDTiatKilled", _SoDTiatKilled);
			_SoDLastStateChangeDate.setTimeInMillis(GlobalVariablesManager.getInstance().getLong("SoDLSCDate"));
		}
		else
		{
			// save Initial values
			saveData(SODTYPE);
		}
		
		// Seed of Infinity variables
		if (GlobalVariablesManager.getInstance().hasVariable("SoIState"))
		{
			_SoIState = GlobalVariablesManager.getInstance().getInt("SoIState");
			_SoITwinKilled = GlobalVariablesManager.getInstance().getInt("SoITwinKilled", _SoITwinKilled);
			_SoICohemenesKilled = GlobalVariablesManager.getInstance().getInt("SoICohemenesKilled", _SoICohemenesKilled);
			_SoIEkimusKilled = GlobalVariablesManager.getInstance().getInt("SoIEkimusKilled", _SoIEkimusKilled);
			_SoILastStateChangeDate.setTimeInMillis(GlobalVariablesManager.getInstance().getLong("SoILSCDate"));
		}
		else
		{
			// save Initial values
			saveData(SOITYPE);
		}
	}
	
	private void handleSodStages()
	{
		switch (_SoDState)
		{
			case 1:
				// Despawn Edric(Remnant Manager) and do nothing else, players should kill Tiat a few times
				despawnSoDRemnantManager();
				break;
			case 2:
				// Conquest Complete state, if too much time is passed than change to defense state
				long timePast = System.currentTimeMillis() - _SoDLastStateChangeDate.getTimeInMillis();
				if (timePast >= GraciaSeedsConfig.SOD_STAGE_2_LENGTH)
				{
					// change to Defend state
					setSoDState(5, true, false);
				}
				else
				{
					// Schedule change to Defend state
					ThreadPoolManager.getInstance().scheduleEffect(new UpdateSoDStateTask(), GraciaSeedsConfig.SOD_STAGE_2_LENGTH - timePast);
				}
				// Spawn Edric(Remnant Manager) for solo instances;
				spawnSoDRemnantManager();
				break;
			case 3:
				// Spawn Edric(Remnant Manager) else is handled by datapack
				spawnSoDRemnantManager();
				break;
			case 4:
				// Spawn Edric(Remnant Manager) else is handled by datapack
				spawnSoDRemnantManager();
				break;
			case 5:
				// Spawn Edric(Remnant Manager) else is handled by datapack
				spawnSoDRemnantManager();
				break;
			default:
				LOG.warn("Unknown Seed of Destruction state({})! ", _SoDState);
		}
	}
	
	private void handleSoIStages()
	{
		long timeStep = Rnd.get(_timeStepMin, _timeStepMax);
		switch (_SoIState)
		{
			case 1:
				if ((getSoICohemenesKilled() >= GraciaSeedsConfig.SOI_COHEMENES_KILL_COUNT) && (getSoITwinKilled() >= GraciaSeedsConfig.SOI_TWIN_KILL_COUNT))
				{
					_SoICohemenesKilled = 0;
					_SoITwinKilled = 0;
					setSoIState(2, true);
				}
				break;
			case 2:
				if (getSoIEkimusKilled() >= GraciaSeedsConfig.SOI_EKIMUS_KILL_COUNT)
				{
					_SoIEkimusKilled = 0;
					setSoIState(3, true);
				}
				break;
			case 3:
				_SoITwinKilled = 0;
				_SoILastStateChangeDate.setTimeInMillis(System.currentTimeMillis() + timeStep);
				ThreadPoolManager.getInstance().scheduleEffect(new UpdateSoIStateTask(), timeStep);
				saveData(SOITYPE);
				break;
			case 4:
				if ((getSoICohemenesKilled() >= GraciaSeedsConfig.SOI_COHEMENES_KILL_COUNT) && (getSoITwinKilled() >= GraciaSeedsConfig.SOI_TWIN_KILL_COUNT))
				{
					_SoITwinKilled = 0;
					_SoICohemenesKilled = 0;
					setSoIState(5, true);
				}
				break;
			case 5:
				if (getSoIEkimusKilled() >= GraciaSeedsConfig.SOI_EKIMUS_KILL_COUNT)
				{
					_SoIEkimusKilled = 0;
					setSoIState(1, true);
				}
				break;
			default:
				LOG.warn("Unknown Seed of Infinity state({})! ", _SoIState);
		}
	}
	
	public void updateSoDDefence(int state)
	{
		if ((state >= 3) && (state <= 5))
		{
			var quest = QuestManager.getInstance().getQuest(SOD_DEFENCE);
			if (quest == null)
			{
				LOG.warn("missing Defence Quest!");
			}
			else
			{
				quest.notifyEvent("start", null, null);
			}
		}
		else
		{
			LOG.warn("Invalid Seed of Destruction defence state({}), should be 3, 4 or 5", state);
		}
	}
	
	public void stopSoDInvasion()
	{
		var defQuest = QuestManager.getInstance().getQuest(SOD_DEFENCE);
		if (defQuest == null)
		{
			LOG.warn("missing Defence Quest!");
		}
		else
		{
			defQuest.notifyEvent("stop", null, null);
		}
	}
	
	public void updateSodState()
	{
		var esQuest = QuestManager.getInstance().getQuest(ENERGY_SEEDS);
		if (esQuest == null)
		{
			LOG.warn("missing EnergySeeds Quest!");
		}
		else
		{
			esQuest.notifyEvent("StopSoDAi", null, null);
			stopSoDInvasion();
		}
	}
	
	public void updateSoIState()
	{
		var quest = QuestManager.getInstance().getQuest(ENERGY_SEEDS);
		if (quest == null)
		{
			LOG.warn("missing EnergySeeds Quest!");
		}
		else
		{
			quest.notifyEvent("StopSoIAi", null, null);
		}
	}
	
	public void increaseSoDTiatKilled()
	{
		if (_SoDState == 1)
		{
			_SoDTiatKilled++;
			if (_SoDTiatKilled >= GraciaSeedsConfig.SOD_TIAT_KILL_COUNT)
			{
				setSoDState(2, false, true);
			}
			saveData(SODTYPE);
		}
	}
	
	public void increaseSoIEkimusKilled()
	{
		_SoIEkimusKilled++;
		handleSoIStages();
		{
			saveData(SOITYPE);
			// Start Energy Seeds AI
			var esQuest = QuestManager.getInstance().getQuest(ENERGY_SEEDS);
			if (esQuest == null)
			{
				LOG.warn("missing EnergySeeds Quest!");
			}
			else
			{
				esQuest.notifyEvent("StartSoIAi", null, null);
			}
		}
	}
	
	public void increaseSoITwinKilled()
	{
		_SoITwinKilled++;
		handleSoIStages();
		{
			saveData(SOITYPE);
		}
	}
	
	public void increaseSoICohemenesKilled()
	{
		_SoICohemenesKilled++;
		handleSoIStages();
		{
			saveData(SOITYPE);
		}
	}
	
	public int getSoDTiatKilled()
	{
		return _SoDTiatKilled;
	}
	
	public int getSoITwinKilled()
	{
		return _SoITwinKilled;
	}
	
	public int getSoICohemenesKilled()
	{
		return _SoICohemenesKilled;
	}
	
	public int getSoIEkimusKilled()
	{
		return _SoIEkimusKilled;
	}
	
	public void setSoDOpenState()
	{
		var esQuest = QuestManager.getInstance().getQuest(ENERGY_SEEDS);
		if (esQuest == null)
		{
			LOG.warn("missing EnergySeeds Quest!");
		}
		else
		{
			esQuest.notifyEvent("StartSoDAi", null, null);
			stopSoDInvasion();
		}
	}
	
	public void spawnSoDRemnantManager()
	{
		try
		{
			if ((edricSpawn == null) || edricSpawn.isDecayed())
			{
				var spawn = new L2Spawn(EDRIC);
				spawn.setInstanceId(0);
				spawn.setLocation(EDRIC_SPAWN_LOCATION);
				spawn.stopRespawn();
				var npc = spawn.spawnOne(false);
				edricSpawn = npc;
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not spawn NPC Edric #{}!", EDRIC, ex);
		}
	}
	
	public void despawnSoDRemnantManager()
	{
		if (edricSpawn != null)
		{
			edricSpawn.deleteMe();
		}
	}
	
	public void setSoDState(int value, boolean save, boolean updateDate)
	{
		LOG.info("Loaded New Seed of Destruction state -> {}.", value);
		if (updateDate)
		{
			_SoDLastStateChangeDate.setTimeInMillis(System.currentTimeMillis());
		}
		_SoDState = value;
		if (_SoDState == 1)
		{
			// reset number of Tiat kills
			_SoDTiatKilled = 0;
			updateSodState();
		}
		else if (_SoDState == 2)
		{
			setSoDOpenState();
		}
		else if (_SoDState > 2)
		{
			updateSoDDefence(_SoDState);
		}
		
		handleSodStages();
		
		if (save)
		{
			saveData(SODTYPE);
		}
	}
	
	public void setSoIState(int value, boolean save)
	{
		LOG.info("Loaded New Seed of Infinity state -> {}.", value);
		_SoILastStateChangeDate.setTimeInMillis(System.currentTimeMillis());
		_SoIState = value;
		// If stage 1, clear all values
		if (_SoIState == 1)
		{
			_SoITwinKilled = 0;
			_SoICohemenesKilled = 0;
			_SoIEkimusKilled = 0;
		}
		
		handleSoIStages();
		
		if (save)
		{
			saveData(SOITYPE);
		}
	}
	
	public long getSoDTimeForNextStateChange()
	{
		switch (_SoDState)
		{
			case 1:
				return -1;
			case 2:
				return ((_SoDLastStateChangeDate.getTimeInMillis() + GraciaSeedsConfig.SOD_STAGE_2_LENGTH) - System.currentTimeMillis());
			case 3:
				// not implemented yet
				return -1;
			default:
				// this should not happen!
				return -1;
		}
	}
	
	public long getSoITimeForNextStateChange()
	{
		switch (_SoIState)
		{
			case 1:
				return -1;
			case 2:
				return -1;
			case 3:
				return (_SoILastStateChangeDate.getTimeInMillis() - System.currentTimeMillis());
			case 4:
				return -1;
			case 5:
				return -1;
			default:
				// this should not happen!
				return -1;
		}
	}
	
	public Calendar getSoDLastStateChangeDate()
	{
		return _SoDLastStateChangeDate;
	}
	
	public int getSoDState()
	{
		return _SoDState;
	}
	
	public int getSoIState()
	{
		return _SoIState;
	}
	
	public static final GraciaSeedsManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final GraciaSeedsManager INSTANCE = new GraciaSeedsManager();
	}
}