/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.instancemanager;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.configuration.config.custom.PremiumConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.SkillData;
import com.l2jserver.gameserver.enums.events.EventType;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.events.Containers;
import com.l2jserver.gameserver.model.events.ListenersContainer;
import com.l2jserver.gameserver.model.events.impl.character.player.OnPlayerLogin;
import com.l2jserver.gameserver.model.events.impl.character.player.OnPlayerLogout;
import com.l2jserver.gameserver.model.events.listeners.ConsumerEventListener;
import com.l2jserver.gameserver.network.serverpackets.CreatureSay;

public class PremiumManager
{
	private final Map<String, Long> _premiumData = new HashMap<>();
	private final ListenersContainer _listenerContainer = Containers.Players();
	
	private final Consumer<OnPlayerLogin> _playerLoginEvent = event ->
	{
		var player = event.getActiveChar();
		var accountName = player.getAccountName();
		DAOFactory.getInstance().getPremiumAccDAO().load(accountName);
		var now = System.currentTimeMillis();
		var premiumExpiration = getPremiumExpiration(accountName);
		player.getPcPremiumSystem().setPremium(premiumExpiration > now);
		if (player.getPcPremiumSystem().isActive())
		{
			player.getPcPremiumSystem().startExpireTask(premiumExpiration - now);
			
			if (PremiumConfig.PREMIUM_HUNTING_MONSTER_SKILL_ENABLED)
			{
				player.addSkill(SkillData.getInstance().getSkill(PremiumConfig.PREMIUM_HUNTING_MONSTER_SKILL_ID, PremiumConfig.PREMIUM_HUNTING_MONSTER_SKILL_LEVEL), false);
				player.sendSkillList();
			}
			
			if (PremiumConfig.NOTIFY_PREMIUM_EXPIRATION)
			{
				var text = TimeUtils.dateTimeFormat(Instant.ofEpochMilli(premiumExpiration));
				player.sendPacket(new CreatureSay(0, ChatType.PARTY, MessagesData.getInstance().getMessage(player, "player_message_server"), MessagesData.getInstance().getMessage(player, "your_premium_will_expire").replace("%i%", text + "")));
			}
		}
		else if (premiumExpiration > 0L)
		{
			removePremiumStatus(accountName, false);
		}
	};
	
	private final Consumer<OnPlayerLogout> _playerLogoutEvent = event ->
	{
		var player = event.getActiveChar();
		player.getPcPremiumSystem().stopExpireTask();
	};
	
	protected PremiumManager()
	{
		getListenerContainer().addListener(new ConsumerEventListener(getListenerContainer(), EventType.ON_PLAYER_LOGIN, getPlayerLoginEvent(), this));
		getListenerContainer().addListener(new ConsumerEventListener(getListenerContainer(), EventType.ON_PLAYER_LOGOUT, getPlayerLogoutEvent(), this));
	}
	
	public long getPremiumExpiration(String accountName)
	{
		return getPremiumData().getOrDefault(accountName, 0L);
	}
	
	public void addPremiumTime(String accountName, int timeValue, TimeUnit timeUnit)
	{
		long now;
		long newPremiumExpiration;
		var addTime = timeUnit.toMillis(timeValue);
		now = System.currentTimeMillis();
		var oldPremiumExpiration = Math.max(now, getPremiumExpiration(accountName));
		newPremiumExpiration = oldPremiumExpiration + addTime;
		DAOFactory.getInstance().getPremiumAccDAO().update(newPremiumExpiration, accountName);
		getPremiumData().put(accountName, newPremiumExpiration);
		var player = L2World.getInstance().getPlayers().stream().filter(p -> accountName.equals(p.getAccountName())).findFirst().orElse(null);
		if (player != null)
		{
			player.getPcPremiumSystem().stopExpireTask();
			player.getPcPremiumSystem().startExpireTask(newPremiumExpiration - now);
			if (!player.getPcPremiumSystem().isActive())
			{
				player.getPcPremiumSystem().setPremium(true);
				
				if (PremiumConfig.PREMIUM_HUNTING_MONSTER_SKILL_ENABLED)
				{
					player.addSkill(SkillData.getInstance().getSkill(PremiumConfig.PREMIUM_HUNTING_MONSTER_SKILL_ID, PremiumConfig.PREMIUM_HUNTING_MONSTER_SKILL_LEVEL), false);
					player.sendSkillList();
					player.sendMessage(MessagesData.getInstance().getMessage(player, "premium_account_give_skills_for_mobs"));
				}
				
				var text = TimeUtils.dateTimeFormat(Instant.ofEpochMilli(newPremiumExpiration));
				player.sendPacket(new CreatureSay(0, ChatType.PARTY, MessagesData.getInstance().getMessage(player, "player_message_server"), MessagesData.getInstance().getMessage(player, "Congratulations_premium_account_activity").replace("%i%", text + "")));
			}
		}
	}
	
	public void removePremiumStatus(String accountName, boolean checkOnline)
	{
		if (checkOnline)
		{
			var player = L2World.getInstance().getPlayers().stream().filter(p -> accountName.equals(p.getAccountName())).findFirst().orElse(null);
			if ((player != null) && player.getPcPremiumSystem().isActive())
			{
				player.getPcPremiumSystem().setPremium(false);
				player.getPcPremiumSystem().stopExpireTask();
				
				if (PremiumConfig.PREMIUM_HUNTING_MONSTER_SKILL_ENABLED)
				{
					player.removeSkill(PremiumConfig.PREMIUM_HUNTING_MONSTER_SKILL_ID);
					player.sendSkillList();
					player.sendMessage(MessagesData.getInstance().getMessage(player, "premium_account_remove_skills_for_mobs"));
				}
				
				player.sendPacket(new CreatureSay(0, ChatType.PARTY, MessagesData.getInstance().getMessage(player, "player_message_server"), MessagesData.getInstance().getMessage(player, "your_premium_expire")));
			}
		}
		getPremiumData().remove(accountName);
		DAOFactory.getInstance().getPremiumAccDAO().remove(accountName);
	}
	
	public Map<String, Long> getPremiumData()
	{
		return _premiumData;
	}
	
	public ListenersContainer getListenerContainer()
	{
		return _listenerContainer;
	}
	
	public Consumer<OnPlayerLogin> getPlayerLoginEvent()
	{
		return _playerLoginEvent;
	}
	
	public Consumer<OnPlayerLogout> getPlayerLogoutEvent()
	{
		return _playerLogoutEvent;
	}
	
	public static final PremiumManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final PremiumManager INSTANCE = new PremiumManager();
	}
}
