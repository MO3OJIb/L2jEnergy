/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.instancemanager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * @author Kerberos, JIV
 */
public class RaidBossPointsManager
{
	private static final Logger LOG = LoggerFactory.getLogger(RaidBossPointsManager.class);
	
	private static final String SELECT = "SELECT charId, boss_id, points FROM character_raid_points";
	private static final String REPLACE = "REPLACE INTO character_raid_points (charId, boss_id, points) VALUES (?, ?, ?)";
	private static final String DELETE = "DELETE from character_raid_points WHERE charId > 0";
	
	private final Map<Integer, Map<Integer, Integer>> _list = new ConcurrentHashMap<>();
	
	public RaidBossPointsManager()
	{
		init();
	}
	
	private final void init()
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT);
			var rset = ps.executeQuery())
		{
			while (rset.next())
			{
				var charId = rset.getInt("charId");
				var bossId = rset.getInt("boss_id");
				var points = rset.getInt("points");
				var values = _list.get(charId);
				if (values == null)
				{
					values = new HashMap<>();
				}
				values.put(bossId, points);
				_list.put(charId, values);
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not load raid points {}!", ex);
		}
		LOG.info("Loaded {} Character Raid Points.", _list.size());
	}
	
	public final void updatePointsInDB(L2PcInstance player, int raidId, int points)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(REPLACE))
		{
			ps.setInt(1, player.getObjectId());
			ps.setInt(2, raidId);
			ps.setInt(3, points);
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not update char raid points for player: {}!", player, ex);
		}
	}
	
	public final void addPoints(L2PcInstance player, int bossId, int points)
	{
		var tmpPoint = _list.computeIfAbsent(player.getObjectId(), k -> new HashMap<>());
		updatePointsInDB(player, bossId, tmpPoint.merge(bossId, points, Integer::sum));
	}
	
	public final int getPointsByOwnerId(int ownerId)
	{
		var tmpPoint = _list.get(ownerId);
		var totalPoints = 0;
		
		if ((tmpPoint == null) || tmpPoint.isEmpty())
		{
			return 0;
		}
		
		for (var points : tmpPoint.values())
		{
			totalPoints += points;
		}
		return totalPoints;
	}
	
	public final Map<Integer, Integer> getList(L2PcInstance player)
	{
		return _list.get(player.getObjectId());
	}
	
	public final void cleanUp()
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE))
		{
			ps.executeUpdate();
			_list.clear();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not clean raid points.", ex);
		}
	}
	
	public final int calculateRanking(int playerObjId)
	{
		var rank = getRankList();
		if (rank.containsKey(playerObjId))
		{
			return rank.get(playerObjId);
		}
		return 0;
	}
	
	public Map<Integer, Integer> getRankList()
	{
		final Map<Integer, Integer> tmpPoints = new HashMap<>();
		for (var ownerId : _list.keySet())
		{
			var totalPoints = getPointsByOwnerId(ownerId);
			if (totalPoints != 0)
			{
				tmpPoints.put(ownerId, totalPoints);
			}
		}
		
		final List<Entry<Integer, Integer>> list = new ArrayList<>(tmpPoints.entrySet());
		list.sort(Comparator.comparing(Entry<Integer, Integer>::getValue).reversed());
		var ranking = 1;
		final Map<Integer, Integer> tmpRanking = new HashMap<>();
		for (var entry : list)
		{
			tmpRanking.put(entry.getKey(), ranking++);
		}
		return tmpRanking;
	}
	
	public static final RaidBossPointsManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final RaidBossPointsManager INSTANCE = new RaidBossPointsManager();
	}
}