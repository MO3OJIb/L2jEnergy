/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.instancemanager;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.model.L2Spawn;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.Castle;

public final class SiegeGuardManager
{
	private static Logger LOG = LoggerFactory.getLogger(SiegeGuardManager.class);
	
	private final Castle _castle;
	private final List<L2Spawn> _siegeGuardSpawn = new ArrayList<>();
	
	public SiegeGuardManager(Castle castle)
	{
		_castle = castle;
	}
	
	public void addSiegeGuard(L2PcInstance activeChar, int npcId)
	{
		if (activeChar == null)
		{
			return;
		}
		addSiegeGuard(activeChar.getX(), activeChar.getY(), activeChar.getZ(), activeChar.getHeading(), npcId);
	}
	
	public void addSiegeGuard(int x, int y, int z, int heading, int npcId)
	{
		DAOFactory.getInstance().getSiegeGuardDAO().saveSiegeGuard(_castle, x, y, z, heading, npcId, 0);
	}
	
	public void hireMerc(L2PcInstance activeChar, int npcId)
	{
		if (activeChar == null)
		{
			return;
		}
		hireMerc(activeChar.getX(), activeChar.getY(), activeChar.getZ(), activeChar.getHeading(), npcId);
	}
	
	public void hireMerc(int x, int y, int z, int heading, int npcId)
	{
		DAOFactory.getInstance().getSiegeGuardDAO().saveSiegeGuard(_castle, x, y, z, heading, npcId, 1);
	}
	
	public void spawnSiegeGuard()
	{
		try
		{
			int hiredCount = 0, hiredMax = MercTicketManager.getInstance().getMaxAllowedMerc(_castle.getResidenceId());
			var isHired = (getCastle().getOwnerId() > 0) ? true : false;
			loadSiegeGuard();
			for (var spawn : _siegeGuardSpawn)
			{
				if (spawn != null)
				{
					spawn.init();
					if (isHired)
					{
						spawn.stopRespawn();
						if (++hiredCount > hiredMax)
						{
							return;
						}
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.error("Error spawning siege guards for castle {}!", getCastle().getName(), ex);
		}
	}
	
	public void unspawnSiegeGuard()
	{
		for (var spawn : _siegeGuardSpawn)
		{
			if ((spawn != null) && (spawn.getLastSpawn() != null))
			{
				spawn.stopRespawn();
				spawn.getLastSpawn().doDie(spawn.getLastSpawn());
			}
		}
		_siegeGuardSpawn.clear();
	}
	
	private void loadSiegeGuard()
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("SELECT * FROM castle_siege_guards WHERE castleId = ? and isHired = ?"))
		{
			ps.setInt(1, getCastle().getResidenceId());
			if (getCastle().getOwnerId() > 0)
			{
				ps.setInt(2, 1);
			}
			else
			{
				ps.setInt(2, 0);
			}
			
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					var spawn = new L2Spawn(rs.getInt("npcId"));
					spawn.setAmount(1);
					spawn.setX(rs.getInt("x"));
					spawn.setY(rs.getInt("y"));
					spawn.setZ(rs.getInt("z"));
					spawn.setHeading(rs.getInt("heading"));
					spawn.setRespawnDelay(rs.getInt("respawnDelay"));
					spawn.setLocationId(0);
					
					_siegeGuardSpawn.add(spawn);
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Error loading siege guard for castle {}!", getCastle().getName(), ex);
		}
	}
	
	public final Castle getCastle()
	{
		return _castle;
	}
	
	public final List<L2Spawn> getSiegeGuardSpawn()
	{
		return _siegeGuardSpawn;
	}
}
