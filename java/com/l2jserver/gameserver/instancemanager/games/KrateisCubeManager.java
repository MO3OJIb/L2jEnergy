/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.instancemanager.games;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.events.KrateisCubeConfig;
import com.l2jserver.gameserver.data.xml.impl.SpawnData;
import com.l2jserver.gameserver.enums.events.PvPMatchRecordAction;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.model.L2Spawn;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.KrateisCubeEngine;
import com.l2jserver.gameserver.network.serverpackets.CreatureSay;
import com.l2jserver.gameserver.network.serverpackets.ExPVPMatchCCRecord;

/**
 * Krateis Cube Manager.
 * @author U3Games
 * @author Sacrifice
 */
// TODO: need rework
public final class KrateisCubeManager
{
	private static final Logger LOG = LoggerFactory.getLogger(KrateisCubeManager.class);
	
	private static final int MATCH_MANAGER = 32503;
	
	private static final Map<L2PcInstance, PlayerData> PLAYER_DATA = new ConcurrentHashMap<>();
	
	private L2Npc _npc = null;
	
	private L2Spawn _npcSpawn = null;
	
	private boolean _registerActive = false;
	
	private ScheduledFuture<?> _scheduledStartKCTask = null;
	
	public KrateisCubeManager()
	{
		boolean spawnManager = false;
		try
		{
			_npcSpawn = new L2Spawn(MATCH_MANAGER);
			_npcSpawn.setLocation(new Location(-70596, -71064, -1416, 0));
			_npcSpawn.setAmount(1);
			_npcSpawn.setRespawnDelay(1);
			SpawnData.getInstance().addNewSpawn(_npcSpawn, false);
			_npcSpawn.init();
			_npc = _npcSpawn.getLastSpawn();
			_npc.setCurrentHp(_npc.getMaxHp());
			_npc.isAggressive();
			_npc.decayMe();
			_npc.spawnMe(_npcSpawn.getLastSpawn().getX(), _npcSpawn.getLastSpawn().getY(), _npcSpawn.getLastSpawn().getZ());
			spawnManager = true;
		}
		catch (Exception ex)
		{
			spawnManager = false;
			LOG.info("Krateis Cube Manager: Error trying to spawn npc!", ex);
		}
		
		if (spawnManager)
		{
			startEvent();
		}
		else
		{
			LOG.info("Krateis Cube Manager: Event can't be started because npc is disabled!");
		}
	}
	
	private enum MsgType
	{
		INITIALIZED,
		REGISTRATION,
		REGISTRATION_OVER,
		STARTED,
		ABORTED
	}
	
	/**
	 * Add kills in event
	 * @param player
	 * @return
	 */
	public int addPoints(L2PcInstance player)
	{
		var playerData = PLAYER_DATA.get(player);
		if (playerData != null)
		{
			return playerData._kills++;
		}
		return 0;
	}
	
	/**
	 * isInside
	 * @param player
	 * @return
	 */
	public static boolean checkIsInsided(L2PcInstance player)
	{
		var playerData = PLAYER_DATA.get(player);
		if (playerData == null)
		{
			return false;
		}
		
		if (!playerData._isInside)
		{
			return false;
		}
		return true;
	}
	
	/**
	 * Check if is valid for register in arena
	 * @param arena
	 * @return
	 */
	public static boolean checkMaxPlayersArena(int arena)
	{
		var registerCount = 0;
		for (var playerData : PLAYER_DATA.values())
		{
			if (playerData == null)
			{
				return false;
			}
			
			if (playerData._isInside)
			{
				if (playerData._arena == arena)
				{
					registerCount++;
				}
			}
		}
		
		if (registerCount >= KrateisCubeConfig.KRATEIS_CUBE_MAX_PLAYER)
		{
			return false;
		}
		return true;
	}
	
	/**
	 * Check if is valid for create instance
	 * @param arena
	 * @param createInstance
	 * @return
	 */
	public boolean checkMinPlayersArena(int arena, boolean createInstance)
	{
		var count = 0;
		for (var playerData : PLAYER_DATA.values())
		{
			if (createInstance)
			{
				if ((playerData._isInside) && (playerData._arena == arena))
				{
					count++;
				}
			}
			else
			{
				if ((playerData._isRegister) && (playerData._arena == arena))
				{
					count++;
				}
			}
		}
		
		if (count >= KrateisCubeConfig.KRATEIS_CUBE_MIN_PLAYER)
		{
			return true;
		}
		return false;
	}
	
	public boolean checkIsRegistered(L2PcInstance player)
	{
		var playerData = PLAYER_DATA.get(player);
		if (playerData == null)
		{
			return false;
		}
		
		if (!playerData._isRegister)
		{
			return false;
		}
		return true;
	}
	
	private void checkRegistered()
	{
		if (_registerActive)
		{
			_registerActive = false;
		}
		
		getNpcManagerMessage(MsgType.REGISTRATION_OVER, 0);
		
		var createInstance = false;
		for (int instanceId = 1; instanceId < 4; instanceId++)
		{
			if (checkMinPlayersArena(instanceId, false))
			{
				createInstance = true;
				for (var playerData : PLAYER_DATA.values())
				{
					if ((playerData._isRegister) && (playerData._arena == instanceId))
					{
						playerData._isInside = true;
						playerData._isRegister = false;
						teleportToInstance(null);
					}
				}
			}
		}
		
		if (createInstance)
		{
			KrateisCubeEngine.getInstance().createInstances();
			getNpcManagerMessage(MsgType.STARTED, 0);
		}
		else
		{
			getNpcManagerMessage(MsgType.ABORTED, 0);
			startEvent();
		}
	}
	
	/**
	 * Delete players of event
	 */
	public void deleteInsideParticipants()
	{
		for (var playerData : PLAYER_DATA.values())
		{
			if (playerData != null)
			{
				if (playerData._isInside)
				{
					playerData.cleanValues();
				}
			}
		}
	}
	
	/**
	 * Get arena in event
	 * @param player
	 * @return
	 */
	public int getArena(L2PcInstance player)
	{
		var playerData = PLAYER_DATA.get(player);
		if (playerData != null)
		{
			return playerData._arena;
		}
		return 0;
	}
	
	private void getNpcManagerMessage(MsgType state, int time)
	{
		for (var player : _npc.getKnownList().getKnownPlayersInRadius(1500))
		{
			if (player != null)
			{
				switch (state)
				{
					case INITIALIZED:
						player.sendPacket(new CreatureSay(_npc.getObjectId(), ChatType.GENERAL, _npc.getName(), "Register of Krateis Cube initialized!"));
						player.sendPacket(new CreatureSay(_npc.getObjectId(), ChatType.GENERAL, _npc.getName(), time + " minute(s) until the match starts."));
						break;
					case REGISTRATION:
						player.sendPacket(new CreatureSay(_npc.getObjectId(), ChatType.GENERAL, _npc.getName(), time + " minute(s) until the match starts."));
						break;
					case REGISTRATION_OVER:
						player.sendPacket(new CreatureSay(_npc.getObjectId(), ChatType.GENERAL, _npc.getName(), "The registration period for this match is over"));
						break;
					case STARTED:
						player.sendPacket(new CreatureSay(_npc.getObjectId(), ChatType.GENERAL, _npc.getName(), "Krateis Cube match has started!"));
						break;
					case ABORTED:
						player.sendPacket(new CreatureSay(_npc.getObjectId(), ChatType.GENERAL, _npc.getName(), "Krateis Cube cancelled due to lack of participation."));
						break;
				}
			}
		}
		return;
	}
	
	public static List<L2PcInstance> getParticipants()
	{
		final ArrayList<L2PcInstance> participants = new ArrayList<>();
		for (var player : PLAYER_DATA.keySet())
		{
			if (player != null)
			{
				if (checkIsInsided(player))
				{
					if (!participants.contains(player))
					{
						participants.add(player);
					}
				}
			}
		}
		
		if (participants.isEmpty())
		{
			return null;
		}
		return participants;
	}
	
	/**
	 * Get map values of participants.
	 * @return
	 */
	public static Map<String, Integer> getParticipantsMatch()
	{
		final Map<String, Integer> participants = new HashMap<>();
		for (var player : getParticipants())
		{
			if (player != null)
			{
				if (!participants.containsKey(player.getName()))
				{
					participants.put(player.getName(), getPoints(player));
				}
			}
		}
		
		if (participants.isEmpty())
		{
			return null;
		}
		var sortedMap = participants.entrySet().stream().sorted(Entry.comparingByValue(Comparator.reverseOrder())).collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
		return sortedMap;
	}
	
	public static int getPoints(L2PcInstance player)
	{
		var kills = 0;
		var playerData = PLAYER_DATA.get(player);
		if (playerData != null)
		{
			kills = playerData._kills;
		}
		return kills;
	}
	
	public boolean isTimeToRegister()
	{
		return _registerActive;
	}
	
	public void registerPlayer(L2PcInstance player, int arena)
	{
		var playerData = PLAYER_DATA.get(player);
		playerData = PLAYER_DATA.computeIfAbsent(player, data -> new PlayerData());
		PLAYER_DATA.put(player, playerData);
		playerData._isRegister = true;
		playerData._isInside = false;
		playerData._arena = arena;
		playerData._kills = 0;
	}
	
	public void startEvent()
	{
		if (!_registerActive)
		{
			_registerActive = true;
		}
		
		if (_scheduledStartKCTask != null)
		{
			_scheduledStartKCTask.cancel(false);
		}
		
		var cal = Calendar.getInstance();
		if (cal.get(Calendar.MINUTE) >= 57)
		{
			cal.add(Calendar.HOUR, 1);
			cal.set(Calendar.MINUTE, 27);
		}
		else if ((cal.get(Calendar.MINUTE) >= 0) && (cal.get(Calendar.MINUTE) <= 26))
		{
			cal.set(Calendar.MINUTE, 27);
		}
		else
		{
			cal.set(Calendar.MINUTE, 57);
			cal.set(Calendar.SECOND, 0);
		}
		
		ThreadPoolManager.getInstance().scheduleEvent(() -> startRegisterTime(), cal.getTimeInMillis() - System.currentTimeMillis());
		getNpcManagerMessage(MsgType.INITIALIZED, (int) (((cal.getTimeInMillis() - System.currentTimeMillis()) / (1000 * 60))));
		LOG.info("Loaded next match of Krateis Cube: {}", new Date(cal.getTimeInMillis()).toString());
	}
	
	private void startRegisterTime()
	{
		if (_scheduledStartKCTask != null)
		{
			_scheduledStartKCTask.cancel(false);
		}
		getNpcManagerMessage(MsgType.REGISTRATION, KrateisCubeConfig.KRATEIS_CUBE_REGISTRATION_TIME);
		_scheduledStartKCTask = ThreadPoolManager.getInstance().scheduleEvent(() -> checkRegistered(), KrateisCubeConfig.KRATEIS_CUBE_REGISTRATION_TIME * 60 * 1000);
	}
	
	public void teleportOutEvent(L2PcInstance character)
	{
		if (character != null)
		{
			character.setInstanceId(0);
			character.teleToLocation(-70381, -70937, -1428, 0, 0);
		}
		else
		{
			for (var player : PLAYER_DATA.keySet())
			{
				if (checkIsInsided(player))
				{
					player.setInstanceId(0);
					player.teleToLocation(-70381, -70937, -1428, 0, 0);
					player.sendPacket(new ExPVPMatchCCRecord(PvPMatchRecordAction.SHOW_WITHOUT_BUTTON));
				}
			}
		}
	}
	
	public void teleportToInstance(L2PcInstance player)
	{
		if (player != null)
		{
			KrateisCubeEngine.getInstance().teleportToInstance(player, getArena(player));
		}
		else
		{
			for (var participant : PLAYER_DATA.keySet())
			{
				if (checkIsInsided(participant))
				{
					KrateisCubeEngine.getInstance().teleportToWaitingRoom(participant, getArena(participant));
				}
			}
		}
	}
	
	public void teleportToWaitingRoomInstance(L2PcInstance player)
	{
		KrateisCubeEngine.getInstance().countDownReturnMessage(player);
		ThreadPoolManager.getInstance().scheduleEvent(() -> KrateisCubeEngine.getInstance().teleportToWaitingRoom(player, getArena(player)), KrateisCubeConfig.KRATEIS_CUBE_TIME_TO_REVIVE * 1000);
	}
	
	public void unregisterPlayer(L2PcInstance player)
	{
		PLAYER_DATA.remove(player);
	}
	
	private static class PlayerData
	{
		protected int _arena;
		protected boolean _isInside;
		protected boolean _isRegister;
		protected int _kills;
		
		protected PlayerData()
		{
			// empty
		}
		
		protected void cleanValues()
		{
			_arena = 0;
			_isInside = false;
			_isRegister = false;
			_kills = 0;
		}
	}
	
	public static KrateisCubeManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final KrateisCubeManager INSTANCE = new KrateisCubeManager();
	}
}
