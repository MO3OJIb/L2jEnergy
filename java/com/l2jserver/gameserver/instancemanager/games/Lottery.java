/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.instancemanager.games;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.events.LotteryConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.Broadcast;

public class Lottery
{
	private static final long MILLISECONDS_IN_MINUTE = 60 * 1000;
	
	protected static final Logger LOG = LoggerFactory.getLogger(Lottery.class);
	
	private static final String SELECT_LAST_LOTTERY = "SELECT idnr, prize, newprize, enddate, finished FROM games WHERE id = 1 ORDER BY idnr DESC LIMIT 1";
	
	protected int _number;
	protected long _prize;
	protected boolean _isSellingTickets;
	protected boolean _isStarted;
	protected Instant _enddate;
	
	protected Lottery()
	{
		_number = 1;
		_prize = LotteryConfig.ALT_LOTTERY_PRIZE;
		_isSellingTickets = false;
		_isStarted = false;
		_enddate = Instant.now();
		
		if (LotteryConfig.ALLOW_LOTTERY)
		{
			(new startLottery()).run();
		}
	}
	
	public int getId()
	{
		return _number;
	}
	
	public long getPrize()
	{
		return _prize;
	}
	
	public Instant getEndDate()
	{
		return _enddate;
	}
	
	public void increasePrize(long count)
	{
		_prize += count;
		DAOFactory.getInstance().getLotteryDAO().updatePrize(getId(), getPrize());
	}
	
	public boolean isSellableTickets()
	{
		return _isSellingTickets;
	}
	
	public boolean isStarted()
	{
		return _isStarted;
	}
	
	private class startLottery implements Runnable
	{
		protected startLottery()
		{
			// Do nothing
		}
		
		@Override
		public void run()
		{
			
			try (var con = ConnectionFactory.getInstance().getConnection();
				var st = con.createStatement();
				var rs = st.executeQuery(SELECT_LAST_LOTTERY))
			{
				if (rs.next())
				{
					_number = rs.getInt("idnr");
					
					if (rs.getInt("finished") == 1)
					{
						_number++;
						_prize = rs.getLong("newprize");
					}
					else
					{
						_prize = rs.getLong("prize");
						_enddate = Instant.ofEpochSecond(rs.getLong("enddate"));
						
						var now = Instant.now();
						
						if (!_enddate.isAfter(now.plusMillis(2 * MILLISECONDS_IN_MINUTE)))
						{
							(new finishLottery()).run();
							return;
						}
						
						if (_enddate.isAfter(now))
						{
							_isStarted = true;
							Duration duration = Duration.between(now, _enddate);
							ThreadPoolManager.getInstance().scheduleGeneral(new finishLottery(), duration.toMillis());
							
							if (_enddate.isAfter(now.plusMillis(12 * MILLISECONDS_IN_MINUTE)))
							{
								_isSellingTickets = true;
								duration = Duration.between(now.plusMillis(10 * MILLISECONDS_IN_MINUTE), _enddate);
								ThreadPoolManager.getInstance().scheduleGeneral(new stopSellingTickets(), duration.toMillis());
							}
							return;
						}
					}
				}
			}
			catch (Exception ex)
			{
				LOG.warn("Could not restore lottery data!", ex);
			}
			
			_isSellingTickets = true;
			_isStarted = true;
			
			Broadcast.announceToOnlinePlayers("lottery_tickets_now_available_for_lucky_lottery", getId() + "", false);
			
			var finishTime = ZonedDateTime.ofInstant(_enddate, ZoneId.systemDefault());
			finishTime = finishTime.withMinute(0).withSecond(0);
			
			if (finishTime.getDayOfWeek() == DayOfWeek.SUNDAY)
			{
				finishTime = finishTime.withHour(19);
				_enddate = finishTime.plusWeeks(1).toInstant();
			}
			else
			{
				finishTime = finishTime.with(ChronoField.DAY_OF_WEEK, DayOfWeek.SUNDAY.getValue()).withHour(19);
				_enddate = finishTime.toInstant();
			}
			
			var now = Instant.now();
			var duration = Duration.between(now.plusMillis(10 * MILLISECONDS_IN_MINUTE), _enddate);
			ThreadPoolManager.getInstance().scheduleGeneral(new stopSellingTickets(), duration.toMillis());
			duration = Duration.between(now, _enddate);
			ThreadPoolManager.getInstance().scheduleGeneral(new finishLottery(), duration.toMillis());
			
			DAOFactory.getInstance().getLotteryDAO().add(getId(), _enddate.getEpochSecond(), getPrize());
		}
	}
	
	private class stopSellingTickets implements Runnable
	{
		protected stopSellingTickets()
		{
			// Do nothing
		}
		
		@Override
		public void run()
		{
			_isSellingTickets = false;
			
			Broadcast.toAllOnlinePlayers(SystemMessage.getSystemMessage(SystemMessageId.LOTTERY_TICKET_SALES_HAVE_BEEN_TEMPORARILY_SUSPENDED));
		}
	}
	
	private class finishLottery implements Runnable
	{
		protected finishLottery()
		{
			// Do nothing
		}
		
		@Override
		public void run()
		{
			var luckynums = new int[5];
			var luckynum = 0;
			
			for (var i = 0; i < 5; i++)
			{
				var found = true;
				
				while (found)
				{
					luckynum = Rnd.get(20) + 1;
					found = false;
					
					for (var j = 0; j < i; j++)
					{
						if (luckynums[j] == luckynum)
						{
							found = true;
						}
					}
				}
				
				luckynums[i] = luckynum;
			}
			
			var enchant = 0;
			var type2 = 0;
			
			for (var i = 0; i < 5; i++)
			{
				if (luckynums[i] < 17)
				{
					enchant += Math.pow(2, luckynums[i] - 1);
				}
				else
				{
					type2 += Math.pow(2, luckynums[i] - 17);
				}
			}
			
			var count1 = 0;
			var count2 = 0;
			var count3 = 0;
			var count4 = 0;
			
			DAOFactory.getInstance().getLotteryDAO().select(getId(), count1, count2, count3, count4, enchant, type2);
			
			long prize4 = count4 * LotteryConfig.ALT_LOTTERY_2_AND_1_NUMBER_PRIZE;
			long prize1 = 0;
			long prize2 = 0;
			long prize3 = 0;
			
			if (count1 > 0)
			{
				prize1 = (long) (((getPrize() - prize4) * LotteryConfig.ALT_LOTTERY_5_NUMBER_RATE) / count1);
			}
			
			if (count2 > 0)
			{
				prize2 = (long) (((getPrize() - prize4) * LotteryConfig.ALT_LOTTERY_4_NUMBER_RATE) / count2);
			}
			
			if (count3 > 0)
			{
				prize3 = (long) (((getPrize() - prize4) * LotteryConfig.ALT_LOTTERY_3_NUMBER_RATE) / count3);
			}
			
			var newprize = getPrize() - (prize1 + prize2 + prize3 + prize4);
			
			SystemMessage sm;
			if (count1 > 0)
			{
				// There are winners.
				sm = SystemMessage.getSystemMessage(SystemMessageId.THE_PRIZE_AMOUNT_FOR_THE_WINNER_OF_LOTTERY__S1__IS_S2_ADENA_WE_HAVE_S3_FIRST_PRIZE_WINNERS);
				sm.addInt(getId());
				sm.addLong(getPrize());
				sm.addLong(count1);
				Broadcast.toAllOnlinePlayers(sm);
			}
			else
			{
				// There are no winners.
				sm = SystemMessage.getSystemMessage(SystemMessageId.THE_PRIZE_AMOUNT_FOR_LUCKY_LOTTERY__S1__IS_S2_ADENA_THERE_WAS_NO_FIRST_PRIZE_WINNER_IN_THIS_DRAWING_THEREFORE_THE_JACKPOT_WILL_BE_ADDED_TO_THE_NEXT_DRAWING);
				sm.addInt(getId());
				sm.addLong(getPrize());
				Broadcast.toAllOnlinePlayers(sm);
			}
			
			DAOFactory.getInstance().getLotteryDAO().update(getId(), getPrize(), newprize, prize1, prize2, prize3, enchant, type2);
			
			ThreadPoolManager.getInstance().scheduleGeneral(new startLottery(), MILLISECONDS_IN_MINUTE);
			_number++;
			
			_isStarted = false;
		}
	}
	
	public int[] decodeNumbers(int enchant, int type2)
	{
		int res[] = new int[5];
		var id = 0;
		var nr = 1;
		
		while (enchant > 0)
		{
			var val = enchant / 2;
			if (val != Math.round((double) enchant / 2))
			{
				res[id++] = nr;
			}
			enchant /= 2;
			nr++;
		}
		
		nr = 17;
		
		while (type2 > 0)
		{
			var val = type2 / 2;
			if (val != ((double) type2 / 2))
			{
				res[id++] = nr;
			}
			type2 /= 2;
			nr++;
		}
		
		return res;
	}
	
	public long[] checkTicket(L2ItemInstance item)
	{
		return DAOFactory.getInstance().getLotteryDAO().checkTicket(item.getCustomType1(), item.getEnchantLevel(), item.getCustomType2());
	}
	
	public static Lottery getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final Lottery INSTANCE = new Lottery();
	}
}
