/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.instancemanager.games;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.events.UCConfig;
import com.l2jserver.gameserver.data.IXmlReader;
import com.l2jserver.gameserver.instancemanager.tasks.UCRegistrationTask;
import com.l2jserver.gameserver.model.coliseum.UCArena;
import com.l2jserver.gameserver.model.coliseum.UCPoint;
import com.l2jserver.gameserver.model.coliseum.UCTeam;

public class UndergroundColiseum implements IXmlReader
{
	private static final Logger LOG = LoggerFactory.getLogger(UndergroundColiseum.class);
	
	private final Map<Integer, UCArena> _arenas = new HashMap<>();
	private boolean _isStarted;
	private final List<Integer> _hourList = new ArrayList<>(UCConfig.UC_HOUR_STOP - UCConfig.UC_HOUR_START);
	
	public UndergroundColiseum()
	{
		for (var i = UCConfig.UC_HOUR_START; i < UCConfig.UC_HOUR_STOP; i++)
		{
			_hourList.add(i);
		}
		
		for (var day : UCConfig.UC_BATTLE_DAY)
		{
			runStartTask(day);
		}
		
		load();
	}
	
	@Override
	public void load()
	{
		_arenas.clear();
		parseDatapackFile("data/xml/underground_coliseum.xml");
		LOG.info("Loaded {} Underground Coliseum Arenas.", _arenas.size());
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		for (var n = doc.getFirstChild(); n != null; n = n.getNextSibling())
		{
			if ("list".equalsIgnoreCase(n.getNodeName()))
			{
				for (var d = n.getFirstChild(); d != null; d = d.getNextSibling())
				{
					if ("arena".equalsIgnoreCase(d.getNodeName()))
					{
						var map = d.getAttributes();
						var id = Integer.parseInt(map.getNamedItem("id").getNodeValue());
						var min_level = Integer.parseInt(map.getNamedItem("min_level").getNodeValue());
						var max_level = Integer.parseInt(map.getNamedItem("max_level").getNodeValue());
						var curator = Integer.parseInt(map.getNamedItem("curator").getNodeValue());
						
						var arena = new UCArena(id, curator, min_level, max_level);
						var index = 0;
						var index2 = 0;
						
						for (var und = d.getFirstChild(); und != null; und = und.getNextSibling())
						{
							if ("tower".equalsIgnoreCase(und.getNodeName()))
							{
								map = und.getAttributes();
								
								var npc_id = Integer.parseInt(map.getNamedItem("npc_id").getNodeValue());
								var x = Integer.parseInt(map.getNamedItem("x").getNodeValue());
								var y = Integer.parseInt(map.getNamedItem("y").getNodeValue());
								var z = Integer.parseInt(map.getNamedItem("z").getNodeValue());
								var team = new UCTeam(index, arena, x, y, z, npc_id);
								arena.setUCTeam(index, team);
							}
							else if ("point".equalsIgnoreCase(und.getNodeName()))
							{
								map = und.getAttributes();
								
								var door1 = Integer.parseInt(map.getNamedItem("door1").getNodeValue());
								var door2 = Integer.parseInt(map.getNamedItem("door2").getNodeValue());
								
								var x = Integer.parseInt(map.getNamedItem("x").getNodeValue());
								var y = Integer.parseInt(map.getNamedItem("y").getNodeValue());
								var z = Integer.parseInt(map.getNamedItem("z").getNodeValue());
								
								var point = new UCPoint(door1, door2, x, y, z);
								arena.setUCPoint(index2, point);
								
								index2++;
							}
						}
						
						_arenas.put(arena.getId(), arena);
					}
				}
			}
		}
	}
	
	public UCArena getArena(int id)
	{
		return _arenas.get(id);
	}
	
	public void setStarted(boolean started)
	{
		_isStarted = started;
		
		for (var arena : getAllArenas())
		{
			arena.switchStatus(started);
		}
	}
	
	public boolean isStarted()
	{
		return _isStarted;
	}
	
	public UCArena[] getAllArenas()
	{
		return _arenas.values().toArray(new UCArena[0]);
	}
	
	public void runStartTask(int calendarDayOfWeek)
	{
		var now = LocalDateTime.now();
		
		if (now.getDayOfWeek().getValue() == calendarDayOfWeek)
		{
			int hour = now.getHour();
			
			if (_hourList.contains(hour))
			{
				ThreadPoolManager.getInstance().scheduleGeneral(new UCRegistrationTask(true, calendarDayOfWeek), 0);
				return;
			}
		}
		
		var startDate = now.with(TemporalAdjusters.nextOrSame(DayOfWeek.of(calendarDayOfWeek))).withHour(UCConfig.UC_HOUR_START).withMinute(0).withSecond(0);
		
		while (startDate.isBefore(now))
		{
			startDate = startDate.plusWeeks(1);
		}
		
		var delay = Duration.between(now, startDate);
		ThreadPoolManager.getInstance().scheduleGeneral(new UCRegistrationTask(true, calendarDayOfWeek), delay.toMillis());
	}
	
	public void runEndTask(int calendarDayOfWeek)
	{
		var now = LocalDateTime.now();
		
		if (now.getDayOfWeek().getValue() == calendarDayOfWeek)
		{
			int hour = now.getHour();
			
			if (_hourList.contains(hour))
			{
				var endTaskTime = now.withHour(UCConfig.UC_HOUR_STOP).withMinute(0).withSecond(0);
				var delay = Duration.between(now, endTaskTime);
				
				ThreadPoolManager.getInstance().scheduleGeneral(new UCRegistrationTask(false, calendarDayOfWeek), delay.toMillis());
				return;
			}
		}
		
		throw new IllegalArgumentException("Try run End Task from not active time");
	}
	
	public static UndergroundColiseum getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final UndergroundColiseum INSTANCE = new UndergroundColiseum();
	}
}