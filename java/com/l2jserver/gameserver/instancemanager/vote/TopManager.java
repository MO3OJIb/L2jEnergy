/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.instancemanager.vote;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.configuration.config.custom.TopsConfig;
import com.l2jserver.gameserver.model.vote.Hopzone;
import com.l2jserver.gameserver.model.vote.L2Top;
import com.l2jserver.gameserver.model.vote.L2TopSms;
import com.l2jserver.gameserver.model.vote.MMOTop;
import com.l2jserver.gameserver.model.vote.Network;
import com.l2jserver.gameserver.model.vote.TopSystem;
import com.l2jserver.gameserver.model.vote.Topzone;

/**
 * @author Мо3олЬ
 */
public class TopManager
{
	protected static final Logger LOG = LoggerFactory.getLogger(TopManager.class);
	
	protected TopManager()
	{
		if (!TopsConfig.ENABLE_VOTE_SYSTEM)
		{
			LOG.info("Loaded Vote Bonus System disabled.");
			return;
		}
		
		registerTops(new Hopzone());
		registerTops(new L2Top());
		registerTops(new L2TopSms());
		registerTops(new MMOTop());
		registerTops(new Network());
		registerTops(new Topzone());
	}
	
	public void registerTops(TopSystem tops)
	{
		tops.init();
	}
	
	public static TopManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final TopManager INSTANCE = new TopManager();
	}
}
