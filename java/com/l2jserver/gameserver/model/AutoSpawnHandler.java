/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.data.xml.impl.SpawnData;
import com.l2jserver.gameserver.idfactory.IdFactory;
import com.l2jserver.gameserver.instancemanager.MapRegionManager;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.holders.AutoSpawnHolder;
import com.l2jserver.gameserver.util.Broadcast;

/**
 * @author Tempy
 */
public class AutoSpawnHandler
{
	protected static final Logger LOG = LoggerFactory.getLogger(AutoSpawnHandler.class);
	
	private static final int DEFAULT_INITIAL_SPAWN = 30000; // 30 seconds after registration
	private static final int DEFAULT_RESPAWN = 3600000; // 1 hour in millisecs
	private static final int DEFAULT_DESPAWN = 3600000; // 1 hour in millisecs
	
	protected Map<Integer, AutoSpawnHolder> _registeredSpawns = new ConcurrentHashMap<>();
	protected Map<Integer, ScheduledFuture<?>> _runningSpawns = new ConcurrentHashMap<>();
	
	protected boolean _activeState = true;
	
	protected AutoSpawnHandler()
	{
		restoreSpawnData();
	}
	
	public final int size()
	{
		return _registeredSpawns.size();
	}
	
	public void reload()
	{
		// stop all timers
		for (var sf : _runningSpawns.values())
		{
			if (sf != null)
			{
				sf.cancel(true);
			}
		}
		// unregister all registered spawns
		for (var asi : _registeredSpawns.values())
		{
			if (asi != null)
			{
				removeSpawn(asi);
			}
		}
		
		// create clean list
		_registeredSpawns.clear();
		_runningSpawns.clear();
		
		// load
		restoreSpawnData();
	}
	
	private void restoreSpawnData()
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var s = con.createStatement();
			var rs = s.executeQuery("SELECT * FROM random_spawn ORDER BY groupId ASC");
			var ps = con.prepareStatement("SELECT * FROM random_spawn_loc WHERE groupId=?"))
		{
			// Restore spawn group data, then the location data.
			while (rs.next())
			{
				// Register random spawn group, set various options on the
				// created spawn instance.
				var spawnInst = registerSpawn(rs.getInt("npcId"), rs.getInt("initialDelay"), rs.getInt("respawnDelay"), rs.getInt("despawnDelay"));
				
				spawnInst.setSpawnCount(rs.getInt("count"));
				spawnInst.setBroadcast(rs.getBoolean("broadcastSpawn"));
				spawnInst.setRandomSpawn(rs.getBoolean("randomSpawn"));
				
				// Restore the spawn locations for this spawn group/instance.
				ps.setInt(1, rs.getInt("groupId"));
				try (var rs2 = ps.executeQuery())
				{
					ps.clearParameters();
					
					while (rs2.next())
					{
						// Add each location to the spawn group/instance.
						spawnInst.addSpawnLocation(rs2.getInt("x"), rs2.getInt("y"), rs2.getInt("z"), rs2.getInt("heading"));
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Could not restore spawn data!", ex);
		}
	}
	
	public AutoSpawnHolder registerSpawn(int npcId, int[][] spawnPoints, int initialDelay, int respawnDelay, int despawnDelay)
	{
		if (initialDelay < 0)
		{
			initialDelay = DEFAULT_INITIAL_SPAWN;
		}
		
		if (respawnDelay < 0)
		{
			respawnDelay = DEFAULT_RESPAWN;
		}
		
		if (despawnDelay < 0)
		{
			despawnDelay = DEFAULT_DESPAWN;
		}
		
		var newSpawn = new AutoSpawnHolder(npcId, initialDelay, respawnDelay, despawnDelay);
		
		if (spawnPoints != null)
		{
			for (var spawnPoint : spawnPoints)
			{
				newSpawn.addSpawnLocation(spawnPoint);
			}
		}
		
		var newId = IdFactory.getInstance().getNextId();
		newSpawn.setObjectId(newId);
		_registeredSpawns.put(newId, newSpawn);
		
		setSpawnActive(newSpawn, true);
		return newSpawn;
	}
	
	public AutoSpawnHolder registerSpawn(int npcId, int initialDelay, int respawnDelay, int despawnDelay)
	{
		return registerSpawn(npcId, null, initialDelay, respawnDelay, despawnDelay);
	}
	
	public boolean removeSpawn(AutoSpawnHolder spawnInst)
	{
		if (!isSpawnRegistered(spawnInst))
		{
			return false;
		}
		
		try
		{
			// Try to remove from the list of registered spawns if it exists.
			_registeredSpawns.remove(spawnInst.getId());
			
			// Cancel the currently associated running scheduled task.
			var respawnTask = _runningSpawns.remove(spawnInst.getObjectId());
			respawnTask.cancel(false);
		}
		catch (Exception ex)
		{
			LOG.warn("Could not auto spawn for NPC ID {} (Object ID = {})!", spawnInst.getId(), spawnInst.getObjectId(), ex);
			return false;
		}
		return true;
	}
	
	public void removeSpawn(int objectId)
	{
		removeSpawn(_registeredSpawns.get(objectId));
	}
	
	public void setSpawnActive(AutoSpawnHolder spawnInst, boolean isActive)
	{
		if (spawnInst == null)
		{
			return;
		}
		
		var objectId = spawnInst.getObjectId();
		
		if (isSpawnRegistered(objectId))
		{
			ScheduledFuture<?> spawnTask = null;
			
			if (isActive)
			{
				var rs = new AutoSpawner(objectId);
				
				if (spawnInst.getDespawnDelay() > 0)
				{
					spawnTask = ThreadPoolManager.getInstance().scheduleEffectAtFixedRate(rs, spawnInst.getInitialDelay(), spawnInst.getRespawnDelay());
				}
				else
				{
					spawnTask = ThreadPoolManager.getInstance().scheduleEffect(rs, spawnInst.getInitialDelay());
				}
				
				_runningSpawns.put(objectId, spawnTask);
			}
			else
			{
				var rd = new AutoDespawner(objectId);
				spawnTask = _runningSpawns.remove(objectId);
				
				if (spawnTask != null)
				{
					spawnTask.cancel(false);
				}
				ThreadPoolManager.getInstance().scheduleEffect(rd, 0);
			}
			spawnInst.setSpawnActive(isActive);
		}
	}
	
	public void setAllActive(boolean isActive)
	{
		if (_activeState == isActive)
		{
			return;
		}
		
		for (var spawnInst : _registeredSpawns.values())
		{
			setSpawnActive(spawnInst, isActive);
		}
		
		_activeState = isActive;
	}
	
	public final long getTimeToNextSpawn(AutoSpawnHolder spawnInst)
	{
		var objectId = spawnInst.getObjectId();
		
		if (!isSpawnRegistered(objectId))
		{
			return -1;
		}
		
		return (_runningSpawns.containsKey(objectId)) ? _runningSpawns.get(objectId).getDelay(TimeUnit.MILLISECONDS) : 0;
	}
	
	/**
	 * Attempts to return the AutoSpawnInstance associated with the given NPC or Object ID type.<br>
	 * Note: If isObjectId == false, returns first instance for the specified NPC ID.
	 * @param id
	 * @param isObjectId
	 * @return AutoSpawnInstance spawnInst
	 */
	public final AutoSpawnHolder getAutoSpawnInstance(int id, boolean isObjectId)
	{
		if (isObjectId)
		{
			if (isSpawnRegistered(id))
			{
				return _registeredSpawns.get(id);
			}
		}
		else
		{
			for (var spawnInst : _registeredSpawns.values())
			{
				if (spawnInst.getId() == id)
				{
					return spawnInst;
				}
			}
		}
		return null;
	}
	
	public List<AutoSpawnHolder> getAutoSpawnInstances(int npcId)
	{
		final List<AutoSpawnHolder> result = new LinkedList<>();
		for (var spawnInst : _registeredSpawns.values())
		{
			if (spawnInst.getId() == npcId)
			{
				result.add(spawnInst);
			}
		}
		return result;
	}
	
	public final boolean isSpawnRegistered(int objectId)
	{
		return _registeredSpawns.containsKey(objectId);
	}
	
	public final boolean isSpawnRegistered(AutoSpawnHolder spawnInst)
	{
		return _registeredSpawns.containsValue(spawnInst);
	}
	
	private class AutoSpawner implements Runnable
	{
		private final int _objectId;
		
		protected AutoSpawner(int objectId)
		{
			_objectId = objectId;
		}
		
		@Override
		public void run()
		{
			try
			{
				// Retrieve the required spawn instance for this spawn task.
				var spawnInst = _registeredSpawns.get(_objectId);
				
				// If the spawn is not scheduled to be active, cancel the spawn
				// task.
				if (!spawnInst.isSpawnActive())
				{
					return;
				}
				
				var locationList = spawnInst.getLocationList();
				
				// If there are no set co-ordinates, cancel the spawn task.
				if (locationList.length == 0)
				{
					LOG.info("AutoSpawnHandler: No location co-ords specified for spawn instance (Object ID = {}).", _objectId);
					return;
				}
				
				var locationCount = locationList.length;
				var locationIndex = Rnd.nextInt(locationCount);
				
				// If random spawning is disabled, the spawn at the next set of co-ordinates after the last.
				// If the index is greater than the number of possible spawns, reset the counter to zero.
				if (!spawnInst.isRandomSpawn())
				{
					locationIndex = spawnInst.getLastLocIndex() + 1;
					
					if (locationIndex == locationCount)
					{
						locationIndex = 0;
					}
					
					spawnInst.setLastLocIndex(locationIndex);
				}
				
				// Set the X, Y and Z co-ordinates, where this spawn will take place.
				var x = locationList[locationIndex].getX();
				var y = locationList[locationIndex].getY();
				var z = locationList[locationIndex].getZ();
				var heading = locationList[locationIndex].getHeading();
				
				var newSpawn = new L2Spawn(spawnInst.getId());
				newSpawn.setX(x);
				newSpawn.setY(y);
				newSpawn.setZ(z);
				if (heading != -1)
				{
					newSpawn.setHeading(heading);
				}
				newSpawn.setAmount(spawnInst.getSpawnCount());
				if (spawnInst.getDespawnDelay() == 0)
				{
					newSpawn.setRespawnDelay(spawnInst.getRespawnDelay());
				}
				
				// Add the new spawn information to the spawn table, but do not store it.
				SpawnData.getInstance().addNewSpawn(newSpawn, false);
				L2Npc npcInst = null;
				
				if (spawnInst.getSpawnCount() == 1)
				{
					npcInst = newSpawn.doSpawn();
					npcInst.setXYZ(npcInst.getX(), npcInst.getY(), npcInst.getZ());
					spawnInst.addNpcInstance(npcInst);
				}
				else
				{
					for (int i = 0; i < spawnInst.getSpawnCount(); i++)
					{
						npcInst = newSpawn.doSpawn();
						
						// To prevent spawning of more than one NPC in the exact same spot, move it slightly by a small random offset.
						npcInst.setXYZ(npcInst.getX() + Rnd.nextInt(50), npcInst.getY() + Rnd.nextInt(50), npcInst.getZ());
						
						// Add the NPC instance to the list of managed instances.
						spawnInst.addNpcInstance(npcInst);
					}
				}
				
				if (npcInst != null)
				{
					var nearestTown = MapRegionManager.getInstance().getClosestTownName(npcInst);
					
					// Announce to all players that the spawn has taken place, with the nearest town location.
					if (spawnInst.isBroadcasting())
					{
						Broadcast.announceToOnlinePlayers("announce_the_has_spawned_near", npcInst.getName() + "", nearestTown + "", false);
					}
				}
				
				// If there is no despawn time, do not create a despawn task.
				if (spawnInst.getDespawnDelay() > 0)
				{
					var rd = new AutoDespawner(_objectId);
					ThreadPoolManager.getInstance().scheduleAi(rd, spawnInst.getDespawnDelay() - 1000);
				}
			}
			catch (Exception ex)
			{
				LOG.warn("An error occurred while initializing spawn instance (Object ID = {})!", _objectId, ex);
			}
		}
	}
	
	private class AutoDespawner implements Runnable
	{
		private final int _objectId;
		
		protected AutoDespawner(int objectId)
		{
			_objectId = objectId;
		}
		
		@Override
		public void run()
		{
			try
			{
				var spawnInst = _registeredSpawns.get(_objectId);
				
				if (spawnInst == null)
				{
					LOG.info("AutoSpawnHandler: No spawn registered for object ID = {}.", _objectId);
					return;
				}
				
				for (var npcInst : spawnInst.getNPCInstanceList())
				{
					npcInst.deleteMe();
					SpawnData.getInstance().deleteSpawn(npcInst.getSpawn(), false);
					spawnInst.removeNpcInstance(npcInst);
				}
			}
			catch (Exception ex)
			{
				LOG.warn("An error occurred while despawning spawn (Object ID = {})!", _objectId, ex);
			}
		}
	}
	
	public static AutoSpawnHandler getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final AutoSpawnHandler INSTANCE = new AutoSpawnHandler();
	}
}
