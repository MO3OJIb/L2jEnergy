/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model;

import com.l2jserver.gameserver.enums.actors.Stats;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.stats.functions.FuncAdd;

public class ElementalBonus
{
	private byte _elementalType;
	private int _elementalValue;
	private boolean _active;
	
	public ElementalBonus(byte type, int value)
	{
		_elementalType = type;
		_elementalValue = value;
		_active = false;
	}
	
	public void applyBonus(L2PcInstance player, boolean isArmor)
	{
		// make sure the bonuses are not applied twice..
		if (_active)
		{
			return;
		}
		
		switch (_elementalType)
		{
			case Elementals.FIRE -> player.addStatFuncs(new FuncAdd(isArmor ? Stats.FIRE_RES : Stats.FIRE_POWER, 0x40, this, _elementalValue, null));
			case Elementals.WATER -> player.addStatFuncs(new FuncAdd(isArmor ? Stats.WATER_RES : Stats.WATER_POWER, 0x40, this, _elementalValue, null));
			case Elementals.WIND -> player.addStatFuncs(new FuncAdd(isArmor ? Stats.WIND_RES : Stats.WIND_POWER, 0x40, this, _elementalValue, null));
			case Elementals.EARTH -> player.addStatFuncs(new FuncAdd(isArmor ? Stats.EARTH_RES : Stats.EARTH_POWER, 0x40, this, _elementalValue, null));
			case Elementals.DARK -> player.addStatFuncs(new FuncAdd(isArmor ? Stats.DARK_RES : Stats.DARK_POWER, 0x40, this, _elementalValue, null));
			case Elementals.HOLY -> player.addStatFuncs(new FuncAdd(isArmor ? Stats.HOLY_RES : Stats.HOLY_POWER, 0x40, this, _elementalValue, null));
		}
		
		_active = true;
	}
	
	public void removeBonus(L2PcInstance player)
	{
		// make sure the bonuses are not removed twice
		if (!_active)
		{
			return;
		}
		
		player.removeStatsOwner(this);
		
		_active = false;
	}
	
	public void setValue(int val)
	{
		_elementalValue = val;
	}
	
	public void setElement(byte type)
	{
		_elementalType = type;
	}
}
