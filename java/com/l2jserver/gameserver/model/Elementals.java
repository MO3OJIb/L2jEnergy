/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model;

import java.util.HashMap;
import java.util.Map;

import com.l2jserver.gameserver.enums.items.Elemental;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

public final class Elementals
{
	private static final Map<Integer, Elemental> TABLE = new HashMap<>();
	
	static
	{
		for (var item : Elemental.values())
		{
			TABLE.put(item.getItemId(), item);
		}
	}
	
	public static final byte NONE = -1;
	public static final byte FIRE = 0;
	public static final byte WATER = 1;
	public static final byte WIND = 2;
	public static final byte EARTH = 3;
	public static final byte HOLY = 4;
	public static final byte DARK = 5;
	
	public static final int FIRST_WEAPON_BONUS = 20;
	public static final int NEXT_WEAPON_BONUS = 5;
	public static final int ARMOR_BONUS = 6;
	
	public static final int[] WEAPON_VALUES =
	{
		0, // Level 1
		25, // Level 2
		75, // Level 3
		150, // Level 4
		175, // Level 5
		225, // Level 6
		300, // Level 7
		325, // Level 8
		375, // Level 9
		450, // Level 10
		475, // Level 11
		525, // Level 12
		600, // Level 13
		Integer.MAX_VALUE
		// TODO: Higher stones
	};
	
	public static final int[] ARMOR_VALUES =
	{
		0, // Level 1
		12, // Level 2
		30, // Level 3
		60, // Level 4
		72, // Level 5
		90, // Level 6
		120, // Level 7
		132, // Level 8
		150, // Level 9
		180, // Level 10
		192, // Level 11
		210, // Level 12
		240, // Level 13
		Integer.MAX_VALUE
		// TODO: Higher stones
	};
	
	public static byte getItemElement(int itemId)
	{
		var item = TABLE.get(itemId);
		if (item != null)
		{
			return item.getElement();
		}
		return NONE;
	}
	
	public static Elemental getItemElemental(int itemId)
	{
		return TABLE.get(itemId);
	}
	
	public static int getMaxElementLevel(int itemId)
	{
		var item = TABLE.get(itemId);
		if (item != null)
		{
			return item.getType().getMaxLevel();
		}
		return -1;
	}
	
	public static String getElementName(byte element)
	{
		return switch (element)
		{
			case FIRE -> "Fire";
			case WATER -> "Water";
			case WIND -> "Wind";
			case EARTH -> "Earth";
			case DARK -> "Dark";
			case HOLY -> "Holy";
			default -> "None";
		};
	}
	
	public static byte getElementId(String name)
	{
		String tmp = name.toLowerCase();
		if (tmp.equals("fire"))
		{
			return FIRE;
		}
		if (tmp.equals("water"))
		{
			return WATER;
		}
		if (tmp.equals("wind"))
		{
			return WIND;
		}
		if (tmp.equals("earth"))
		{
			return EARTH;
		}
		if (tmp.equals("dark"))
		{
			return DARK;
		}
		if (tmp.equals("holy"))
		{
			return HOLY;
		}
		return NONE;
	}
	
	public static byte getOppositeElement(byte element)
	{
		return (byte) (((element % 2) == 0) ? (element + 1) : (element - 1));
	}
	
	// non static:
	private ElementalBonus _bonus = null;
	private byte _element = NONE;
	private int _value = 0;
	
	public byte getElement()
	{
		return _element;
	}
	
	public void setElement(byte type)
	{
		_element = type;
		_bonus.setElement(type);
	}
	
	public int getValue()
	{
		return _value;
	}
	
	public void setValue(int val)
	{
		_value = val;
		_bonus.setValue(val);
	}
	
	@Override
	public String toString()
	{
		return getElementName(_element) + " +" + _value;
	}
	
	public Elementals(byte type, int value)
	{
		_element = type;
		_value = value;
		_bonus = new ElementalBonus(_element, _value);
	}
	
	public void applyBonus(L2PcInstance player, boolean isArmor)
	{
		_bonus.applyBonus(player, isArmor);
	}
	
	public void removeBonus(L2PcInstance player)
	{
		_bonus.removeBonus(player);
	}
	
	public void updateBonus(L2PcInstance player, boolean isArmor)
	{
		_bonus.removeBonus(player);
		_bonus.applyBonus(player, isArmor);
	}
}
