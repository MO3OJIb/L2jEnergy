/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model;

import com.l2jserver.gameserver.data.xml.impl.AdminData;

/**
 * @author HorridoJoho
 */
public class L2AccessLevel
{
	private final int _accessLevel;
	
	private final String _name;
	
	private final int _child;
	private L2AccessLevel _childsAccessLevel;
	
	private final int _nameColor;
	private final int _titleColor;
	
	private final boolean _isGm;
	private final boolean _allowPeaceAttack;
	private final boolean _allowFixedRes;
	private final boolean _allowTransaction;
	private final boolean _allowAltG;
	private final boolean _giveDamage;
	private final boolean _takeAggro;
	private final boolean _gainExp;
	
	public L2AccessLevel(StatsSet set)
	{
		_accessLevel = set.getInt("level");
		_name = set.getString("name");
		_nameColor = Integer.decode("0x" + set.getString("nameColor", "FFFFFF"));
		_titleColor = Integer.decode("0x" + set.getString("titleColor", "FFFFFF"));
		_child = set.getInt("childAccess", 0);
		_isGm = set.getBoolean("isGM", false);
		_allowPeaceAttack = set.getBoolean("allowPeaceAttack", false);
		_allowFixedRes = set.getBoolean("allowFixedRes", false);
		_allowTransaction = set.getBoolean("allowTransaction", true);
		_allowAltG = set.getBoolean("allowAltg", false);
		_giveDamage = set.getBoolean("giveDamage", true);
		_takeAggro = set.getBoolean("takeAggro", true);
		_gainExp = set.getBoolean("gainExp", true);
	}
	
	public int getLevel()
	{
		return _accessLevel;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public int getNameColor()
	{
		return _nameColor;
	}
	
	public int getTitleColor()
	{
		return _titleColor;
	}
	
	public boolean isGm()
	{
		return _isGm;
	}
	
	public boolean allowPeaceAttack()
	{
		return _allowPeaceAttack;
	}
	
	public boolean allowFixedRes()
	{
		return _allowFixedRes;
	}
	
	public boolean allowTransaction()
	{
		return _allowTransaction;
	}
	
	public boolean allowAltG()
	{
		return _allowAltG;
	}
	
	public boolean canGiveDamage()
	{
		return _giveDamage;
	}
	
	public boolean canTakeAggro()
	{
		return _takeAggro;
	}
	
	public boolean canGainExp()
	{
		return _gainExp;
	}
	
	public boolean hasChildAccess(L2AccessLevel accessLevel)
	{
		if ((_childsAccessLevel == null) && (_child > 0))
		{
			_childsAccessLevel = AdminData.getInstance().getAccessLevel(_child);
		}
		return (_childsAccessLevel != null) && ((_childsAccessLevel.getLevel() == accessLevel.getLevel()) || _childsAccessLevel.hasChildAccess(accessLevel));
	}
}