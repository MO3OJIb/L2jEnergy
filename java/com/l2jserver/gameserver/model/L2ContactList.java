/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.sql.impl.CharNameTable;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * TODO: System messages:<br>
 * ADD: 3223: The previous name is being registered. Please try again later.<br>
 * DEL 3219: $s1 was successfully deleted from your Contact List.<br>
 * DEL 3217: The name is not currently registered.
 * @author UnAfraid
 * @author mrTJO
 */
public class L2ContactList
{
	private final L2PcInstance _player;
	
	private final List<String> _contacts = new CopyOnWriteArrayList<>();
	
	public static final int MAX_POST_FRIEND_SIZE = 100;
	
	public L2ContactList(L2PcInstance player)
	{
		_player = player;
		restore();
	}
	
	public void restore()
	{
		_contacts.clear();
		
		DAOFactory.getInstance().getPlayerPostFriendDAO().select(_player);
	}
	
	public boolean add(String name)
	{
		var contactId = CharNameTable.getInstance().getIdByName(name);
		if (_contacts.contains(name))
		{
			_player.sendPacket(SystemMessageId.THE_NAME_ALREADY_EXISTS_ON_THE_ADDED_LIST);
			return false;
		}
		else if (_player.getName().equals(name))
		{
			_player.sendPacket(SystemMessageId.YOU_CANNOT_ADD_YOUR_OWN_NAME);
			return false;
		}
		else if (_contacts.size() >= MAX_POST_FRIEND_SIZE)
		{
			_player.sendPacket(SystemMessageId.THE_MAXIMUM_NUMBER_OF_NAMES_100_HAS_BEEN_REACHED);
			return false;
		}
		else if (contactId < 1)
		{
			var sm = SystemMessage.getSystemMessage(SystemMessageId.THE_NAME_S1_NOT_EXIST_TRY_ANOTHER_NAME);
			sm.addString(name);
			_player.sendPacket(sm);
			return false;
		}
		else
		{
			for (var contactName : _contacts)
			{
				if (contactName.equalsIgnoreCase(name))
				{
					_player.sendPacket(SystemMessageId.THE_NAME_ALREADY_EXISTS_ON_THE_ADDED_LIST);
					return false;
				}
			}
		}
		DAOFactory.getInstance().getPlayerPostFriendDAO().remove(_player, contactId, name);
		return true;
	}
	
	public void remove(String name)
	{
		var contactId = CharNameTable.getInstance().getIdByName(name);
		
		if (!_contacts.contains(name))
		{
			_player.sendPacket(SystemMessageId.THE_NAME_IS_NOT_CURRENTLY_REGISTERED);
			return;
		}
		else if (contactId < 1)
		{
			// TODO: Message?
			return;
		}
		_contacts.remove(name);
		DAOFactory.getInstance().getPlayerPostFriendDAO().remove(_player, contactId, name);
	}
	
	public List<String> getAllContacts()
	{
		return _contacts;
	}
}