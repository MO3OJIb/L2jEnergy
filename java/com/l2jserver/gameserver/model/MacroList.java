/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.enums.ShortcutType;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.interfaces.IRestorable;
import com.l2jserver.gameserver.network.serverpackets.SendMacroList;

public class MacroList implements IRestorable
{
	private final L2PcInstance _owner;
	private int _revision;
	private int _macroId;
	private final Map<Integer, Macro> _macroses = Collections.synchronizedMap(new LinkedHashMap<>());
	
	public MacroList(L2PcInstance owner)
	{
		_owner = owner;
		_revision = 1;
		_macroId = 1000;
	}
	
	public int getRevision()
	{
		return _revision;
	}
	
	public Map<Integer, Macro> getAllMacroses()
	{
		return _macroses;
	}
	
	public void registerMacro(Macro macro)
	{
		if (macro.getId() == 0)
		{
			macro.setId(_macroId++);
			while (_macroses.containsKey(macro.getId()))
			{
				macro.setId(_macroId++);
			}
			_macroses.put(macro.getId(), macro);
			DAOFactory.getInstance().getPlayerMacrosesDAO().registerMacro(_owner, macro);
		}
		else
		{
			var old = _macroses.put(macro.getId(), macro);
			if (old != null)
			{
				DAOFactory.getInstance().getPlayerMacrosesDAO().deleteMacro(_owner, old);
			}
			DAOFactory.getInstance().getPlayerMacrosesDAO().registerMacro(_owner, macro);
		}
		sendUpdate();
	}
	
	public void deleteMacro(int id)
	{
		var removed = _macroses.remove(id);
		if (removed != null)
		{
			DAOFactory.getInstance().getPlayerMacrosesDAO().deleteMacro(_owner, removed);
		}
		
		for (var sc : _owner.getAllShortCuts())
		{
			if ((sc.getId() == id) && (sc.getType() == ShortcutType.MACRO))
			{
				_owner.deleteShortCut(sc.getSlot(), sc.getPage());
			}
		}
		
		sendUpdate();
	}
	
	public void sendUpdate()
	{
		_revision++;
		var allMacros = _macroses.values();
		synchronized (_macroses)
		{
			if (allMacros.isEmpty())
			{
				_owner.sendPacket(new SendMacroList(_revision, 0, null));
			}
			else
			{
				for (var m : allMacros)
				{
					_owner.sendPacket(new SendMacroList(_revision, allMacros.size(), m));
				}
			}
		}
	}
	
	@Override
	public boolean restoreMe()
	{
		return DAOFactory.getInstance().getPlayerMacrosesDAO().restoreMe(_owner, _macroses);
	}
}
