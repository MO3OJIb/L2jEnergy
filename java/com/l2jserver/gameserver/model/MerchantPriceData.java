/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model;

import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.model.entity.Castle;

/**
 * @author KenM
 */
public class MerchantPriceData
{
	private final int _id;
	private final String _name;
	private final int _baseTax;
	private final int _castleId;
	private Castle _castle;
	private final int _zoneId;
	
	public MerchantPriceData(final int id, final String name, final int baseTax, final int castleId, final int zoneId)
	{
		_id = id;
		_name = name;
		_baseTax = baseTax;
		_castleId = castleId;
		_zoneId = zoneId;
	}
	
	public int getId()
	{
		return _id;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public int getBaseTax()
	{
		return _baseTax;
	}
	
	public double getBaseTaxRate()
	{
		return _baseTax / 100.0;
	}
	
	public Castle getCastle()
	{
		return _castle;
	}
	
	public int getZoneId()
	{
		return _zoneId;
	}
	
	public boolean hasCastle()
	{
		return getCastle() != null;
	}
	
	public double getCastleTaxRate()
	{
		return hasCastle() ? getCastle().getTaxRate() : 0.0;
	}
	
	public int getTotalTax()
	{
		return hasCastle() ? (getCastle().getTaxPercent() + getBaseTax()) : getBaseTax();
	}
	
	public double getTotalTaxRate()
	{
		return getTotalTax() / 100.0;
	}
	
	public void updateReferences()
	{
		if (_castleId > 0)
		{
			_castle = CastleManager.getInstance().getCastleById(_castleId);
		}
	}
}
