/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model;

import java.util.HashMap;
import java.util.Map;

import com.l2jserver.gameserver.model.holders.NpcBufferHolder;

public class NpcBufferSkills
{
	private final int _npcId;
	private final Map<Integer, NpcBufferHolder> _skills = new HashMap<>();
	
	public NpcBufferSkills(int npcId)
	{
		_npcId = npcId;
	}
	
	public void addSkill(int skillId, int skillLevel, int skillFeeId, int skillFeeAmount, int buffGroup)
	{
		_skills.put(buffGroup, new NpcBufferHolder(skillId, skillLevel, skillFeeId, skillFeeAmount));
	}
	
	public NpcBufferHolder getSkillGroupInfo(int buffGroup)
	{
		return _skills.get(buffGroup);
	}
	
	public int getNpcId()
	{
		return _npcId;
	}
}
