/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model;

public class PvpReward
{
	private int pvpAmount;
	private int itemId;
	private int itemCount;
	private String nameColor;
	private String titleColor;
	
	public int getPvpAmount()
	{
		return pvpAmount;
	}
	
	public int getId()
	{
		return itemId;
	}
	
	public int getCount()
	{
		return itemCount;
	}
	
	public int getNameColor()
	{
		if ((nameColor != null) && (nameColor.length() > 0))
		{
			return Integer.decode("0x" + nameColor);
		}
		return Integer.decode("0xFF0000");
	}
	
	public int getTitleColor()
	{
		if ((titleColor != null) && (titleColor.length() > 0))
		{
			return Integer.decode("0x" + titleColor);
		}
		return Integer.decode("0xFF0000");
	}
}
