/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model;

import java.util.Map;
import java.util.TreeMap;

import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.enums.ShortcutType;
import com.l2jserver.gameserver.enums.items.EtcItemType;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.interfaces.IRestorable;
import com.l2jserver.gameserver.network.serverpackets.ExAutoSoulShot;
import com.l2jserver.gameserver.network.serverpackets.ShortCutInit;
import com.l2jserver.gameserver.network.serverpackets.ShortCutRegister;

public class ShortCuts implements IRestorable
{
	public static final int MAX_SHORTCUTS_PER_BAR = 12;
	private final L2PcInstance _player;
	private final Map<Integer, Shortcut> _shortCuts = new TreeMap<>();
	
	public ShortCuts(L2PcInstance player)
	{
		_player = player;
	}
	
	public Shortcut[] getAllShortCuts()
	{
		return _shortCuts.values().toArray(new Shortcut[_shortCuts.size()]);
	}
	
	public Shortcut getShortCut(int slot, int page)
	{
		var sc = _shortCuts.get(slot + (page * MAX_SHORTCUTS_PER_BAR));
		// Verify shortcut
		if ((sc != null) && (sc.getType() == ShortcutType.ITEM))
		{
			if (_player.getInventory().getItemByObjectId(sc.getId()) == null)
			{
				deleteShortCut(sc.getSlot(), sc.getPage());
				sc = null;
			}
		}
		return sc;
	}
	
	public synchronized void registerShortCut(Shortcut shortcut)
	{
		// Verify shortcut
		if (shortcut.getType() == ShortcutType.ITEM)
		{
			var item = _player.getInventory().getItemByObjectId(shortcut.getId());
			if (item == null)
			{
				return;
			}
			shortcut.setSharedReuseGroup(item.getSharedReuseGroup());
		}
		var oldShortCut = _shortCuts.put(shortcut.getSlot() + (shortcut.getPage() * MAX_SHORTCUTS_PER_BAR), shortcut);
		DAOFactory.getInstance().getShortcutDAO().registerShortCut(_player.getObjectId(), shortcut, oldShortCut, _player.getClassIndex());
	}
	
	public synchronized void deleteShortCut(int slot, int page)
	{
		var old = _shortCuts.remove(slot + (page * MAX_SHORTCUTS_PER_BAR));
		if ((old == null) || (_player == null))
		{
			return;
		}
		
		DAOFactory.getInstance().getShortcutDAO().delete(_player.getObjectId(), old, _player.getClassIndex());
		
		if (old.getType() == ShortcutType.ITEM)
		{
			var item = _player.getInventory().getItemByObjectId(old.getId());
			
			if ((item != null) && (item.getItemType() == EtcItemType.SHOT))
			{
				if (_player.removeAutoSoulShot(item.getId()))
				{
					_player.sendPacket(new ExAutoSoulShot(item.getId(), 0));
				}
			}
		}
		
		_player.sendPacket(new ShortCutInit(_player));
		
		for (int shotId : _player.getAutoSoulShot())
		{
			_player.sendPacket(new ExAutoSoulShot(shotId, 1));
		}
	}
	
	public synchronized void deleteShortCutByObjectId(int objectId)
	{
		for (var shortcut : _shortCuts.values())
		{
			if ((shortcut.getType() == ShortcutType.ITEM) && (shortcut.getId() == objectId))
			{
				deleteShortCut(shortcut.getSlot(), shortcut.getPage());
				break;
			}
		}
	}
	
	@Override
	public boolean restoreMe()
	{
		DAOFactory.getInstance().getShortcutDAO().restore(_player, _shortCuts);
		
		// Verify shortcuts
		for (var sc : getAllShortCuts())
		{
			if (sc.getType() == ShortcutType.ITEM)
			{
				var item = _player.getInventory().getItemByObjectId(sc.getId());
				if (item == null)
				{
					deleteShortCut(sc.getSlot(), sc.getPage());
				}
				else if (item.isEtcItem())
				{
					sc.setSharedReuseGroup(item.getEtcItem().getSharedReuseGroup());
				}
			}
		}
		return true;
	}
	
	public synchronized void updateShortCuts(int skillId, int skillLevel)
	{
		// Update all the shortcuts for this skill
		for (var sc : _shortCuts.values())
		{
			if ((sc.getId() == skillId) && (sc.getType() == ShortcutType.SKILL))
			{
				var newsc = new Shortcut(sc.getSlot(), sc.getPage(), sc.getType(), sc.getId(), skillLevel, 1);
				_player.sendPacket(new ShortCutRegister(newsc));
				_player.registerShortCut(newsc);
			}
		}
	}
}
