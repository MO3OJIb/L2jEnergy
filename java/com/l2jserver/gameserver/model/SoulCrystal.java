/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model;

public class SoulCrystal
{
	private final int _level;
	private final int _itemId;
	private final int _leveledItemId;
	
	public SoulCrystal(int level, int itemId, int leveledItemId)
	{
		_level = level;
		_itemId = itemId;
		_leveledItemId = leveledItemId;
	}
	
	public final int getItemId()
	{
		return _itemId;
	}
	
	public final int getLevel()
	{
		return _level;
	}
	
	public final int getLeveledItemId()
	{
		return _leveledItemId;
	}
}
