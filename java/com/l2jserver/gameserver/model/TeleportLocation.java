/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model;

public class TeleportLocation
{
	private final int _teleId;
	private final int _locX;
	private final int _locY;
	private final int _locZ;
	private final int _price;
	private final boolean _isNoble;
	private final int _itemId;
	
	public TeleportLocation(int id, int x, int y, int z, int price, boolean isNoble, int itemId)
	{
		_teleId = id;
		_locX = x;
		_locY = y;
		_locZ = z;
		_price = price;
		_isNoble = isNoble;
		_itemId = itemId;
	}
	
	public int getTeleId()
	{
		return _teleId;
	}
	
	public int getLocX()
	{
		return _locX;
	}
	
	public int getLocY()
	{
		return _locY;
	}
	
	public int getLocZ()
	{
		return _locZ;
	}
	
	public int getPrice()
	{
		return _price;
	}
	
	public boolean isNoble()
	{
		return _isNoble;
	}
	
	public int getItemId()
	{
		return _itemId;
	}
}
