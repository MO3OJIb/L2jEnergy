/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model;

import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;

public final class TempItem
{
	private final int _itemId;
	private int _quantity;
	private final int _referencePrice;
	private final String _itemName;
	
	/**
	 * @param item
	 * @param quantity of that item
	 */
	public TempItem(L2ItemInstance item, int quantity)
	{
		_itemId = item.getId();
		_quantity = quantity;
		_itemName = item.getItem().getName();
		_referencePrice = item.getReferencePrice();
	}
	
	/**
	 * @return the quantity.
	 */
	public int getQuantity()
	{
		return _quantity;
	}
	
	/**
	 * @param quantity The quantity to set.
	 */
	public void setQuantity(int quantity)
	{
		_quantity = quantity;
	}
	
	public int getReferencePrice()
	{
		return _referencePrice;
	}
	
	/**
	 * @return the itemId.
	 */
	public int getItemId()
	{
		return _itemId;
	}
	
	/**
	 * @return the itemName.
	 */
	public String getItemName()
	{
		return _itemName;
	}
}
