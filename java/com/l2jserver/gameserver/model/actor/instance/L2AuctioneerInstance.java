/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.actor.instance;

import static com.l2jserver.gameserver.model.itemcontainer.Inventory.MAX_ADENA;
import static java.util.concurrent.TimeUnit.DAYS;

import java.util.Calendar;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.ClanPrivilege;
import com.l2jserver.gameserver.enums.InstanceType;
import com.l2jserver.gameserver.instancemanager.AuctionManager;
import com.l2jserver.gameserver.instancemanager.ClanHallManager;
import com.l2jserver.gameserver.instancemanager.MapRegionManager;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.templates.L2NpcTemplate;
import com.l2jserver.gameserver.model.entity.Auction;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;

public final class L2AuctioneerInstance extends L2Npc
{
	private static final Logger LOG = LoggerFactory.getLogger(L2AuctioneerInstance.class);
	
	private static final int COND_ALL_FALSE = 0;
	private static final int COND_BUSY_BECAUSE_OF_SIEGE = 1;
	private static final int COND_REGULAR = 3;
	
	private int _currentPage = 1;
	
	private final Map<Integer, Auction> _pendingAuctions = new ConcurrentHashMap<>();
	
	public L2AuctioneerInstance(L2NpcTemplate template)
	{
		super(template);
		setInstanceType(InstanceType.L2AuctioneerInstance);
	}
	
	@Override
	public void onBypassFeedback(L2PcInstance player, String command)
	{
		var condition = validateCondition(player);
		if (condition <= COND_ALL_FALSE)
		{
			// TODO: html
			player.sendMessage(MessagesData.getInstance().getMessage(player, "auction_wrong_conditions"));
			return;
		}
		else if (condition == COND_BUSY_BECAUSE_OF_SIEGE)
		{
			var html = new NpcHtmlMessage(getObjectId());
			html.setFile(player, "data/html/auction/auction-busy.htm");
			html.replace("%objectId%", String.valueOf(getObjectId()));
			player.sendPacket(html);
			return;
		}
		else if (condition == COND_REGULAR)
		{
			var st = new StringTokenizer(command, " ");
			var actualCommand = st.nextToken(); // Get actual command
			
			String val = "";
			if (st.hasMoreTokens())
			{
				val = st.nextToken();
			}
			
			if (actualCommand.equalsIgnoreCase("auction"))
			{
				if (val.isEmpty())
				{
					return;
				}
				
				try
				{
					var days = Integer.parseInt(val);
					try
					{
						long bid = 0;
						if (st.hasMoreTokens())
						{
							bid = Math.min(Long.parseLong(st.nextToken()), MAX_ADENA);
						}
						
						var a = new Auction(player.getClan().getHideoutId(), player.getClan(), DAYS.toMillis(days), bid, ClanHallManager.getInstance().getClanHallByOwner(player.getClan()).getName());
						if (_pendingAuctions.get(a.getId()) != null)
						{
							_pendingAuctions.remove(a.getId());
						}
						
						_pendingAuctions.put(a.getId(), a);
						
						var html = new NpcHtmlMessage(getObjectId());
						html.setFile(player, "data/html/auction/AgitSale3.htm");
						html.replace("%x%", val);
						html.replace("%AGIT_AUCTION_END%", String.valueOf(TimeUtils.DATE_TIME_FORMATTER_FIX.format(a.getEndDate())));
						html.replace("%AGIT_AUCTION_MINBID%", String.valueOf(a.getStartingBid()));
						html.replace("%AGIT_AUCTION_MIN%", String.valueOf(a.getStartingBid()));
						html.replace("%AGIT_AUCTION_DESC%", ClanHallManager.getInstance().getClanHallByOwner(player.getClan()).getDesc());
						html.replace("%AGIT_LINK_BACK%", "bypass -h npc_" + getObjectId() + "_sale2");
						html.replace("%objectId%", String.valueOf((getObjectId())));
						player.sendPacket(html);
					}
					catch (Exception ex)
					{
						player.sendMessage(MessagesData.getInstance().getMessage(player, "auction_invalid_bid"));
					}
				}
				catch (Exception ex)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "auction_invalid_duration"));
				}
				return;
			}
			else if (actualCommand.equalsIgnoreCase("confirmAuction"))
			{
				try
				{
					var a = _pendingAuctions.get(player.getClan().getHideoutId());
					a.confirmAuction();
					_pendingAuctions.remove(player.getClan().getHideoutId());
				}
				catch (Exception ex)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "auction_invalid"));
				}
				return;
			}
			else if (actualCommand.equalsIgnoreCase("bidding"))
			{
				if (val.isEmpty())
				{
					return;
				}
				
				try
				{
					var auctionId = Integer.parseInt(val);
					
					var a = AuctionManager.getInstance().getAuction(auctionId);
					
					var html = new NpcHtmlMessage(getObjectId());
					html.setFile(player, "data/html/auction/AgitAuctionInfo.htm");
					if (a != null)
					{
						html.replace("%AGIT_NAME%", a.getItemName());
						html.replace("%OWNER_PLEDGE_NAME%", a.getSellerClanName());
						html.replace("%OWNER_PLEDGE_MASTER%", a.getSellerName());
						html.replace("%AGIT_SIZE%", String.valueOf(ClanHallManager.getInstance().getAuctionableHallById(a.getItemId()).getGrade() * 10));
						html.replace("%AGIT_LEASE%", String.valueOf(ClanHallManager.getInstance().getAuctionableHallById(a.getItemId()).getLease()));
						html.replace("%AGIT_LOCATION%", ClanHallManager.getInstance().getAuctionableHallById(a.getItemId()).getLocation());
						html.replace("%AGIT_AUCTION_END%", String.valueOf(TimeUtils.DATE_TIME_FORMATTER_FIX.format(a.getEndDate())));
						html.replace("%AGIT_AUCTION_REMAIN%", String.valueOf((a.getEndDate() - System.currentTimeMillis()) / 3600000) + " hours " + String.valueOf((((a.getEndDate() - System.currentTimeMillis()) / 60000) % 60)) + " minutes");
						html.replace("%AGIT_AUCTION_MINBID%", String.valueOf(a.getStartingBid()));
						html.replace("%AGIT_AUCTION_COUNT%", String.valueOf(a.getBidders().size()));
						html.replace("%AGIT_AUCTION_DESC%", ClanHallManager.getInstance().getAuctionableHallById(a.getItemId()).getDesc());
						html.replace("%AGIT_LINK_BACK%", "bypass -h npc_" + getObjectId() + "_list");
						html.replace("%AGIT_LINK_BIDLIST%", "bypass -h npc_" + getObjectId() + "_bidlist " + a.getId());
						html.replace("%AGIT_LINK_RE%", "bypass -h npc_" + getObjectId() + "_bid1 " + a.getId());
					}
					else
					{
						LOG.debug("Auctioneer Auction null for AuctionId : {}", auctionId);
					}
					
					player.sendPacket(html);
				}
				catch (Exception ex)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "auction_invalid"));
				}
				return;
			}
			else if (actualCommand.equalsIgnoreCase("bid"))
			{
				if (val.isEmpty())
				{
					return;
				}
				
				try
				{
					var auctionId = Integer.parseInt(val);
					try
					{
						long bid = 0;
						if (st.hasMoreTokens())
						{
							bid = Math.min(Long.parseLong(st.nextToken()), MAX_ADENA);
						}
						AuctionManager.getInstance().getAuction(auctionId).setBid(player, bid);
					}
					catch (Exception ex)
					{
						player.sendMessage(MessagesData.getInstance().getMessage(player, "auction_invalid_bid"));
					}
				}
				catch (Exception ex)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "auction_invalid"));
				}
				return;
			}
			else if (actualCommand.equalsIgnoreCase("bid1"))
			{
				if ((player.getClan() == null) || (player.getClan().getLevel() < 2))
				{
					player.sendPacket(SystemMessageId.ONLY_A_CLAN_LEADER_WHOSE_CLAN_IS_OF_LEVEL_2_OR_HIGHER_IS_ALLOWED_TO_PARTICIPATE_IN_A_CLAN_HALL_AUCTION);
					return;
				}
				
				if (val.isEmpty())
				{
					return;
				}
				
				if (((player.getClan().getAuctionBiddedAt() > 0) && (player.getClan().getAuctionBiddedAt() != Integer.parseInt(val))) || (player.getClan().getHideoutId() > 0))
				{
					player.sendPacket(SystemMessageId.SINCE_YOU_HAVE_ALREADY_SUBMITTED_A_BID_YOU_ARE_NOT_ALLOWED_TO_PARTICIPATE_IN_ANOTHER_AUCTION_AT_THIS_TIME);
					return;
				}
				
				try
				{
					var minimumBid = AuctionManager.getInstance().getAuction(Integer.parseInt(val)).getHighestBidderMaxBid();
					if (minimumBid == 0)
					{
						minimumBid = AuctionManager.getInstance().getAuction(Integer.parseInt(val)).getStartingBid();
					}
					
					var html = new NpcHtmlMessage(getObjectId());
					html.setFile(player, "data/html/auction/AgitBid1.htm");
					html.replace("%AGIT_LINK_BACK%", "bypass -h npc_" + getObjectId() + "_bidding " + val);
					html.replace("%PLEDGE_ADENA%", String.valueOf(player.getClan().getWarehouse().getAdena()));
					html.replace("%AGIT_AUCTION_MINBID%", String.valueOf(minimumBid));
					html.replace("npc_%objectId%_bid", "npc_" + getObjectId() + "_bid " + val);
					player.sendPacket(html);
					return;
				}
				catch (Exception ex)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "auction_invalid"));
				}
				return;
			}
			else if (actualCommand.equalsIgnoreCase("list"))
			{
				var auctions = AuctionManager.getInstance().getAuctions();
				/** Limit for make new page, prevent client crash **/
				int limit = 10;
				int start;
				int i = 1;
				
				if (val.isEmpty())
				{
					start = 1;
					_currentPage = 1;
				}
				else
				{
					start = (limit * (Integer.parseInt(val) - 1)) + 1;
					limit *= Integer.parseInt(val);
					_currentPage = Integer.parseInt(val);
				}
				
				var sb = new StringBuilder();
				sb.append("<table width=280 border=0>");
				
				for (var a : auctions)
				{
					if (a == null)
					{
						continue;
					}
					
					if (i > limit)
					{
						break;
					}
					else if (i < start)
					{
						i++;
						continue;
					}
					else
					{
						i++;
					}
					
					sb.append("<tr>");
					sb.append("<td width=70 align=left><font color=\"99B3FF\">");
					sb.append(ClanHallManager.getInstance().getAuctionableHallById(a.getItemId()).getLocation());
					sb.append("</font></td>");
					sb.append("<td width=70 align=left><font color=\"FFFF99\"><a action=\"bypass -h npc_");
					sb.append(getObjectId());
					sb.append("_bidding ");
					sb.append(a.getId());
					sb.append("\">");
					sb.append(a.getItemName());
					sb.append("[");
					sb.append(AuctionManager.getInstance().getAuction(a.getId()).getBidders().size());
					sb.append("]</a></font></td>");
					sb.append("<td width=70 align=left>");
					sb.append("<td>" + TimeUtils.DATE_FORMAT_FIX.format(a.getEndDate()));
					sb.append("</td>");
					sb.append("<td width=70 align=left><font color=\"99FFFF\">");
					sb.append(a.getStartingBid());
					sb.append("</font></td>");
					sb.append("</tr>");
					sb.append("<tr><td height=5></td></tr>");
				}
				sb.append("</table>");
				
				sb.append("<table width=280 border=0>");
				sb.append("<tr>");
				if (_currentPage > 1)
				{
					sb.append("<td width=80 align=left>");
					sb.append("<button action=\"bypass -h npc_");
					sb.append(getObjectId());
					sb.append("_list ");
					sb.append(_currentPage - 1);
					sb.append("\"");
					sb.append(" value=\"Previous\" width=80 height=27 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\">");
					sb.append("</td>");
				}
				sb.append("<td width=80 align=left>");
				sb.append("<button action=\"bypass -h npc_");
				sb.append(getObjectId());
				sb.append("_list ");
				sb.append(_currentPage + 1);
				sb.append("\"");
				sb.append(" value=\"Next\" width=80 height=27 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\">");
				sb.append("</td>");
				sb.append("<td width=120></td>");
				sb.append("</tr>");
				
				sb.append("</table>");
				
				var html = new NpcHtmlMessage(getObjectId());
				html.setFile(player, "data/html/auction/AgitAuctionList.htm");
				html.replace("%AGIT_LINK_BACK%", "bypass -h npc_" + getObjectId() + "_start");
				html.replace("%itemsField%", sb.toString());
				player.sendPacket(html);
				return;
			}
			else if (actualCommand.equalsIgnoreCase("bidlist"))
			{
				int auctionId = 0;
				if (val.isEmpty())
				{
					if (player.getClan().getAuctionBiddedAt() <= 0)
					{
						return;
					}
					auctionId = player.getClan().getAuctionBiddedAt();
				}
				else
				{
					auctionId = Integer.parseInt(val);
				}
				
				var sb = new StringBuilder();
				for (var b : AuctionManager.getInstance().getAuction(auctionId).getBidders().values())
				{
					sb.append("<tr><td>")
						.append(b.getClanName())
						.append("</td><td>")
						.append(b.getName())
						.append("</td><td>")
						.append(b.getTimeBid().get(Calendar.YEAR))
						.append("/")
						.append(b.getTimeBid().get(Calendar.MONTH) + 1)
						.append("/")
						.append(b.getTimeBid().get(Calendar.DATE))
						.append("</td><td>")
						.append(b.getBid())
						.append("</td>")
						.append("</tr>");
				}
				var html = new NpcHtmlMessage(getObjectId());
				html.setFile(player, "data/html/auction/AgitBidderList.htm");
				html.replace("%AGIT_LIST%", sb.toString());
				html.replace("%AGIT_LINK_BACK%", "bypass -h npc_" + getObjectId() + "_bidding " + auctionId);
				player.sendPacket(html);
				return;
			}
			else if (actualCommand.equalsIgnoreCase("selectedItems"))
			{
				if ((player.getClan() != null) && (player.getClan().getHideoutId() == 0) && (player.getClan().getAuctionBiddedAt() > 0))
				{
					var html = new NpcHtmlMessage(getObjectId());
					html.setFile(player, "data/html/auction/AgitBidInfo.htm");
					var a = AuctionManager.getInstance().getAuction(player.getClan().getAuctionBiddedAt());
					if (a != null)
					{
						html.replace("%AGIT_NAME%", a.getItemName());
						html.replace("%OWNER_PLEDGE_NAME%", a.getSellerClanName());
						html.replace("%OWNER_PLEDGE_MASTER%", a.getSellerName());
						html.replace("%AGIT_SIZE%", String.valueOf(ClanHallManager.getInstance().getAuctionableHallById(a.getItemId()).getGrade() * 10));
						html.replace("%AGIT_LEASE%", String.valueOf(ClanHallManager.getInstance().getAuctionableHallById(a.getItemId()).getLease()));
						html.replace("%AGIT_LOCATION%", ClanHallManager.getInstance().getAuctionableHallById(a.getItemId()).getLocation());
						html.replace("%AGIT_AUCTION_END%", String.valueOf(TimeUtils.DATE_TIME_FORMATTER_FIX.format(a.getEndDate())));
						html.replace("%AGIT_AUCTION_REMAIN%", String.valueOf((a.getEndDate() - System.currentTimeMillis()) / 3600000) + " hours " + String.valueOf((((a.getEndDate() - System.currentTimeMillis()) / 60000) % 60)) + " minutes");
						html.replace("%AGIT_AUCTION_MINBID%", String.valueOf(a.getStartingBid()));
						html.replace("%AGIT_AUCTION_MYBID%", String.valueOf(a.getBidders().get(player.getClanId()).getBid()));
						html.replace("%AGIT_AUCTION_DESC%", ClanHallManager.getInstance().getAuctionableHallById(a.getItemId()).getDesc());
						html.replace("%objectId%", String.valueOf(getObjectId()));
						html.replace("%AGIT_LINK_BACK%", "bypass -h npc_" + getObjectId() + "_start");
					}
					else
					{
						LOG.warn("Auctioneer Auction null for AuctionBiddedAt : {}", player.getClan().getAuctionBiddedAt());
					}
					
					player.sendPacket(html);
					return;
				}
				else if ((player.getClan() != null) && (AuctionManager.getInstance().getAuction(player.getClan().getHideoutId()) != null))
				{
					var html = new NpcHtmlMessage(getObjectId());
					html.setFile(player, "data/html/auction/AgitSaleInfo.htm");
					var a = AuctionManager.getInstance().getAuction(player.getClan().getHideoutId());
					if (a != null)
					{
						html.replace("%AGIT_NAME%", a.getItemName());
						html.replace("%AGIT_OWNER_PLEDGE_NAME%", a.getSellerClanName());
						html.replace("%OWNER_PLEDGE_MASTER%", a.getSellerName());
						html.replace("%AGIT_SIZE%", String.valueOf(ClanHallManager.getInstance().getAuctionableHallById(a.getItemId()).getGrade() * 10));
						html.replace("%AGIT_LEASE%", String.valueOf(ClanHallManager.getInstance().getAuctionableHallById(a.getItemId()).getLease()));
						html.replace("%AGIT_LOCATION%", ClanHallManager.getInstance().getAuctionableHallById(a.getItemId()).getLocation());
						html.replace("%AGIT_AUCTION_END%", String.valueOf(TimeUtils.DATE_TIME_FORMATTER_FIX.format(a.getEndDate())));
						html.replace("%AGIT_AUCTION_REMAIN%", String.valueOf((a.getEndDate() - System.currentTimeMillis()) / 3600000) + " hours " + String.valueOf((((a.getEndDate() - System.currentTimeMillis()) / 60000) % 60)) + " minutes");
						html.replace("%AGIT_AUCTION_MINBID%", String.valueOf(a.getStartingBid()));
						html.replace("%AGIT_AUCTION_BIDCOUNT%", String.valueOf(a.getBidders().size()));
						html.replace("%AGIT_AUCTION_DESC%", ClanHallManager.getInstance().getAuctionableHallById(a.getItemId()).getDesc());
						html.replace("%AGIT_LINK_BACK%", "bypass -h npc_" + getObjectId() + "_start");
						html.replace("%id%", String.valueOf(a.getId()));
						html.replace("%objectId%", String.valueOf(getObjectId()));
					}
					else
					{
						LOG.warn("Auctioneer Auction null for getHasHideout : {}", player.getClan().getHideoutId());
					}
					
					player.sendPacket(html);
					return;
				}
				else if ((player.getClan() != null) && (player.getClan().getHideoutId() != 0))
				{
					var ItemId = player.getClan().getHideoutId();
					var html = new NpcHtmlMessage(getObjectId());
					html.setFile(player, "data/html/auction/AgitInfo.htm");
					if (ClanHallManager.getInstance().getAuctionableHallById(ItemId) != null)
					{
						html.replace("%AGIT_NAME%", ClanHallManager.getInstance().getAuctionableHallById(ItemId).getName());
						html.replace("%AGIT_OWNER_PLEDGE_NAME%", player.getClan().getName());
						html.replace("%OWNER_PLEDGE_MASTER%", player.getClan().getLeaderName());
						html.replace("%AGIT_SIZE%", String.valueOf(ClanHallManager.getInstance().getAuctionableHallById(ItemId).getGrade() * 10));
						html.replace("%AGIT_LEASE%", String.valueOf(ClanHallManager.getInstance().getAuctionableHallById(ItemId).getLease()));
						html.replace("%AGIT_LOCATION%", ClanHallManager.getInstance().getAuctionableHallById(ItemId).getLocation());
						html.replace("%AGIT_LINK_BACK%", "bypass -h npc_" + getObjectId() + "_start");
						html.replace("%objectId%", String.valueOf(getObjectId()));
					}
					else
					{
						LOG.warn("Clan Hall ID NULL : {} Can be caused by concurent write in ClanHallManager", ItemId);
					}
					
					player.sendPacket(html);
					return;
				}
				else if ((player.getClan() != null) && (player.getClan().getHideoutId() == 0))
				{
					player.sendPacket(SystemMessageId.THERE_ARE_NO_OFFERINGS_I_OWN_OR_I_MADE_A_BID_FOR);
					return;
				}
				else if (player.getClan() == null)
				{
					player.sendPacket(SystemMessageId.YOU_DO_NOT_MEET_THE_REQUIREMENTS_TO_PARTICIPATE_IN_AN_AUCTION);
					return;
				}
			}
			else if (actualCommand.equalsIgnoreCase("cancelBid"))
			{
				var bid = AuctionManager.getInstance().getAuction(player.getClan().getAuctionBiddedAt()).getBidders().get(player.getClanId()).getBid();
				var html = new NpcHtmlMessage(getObjectId());
				html.setFile(player, "data/html/auction/AgitBidCancel.htm");
				html.replace("%AGIT_BID%", String.valueOf(bid));
				html.replace("%AGIT_BID_REMAIN%", String.valueOf((long) (bid * 0.9)));
				html.replace("%AGIT_LINK_BACK%", "bypass -h npc_" + getObjectId() + "_selectedItems");
				html.replace("%objectId%", String.valueOf(getObjectId()));
				player.sendPacket(html);
				return;
			}
			else if (actualCommand.equalsIgnoreCase("doCancelBid"))
			{
				if (AuctionManager.getInstance().getAuction(player.getClan().getAuctionBiddedAt()) != null)
				{
					AuctionManager.getInstance().getAuction(player.getClan().getAuctionBiddedAt()).cancelBid(player.getClanId());
					player.sendPacket(SystemMessageId.YOU_HAVE_CANCELED_YOUR_BID);
				}
				return;
			}
			else if (actualCommand.equalsIgnoreCase("cancelAuction"))
			{
				if (!player.hasClanPrivilege(ClanPrivilege.CH_AUCTION))
				{
					var html = new NpcHtmlMessage(getObjectId());
					html.setFile(player, "data/html/auction/not_authorized.htm");
					html.replace("%objectId%", String.valueOf(getObjectId()));
					player.sendPacket(html);
					return;
				}
				var html = new NpcHtmlMessage(getObjectId());
				html.setFile(player, "data/html/auction/AgitSaleCancel.htm");
				html.replace("%AGIT_DEPOSIT%", String.valueOf(ClanHallManager.getInstance().getClanHallByOwner(player.getClan()).getLease()));
				html.replace("%AGIT_LINK_BACK%", "bypass -h npc_" + getObjectId() + "_selectedItems");
				html.replace("%objectId%", String.valueOf(getObjectId()));
				player.sendPacket(html);
				return;
			}
			else if (actualCommand.equalsIgnoreCase("doCancelAuction"))
			{
				if (AuctionManager.getInstance().getAuction(player.getClan().getHideoutId()) != null)
				{
					AuctionManager.getInstance().getAuction(player.getClan().getHideoutId()).cancelAuction();
					player.sendMessage(MessagesData.getInstance().getMessage(player, "auction_canceled"));
				}
				return;
			}
			else if (actualCommand.equalsIgnoreCase("sale2"))
			{
				var html = new NpcHtmlMessage(getObjectId());
				html.setFile(player, "data/html/auction/AgitSale2.htm");
				html.replace("%AGIT_LAST_PRICE%", String.valueOf(ClanHallManager.getInstance().getClanHallByOwner(player.getClan()).getLease()));
				html.replace("%AGIT_LINK_BACK%", "bypass -h npc_" + getObjectId() + "_sale");
				html.replace("%objectId%", String.valueOf(getObjectId()));
				player.sendPacket(html);
				return;
			}
			else if (actualCommand.equalsIgnoreCase("sale"))
			{
				if (!player.hasClanPrivilege(ClanPrivilege.CH_AUCTION))
				{
					var html = new NpcHtmlMessage(getObjectId());
					html.setFile(player, "data/html/auction/not_authorized.htm");
					html.replace("%objectId%", String.valueOf(getObjectId()));
					player.sendPacket(html);
					return;
				}
				var html = new NpcHtmlMessage(getObjectId());
				html.setFile(player, "data/html/auction/AgitSale1.htm");
				html.replace("%AGIT_DEPOSIT%", String.valueOf(ClanHallManager.getInstance().getClanHallByOwner(player.getClan()).getLease()));
				html.replace("%AGIT_PLEDGE_ADENA%", String.valueOf(player.getClan().getWarehouse().getAdena()));
				html.replace("%AGIT_LINK_BACK%", "bypass -h npc_" + getObjectId() + "_selectedItems");
				html.replace("%objectId%", String.valueOf(getObjectId()));
				player.sendPacket(html);
				return;
			}
			else if (actualCommand.equalsIgnoreCase("rebid"))
			{
				if (!player.hasClanPrivilege(ClanPrivilege.CH_AUCTION))
				{
					var html = new NpcHtmlMessage(getObjectId());
					html.setFile(player, "data/html/auction/not_authorized.htm");
					html.replace("%objectId%", String.valueOf(getObjectId()));
					player.sendPacket(html);
					return;
				}
				try
				{
					var html = new NpcHtmlMessage(getObjectId());
					html.setFile(player, "data/html/auction/AgitBid2.htm");
					var a = AuctionManager.getInstance().getAuction(player.getClan().getAuctionBiddedAt());
					if (a != null)
					{
						html.replace("%AGIT_AUCTION_BID%", String.valueOf(a.getBidders().get(player.getClanId()).getBid()));
						html.replace("%AGIT_AUCTION_MINBID%", String.valueOf(a.getStartingBid()));
						html.replace("%AGIT_AUCTION_END%", String.valueOf(TimeUtils.DATE_TIME_FORMATTER_FIX.format(a.getEndDate())));
						html.replace("%AGIT_LINK_BACK%", "bypass -h npc_" + getObjectId() + "_selectedItems");
						html.replace("npc_%objectId%_bid1", "npc_" + getObjectId() + "_bid1 " + a.getId());
					}
					else
					{
						LOG.warn("Auctioneer Auction null for AuctionBiddedAt : {}", player.getClan().getAuctionBiddedAt());
					}
					player.sendPacket(html);
				}
				catch (Exception ex)
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "auction_invalid"));
				}
				return;
			}
			else if (actualCommand.equalsIgnoreCase("location"))
			{
				var html = new NpcHtmlMessage(getObjectId());
				html.setFile(player, "data/html/auction/location.htm");
				html.replace("%location%", MapRegionManager.getInstance().getClosestTownName(player));
				html.replace("%LOCATION%", getPictureName(player));
				html.replace("%AGIT_LINK_BACK%", "bypass -h npc_" + getObjectId() + "_start");
				player.sendPacket(html);
				return;
			}
			else if (actualCommand.equalsIgnoreCase("start"))
			{
				showChatWindow(player);
				return;
			}
		}
		super.onBypassFeedback(player, command);
	}
	
	@Override
	public void showChatWindow(L2PcInstance player)
	{
		var filename = "data/html/auction/auction-no.htm";
		
		var condition = validateCondition(player);
		if (condition == COND_BUSY_BECAUSE_OF_SIEGE)
		{
			filename = "data/html/auction/auction-busy.htm"; // Busy because of siege
		}
		else
		{
			filename = "data/html/auction/auction.htm";
		}
		
		var html = new NpcHtmlMessage(getObjectId());
		html.setFile(player, filename);
		html.replace("%objectId%", String.valueOf(getObjectId()));
		html.replace("%npcId%", String.valueOf(getId()));
		html.replace("%npcname%", getName());
		player.sendPacket(html);
	}
	
	private int validateCondition(L2PcInstance player)
	{
		if ((getCastle() != null) && (getCastle().getResidenceId() > 0))
		{
			if (getCastle().getSiege().isInProgress())
			{
				return COND_BUSY_BECAUSE_OF_SIEGE; // Busy because of siege
			}
			return COND_REGULAR;
		}
		return COND_ALL_FALSE;
	}
	
	private String getPictureName(L2PcInstance player)
	{
		var nearestTownId = MapRegionManager.getInstance().getMapRegionLocId(player);
		return switch (nearestTownId)
		{
			case 911 -> "GLUDIN";
			case 912 -> "GLUDIO";
			case 916 -> "DION";
			case 918 -> "GIRAN";
			case 1537 -> "RUNE";
			case 1538 -> "GODARD";
			case 1714 -> "SCHUTTGART";
			default -> "ADEN";
		};
	}
}