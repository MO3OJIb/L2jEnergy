/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.actor.instance;

import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.configuration.config.events.LotteryConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.InstanceType;
import com.l2jserver.gameserver.idfactory.IdFactory;
import com.l2jserver.gameserver.instancemanager.games.Lottery;
import com.l2jserver.gameserver.model.actor.templates.L2NpcTemplate;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ActionFailed;
import com.l2jserver.gameserver.network.serverpackets.InventoryUpdate;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * @author Мо3олЬ
 */
public class L2LotteryManagerInstance extends L2NpcInstance
{
	public L2LotteryManagerInstance(L2NpcTemplate template)
	{
		super(template);
		setInstanceType(InstanceType.L2LotteryManagerInstance);
	}
	
	@Override
	public void onBypassFeedback(final L2PcInstance player, final String command)
	{
		if (command.startsWith("Loto"))
		{
			int val = 0;
			try
			{
				val = Integer.parseInt(command.substring(5));
			}
			catch (IndexOutOfBoundsException ioobe)
			{
			}
			catch (NumberFormatException nfe)
			{
			}
			
			if (val == 0)
			{
				// new loto ticket
				for (int i = 0; i < 5; i++)
				{
					player.setLoto(i, 0);
				}
			}
			showLotoWindow(player, val);
		}
		else
		{
			super.onBypassFeedback(player, command);
		}
	}
	
	public void showLotoWindow(final L2PcInstance player, final int val)
	{
		int npcId = getTemplate().getId();
		String filename;
		SystemMessage sm;
		var html = new NpcHtmlMessage(getObjectId());
		
		if (val == 0) // 0 - first buy lottery ticket window
		{
			filename = getHtmlPath(npcId, 1);
			html.setFile(player, filename);
		}
		else if ((val >= 1) && (val <= 21)) // 1-20 - buttons, 21 - second buy lottery ticket window
		{
			if (!Lottery.getInstance().isStarted())
			{
				// tickets can't be sold
				player.sendPacket(SystemMessageId.LOTTERY_TICKETS_ARE_NOT_CURRENTLY_BEING_SOLD);
				return;
			}
			if (!Lottery.getInstance().isSellableTickets())
			{
				// tickets can't be sold
				player.sendPacket(SystemMessageId.TICKETS_FOR_THE_CURRENT_LOTTERY_ARE_NO_LONGER_AVAILABLE);
				return;
			}
			
			filename = getHtmlPath(npcId, 5);
			html.setFile(player, filename);
			
			int count = 0;
			int found = 0;
			// counting buttons and unsetting button if found
			for (int i = 0; i < 5; i++)
			{
				if (player.getLoto(i) == val)
				{
					// unsetting button
					player.setLoto(i, 0);
					found = 1;
				}
				else if (player.getLoto(i) > 0)
				{
					count++;
				}
			}
			
			// if not rearched limit 5 and not unseted value
			if ((count < 5) && (found == 0) && (val <= 20))
			{
				for (int i = 0; i < 5; i++)
				{
					if (player.getLoto(i) == 0)
					{
						player.setLoto(i, val);
						break;
					}
				}
			}
			
			// setting pusshed buttons
			count = 0;
			for (int i = 0; i < 5; i++)
			{
				if (player.getLoto(i) > 0)
				{
					count++;
					var button = String.valueOf(player.getLoto(i));
					if (player.getLoto(i) < 10)
					{
						button = "0" + button;
					}
					String search = "fore=\"L2UI.lottoNum" + button + "\" back=\"L2UI.lottoNum" + button + "a_check\"";
					String replace = "fore=\"L2UI.lottoNum" + button + "a_check\" back=\"L2UI.lottoNum" + button + "\"";
					html.replace(search, replace);
				}
			}
			
			if (count == 5)
			{
				var search = "0\">" + MessagesData.getInstance().getMessage(player, "html_return") + "";
				var replace = "22\">" + MessagesData.getInstance().getMessage(player, "lottery_you_lucky_numbers_have") + "";
				html.replace(search, replace);
			}
		}
		else if (val == 22) // 22 - selected ticket with 5 numbers
		{
			if (!Lottery.getInstance().isStarted())
			{
				// tickets can't be sold
				player.sendPacket(SystemMessageId.LOTTERY_TICKETS_ARE_NOT_CURRENTLY_BEING_SOLD);
				return;
			}
			if (!Lottery.getInstance().isSellableTickets())
			{
				// tickets can't be sold
				player.sendPacket(SystemMessageId.TICKETS_FOR_THE_CURRENT_LOTTERY_ARE_NO_LONGER_AVAILABLE);
				return;
			}
			
			long price = LotteryConfig.ALT_LOTTERY_TICKET_PRICE;
			int lotonumber = Lottery.getInstance().getId();
			int enchant = 0;
			int type2 = 0;
			
			for (var i = 0; i < 5; i++)
			{
				if (player.getLoto(i) == 0)
				{
					return;
				}
				
				if (player.getLoto(i) < 17)
				{
					enchant += Math.pow(2, player.getLoto(i) - 1);
				}
				else
				{
					type2 += Math.pow(2, player.getLoto(i) - 17);
				}
			}
			if (player.getAdena() < price)
			{
				player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.YOU_NOT_ENOUGH_ADENA));
				return;
			}
			if (!player.reduceAdena("Loto", price, null, true))
			{
				return;
			}
			Lottery.getInstance().increasePrize(price);
			
			sm = SystemMessage.getSystemMessage(SystemMessageId.YOU_HAVE_EARNED_S1);
			sm.addItemName(4442);
			player.sendPacket(sm);
			
			var item = new L2ItemInstance(IdFactory.getInstance().getNextId(), 4442);
			item.setCount(1);
			item.setCustomType1(lotonumber);
			item.setEnchantLevel(enchant);
			item.setCustomType2(type2);
			player.getInventory().addItem("Loto", item, player, null);
			
			var iu = new InventoryUpdate();
			iu.addItem(item);
			var adenaupdate = player.getInventory().getItemByItemId(57);
			if (adenaupdate != null)
			{
				iu.addModifiedItem(adenaupdate);
			}
			player.sendPacket(iu);
			
			// filename = (npc.getHtmlPath(npcId, 6));
			filename = getHtmlPath(npcId, 3);
			html.setFile(player, filename);
		}
		else if (val == 23) // 23 - current lottery jackpot
		{
			filename = getHtmlPath(npcId, 3);
			html.setFile(player, filename);
		}
		else if (val == 24) // 24 - Previous winning numbers/Prize claim
		{
			filename = getHtmlPath(npcId, 4);
			html.setFile(player, filename);
			
			var lotonumber = Lottery.getInstance().getId();
			var message = "";
			for (var item : player.getInventory().getItems())
			{
				if (item == null)
				{
					continue;
				}
				if ((item.getId() == 4442) && (item.getCustomType1() < lotonumber))
				{
					message = message + "<a action=\"bypass -h npc_%objectId%_Loto " + item.getObjectId() + "\">" + item.getCustomType1() + "" + MessagesData.getInstance().getMessage(player, "lottery_ticket_number") + "";
					
					var numbers = Lottery.getInstance().decodeNumbers(item.getEnchantLevel(), item.getCustomType2());
					for (var i = 0; i < 5; i++)
					{
						message += numbers[i] + " ";
					}
					var check = Lottery.getInstance().checkTicket(item);
					if (check[0] > 0)
					{
						switch ((int) check[0])
						{
							case 1:
								message += "" + MessagesData.getInstance().getMessage(player, "lottery_ticket_1_prize") + "";
								break;
							case 2:
								message += "" + MessagesData.getInstance().getMessage(player, "lottery_ticket_2_prize") + "";
								break;
							case 3:
								message += "" + MessagesData.getInstance().getMessage(player, "lottery_ticket_3_prize") + "";
								break;
							case 4:
								message += "" + MessagesData.getInstance().getMessage(player, "lottery_ticket_4_prize") + "";
								break;
						}
						message += " " + check[1] + " a.";
					}
					message += "</a><br>";
				}
			}
			if (message.isEmpty())
			{
				message += "" + MessagesData.getInstance().getMessage(player, "lottery_ticket_no_winning") + "<br>";
			}
			html.replace("%result%", message);
		}
		else if (val == 25) // 25 - lottery instructions
		{
			filename = getHtmlPath(npcId, 2);
			html.setFile(player, filename);
		}
		else if (val > 25) // >25 - check lottery ticket by item object id
		{
			var lotonumber = Lottery.getInstance().getId();
			var item = player.getInventory().getItemByObjectId(val);
			if ((item == null) || (item.getId() != 4442) || (item.getCustomType1() >= lotonumber))
			{
				return;
			}
			var check = Lottery.getInstance().checkTicket(item);
			
			sm = SystemMessage.getSystemMessage(SystemMessageId.S1_DISAPPEARED);
			sm.addItemName(4442);
			player.sendPacket(sm);
			
			var adena = check[1];
			if (adena > 0)
			{
				player.addAdena("Loto", adena, null, true);
			}
			player.destroyItem("Loto", item, null, false);
			return;
		}
		html.replace("%objectId%", String.valueOf(getObjectId()));
		html.replace("%race%", "" + Lottery.getInstance().getId());
		html.replace("%adena%", "" + Lottery.getInstance().getPrize());
		html.replace("%ticket_price%", "" + LotteryConfig.ALT_LOTTERY_TICKET_PRICE);
		html.replace("%prize5%", "" + (LotteryConfig.ALT_LOTTERY_5_NUMBER_RATE * 100));
		html.replace("%prize4%", "" + (LotteryConfig.ALT_LOTTERY_4_NUMBER_RATE * 100));
		html.replace("%prize3%", "" + (LotteryConfig.ALT_LOTTERY_3_NUMBER_RATE * 100));
		html.replace("%prize2%", "" + LotteryConfig.ALT_LOTTERY_2_AND_1_NUMBER_PRIZE);
		var instant = Lottery.getInstance().getEndDate();
		html.replace("%enddate%", "" + TimeUtils.dateTimeFormat(instant));
		player.sendPacket(html);
		
		// Send a Server->Client ActionFailed to the L2PcInstance in order to avoid that the client wait another packet
		player.sendPacket(ActionFailed.STATIC_PACKET);
	}
	
	@Override
	public String getHtmlPath(final int npcId, final int val)
	{
		var pom = "";
		if (val == 0)
		{
			pom = "LotteryManager";
		}
		else
		{
			pom = "LotteryManager-" + val;
		}
		return "data/html/lottery/" + pom + ".htm";
	}
}
