/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.actor.tasks.player;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.UserInfo;

public class RemoveWearItemsTask implements Runnable
{
	private static final Logger LOG = LoggerFactory.getLogger(RemoveWearItemsTask.class);
	
	private final L2PcInstance _player;
	
	public RemoveWearItemsTask(L2PcInstance player)
	{
		Objects.requireNonNull(player);
		_player = player;
	}
	
	@Override
	public void run()
	{
		try
		{
			_player.sendPacket(SystemMessageId.YOU_ARE_NO_LONGER_TRYING_ON_EQUIPMENT);
			_player.sendPacket(new UserInfo(_player));
		}
		catch (Exception ex)
		{
			LOG.error("RemoveWearItemsTask for player {}", _player.getName(), ex);
		}
	}
}
