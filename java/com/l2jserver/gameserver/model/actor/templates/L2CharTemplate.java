/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.actor.templates;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import com.l2jserver.gameserver.enums.actors.ClassRace;
import com.l2jserver.gameserver.enums.actors.MoveType;
import com.l2jserver.gameserver.enums.items.WeaponType;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.events.ListenersContainer;
import com.l2jserver.gameserver.model.skills.Skill;

/**
 * Character template.
 * @author Zoey76
 */
public class L2CharTemplate extends ListenersContainer
{
	// BaseStats
	private int _baseSTR;
	private int _baseCON;
	private int _baseDEX;
	private int _baseINT;
	private int _baseWIT;
	private int _baseMEN;
	private float _baseHpMax;
	private float _baseCpMax;
	private float _baseMpMax;
	private float _baseHpReg;
	private float _baseMpReg;
	private int _basePAtk;
	private int _baseMAtk;
	private int _basePDef;
	private int _baseMDef;
	private int _basePAtkSpd;
	private int _baseMAtkSpd;
	private int _baseAttackRange;
	private int _randomDamage;
	private WeaponType _baseAttackType;
	private int _baseShldDef;
	private int _baseShldRate;
	private int _baseCritRate;
	private int _baseMCritRate;
	private int _baseReuseDelay;
	// SpecialStats
	private int _baseBreath;
	private int _baseFire;
	private int _baseWind;
	private int _baseWater;
	private int _baseEarth;
	private int _baseHoly;
	private int _baseDark;
	private double _baseFireRes;
	private double _baseWindRes;
	private double _baseWaterRes;
	private double _baseEarthRes;
	private double _baseHolyRes;
	private double _baseDarkRes;
	private double _baseElementRes;
	/** For client info use {@link #_fCollisionRadius} */
	private int _collisionRadius;
	/** For client info use {@link #_fCollisionHeight} */
	private int _collisionHeight;
	
	private double _fCollisionRadius;
	private double _fCollisionHeight;
	
	private final double[] _moveType = new double[MoveType.values().length];
	/** The creature's race. */
	private ClassRace _race;
	
	public L2CharTemplate(StatsSet set)
	{
		set(set);
	}
	
	public void set(StatsSet set)
	{
		// Base stats
		_baseSTR = set.getInt("baseSTR", 0);
		_baseCON = set.getInt("baseCON", 0);
		_baseDEX = set.getInt("baseDEX", 0);
		_baseINT = set.getInt("baseINT", 0);
		_baseWIT = set.getInt("baseWIT", 0);
		_baseMEN = set.getInt("baseMEN", 0);
		_baseHpMax = set.getFloat("baseHpMax", 0);
		_baseCpMax = set.getFloat("baseCpMax", 0);
		_baseMpMax = set.getFloat("baseMpMax", 0);
		_baseHpReg = set.getFloat("baseHpReg", 0);
		_baseMpReg = set.getFloat("baseMpReg", 0);
		_basePAtk = set.getInt("basePAtk", 0);
		_baseMAtk = set.getInt("baseMAtk", 0);
		_basePDef = set.getInt("basePDef", 0);
		_baseMDef = set.getInt("baseMDef", 0);
		_basePAtkSpd = set.getInt("basePAtkSpd", 300);
		_baseMAtkSpd = set.getInt("baseMAtkSpd", 333);
		_baseShldDef = set.getInt("baseShldDef", 0);
		_baseAttackRange = set.getInt("baseAtkRange", 40);
		_randomDamage = set.getInt("baseRndDam", 0);
		_baseAttackType = set.getEnum("baseAtkType", WeaponType.class, WeaponType.FIST);
		_baseShldRate = set.getInt("baseShldRate", 0);
		_baseCritRate = set.getInt("baseCritRate", 4);
		_baseMCritRate = set.getInt("baseMCritRate", 0);
		_baseReuseDelay = set.getInt("reuseDelay", 0);
		// SpecialStats
		_baseBreath = set.getInt("baseBreath", 100);
		_baseFire = set.getInt("baseFire", 0);
		_baseWind = set.getInt("baseWind", 0);
		_baseWater = set.getInt("baseWater", 0);
		_baseEarth = set.getInt("baseEarth", 0);
		_baseHoly = set.getInt("baseHoly", 0);
		_baseDark = set.getInt("baseDark", 0);
		_baseFireRes = set.getInt("baseFireRes", 0);
		_baseWindRes = set.getInt("baseWindRes", 0);
		_baseWaterRes = set.getInt("baseWaterRes", 0);
		_baseEarthRes = set.getInt("baseEarthRes", 0);
		_baseHolyRes = set.getInt("baseHolyRes", 0);
		_baseDarkRes = set.getInt("baseDarkRes", 0);
		_baseElementRes = set.getInt("baseElementRes", 0);
		
		// Geometry
		_fCollisionHeight = set.getDouble("collisionHeight", 0);
		_fCollisionRadius = set.getDouble("collisionRadius", 0);
		_collisionRadius = (int) _fCollisionRadius;
		_collisionHeight = (int) _fCollisionHeight;
		
		// speed.
		Arrays.fill(_moveType, 1);
		setBaseMoveSpeed(MoveType.RUN, set.getDouble("baseRunSpd", 120));
		setBaseMoveSpeed(MoveType.WALK, set.getDouble("baseWalkSpd", 50));
		setBaseMoveSpeed(MoveType.FAST_SWIM, set.getDouble("baseSwimRunSpd", getBaseMoveSpeed(MoveType.RUN)));
		setBaseMoveSpeed(MoveType.SLOW_SWIM, set.getDouble("baseSwimWalkSpd", getBaseMoveSpeed(MoveType.WALK)));
		setBaseMoveSpeed(MoveType.FAST_FLY, set.getDouble("baseFlyRunSpd", getBaseMoveSpeed(MoveType.RUN)));
		setBaseMoveSpeed(MoveType.SLOW_FLY, set.getDouble("baseFlyWalkSpd", getBaseMoveSpeed(MoveType.WALK)));
	}
	
	public float getBaseHpMax()
	{
		return _baseHpMax;
	}
	
	public int getBaseFire()
	{
		return _baseFire;
	}
	
	public int getBaseWind()
	{
		return _baseWind;
	}
	
	public int getBaseWater()
	{
		return _baseWater;
	}
	
	public int getBaseEarth()
	{
		return _baseEarth;
	}
	
	public int getBaseHoly()
	{
		return _baseHoly;
	}
	
	public int getBaseDark()
	{
		return _baseDark;
	}
	
	public double getBaseFireRes()
	{
		return _baseFireRes;
	}
	
	public double getBaseWindRes()
	{
		return _baseWindRes;
	}
	
	public double getBaseWaterRes()
	{
		return _baseWaterRes;
	}
	
	public double getBaseEarthRes()
	{
		return _baseEarthRes;
	}
	
	public double getBaseHolyRes()
	{
		return _baseHolyRes;
	}
	
	public double getBaseDarkRes()
	{
		return _baseDarkRes;
	}
	
	public double getBaseElementRes()
	{
		return _baseElementRes;
	}
	
	public int getBaseSTR()
	{
		return _baseSTR;
	}
	
	public int getBaseCON()
	{
		return _baseCON;
	}
	
	public int getBaseDEX()
	{
		return _baseDEX;
	}
	
	public int getBaseINT()
	{
		return _baseINT;
	}
	
	public int getBaseWIT()
	{
		return _baseWIT;
	}
	
	public int getBaseMEN()
	{
		return _baseMEN;
	}
	
	public float getBaseCpMax()
	{
		return _baseCpMax;
	}
	
	public float getBaseMpMax()
	{
		return _baseMpMax;
	}
	
	public float getBaseHpReg()
	{
		return _baseHpReg;
	}
	
	public float getBaseMpReg()
	{
		return _baseMpReg;
	}
	
	public int getBasePAtk()
	{
		return _basePAtk;
	}
	
	public int getBaseMAtk()
	{
		return _baseMAtk;
	}
	
	public int getBasePDef()
	{
		return _basePDef;
	}
	
	public int getBaseMDef()
	{
		return _baseMDef;
	}
	
	public int getBasePAtkSpd()
	{
		return _basePAtkSpd;
	}
	
	public int getBaseMAtkSpd()
	{
		return _baseMAtkSpd;
	}
	
	public int getRandomDamage()
	{
		return _randomDamage;
	}
	
	public int getBaseShldDef()
	{
		return _baseShldDef;
	}
	
	public int getBaseShldRate()
	{
		return _baseShldRate;
	}
	
	public int getBaseCritRate()
	{
		return _baseCritRate;
	}
	
	public int getBaseMCritRate()
	{
		return _baseMCritRate;
	}
	
	public int getBaseReuseDelay()
	{
		return _baseReuseDelay;
	}
	
	public void setBaseMoveSpeed(MoveType type, double val)
	{
		_moveType[type.ordinal()] = val;
	}
	
	public double getBaseMoveSpeed(MoveType mt)
	{
		return _moveType[mt.ordinal()];
	}
	
	public int getBaseBreath()
	{
		return _baseBreath;
	}
	
	public int getCollisionRadius()
	{
		return _collisionRadius;
	}
	
	public int getCollisionHeight()
	{
		return _collisionHeight;
	}
	
	public double getfCollisionRadius()
	{
		return _fCollisionRadius;
	}
	
	public double getfCollisionHeight()
	{
		return _fCollisionHeight;
	}
	
	public void setBaseFire(int baseFire)
	{
		_baseFire = baseFire;
	}
	
	public void setBaseWater(int baseWater)
	{
		_baseWater = baseWater;
	}
	
	public void setBaseEarth(int baseEarth)
	{
		_baseEarth = baseEarth;
	}
	
	public void setBaseWind(int baseWind)
	{
		_baseWind = baseWind;
	}
	
	public void setBaseHoly(int baseHoly)
	{
		_baseHoly = baseHoly;
	}
	
	public void setBaseDark(int baseDark)
	{
		_baseDark = baseDark;
	}
	
	public void setBaseFireRes(double baseFireRes)
	{
		_baseFireRes = baseFireRes;
	}
	
	public void setBaseWaterRes(double baseWaterRes)
	{
		_baseWaterRes = baseWaterRes;
	}
	
	public void setBaseEarthRes(double baseEarthRes)
	{
		_baseEarthRes = baseEarthRes;
	}
	
	public void setBaseWindRes(double baseWindRes)
	{
		_baseWindRes = baseWindRes;
	}
	
	public void setBaseHolyRes(double baseHolyRes)
	{
		_baseHolyRes = baseHolyRes;
	}
	
	public void setBaseDarkRes(double baseDarkRes)
	{
		_baseDarkRes = baseDarkRes;
	}
	
	public void setBaseElementRes(double baseElementRes)
	{
		_baseElementRes = baseElementRes;
	}
	
	public WeaponType getBaseAttackType()
	{
		return _baseAttackType;
	}
	
	public void setBaseAttackType(WeaponType type)
	{
		_baseAttackType = type;
	}
	
	public int getBaseAttackRange()
	{
		return _baseAttackRange;
	}
	
	public void setBaseAttackRange(int val)
	{
		_baseAttackRange = val;
	}
	
	public Map<Integer, Skill> getSkills()
	{
		return Collections.emptyMap();
	}
	
	public ClassRace getRace()
	{
		return _race;
	}
	
	public void setRace(ClassRace race)
	{
		_race = race;
	}
}
