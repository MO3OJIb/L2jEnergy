/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.agathion;

/**
 * Agathion.
 * @author Zoey76
 */
public class Agathion
{
	private final int _npcId;
	private final int _id;
	private final int _itemId;
	private final int _energy;
	private final int _maxEnergy;
	
	public Agathion(int npcId, int id, int itemId, int energy, int maxEnergy)
	{
		_npcId = npcId;
		_id = id;
		_itemId = itemId;
		_energy = energy;
		_maxEnergy = maxEnergy;
	}
	
	public int getNpcId()
	{
		return _npcId;
	}
	
	public int getId()
	{
		return _id;
	}
	
	public int getItemId()
	{
		return _itemId;
	}
	
	public int getEnergy()
	{
		return _energy;
	}
	
	public int getMaxEnergy()
	{
		return _maxEnergy;
	}
}
