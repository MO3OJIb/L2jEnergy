/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.announce;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.enums.AnnouncementType;

/**
 * @author UnAfraid
 */
public class Announcement implements IAnnouncement
{
	protected int _id;
	private AnnouncementType _type;
	private String _content;
	private String _author;
	
	public Announcement(AnnouncementType type, String content, String author)
	{
		_type = type;
		_content = content;
		_author = author;
	}
	
	public Announcement(ResultSet rset) throws SQLException
	{
		_id = rset.getInt("id");
		_type = AnnouncementType.findById(rset.getInt("type"));
		_content = rset.getString("content");
		_author = rset.getString("author");
	}
	
	@Override
	public int getId()
	{
		return _id;
	}
	
	@Override
	public AnnouncementType getType()
	{
		return _type;
	}
	
	@Override
	public void setType(AnnouncementType type)
	{
		_type = type;
	}
	
	@Override
	public String getContent()
	{
		return _content;
	}
	
	@Override
	public void setContent(String content)
	{
		_content = content;
	}
	
	@Override
	public String getAuthor()
	{
		return _author;
	}
	
	@Override
	public void setAuthor(String author)
	{
		_author = author;
	}
	
	@Override
	public boolean isValid()
	{
		return true;
	}
	
	@Override
	public boolean storeMe()
	{
		DAOFactory.getInstance().getAnnounceDAO().storeMe(_type, _author, _author, _id);
		return true;
	}
	
	@Override
	public boolean updateMe()
	{
		DAOFactory.getInstance().getAnnounceDAO().updateMe(_type, _author, _author, _id);
		return true;
	}
	
	@Override
	public boolean deleteMe()
	{
		DAOFactory.getInstance().getAnnounceDAO().deleteMe(_id);
		return true;
	}
}
