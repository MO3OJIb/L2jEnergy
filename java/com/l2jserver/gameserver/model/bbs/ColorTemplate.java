/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.bbs;

import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * @author Mangol
 */
public class ColorTemplate
{
	private final String _color;
	private final String _nameEn;
	private final String _nameRu;
	
	public ColorTemplate(final String color, final String nameEn, final String nameRu)
	{
		_color = color;
		_nameEn = nameEn;
		_nameRu = nameRu;
	}
	
	public String getColor()
	{
		return _color;
	}
	
	public String getNameEn()
	{
		return _nameEn;
	}
	
	public String getNameRu()
	{
		return _nameRu;
	}
	
	public String getName(final L2PcInstance player)
	{
		switch (player.getLanguage())
		{
			case RUSSIAN:
				return getNameRu();
			case ENGLISH:
				return getNameEn();
		}
		return "No Name";
	}
}
