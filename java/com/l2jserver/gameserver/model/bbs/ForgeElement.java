/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.bbs;

import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.configuration.config.bbs.CBasicConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.client.Language;
import com.l2jserver.gameserver.enums.items.EtcItemType;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;

public class ForgeElement
{
	public static String[] generateEnchant(L2ItemInstance item, int max, int slot, Language lang)
	{
		var data = new String[4];
		
		var noicon = "icon.NOIMAGE";
		var slotclose = "L2UI_CT1.ItemWindow_DF_SlotBox_Disable";
		var dot = "<font color=\"FF0000\">...</font>";
		var maxenchant = "<font color=FF0000>" + MessagesData.getInstance().getMessage("html_enchant_max", lang) + "</font>";
		var picenchant = "l2ui_ch3.multisell_plusicon";
		var pvp = "icon.pvp_tab";
		
		if (item != null)
		{
			data[0] = item.getItem().getIcon();
			data[1] = item.getName() + " " + (item.getEnchantLevel() > 0 ? "+" + item.getEnchantLevel() : "");
			if (!(item.getItem().getItemType() == EtcItemType.ARROW))
			{
				if ((item.getEnchantLevel() >= max))
				{
					data[2] = maxenchant;
					data[3] = slotclose;
				}
				else
				{
					data[2] = "<button action=\"bypass _bbsforge:enchant:item:" + slot + "\" value=\"" + MessagesData.getInstance().getMessage("button_enchant", lang) + "\"width=120 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\">";
					
					if (item.isPvp())
					{
						data[3] = pvp;
					}
					else
					{
						data[3] = picenchant;
					}
				}
			}
			else
			{
				data[2] = dot;
				data[3] = slotclose;
			}
		}
		else
		{
			data[0] = noicon;
			data[1] = MessagesData.getInstance().getMessage("communityboard_forge_item_not_clothed_" + slot + "", lang);
			data[2] = dot;
			data[3] = slotclose;
		}
		return data;
	}
	
	public static String page(L2PcInstance player)
	{
		var customPath = CBasicConfig.CUSTOM_BBS_ENABLED ? "Custom/" : "";
		return HtmCache.getInstance().getHtm(player, "data/html/CommunityBoard/" + customPath + "forge/page_template.html");
	}
}
