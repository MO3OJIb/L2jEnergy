/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.bbs;

import com.l2jserver.gameserver.configuration.config.bbs.CStatsConfig;

/**
 * @author Мо3олЬ
 */
public class Rating
{
	public static int Hero = 0;
	public static int Noble = 0;
	public static int Account = 0;
	public static int Players = 0;
	public static int Clan = 0;
	public static int Ally = 0;
	
	public static int Human = 0;
	public static int Elf = 0;
	public static int DarkElf = 0;
	public static int Orc = 0;
	public static int Dwarf = 0;
	public static int Kamael = 0;
	
	// TOP_PVP
	private final static String[] _topPvPName = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static String[] _topPvPClan = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPvPSex = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPvPClass = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPvPOn = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPvPOnline = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPvP = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPvPPvP = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	
	// TOP_PK
	private final static String[] _topPkName = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static String[] _topPkClan = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPkSex = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPkClass = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPkOn = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPkOnline = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPk = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPkPvP = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	
	// TOP_ONLINE
	private final static String[] _topOnlineName = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static String[] _topOnlineClan = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topOnlineSex = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topOnlineClass = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topOnlineOn = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topOnlineOnline = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topOnline = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topOnlinePvP = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	
	// TOP_PC_CAFE_POINTS
	private final static String[] _topPcCafeName = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static String[] _topPcCafeClan = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPcCafeSex = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPcCafeClass = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPcCafeOn = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPcCafeOnline = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topPcCafeCount = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	
	// TOP_CLAN
	private final static String[] _clanName = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static String[] _allyName = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static String[] _clanLeader = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _clanlvl = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _clanReputation = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static String[] _clanCastleName = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static String[] _allyStatus = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	
	// RAID_STATUS
	private final static String[] _raidNpcName = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static boolean[] _raidStatus = new boolean[CStatsConfig.BBS_STATISTIC_COUNT];
	
	// CASTLE_STATUS
	private final static int[] _castleId = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static String[] _castleName = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static String[] _castlePercent = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static String[] _castleSiegeDate = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	
	// ========================== TODO: need test and fix
	// OLYMPIAD POINTS LIST
	private final static String[] _topOlympiadName = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static String[] _topOlympiadClan = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topOlympiadOn = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topOlympiadSex = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topOlympiadClass = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topOlympiadWin = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topOlympiadDone = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	
	// TOP_RICH
	private final static String[] _topRichName = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static String[] _topRichClan = new String[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topRichSex = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topRichClass = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topRichOn = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topRichOnline = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	private final static int[] _topRichCount = new int[CStatsConfig.BBS_STATISTIC_COUNT];
	
	public static String[] getTopPvPName()
	{
		return _topPvPName;
	}
	
	public static String[] getTopPvPClan()
	{
		return _topPvPClan;
	}
	
	public static int[] getTopPvPSex()
	{
		return _topPvPSex;
	}
	
	public static int[] getTopPvPClass()
	{
		return _topPvPClass;
	}
	
	public static int[] getTopPvPOn()
	{
		return _topPvPOn;
	}
	
	public static int[] getTopPvPOnline()
	{
		return _topPvPOnline;
	}
	
	public static int[] getTopPvP()
	{
		return _topPvP;
	}
	
	public static int[] getTopPvPPvP()
	{
		return _topPvPPvP;
	}
	
	public static String[] getTopPkName()
	{
		return _topPkName;
	}
	
	public static String[] getTopPkClan()
	{
		return _topPkClan;
	}
	
	public static int[] getTopPkSex()
	{
		return _topPkSex;
	}
	
	public static int[] getTopPkClass()
	{
		return _topPkClass;
	}
	
	public static int[] getTopPkOn()
	{
		return _topPkOn;
	}
	
	public static int[] getTopPkOnline()
	{
		return _topPkOnline;
	}
	
	public static int[] getTopPk()
	{
		return _topPk;
	}
	
	public static int[] getTopPkPvP()
	{
		return _topPkPvP;
	}
	
	public static String[] getTopOnlineName()
	{
		return _topOnlineName;
	}
	
	public static String[] getTopOnlineClan()
	{
		return _topOnlineClan;
	}
	
	public static int[] getTopOnlineSex()
	{
		return _topOnlineSex;
	}
	
	public static int[] getTopOnlineClass()
	{
		return _topOnlineClass;
	}
	
	public static int[] getTopOnlineOn()
	{
		return _topOnlineOn;
	}
	
	public static int[] getTopOnlineOnline()
	{
		return _topOnlineOnline;
	}
	
	public static int[] getTopOnline()
	{
		return _topOnline;
	}
	
	public static int[] getTopOnlinePvP()
	{
		return _topOnlinePvP;
	}
	
	public static String[] getTopOlympiadName()
	{
		return _topOlympiadName;
	}
	
	public static String[] getTopOlympiadClan()
	{
		return _topOlympiadClan;
	}
	
	public static int[] getTopOlympiadOn()
	{
		return _topOlympiadOn;
	}
	
	public static int[] getTopOlympiadSex()
	{
		return _topOlympiadSex;
	}
	
	public static int[] getTopOlympiadClass()
	{
		return _topOlympiadClass;
	}
	
	public static int[] getTopOlympiadWin()
	{
		return _topOlympiadWin;
	}
	
	public static int[] getTopOlympiadDone()
	{
		return _topOlympiadDone;
	}
	
	public static String[] getRaidNpcName()
	{
		return _raidNpcName;
	}
	
	public static boolean[] getRaidStatus()
	{
		return _raidStatus;
	}
	
	public static int[] getCastleId()
	{
		return _castleId;
	}
	
	public static String[] getCastleName()
	{
		return _castleName;
	}
	
	public static String[] getCastlePercent()
	{
		return _castlePercent;
	}
	
	public static String[] getCastleSiegeDate()
	{
		return _castleSiegeDate;
	}
	
	public static String[] getTopPcCafeName()
	{
		return _topPcCafeName;
	}
	
	public static String[] getTopPcCafeClan()
	{
		return _topPcCafeClan;
	}
	
	public static int[] getTopPcCafeSex()
	{
		return _topPcCafeSex;
	}
	
	public static int[] getTopPcCafeClass()
	{
		return _topPcCafeClass;
	}
	
	public static int[] getTopPcCafeOn()
	{
		return _topPcCafeOn;
	}
	
	public static int[] getTopPcCafeOnline()
	{
		return _topPcCafeOnline;
	}
	
	public static int[] getTopPcCafeCount()
	{
		return _topPcCafeCount;
	}
	
	public static String[] getTopRichName()
	{
		return _topRichName;
	}
	
	public static String[] getTopRichClan()
	{
		return _topRichClan;
	}
	
	public static int[] getTopRichSex()
	{
		return _topRichSex;
	}
	
	public static int[] getTopRichClass()
	{
		return _topRichClass;
	}
	
	public static int[] getTopRichOn()
	{
		return _topRichOn;
	}
	
	public static int[] getTopRichOnline()
	{
		return _topRichOnline;
	}
	
	public static int[] getTopRichCount()
	{
		return _topRichCount;
	}
	
	public static String[] getClanName()
	{
		return _clanName;
	}
	
	public static String[] getAllyName()
	{
		return _allyName;
	}
	
	public static String[] getClanLeader()
	{
		return _clanLeader;
	}
	
	public static int[] getClanLvl()
	{
		return _clanlvl;
	}
	
	public static int[] getClanReputation()
	{
		return _clanReputation;
	}
	
	public static String[] getClanCastleName()
	{
		return _clanCastleName;
	}
	
	public static String[] getAllyStatus()
	{
		return _allyStatus;
	}
}
