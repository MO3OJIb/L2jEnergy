/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.bbs;

import java.util.HashMap;
import java.util.Map;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.LoginServerThread;
import com.l2jserver.gameserver.configuration.config.protection.SecuritySettingConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.MagicSkillUse;

public class Security
{
	private static Map<Integer, Long> bind_ip = new HashMap<>();
	private static Map<Integer, Long> bind_hwid = new HashMap<>();
	
	public static String check(L2PcInstance player, boolean ip_bind, boolean hwid_bind, boolean ip, boolean hwid)
	{
		var allow_ip = "";
		var allow_hwid = "";
		var result = "...";
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("SELECT * FROM accounts WHERE login=?"))
		{
			ps.setString(1, player.getAccountName());
			
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					allow_hwid = rs.getString("allow_hwid");
					allow_ip = rs.getString("allow_ip");
				}
			}
		}
		catch (Exception ex)
		{
		}
		
		var IP = allow_ip.equals("") || allow_ip.equals("NoGuard");
		var HWID = allow_hwid.equals("") || allow_hwid.equals("NoGuard");
		
		if (ip)
		{
			result = IP ? "<font color=\"FF0000\">No</font>" : "<font color=\"18FF00\">Yes</font>";
		}
		else if (hwid)
		{
			result = HWID ? "<font color=\"FF0000\">No</font>" : "<font color=\"18FF00\">Yes</font>";
		}
		else if (ip_bind)
		{
			result = IP ? "bypass _bbscabinet:security:lockip" : "bypass _bbscabinet:security:unlockip";
		}
		else if (hwid_bind)
		{
			result = HWID ? "bypass _bbscabinet:security:lockhwid" : "bypass _bbscabinet:security:unlockhwid";
		}
		return result;
	}
	
	public static void lock(L2PcInstance player, boolean ip, boolean hwid)
	{
		if (ip)
		{
			if (!SecuritySettingConfig.ALLOW_LOCK_IP)
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
				return;
			}
			
			if (bind_ip.containsKey(player.getObjectId()))
			{
				var time = (int) ((bind_ip.get(player.getObjectId()) - System.currentTimeMillis()) / 1000);
				if (time > 0)
				{
					player.sendMessage("Wait " + time + " seconds for use this function.");
					return;
				}
			}
			
			bind_ip.put(player.getObjectId(), System.currentTimeMillis() + 60000);
			LoginServerThread.getInstance().sendAllowedIP(player.getAccountName(), player.getIPAddress());
			player.sendMessage("Access to your account is available now only from IP: " + player.getIPAddress());
			player.broadcastPacket(new MagicSkillUse(player, player, 5662, 1, 0, 0));
			return;
		}
		else if (hwid)
		{
			if (!SecuritySettingConfig.ALLOW_LOCK_HWID)
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
				return;
			}
			
			if (bind_hwid.containsKey(player.getObjectId()))
			{
				int time = (int) ((bind_hwid.get(player.getObjectId()) - System.currentTimeMillis()) / 1000);
				if (time > 0)
				{
					player.sendMessage("Wait " + time + " seconds for use this function.");
					return;
				}
			}
			
			bind_hwid.put(player.getObjectId(), System.currentTimeMillis() + 60000);
			LoginServerThread.getInstance().sendAllowedHwid(player.getAccountName(), player.getHWID());
			player.sendMessage("Access to your account is available now only from your PC");
			player.broadcastPacket(new MagicSkillUse(player, player, 5662, 1, 1000, 0));
			return;
		}
	}
	
	public static void unlock(L2PcInstance player, boolean ip, boolean hwid)
	{
		if (ip)
		{
			if (!SecuritySettingConfig.ALLOW_LOCK_IP)
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
				return;
			}
			
			if (bind_ip.containsKey(player.getObjectId()))
			{
				int time = (int) ((bind_ip.get(player.getObjectId()) - System.currentTimeMillis()) / 1000);
				if (time > 0)
				{
					player.sendMessage("Wait " + time + " seconds for use this function.");
					return;
				}
			}
			
			bind_ip.put(player.getObjectId(), System.currentTimeMillis() + 60000);
			LoginServerThread.getInstance().sendAllowedIP(player.getAccountName(), "*");
			player.sendMessage("Bind by IP is removed");
			player.broadcastPacket(new MagicSkillUse(player, player, 6802, 1, 1000, 0));
			return;
		}
		else if (hwid)
		{
			if (!SecuritySettingConfig.ALLOW_LOCK_HWID)
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled"));
				return;
			}
			
			if (bind_hwid.containsKey(player.getObjectId()))
			{
				int time = (int) ((bind_hwid.get(player.getObjectId()) - System.currentTimeMillis()) / 1000);
				if (time > 0)
				{
					player.sendMessage("Wait " + time + " seconds for use this function.");
					return;
				}
			}
			
			bind_hwid.put(player.getObjectId(), System.currentTimeMillis() + 60000);
			LoginServerThread.getInstance().sendAllowedHwid(player.getAccountName(), "*");
			player.sendMessage("Bind by HWID is removed");
			player.broadcastPacket(new MagicSkillUse(player, player, 6802, 1, 1000, 0));
			return;
		}
	}
}
