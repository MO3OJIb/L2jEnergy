/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.coliseum;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.events.UCConfig;
import com.l2jserver.gameserver.enums.TeleportWhereType;
import com.l2jserver.gameserver.enums.events.Team;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.serverpackets.ExPVPMatchUserDie;
import com.l2jserver.gameserver.network.serverpackets.L2GameServerPacket;

public class UCArena
{
	private static final Logger LOG = LoggerFactory.getLogger(UCArena.class);
	
	private static final int MINUTES_IN_MILISECONDS = UCConfig.UC_ROUND_TIME * 60 * 1000;
	
	private final int _id;
	private final int _minLevel;
	private final int _maxLevel;
	
	private final List<UCPoint> _points = new ArrayList<>();
	private final List<UCTeam> _teams = new ArrayList<>();
	
	private ScheduledFuture<?> _taskFuture = null;
	private final List<UCWaiting> _waitingPartys = new ArrayList<>();
	
	public UCArena(int id, int curator, int min_level, int max_level)
	{
		_id = id;
		_minLevel = min_level;
		_maxLevel = max_level;
	}
	
	public int getId()
	{
		return _id;
	}
	
	public int getMinLevel()
	{
		return _minLevel;
	}
	
	public int getMaxLevel()
	{
		return _maxLevel;
	}
	
	public void setUCPoint(int index, UCPoint point)
	{
		if (index > 4)
		{
			LOG.warn("Points can't be bigger than 4.");
			return;
		}
		if (index >= _points.size())
		{
			_points.add(point);
		}
		else
		{
			_points.set(index, point);
		}
	}
	
	public void setUCTeam(int index, UCTeam team)
	{
		if (index > 2)
		{
			LOG.warn("There can't be more than 2 teams.");
			return;
		}
		
		if (index >= _teams.size())
		{
			_teams.add(team);
		}
		else
		{
			_teams.set(index, team);
		}
	}
	
	public List<UCTeam> getTeams()
	{
		return _teams;
	}
	
	public List<UCPoint> getPoints()
	{
		return _points;
	}
	
	public List<UCWaiting> getWaitingList()
	{
		return _waitingPartys;
	}
	
	public void switchStatus(boolean start)
	{
		if ((_taskFuture == null) && start)
		{
			runNewTask();
		}
		else
		{
			_taskFuture.cancel(true);
			_taskFuture = null;
			
			checkLost(true);
			
			for (var team : _teams)
			{
				team.clean(true);
			}
		}
	}
	
	public void runNewTask()
	{
		_taskFuture = ThreadPoolManager.getInstance().scheduleGeneral(new UCRunningTask(this), MINUTES_IN_MILISECONDS);
	}
	
	public void runTaskNow()
	{
		_taskFuture.cancel(true);
		_taskFuture = null;
		var task = new UCRunningTask(this);
		task.run();
	}
	
	public void checkLost(boolean removeWinners)
	{
		var blueTeam = _teams.get(0);
		var redTeam = _teams.get(1);
		UCTeam winnerTeam = null;
		
		if ((blueTeam.getStatus() == UCTeam.WIN) || (redTeam.getStatus() == UCTeam.WIN))
		{
			winnerTeam = blueTeam.getStatus() == UCTeam.WIN ? blueTeam : redTeam;
		}
		else
		{
			if (blueTeam.getParty() == null)
			{
				redTeam.setStatus(UCTeam.WIN);
				winnerTeam = redTeam;
			}
			else if (redTeam.getParty() == null)
			{
				blueTeam.setStatus(UCTeam.WIN);
				winnerTeam = blueTeam;
			}
			else
			{
				if (blueTeam.getKillCount() > redTeam.getKillCount())
				{
					blueTeam.setStatus(UCTeam.WIN);
					redTeam.setStatus(UCTeam.FAIL);
					winnerTeam = blueTeam;
				}
				else if (redTeam.getKillCount() > blueTeam.getKillCount())
				{
					blueTeam.setStatus(UCTeam.FAIL);
					redTeam.setStatus(UCTeam.WIN);
					winnerTeam = redTeam;
				}
				else if (blueTeam.getKillCount() == redTeam.getKillCount())
				{
					if (blueTeam.getRegisterTime() > redTeam.getRegisterTime())
					{
						blueTeam.setStatus(UCTeam.FAIL);
						redTeam.setStatus(UCTeam.WIN);
						winnerTeam = redTeam;
					}
					else
					{
						blueTeam.setStatus(UCTeam.WIN);
						redTeam.setStatus(UCTeam.FAIL);
						winnerTeam = blueTeam;
					}
				}
			}
		}
		
		broadcastToAll(new ExPVPMatchUserDie(_teams.get(0).getKillCount(), _teams.get(1).getKillCount()));
		
		blueTeam.setLastParty(redTeam.getParty());
		redTeam.setLastParty(blueTeam.getParty());
		
		if (removeWinners && (winnerTeam != null))
		{
			for (var point : _points)
			{
				point.actionDoors(false);
			}
			winnerTeam.clean(true);
		}
	}
	
	public void broadcastToAll(L2GameServerPacket packet)
	{
		for (var team : getTeams())
		{
			var party = team.getParty();
			if (party != null)
			{
				for (var pl : party.getMembers())
				{
					party.broadcastToPartyMembers(pl, packet);
				}
			}
		}
	}
	
	public void startFight()
	{
		for (var team : _teams)
		{
			team.spawnTower();
			
			for (var player : team.getParty().getMembers())
			{
				if (player != null)
				{
					player.setTeam(team.getTeam());
				}
			}
		}
		runNewTask();
	}
	
	public TeleportWhereType getLocation()
	{
		return TeleportWhereType.TOWN;
	}
	
	public void removeTeam()
	{
		for (var team : _teams)
		{
			for (var player : team.getParty().getMembers())
			{
				if (player == null)
				{
					continue;
				}
				
				player.setTeam(Team.NONE);
				player.cleanUCStats();
				player.setUCState(L2PcInstance.UC_STATE_NONE);
				if (player.isDead())
				{
					UCTeam.resPlayer(player);
				}
				
				player.teleToLocation(getLocation());
			}
		}
	}
}