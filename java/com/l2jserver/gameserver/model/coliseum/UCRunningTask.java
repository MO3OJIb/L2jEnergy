/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.coliseum;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UCRunningTask implements Runnable
{
	private static final Logger LOG = LoggerFactory.getLogger(UCRunningTask.class);
	
	private final UCArena _arena;
	
	public UCRunningTask(UCArena arena)
	{
		_arena = arena;
	}
	
	@Override
	public void run()
	{
		_arena.removeTeam();
		_arena.checkLost(false);
		
		var winnerTeam = _arena.getTeams().stream().filter(team -> team.getStatus() == UCTeam.WIN).findFirst();
		
		winnerTeam.ifPresent(team -> team.setStatus(UCTeam.NOT_DECIDED));
		
		_arena.getPoints().forEach(point -> point.actionDoors(false));
		
		if (winnerTeam.isPresent() && (_arena.getWaitingList().size() >= 1))
		{
			var other = winnerTeam.get().getOtherTeam();
			var otherWaiting = _arena.getWaitingList().get(0);
			other.setParty(otherWaiting.getParty());
			other.setRegisterTime(otherWaiting.getRegisterMillis());
			_arena.getWaitingList().remove(0);
			winnerTeam.get().splitMembersAndTeleport();
			other.splitMembersAndTeleport();
			
			_arena.startFight();
			return;
		}
		
		while (true)
		{
			if (_arena.getWaitingList().size() >= 2)
			{
				var i = 0;
				for (var team : _arena.getTeams())
				{
					var teamWaiting = _arena.getWaitingList().get(i);
					team.setParty(teamWaiting.getParty());
					team.setRegisterTime(teamWaiting.getRegisterMillis());
					_arena.getWaitingList().remove(i);
					team.splitMembersAndTeleport();
					i++;
				}
				
				_arena.startFight();
				break;
			}
			
			try
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException ex)
			{
				LOG.error("UC infinite while loop error.", ex);
			}
		}
	}
}