/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.coliseum;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.events.UCConfig;
import com.l2jserver.gameserver.data.xml.impl.NpcData;
import com.l2jserver.gameserver.enums.events.Team;
import com.l2jserver.gameserver.idfactory.IdFactory;
import com.l2jserver.gameserver.model.L2Party;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.instance.L2UCTowerInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ExPVPMatchUserDie;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.network.serverpackets.UserInfo;

public class UCTeam
{
	protected static final Logger LOG = LoggerFactory.getLogger(UCTeam.class);
	
	public final static byte NOT_DECIDED = 0;
	public final static byte WIN = 1;
	public final static byte FAIL = 2;
	
	private final int _index;
	private final UCArena _baseArena;
	// Resurrection Tower Info
	protected final int _x;
	protected final int _y;
	protected final int _z;
	private final int _npcId;
	
	private L2UCTowerInstance _tower;
	
	private L2Party _party;
	
	private int _killCount;
	private byte _status;
	private L2Party _lastParty;
	
	private int _consecutiveWins;
	
	private long registerTime;
	
	List<L2PcInstance> _team1 = new ArrayList<>();
	List<L2PcInstance> _team2 = new ArrayList<>();
	List<L2PcInstance> _team3 = new ArrayList<>();
	
	public UCTeam(int index, UCArena baseArena, int x, int y, int z, int npcId)
	{
		_index = index;
		_baseArena = baseArena;
		_x = x;
		_y = y;
		_z = z;
		_npcId = npcId;
		
		setStatus(NOT_DECIDED);
	}
	
	public long getRegisterTime()
	{
		return registerTime;
	}
	
	public void setLastParty(L2Party party)
	{
		_lastParty = party;
	}
	
	public void setRegisterTime(long time)
	{
		registerTime = time;
	}
	
	public void increaseConsecutiveWins()
	{
		_consecutiveWins++;
	}
	
	public int getConsecutiveWins()
	{
		return _consecutiveWins;
	}
	
	public void spawnTower()
	{
		if (_tower != null)
		{
			LOG.warn("Tower already exists. Can't spawn tower.");
			return;
		}
		
		var template = NpcData.getInstance().getTemplate(_npcId);
		
		if (template == null)
		{
			LOG.warn("Can't find Underground Coliseum tower template: " + _npcId);
			return;
		}
		
		_tower = new L2UCTowerInstance(this, IdFactory.getInstance().getNextId(), template);
		// _tower.setTeam(_index);
		_tower.setIsInvul(false);
		_tower.setCurrentHpMp(_tower.getMaxHp(), _tower.getMaxMp());
		_tower.spawnMe(_x, _y, _z);
	}
	
	public void deleteTower()
	{
		if (_tower == null)
		{
			return;
		}
		
		_tower.deleteMe();
		_tower = null;
	}
	
	public void splitMembersAndTeleport()
	{
		var pointzor = _baseArena.getPoints();
		for (var player : getParty().getMembers())
		{
			if (player == null)
			{
				continue;
			}
			
			if (_team1.size() < 3)
			{
				_team1.add(player);
			}
			else if (_team2.size() < 3)
			{
				_team2.add(player);
			}
			else
			{
				_team3.add(player);
			}
			player.setUCState(L2PcInstance.UC_STATE_POINT);
		}
		pointzor.get(1).teleportPeoples(_team1.toArray(new L2PcInstance[0]));
		pointzor.get(2).teleportPeoples(_team2.toArray(new L2PcInstance[0]));
		pointzor.get(3).teleportPeoples(_team3.toArray(new L2PcInstance[0]));
	}
	
	public boolean isKilledByThisTeam(final L2PcInstance killer)
	{
		if ((getParty() == null) || (killer == null))
		{
			return false;
		}
		
		for (var member : getParty().getMembers())
		{
			if ((member != null) && (member == killer))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public void onKill(final L2PcInstance player, final L2PcInstance killer)
	{
		if ((player == null) || (killer == null) || (getParty() == null))
		{
			return;
		}
		
		if (!player.isDead())
		{
			return;
		}
		
		var otherTeam = getOtherTeam();
		var pointzor = _baseArena.getPoints();
		
		if (!otherTeam.isKilledByThisTeam(killer))
		{
			return;
		}
		
		otherTeam.increaseKillCount();
		player.increaseDeathCountUC();
		killer.increaseKillCountUC();
		
		_baseArena.broadcastToAll(new ExPVPMatchUserDie(_baseArena.getTeams().get(0).getKillCount(), _baseArena.getTeams().get(1).getKillCount()));
		
		if (player.getUCState() == L2PcInstance.UC_STATE_POINT)
		{
			var deadzor = true;
			for (var member1 : _team1)
			{
				if ((member1 != null) && !member1.isDead())
				{
					deadzor = false;
				}
			}
			if (deadzor)
			{
				pointzor.get(1).actionDoors(true);
				for (var memberzor1 : _team1)
				{
					memberzor1.setUCState(L2PcInstance.UC_STATE_ARENA);
				}
			}
			else
			{
				deadzor = true;
			}
			for (var member2 : _team2)
			{
				if ((member2 != null) && !member2.isDead())
				{
					deadzor = false;
				}
			}
			if (deadzor)
			{
				pointzor.get(2).actionDoors(true);
				for (var memberzor2 : _team2)
				{
					memberzor2.setUCState(L2PcInstance.UC_STATE_ARENA);
				}
			}
			else
			{
				deadzor = true;
			}
			for (var member3 : _team3)
			{
				if ((member3 != null) && !member3.isDead())
				{
					deadzor = false;
				}
			}
			if (deadzor)
			{
				pointzor.get(3).actionDoors(true);
				for (var memberzor3 : _team3)
				{
					memberzor3.setUCState(L2PcInstance.UC_STATE_ARENA);
				}
			}
			else
			{
				deadzor = true;
			}
		}
		
		if (_tower == null)
		{
			var flag = true;
			for (var member : getParty().getMembers())
			{
				if ((member != null) && !member.isDead())
				{
					flag = false;
				}
			}
			
			if (flag)
			{
				setStatus(FAIL);
				otherTeam.setStatus(WIN);
				_baseArena.runTaskNow();
				
			}
			return;
		}
		
		ThreadPoolManager.getInstance().executeGeneral(() ->
		{
			resPlayer(player);
			
			player.teleToLocation(_x + Rnd.get(2, 50), _y + Rnd.get(10, 100), _z);
		});
	}
	
	public void increaseKillCount()
	{
		_killCount++;
	}
	
	public static void resPlayer(L2PcInstance player)
	{
		if (player == null)
		{
			return;
		}
		
		player.restoreExp(100.0);
		player.doRevive();
		player.setCurrentHpMp(player.getMaxHp(), player.getMaxMp());
		player.setCurrentCp(player.getMaxCp());
	}
	
	public void clean(boolean full)
	{
		deleteTower();
		
		if (full)
		{
			if (getParty() != null)
			{
				getParty().setUCState(null);
				_party = null;
			}
			
			_lastParty = null;
			_consecutiveWins = 0;
			setStatus(NOT_DECIDED);
		}
		
		_killCount = 0;
	}
	
	public byte getStatus()
	{
		return _status;
	}
	
	public UCArena getBaseArena()
	{
		return _baseArena;
	}
	
	public void computeReward()
	{
		if ((_lastParty == null) || (_lastParty != getOtherTeam().getParty()))
		{
			var reward = 0;
			switch (_consecutiveWins)
			{
				case 1:
					reward = UCConfig.UC_REWARD_1;
					break;
				case 2:
					reward = UCConfig.UC_REWARD_2;
					break;
				case 3:
					reward = UCConfig.UC_REWARD_3;
					break;
				case 4:
					reward = UCConfig.UC_REWARD_4;
					break;
				case 5:
					reward = UCConfig.UC_REWARD_5;
					break;
				case 6:
					reward = UCConfig.UC_REWARD_6;
					break;
				case 7:
					reward = UCConfig.UC_REWARD_7;
					break;
				case 8:
					reward = UCConfig.UC_REWARD_8;
					break;
				case 9:
					reward = UCConfig.UC_REWARD_9;
					break;
				case 10:
					reward = UCConfig.UC_REWARD_10;
					break;
				default:
					if (_consecutiveWins > 10)
					{
						reward = (UCConfig.UC_REWARD_10 + _consecutiveWins) - 10;
					}
					break;
			}
			
			reward += UCConfig.UC_REWARD_BONUS;
			
			for (var member : getParty().getMembers())
			{
				if (member != null)
				{
					member.setFame(member.getFame() + reward);
					var sm = SystemMessage.getSystemMessage(SystemMessageId.ACQUIRED_S1_REPUTATION_SCORE);
					sm.addInt(reward);
					member.sendPacket(sm);
					member.sendPacket(new UserInfo(member));
				}
			}
		}
	}
	
	public void setStatus(byte status)
	{
		_status = status;
		
		if (_status == WIN)
		{
			if (getIndex() == 0)
			{
				_baseArena.broadcastToAll(SystemMessage.getSystemMessage(SystemMessageId.THE_BLUE_TEAM_IS_VICTORIOUS));
				
			}
			else
			{
				_baseArena.broadcastToAll(SystemMessage.getSystemMessage(SystemMessageId.THE_RED_TEAM_IS_VICTORIOUS));
			}
		}
		
		switch (_status)
		{
			case NOT_DECIDED:
				break;
			case WIN:
				increaseConsecutiveWins();
				computeReward();
				clean(false);
				break;
			case FAIL:
				clean(true);
				break;
		}
	}
	
	public UCTeam getOtherTeam()
	{
		return _baseArena.getTeams().get(getTeam().getId() == 0 ? 1 : 0);
	}
	
	public Team getTeam()
	{
		switch (_index)
		{
			case 0:
				return Team.BLUE;
			case 1:
				return Team.RED;
			default:
				throw new IllegalArgumentException("Incorrect index: " + _index);
		}
	}
	
	public int getKillCount()
	{
		return _killCount;
	}
	
	public void setParty(L2Party pa)
	{
		var oldParty = _party;
		_party = pa;
		
		if (oldParty != null)
		{
			oldParty.setUCState(null);
		}
		
		if (_party != null)
		{
			_party.setUCState(this);
		}
	}
	
	public L2Party getParty()
	{
		return _party;
	}
	
	public int getIndex()
	{
		return _index;
	}
}