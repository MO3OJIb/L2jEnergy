/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.conditions;

import com.l2jserver.gameserver.enums.skills.AbnormalType;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.items.L2Item;
import com.l2jserver.gameserver.model.skills.Skill;

/**
 * Check abnormal type and level.
 * @author Zoey76
 */
public class ConditionCheckAbnormal extends Condition
{
	private final AbnormalType _type;
	private final int _level;
	private final boolean _present;
	
	public ConditionCheckAbnormal(AbnormalType type, int level, boolean present)
	{
		_type = type;
		_level = level;
		_present = present;
	}
	
	@Override
	
	public boolean testImpl(L2Character effector, L2Character effected, Skill skill, L2Item item)
	{
		var info = effector.getEffectList().getBuffInfoByAbnormalType(_type);
		return _present == ((info != null) && (_level <= info.getSkill().getAbnormalLvl()));
	}
}
