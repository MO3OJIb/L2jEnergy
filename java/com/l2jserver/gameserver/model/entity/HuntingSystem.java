/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.entity;

import java.util.Calendar;
import java.util.Objects;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.custom.HuntingConfig;
import com.l2jserver.gameserver.enums.events.EventType;
import com.l2jserver.gameserver.enums.skills.AbnormalVisualEffect;
import com.l2jserver.gameserver.model.actor.L2Attackable;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.events.annotations.RegisterEvent;
import com.l2jserver.gameserver.model.events.impl.character.player.OnPlayerLogin;
import com.l2jserver.gameserver.model.events.impl.character.player.OnPlayerLogout;
import com.l2jserver.gameserver.model.events.listeners.ConsumerEventListener;
import com.l2jserver.gameserver.model.variables.PlayerVariables;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ExNavitAdventEffect;
import com.l2jserver.gameserver.network.serverpackets.ExNavitAdventPointInfo;
import com.l2jserver.gameserver.network.serverpackets.ExNavitAdventTimeChange;

/**
 * @author Janiko
 * @author IrLex
 */
public class HuntingSystem
{
	private static final int ADDITIONAL_NEVIT_POINTS = 2;
	private static final int HUNTING_BONUS_REFRESH_RATE = 10;
	
	private final L2PcInstance _player;
	
	private volatile ScheduledFuture<?> _huntingBonusTask;
	private volatile ScheduledFuture<?> _nevitBlessingTimeTask;
	
	public HuntingSystem(L2PcInstance player)
	{
		Objects.requireNonNull(player);
		_player = player;
		player.addListener(new ConsumerEventListener(player, EventType.ON_PLAYER_LOGIN, (OnPlayerLogin event) -> onPlayerLogin(event), this));
		player.addListener(new ConsumerEventListener(player, EventType.ON_PLAYER_LOGOUT, (OnPlayerLogout event) -> OnPlayerLogout(event), this));
	}
	
	@RegisterEvent(EventType.ON_PLAYER_LOGIN)
	private void onPlayerLogin(OnPlayerLogin event)
	{
		var cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 6);
		cal.set(Calendar.MINUTE, 30);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		// Reset Hunting System
		if ((getPlayer().getLastAccess() < (cal.getTimeInMillis() / 1000)) && (System.currentTimeMillis() > cal.getTimeInMillis()))
		{
			setHuntingBonusTime(0); // Refuel-reset hunting bonus time
		}
		
		// Send Packets
		getPlayer().sendPacket(new ExNavitAdventPointInfo(getNevitBlessingPoints()));
		getPlayer().sendPacket(new ExNavitAdventTimeChange(getHuntingBonusTime(), true));
		
		checkNevitBlessingEffect(getNevitBlessingTime());
		
		// Set percent
		int percent = calcPercent(getNevitBlessingPoints());
		
		if ((percent >= 25) && (percent < 50))
		{
			getPlayer().sendPacket(SystemMessageId.YOU_ARE_STARTING_TO_FEEL_THE_EFFECTS_OF_NEVITS_ADVENT_BLESSING);
		}
		else if ((percent >= 50) && (percent < 75))
		{
			getPlayer().sendPacket(SystemMessageId.YOU_ARE_FURTHER_INFUSED_WITH_THE_BLESSINGS_OF_NEVIT);
		}
		else if (percent >= 75)
		{
			getPlayer().sendPacket(SystemMessageId.NEVITS_ADVENT_BLESSING_SHINES_STRONGLY_FROM_ABOVE);
		}
	}
	
	@RegisterEvent(EventType.ON_PLAYER_LOGOUT)
	private void OnPlayerLogout(OnPlayerLogout event)
	{
		stopNevitBlessingEffectTask(true);
		stopHuntingBonusTask(false);
	}
	
	public void addPoints(int val)
	{
		setNevitBlessingPoints(getNevitBlessingPoints() + val);
		if (getNevitBlessingPoints() > HuntingConfig.HUNTING_SYSTEM_MAX_POINTS)
		{
			setNevitBlessingPoints(0);
			checkNevitBlessingEffect(HuntingConfig.HUNTING_SYSTEM_EFFECT_TIME);
		}
		
		switch (calcPercent(getNevitBlessingPoints()))
		{
			case 25:
			{
				getPlayer().sendPacket(SystemMessageId.YOU_ARE_STARTING_TO_FEEL_THE_EFFECTS_OF_NEVITS_ADVENT_BLESSING);
				break;
			}
			case 50:
			{
				getPlayer().sendPacket(SystemMessageId.YOU_ARE_FURTHER_INFUSED_WITH_THE_BLESSINGS_OF_NEVIT);
				break;
			}
			case 75:
			{
				getPlayer().sendPacket(SystemMessageId.NEVITS_ADVENT_BLESSING_SHINES_STRONGLY_FROM_ABOVE);
				break;
			}
		}
		getPlayer().sendPacket(new ExNavitAdventPointInfo(getNevitBlessingPoints()));
	}
	
	public void startHuntingSystemTask()
	{
		if ((_huntingBonusTask == null) && (((getHuntingBonusTime() < HuntingConfig.HUNTING_SYSTEM_MAX_TIME) || !HuntingConfig.HUNTING_SYSTEM_LIMIT)))
		{
			_huntingBonusTask = ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new HuntingBonusTask(), 1000, 10000);
			if (HuntingConfig.HUNTING_SYSTEM_LIMIT)
			{
				getPlayer().sendPacket(new ExNavitAdventTimeChange(getHuntingBonusTime(), false));
			}
		}
	}
	
	protected class HuntingBonusTask implements Runnable
	{
		@Override
		public void run()
		{
			setHuntingBonusTime(getHuntingBonusTime() + HUNTING_BONUS_REFRESH_RATE);
			
			if ((getHuntingBonusTime() >= HuntingConfig.HUNTING_SYSTEM_MAX_TIME) && HuntingConfig.HUNTING_SYSTEM_LIMIT)
			{
				setHuntingBonusTime(HuntingConfig.HUNTING_SYSTEM_MAX_TIME);
				stopHuntingBonusTask(true);
				return;
			}
			
			if (HuntingConfig.HUNTING_SYSTEM_LIMIT)
			{
				getPlayer().sendPacket(new ExNavitAdventTimeChange(getHuntingBonusTime(), false));
			}
			
			addPoints(ADDITIONAL_NEVIT_POINTS);
			if (getNevitBlessingTime() > 0)
			{
				addPoints(HuntingConfig.HUNTING_SYSTEM_REGULAR_POINTS);
			}
			else
			{
				addPoints(HuntingConfig.HUNTING_SYSTEM_REGULAR_POINTS_2);
			}
		}
	}
	
	private void checkNevitBlessingEffect(int value)
	{
		if (getNevitBlessingTime() > 0)
		{
			stopNevitBlessingEffectTask(false);
			value = getNevitBlessingTime();
		}
		
		if (value > 0)
		{
			setNevitBlessingTime(value);
			getPlayer().sendPacket(new ExNavitAdventEffect(value));
			getPlayer().sendPacket(SystemMessageId.THE_ANGEL_NEVIT_HAS_BLESSED_YOU_FROM_ABOVE);
			getPlayer().startAbnormalVisualEffect(true, AbnormalVisualEffect.NEVIT_ADVENT);
			_nevitBlessingTimeTask = ThreadPoolManager.getInstance().scheduleGeneral(new NevitEffectEnd(), value * 1000);
		}
	}
	
	protected class NevitEffectEnd implements Runnable
	{
		@Override
		public void run()
		{
			setNevitBlessingTime(0);
			getPlayer().sendPacket(new ExNavitAdventEffect(0));
			getPlayer().sendPacket(new ExNavitAdventPointInfo(getNevitBlessingPoints()));
			getPlayer().sendPacket(SystemMessageId.NEVITS_ADVENT_BLESSING_HAS_ENDED);
			getPlayer().stopAbnormalVisualEffect(true, AbnormalVisualEffect.NEVIT_ADVENT);
			stopNevitBlessingEffectTask(false);
		}
	}
	
	public void stopHuntingBonusTask(boolean sendPacket)
	{
		if (_huntingBonusTask != null)
		{
			_huntingBonusTask.cancel(true);
			_huntingBonusTask = null;
		}
		
		if (sendPacket)
		{
			getPlayer().sendPacket(new ExNavitAdventTimeChange(getHuntingBonusTime(), true));
		}
	}
	
	protected void stopNevitBlessingEffectTask(boolean value)
	{
		if (_nevitBlessingTimeTask != null)
		{
			if (value)
			{
				int time = (int) _nevitBlessingTimeTask.getDelay(TimeUnit.SECONDS);
				if (time > 0)
				{
					setNevitBlessingTime(time);
				}
				else
				{
					setNevitBlessingTime(0);
				}
			}
			_nevitBlessingTimeTask.cancel(true);
			_nevitBlessingTimeTask = null;
		}
	}
	
	public void checkIfMustGivePoints(long baseExp, L2Attackable l2Attackable)
	{
		if (HuntingConfig.HUNTING_SYSTEM_EXTRA_POINTS)
		{
			if (((_huntingBonusTask != null) && HuntingConfig.HUNTING_SYSTEM_EXTRA_POINTS_ALL_TIME) || (_huntingBonusTask == null))
			{
				int nevitPoints = Math.round(((baseExp / (l2Attackable.getLevel() * l2Attackable.getLevel())) * 100) / 20);
				addPoints(nevitPoints);
			}
		}
	}
	
	public boolean isNevitBlessingActive()
	{
		return ((_nevitBlessingTimeTask != null) && (_nevitBlessingTimeTask.getDelay(TimeUnit.MILLISECONDS) > 0));
	}
	
	public boolean isNevitBonusTaskActive()
	{
		return (_nevitBlessingTimeTask != null);
	}
	
	public static int calcPercent(int points)
	{
		return (int) ((100.0 / HuntingConfig.HUNTING_SYSTEM_MAX_POINTS) * points);
	}
	
	public L2PcInstance getPlayer()
	{
		return _player;
	}
	
	public int getNevitBlessingPoints()
	{
		return getPlayer().getVariables().getInt(PlayerVariables.NEVIT_BLESSING_POINT, 0);
	}
	
	public void setNevitBlessingPoints(int points)
	{
		var vars = getPlayer().getVariables();
		vars.set(PlayerVariables.NEVIT_BLESSING_POINT, points);
		vars.storeMe();
	}
	
	public int getHuntingBonusTime()
	{
		return getPlayer().getVariables().getInt(PlayerVariables.HUNTING_BONUS, 0);
	}
	
	public void setHuntingBonusTime(int points)
	{
		var vars = getPlayer().getVariables();
		vars.set(PlayerVariables.HUNTING_BONUS, points);
		vars.storeMe();
	}
	
	public int getNevitBlessingTime()
	{
		return getPlayer().getVariables().getInt(PlayerVariables.NEVIT_BLESSING_TIME, 0);
	}
	
	public void setNevitBlessingTime(int points)
	{
		var vars = getPlayer().getVariables();
		vars.set(PlayerVariables.NEVIT_BLESSING_TIME, points);
		vars.storeMe();
	}
	
	public double getNevitHourglassMultiplier()
	{
		return (getPlayer().getRecSystem().getBonusTime() > 0) || getPlayer().hasAbnormalTypeVote() ? RecoBonus.getRecoMultiplier(getPlayer()) : 0;
	}
}