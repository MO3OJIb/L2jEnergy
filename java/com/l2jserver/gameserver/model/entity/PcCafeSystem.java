/*
 * Copyright (C) 2004-2022 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.entity;

import java.util.Objects;
import java.util.concurrent.ScheduledFuture;

import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.events.PcCafeConfig;
import com.l2jserver.gameserver.enums.InstanceType;
import com.l2jserver.gameserver.enums.PcCafeType;
import com.l2jserver.gameserver.enums.ZoneId;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2GrandBossInstance;
import com.l2jserver.gameserver.model.actor.instance.L2MonsterInstance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.actor.instance.L2RaidBossInstance;
import com.l2jserver.gameserver.model.actor.tasks.player.PCCafePointsTask;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ExPCCafePointInfo;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * @author Мо3олЬ
 */
public class PcCafeSystem
{
	private final L2PcInstance _player;
	private int _pcCafePoints = 0;
	private ScheduledFuture<?> _pcCafePointsGiveTask;
	
	public PcCafeSystem(L2PcInstance player)
	{
		Objects.requireNonNull(player);
		_player = player;
	}
	
	public void addPoints(final int count)
	{
		addPoints(count, false);
	}
	
	// startBonusTask
	public void addPoints(int count, final boolean doubleAmount)
	{
		count = doubleAmount ? count * 2 : count;
		var newAmount = Math.min(_player.getPcCafeSystem().getPoints() + count, PcCafeConfig.MAX_PC_BANG_POINTS);
		_player.getPcCafeSystem().setPoints(newAmount);
		_player.sendPacket(SystemMessage.getSystemMessage(doubleAmount ? SystemMessageId.DOUBLE_POINTS_YOU_ACQUIRED_S1_PC_BANG_POINT : SystemMessageId.YOU_ACQUIRED_S1_PC_BANG_POINT).addLong(count));
		_player.sendPacket(new ExPCCafePointInfo(newAmount, count, 1, doubleAmount ? PcCafeType.DOUBLE_ADD : PcCafeType.ADD, 12));
	}
	
	public void decreasePoints(final int count)
	{
		var newAmount = Math.max(_player.getPcCafeSystem().getPoints() - count, 0);
		_player.getPcCafeSystem().setPoints(newAmount);
		_player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.YOU_ARE_USING_S1_POINT).addLong(count));
		_player.sendPacket(new ExPCCafePointInfo(newAmount, -count, 1, PcCafeType.CONSUME, 12));
	}
	
	public void givePvEKillPoints(final long exp)
	{
		if (!PcCafeConfig.ALT_PCBANG_POINTS_RATE_ENABLED || !_player.getPcPremiumSystem().isActive() || _player.isInsideZone(ZoneId.PEACE) || _player.isInsideZone(ZoneId.PVP) || _player.isInsideZone(ZoneId.SIEGE) || (_player.isOnlineInt() == 0) || _player.isJailed())
		{
			return;
		}
		
		var points = (int) (exp * 0.0001 * PcCafeConfig.ALT_PCBANG_POINTS_RATE);
		
		var targetmob = _player.getTarget();
		var target = (L2Character) targetmob;
		
		if ((target instanceof L2GrandBossInstance) || (target instanceof L2RaidBossInstance))
		{
			if ((target.getInstanceType() == InstanceType.L2GrandBossInstance) || target.isRaid())
			{
				points = (int) (exp * 0.0001 * PcCafeConfig.ALT_PCBANG_POINTS_RATE_BOSS);
			}
		}
		if (target instanceof L2MonsterInstance)
		{
			if (target.isChampion())
			{
				points = (int) (exp * 0.0001 * PcCafeConfig.ALT_PCBANG_POINTS_RATE_EASY_CHAMPION);
			}
		}
		if (target instanceof L2MonsterInstance)
		{
			if (target.isHardChampion())
			{
				points = (int) (exp * 0.0001 * PcCafeConfig.ALT_PCBANG_POINTS_RATE_HARD_CHAMIPON);
			}
		}
		addPoints(points, (PcCafeConfig.ALT_PCBANG_POINTS_BONUS_DOUBLE_CHANCE > 0) && (Rnd.get(100) < PcCafeConfig.ALT_PCBANG_POINTS_BONUS_DOUBLE_CHANCE));
	}
	
	public void givePvPKillPoints()
	{
		addPoints(PcCafeConfig.ALT_PCBANG_POINTS_PVP_BONUS_POINTS, (PcCafeConfig.ALT_PCBANG_POINTS_BONUS_DOUBLE_CHANCE > 0) && (Rnd.get(100) < PcCafeConfig.ALT_PCBANG_POINTS_BONUS_DOUBLE_CHANCE));
	}
	
	public void startGiveTask()
	{
		if (!PcCafeConfig.ALT_PCBANG_POINTS_ENABLED || (PcCafeConfig.ALT_PCBANG_POINTS_DELAY <= 0))
		{
			return;
		}
		
		if (_pcCafePointsGiveTask == null)
		{
			_pcCafePointsGiveTask = ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new PCCafePointsTask(_player), PcCafeConfig.ALT_PCBANG_POINTS_DELAY * 60000, PcCafeConfig.ALT_PCBANG_POINTS_DELAY * 60000);
		}
	}
	
	public void stopGiveTask()
	{
		if (_pcCafePointsGiveTask != null)
		{
			_pcCafePointsGiveTask.cancel(false);
		}
		_pcCafePointsGiveTask = null;
	}
	
	public int getPoints()
	{
		return _pcCafePoints;
	}
	
	public void setPoints(long points)
	{
		_pcCafePoints = (int) points;
	}
}
