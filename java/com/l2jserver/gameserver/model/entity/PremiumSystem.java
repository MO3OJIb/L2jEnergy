/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.entity;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ScheduledFuture;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.custom.PremiumConfig;
import com.l2jserver.gameserver.instancemanager.tasks.PremiumExpireTask;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.variables.AccountVariables;
import com.l2jserver.gameserver.network.serverpackets.ExBR_PremiumState;

/**
 * @author Мо3олЬ
 */
public class PremiumSystem
{
	private final L2PcInstance _player;
	private ScheduledFuture<?> _task = null;
	private final Map<String, ScheduledFuture<?>> _expiretasks = new HashMap<>();
	
	public PremiumSystem(L2PcInstance player)
	{
		Objects.requireNonNull(player);
		_player = player;
	}
	
	public void startExpireTask(long delay)
	{
		_task = ThreadPoolManager.getInstance().scheduleGeneral(new PremiumExpireTask(_player), delay);
		_expiretasks.put(_player.getAccountName(), _task);
	}
	
	public void stopExpireTask()
	{
		_task = _expiretasks.remove(_player.getAccountName());
		if (_task != null)
		{
			_task.cancel(false);
			_task = null;
		}
	}
	
	public boolean isActive()
	{
		if (!PremiumConfig.PREMIUM_SYSTEM_ENABLED)
		{
			return false;
		}
		return _player.getAccountVariables().getBoolean(AccountVariables.PREMIUM_ACCOUNT, false);
	}
	
	public Map<String, ScheduledFuture<?>> getExpiretasks()
	{
		return _expiretasks;
	}
	
	public void setPremium(boolean isPremium)
	{
		_player.getAccountVariables().set(AccountVariables.PREMIUM_ACCOUNT, isPremium);
		_player.sendPacket(new ExBR_PremiumState(_player));
	}
}
