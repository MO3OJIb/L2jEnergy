/*
 * Copyright (C) 2004-2022 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.entity;

import java.util.Objects;

import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.variables.AccountVariables;

/**
 * @author Мо3олЬ
 */
public class PrimePointSystem
{
	private final L2PcInstance _player;
	
	public PrimePointSystem(L2PcInstance player)
	{
		Objects.requireNonNull(player);
		_player = player;
	}
	
	public long getPoints()
	{
		return _player.getAccountVariables().getLong(AccountVariables.PRIME_POINTS, 0);
	}
	
	public void setPoints(long points)
	{
		var vars = _player.getAccountVariables();
		vars.set(AccountVariables.PRIME_POINTS, points);
		vars.storeMe();
	}
}
