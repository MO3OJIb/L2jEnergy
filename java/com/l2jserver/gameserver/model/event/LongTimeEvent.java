/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 *
 * This file is part of L2jEnergy Server.
 *
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.event;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilderFactory;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.ServerConfig;
import com.l2jserver.gameserver.data.sql.impl.AnnouncementsTable;
import com.l2jserver.gameserver.data.xml.impl.EventDroplistData;
import com.l2jserver.gameserver.data.xml.impl.NpcData;
import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.enums.events.MessageType;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.announce.EventAnnouncement;
import com.l2jserver.gameserver.model.drops.DropListScope;
import com.l2jserver.gameserver.model.drops.GeneralDropItem;
import com.l2jserver.gameserver.model.holders.NpcSpawnHolder;
import com.l2jserver.gameserver.model.quest.Quest;
import com.l2jserver.gameserver.script.DateRange;
import com.l2jserver.gameserver.util.EventUtil;

/**
 * Parent class for long time events.<br>
 * Maintains config reading, spawn of NPC's, adding of event's drop.
 * @author GKR
 * @author Sacrifice
 */
public class LongTimeEvent extends Quest
{
	private final List<GeneralDropItem> _dropList = new ArrayList<>();
	private final List<NpcSpawnHolder> _spawnList = new ArrayList<>();
	
	private DateRange _dropEventPeriod;
	private String _eventName;
	private DateRange _eventPeriod = null;
	
	// Messages
	private String _onEnterMsg = "Event is in process";
	protected String _endMsg = "Event ends!";
	
	public LongTimeEvent(String name, String descr)
	{
		super(-1, name, descr);
		loadConfig();
		
		if (getEventPeriod() != null)
		{
			if (getEventPeriod().isWithinRange(new Date()))
			{
				startEvent();
				LOG.info("Event {} active till {}", _eventName, getEventPeriod().getEndDate());
			}
			else if (_eventPeriod.getStartDate().after(new Date()))
			{
				var delay = getEventPeriod().getStartDate().getTime() - System.currentTimeMillis();
				ThreadPoolManager.getInstance().scheduleEvent(new ScheduleStart(), delay);
				LOG.info("Event {} will be started at {}", _eventName, getEventPeriod().getStartDate());
			}
			else
			{
				LOG.info("The event {} has already passed...Ignored", _eventName);
			}
		}
	}
	
	public DateRange getEventPeriod()
	{
		return _eventPeriod;
	}
	
	public boolean isDropPeriod()
	{
		return _dropEventPeriod.isWithinRange(new Date());
	}
	
	public boolean isEventPeriod()
	{
		return _eventPeriod.isWithinRange(new Date());
	}
	
	private void loadConfig()
	{
		var configFile = new File(ServerConfig.DATAPACK_ROOT, "data/scripts/events/" + getName() + "/config.xml");
		var currentYear = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
		try
		{
			var dbf = DocumentBuilderFactory.newInstance();
			var db = dbf.newDocumentBuilder();
			var doc = db.parse(configFile);
			if (!doc.getDocumentElement().getNodeName().equalsIgnoreCase("event"))
			{
				throw new NullPointerException("WARNING!!! " + getName() + " event: bad config file!");
			}
			_eventName = doc.getDocumentElement().getAttributes().getNamedItem("name").getNodeValue();
			var period = doc.getDocumentElement().getAttributes().getNamedItem("active").getNodeValue();
			
			if (period.length() == 21) // dd MM yyyy-dd MM yyyy
			{
				_eventPeriod = DateRange.parse(period, new SimpleDateFormat("dd MM yyyy", Locale.US));
			}
			else if (period.length() == 11) // dd MM-dd MM
			{
				var start = period.split("-")[0].concat(" ").concat(currentYear);
				var end = period.split("-")[1].concat(" ").concat(currentYear);
				var activePeriod = start.concat("-").concat(end);
				_eventPeriod = DateRange.parse(activePeriod, new SimpleDateFormat("dd MM yyyy", Locale.US));
			}
			
			if (doc.getDocumentElement().getAttributes().getNamedItem("dropPeriod") != null)
			{
				var dropPeriod = doc.getDocumentElement().getAttributes().getNamedItem("dropPeriod").getNodeValue();
				
				if (dropPeriod.length() == 21) // dd MM yyyy-dd MM yyyy
				{
					_dropEventPeriod = DateRange.parse(dropPeriod, new SimpleDateFormat("dd MM yyyy", Locale.US));
				}
				else if (dropPeriod.length() == 11) // dd MM-dd MM
				{
					var start = dropPeriod.split("-")[0].concat(" ").concat(currentYear);
					var end = dropPeriod.split("-")[1].concat(" ").concat(currentYear);
					var activeDropPeriod = start.concat("-").concat(end);
					_dropEventPeriod = DateRange.parse(activeDropPeriod, new SimpleDateFormat("dd MM yyyy", Locale.US));
				}
				
				// Check if drop period is within range of event period
				if (!getEventPeriod().isWithinRange(_dropEventPeriod.getStartDate()) || !getEventPeriod().isWithinRange(_dropEventPeriod.getEndDate()))
				{
					_dropEventPeriod = _eventPeriod;
				}
			}
			else
			{
				_dropEventPeriod = _eventPeriod; // Drop period, if not specified, assumes all event period.
			}
			
			if (getEventPeriod() == null)
			{
				throw new NullPointerException("WARNING!!! " + getName() + " event: illegal event period");
			}
			
			var today = new Date();
			if (getEventPeriod().getStartDate().after(today) || getEventPeriod().isWithinRange(today))
			{
				var first = doc.getDocumentElement().getFirstChild();
				for (var n = first; n != null; n = n.getNextSibling())
				{
					// Loading droplist
					if (n.getNodeName().equalsIgnoreCase("droplist"))
					{
						for (var d = n.getFirstChild(); d != null; d = d.getNextSibling())
						{
							if (d.getNodeName().equalsIgnoreCase("add"))
							{
								try
								{
									var itemId = Integer.parseInt(d.getAttributes().getNamedItem("item").getNodeValue());
									var minCount = Integer.parseInt(d.getAttributes().getNamedItem("min").getNodeValue());
									var maxCount = Integer.parseInt(d.getAttributes().getNamedItem("max").getNodeValue());
									var chance = d.getAttributes().getNamedItem("chance").getNodeValue();
									var finalChance = 0;
									
									if (!chance.isEmpty() && chance.endsWith("%"))
									{
										finalChance = Integer.parseInt(chance.substring(0, chance.length() - 1)) * 10000;
									}
									
									if (ItemTable.getInstance().getTemplate(itemId) == null)
									{
										LOG.warn("{} event: {} is wrong item id, item was not added in droplist", getName(), itemId);
										continue;
									}
									
									if (minCount > maxCount)
									{
										LOG.warn("{} event: item {} - min greater than max, item was not added in droplist", getName(), itemId);
										continue;
									}
									
									if ((finalChance < 10000) || (finalChance > 1000000))
									{
										LOG.warn("{} event: item {} - incorrect drop chance, item was not added in droplist", getName(), itemId);
										continue;
									}
									_dropList.add((GeneralDropItem) DropListScope.STATIC.newDropItem(itemId, minCount, maxCount, finalChance));
								}
								catch (NumberFormatException nfe)
								{
									LOG.warn("Wrong number format in config.xml droplist block for {} event", getName());
								}
							}
						}
					}
					else if (n.getNodeName().equalsIgnoreCase("spawnlist"))
					{
						// Loading spawnlist
						for (var d = n.getFirstChild(); d != null; d = d.getNextSibling())
						{
							if (d.getNodeName().equalsIgnoreCase("add"))
							{
								try
								{
									var npcId = Integer.parseInt(d.getAttributes().getNamedItem("npc").getNodeValue());
									var xPos = Integer.parseInt(d.getAttributes().getNamedItem("x").getNodeValue());
									var yPos = Integer.parseInt(d.getAttributes().getNamedItem("y").getNodeValue());
									var zPos = Integer.parseInt(d.getAttributes().getNamedItem("z").getNodeValue());
									var heading = d.getAttributes().getNamedItem("heading").getNodeValue() != null ? Integer.parseInt(d.getAttributes().getNamedItem("heading").getNodeValue()) : 0;
									
									if (NpcData.getInstance().getTemplate(npcId) == null)
									{
										LOG.warn("{} event: {} is wrong NPC id, NPC was not added in spawnlist", getName(), npcId);
										continue;
									}
									_spawnList.add(new NpcSpawnHolder(npcId, new Location(xPos, yPos, zPos, heading)));
								}
								catch (NumberFormatException nfe)
								{
									LOG.warn("Wrong number format in config.xml spawnlist block for {} event", getName());
								}
							}
						}
					}
					else if (n.getNodeName().equalsIgnoreCase("messages"))
					{
						// Loading Messages
						for (var d = n.getFirstChild(); d != null; d = d.getNextSibling())
						{
							if (d.getNodeName().equalsIgnoreCase("add"))
							{
								var msgType = d.getAttributes().getNamedItem("type").getNodeValue();
								var msgText = d.getAttributes().getNamedItem("text").getNodeValue();
								if ((msgType != null) && (msgText != null))
								{
									if (msgType.equalsIgnoreCase("onEnd"))
									{
										_endMsg = msgText;
									}
									else if (msgType.equalsIgnoreCase("onEnter"))
									{
										_onEnterMsg = msgText;
									}
								}
							}
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("{} event: error reading {}! {} {}", getName(), configFile.getAbsolutePath(), e.getMessage(), e);
		}
	}
	
	protected void startEvent()
	{
		var currentTime = System.currentTimeMillis();
		
		// Add drop
		if (_dropList != null)
		{
			if (currentTime < _dropEventPeriod.getEndDate().getTime())
			{
				for (GeneralDropItem drop : _dropList)
				{
					EventDroplistData.getInstance().addGlobalDrop(drop.getItemId(), drop.getMin(), drop.getMax(), (int) drop.getChance(), _dropEventPeriod);
				}
			}
		}
		
		// Add spawns
		var millisToEventEnd = getEventPeriod().getEndDate().getTime() - currentTime;
		if (_spawnList != null)
		{
			for (NpcSpawnHolder spawn : _spawnList)
			{
				addSpawn(spawn.getNpcId(), spawn.getLocation().getX(), spawn.getLocation().getY(), spawn.getLocation().getZ(), spawn.getLocation().getHeading(), false, millisToEventEnd, false);
			}
		}
		
		// Send message on begin
		EventUtil.showAnnounceEvents("events_starts", _eventName + "", MessageType.CUSTOM);
		// Add announce for entering players
		AnnouncementsTable.getInstance().addAnnouncement(new EventAnnouncement(getEventPeriod(), _onEnterMsg)); // TODO
		// Schedule event end (now only for message sending)
		ThreadPoolManager.getInstance().scheduleEvent(new ScheduleEnd(), millisToEventEnd);
	}
	
	protected class ScheduleEnd implements Runnable
	{
		@Override
		public void run()
		{
			EventUtil.showAnnounceEvents("events_ends", _eventName + "", MessageType.CUSTOM);
		}
	}
	
	protected class ScheduleStart implements Runnable
	{
		@Override
		public void run()
		{
			startEvent();
		}
	}
}