/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.MailMessageType;
import com.l2jserver.gameserver.enums.PlayerAction;
import com.l2jserver.gameserver.enums.TeleportWhereType;
import com.l2jserver.gameserver.enums.events.EventState;
import com.l2jserver.gameserver.enums.events.MessageType;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.instancemanager.AntiFeedManager;
import com.l2jserver.gameserver.instancemanager.InstanceManager;
import com.l2jserver.gameserver.instancemanager.MailManager;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.Message;
import com.l2jserver.gameserver.model.events.EventDispatcher;
import com.l2jserver.gameserver.model.events.impl.events.OnEventRegistrationStart;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.gameserver.model.olympiad.OlympiadManager;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ConfirmDlg;
import com.l2jserver.gameserver.network.serverpackets.CreatureSay;
import com.l2jserver.gameserver.network.serverpackets.L2GameServerPacket;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.EventUtil;

/**
 * @author nuLL
 * @author Мо3олЬ
 */
public abstract class GameEvent
{
	protected static final Logger LOG = LoggerFactory.getLogger(GameEvent.class);
	
	public static EventState _state = EventState.INACTIVE;
	
	private int _eventInstance = 0;
	
	private String _eventName = "";
	
	public abstract GameEventConfig getConfigs();
	
	public abstract void init();
	
	protected abstract void calculateRewards();
	
	protected abstract void teleportPlayersToArena();
	
	protected abstract void teleportPlayersBack();
	
	protected abstract void clear();
	
	public abstract boolean isParticipant(int playerId);
	
	public abstract int getCountPlayers();
	
	public abstract boolean addParticipant(GameEventPlayer player);
	
	public abstract boolean removeParticipant(L2PcInstance player);
	
	public abstract void sysMsgToAllParticipants(String message, String string, String string2);
	
	public abstract void sysMsgToAllParticipants(String message, String string);
	
	public abstract void sysMsgToAllParticipants(String message);
	
	public abstract void broadcastPacketToTeams(L2GameServerPacket... packets);
	
	public abstract void broadcastScoreMessage();
	
	public abstract void onKill(L2Character killerCharacter, L2PcInstance killedPlayer);
	
	public abstract void onLogin(L2PcInstance player);
	
	public abstract boolean onAction(L2PcInstance player, int playerId);
	
	public abstract boolean checkForEventsSkill(L2PcInstance player, L2PcInstance target, Skill skill);
	
	public abstract boolean eventTarget(L2PcInstance player, L2Character target);
	
	public EventState getState()
	{
		synchronized (_state)
		{
			return _state;
		}
	}
	
	protected void setState(EventState state)
	{
		synchronized (_state)
		{
			_state = state;
		}
	}
	
	public boolean isInactive()
	{
		return getState() == EventState.INACTIVE;
	}
	
	public boolean isParticipating() // isRegistrating
	{
		return getState() == EventState.PARTICIPATING;
	}
	
	public boolean isStarting()
	{
		return getState() == EventState.STARTING;
	}
	
	public boolean isStarted() // isRunning
	{
		return getState() == EventState.STARTED;
	}
	
	public boolean isRewarding()
	{
		return getState() == EventState.REWARDING;
	}
	
	public String getEventName()
	{
		return _eventName;
	}
	
	public void setEventName(String eventName)
	{
		_eventName = eventName;
	}
	
	public boolean startParticipation()
	{
		setState(EventState.PARTICIPATING);
		
		showGameEventConfirmDlgTask();
		EventDispatcher.getInstance().notifyEventAsync(new OnEventRegistrationStart());
		return true;
	}
	
	public void showGameEventConfirmDlgTask()
	{
		for (var player : L2World.getInstance().getPlayers())
		{
			if (!player.isDead() && (player.getLevel() >= getConfigs().EVENT_MIN_LVL) && (player.getInstanceId() > 0) && (player.getLevel() <= getConfigs().EVENT_MAX_LVL) && !player.isInOlympiadMode() && !OlympiadManager.getInstance().isRegisteredInComp(player) && !player.inObserverMode()
				&& !player.isInSiege())
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "events_you_registration_canceled_cannot_participate_several_events_same_time"));
			}
			else
			{
				try
				{
					var confirm = new ConfirmDlg(SystemMessageId.S1);
					confirm.addTime(15000);
					confirm.addString(MessagesData.getInstance().getMessage(player, "event_you_want_participate_event").replace("%s%", getEventName() + ""));
					confirm.addRequesterId(player.getObjectId());
					player.addAction(PlayerAction.EVENTS);
					player.sendPacket(confirm);
				}
				catch (Throwable e)
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public boolean startFight()
	{
		// Set state to STARTING
		setState(EventState.STARTING);
		
		if (getCountPlayers() >= getConfigs().EVENT_MIN_PLAYERS_IN_TEAMS)
		{
			if (getConfigs().EVENT_IN_INSTANCE)
			{
				createInstance();
			}
			setState(EventState.STARTED);
			closeDoors();
			teleportPlayersToArena();
			return true;
		}
		clear();
		setState(EventState.INACTIVE);
		AntiFeedManager.getInstance().clear(AntiFeedManager.CUSTOM_EVENT_ID);
		return false;
	}
	
	public void stopFight()
	{
		// Set state INACTIVATING
		setState(EventState.INACTIVATING);
		// Opens all doors specified in configs
		openDoors();
		teleportPlayersBack();
		clear();
		setState(EventState.INACTIVE);
		AntiFeedManager.getInstance().clear(AntiFeedManager.CUSTOM_EVENT_ID);
	}
	
	public boolean register(L2PcInstance player)
	{
		if (player == null)
		{
			return false;
		}
		
		if (!EventUtil.checkPlayer(player, true))
		{
			return false;
		}
		
		var eventPlayer = new GameEventPlayer(player, getConfigs().PLAYER_CREDITS);
		
		if (getConfigs().EVENT_PARTICIPATION_FEE)
		{
			if (player.destroyItemByItemId("Event", getConfigs().EVENT_TAKE_ITEM_ID, getConfigs().EVENT_TAKE_COUNT, null, true))
			{
				if (addParticipant(eventPlayer))
				{
					EventUtil.showMessage(player, "events_you_registration_ok", getEventName(), MessageType.CUSTOM);
					return true;
				}
			}
		}
		
		if (addParticipant(eventPlayer))
		{
			EventUtil.showMessage(player, "events_you_registration_ok", getEventName(), MessageType.CUSTOM);
			return true;
		}
		return false;
	}
	
	public boolean unRegister(L2PcInstance player)
	{
		if (player == null)
		{
			return false;
		}
		
		if ((getState() == EventState.STARTED) || !isParticipant(player.getObjectId()))
		{
			EventUtil.showMessage(player, "events_you_registration_cancel_cant_moment", MessageType.CUSTOM);
			return false;
		}
		
		if (removeParticipant(player))
		{
			EventUtil.showMessage(player, "events_you_registration_cancelled", getEventName(), MessageType.CUSTOM);
			return true;
		}
		return false;
	}
	
	protected void closeDoors()
	{
		var doorsToClose = InstanceManager.getInstance().getInstance(_eventInstance).getDoors();
		
		for (var door : doorsToClose)
		{
			if (door != null)
			{
				door.closeMe();
			}
		}
	}
	
	protected void openDoors()
	{
		var doorsToOpen = InstanceManager.getInstance().getInstance(_eventInstance).getDoors();
		
		for (var door : doorsToOpen)
		{
			if (door != null)
			{
				door.openMe();
			}
		}
	}
	
	protected void createInstance()
	{
		try
		{
			_eventInstance = InstanceManager.getInstance().createDynamicInstance(getConfigs().EVENT_INSTANCE_FILE);
			InstanceManager.getInstance().getInstance(_eventInstance).setAllowSummon(false);
			InstanceManager.getInstance().getInstance(_eventInstance).setPvPInstance(true);
			InstanceManager.getInstance().getInstance(_eventInstance).setEmptyDestroyTime((getConfigs().EVENT_START_LEAVE_TELEPORT_DELAY * 1000) + 60000L);
		}
		catch (Exception ex)
		{
			_eventInstance = 0;
			LOG.warn("{} Event: Could not create the Event Instance Location!", getEventName());
		}
	}
	
	protected void giveKillBonus(GameEventPlayer player)
	{
		for (var rewardKills : getConfigs().EVENT_KILL_BONUS_REWARD)
		{
			var inv = player.getPlayer().getInventory();
			
			// Count the kill
			player.increaseKills();
			
			switch (player.getKills())
			{
				case 3: // Reward after 3 kills without die
				case 6: // Reward after 6 kills without die
				case 9: // Reward after 9 kills without die
				case 12: // Reward after 12 kills without die
				case 15: // Reward after 15 kills without die
				case 18: // Reward after 18 kills without die
				case 21: // Reward after 21 kills without die
					
					var systemMessage = SystemMessage.getSystemMessage(SystemMessageId.YOU_HAVE_EARNED_S2_S1S);
					systemMessage.addItemName(rewardKills[0]);
					systemMessage.addLong(rewardKills[1]);
					
					inv.addItem(getEventName(), rewardKills[0], rewardKills[1], player.getPlayer(), player.getPlayer());
					player.getPlayer().sendPacket(new CreatureSay(0, ChatType.PARTYROOM_ALL, MessagesData.getInstance().getMessage(player.getPlayer(), "events_amazing"), MessagesData.getInstance().getMessage(player.getPlayer(), "events_amazing_gets_kills_rewarded").replace("%s%", player.getName()
						+ "").replace("%i%", player.getKills() + "")));
					player.getPlayer().sendPacket(systemMessage);
					break;
			}
		}
	}
	
	protected void deliverRewards(L2PcInstance player, List<int[]> rewards)
	{
		var winner = (L2PcInstance) L2World.getInstance().getObject(player.getObjectId());
		
		var text1 = MessagesData.getInstance().getMessage(player, "events_you_reward");
		var text2 = MessagesData.getInstance().getMessage(player, "events_you_сongratulations_reward").replace("%i%", getEventName() + "");
		
		for (var reward : rewards)
		{
			var msg = new Message(winner.getObjectId(), text1, text2, MailMessageType.EVENTS);
			var attachments = msg.createAttachments();
			attachments.addItem(getEventName(), reward[0], reward[1], null, null);
			MailManager.getInstance().sendMessage(msg);
		}
	}
	
	public void onTeleported(L2PcInstance player)
	{
		if (!isStarted() || (player == null) || !isParticipant(player.getObjectId()))
		{
			return;
		}
		
		if (player.isMageClass())
		{
			for (var skillHolder : getConfigs().EVENT_MAGE_BUFFS)
			{
				var skill = skillHolder.getSkill();
				if (skill != null)
				{
					skill.applyEffects(player, player);
				}
			}
		}
		else
		{
			for (var skillHolder : getConfigs().EVENT_FIGHTER_BUFFS)
			{
				var skill = skillHolder.getSkill();
				if (skill != null)
				{
					skill.applyEffects(player, player);
				}
			}
		}
		
		player.setCurrentCp(player.getMaxCp());
		player.setCurrentHp(player.getMaxHp());
		player.setCurrentMp(player.getMaxMp());
	}
	
	public void onLogout(L2PcInstance player)
	{
		if ((player != null) && (isStarting() || isStarted() || isParticipating()))
		{
			if (removeParticipant(player))
			{
				player.teleToLocation(TeleportWhereType.TOWN);
			}
		}
	}
	
	public boolean onScrollUse(int playerId)
	{
		if (!isStarted())
		{
			return true;
		}
		return !isParticipant(playerId) || getConfigs().EVENT_SCROLL_ALLOWED;
	}
	
	public boolean onPotionUse(int playerId)
	{
		if (!isStarted())
		{
			return true;
		}
		return !isParticipant(playerId) || getConfigs().EVENT_POTIONS_ALLOWED;
	}
	
	public boolean onEscapeUse(int playerId)
	{
		if (!isStarted())
		{
			return true;
		}
		return !isParticipant(playerId);
	}
	
	public boolean onItemSummon(int playerId)
	{
		if (!isStarted())
		{
			return true;
		}
		return !isParticipant(playerId) || getConfigs().EVENT_SUMMON_BY_ITEM_ALLOWED;
	}
	
	public boolean onUseItem(L2PcInstance player, L2ItemInstance item)
	{
		if (!isStarted() || !isParticipant(player.getObjectId()))
		{
			return true;
		}
		
		if ((getConfigs().EVENT_RESTRICT_ITEMS != null) && (getConfigs().EVENT_RESTRICT_ITEMS.length != 0))
		{
			for (var itemId : getConfigs().EVENT_RESTRICT_ITEMS)
			{
				if (item.getId() == itemId)
				{
					return false;
				}
			}
		}
		return true;
	}
	
	// TODO: add
	public boolean onRequestUnEquipItem(L2PcInstance player)
	{
		if (isStarted() && isParticipant(player.getObjectId()))
		{
			player.sendMessage("You are not allowed using this.");
			return true;
		}
		return false;
	}
	
	// TODO: add
	public boolean onHandleCommandBBS(L2PcInstance player)
	{
		return isStarted() && isParticipant(player.getObjectId());
	}
	
	public int getEventInstance()
	{
		return _eventInstance;
	}
	
	public boolean talkWithNpc(L2PcInstance player, L2Npc npc)
	{
		return false;
	}
	
	public void onAttack(L2PcInstance player, L2Npc npc)
	{
		if ((getState() == EventState.STARTED) && (player != null) && isParticipant(player.getObjectId()))
		{
			
		}
	}
	
	public void showEventHtml(L2PcInstance player, String objectid)
	{
		// TODO: work on this
		if (getState() == EventState.PARTICIPATING)
		{
			try
			{
				final String htmContent;
				var html = new NpcHtmlMessage(Integer.parseInt(objectid));
				
				if (isParticipant(player.getObjectId()))
				{
					htmContent = HtmCache.getInstance().getHtm(player, "data/html/mods/EventEngine/Participating.htm");
				}
				else
				{
					htmContent = HtmCache.getInstance().getHtm(player, "data/html/mods/EventEngine/Participation.htm");
				}
				
				if (htmContent != null)
				{
					html.setHtml(htmContent);
				}
				
				html.replace("%objectId%", objectid); // Yeah, we need this.
				html.replace("%eventName%", _eventName);
				player.sendPacket(html);
			}
			catch (Exception e)
			{
				LOG.warn("Exception on showEventHtml()", e);
			}
		}
	}
}
