/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine;

import java.util.ArrayList;
import java.util.List;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.eventengine.tasks.GameEventAntiAfkTask;

/**
 * @author Мо3олЬ
 */
public class GameEventAntiAfk
{
	// Delay between location checks , Default 30000 ms (30 Sec.)
	private final int checkDelay = 60000;
	
	private final List<String> _eventsPlayerList = new ArrayList<>();
	private String[] Splitter;
	private int xx, yy, zz, SameLoc;
	private L2PcInstance _player;
	
	public GameEventAntiAfk()
	{
		ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new GameEventAntiAfkTask(), 60000, checkDelay);
	}
	
	public void setEventsSpawnInfo(String name, int _x, int _y, int _z)
	{
		if (!checkEventsSpawnInfo(name))
		{
			var temp = name + ":" + Integer.toString(_x) + ":" + Integer.toString(_y) + ":" + Integer.toString(_z) + ":1";
			_eventsPlayerList.add(temp);
		}
		else
		{
			var elements = _eventsPlayerList.toArray();
			for (var i = 0; i < elements.length; i++)
			{
				Splitter = ((String) elements[i]).split(":");
				var nameVal = Splitter[0];
				if (name.equals(nameVal))
				{
					getEventsSpawnInfo(name);
					if ((_x == xx) && (_y == yy) && (_z == zz) && (_player.isAttackingNow() == false) && (_player.isCastingNow() == false) && (_player.isOnline() == true))
					{
						++SameLoc;
						if (SameLoc >= 4)// Kick after 4 same x/y/z, location checks
						{
							// kick here
							_eventsPlayerList.remove(i);
							_player.logout();
							return;
						}
						
						_eventsPlayerList.remove(i);
						var temp = name + ":" + Integer.toString(_x) + ":" + Integer.toString(_y) + ":" + Integer.toString(_z) + ":" + SameLoc;
						_eventsPlayerList.add(temp);
						return;
					}
					
					_eventsPlayerList.remove(i);
					var temp = name + ":" + Integer.toString(_x) + ":" + Integer.toString(_y) + ":" + Integer.toString(_z) + ":1";
					_eventsPlayerList.add(temp);
				}
			}
		}
	}
	
	private boolean checkEventsSpawnInfo(String name)
	{
		var elements = _eventsPlayerList.toArray();
		for (var element : elements)
		{
			Splitter = ((String) element).split(":");
			var nameVal = Splitter[0];
			if (name.equals(nameVal))
			{
				return true;
			}
		}
		return false;
	}
	
	private void getEventsSpawnInfo(String name)
	{
		var elements = _eventsPlayerList.toArray();
		for (var element : elements)
		{
			Splitter = ((String) element).split(":");
			var nameVal = Splitter[0];
			if (name.equals(nameVal))
			{
				xx = Integer.parseInt(Splitter[1]);
				yy = Integer.parseInt(Splitter[2]);
				zz = Integer.parseInt(Splitter[3]);
				SameLoc = Integer.parseInt(Splitter[4]);
			}
		}
	}
	
	public List<String> getEventsPlayerList()
	{
		return _eventsPlayerList;
	}
	
	public static GameEventAntiAfk getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final GameEventAntiAfk INSTANCE = new GameEventAntiAfk();
	}
}
