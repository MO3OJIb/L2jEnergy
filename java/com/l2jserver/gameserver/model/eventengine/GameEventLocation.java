/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.data.IXmlReader;
import com.l2jserver.gameserver.model.Location;

/**
 * @author Мо3олЬ
 */
public class GameEventLocation implements IXmlReader
{
	protected static final Logger LOG = LoggerFactory.getLogger(GameEventLocation.class);
	
	private static Map<Integer, EventLocation> _teamLocs = new HashMap<>();
	
	private int _lastId;
	private int _lastMap;
	
	public static final int TEAM_BLUE = 0;
	public static final int TEAM_RED = 1;
	
	public GameEventLocation()
	{
		load();
	}
	
	@Override
	public void load()
	{
		_teamLocs.clear();
		parseDatapackFile("data/xml/events_locs.xml");
		LOG.info("Loaded {} Custom Event locations.", _teamLocs.size());
	}
	
	@Override
	public void parseDocument(Document doc)
	{
		for (var list = doc.getFirstChild(); list != null; list = list.getNextSibling())
		{
			if ("list".equalsIgnoreCase(list.getNodeName()))
			{
				for (var point = list.getFirstChild(); point != null; point = point.getNextSibling())
				{
					if ("id".equalsIgnoreCase(point.getNodeName()))
					{
						var PointMap = point.getAttributes();
						var locationName = PointMap.getNamedItem("locationName").getNodeValue();
						var locationId = Integer.parseInt(PointMap.getNamedItem("locationId").getNodeValue());
						
						var teamId = Integer.parseInt(PointMap.getNamedItem("teamId").getNodeValue());
						var x = Integer.parseInt(PointMap.getNamedItem("x").getNodeValue());
						var y = Integer.parseInt(PointMap.getNamedItem("y").getNodeValue());
						var z = Integer.parseInt(PointMap.getNamedItem("z").getNodeValue());
						
						EventLocation loc;
						
						_lastId = Integer.parseInt(PointMap.getNamedItem("locationId").getNodeValue());
						
						if (_teamLocs.containsKey(locationId))
						{
							loc = _teamLocs.get(locationId);
						}
						else
						{
							loc = new EventLocation(locationId, locationName);
							_teamLocs.put(loc.getId(), loc);
						}
						loc.addLocationToTeam(teamId, new Location(x, y, z));
					}
				}
			}
		}
	}
	
	public void clearMap()
	{
		_teamLocs.clear();
	}
	
	public EventLocation getLocation(int id)
	{
		return _teamLocs.get(id);
	}
	
	public int generateRandomMap()
	{
		var maps = new int[_teamLocs.size()];
		var i = 0;
		for (var map : _teamLocs.keySet())
		{
			maps[i++] = map;
		}
		var posId = _lastMap;
		while (posId == _lastMap)
		{
			posId = maps[Rnd.get(maps.length)];
		}
		return posId;
	}
	
	public int getLastId()
	{
		return _lastId;
	}
	
	public static class EventLocation
	{
		private final int _id;
		private final String _name;
		private final Map<Integer, List<Location>> _locations;
		private int _lastId;
		
		public EventLocation(int id, String name)
		{
			_id = id;
			_name = name;
			_locations = new HashMap<>();
			_locations.put(TEAM_BLUE, new ArrayList<>());
			_locations.put(TEAM_RED, new ArrayList<>());
		}
		
		public int getId()
		{
			return _id;
		}
		
		public String getName()
		{
			return _name;
		}
		
		public List<Location> getLocations()
		{
			List<Location> temp = new ArrayList<>();
			temp.addAll(_locations.get(TEAM_BLUE));
			temp.addAll(_locations.get(TEAM_RED));
			return temp;
		}
		
		public void addLocationToTeam(int team, Location loc)
		{
			_lastId++;
			if (loc.getInstanceId() == 0)
			{
				loc.setInstanceId(_lastId);
			}
			_locations.get(team).add(loc);
		}
		
		public List<Location> getLocationsForTeam(int team)
		{
			return _locations.get(team);
		}
		
		public Location getRandomLoc(int teamId)
		{
			Location cord = null;
			if (_locations.containsKey(teamId))
			{
				cord = _locations.get(teamId).get(Rnd.get(_locations.get(teamId).size()));
			}
			return cord;
		}
	}
	
	public static GameEventLocation getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final GameEventLocation INSTANCE = new GameEventLocation();
	}
}
