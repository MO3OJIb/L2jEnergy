/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.enums.events.MessageType;
import com.l2jserver.gameserver.model.eventengine.tasks.GameEventStartTask;
import com.l2jserver.gameserver.model.eventengine.types.DeathMatch;
import com.l2jserver.gameserver.model.eventengine.types.TeamVsTeam;
import com.l2jserver.gameserver.util.EventUtil;

/**
 * @author nuLL
 * @author Мо3олЬ
 */
public class GameEventManager
{
	protected static final Logger LOG = LoggerFactory.getLogger(GameEventManager.class);
	
	private final Map<String, GameEvent> _events = new HashMap<>();
	
	private GameEventStartTask _task;
	private String _currentEventName = "-";
	private String _nextEventInfo = "-";
	private GameEvent _event = null;
	
	protected GameEventManager()
	{
		registerEventEngine(new TeamVsTeam());
		registerEventEngine(new DeathMatch());
		
		scheduleNextEvent();
		LOG.info("Loaded Custom Event {} events.", _events.size());
	}
	
	public void registerEventEngine(GameEvent event)
	{
		event.init();
		nextEventStartTime(event);
		_events.put(event.getEventName(), event);
	}
	
	public GameEvent getEvent()
	{
		return _event;
	}
	
	public GameEvent getEventEngine(String name)
	{
		return _events.get(name);
	}
	
	public Collection<GameEvent> getAllEvents()
	{
		return _events.values();
	}
	
	public GameEvent findEvent(String name)
	{
		for (var event : _events.values())
		{
			if (name.equals(event.getEventName()))
			{
				return event;
			}
		}
		return null;
	}
	
	public GameEvent participantOf(GameEventPlayer player)
	{
		for (var event : getAllEvents())
		{
			if (event.addParticipant(player))
			{
				return event;
			}
		}
		return null;
	}
	
	public void startRegistration()
	{
		_currentEventName = _event.getEventName();
		_nextEventInfo = "-";
		_event.startParticipation();
		
		EventUtil.showAnnounceEvents("event_registration_opened", _event.getEventName() + "", MessageType.CUSTOM);
		EventUtil.showAnnounceEvents("event_registration_recruiting_levels", _event.getEventName() + "", _event.getConfigs().EVENT_MIN_LVL + "", _event.getConfigs().EVENT_MAX_LVL + "", MessageType.CUSTOM);
		EventUtil.showAnnounceEvents("event_registration_minutes", _event.getEventName() + "", _event.getConfigs().EVENT_PARTICIPATION_TIME + "", MessageType.CUSTOM);
		EventUtil.showAnnounceEvents("event_registration_alt_b", _event.getEventName(), MessageType.CUSTOM);
		
		_task.setStartTime(System.currentTimeMillis() + (60000L * getEvent().getConfigs().EVENT_PARTICIPATION_TIME));
		ThreadPoolManager.getInstance().executeGeneral(_task);
		LOG.info("Registration period started for event {}!", _event.getEventName());
	}
	
	public void startEvent()
	{
		if (!_event.startFight())
		{
			EventUtil.showAnnounceEvents("event_registration_cancelled", _event.getEventName(), MessageType.CUSTOM);
			LOG.info("Lack of registration, abort event {}!", _event.getEventName());
			scheduleNextEvent();
			_currentEventName = "-";
		}
		else
		{
			_event.sysMsgToAllParticipants("events_player_teleporting_participants_to_arena", _event.getConfigs().EVENT_START_LEAVE_TELEPORT_DELAY + "");
			_task.setStartTime(System.currentTimeMillis() + (60000L * getEvent().getConfigs().EVENT_RUNNING_TIME));
			ThreadPoolManager.getInstance().executeGeneral(_task);
			LOG.info("Registration to event {} is over! Event is starting in {} seconds!", _event.getEventName(), _event.getConfigs().EVENT_START_LEAVE_TELEPORT_DELAY);
		}
	}
	
	public void endEvent()
	{
		_event.calculateRewards();
		_event.sysMsgToAllParticipants("events_player_teleporting_to_back", getEvent().getConfigs().EVENT_START_LEAVE_TELEPORT_DELAY + "");
		_event.stopFight();
		scheduleNextEvent();
		_currentEventName = "-";
	}
	
	public Calendar nextEventStartTime(GameEvent event)
	{
		Calendar currentTime = Calendar.getInstance();
		Calendar nextStartTime = null;
		Calendar testStartTime = null;
		
		if (event.getConfigs().EVENT_TIME_INTERVAL != null)
		{
			for (String timeOfDay : event.getConfigs().EVENT_TIME_INTERVAL)
			{
				testStartTime = Calendar.getInstance();
				testStartTime.setLenient(true);
				String[] splitTimeOfDay = timeOfDay.split(":");
				testStartTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(splitTimeOfDay[0]));
				testStartTime.set(Calendar.MINUTE, Integer.parseInt(splitTimeOfDay[1]));
				
				if (testStartTime.getTimeInMillis() < currentTime.getTimeInMillis())
				{
					testStartTime.add(Calendar.DAY_OF_MONTH, 1);
				}
				
				if ((nextStartTime == null) || (testStartTime.getTimeInMillis() < nextStartTime.getTimeInMillis()))
				{
					nextStartTime = testStartTime;
				}
			}
		}
		return nextStartTime;
	}
	
	public void scheduleNextEvent()
	{
		if ((_events != null) && !_events.isEmpty())
		{
			Calendar tempNextStartTime = null;
			Calendar nextStartTime = null;
			
			for (GameEvent gameEvent : _events.values())
			{
				tempNextStartTime = nextEventStartTime(gameEvent);
				
				if (tempNextStartTime == null)
				{
					continue;
				}
				
				if (nextStartTime == null)
				{
					nextStartTime = tempNextStartTime;
					_event = gameEvent;
				}
				else
				{
					if (tempNextStartTime.getTimeInMillis() < nextStartTime.getTimeInMillis())
					{
						nextStartTime = tempNextStartTime;
						_event = gameEvent;
					}
				}
			}
			
			if (nextStartTime != null)
			{
				_nextEventInfo = _event.getEventName() + " will be started at " + TimeUtils.dateTimeFormat(nextStartTime.getTime());
				_task = new GameEventStartTask(nextStartTime.getTimeInMillis());
				ThreadPoolManager.getInstance().executeGeneral(_task);
				LOG.info("Loaded Next Custom Event {}", _nextEventInfo);
			}
		}
	}
	
	public void skipDelay()
	{
		if (_task.getNextRun().cancel(false))
		{
			_task.setStartTime(System.currentTimeMillis());
			ThreadPoolManager.getInstance().executeGeneral(_task);
		}
	}
	
	public String getCurrentEventName()
	{
		return _currentEventName;
	}
	
	public String getNextEventInfo()
	{
		return _nextEventInfo;
	}
	
	public static GameEventManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final GameEventManager INSTANCE = new GameEventManager();
	}
}
