/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine;

import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

public class GameEventPlayer implements Comparable<GameEventPlayer>
{
	private final L2PcInstance _player;
	private String _name;
	private int _kills;
	private int _deaths;
	private byte _credits;
	private int[] _originalCoordinates;
	
	protected GameEventPlayer(L2PcInstance player, byte playerCredits)
	{
		_player = player;
		_kills = 0;
		_deaths = 0;
		_credits = playerCredits;
	}
	
	public boolean isOnEvent()
	{
		return GameEventManager.getInstance().getEvent().isStarted() && GameEventManager.getInstance().getEvent().isParticipant(getPlayer().getObjectId());
	}
	
	public L2PcInstance getPlayer()
	{
		return _player;
	}
	
	public boolean isBlockingExit()
	{
		return true;
	}
	
	public boolean isBlockingDeathPenalty()
	{
		return true;
	}
	
	public boolean canRevive()
	{
		return false;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public int getKills()
	{
		return _kills;
	}
	
	public int increaseKills()
	{
		return ++_kills;
	}
	
	public int getDeaths()
	{
		return _deaths;
	}
	
	public int increaseDeaths()
	{
		return ++_deaths;
	}
	
	public byte getCredits()
	{
		return _credits;
	}
	
	public void decreaseCredits()
	{
		--_credits;
	}
	
	public int[] getOriginalCoordinates()
	{
		return _originalCoordinates;
	}
	
	public void setOriginalCoordinates(int[] originalCoordinates)
	{
		_originalCoordinates = originalCoordinates;
	}
	
	@Override
	public int compareTo(GameEventPlayer player)
	{
		var c1 = Integer.valueOf(player.getKills() - getKills());
		var c2 = Integer.valueOf(getDeaths() - player.getDeaths());
		var c3 = getPlayer().getName().compareTo(player.getPlayer().getName());
		
		if (c1 == 0)
		{
			if (c2 == 0)
			{
				return c3;
			}
			return c2;
		}
		return c1;
	}
}
