/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author HorridoJoho
 * @author nuLL
 * @author Мо3олЬ
 */
public class GameEventTeam
{
	private final byte _id;
	private final String _name;
	private final int[] _coordinates;
	private short _points;
	private final Map<Integer, GameEventPlayer> _participatedPlayers = new ConcurrentHashMap<>();
	
	public GameEventTeam(byte id, String name, int[] coordinates)
	{
		_id = id;
		_name = name;
		_coordinates = coordinates;
		_points = 0;
	}
	
	public boolean addPlayer(GameEventPlayer player)
	{
		if (player == null)
		{
			return false;
		}
		_participatedPlayers.put(player.getPlayer().getObjectId(), player);
		return true;
	}
	
	public void removePlayer(int playerId)
	{
		_participatedPlayers.remove(playerId);
	}
	
	public void increasePoints()
	{
		++_points;
	}
	
	public void cleanMe()
	{
		_participatedPlayers.clear();
		_points = 0;
	}
	
	public boolean containsPlayer(int playerId)
	{
		return _participatedPlayers.containsKey(playerId);
	}
	
	public byte getId()
	{
		return _id;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public int[] getCoordinates()
	{
		return _coordinates;
	}
	
	public int getPoints()
	{
		return _points;
	}
	
	public Map<Integer, GameEventPlayer> getParticipatedPlayers()
	{
		return _participatedPlayers;
	}
	
	public int getParticipatedPlayerCount()
	{
		return _participatedPlayers.size();
	}
	
	public GameEventPlayer getEventPlayer(int playerId)
	{
		return _participatedPlayers.get(playerId);
	}
}
