/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine;

import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.enums.DuelState;
import com.l2jserver.gameserver.enums.events.Team;

/**
 * @author nuLL
 * @author Мо3олЬ
 */
public class GameEventTeleporter implements Runnable
{
	private GameEventPlayer _player = null;
	private GameEvent _event = null;
	private int[] _coordinates = new int[4];
	
	public GameEventTeleporter(GameEventPlayer player, int[] coordinates, boolean fastSchedule)
	{
		_player = player;
		_event = _player.getPlayer().getEvent();
		_coordinates = coordinates;
		long delay = (_event.isStarted() ? _event.getConfigs().EVENT_RESPAWN_TELEPORT_DELAY : _event.getConfigs().EVENT_START_LEAVE_TELEPORT_DELAY) * 1000;
		ThreadPoolManager.getInstance().scheduleGeneral(this, fastSchedule ? 0 : delay);
	}
	
	@Override
	public void run()
	{
		if ((_player == null) || (_event == null))
		{
			return;
		}
		
		var player = _player.getPlayer();
		
		if (player == null)
		{
			return;
		}
		
		var summon = player.getSummon();
		
		if (summon != null)
		{
			summon.unSummon(player);
		}
		
		if ((_event.getConfigs().EVENT_EFFECTS_REMOVAL == 0) || ((_event.getConfigs().EVENT_EFFECTS_REMOVAL == 1) && ((player.getTeam() == Team.NONE) || (player.isInDuel() && (player.getDuelState() != DuelState.INTERRUPTED)))))
		{
			player.stopAllEffectsExceptThoseThatLastThroughDeath();
		}
		
		if (player.isInParty())
		{
			var party = player.getParty();
			party.removePartyMember(player, null);
		}
		
		if (player.isInDuel())
		{
			player.setDuelState(DuelState.INTERRUPTED);
		}
		
		if (player.getInstanceId() != 0)
		{
			player.setInstanceId(0);
		}
		
		if (_event.getEventInstance() != 0)
		{
			if (_event.isStarted())
			{
				player.setInstanceId(_event.getEventInstance());
			}
			else
			{
				player.setInstanceId(0);
			}
		}
		else
		{
			player.setInstanceId(0);
		}
		
		player.doRevive();
		
		player.teleToLocation((_coordinates[0] + Rnd.get(101)) - 50, (_coordinates[1] + Rnd.get(101)) - 50, _coordinates[2], _coordinates[3]);
		
		if (_event.isStarted())
		{
			if (_event instanceof TeamGameEvent teamGameEvent)
			{
				var team = teamGameEvent.getParticipantTeam(player.getObjectId());
				
				if (team != null)
				{
					if (teamGameEvent.getConfigs().EVENT_TEAM_ID_NAME_COORDINATES.size() <= 2)
					{
						player.setTeam(team.getId() == 1 ? Team.BLUE : Team.RED);
					}
				}
			}
			else if (_event instanceof IndividualGameEvent)
			{
				player.setTeam(Team.BLUE);
			}
			player.setCanRevive(false);
		}
		else
		{
			if (_event instanceof TeamGameEvent teamGameEvent)
			{
				if (teamGameEvent.getConfigs().EVENT_TEAM_ID_NAME_COORDINATES.size() <= 2)
				{
					player.setTeam(Team.NONE);
				}
			}
			else if (_event instanceof IndividualGameEvent)
			{
				player.setTeam(Team.NONE);
			}
			player.setCanRevive(true);
			player.removeEventListener(GameEventListener.class);
			player.setEvent(null); // TODO:
		}
		player.setCurrentCp(player.getMaxCp());
		player.setCurrentHp(player.getMaxHp());
		player.setCurrentMp(player.getMaxMp());
		
		player.broadcastStatusUpdate();
		player.broadcastUserInfo();
	}
}