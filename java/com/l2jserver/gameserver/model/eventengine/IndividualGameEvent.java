/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine;

import java.util.Map;
import java.util.TreeSet;

import com.l2jserver.gameserver.cache.HtmCache;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.events.MessageType;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.events.EventDispatcher;
import com.l2jserver.gameserver.model.events.impl.events.OnEventStart;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.network.serverpackets.CreatureSay;
import com.l2jserver.gameserver.network.serverpackets.ExShowScreenMessage;
import com.l2jserver.gameserver.network.serverpackets.L2GameServerPacket;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.util.EventUtil;

/**
 * @author nuLL
 * @author Мо3олЬ
 */
public abstract class IndividualGameEvent extends GameEvent
{
	protected Map<Integer, GameEventPlayer> _participantPlayers;
	
	@Override
	public abstract IndividualGameEventConfig getConfigs();
	
	@Override
	public boolean isParticipant(int playerId)
	{
		return _participantPlayers.containsKey(playerId);
	}
	
	@Override
	public int getCountPlayers()
	{
		return _participantPlayers.size();
	}
	
	@Override
	public boolean addParticipant(GameEventPlayer player)
	{
		if (_participantPlayers.containsKey(player.getPlayer().getObjectId()))
		{
			return false;
		}
		_participantPlayers.put(player.getPlayer().getObjectId(), player);
		player.getPlayer().setEvent(this);
		player.getPlayer().addEventListener(new GameEventListener(player.getPlayer()));
		return true;
	}
	
	@Override
	public boolean removeParticipant(L2PcInstance player)
	{
		if (!_participantPlayers.containsKey(player.getObjectId()))
		{
			return false;
		}
		
		_participantPlayers.remove(player.getObjectId());
		player.setEvent(null);
		
		var pl = L2World.getInstance().getPlayer(player.getObjectId());
		if (pl != null)
		{
			pl.removeEventListener(GameEventListener.class);
		}
		return true;
	}
	
	@Override
	public void teleportPlayersToArena()
	{
		for (var player : _participantPlayers.values())
		{
			if ((player != null) && (player.getPlayer() != null))
			{
				// Disabled skills by config.
				if (!getConfigs().EVENT_RESTRICT_SKILLS.isEmpty())
				{
					for (var skill : getConfigs().EVENT_RESTRICT_SKILLS)
					{
						if (player.getPlayer().getSkills().values().contains(skill.getSkill()))
						{
							player.getPlayer().disableSkill(skill.getSkill(), ((getConfigs().EVENT_RUNNING_TIME * 60) + getConfigs().EVENT_RESPAWN_TELEPORT_DELAY) * 1000);
						}
					}
				}
				
				var playerCoordinates = new int[]
				{
					player.getPlayer().getLocation().getX(),
					player.getPlayer().getLocation().getY(),
					player.getPlayer().getLocation().getZ(),
					player.getPlayer().getLocation().getHeading()
				};
				
				player.setOriginalCoordinates(playerCoordinates);
				
				new GameEventTeleporter(player, getConfigs().EVENT_PLAYER_COORDINATES, false);
				
				EventUtil.showEventMessage(player.getPlayer(), "status_started", null, MessageType.START);
			}
		}
		// Notify to scripts.
		EventDispatcher.getInstance().notifyEventAsync(new OnEventStart());
	}
	
	@Override
	public void teleportPlayersBack()
	{
		for (var player : _participantPlayers.values())
		{
			if ((player != null) && (player.getPlayer() != null))
			{
				EventUtil.showEventMessage(player.getPlayer(), "status_finished", null, MessageType.FINISH);
				
				showTopPos(player);
				
				new GameEventTeleporter(player, player.getOriginalCoordinates(), false);
			}
		}
	}
	
	public void showTopPos(GameEventPlayer player)
	{
		String htmltext = "";
		String[] topPositions = getFirstPosition(10);
		Boolean c = true;
		String c1 = "D9CC46";
		String c2 = "FFFFFF";
		if (topPositions != null)
		{
			for (int i = 0; i < topPositions.length; i++)
			{
				String color = (c ? c1 : c2);
				String[] row = topPositions[i].split("\\,");
				htmltext += "<tr>";
				htmltext += "<td width=\"50\" align=\"center\"><font color=\"" + color + "\">" + String.valueOf(i + 1) + "</font></td>";
				htmltext += "<td width=\"150\" align=\"center\"><font color=\"" + color + "\">" + row[0] + "</font></td>";
				htmltext += "<td width=\"70\" align=\"center\"><font color=\"" + color + "\">" + row[1] + "</font></td>";
				htmltext += "</tr>";
				c = !c;
			}
		}
		
		var html = new NpcHtmlMessage();
		html.setHtml(HtmCache.getInstance().getHtm(player.getPlayer(), "data/html/events/TopRank.html"));
		html.replace("%toprank%", htmltext);
		player.getPlayer().sendPacket(html);
	}
	
	@Override
	public void sysMsgToAllParticipants(String message, String string, String string2)
	{
		for (var player : _participantPlayers.values())
		{
			if ((player != null) && (player.getPlayer() != null))
			{
				player.getPlayer().sendPacket(new CreatureSay(0, ChatType.BATTLEFIELD, getEventName(), MessagesData.getInstance().getMessage(player.getPlayer(), message).replace("%s%", string + "").replace("%i%", string2 + "")));
			}
		}
	}
	
	@Override
	public void sysMsgToAllParticipants(String message, String string)
	{
		sysMsgToAllParticipants(message, string, null);
	}
	
	@Override
	public void sysMsgToAllParticipants(String message)
	{
		sysMsgToAllParticipants(message, null, null);
	}
	
	@Override
	public void broadcastPacketToTeams(L2GameServerPacket... packets)
	{
		for (var player : _participantPlayers.values())
		{
			if ((player != null) && (player.getPlayer() != null))
			{
				for (var packet : packets)
				{
					player.getPlayer().sendPacket(packet);
				}
			}
		}
	}
	
	@Override
	public void broadcastScoreMessage()
	{
		for (var player : _participantPlayers.values())
		{
			if ((player != null) && (player.getPlayer().getInstanceId() == getEventInstance()))
			{
				player.getPlayer().sendPacket(new ExShowScreenMessage("You Kills: " + player.getKills() + "", ExShowScreenMessage.BOTTOM_RIGHT, 15000));
			}
		}
	}
	
	public String[] getFirstPosition(int count)
	{
		TreeSet<GameEventPlayer> players = new TreeSet<>();
		players.addAll(_participantPlayers.values());
		String text = "";
		
		for (int i = 0; i < count; i++)
		{
			if (players.isEmpty())
			{
				break;
			}
			
			var player = players.first();
			
			if (player.getKills() == 0)
			{
				break;
			}
			
			text += player.getPlayer().getName() + "," + String.valueOf(player.getKills()) + ";";
			players.remove(player);
			
			int playerPointPrev = player.getKills();
			
			if (!getConfigs().EVENT_REWARD_TEAM_TIE)
			{
				continue;
			}
			
			while (!players.isEmpty())
			{
				player = players.first();
				if (player.getKills() != playerPointPrev)
				{
					break;
				}
				text += player.getPlayer().getName() + "," + String.valueOf(player.getKills()) + ";";
				players.remove(player);
			}
		}
		
		if (text != "")
		{
			return text.split("\\;");
		}
		return null;
	}
	
	@Override
	public boolean onAction(L2PcInstance player, int playerId)
	{
		if ((player == null) || !isStarted())
		{
			return true;
		}
		
		if (player.isGM())
		{
			return true;
		}
		
		if (!isParticipant(player.getObjectId()) && isParticipant(playerId))
		{
			return false;
		}
		
		if (isParticipant(player.getObjectId()) && !isParticipant(playerId))
		{
			return false;
		}
		return true;
	}
	
	@Override
	public void onLogin(L2PcInstance player)
	{
		if ((player == null) || (!isStarting() && !isStarted()))
		{
			return;
		}
		
		if (isParticipant(player.getObjectId()))
		{
			var eventPlayer = _participantPlayers.get(player.getObjectId());
			
			if (eventPlayer == null)
			{
				return;
			}
			
			new GameEventTeleporter(eventPlayer, getConfigs().EVENT_PLAYER_COORDINATES, true);
		}
	}
	
	@Override
	public boolean checkForEventsSkill(L2PcInstance player, L2PcInstance target, Skill skill)
	{
		if (!isStarted())
		{
			return true;
		}
		
		var sourcePlayerId = player.getObjectId();
		var targetPlayerId = target.getObjectId();
		var isSourceParticipant = isParticipant(sourcePlayerId);
		var isTargetParticipant = isParticipant(targetPlayerId);
		
		// both players not participating
		if (!isSourceParticipant && !isTargetParticipant)
		{
			return true;
		}
		// one player not participating
		if (!(isSourceParticipant && isTargetParticipant))
		{
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean eventTarget(L2PcInstance player, L2Character target)
	{
		var ownerTarget = player.getTarget();
		if (ownerTarget == null)
		{
			return true;
		}
		
		if (isStarted() && isParticipant(player.getObjectId()))
		{
			_participantPlayers.containsKey(player.getObjectId());
			
			if (ownerTarget.getActingPlayer() != null)
			{
				var tg = ownerTarget.getActingPlayer();
				if (_participantPlayers.containsKey(tg.getObjectId()) && !tg.isDead())
				{
					target = (L2Character) ownerTarget;
				}
			}
			return true;
		}
		return false;
	}
	
	@Override
	protected void clear()
	{
		_participantPlayers.clear();
	}
}
