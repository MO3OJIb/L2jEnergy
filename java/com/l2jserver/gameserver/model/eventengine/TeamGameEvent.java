/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine;

import java.util.ArrayList;
import java.util.List;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.events.EventState;
import com.l2jserver.gameserver.enums.events.MessageType;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.events.EventDispatcher;
import com.l2jserver.gameserver.model.events.impl.events.OnEventFinish;
import com.l2jserver.gameserver.model.events.impl.events.OnEventStart;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.network.serverpackets.CreatureSay;
import com.l2jserver.gameserver.network.serverpackets.ExShowScreenMessage;
import com.l2jserver.gameserver.network.serverpackets.L2GameServerPacket;
import com.l2jserver.gameserver.util.EventUtil;

/**
 * @author nuLL
 * @author Мо3олЬ
 */
public abstract class TeamGameEvent extends GameEvent
{
	protected GameEventTeam[] _teams;
	// protected short _rounds = 1; // TODO
	
	@Override
	public abstract TeamGameEventConfig getConfigs();
	
	@Override
	public boolean isParticipant(int playerId)
	{
		var isParticipant = false;
		for (var team : _teams)
		{
			if (team.containsPlayer(playerId))
			{
				isParticipant = true;
			}
		}
		return isParticipant;
	}
	
	@Override
	public int getCountPlayers()
	{
		var playersCount = 0;
		
		for (var team : _teams)
		{
			playersCount += team.getParticipatedPlayerCount();
		}
		
		return playersCount;
	}
	
	public GameEventTeam[] getTeams()
	{
		return _teams;
	}
	
	private GameEventTeam getTeamWithMinPlayers()
	{
		GameEventTeam teamWithMinPlayers = null;
		int players = getConfigs().EVENT_MAX_PLAYERS_IN_TEAMS;
		
		for (var team : _teams)
		{
			if (players > team.getParticipatedPlayerCount())
			{
				players = team.getParticipatedPlayerCount();
				teamWithMinPlayers = team;
			}
		}
		return teamWithMinPlayers;
	}
	
	public GameEventTeam getParticipantTeam(int playerId)
	{
		return (_teams[0].containsPlayer(playerId) ? _teams[0] : (_teams[1].containsPlayer(playerId) ? _teams[1] : null));
	}
	
	public GameEventTeam getParticipantEnemyTeam(int playerId)
	{
		return (_teams[0].containsPlayer(playerId) ? _teams[1] : (_teams[1].containsPlayer(playerId) ? _teams[0] : null));
	}
	
	@Override
	public boolean addParticipant(GameEventPlayer player)
	{
		if (getTeamWithMinPlayers().addPlayer(player))
		{
			player.getPlayer().setEvent(this);
			player.getPlayer().addEventListener(new GameEventListener(player.getPlayer()));
			return true;
		}
		return false;
	}
	
	@Override
	public boolean removeParticipant(L2PcInstance player)
	{
		var playerTeam = getParticipantTeam(player.getObjectId());
		
		if (playerTeam != null)
		{
			playerTeam.removePlayer(player.getObjectId());
			player.setEvent(null);
			var pl = L2World.getInstance().getPlayer(player.getObjectId());
			if (pl != null)
			{
				pl.removeEventListener(GameEventListener.class);
			}
			return true;
		}
		return false;
	}
	
	public int getParticipatedPlayersCount()
	{
		if (!isParticipating() && !isStarting() && !isStarted())
		{
			return 0;
		}
		return _teams[0].getParticipatedPlayerCount() + _teams[1].getParticipatedPlayerCount();
	}
	
	public String[] getTeamNames()
	{
		return new String[]
		{
			_teams[0].getName(),
			_teams[1].getName()
		};
	}
	
	public int[] getTeamsPlayerCounts()
	{
		return new int[]
		{
			_teams[0].getParticipatedPlayerCount(),
			_teams[1].getParticipatedPlayerCount()
		};
	}
	
	/**
	 * Returns points count of both teams
	 * @return int[]: points of teams, 2 elements, index 0 for team 1 and index 1 for team 2<br>
	 */
	public int[] getTeamsPoints()
	{
		return new int[]
		{
			_teams[0].getPoints(),
			_teams[1].getPoints()
		};
	}
	
	@Override
	public void teleportPlayersToArena()
	{
		for (var team : _teams)
		{
			team.getParticipatedPlayers().values().stream().filter(player -> (player != null) && (player.getPlayer() != null)).forEach(player ->
			{
				// Disabled skills by config.
				if (!getConfigs().EVENT_RESTRICT_SKILLS.isEmpty())
				{
					for (var skill : getConfigs().EVENT_RESTRICT_SKILLS)
					{
						if (player.getPlayer().getSkills().values().contains(skill.getSkill()))
						{
							player.getPlayer().disableSkill(skill.getSkill(), ((getConfigs().EVENT_RUNNING_TIME * 60) + getConfigs().EVENT_RESPAWN_TELEPORT_DELAY) * 1000);
						}
					}
				}
				
				var playerCoordinates = new int[]
				{
					player.getPlayer().getLocation().getX(),
					player.getPlayer().getLocation().getY(),
					player.getPlayer().getLocation().getZ(),
					player.getPlayer().getLocation().getHeading()
				};
				
				player.setOriginalCoordinates(playerCoordinates);
				player.getPlayer().setCanRevive(false);
				
				// Teleporter implements Runnable and starts itself
				new GameEventTeleporter(player, team.getCoordinates(), false);
				
				EventUtil.showEventMessage(player.getPlayer(), "status_started", null, MessageType.START);
				
				/*
				 * TODO // Кидаем рейд курс SkillData.getInstance().getSkill(4515, 1).applyEffects(player.getPlayer(), player.getPlayer()); player.getPlayer().sendPacket(new ExShowScreenMessage("Бой начнется через 1 минуту. Ожидайте старта!", 10000));
				 * ThreadPoolManager.getInstance().scheduleGeneral(() -> { player.getPlayer().stopAllEffects(); EventUtil.showEventMessage(player.getPlayer(), "status_started", null, MessageType.START); // AnnounceToPlayers(false, " Матч начался."); // AnnounceToPlayers(false, "Матч продлится " +
				 * Config.EVENT_TIME + " минут(ы)."); }, 60000);
				 */
				
			});
		}
		// Notify to scripts.
		EventDispatcher.getInstance().notifyEventAsync(new OnEventStart());
	}
	
	@Override
	public void teleportPlayersBack()
	{
		for (var team : _teams)
		{
			// Check for nullpointer
			team.getParticipatedPlayers().values().stream().filter(player -> (player != null) && (player.getPlayer() != null)).forEach(player ->
			{
				EventUtil.showEventMessage(player.getPlayer(), "status_finished", null, MessageType.FINISH);
				
				// Enable player revival.
				player.getPlayer().setCanRevive(true);
				// Untransform player.
				if (player.getPlayer().isTransformed())
				{
					player.getPlayer().untransform();
				}
				new GameEventTeleporter(player, player.getOriginalCoordinates(), false);
			});
		}
	}
	
	@Override
	public void calculateRewards()
	{
		List<GameEventTeam> winners = new ArrayList<>(getConfigs().EVENT_TEAM_ID_NAME_COORDINATES.size());
		GameEventTeam winnerTeam = null;
		int maxPoints = 0;
		
		for (var team : _teams)
		{
			if ((maxPoints == 0) && (team.getPoints() > 0))
			{
				maxPoints = team.getPoints();
				winnerTeam = team;
			}
			else if ((team.getPoints() == maxPoints) && (team.getPoints() > 0))
			{
				maxPoints = team.getPoints();
				winners.add(team);
				winners.add(winnerTeam);
				winnerTeam = null;
			}
			else if ((team.getPoints() > maxPoints))
			{
				maxPoints = team.getPoints();
				winnerTeam = team;
				winners.clear();
			}
		}
		
		setState(EventState.REWARDING);
		
		if (winnerTeam != null)
		{
			rewardTeam(winnerTeam);
			EventUtil.showAnnounceEvents("event_ended_team_won_with_kills", getEventName(), winnerTeam.getName(), String.valueOf(winnerTeam.getPoints()), MessageType.CUSTOM);
		}
		else if (winners.size() >= 2)
		{
			if (getConfigs().EVENT_REWARD_TEAM_TIE)
			{
				for (var team : winners)
				{
					rewardTeam(team);
				}
			}
			EventUtil.showAnnounceEvents("event_ended_with_both_teams_tying", getEventName(), MessageType.CUSTOM);
		}
		else if (winners.isEmpty())
		{
			EventUtil.showAnnounceEvents("event_ended_no_team_won_inactivity", getEventName(), MessageType.CUSTOM);
		}
		
		winners.clear();
		// Notify to scripts.
		EventDispatcher.getInstance().notifyEventAsync(new OnEventFinish());
	}
	
	public void rewardTeam(GameEventTeam team)
	{
		for (var player : team.getParticipatedPlayers().values())
		{
			if (player == null)
			{
				continue;
			}
			
			var playerInstance = player.getPlayer();
			
			if (playerInstance == null)
			{
				continue;
			}
			
			if (getConfigs().EVENT_REQUIRE_MIN_FRAGS_TO_REWARD && (player.getKills() < getConfigs().EVENT_MIN_FRAGS_TO_REWARD))
			{
				EventUtil.showEventMessage(playerInstance, "events_you_not_killed_enough_players", null, MessageType.CUSTOM);
				continue;
			}
			
			if (getConfigs().EVENT_REWARDS != null)
			{
				deliverRewards(playerInstance, getConfigs().EVENT_REWARDS);
			}
		}
	}
	
	@Override
	public void broadcastScoreMessage()
	{
		for (var team : _teams)
		{
			for (var playerInstance : team.getParticipatedPlayers().values())
			{
				if ((playerInstance != null) && (playerInstance.getPlayer().getInstanceId() == getEventInstance()))
				{
					playerInstance.getPlayer().sendPacket(new ExShowScreenMessage("Blue: " + _teams[0].getPoints() + " - Red: " + _teams[1].getPoints(), ExShowScreenMessage.BOTTOM_RIGHT, 15000));
				}
			}
		}
	}
	
	@Override
	public void broadcastPacketToTeams(L2GameServerPacket... packets)
	{
		for (var team : _teams)
		{
			team.getParticipatedPlayers().values().stream().filter(player -> (player != null) && (player.getPlayer() != null)).forEach(player ->
			{
				for (var packet : packets)
				{
					player.getPlayer().sendPacket(packet);
				}
			});
		}
	}
	
	@Override
	public void sysMsgToAllParticipants(String message, String string, String string2)
	{
		for (var team : _teams)
		{
			team.getParticipatedPlayers().values().stream().filter(player -> (player != null) && (player.getPlayer() != null)).forEach(player ->
			{
				player.getPlayer().sendPacket(new CreatureSay(0, ChatType.BATTLEFIELD, getEventName(), MessagesData.getInstance().getMessage(player.getPlayer(), message).replace("%s%", string + "").replace("%i%", string2 + "")));
			});
		}
	}
	
	@Override
	public void sysMsgToAllParticipants(String message, String string)
	{
		sysMsgToAllParticipants(message, string, null);
	}
	
	@Override
	public void sysMsgToAllParticipants(String message)
	{
		sysMsgToAllParticipants(message, null, null);
	}
	
	@Override
	public boolean onAction(L2PcInstance player, int playerId)
	{
		if ((player == null) || !isStarted())
		{
			return true;
		}
		
		if (player.isGM())
		{
			return true;
		}
		
		var playerTeam = getParticipantTeam(player.getObjectId());
		var targetedPlayerTeam = getParticipantTeam(playerId);
		
		if (((playerTeam != null) && (targetedPlayerTeam == null)) || ((playerTeam == null) && (targetedPlayerTeam != null)))
		{
			return false;
		}
		return (playerTeam == null) || (playerTeam != targetedPlayerTeam) || (player.getObjectId() == playerId) || getConfigs().EVENT_TARGET_TEAM_MEMBERS_ALLOWED;
	}
	
	@Override
	public boolean eventTarget(L2PcInstance player, L2Character target)
	{
		var ownerTarget = player.getTarget();
		if (ownerTarget == null)
		{
			return true;
		}
		
		if (isStarted() && isParticipant(player.getObjectId()))
		{
			var enemyTeam = getParticipantTeam(player.getObjectId());
			
			if (ownerTarget.getActingPlayer() != null)
			{
				var tg = ownerTarget.getActingPlayer();
				if (enemyTeam.containsPlayer(tg.getObjectId()) && !tg.isDead())
				{
					target = (L2Character) ownerTarget;
				}
			}
			return true;
		}
		return false;
	}
	
	@Override
	public final boolean checkForEventsSkill(L2PcInstance player, L2PcInstance target, Skill skill)
	{
		if (!isStarted())
		{
			return true;
		}
		
		var sourcePlayerId = player.getObjectId();
		var targetPlayerId = target.getObjectId();
		var isSourceParticipant = isParticipant(sourcePlayerId);
		var isTargetParticipant = isParticipant(targetPlayerId);
		
		// both players not participating
		if (!isSourceParticipant && !isTargetParticipant)
		{
			return true;
		}
		// one player not participating
		if (!(isSourceParticipant && isTargetParticipant))
		{
			return false;
		}
		// players in the different teams ?
		if (getParticipantTeam(sourcePlayerId) != getParticipantTeam(targetPlayerId))
		{
			if (!skill.isBad())
			{
				return false;
			}
		}
		return true;
	}
	
	@Override
	public void onLogin(L2PcInstance player)
	{
		if ((player == null) || (!isStarting() && !isStarted()))
		{
			return;
		}
		
		var team = getParticipantTeam(player.getObjectId());
		
		if (team == null)
		{
			return;
		}
		
		var eventPlayer = team.getEventPlayer(player.getObjectId());
		
		if (eventPlayer == null)
		{
			return;
		}
		new GameEventTeleporter(eventPlayer, team.getCoordinates(), true);
	}
	
	@Override
	protected void clear()
	{
		for (var team : _teams)
		{
			team.cleanMe();
		}
	}
}
