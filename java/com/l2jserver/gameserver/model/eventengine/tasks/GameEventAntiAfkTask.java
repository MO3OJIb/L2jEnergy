/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine.tasks;

import com.l2jserver.gameserver.model.eventengine.GameEventAntiAfk;
import com.l2jserver.gameserver.model.eventengine.GameEventManager;
import com.l2jserver.gameserver.model.eventengine.GameEventTeam;

/**
 * @author Мо3олЬ
 */
public class GameEventAntiAfkTask implements Runnable
{
	protected GameEventTeam[] _teams;
	
	@Override
	public void run()
	{
		if ((GameEventManager.getInstance().getEvent() != null) && GameEventManager.getInstance().getEvent().isStarted())
		{
			// Iterate over all teams
			for (var team : _teams)
			{
				// Iterate over all participated player instances in this team
				for (var playerInstance : team.getParticipatedPlayers().values())
				{
					if ((playerInstance != null) && playerInstance.getPlayer().isOnline())
					{
						GameEventAntiAfk.getInstance().setEventsSpawnInfo(playerInstance.getName(), playerInstance.getPlayer().getX(), playerInstance.getPlayer().getY(), playerInstance.getPlayer().getZ());
					}
				}
			}
		}
		else
		{
			GameEventAntiAfk.getInstance().getEventsPlayerList().clear();
		}
	}
}