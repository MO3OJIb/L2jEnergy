/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine.tasks;

import java.util.concurrent.ScheduledFuture;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.enums.events.MessageType;
import com.l2jserver.gameserver.model.eventengine.GameEventManager;
import com.l2jserver.gameserver.util.EventUtil;

/**
 * @author Мо3олЬ
 */
public class GameEventStartTask implements Runnable
{
	private long _startTime;
	private ScheduledFuture<?> _nextRun;
	
	public GameEventStartTask(long startTime)
	{
		_startTime = startTime;
	}
	
	public void setStartTime(long startTime)
	{
		_startTime = startTime;
	}
	
	@Override
	public void run()
	{
		int delay = (int) Math.round((_startTime - System.currentTimeMillis()) / 1000.0);
		
		if (delay > 0)
		{
			announce(delay);
		}
		
		int nextMsg = 0;
		if (delay > 3600)
		{
			nextMsg = delay - 3600;
		}
		else if (delay > 1800)
		{
			nextMsg = delay - 1800;
		}
		else if (delay > 900)
		{
			nextMsg = delay - 900;
		}
		else if (delay > 600)
		{
			nextMsg = delay - 600;
		}
		else if (delay > 300)
		{
			nextMsg = delay - 300;
		}
		else if (delay > 60)
		{
			nextMsg = delay - 60;
		}
		else if (delay > 5)
		{
			nextMsg = delay - 5;
		}
		else if (delay > 0)
		{
			nextMsg = delay;
		}
		else
		{
			if (GameEventManager.getInstance().getEvent().isInactive())
			{
				GameEventManager.getInstance().startRegistration();
			}
			else if (GameEventManager.getInstance().getEvent().isParticipating())
			{
				GameEventManager.getInstance().startEvent();
			}
			else
			{
				GameEventManager.getInstance().endEvent();
			}
		}
		
		if (delay > 0)
		{
			setNextRun(ThreadPoolManager.getInstance().scheduleGeneral(this, nextMsg * 1000));
		}
	}
	
	private void announce(long time)
	{
		if ((time >= 3600) && ((time % 3600) == 0))
		{
			if (GameEventManager.getInstance().getEvent().isParticipating())
			{
				EventUtil.showAnnounceEvents("event_hours_until_registration", GameEventManager.getInstance().getEvent().getEventName() + "", (time / 60 / 60) + "", MessageType.CUSTOM);
			}
			else if (GameEventManager.getInstance().getEvent().isStarted())
			{
				GameEventManager.getInstance().getEvent().sysMsgToAllParticipants("events_player_hours_until_finished", (time / 60 / 60) + "");
			}
		}
		else if (time >= 60)
		{
			if (GameEventManager.getInstance().getEvent().isParticipating())
			{
				EventUtil.showAnnounceEvents("event_minute_until_registration", GameEventManager.getInstance().getEvent().getEventName() + "", (time / 60) + "", MessageType.CUSTOM);
			}
			else if (GameEventManager.getInstance().getEvent().isStarted())
			{
				GameEventManager.getInstance().getEvent().sysMsgToAllParticipants("events_player_minute_until_finished", (time / 60) + "");
			}
		}
		else
		{
			if (GameEventManager.getInstance().getEvent().isParticipating())
			{
				EventUtil.showAnnounceEvents("event_seconds_until_registration", GameEventManager.getInstance().getEvent().getEventName() + "", time + "", MessageType.CUSTOM);
			}
			else if (GameEventManager.getInstance().getEvent().isStarted())
			{
				GameEventManager.getInstance().getEvent().sysMsgToAllParticipants("events_player_second_until_finished", time + "");
			}
		}
	}
	
	public ScheduledFuture<?> getNextRun()
	{
		return _nextRun;
	}
	
	public void setNextRun(ScheduledFuture<?> nextRun)
	{
		_nextRun = nextRun;
	}
}
