/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine.types;

import java.util.List;

import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.configuration.config.events.CtFConfig;
import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.enums.events.EventState;
import com.l2jserver.gameserver.enums.events.Team;
import com.l2jserver.gameserver.instancemanager.AntiFeedManager;
import com.l2jserver.gameserver.model.L2Spawn;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.L2Summon;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.eventengine.GameEventPlayer;
import com.l2jserver.gameserver.model.eventengine.GameEventTeam;
import com.l2jserver.gameserver.model.eventengine.GameEventTeleporter;
import com.l2jserver.gameserver.model.eventengine.TeamGameEvent;
import com.l2jserver.gameserver.model.events.EventDispatcher;
import com.l2jserver.gameserver.model.events.impl.events.OnEventKill;
import com.l2jserver.gameserver.model.itemcontainer.Inventory;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.gameserver.network.serverpackets.ExShowScreenMessage;
import com.l2jserver.gameserver.util.EventUtil;

/**
 * @author Мо3олЬ
 */
public class CaptureTheFlag extends TeamGameEvent
{
	private static L2Spawn redFlag = null;
	private static L2Spawn blueFlag = null;
	
	private static final int BLUE_FLAG_ID = 13560;
	private static final int RED_FLAG_ID = 13561;
	
	private static final int BLUE_NPC_ID = 35426;
	private static final int RED_NPC_ID = 35423;
	
	@Override
	public CtFConfig getConfigs()
	{
		return CtFConfig.getInstance();
	}
	
	@Override
	public void init()
	{
		setEventName("Capture the Flag");
		AntiFeedManager.getInstance().registerEvent(AntiFeedManager.CUSTOM_EVENT_ID);
		List<GameEventTeam> teamsConfig = getConfigs().EVENT_TEAM_ID_NAME_COORDINATES;
		_teams = new GameEventTeam[teamsConfig.size()];
		for (int i = 0; i < teamsConfig.size(); i++)
		{
			_teams[i] = new GameEventTeam(teamsConfig.get(i).getId(), teamsConfig.get(i).getName(), teamsConfig.get(i).getCoordinates());
		}
		
		setState(EventState.INACTIVE);
	}
	
	@Override
	public boolean startFight()
	{
		// Set state to STARTING
		setState(EventState.STARTING);
		if (getCountPlayers() >= getConfigs().EVENT_MIN_PLAYERS_IN_TEAMS)
		{
			if (getConfigs().EVENT_IN_INSTANCE)
			{
				createInstance();
			}
			setState(EventState.STARTED);
			closeDoors();
			spawnFlags();
			teleportPlayersToArena();
			return true;
		}
		clear();
		setState(EventState.INACTIVE);
		AntiFeedManager.getInstance().clear(AntiFeedManager.CUSTOM_EVENT_ID);
		return false;
	}
	
	@Override
	public void stopFight()
	{
		// Set state INACTIVATING
		setState(EventState.INACTIVATING);
		// Opens all doors specified in configs
		openDoors();
		// Reset flag carriers
		teleportPlayersBack();
		clear();
		setState(EventState.INACTIVE);
		AntiFeedManager.getInstance().clear(AntiFeedManager.CUSTOM_EVENT_ID);
	}
	
	private L2Spawn spawnFlag(int teamId)
	{
		return EventUtil.spawnSingle(teamId == 1 ? BLUE_NPC_ID : RED_NPC_ID, getConfigs().FLAGS.get(teamId), getEventInstance());
	}
	
	private void dropFlag(L2PcInstance player, boolean onBase)
	{
		if ((player != null) && player.isCombatFlagEquipped())
		{
			L2ItemInstance flag = player.getActiveWeaponInstance();
			
			if (flag != null)
			{
				removeFlag(player);
				
				if (flag.getId() == BLUE_FLAG_ID)
				{
					getConfigs();
					redFlag.getLastSpawn().setLocationInvisible(onBase ? getConfigs().FLAGS.get(2) : player.getLocation());
					redFlag.getLastSpawn().decayMe();
					redFlag.getLastSpawn().spawnMe();
				}
				else if (flag.getId() == RED_FLAG_ID)
				{
					getConfigs();
					blueFlag.getLastSpawn().setLocationInvisible(onBase ? getConfigs().FLAGS.get(1) : player.getLocation());
					blueFlag.getLastSpawn().decayMe();
					blueFlag.getLastSpawn().spawnMe();
				}
			}
		}
	}
	
	private void equipFlag(L2PcInstance player)
	{
		L2ItemInstance weaponRightHand = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);
		L2ItemInstance weaponLeftHand = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LHAND);
		
		if (weaponRightHand != null)
		{
			player.getInventory().unEquipItemInSlotAndRecord(Inventory.PAPERDOLL_RHAND);
		}
		
		if (weaponLeftHand != null)
		{
			player.getInventory().unEquipItemInSlotAndRecord(Inventory.PAPERDOLL_LHAND);
		}
		
		int flagId = player.getTeam() == Team.BLUE ? BLUE_FLAG_ID : RED_FLAG_ID;
		
		player.getInventory().equipItem(ItemTable.getInstance().createItem("CtF Event", flagId, 1, player, null));
		
		if (flagId == BLUE_FLAG_ID)
		{
			redFlag.getLastSpawn().decayMe();
		}
		else
		{
			blueFlag.getLastSpawn().decayMe();
		}
		
		player.getInventory().blockAllItems();
		
		player.broadcastUserInfo();
		
		player.sendPacket(new ExShowScreenMessage("You got the enemy's flag! Run back!", 5000));
	}
	
	private void removeFlag(L2PcInstance player)
	{
		if ((player != null) && player.isCombatFlagEquipped())
		{
			L2ItemInstance flag = player.getActiveWeaponInstance();
			if ((flag != null) && ((flag.getId() == BLUE_FLAG_ID) || (flag.getId() == RED_FLAG_ID)))
			{
				player.getInventory().unblock();
				player.getInventory().destroyItem("CtF Event", flag, player, null);
				player.broadcastUserInfo();
			}
		}
	}
	
	private void flagToBase(L2PcInstance player)
	{
		dropFlag(player, true);
		
		GameEventTeam team = getParticipantTeam(player.getObjectId());
		
		if (team != null)
		{
			team.increasePoints();
			
			sysMsgToAllParticipants("Player: " + player.getName() + " has scored for the " + team.getName() + " team!");
		}
	}
	
	public void spawnFlags()
	{
		for (GameEventTeam team : _teams)
		{
			if (team.getId() == 1)
			{
				blueFlag = spawnFlag(team.getId());
			}
			else
			{
				redFlag = spawnFlag(team.getId());
			}
		}
	}
	
	@Override
	public void onKill(L2Character character, L2PcInstance player)
	{
		if ((player == null) || !isStarted())
		{
			return;
		}
		
		GameEventTeam killedTeam = getParticipantTeam(player.getObjectId());
		
		if (killedTeam == null)
		{
			return;
		}
		
		GameEventPlayer killedPlayer = killedTeam.getEventPlayer(player.getObjectId());
		
		if ((killedPlayer == null) || (character == null))
		{
			return;
		}
		
		L2PcInstance killerPlayer = null;
		
		if (character.isSummon())
		{
			killerPlayer = ((L2Summon) character).getOwner();
			
			if (killerPlayer == null)
			{
				return;
			}
		}
		else if (character.isPlayer())
		{
			killerPlayer = (L2PcInstance) character;
		}
		else
		{
			return;
		}
		
		GameEventTeam killerTeam = getParticipantTeam(killerPlayer.getObjectId());
		
		if (killerTeam == null)
		{
			return;
		}
		
		if (getConfigs().EVENT_REQUIRE_MIN_FRAGS_TO_REWARD)
		{
			GameEventPlayer killerEventPlayer = killerTeam.getEventPlayer(killerPlayer.getObjectId());
			killerEventPlayer.increaseKills();
		}
		
		dropFlag(player, true);
		
		new GameEventTeleporter(killedPlayer, killedTeam.getCoordinates(), false);
		
		// Notify to scripts.
		EventDispatcher.getInstance().notifyEventAsync(new OnEventKill(killerPlayer, player, killerTeam));
		
		if (getConfigs().EVENT_ENABLED_KILL_BONUS)
		{
			giveKillBonus(killedPlayer);
		}
	}
	
	@Override
	public boolean talkWithNpc(L2PcInstance player, L2Npc npc)
	{
		if ((getState() == EventState.STARTED) && (player != null) && isParticipant(player.getObjectId()))
		{
			if (npc.getId() == BLUE_NPC_ID)
			{
				if (player.isCombatFlagEquipped() && (player.getTeam() == Team.BLUE))
				{
					flagToBase(player);
				}
				else if (!player.isCombatFlagEquipped() && (player.getTeam() == Team.RED) && npc.isVisible())
				{
					equipFlag(player);
				}
				return true;
			}
			
			if (npc.getId() == RED_NPC_ID)
			{
				if (player.isCombatFlagEquipped() && (player.getTeam() == Team.RED))
				{
					flagToBase(player);
				}
				else if (!player.isCombatFlagEquipped() && (player.getTeam() == Team.BLUE) && npc.isVisible())
				{
					equipFlag(player);
				}
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void onLogout(L2PcInstance player)
	{
		if ((player != null) && (isStarted() || isParticipating()))
		{
			dropFlag(player, true);
			
			if (removeParticipant(player))
			{
				player.setXYZInvisible((83447 + Rnd.get(101)) - 50, (148638 + Rnd.get(101)) - 50, -3400); // Giran
			}
		}
	}
	
	@Override
	protected void clear()
	{
		for (GameEventTeam team : _teams)
		{
			team.getParticipatedPlayers().values().stream().filter(player -> (player != null) && (player.getPlayer() != null)).forEach(player ->
			{
				removeFlag(player.getPlayer());
			});
		}
		super.clear();
	}
}
