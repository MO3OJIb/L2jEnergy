/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine.types;

/**
 * @author Мо3олЬ
 */
public final class ChaosMatch // extends IndividualGameEvent
{
	/*
	 * @Override public boolean startFight() { // Set state to STARTING setState(GameEventState.STARTING); if (getCountPlayers() >= getConfigs().EVENT_MIN_PLAYERS_IN_TEAMS) { if (getConfigs().EVENT_IN_INSTANCE) { createInstance(); } closeDoors(); setState(GameEventState.STARTED); addSuperHaste();
	 * teleportPlayersToArena(); return true; } clear(); setState(GameEventState.INACTIVE); AntiFeedManager.getInstance().clear(AntiFeedManager.CUSTOM_EVENT_ID); return false; }
	 * @Override public void stopFight() { // Set state INACTIVATING setState(GameEventState.INACTIVATING); removeSuperHaste(); // Opens all doors specified in configs openDoors(); teleportPlayersBack(); clear(); setState(GameEventState.INACTIVE);
	 * AntiFeedManager.getInstance().clear(AntiFeedManager.CUSTOM_EVENT_ID); } public void addSuperHaste() { for (GameEventPlayer player : _participantPlayers.values()) { if ((player != null) && (player.getPlayer() != null)) { Skill skill = SkillData.getInstance().getSkill(7029, 4); if (skill !=
	 * null) { skill.applyEffects(player.getPlayer(), player.getPlayer()); } } } } public void removeSuperHaste() { // TODO }
	 */
}
