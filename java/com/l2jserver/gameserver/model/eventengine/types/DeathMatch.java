/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine.types;

import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import com.l2jserver.gameserver.configuration.config.events.DMConfig;
import com.l2jserver.gameserver.enums.events.EventState;
import com.l2jserver.gameserver.enums.events.MessageType;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.instancemanager.AntiFeedManager;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.L2Summon;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.eventengine.GameEventPlayer;
import com.l2jserver.gameserver.model.eventengine.GameEventTeleporter;
import com.l2jserver.gameserver.model.eventengine.IndividualGameEvent;
import com.l2jserver.gameserver.model.events.EventDispatcher;
import com.l2jserver.gameserver.model.events.impl.events.OnEventFinish;
import com.l2jserver.gameserver.model.events.impl.events.OnEventKill;
import com.l2jserver.gameserver.network.serverpackets.CreatureSay;
import com.l2jserver.gameserver.util.EventUtil;

public final class DeathMatch extends IndividualGameEvent
{
	@Override
	public DMConfig getConfigs()
	{
		return DMConfig.getInstance();
	}
	
	@Override
	public void init()
	{
		setEventName("Death Match");
		AntiFeedManager.getInstance().registerEvent(AntiFeedManager.CUSTOM_EVENT_ID);
		
		_participantPlayers = new HashMap<>();
		setState(EventState.INACTIVE);
	}
	
	@Override
	public void calculateRewards()
	{
		TreeSet<GameEventPlayer> players = new TreeSet<>();
		players.addAll(_participantPlayers.values());
		
		for (int i = 0; i < 3; i++)
		{
			if (players.isEmpty())
			{
				break;
			}
			
			GameEventPlayer player = players.first();
			
			if (player.getKills() == 0)
			{
				break;
			}
			
			reward(player, i + 1);
			players.remove(player);
			
			int playerPointPrev = player.getKills();
			
			if (getConfigs().EVENT_REWARD_TEAM_TIE)
			{
				continue;
			}
			
			while (!players.isEmpty())
			{
				player = players.first();
				
				if (player.getKills() != playerPointPrev)
				{
					break;
				}
				
				reward(player, i + 1);
				players.remove(player);
			}
		}
		
		// Set state REWARDING so nobody can point anymore
		setState(EventState.REWARDING);
		
		// Notify to scripts.
		EventDispatcher.getInstance().notifyEventAsync(new OnEventFinish());
		
		String[] topPositions;
		topPositions = getFirstPosition(3);
		
		if (topPositions != null)
		{
			EventUtil.showAnnounceEvents("event_winners", getEventName() + "", MessageType.CUSTOM);
			for (String topPosition : topPositions)
			{
				String[] row = topPosition.split("\\,");
				EventUtil.showAnnounceEvents("event_winners_with_kills", getEventName() + "", row[0] + "", row[1] + "", MessageType.CUSTOM);
			}
		}
		else
		{
			EventUtil.showAnnounceEvents("event_no_winners_match", getEventName() + "", MessageType.CUSTOM);
		}
		EventUtil.showAnnounceEvents("event_thanks_match", getEventName() + "", MessageType.CUSTOM);
	}
	
	private void reward(GameEventPlayer eventPlayer, int pos)
	{
		L2PcInstance player = eventPlayer.getPlayer();
		
		if (player == null)
		{
			return;
		}
		
		List<int[]> rewards = null;
		
		if (pos == 1)
		{
			rewards = getConfigs().REWARDS_1ST_PLACE;
		}
		else if (pos == 2)
		{
			rewards = getConfigs().REWARDS_2ND_PLACE;
		}
		else if (pos == 3)
		{
			rewards = getConfigs().REWARDS_3RD_PLACE;
		}
		else
		{
			return;
		}
		
		if (rewards != null)
		{
			deliverRewards(player, rewards);
		}
	}
	
	@Override
	public void onKill(L2Character killerCharacter, L2PcInstance killedPlayerInstance)
	{
		if ((killedPlayerInstance == null) || !isStarted())
		{
			return;
		}
		
		if (!isParticipant(killedPlayerInstance.getObjectId()))
		{
			return;
		}
		
		GameEventPlayer killedEventPlayer = _participantPlayers.get(killedPlayerInstance.getObjectId());
		
		if (killedEventPlayer == null)
		{
			return;
		}
		
		new GameEventTeleporter(killedEventPlayer, getConfigs().EVENT_PLAYER_COORDINATES, false);
		
		if (killerCharacter == null)
		{
			return;
		}
		
		L2PcInstance killerPlayerInstance = null;
		
		if (killerCharacter.isSummon())
		{
			killerPlayerInstance = ((L2Summon) killerCharacter).getOwner();
			
			if (killerPlayerInstance == null)
			{
				return;
			}
		}
		else if (killerCharacter.isPlayer())
		{
			killerPlayerInstance = (L2PcInstance) killerCharacter;
		}
		else
		{
			return;
		}
		
		if (isParticipant(killerPlayerInstance.getObjectId()))
		{
			GameEventPlayer killerEventPlayer = _participantPlayers.get(killerPlayerInstance.getObjectId());
			
			killerEventPlayer.increaseKills();
			killerPlayerInstance.sendPacket(new CreatureSay(killerPlayerInstance.getObjectId(), ChatType.PARTYROOM_ALL, killerPlayerInstance.getName(), "I have killed " + killedPlayerInstance.getName() + "!"));
			
			killedEventPlayer.increaseDeaths();
			killedPlayerInstance.sendPacket(new CreatureSay(killerPlayerInstance.getObjectId(), ChatType.PARTYROOM_ALL, killerPlayerInstance.getName(), "I killed you!"));
			
			broadcastScoreMessage();
			// Notify to scripts.
			EventDispatcher.getInstance().notifyEventAsync(new OnEventKill(killerPlayerInstance, killedPlayerInstance, null));
		}
		
		if (getConfigs().EVENT_ENABLED_KILL_BONUS)
		{
			giveKillBonus(killedEventPlayer);
		}
	}
}
