/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine.types;

import java.util.ArrayList;
import java.util.List;

import com.l2jserver.gameserver.configuration.config.events.FMConfig;
import com.l2jserver.gameserver.data.xml.impl.NpcData;
import com.l2jserver.gameserver.data.xml.impl.SpawnData;
import com.l2jserver.gameserver.enums.events.EventState;
import com.l2jserver.gameserver.instancemanager.AntiFeedManager;
import com.l2jserver.gameserver.model.L2Spawn;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.L2Summon;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.eventengine.GameEventPlayer;
import com.l2jserver.gameserver.model.eventengine.GameEventTeam;
import com.l2jserver.gameserver.model.eventengine.GameEventTeleporter;
import com.l2jserver.gameserver.model.eventengine.TeamGameEvent;
import com.l2jserver.gameserver.model.events.EventDispatcher;
import com.l2jserver.gameserver.model.events.impl.events.OnEventKill;
import com.l2jserver.gameserver.util.Util;

/**
 * @author Мо3олЬ
 */
public class FootballMatch extends TeamGameEvent
{
	private final List<L2Spawn> _posters = new ArrayList<>();
	private L2Spawn _ball = null;
	
	@Override
	public FMConfig getConfigs()
	{
		return FMConfig.getInstance();
	}
	
	@Override
	public void init()
	{
		setEventName("Football Match");
		AntiFeedManager.getInstance().registerEvent(AntiFeedManager.CUSTOM_EVENT_ID);
		
		List<GameEventTeam> teamsConfig = getConfigs().EVENT_TEAM_ID_NAME_COORDINATES;
		_teams = new GameEventTeam[teamsConfig.size()];
		for (int i = 0; i < teamsConfig.size(); i++)
		{
			_teams[i] = new GameEventTeam(teamsConfig.get(i).getId(), teamsConfig.get(i).getName(), teamsConfig.get(i).getCoordinates());
		}
		
		setState(EventState.INACTIVE);
	}
	
	@Override
	public boolean startFight()
	{
		// Set state to STARTING
		setState(EventState.STARTING);
		if (getCountPlayers() >= getConfigs().EVENT_MIN_PLAYERS_IN_TEAMS)
		{
			if (getConfigs().EVENT_IN_INSTANCE)
			{
				createInstance();
			}
			closeDoors();
			setState(EventState.STARTED);
			// Porter 1
			spawnPoster(150480, 46485, -3411);
			spawnPoster(150480, 46945, -3411);
			// Porter 2
			spawnPoster(148491, 46955, -3415);
			spawnPoster(148491, 46478, -3415);
			// Ball
			spawnBall();
			teleportPlayersToArena();
			return true;
		}
		clear();
		setState(EventState.INACTIVE);
		AntiFeedManager.getInstance().clear(AntiFeedManager.CUSTOM_EVENT_ID);
		return false;
	}
	
	@Override
	public void stopFight()
	{
		// Set state INACTIVATING
		setState(EventState.INACTIVATING);
		removePoster();
		// Quitamos balon
		removeBall();
		// Opens all doors specified in configs
		openDoors();
		teleportPlayersBack();
		clear();
		_posters.clear();
		_ball = null;
		setState(EventState.INACTIVE);
		AntiFeedManager.getInstance().clear(AntiFeedManager.CUSTOM_EVENT_ID);
	}
	
	private void spawnPoster(int x, int y, int z)
	{
		try
		{
			L2Spawn spawn = new L2Spawn(NpcData.getInstance().getTemplate(getConfigs().POSTER_NPC_ID));
			spawn.setX(x);
			spawn.setY(y);
			spawn.setZ(z);
			spawn.setAmount(1);
			spawn.setInstanceId(0);
			spawn.setRespawnDelay(1);
			SpawnData.getInstance().addNewSpawn(spawn, false);
			spawn.init();
			spawn.getLastSpawn().setName("Poster");
			spawn.getLastSpawn().setTitle("Football Match");
			_posters.add(spawn);
		}
		catch (Exception ex)
		{
			LOG.warn("Error on spawn Npc Poster!", ex);
		}
	}
	
	private void spawnBall()
	{
		try
		{
			L2Spawn spawn = new L2Spawn(NpcData.getInstance().getTemplate(getConfigs().BALL_NPC_ID));
			spawn.setX(149372);
			spawn.setY(46749);
			spawn.setZ(-3413);
			spawn.setAmount(1);
			spawn.setInstanceId(0);
			spawn.setRespawnDelay(1);
			SpawnData.getInstance().addNewSpawn(spawn, false);
			spawn.init();
			spawn.getLastSpawn().setName("Ball");
			spawn.getLastSpawn().setTitle("Football Match");
			spawn.getLastSpawn().setIsInvul(true);
			spawn.getLastSpawn().setIsImmobilized(true);
			_ball = spawn;
		}
		catch (Exception ex)
		{
			LOG.warn("Error on spawn Npc Ball!", ex);
		}
	}
	
	private void removePoster()
	{
		for (L2Spawn spawn : _posters)
		{
			spawn.getLastSpawn().decayMe();
		}
	}
	
	private void removeBall()
	{
		_ball.getLastSpawn().decayMe();
	}
	
	@SuppressWarnings("unused")
	private void isInPorter(L2Npc ball)
	{
		for (GameEventTeam team : _teams)
		{
			if ((ball.getX() > 150480) && (ball.getY() > 46485) && (ball.getY() < 46945))
			{
				_teams[0].increasePoints();
				// Broadcast.announceToOnlinePlayers(getEventName() + ": The team " + team.getName() + " scored a goal."); TODO: fix
				// EventUtil.showAnnounceEvents("event_ended_with_both_teams_tying", getEventName(), MessageType.CUSTOM);
				ball.teleToLocation(149372, 46749, -3413, false);
			}
			else if ((ball.getX() < 148491) && (ball.getY() < 46955) && (ball.getY() > 46478))
			{
				_teams[1].increasePoints();
				// Broadcast.announceToOnlinePlayers(getEventName() + ": The team " + team.getName() + " scored a goal."); TODO: fix
				ball.teleToLocation(149372, 46749, -3413, false);
			}
		}
	}
	
	@Override
	public void onAttack(L2PcInstance player, L2Npc npc)
	{
		double degree = Util.convertHeadingToDegree(player.getHeading());
		if (npc.getId() == getConfigs().BALL_NPC_ID)
		{
			if ((degree >= 357) || ((degree >= 0) && (degree <= 10)))
			{
				npc.teleToLocation(npc.getX() + 100, npc.getY(), npc.getZ(), false);
			}
			else if ((degree > 10) && (degree <= 50))
			{
				npc.teleToLocation(npc.getX() + 100, npc.getY() + 100, npc.getZ(), false);
			}
			else if ((degree > 50) && (degree <= 100))
			{
				npc.teleToLocation(npc.getX(), npc.getY() + 100, npc.getZ(), false);
			}
			else if ((degree > 100) && (degree <= 160))
			{
				npc.teleToLocation(npc.getX() - 100, npc.getY() + 100, npc.getZ(), false);
			}
			else if ((degree > 160) && (degree <= 190))
			{
				npc.teleToLocation(npc.getX() - 100, npc.getY(), npc.getZ(), false);
			}
			else if ((degree > 190) && (degree <= 250))
			{
				npc.teleToLocation(npc.getX() - 100, npc.getY() - 100, npc.getZ(), false);
			}
			else if ((degree > 250) && (degree <= 280))
			{
				npc.teleToLocation(npc.getX(), npc.getY() - 100, npc.getZ(), false);
			}
			else if ((degree > 280) && (degree < 357))
			{
				npc.teleToLocation(npc.getX() + 100, npc.getY() - 100, npc.getZ(), false);
			}
			isInPorter(npc);
		}
	}
	
	@Override
	public void onKill(L2Character character, L2PcInstance player)
	{
		if ((player == null) || !isStarted())
		{
			return;
		}
		
		GameEventTeam killedTeam = getParticipantTeam(player.getObjectId());
		
		if (killedTeam == null)
		{
			return;
		}
		
		GameEventPlayer killedPlayer = killedTeam.getEventPlayer(player.getObjectId());
		
		if ((killedPlayer == null) || (character == null))
		{
			return;
		}
		
		L2PcInstance killerPlayer = null;
		
		if (character.isSummon())
		{
			killerPlayer = ((L2Summon) character).getOwner();
			
			if (killerPlayer == null)
			{
				return;
			}
		}
		else if (character.isPlayer())
		{
			killerPlayer = (L2PcInstance) character;
		}
		else
		{
			return;
		}
		
		new GameEventTeleporter(killedPlayer, killedTeam.getCoordinates(), false);
		
		// Notify to scripts.
		EventDispatcher.getInstance().notifyEventAsync(new OnEventKill(killerPlayer, player, null));
	}
}
