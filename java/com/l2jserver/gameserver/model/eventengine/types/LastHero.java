/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine.types;

import java.util.HashMap;
import java.util.TreeSet;

import com.l2jserver.gameserver.configuration.config.events.LHConfig;
import com.l2jserver.gameserver.enums.events.EventState;
import com.l2jserver.gameserver.enums.events.MessageType;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.instancemanager.AntiFeedManager;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.L2Summon;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.eventengine.GameEventManager;
import com.l2jserver.gameserver.model.eventengine.GameEventPlayer;
import com.l2jserver.gameserver.model.eventengine.GameEventTeleporter;
import com.l2jserver.gameserver.model.eventengine.IndividualGameEvent;
import com.l2jserver.gameserver.model.events.EventDispatcher;
import com.l2jserver.gameserver.model.events.impl.events.OnEventFinish;
import com.l2jserver.gameserver.model.events.impl.events.OnEventKill;
import com.l2jserver.gameserver.network.serverpackets.CreatureSay;
import com.l2jserver.gameserver.util.EventUtil;

public class LastHero extends IndividualGameEvent
{
	@Override
	public LHConfig getConfigs()
	{
		return LHConfig.getInstance();
	}
	
	@Override
	public void init()
	{
		setEventName("Last Hero");
		AntiFeedManager.getInstance().registerEvent(AntiFeedManager.CUSTOM_EVENT_ID);
		_participantPlayers = new HashMap<>();
		setState(EventState.INACTIVE);
	}
	
	@Override
	public void calculateRewards()
	{
		TreeSet<GameEventPlayer> players = new TreeSet<>();
		players.addAll(_participantPlayers.values());
		
		if (!getConfigs().EVENT_REWARD_TEAM_TIE && (getCountPlayers() > 1))
		{
			// return "Ended, thanks to everyone who participated!\nWe did not have winners!"; TODO: add
		}
		
		for (int i = 0; i < players.size(); i++)
		{
			GameEventPlayer player = players.first();
			
			if ((player.getCredits() == 0) || (player.getKills() == 0))
			{
				break;
			}
			
			if (getConfigs().EVENT_REWARDS != null)
			{
				deliverRewards(player.getPlayer(), getConfigs().EVENT_REWARDS);
			}
			
			players.remove(player);
			
			EventUtil.showAnnounceEvents("event_winners", getEventName() + "", MessageType.CUSTOM);
			EventUtil.showAnnounceEvents("event_winners_with_kills", getEventName() + "", player.getPlayer().getName() + "", player.getKills() + "", MessageType.CUSTOM);
			
			if (!getConfigs().EVENT_REWARD_TEAM_TIE)
			{
				break;
			}
		}
		
		setState(EventState.REWARDING);
		// Notify to scripts.
		EventDispatcher.getInstance().notifyEventAsync(new OnEventFinish());
		
		EventUtil.showAnnounceEvents("event_thanks_match", getEventName() + "", MessageType.CUSTOM);
	}
	
	@Override
	public void onKill(L2Character killerCharacter, L2PcInstance killedPlayerInstance)
	{
		if ((killedPlayerInstance == null) || !isStarted())
		{
			return;
		}
		
		if (!isParticipant(killedPlayerInstance.getObjectId()))
		{
			return;
		}
		
		GameEventPlayer killedEventPlayer = _participantPlayers.get(killedPlayerInstance.getObjectId());
		
		if (killedEventPlayer.getCredits() <= 1)
		{
			new GameEventTeleporter(killedEventPlayer, killedEventPlayer.getOriginalCoordinates(), true);
			removeParticipant(killedPlayerInstance);
		}
		else
		{
			killedEventPlayer.decreaseCredits();
			new GameEventTeleporter(killedEventPlayer, getConfigs().EVENT_PLAYER_COORDINATES, false);
		}
		
		if (killerCharacter == null)
		{
			return;
		}
		
		L2PcInstance killerPlayerInstance = null;
		
		if (killerCharacter.isSummon())
		{
			killerPlayerInstance = ((L2Summon) killerCharacter).getOwner();
			
			if (killerPlayerInstance == null)
			{
				return;
			}
		}
		else if (killerCharacter.isPlayer())
		{
			killerPlayerInstance = (L2PcInstance) killerCharacter;
		}
		else
		{
			return;
		}
		
		if (isParticipant(killerPlayerInstance.getObjectId()))
		{
			GameEventPlayer killerEventPlayer = _participantPlayers.get(killerPlayerInstance.getObjectId());
			
			killerEventPlayer.increaseKills();
			killerPlayerInstance.sendPacket(new CreatureSay(killerPlayerInstance.getObjectId(), ChatType.PARTYROOM_ALL, killerPlayerInstance.getName(), "I have killed " + killedPlayerInstance.getName() + "!"));
			
			killedEventPlayer.increaseDeaths();
			killedPlayerInstance.sendPacket(new CreatureSay(killerPlayerInstance.getObjectId(), ChatType.PARTYROOM_ALL, killerPlayerInstance.getName(), "I killed you!"));
			
			if (killedEventPlayer.getCredits() == 0)
			{
				killedPlayerInstance.sendMessage("You do not have any credits, leaving the event!");
			}
			else
			{
				killedPlayerInstance.sendMessage("Now you have " + String.valueOf(killedEventPlayer.getCredits()) + " credit(s)!");
			}
			
			// Notify to scripts.
			EventDispatcher.getInstance().notifyEventAsync(new OnEventKill(killerPlayerInstance, killedPlayerInstance, null));
		}
		
		if (getCountPlayers() == 1)
		{
			GameEventManager.getInstance().skipDelay();
		}
		
		if (getConfigs().EVENT_ENABLED_KILL_BONUS)
		{
			giveKillBonus(killedEventPlayer);
		}
	}
}
