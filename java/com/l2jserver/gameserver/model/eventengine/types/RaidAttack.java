/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine.types;

import java.util.ArrayList;
import java.util.List;

import com.l2jserver.gameserver.instancemanager.AntiFeedManager;
import com.l2jserver.gameserver.model.Location;

/**
 * @author Мо3олЬ
 */
// TODO: need implement
public class RaidAttack // extends GlobalGameEvent
{
	// Locations
	private static final List<RaidLocation> LOCATIONS = new ArrayList<>();
	static
	{
		LOCATIONS.add(new RaidLocation("Dragon Valley", new Location(71640, 117976, -3680)));
		LOCATIONS.add(new RaidLocation("The Outsides of Elven Village", new Location(54184, 44440, -3584)));
		LOCATIONS.add(new RaidLocation("Beast Farm", new Location(52456, -79512, -3104)));
		LOCATIONS.add(new RaidLocation("Anghel Waterfall", new Location(170728, 85352, -1984)));
		LOCATIONS.add(new RaidLocation("Tower of Insolence 11 Floor", new Location(114648, 16088, 6992)));
	}
	
	public void init()
	{
		// setEventName("Attack Special Boss");
		
		AntiFeedManager.getInstance().registerEvent(AntiFeedManager.CUSTOM_EVENT_ID);
		
		// setState(GameEventState.INACTIVE);
	}
	
	public boolean startFight()
	{
		return false;
	}
	
	public void stopFight()
	{
		// Set state INACTIVATING
		// setState(GameEventState.INACTIVATING);
		
		// setState(GameEventState.INACTIVE);
		AntiFeedManager.getInstance().clear(AntiFeedManager.CUSTOM_EVENT_ID);
	}
	
	// Class to save each raid spawn location
	public static final class RaidLocation
	{
		private final String _name;
		private final Location _loc;
		
		public RaidLocation(String name, Location loc)
		{
			_name = name;
			_loc = loc;
		}
		
		public String getName()
		{
			return _name;
		}
		
		public Location getLocation()
		{
			return _loc;
		}
	}
}
