/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine.types;

import java.util.List;

import com.l2jserver.gameserver.configuration.config.events.TvTConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.events.EventState;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.instancemanager.AntiFeedManager;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.L2Summon;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.eventengine.GameEventTeam;
import com.l2jserver.gameserver.model.eventengine.GameEventTeleporter;
import com.l2jserver.gameserver.model.eventengine.TeamGameEvent;
import com.l2jserver.gameserver.model.events.EventDispatcher;
import com.l2jserver.gameserver.model.events.impl.events.OnEventKill;
import com.l2jserver.gameserver.network.serverpackets.CreatureSay;

/**
 * @author nuLL
 * @author Мо3олЬ
 */
public class TeamVsTeam extends TeamGameEvent
{
	@Override
	public TvTConfig getConfigs()
	{
		return TvTConfig.getInstance();
	}
	
	@Override
	public void init()
	{
		setEventName("Team VS Team");
		AntiFeedManager.getInstance().registerEvent(AntiFeedManager.CUSTOM_EVENT_ID);
		
		List<GameEventTeam> teamsConfig = getConfigs().EVENT_TEAM_ID_NAME_COORDINATES;
		_teams = new GameEventTeam[teamsConfig.size()];
		for (int i = 0; i < teamsConfig.size(); i++)
		{
			_teams[i] = new GameEventTeam(teamsConfig.get(i).getId(), teamsConfig.get(i).getName(), teamsConfig.get(i).getCoordinates());
		}
		
		setState(EventState.INACTIVE);
	}
	
	@Override
	public void onKill(L2Character character, L2PcInstance player)
	{
		if ((player == null) || !isStarted())
		{
			return;
		}
		
		var killedTeam = getParticipantTeam(player.getObjectId());
		
		if (killedTeam == null)
		{
			return;
		}
		
		var killedEventPlayer = killedTeam.getEventPlayer(player.getObjectId());
		
		if ((killedEventPlayer == null) || (character == null))
		{
			return;
		}
		
		L2PcInstance killerPlayerInstance = null;
		
		if (character.isSummon())
		{
			killerPlayerInstance = ((L2Summon) character).getOwner();
			
			if (killerPlayerInstance == null)
			{
				return;
			}
		}
		else if (character.isPlayer())
		{
			killerPlayerInstance = (L2PcInstance) character;
		}
		else
		{
			return;
		}
		
		var killerTeam = getParticipantTeam(killerPlayerInstance.getObjectId());
		
		if (killerTeam == null)
		{
			return;
		}
		
		var killerEventPlayer = killerTeam.getEventPlayer(killerPlayerInstance.getObjectId());
		
		if (killerEventPlayer == null)
		{
			return;
		}
		
		if (killerTeam.getId() != killedTeam.getId())
		{
			killedEventPlayer.increaseDeaths();
			killedEventPlayer.increaseKills();
			killerTeam.increasePoints();
			
			for (var eventPlayer : killerTeam.getParticipatedPlayers().values())
			{
				if ((eventPlayer != null) && (eventPlayer.getPlayer() != null))
				{
					eventPlayer.getPlayer().sendPacket(new CreatureSay(killerPlayerInstance.getObjectId(), ChatType.PARTYROOM_ALL, killerPlayerInstance.getName(), MessagesData.getInstance().getMessage(eventPlayer.getPlayer(), "events_player_have_killed").replace("%s%", player.getName() + "")));
					broadcastScoreMessage();
				}
			}
			EventDispatcher.getInstance().notifyEventAsync(new OnEventKill(killerPlayerInstance, player, killerTeam));
		}
		
		new GameEventTeleporter(killedEventPlayer, killedTeam.getCoordinates(), false);
		
		if (getConfigs().EVENT_REQUIRE_MIN_FRAGS_TO_REWARD)
		{
			killerEventPlayer.increaseKills();
		}
		
		if (getConfigs().EVENT_ENABLED_KILL_BONUS)
		{
			giveKillBonus(killerEventPlayer);
		}
	}
}
