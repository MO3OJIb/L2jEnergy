/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.eventengine.types;

import com.l2jserver.gameserver.configuration.config.events.TWConfig;
import com.l2jserver.gameserver.instancemanager.AntiFeedManager;
import com.l2jserver.gameserver.instancemanager.TownManager;

/**
 * @author Мо3олЬ
 */
// TODO: need implement
public class TownWar // extends GlobalGameEvent
{
	private static final int ALL_TOWNS_INT = 17;
	
	public TWConfig getConfigs()
	{
		return TWConfig.getInstance();
	}
	
	public void init()
	{
		// setEventName("Town War");
		
		AntiFeedManager.getInstance().registerEvent(AntiFeedManager.CUSTOM_EVENT_ID);
		
		// setState(GameEventState.INACTIVE);
	}
	
	public boolean startFight()
	{
		// Set state to STARTING
		// setState(GameEventState.STARTING);
		if (getConfigs().TW_ALL_TOWNS)
		{
			for (int i = 1; i <= ALL_TOWNS_INT; i++)
			{
				// TownManager.getTown(i).setIsTWZone(true);
				TownManager.getTown(i).getCharactersInside();
			}
			// TownManager.getTown(20).setIsTWZone(true);
			TownManager.getTown(20).getCharactersInside();
			// EventUtil.showAnnounceEvents(": All towns are war zone.", getEvent().getEventName() + "", MessageType.CUSTOM);
		}
		else
		{
			// TownManager.getTown(getConfigs().TW_TOWN_ID).setIsTWZone(true);
			TownManager.getTown(getConfigs().TW_TOWN_ID).getCharactersInside();
			// EventUtil.showAnnounceEvents(": is a war zone.", getEvent().getEventName() + "", TWConfig.TW_TOWN_NAME +"", MessageType.CUSTOM);
		}
		
		// setState(GameEventState.INACTIVE);
		AntiFeedManager.getInstance().clear(AntiFeedManager.CUSTOM_EVENT_ID);
		return false;
	}
	
	public void stopFight()
	{
		// Set state INACTIVATING
		// setState(GameEventState.INACTIVATING);
		
		if (getConfigs().TW_ALL_TOWNS)
		{
			for (int i = 1; i <= ALL_TOWNS_INT; i++)
			{
				// TownManager.getTown(i).setIsTWZone(false);
				TownManager.getTown(i).getCharactersInside();
			}
			// TownManager.getTown(20).setIsTWZone(false);
			TownManager.getTown(20).getCharactersInside();
			// EventUtil.showAnnounceEvents(": All towns are returned normal.", getEvent().getEventName() + "" MessageType.CUSTOM);
		}
		else
		{
			// TownManager.getTown(getConfigs().TW_TOWN_ID).setIsTWZone(false);
			TownManager.getTown(getConfigs().TW_TOWN_ID).getCharactersInside();
			// EventUtil.showAnnounceEvents(": is returned normal.", getEvent().getEventName() + "", TWConfig.TW_TOWN_NAME +"", MessageType.CUSTOM);
		}
		
		// setState(GameEventState.INACTIVE);
		AntiFeedManager.getInstance().clear(AntiFeedManager.CUSTOM_EVENT_ID);
	}
}
