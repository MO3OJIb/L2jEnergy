/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.events.impl.character;

import com.l2jserver.gameserver.enums.events.EventType;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.events.impl.IBaseEvent;

/**
 * Creature See listener.
 * @author Zoey76
 */
public class OnCreatureSee implements IBaseEvent
{
	
	private final L2Character _observer;
	private final L2Character _observed;
	
	public OnCreatureSee(L2Character observer, L2Character observed)
	{
		_observer = observer;
		_observed = observed;
	}
	
	public L2Character getObserver()
	{
		return _observer;
	}
	
	public L2Character getObserved()
	{
		return _observed;
	}
	
	@Override
	public EventType getType()
	{
		return EventType.ON_CREATURE_SEE;
	}
}
