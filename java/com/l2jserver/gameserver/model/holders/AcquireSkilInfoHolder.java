/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.holders;

/**
 * @author Мо3олЬ
 */
public class AcquireSkilInfoHolder extends AcquireSkilHolder
{
	private final int _maxLevel;
	private final int _spCost;
	private final int _requirements;
	
	public AcquireSkilInfoHolder(int pId, int pNextLevel, int pMaxLevel, int pSpCost, int pRequirements)
	{
		super(pId, pNextLevel);
		_maxLevel = pMaxLevel;
		_spCost = pSpCost;
		_requirements = pRequirements;
	}
	
	public int getMaxLevel()
	{
		return _maxLevel;
	}
	
	public int getSpCost()
	{
		return _spCost;
	}
	
	public int getRequirements()
	{
		return _requirements;
	}
}
