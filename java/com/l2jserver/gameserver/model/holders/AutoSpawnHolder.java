/*
 * Copyright (C) 2004-2023 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.holders;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

import com.l2jserver.gameserver.model.L2Spawn;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.interfaces.IIdentifiable;

public class AutoSpawnHolder implements IIdentifiable
{
	protected int _objectId;
	
	protected int _spawnIndex;
	
	protected int _npcId;
	
	protected int _initDelay;
	
	protected int _resDelay;
	
	protected int _desDelay;
	
	protected int _spawnCount = 1;
	
	private int _lastLocIndex = -1;
	
	private final Queue<L2Npc> _npcList = new ConcurrentLinkedQueue<>();
	
	private final List<Location> _locList = new CopyOnWriteArrayList<>();
	
	private boolean _spawnActive;
	
	private boolean _randomSpawn = false;
	
	private boolean _broadcastAnnouncement = false;
	
	public AutoSpawnHolder(int npcId, int initDelay, int respawnDelay, int despawnDelay)
	{
		_npcId = npcId;
		_initDelay = initDelay;
		_resDelay = respawnDelay;
		_desDelay = despawnDelay;
	}
	
	public void setSpawnActive(boolean activeValue)
	{
		_spawnActive = activeValue;
	}
	
	public boolean addNpcInstance(L2Npc npcInst)
	{
		return _npcList.add(npcInst);
	}
	
	public boolean removeNpcInstance(L2Npc npcInst)
	{
		return _npcList.remove(npcInst);
	}
	
	public int getObjectId()
	{
		return _objectId;
	}
	
	public void setObjectId(int objectId)
	{
		_objectId = objectId;
	}
	
	public int getInitialDelay()
	{
		return _initDelay;
	}
	
	public int getRespawnDelay()
	{
		return _resDelay;
	}
	
	public int getDespawnDelay()
	{
		return _desDelay;
	}
	
	@Override
	public int getId()
	{
		return _npcId;
	}
	
	public int getSpawnCount()
	{
		return _spawnCount;
	}
	
	public Location[] getLocationList()
	{
		return _locList.toArray(new Location[_locList.size()]);
	}
	
	public Queue<L2Npc> getNPCInstanceList()
	{
		return _npcList;
	}
	
	public List<L2Spawn> getSpawns()
	{
		final List<L2Spawn> npcSpawns = new ArrayList<>();
		for (L2Npc npcInst : _npcList)
		{
			npcSpawns.add(npcInst.getSpawn());
		}
		return npcSpawns;
	}
	
	public void setSpawnCount(int spawnCount)
	{
		_spawnCount = spawnCount;
	}
	
	public void setRandomSpawn(boolean randValue)
	{
		_randomSpawn = randValue;
	}
	
	public void setBroadcast(boolean broadcastValue)
	{
		_broadcastAnnouncement = broadcastValue;
	}
	
	public boolean isSpawnActive()
	{
		return _spawnActive;
	}
	
	public boolean isRandomSpawn()
	{
		return _randomSpawn;
	}
	
	public boolean isBroadcasting()
	{
		return _broadcastAnnouncement;
	}
	
	public boolean addSpawnLocation(int x, int y, int z, int heading)
	{
		return _locList.add(new Location(x, y, z, heading));
	}
	
	public boolean addSpawnLocation(int[] spawnLoc)
	{
		if (spawnLoc.length != 3)
		{
			return false;
		}
		
		return addSpawnLocation(spawnLoc[0], spawnLoc[1], spawnLoc[2], -1);
	}
	
	public Location removeSpawnLocation(int locIndex)
	{
		try
		{
			return _locList.remove(locIndex);
		}
		catch (IndexOutOfBoundsException e)
		{
			return null;
		}
	}
	
	public int getLastLocIndex()
	{
		return _lastLocIndex;
	}
	
	public void setLastLocIndex(int lastLocIndex)
	{
		_lastLocIndex = lastLocIndex;
	}
}
