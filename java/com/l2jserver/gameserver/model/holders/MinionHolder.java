/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.holders;

import com.l2jserver.gameserver.model.interfaces.IIdentifiable;

/**
 * This class hold info needed for minions spawns<br>
 * @author Zealar
 */
public class MinionHolder implements IIdentifiable
{
	private final int _id;
	private final int _count;
	private final long _respawnTime;
	private final int _weightPoint;
	
	public MinionHolder(final int id, final int count, final long respawnTime, final int weightPoint)
	{
		_id = id;
		_count = count;
		_respawnTime = respawnTime;
		_weightPoint = weightPoint;
	}
	
	@Override
	public int getId()
	{
		return _id;
	}
	
	public int getCount()
	{
		return _count;
	}
	
	public long getRespawnTime()
	{
		return _respawnTime;
	}
	
	public int getWeightPoint()
	{
		return _weightPoint;
	}
}
