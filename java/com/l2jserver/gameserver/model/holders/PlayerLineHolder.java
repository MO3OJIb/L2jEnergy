/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.holders;

/**
 * @author Мо3олЬ
 */
public class PlayerLineHolder extends PlayerPointHolder
{
	private final int _x2;
	private final int _y2;
	private final int _z2;
	
	public PlayerLineHolder(String name, int color, boolean isNameColored, int x, int y, int z, int x2, int y2, int z2)
	{
		super(name, color, isNameColored, x, y, z);
		_x2 = x2;
		_y2 = y2;
		_z2 = z2;
	}
	
	public int getX2()
	{
		return _x2;
	}
	
	public int getY2()
	{
		return _y2;
	}
	
	public int getZ2()
	{
		return _z2;
	}
}
