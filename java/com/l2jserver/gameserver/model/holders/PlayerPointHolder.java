/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.holders;

/**
 * @author Мо3олЬ
 */
public class PlayerPointHolder
{
	private final String _name;
	private final int _color;
	private final boolean _isNameColored;
	private final int _x;
	private final int _y;
	private final int _z;
	
	public PlayerPointHolder(String name, int color, boolean isNameColored, int x, int y, int z)
	{
		_name = name;
		_color = color;
		_isNameColored = isNameColored;
		_x = x;
		_y = y;
		_z = z;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public int getColor()
	{
		return _color;
	}
	
	public boolean isNameColored()
	{
		return _isNameColored;
	}
	
	public int getX()
	{
		return _x;
	}
	
	public int getY()
	{
		return _y;
	}
	
	public int getZ()
	{
		return _z;
	}
}
