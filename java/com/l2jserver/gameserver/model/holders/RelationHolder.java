/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.holders;

/**
 * @author Мо3олЬ
 */
public class RelationHolder
{
	private final int _objId;
	private final int _relation;
	private final int _autoAttackable;
	private final int _karma;
	private final int _pvpFlag;
	
	public RelationHolder(int objId, int relation, int autoAttackable, int karma, int pvpFlag)
	{
		_objId = objId;
		_relation = relation;
		_autoAttackable = autoAttackable;
		_karma = karma;
		_pvpFlag = pvpFlag;
	}
	
	public int getObjId()
	{
		return _objId;
	}
	
	public int getRelation()
	{
		return _relation;
	}
	
	public int getAutoAttackable()
	{
		return _autoAttackable;
	}
	
	public int getKarma()
	{
		return _karma;
	}
	
	public int getPvpFlag()
	{
		return _pvpFlag;
	}
}
