/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.holders;

/**
 * @author Мо3олЬ
 */
public class RewardInfoHolder
{
	private final int _crystalId;
	private final String _messageOk;
	private final String _messageNoAdena;
	private final String _messageNoScore;
	
	public RewardInfoHolder(int crystalId, String messageOk, String messageNoAdena, String messageNoScore)
	{
		_crystalId = crystalId;
		_messageOk = messageOk;
		_messageNoAdena = messageNoAdena;
		_messageNoScore = messageNoScore;
	}
	
	public int getCrystalId()
	{
		return _crystalId;
	}
	
	public String getMessageOk()
	{
		return _messageOk;
	}
	
	public String getMessageNoAdena()
	{
		return _messageNoAdena;
	}
	
	public String getMessageNoScore()
	{
		return _messageNoScore;
	}
}
