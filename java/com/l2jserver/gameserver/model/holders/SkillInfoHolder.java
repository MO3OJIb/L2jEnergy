/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.holders;

/**
 * @author Мо3олЬ
 */
public class SkillInfoHolder
{
	private final int _id;
	private final int _level;
	private final boolean _passive;
	private final boolean _disabled;
	private final boolean _enchanted;
	
	public SkillInfoHolder(int id, int level, boolean passive, boolean disabled, boolean enchanted)
	{
		_id = id;
		_level = level;
		_passive = passive;
		_disabled = disabled;
		_enchanted = enchanted;
	}
	
	public int getId()
	{
		return _id;
	}
	
	public int getLevel()
	{
		return _level;
	}
	
	public boolean getPassive()
	{
		return _passive;
	}
	
	public boolean getDisabled()
	{
		return _disabled;
	}
	
	public boolean getEnchanted()
	{
		return _enchanted;
	}
}
