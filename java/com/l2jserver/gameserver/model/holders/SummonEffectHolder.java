/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.holders;

import com.l2jserver.gameserver.model.skills.Skill;

/**
 * Summon Effect holder.
 * @author Мо3олЬ
 */
public class SummonEffectHolder
{
	private final Skill _skill;
	private final int _effectCurTime;
	
	public SummonEffectHolder(Skill skill, int effectCurTime)
	{
		_skill = skill;
		_effectCurTime = effectCurTime;
	}
	
	public Skill getSkill()
	{
		return _skill;
	}
	
	public int getEffectCurTime()
	{
		return _effectCurTime;
	}
}
