/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.holders;

import static com.l2jserver.gameserver.model.itemcontainer.Inventory.MAX_ADENA;

import com.l2jserver.gameserver.model.tradelist.TradeList;

public class TradeListHolder
{
	private final int _itemId;
	private final long _count;
	private final long _price;
	
	public TradeListHolder(int itemId, long count, long price)
	{
		_itemId = itemId;
		_count = count;
		_price = price;
	}
	
	public boolean addToTradeList(TradeList list)
	{
		if ((MAX_ADENA / _count) < _price)
		{
			return false;
		}
		
		list.addItem(_itemId, _count, _price);
		return true;
	}
	
	public long getPrice()
	{
		return _count * _price;
	}
}
