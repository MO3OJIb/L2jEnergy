/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.itemauction;

import java.sql.SQLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.random.Rnd;
import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.sql.impl.CharNameTable;
import com.l2jserver.gameserver.enums.ItemAuctionExtendState;
import com.l2jserver.gameserver.enums.ItemAuctionState;
import com.l2jserver.gameserver.enums.ItemLocation;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

public final class ItemAuctionInstance
{
	private static final Logger LOG = LoggerFactory.getLogger(ItemAuctionInstance.class);
	
	private static final long START_TIME_SPACE = TimeUnit.MILLISECONDS.convert(1, TimeUnit.MINUTES);
	private static final long FINISH_TIME_SPACE = TimeUnit.MILLISECONDS.convert(10, TimeUnit.MINUTES);
	
	// SQL queries
	private static final String SELECT_AUCTION_ID_BY_INSTANCE_ID = "SELECT auctionId FROM item_auction WHERE instanceId = ?";
	private static final String SELECT_AUCTION_INFO = "SELECT auctionItemId, startingTime, endingTime, auctionStateId FROM item_auction WHERE auctionId = ? ";
	private static final String SELECT_PLAYERS_ID_BY_AUCTION_ID = "SELECT playerObjId, playerBid FROM item_auction_bid WHERE auctionId = ?";
	
	private final int _instanceId;
	private final AtomicInteger _auctionIds;
	private final ConcurrentHashMap<Integer, ItemAuction> _auctions = new ConcurrentHashMap<>();
	
	private final ArrayList<AuctionItem> _items = new ArrayList<>();
	private final AuctionDateGenerator _dateGenerator;
	
	private ItemAuction _currentAuction;
	private ItemAuction _nextAuction;
	private ScheduledFuture<?> _stateTask;
	
	public ItemAuctionInstance(final int instanceId, final AtomicInteger auctionIds, final Node node) throws Exception
	{
		_instanceId = instanceId;
		_auctionIds = auctionIds;
		
		var nanode = node.getAttributes();
		var generatorConfig = new StatsSet();
		for (var i = nanode.getLength(); i-- > 0;)
		{
			var n = nanode.item(i);
			if (n != null)
			{
				generatorConfig.set(n.getNodeName(), n.getNodeValue());
			}
		}
		
		_dateGenerator = new AuctionDateGenerator(generatorConfig);
		
		for (var na = node.getFirstChild(); na != null; na = na.getNextSibling())
		{
			try
			{
				if ("item".equalsIgnoreCase(na.getNodeName()))
				{
					var naa = na.getAttributes();
					var auctionItemId = Integer.parseInt(naa.getNamedItem("auctionItemId").getNodeValue());
					var auctionLenght = Integer.parseInt(naa.getNamedItem("auctionLenght").getNodeValue());
					var auctionInitBid = Integer.parseInt(naa.getNamedItem("auctionInitBid").getNodeValue());
					
					var itemId = Integer.parseInt(naa.getNamedItem("itemId").getNodeValue());
					var itemCount = Integer.parseInt(naa.getNamedItem("itemCount").getNodeValue());
					
					if (auctionLenght < 1)
					{
						throw new IllegalArgumentException("auctionLenght < 1 for instanceId: " + _instanceId + ", itemId " + itemId);
					}
					
					var itemExtra = new StatsSet();
					var item = new AuctionItem(auctionItemId, auctionLenght, auctionInitBid, itemId, itemCount, itemExtra);
					
					if (!item.checkItemExists())
					{
						throw new IllegalArgumentException("Item with id " + itemId + " not found");
					}
					
					for (var tmp : _items)
					{
						if (tmp.getAuctionItemId() == auctionItemId)
						{
							throw new IllegalArgumentException("Dublicated auction item id " + auctionItemId);
						}
					}
					
					_items.add(item);
					
					for (var nb = na.getFirstChild(); nb != null; nb = nb.getNextSibling())
					{
						if ("extra".equalsIgnoreCase(nb.getNodeName()))
						{
							var nab = nb.getAttributes();
							for (int i = nab.getLength(); i-- > 0;)
							{
								var n = nab.item(i);
								if (n != null)
								{
									itemExtra.set(n.getNodeName(), n.getNodeValue());
								}
							}
						}
					}
				}
			}
			catch (IllegalArgumentException ex)
			{
				LOG.warn("Failed loading auction item!", ex);
			}
		}
		
		if (_items.isEmpty())
		{
			throw new IllegalArgumentException("No items defined");
		}
		
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_AUCTION_ID_BY_INSTANCE_ID))
		{
			ps.setInt(1, _instanceId);
			try (var rs = ps.executeQuery())
			{
				while (rs.next())
				{
					var auctionId = rs.getInt(1);
					try
					{
						var auction = loadAuction(auctionId);
						if (auction != null)
						{
							_auctions.put(auctionId, auction);
						}
						else
						{
							DAOFactory.getInstance().getItemAuctionDAO().delete(auctionId);
						}
					}
					catch (Exception ex)
					{
						LOG.warn("Failed loading auction ID {}!", auctionId, ex);
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.error("Error loading auctions.", ex);
			return;
		}
		
		LOG.info("Loaded {} item(s) and registered {} auction(s) for NPC ID {}.", _items.size(), _auctions.size(), _instanceId);
		checkAndSetCurrentAndNextAuction();
	}
	
	public final ItemAuction getCurrentAuction()
	{
		return _currentAuction;
	}
	
	public final ItemAuction getNextAuction()
	{
		return _nextAuction;
	}
	
	public final void shutdown()
	{
		var stateTask = _stateTask;
		if (stateTask != null)
		{
			stateTask.cancel(false);
		}
	}
	
	private final AuctionItem getAuctionItem(final int auctionItemId)
	{
		for (var i = _items.size(); i-- > 0;)
		{
			var item = _items.get(i);
			if (item.getAuctionItemId() == auctionItemId)
			{
				return item;
			}
		}
		return null;
	}
	
	final void checkAndSetCurrentAndNextAuction()
	{
		Collection<ItemAuction> auctions = _auctions.values();
		
		ItemAuction currentAuction = null;
		ItemAuction nextAuction = null;
		
		Instant currentTime = Instant.now();
		long currentTimeMillis = currentTime.toEpochMilli();
		
		auctions.removeIf(auction -> (auction.getStartingTime() < currentTimeMillis) && (auction.getAuctionState() != ItemAuctionState.STARTED));
		auctions.removeIf(auction -> auction.getAuctionState() == ItemAuctionState.FINISHED);
		
		if (auctions.isEmpty())
		{
			nextAuction = createAuction(currentTime.plus(START_TIME_SPACE, ChronoUnit.MILLIS).toEpochMilli());
		}
		else
		{
			currentAuction = auctions.stream()
				.filter(auction -> auction.getAuctionState() == ItemAuctionState.STARTED)
				.findFirst()
				.orElse(auctions.stream().findFirst().orElse(null));
			
			auctions.remove(currentAuction);
			
			nextAuction = auctions.stream()
				.filter(auction -> auction.getStartingTime() > currentTimeMillis)
				.min(Comparator.comparingLong(ItemAuction::getStartingTime))
				.orElse(null);
			
			if (nextAuction == null)
			{
				nextAuction = createAuction(currentTime.plus(START_TIME_SPACE, ChronoUnit.MILLIS).toEpochMilli());
			}
		}
		
		_auctions.put(nextAuction.getAuctionId(), nextAuction);
		
		_currentAuction = currentAuction;
		_nextAuction = nextAuction;
		Instant startTime = Instant.ofEpochMilli(nextAuction.getStartingTime());
		
		if ((currentAuction != null) && (currentAuction.getAuctionState() != ItemAuctionState.FINISHED))
		{
			Instant endTime = (currentAuction.getAuctionState() == ItemAuctionState.STARTED) ? Instant.ofEpochMilli(currentAuction.getEndingTime() + FINISH_TIME_SPACE) : Instant.ofEpochMilli(currentAuction.getStartingTime());
			setStateTask(ThreadPoolManager.getInstance().scheduleGeneral(new ScheduleAuctionTask(currentAuction), Math.max(endTime.toEpochMilli() - currentTime.toEpochMilli(), 0L)));
			LOG.info("Loaded current auction ID {} for NPC ID {}.", currentAuction.getAuctionId(), _instanceId);
		}
		else
		{
			setStateTask(ThreadPoolManager.getInstance().scheduleGeneral(new ScheduleAuctionTask(nextAuction), Math.max(startTime.toEpochMilli() - currentTime.toEpochMilli(), 0L)));
			LOG.info("Loaded next auction ID {} on {} for NPC ID {}.", nextAuction.getAuctionId(), TimeUtils.dateTimeFormat(startTime), _instanceId);
		}
	}
	
	public final ItemAuction getAuction(int auctionId)
	{
		return _auctions.get(auctionId);
	}
	
	public final ItemAuction[] getAuctionsByBidder(int bidderObjId)
	{
		var auctions = getAuctions();
		var stack = new ArrayList<>(auctions.size());
		for (var auction : getAuctions())
		{
			if (auction.getAuctionState() != ItemAuctionState.CREATED)
			{
				var bid = auction.getBidFor(bidderObjId);
				if (bid != null)
				{
					stack.add(auction);
				}
			}
		}
		return stack.toArray(new ItemAuction[stack.size()]);
	}
	
	public final Collection<ItemAuction> getAuctions()
	{
		final Collection<ItemAuction> auctions;
		
		synchronized (_auctions)
		{
			auctions = _auctions.values();
		}
		
		return auctions;
	}
	
	private class ScheduleAuctionTask implements Runnable
	{
		private final Logger LOG = LoggerFactory.getLogger(ScheduleAuctionTask.class);
		
		private final ItemAuction _auction;
		
		public ScheduleAuctionTask(ItemAuction auction)
		{
			_auction = auction;
		}
		
		@Override
		public final void run()
		{
			try
			{
				runImpl();
			}
			catch (final Exception e)
			{
				LOG.error("Failed scheduling auction ID {}!", _auction.getAuctionId(), e);
			}
		}
		
		private void runImpl() throws Exception
		{
			var state = _auction.getAuctionState();
			switch (state)
			{
				case CREATED:
				{
					if (!_auction.setAuctionState(state, ItemAuctionState.STARTED))
					{
						throw new IllegalStateException("Could not set auction state: " + ItemAuctionState.STARTED.toString() + ", expected: " + state.toString());
					}
					
					LOG.info("Loaded auction ID {} has started for npc id {}.", _auction.getAuctionId(), _auction.getInstanceId());
					checkAndSetCurrentAndNextAuction();
					break;
				}
				
				case STARTED:
				{
					switch (_auction.getAuctionEndingExtendState())
					{
						case EXTEND_BY_5_MIN:
						{
							if (_auction.getScheduledAuctionEndingExtendState() == ItemAuctionExtendState.INITIAL)
							{
								_auction.setScheduledAuctionEndingExtendState(ItemAuctionExtendState.EXTEND_BY_5_MIN);
								setStateTask(ThreadPoolManager.getInstance().scheduleGeneral(this, Math.max(_auction.getEndingTime() - System.currentTimeMillis(), 0L)));
								return;
							}
							break;
						}
						case EXTEND_BY_3_MIN:
						{
							if (_auction.getScheduledAuctionEndingExtendState() != ItemAuctionExtendState.EXTEND_BY_3_MIN)
							{
								_auction.setScheduledAuctionEndingExtendState(ItemAuctionExtendState.EXTEND_BY_3_MIN);
								setStateTask(ThreadPoolManager.getInstance().scheduleGeneral(this, Math.max(_auction.getEndingTime() - System.currentTimeMillis(), 0L)));
								return;
							}
							break;
						}
						case EXTEND_BY_CONFIG_PHASE_A:
						{
							if (_auction.getScheduledAuctionEndingExtendState() != ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_B)
							{
								_auction.setScheduledAuctionEndingExtendState(ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_B);
								setStateTask(ThreadPoolManager.getInstance().scheduleGeneral(this, Math.max(_auction.getEndingTime() - System.currentTimeMillis(), 0L)));
								return;
							}
							break;
						}
						case EXTEND_BY_CONFIG_PHASE_B:
						{
							if (_auction.getScheduledAuctionEndingExtendState() != ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_A)
							{
								_auction.setScheduledAuctionEndingExtendState(ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_A);
								setStateTask(ThreadPoolManager.getInstance().scheduleGeneral(this, Math.max(_auction.getEndingTime() - System.currentTimeMillis(), 0L)));
								return;
							}
						}
					}
					
					if (!_auction.setAuctionState(state, ItemAuctionState.FINISHED))
					{
						throw new IllegalStateException("Could not set auction state: " + ItemAuctionState.FINISHED.toString() + ", expected: " + state.toString());
					}
					
					onAuctionFinished(_auction);
					checkAndSetCurrentAndNextAuction();
					break;
				}
				default:
					throw new IllegalStateException("Invalid state: " + state);
			}
		}
	}
	
	final void onAuctionFinished(final ItemAuction auction)
	{
		auction.broadcastToAllBiddersInternal(SystemMessage.getSystemMessage(SystemMessageId.S1_AUCTION_ENDED).addInt(auction.getAuctionId()));
		
		var bid = auction.getHighestBid();
		if (bid != null)
		{
			var item = auction.createNewItemInstance();
			var player = bid.getPlayer();
			if (player != null)
			{
				player.getWarehouse().addItem("ItemAuction", item, null, null);
				player.sendPacket(SystemMessageId.WON_BID_ITEM_CAN_BE_FOUND_IN_WAREHOUSE);
				
				LOG.info("Loaded auction ID {} has finished. Highest bid by {} for instance ID {}.", auction.getAuctionId(), player.getName(), _instanceId);
			}
			else
			{
				item.setOwnerId(bid.getPlayerObjId());
				item.setItemLocation(ItemLocation.WAREHOUSE);
				item.updateDatabase();
				L2World.getInstance().removeObject(item);
				
				var playerName = CharNameTable.getInstance().getNameById(bid.getPlayerObjId());
				LOG.info("Loaded auction ID {} has finished. Highest bid by {} for instance ID {}.", auction.getAuctionId(), playerName, _instanceId);
			}
			
			// Clean all canceled bids
			auction.clearCanceledBids();
		}
		else
		{
			LOG.info("Loaded auction ID {} has finished. There have not been any bid for instance ID {}.", auction.getAuctionId(), _instanceId);
		}
	}
	
	final void setStateTask(final ScheduledFuture<?> future)
	{
		final ScheduledFuture<?> stateTask = _stateTask;
		if (stateTask != null)
		{
			stateTask.cancel(false);
		}
		
		_stateTask = future;
	}
	
	private final ItemAuction createAuction(final long after)
	{
		var auctionItem = _items.get(Rnd.get(_items.size()));
		var startingTime = _dateGenerator.nextDate(after);
		var endingTime = startingTime + TimeUnit.MILLISECONDS.convert(auctionItem.getAuctionLength(), TimeUnit.MINUTES);
		var auction = new ItemAuction(_auctionIds.getAndIncrement(), _instanceId, startingTime, endingTime, auctionItem);
		auction.storeMe();
		return auction;
	}
	
	private final ItemAuction loadAuction(final int auctionId) throws SQLException
	{
		try (var con = ConnectionFactory.getInstance().getConnection())
		{
			var auctionItemId = 0;
			long startingTime = 0;
			long endingTime = 0;
			byte auctionStateId = 0;
			try (var ps = con.prepareStatement(SELECT_AUCTION_INFO))
			{
				ps.setInt(1, auctionId);
				try (var rs = ps.executeQuery())
				{
					if (!rs.next())
					{
						LOG.warn("Auction data not found for auction ID {}!", auctionId);
						return null;
					}
					auctionItemId = rs.getInt(1);
					startingTime = rs.getLong(2);
					endingTime = rs.getLong(3);
					auctionStateId = rs.getByte(4);
				}
			}
			
			if (startingTime >= endingTime)
			{
				LOG.warn("Invalid starting/ending paramaters for auction ID {}!", auctionId);
				return null;
			}
			
			var auctionItem = getAuctionItem(auctionItemId);
			if (auctionItem == null)
			{
				LOG.warn("Auction item ID {} not found for auction ID {}!", auctionItemId, auctionId);
				return null;
			}
			
			var auctionState = ItemAuctionState.stateForStateId(auctionStateId);
			if (auctionState == null)
			{
				LOG.warn("Invalid auctionStateId {} for auction ID {}!", auctionStateId, auctionId);
				return null;
			}
			
			if ((auctionState == ItemAuctionState.FINISHED) && (startingTime < (System.currentTimeMillis() - TimeUnit.MILLISECONDS.convert(GeneralConfig.ALT_ITEM_AUCTION_EXPIRED_AFTER, TimeUnit.DAYS))))
			{
				LOG.info("Clearing expired auction ID {}.", auctionId);
				DAOFactory.getInstance().getItemAuctionDAO().delete(auctionId);
				return null;
			}
			
			final List<ItemAuctionBid> auctionBids = new ArrayList<>();
			try (var ps = con.prepareStatement(SELECT_PLAYERS_ID_BY_AUCTION_ID))
			{
				ps.setInt(1, auctionId);
				try (var rs = ps.executeQuery())
				{
					while (rs.next())
					{
						var playerObjId = rs.getInt(1);
						var playerBid = rs.getLong(2);
						var bid = new ItemAuctionBid(playerObjId, playerBid);
						auctionBids.add(bid);
					}
				}
			}
			return new ItemAuction(auctionId, _instanceId, startingTime, endingTime, auctionItem, auctionBids, auctionState);
		}
	}
}