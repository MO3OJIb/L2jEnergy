/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.items;

public class LifeStone
{
	// lifestone level to player level table
	private static final int[] LEVELS =
	{
		46,
		49,
		52,
		55,
		58,
		61,
		64,
		67,
		70,
		76,
		80,
		82,
		84,
		85
	};
	private final int _grade;
	private final int _level;
	
	public LifeStone(int grade, int level)
	{
		_grade = grade;
		_level = level;
	}
	
	public final int getLevel()
	{
		return _level;
	}
	
	public final int getGrade()
	{
		return _grade;
	}
	
	public final int getPlayerLevel()
	{
		return LEVELS[_level];
	}
}
