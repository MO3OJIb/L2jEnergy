/*
 * Copyright (C) 2004-2024 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.localisation;

public class SMParam
{
	private final byte _type;
	private final Object _value;
	
	public SMParam(final byte type, final Object value)
	{
		_type = type;
		_value = value;
	}
	
	public final byte getType()
	{
		return _type;
	}
	
	public final Object getValue()
	{
		return _value;
	}
	
	public final String getStringValue()
	{
		return (String) _value;
	}
	
	public final int getIntValue()
	{
		return ((Integer) _value).intValue();
	}
	
	public final long getLongValue()
	{
		return ((Long) _value).longValue();
	}
	
	public final int[] getIntArrayValue()
	{
		return (int[]) _value;
	}
}
