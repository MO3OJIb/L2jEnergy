/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.primeshop;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import com.l2jserver.gameserver.enums.ItemMallFlag;

public class PrimeShopGroup
{
	public static final int DEFAULT_MAX_STOCK = -1;
	public static final int DEFAULT_CURRENT_STOCK = 0;
	public static final boolean DEFAULT_IS_EVENT = false;
	
	public static final ZonedDateTime DEFAULT_START_SALE_DATE = ZonedDateTime.of(LocalDateTime.of(1980, 01, 01, 00, 00), ZoneId.systemDefault());
	public static final ZonedDateTime DEFAULT_END_SALE_DATE = ZonedDateTime.of(LocalDateTime.of(2037, 06, 01, 23, 59), ZoneId.systemDefault());
	
	private final int _productId;
	private final int _category;
	public int _price;
	private ItemMallFlag _eventsFlag;
	private final ZonedDateTime _startSaleDate;
	private final ZonedDateTime _endSaleDate;
	private final int _daysOfWeek;
	public int _currentStock;
	private final int _maxStock;
	
	private List<PrimeShopItem> _items = new ArrayList<>(1);
	
	public PrimeShopGroup(int productId, int category, int price, final ItemMallFlag eventsFlag, ZonedDateTime startSaleDate, ZonedDateTime endSaleDate, int daysOfWeek, int currentStock, int isMaxStock)
	{
		_productId = productId;
		_category = category;
		_price = price;
		_eventsFlag = eventsFlag;
		_startSaleDate = startSaleDate;
		_endSaleDate = endSaleDate;
		_daysOfWeek = daysOfWeek;
		_currentStock = currentStock;
		_maxStock = isMaxStock;
	}
	
	public void setComponents(List<PrimeShopItem> items)
	{
		_items = items;
	}
	
	public List<PrimeShopItem> getItems()
	{
		return _items;
	}
	
	public int getProductId()
	{
		return _productId;
	}
	
	public int getCategory()
	{
		return _category;
	}
	
	public int getPrice()
	{
		return _price;
	}
	
	public ItemMallFlag getEventFlag()
	{
		return _eventsFlag;
	}
	
	public void enableEventFlag()
	{
		if (isFlag(ItemMallFlag.EVENT))
		{
			return;
		}
		_eventsFlag = ItemMallFlag.EVENT;
	}
	
	public void disableEventFlag()
	{
		if (!isFlag(ItemMallFlag.EVENT))
		{
			return;
		}
		_eventsFlag = ItemMallFlag.NONE;
	}
	
	public void enableBestFlag()
	{
		if (isFlag(ItemMallFlag.BEST))
		{
			return;
		}
		_eventsFlag = ItemMallFlag.BEST;
	}
	
	public void disableBestFlag()
	{
		if (!isFlag(ItemMallFlag.BEST))
		{
			return;
		}
		_eventsFlag = ItemMallFlag.NONE;
	}
	
	public boolean isFlag(final ItemMallFlag g)
	{
		return _eventsFlag == g;
	}
	
	public long getStartSaleDate()
	{
		return _startSaleDate.toEpochSecond();
	}
	
	public long getEndSaleDate()
	{
		return _endSaleDate.toEpochSecond();
	}
	
	public int getStartSaleHour()
	{
		return _startSaleDate.getHour();
	}
	
	public int getEndSaleHour()
	{
		return _endSaleDate.getHour();
	}
	
	public int getStartSaleMin()
	{
		return _startSaleDate.getMinute();
	}
	
	public int getEndSaleMin()
	{
		return _endSaleDate.getMinute();
	}
	
	public int getDayOfWeek()
	{
		return _daysOfWeek;
	}
	
	public long getCount()
	{
		return _items.stream().mapToLong(PrimeShopItem::getValue).sum();
	}
	
	public int getWeight()
	{
		return _items.stream().mapToInt(PrimeShopItem::getWeight).sum();
	}
	
	public boolean getLimit()
	{
		return _currentStock >= _maxStock;
	}
	
	public boolean isLimited()
	{
		return _maxStock > 0;
	}
	
	public int getCurrentStock()
	{
		return _currentStock;
	}
	
	public int getMaxStock()
	{
		return _maxStock;
	}
}
