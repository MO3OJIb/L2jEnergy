/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.punishment;

import java.util.concurrent.ScheduledFuture;

import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.enums.PunishmentAffect;
import com.l2jserver.gameserver.enums.PunishmentType;
import com.l2jserver.gameserver.handler.PunishmentHandler;
import com.l2jserver.gameserver.instancemanager.PunishmentManager;

/**
 * @author UnAfraid
 */
public class PunishmentTask implements Runnable
{
	private final int _id;
	private final String _key;
	private final PunishmentAffect _affect;
	private final PunishmentType _type;
	private final long _expirationTime;
	private final String _reason;
	private final String _punishedBy;
	private final boolean _isStored;
	private ScheduledFuture<?> _task = null;
	
	public PunishmentTask(Object key, PunishmentAffect affect, PunishmentType type, long expirationTime, String reason, String punishedBy)
	{
		this(0, key, affect, type, expirationTime, reason, punishedBy, false);
	}
	
	public PunishmentTask(int id, Object key, PunishmentAffect affect, PunishmentType type, long expirationTime, String reason, String punishedBy, boolean isStored)
	{
		_id = id;
		_key = String.valueOf(key);
		_affect = affect;
		_type = type;
		_expirationTime = expirationTime;
		_reason = reason;
		_punishedBy = punishedBy;
		_isStored = isStored;
		
		startPunishment();
	}
	
	public Object getKey()
	{
		return _key;
	}
	
	public PunishmentAffect getAffect()
	{
		return _affect;
	}
	
	public PunishmentType getType()
	{
		return _type;
	}
	
	public final long getExpirationTime()
	{
		return _expirationTime;
	}
	
	public String getReason()
	{
		return _reason;
	}
	
	public String getPunishedBy()
	{
		return _punishedBy;
	}
	
	public boolean isStored()
	{
		return _isStored;
	}
	
	public final boolean isExpired()
	{
		return (_expirationTime > 0) && (System.currentTimeMillis() > _expirationTime);
	}
	
	private void startPunishment()
	{
		if (isExpired())
		{
			return;
		}
		
		onStart();
		if (_expirationTime > 0) // Has expiration?
		{
			_task = ThreadPoolManager.getInstance().scheduleGeneral(this, (_expirationTime - System.currentTimeMillis()));
		}
	}
	
	public final void stopPunishment()
	{
		abortTask();
		onEnd();
	}
	
	private void abortTask()
	{
		if (_task != null)
		{
			if (!_task.isCancelled() && !_task.isDone())
			{
				_task.cancel(false);
			}
			_task = null;
		}
	}
	
	private void onStart()
	{
		if (!_isStored)
		{
			DAOFactory.getInstance().getPunishmentDAO().onStart(_key, _affect, _type, _expirationTime, _reason, _punishedBy, _id, _isStored);
		}
		
		var handler = PunishmentHandler.getInstance().getHandler(_type);
		if (handler != null)
		{
			handler.onStart(this);
		}
	}
	
	private void onEnd()
	{
		if (_isStored)
		{
			DAOFactory.getInstance().getPunishmentDAO().onEnd(_id);
		}
		
		var handler = PunishmentHandler.getInstance().getHandler(_type);
		if (handler != null)
		{
			handler.onEnd(this);
		}
	}
	
	@Override
	public final void run()
	{
		PunishmentManager.getInstance().stopPunishment(_key, _affect, _type);
	}
}
