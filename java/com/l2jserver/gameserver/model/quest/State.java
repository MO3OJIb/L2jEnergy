/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.quest;

/**
 * @author Luis Arias
 * @author Fulminus
 */
public class State
{
	public static final int CREATED = 0;
	public static final int STARTED = 1;
	public static final int COMPLETED = 2;
	
	public static String getStateName(int state)
	{
		return switch (state)
		{
			case STARTED -> "Started";
			case COMPLETED -> "Completed";
			default -> "Start";
		};
	}
	
	public static int getStateId(String statename)
	{
		return switch (statename)
		{
			case "Started" -> 1;
			case "Completed" -> 2;
			default -> 0;
		};
	}
}
