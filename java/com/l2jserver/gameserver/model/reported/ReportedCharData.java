/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.reported;

import java.util.HashMap;
import java.util.Map;

import com.l2jserver.gameserver.model.L2Clan;

public class ReportedCharData
{
	private final Map<Integer, Long> _reporters;
	
	public ReportedCharData()
	{
		_reporters = new HashMap<>();
	}
	
	public int getReportCount()
	{
		return _reporters.size();
	}
	
	public boolean alredyReportedBy(int objectId)
	{
		return _reporters.containsKey(objectId);
	}
	
	public void addReporter(int objectId, long reportTime)
	{
		_reporters.put(objectId, reportTime);
	}
	
	public boolean reportedBySameClan(L2Clan clan)
	{
		if (clan == null)
		{
			return false;
		}
		
		for (var reporterId : _reporters.keySet())
		{
			if (clan.isMember(reporterId))
			{
				return true;
			}
		}
		return false;
	}
	
	public Map<Integer, Long> getReporters()
	{
		return _reporters;
	}
}
