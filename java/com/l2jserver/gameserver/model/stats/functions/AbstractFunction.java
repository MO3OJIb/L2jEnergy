/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.stats.functions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.enums.actors.Stats;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.skills.Skill;

/**
 * A Function object is a component of a Calculator created to manage and dynamically calculate the effect of a character property (ex : MAX_HP, REGENERATE_HP_RATE...).<br>
 * In fact, each calculator is a table of functions object in which each function represents a mathematics function:<br>
 * FuncAtkAccuracy -> Math.sqrt(_player.getDEX())*6+_player.getLevel()<br>
 * When the calc method of a calculator is launched, each mathematics function is called according to its priority <B>_order</B>.<br>
 * Indeed, functions with lowest priority order is executed first and functions with the same order are executed in unspecified order.<br>
 * @author Zoey76
 */
public abstract class AbstractFunction
{
	protected static final Logger LOG = LoggerFactory.getLogger(AbstractFunction.class);
	
	private final Stats _stat;
	private final int _order;
	private final Object _funcOwner;
	private final Condition _applayCond;
	private final double _value;
	
	public AbstractFunction(Stats stat, int order, Object owner, double value, Condition applayCond)
	{
		_stat = stat;
		_order = order;
		_funcOwner = owner;
		_value = value;
		_applayCond = applayCond;
	}
	
	public Condition getApplayCond()
	{
		return _applayCond;
	}
	
	public final Object getFuncOwner()
	{
		return _funcOwner;
	}
	
	public final int getOrder()
	{
		return _order;
	}
	
	public final Stats getStat()
	{
		return _stat;
	}
	
	public final double getValue()
	{
		return _value;
	}
	
	public abstract double calc(L2Character effector, L2Character effected, Skill skill, double initVal);
}
