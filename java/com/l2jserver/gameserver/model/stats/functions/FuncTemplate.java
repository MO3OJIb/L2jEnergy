/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.stats.functions;

import java.lang.reflect.Constructor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.enums.StatFunction;
import com.l2jserver.gameserver.enums.actors.Stats;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.gameserver.model.skills.Skill;

/**
 * Function template.
 * @author mkizub, Zoey76
 */
public final class FuncTemplate
{
	private static final Logger LOG = LoggerFactory.getLogger(FuncTemplate.class);
	
	private final Condition _attachCond;
	private final Condition _applayCond;
	private final Constructor<?> _constructor;
	private final Stats _stat;
	private final int _order;
	private final double _value;
	
	public FuncTemplate(Condition attachCond, Condition applayCond, String functionName, int order, Stats stat, double value)
	{
		var function = StatFunction.valueOf(functionName.toUpperCase());
		if (order >= 0)
		{
			_order = order;
		}
		else
		{
			_order = function.getOrder();
		}
		
		_attachCond = attachCond;
		_applayCond = applayCond;
		_stat = stat;
		_value = value;
		
		try
		{
			var functionClass = Class.forName("com.l2jserver.gameserver.model.stats.functions.Func" + function.getName());
			_constructor = functionClass.getConstructor(Stats.class, // Stats to update
				Integer.TYPE, // Order of execution
				Object.class, // Owner
				Double.TYPE, // Value for function
				Condition.class // Condition
			);
		}
		catch (ClassNotFoundException | NoSuchMethodException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	public Stats getStat()
	{
		return _stat;
	}
	
	public int getOrder()
	{
		return _order;
	}
	
	public double getValue()
	{
		return _value;
	}
	
	public AbstractFunction getFunc(L2Character caster, L2Character target, Skill skill, Object owner)
	{
		return getFunc(caster, target, skill, null, owner);
	}
	
	public AbstractFunction getFunc(L2Character caster, L2Character target, L2ItemInstance item, Object owner)
	{
		return getFunc(caster, target, null, item, owner);
	}
	
	private AbstractFunction getFunc(L2Character caster, L2Character target, Skill skill, L2ItemInstance item, Object owner)
	{
		if ((_attachCond != null) && !_attachCond.test(caster, target, skill))
		{
			return null;
		}
		try
		{
			return (AbstractFunction) _constructor.newInstance(_stat, _order, owner, _value, _applayCond);
		}
		catch (Exception ex)
		{
			LOG.warn("", ex);
		}
		return null;
	}
}
