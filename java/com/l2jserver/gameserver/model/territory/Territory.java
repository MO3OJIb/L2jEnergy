/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.territory;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.instancemanager.TerritoryWarManager;
import com.l2jserver.gameserver.model.L2Clan;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.instance.L2SiegeFlagInstance;

public class Territory
{
	private final Logger LOG = LoggerFactory.getLogger(Territory.class);
	
	private final int _territoryId;
	private final int _castleId; // territory Castle
	private int _fortId; // territory Fortress
	private L2Clan _ownerClan;
	private final List<TerritoryNPCSpawn> _spawnList = new LinkedList<>();
	private final TerritoryNPCSpawn[] _territoryWardSpawnPlaces;
	private boolean _isInProgress = false;
	private L2SiegeFlagInstance _territoryHQ = null;
	private final int[] _questDone;
	
	public Territory(int castleId)
	{
		_castleId = castleId;
		_territoryId = castleId + 80;
		_territoryWardSpawnPlaces = new TerritoryNPCSpawn[9];
		_questDone = new int[2];
	}
	
	public void addWardSpawnPlace(Location loc)
	{
		for (var i = 0; i < _territoryWardSpawnPlaces.length; i++)
		{
			if (_territoryWardSpawnPlaces[i] == null)
			{
				_territoryWardSpawnPlaces[i] = new TerritoryNPCSpawn(_castleId, loc, 0, 4, null);
				return;
			}
		}
	}
	
	public TerritoryNPCSpawn getFreeWardSpawnPlace()
	{
		for (var _territoryWardSpawnPlace : _territoryWardSpawnPlaces)
		{
			if ((_territoryWardSpawnPlace != null) && (_territoryWardSpawnPlace.getNpc() == null))
			{
				return _territoryWardSpawnPlace;
			}
		}
		
		LOG.warn("Could not free Ward spawn found for territory: {}!", _territoryId);
		
		for (var i = 0; i < _territoryWardSpawnPlaces.length; i++)
		{
			if (_territoryWardSpawnPlaces[i] == null)
			{
				LOG.warn("Could not territory ward spawn place {} is null!", i);
			}
			else if (_territoryWardSpawnPlaces[i].getNpc() != null)
			{
				LOG.warn("Could not territory ward spawn place {} has npc name: {}!", i, _territoryWardSpawnPlaces[i].getNpc().getName());
			}
			else
			{
				LOG.warn("Could not territory ward spawn place {} is empty!", i);
			}
		}
		return null;
	}
	
	public List<TerritoryNPCSpawn> getSpawnList()
	{
		return _spawnList;
	}
	
	public void changeNPCsSpawn(int type, boolean isSpawn)
	{
		if ((type < 0) || (type > 3))
		{
			LOG.warn("Could not wrong type({}) for NPCs spawn change!", type);
			return;
		}
		for (var twSpawn : _spawnList)
		{
			if (twSpawn.getType() != type)
			{
				continue;
			}
			if (isSpawn)
			{
				twSpawn.setNPC(TerritoryWarManager.getInstance().spawnNPC(twSpawn.getId(), twSpawn.getLocation()));
			}
			else
			{
				var npc = twSpawn.getNpc();
				if ((npc != null) && !npc.isDead())
				{
					npc.deleteMe();
				}
				twSpawn.setNPC(null);
			}
		}
	}
	
	public void removeWard(int wardId)
	{
		for (var wardSpawn : _territoryWardSpawnPlaces)
		{
			if (wardSpawn.getId() == wardId)
			{
				wardSpawn.getNpc().deleteMe();
				wardSpawn.setNPC(null);
				wardSpawn.setId(0);
				return;
			}
		}
		LOG.warn("Could not delete wardId: {} for territory: {}!", wardId, _territoryId);
	}
	
	public int getTerritoryId()
	{
		return _territoryId;
	}
	
	public int getCastleId()
	{
		return _castleId;
	}
	
	public int getFortId()
	{
		return _fortId;
	}
	
	public void setFortId(int fortId)
	{
		_fortId = fortId;
	}
	
	public L2Clan getOwnerClan()
	{
		return _ownerClan;
	}
	
	public void setOwnerClan(L2Clan newOwner)
	{
		_ownerClan = newOwner;
	}
	
	public void setHQ(L2SiegeFlagInstance hq)
	{
		_territoryHQ = hq;
	}
	
	public L2SiegeFlagInstance getHQ()
	{
		return _territoryHQ;
	}
	
	public TerritoryNPCSpawn[] getOwnedWard()
	{
		return _territoryWardSpawnPlaces;
	}
	
	public int[] getQuestDone()
	{
		return _questDone;
	}
	
	public List<Integer> getOwnedWardIds()
	{
		List<Integer> ret = new LinkedList<>();
		for (var wardSpawn : _territoryWardSpawnPlaces)
		{
			if (wardSpawn.getId() > 0)
			{
				ret.add(wardSpawn.getId());
			}
		}
		return ret;
	}
	
	public boolean isInProgress()
	{
		return _isInProgress;
	}
	
	public void setIsInProgress(boolean val)
	{
		_isInProgress = val;
	}
}
