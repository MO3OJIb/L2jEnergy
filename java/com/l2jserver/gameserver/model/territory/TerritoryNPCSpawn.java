/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.territory;

import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.interfaces.IIdentifiable;

public class TerritoryNPCSpawn implements IIdentifiable
{
	private final Location _location;
	protected int _npcId;
	private final int _castleId;
	private final int _type;
	private L2Npc _npc;
	
	public TerritoryNPCSpawn(int castleId, Location location, int npcId, int type, L2Npc npc)
	{
		_castleId = castleId;
		_location = location;
		_npcId = npcId;
		_type = type;
		_npc = npc;
	}
	
	public int getCastleId()
	{
		return _castleId;
	}
	
	@Override
	public int getId()
	{
		return _npcId;
	}
	
	public void setId(int npcId)
	{
		_npcId = npcId;
	}
	
	public int getType()
	{
		return _type;
	}
	
	public void setNPC(L2Npc npc)
	{
		if (_npc != null)
		{
			_npc.deleteMe();
		}
		_npc = npc;
	}
	
	public L2Npc getNpc()
	{
		return _npc;
	}
	
	public Location getLocation()
	{
		return _location;
	}
}
