/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.variables;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * @author UnAfraid
 */
public class PlayerVariables extends AbstractVariables
{
	private static final Logger LOG = LoggerFactory.getLogger(PlayerVariables.class);
	
	// SQL Queries.
	private static final String SELECT_QUERY = "SELECT * FROM character_variables WHERE charId = ?";
	private static final String DELETE_QUERY = "DELETE FROM character_variables WHERE charId = ?";
	private static final String INSERT_QUERY = "INSERT INTO character_variables (charId, var, val) VALUES (?, ?, ?)";
	
	private final int _objectId;
	
	public static final String USED_PC_LOTTERY_TICKET = "USED_PC_LOTTERY_TICKET";
	public static final String USED_PC_FRIEND_RECOMMENDATION_PROOF = "USED_PC_FRIEND_RECOMMENDATION_PROOF";
	public static final String PCC_CODE_ATTEMPTS = "PCC_CODE_ATTEMPTS";
	
	public static final String HUNTING_BONUS = "HUNTING_BONUS";
	public static final String NEVIT_BLESSING_POINT = "NEVIT_BLESSING_POINT";
	public static final String NEVIT_BLESSING_TIME = "NEVIT_BLESSING_TIME";
	
	public static final String LAST_HERO_BACK_COORDINATES = "";
	
	public PlayerVariables(int objectId)
	{
		_objectId = objectId;
		restoreMe();
	}
	
	@Override
	public boolean restoreMe()
	{
		// Restore previous variables.
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_QUERY))
		{
			ps.setInt(1, _objectId);
			try (var rset = ps.executeQuery())
			{
				while (rset.next())
				{
					set(rset.getString("var"), rset.getString("val"));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Couldn't restore variables for: {}!", getPlayer(), ex);
			return false;
		}
		finally
		{
			compareAndSetChanges(true, false);
		}
		return true;
	}
	
	@Override
	public boolean storeMe()
	{
		// No changes, nothing to store.
		if (!hasChanges())
		{
			return false;
		}
		
		try (var con = ConnectionFactory.getInstance().getConnection())
		{
			// Clear previous entries.
			try (var st = con.prepareStatement(DELETE_QUERY))
			{
				st.setInt(1, _objectId);
				st.execute();
			}
			
			// Insert all variables.
			try (var st = con.prepareStatement(INSERT_QUERY))
			{
				st.setInt(1, _objectId);
				for (var entry : getSet().entrySet())
				{
					st.setString(2, entry.getKey());
					st.setString(3, String.valueOf(entry.getValue()));
					st.addBatch();
				}
				st.executeBatch();
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Couldn't update variables for: {}!", getPlayer(), ex);
			return false;
		}
		finally
		{
			compareAndSetChanges(true, false);
		}
		return true;
	}
	
	public L2PcInstance getPlayer()
	{
		return L2World.getInstance().getPlayer(_objectId);
	}
}
