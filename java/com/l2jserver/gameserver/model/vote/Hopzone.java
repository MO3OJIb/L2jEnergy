/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.vote;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import com.l2jserver.gameserver.configuration.config.custom.TopsConfig;
import com.l2jserver.gameserver.enums.Tops;

public class Hopzone extends TopSystem
{
	@Override
	public void init()
	{
		if (!TopsConfig.ALLOW_HOPZONE_VOTE_REWARD)
		{
			return;
		}
		super.init();
	}
	
	@Override
	public Map<Integer, Integer> getReward()
	{
		return TopsConfig.HOPZONE_REWARD;
	}
	
	@Override
	protected String getLink()
	{
		return TopsConfig.HOPZONE_SERVER_LINK;
	}
	
	@Override
	protected int getDelay()
	{
		return TopsConfig.HOPZONE_REWARD_CHECK_TIME * 60000;
	}
	
	@Override
	public Tops getName()
	{
		return Tops.HOPZONENET;
	}
	
	@Override
	protected void getPage()
	{
		var sb = new StringBuffer();
		try
		{
			var uri = new URI(getLink());
			var urlConnection = uri.toURL().openConnection();
			urlConnection.addRequestProperty("User-Agent", "Mozilla/5.0");
			
			try (var in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), StandardCharsets.UTF_8)))
			{
				String line;
				while ((line = in.readLine()) != null)
				{
					sb.append(line);
				}
			}
		}
		catch (Exception ex)
		{
			LOG.error("", ex);
		}
		LOG.info("Update Vote Bonus System for {} services!", getName().getLink());
	}
}