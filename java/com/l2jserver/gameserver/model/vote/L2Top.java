/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.vote;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Calendar;
import java.util.Map;
import java.util.StringTokenizer;

import com.l2jserver.gameserver.configuration.config.custom.TopsConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.enums.Tops;

/**
 * @author Мо3олЬ
 */
public class L2Top extends TopSystem
{
	@Override
	public void init()
	{
		if (!TopsConfig.ALLOW_L2TOP_VOTE_REWARD)
		{
			return;
		}
		super.init();
	}
	
	@Override
	public Map<Integer, Integer> getReward()
	{
		return TopsConfig.L2TOP_REWARD;
	}
	
	@Override
	protected String getLink()
	{
		return TopsConfig.L2TOP_SERVER_LINK;
	}
	
	@Override
	protected int getDelay()
	{
		return TopsConfig.L2TOP_REWARD_CHECK_TIME * 60000;
	}
	
	@Override
	public Tops getName()
	{
		return Tops.L2TOPRU;
	}
	
	protected boolean sms()
	{
		return false;
	}
	
	@Override
	protected void getPage()
	{
		try
		{
			var url = new URI(getLink()).toURL();
			var in = new BufferedReader(new InputStreamReader(url.openStream(), "utf8"));
			for (var line = in.readLine(); line != null; line = in.readLine())
			{
				var calendar = Calendar.getInstance();
				
				var year = calendar.get(Calendar.YEAR);
				if (line.startsWith(String.valueOf(year)))
				{
					try
					{
						var st = new StringTokenizer(line, "\t ");
						var date = st.nextToken().split("-");
						var time = st.nextToken().split(":");
						var nick = st.nextToken();
						if (!TopsConfig.L2TOP_SERVER_PREFIX.isEmpty())
						{
							nick = nick.replace(TopsConfig.L2TOP_SERVER_PREFIX + "-", "");
						}
						
						var voteType = 0;
						if (sms())
						{
							voteType = Integer.parseInt(st.nextToken().replace("x", ""));
						}
						calendar.set(Calendar.YEAR, Integer.parseInt(date[0]));
						calendar.set(Calendar.MONTH, Integer.parseInt(date[1]));
						calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date[2]));
						calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
						calendar.set(Calendar.MINUTE, Integer.parseInt(time[1]));
						calendar.set(Calendar.SECOND, Integer.parseInt(time[2]));
						calendar.set(Calendar.MILLISECOND, 0);
						
						var voteTime = calendar.getTimeInMillis() / 1000;
						
						if ((voteTime + (TopsConfig.TOP_SAVE_DAYS * 86400)) > (System.currentTimeMillis() / 1000))
						{
							DAOFactory.getInstance().getPlayerDAO().checkAndSave(getName(), nick, voteTime, voteType);
						}
						
					}
					catch (Exception ex)
					{
						LOG.error("", ex);
						continue;
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.error("", ex);
		}
		
		LOG.info("Update Vote Bonus System for {} services!", getName().getLink());
	}
}