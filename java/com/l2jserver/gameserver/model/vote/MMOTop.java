/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.vote;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Calendar;
import java.util.Map;
import java.util.StringTokenizer;

import com.l2jserver.gameserver.configuration.config.custom.TopsConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.enums.Tops;

/**
 * @author Мо3олЬ
 */
public class MMOTop extends TopSystem
{
	@Override
	public void init()
	{
		if (!TopsConfig.ALLOW_MMO_TOP_VOTE_REWARD)
		{
			return;
		}
		super.init();
	}
	
	@Override
	public Map<Integer, Integer> getReward()
	{
		return TopsConfig.MMOTOP_REWARD;
	}
	
	@Override
	protected String getLink()
	{
		return TopsConfig.MMOTOP_SERVER_LINK;
	}
	
	@Override
	protected int getDelay()
	{
		return TopsConfig.MMOTOP_REWARD_CHECK_TIME * 60000;
	}
	
	@Override
	public Tops getName()
	{
		return Tops.MMOTOPRU;
	}
	
	@Override
	protected void getPage()
	{
		try
		{
			var url = new URI(getLink()).toURL();
			var in = new BufferedReader(new InputStreamReader(url.openStream(), "utf8"));
			
			String line;
			while ((line = in.readLine()) != null)
			{
				var st = new StringTokenizer(line, "\t. :");
				while (st.hasMoreTokens())
				{
					try
					{
						st.nextToken();
						var day = Integer.parseInt(st.nextToken());
						var month = Integer.parseInt(st.nextToken()) - 1;
						var year = Integer.parseInt(st.nextToken());
						var hour = Integer.parseInt(st.nextToken());
						var minute = Integer.parseInt(st.nextToken());
						var second = Integer.parseInt(st.nextToken());
						st.nextToken();
						st.nextToken();
						st.nextToken();
						st.nextToken();
						var nick = st.nextToken();
						var voteType = Integer.parseInt(st.nextToken());
						var calendar = Calendar.getInstance();
						calendar.set(1, year);
						calendar.set(2, month);
						calendar.set(5, day);
						calendar.set(11, hour);
						calendar.set(12, minute);
						calendar.set(13, second);
						calendar.set(14, 0);
						var voteTime = calendar.getTimeInMillis() / 1000;
						
						if ((voteTime + (TopsConfig.TOP_SAVE_DAYS * 86400)) > (System.currentTimeMillis() / 1000))
						{
							DAOFactory.getInstance().getPlayerDAO().checkAndSave(getName(), nick, voteTime, voteType);
						}
					}
					catch (Exception ex)
					{
						LOG.warn("", ex);
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.error("", ex);
		}
		LOG.info("Update Vote Bonus System for {} services!", getName().getLink());
	}
}
