/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.vote;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.custom.TopsConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.MailMessageType;
import com.l2jserver.gameserver.enums.Tops;
import com.l2jserver.gameserver.instancemanager.MailManager;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.Message;
import com.l2jserver.gameserver.util.LoggingUtils;

public abstract class TopSystem
{
	protected static final Logger LOG = LoggerFactory.getLogger(TopSystem.class);
	protected static final Logger LOG_VOTE = LoggerFactory.getLogger("vote");
	
	private static final String SELECT_TOP_DATA = "SELECT reward FROM character_votes WHERE charId=? AND type=? AND given=0";
	private static final String UPDATE_TOP_DATA = "UPDATE character_votes SET given=1 WHERE charId=? AND type=?";
	
	protected abstract void getPage();
	
	protected abstract String getLink();
	
	protected abstract int getDelay();
	
	public abstract Tops getName();
	
	public abstract Map<Integer, Integer> getReward();
	
	public void init()
	{
		ThreadPoolManager.getInstance().scheduleAiAtFixedRate(new ConnectAndUpdate(), getDelay(), getDelay());
		ThreadPoolManager.getInstance().scheduleAiAtFixedRate(new Clean(), getDelay(), getDelay());
		ThreadPoolManager.getInstance().scheduleAiAtFixedRate(new GiveReward(), getDelay(), getDelay());
		
		LOG.info("Loaded Vote Bonus System for {} services started.", getName().getLink());
	}
	
	protected class ConnectAndUpdate implements Runnable
	{
		public ConnectAndUpdate()
		{
			
		}
		
		@Override
		public void run()
		{
			getPage();
		}
	}
	
	protected class Clean implements Runnable
	{
		public Clean()
		{
			
		}
		
		@Override
		public void run()
		{
			DAOFactory.getInstance().getPlayerDAO().clean();
		}
	}
	
	private class GiveReward implements Runnable
	{
		public GiveReward()
		{
			
		}
		
		@Override
		public void run()
		{
			giveReward();
		}
	}
	
	public void giveReward()
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_TOP_DATA);
			var ps1 = con.prepareStatement(UPDATE_TOP_DATA))
		{
			for (var winner : L2World.getInstance().getPlayers())
			{
				var charId = winner.getObjectId();
				
				var reward = 0;
				var items = getReward();
				
				ps.setInt(1, charId);
				ps.setString(2, getName().getLink());
				try (var rs = ps.executeQuery())
				{
					while (rs.next())
					{
						reward += rs.getInt("reward");
					}
					
					ps1.setInt(1, charId);
					ps1.setString(2, getName().getLink());
					ps1.executeUpdate();
					
					if (reward > 0)
					{
						getRewardsVote(winner, items, reward);
					}
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("", ex);
		}
	}
	
	public void getRewardsVote(L2PcInstance player, Map<Integer, Integer> items, int reward)
	{
		var winner = (L2PcInstance) L2World.getInstance().getObject(player.getObjectId());
		
		var text1 = MessagesData.getInstance().getMessage(player, "event_vote_bonus_title").replace("%i%", getName().getLink() + "");
		var text2 = MessagesData.getInstance().getMessage(player, "event_vote_bonus_text").replace("%i%", getName().getLink() + "").replace("%s%", TopsConfig.TOP_SERVER_ADDRESS + "");
		
		for (var entry : items.entrySet())
		{
			var msg = new Message(winner.getObjectId(), text1, text2, MailMessageType.EVENTS);
			var attachments = msg.createAttachments();
			attachments.addItem(getName().getLink(), entry.getKey(), entry.getValue() * reward, null, null);
			MailManager.getInstance().sendMessage(msg);
			
			LoggingUtils.logVoteSystemBonus(LOG_VOTE, "Give Vote Bonus System", getName(), player, entry.getKey(), entry.getValue() * reward);
		}
	}
}
