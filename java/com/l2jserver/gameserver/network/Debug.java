/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network;

import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.model.Elementals;
import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.network.serverpackets.TutorialShowHtml;

/**
 * @author UnAfraid
 */
public class Debug
{
	public static void sendSkillDebug(L2Character attacker, L2Character target, Skill skill, StatsSet set)
	{
		if (!attacker.isPlayer())
		{
			return;
		}
		
		var sb = new StringBuilder();
		for (var entry : set.getSet().entrySet())
		{
			sb.append("<tr><td>" + entry.getKey() + "</td><td><font color=\"LEVEL\">" + entry.getValue() + "</font></td></tr>");
		}
		
		var html = new NpcHtmlMessage();
		html.setFile(attacker.getActingPlayer(), "data/html/admin/skilldebug.htm");
		html.replace("%patk%", target.getPAtk(target));
		html.replace("%matk%", target.getMAtk(target, skill));
		html.replace("%pdef%", target.getPDef(target));
		html.replace("%mdef%", target.getMDef(target, skill));
		html.replace("%acc%", target.getAccuracy());
		html.replace("%evas%", target.getEvasionRate(target));
		html.replace("%crit%", target.getCriticalHit(target, skill));
		html.replace("%speed%", target.getRunSpeed());
		html.replace("%pAtkSpd%", target.getPAtkSpd());
		html.replace("%mAtkSpd%", target.getMAtkSpd());
		html.replace("%str%", target.getSTR());
		html.replace("%dex%", target.getDEX());
		html.replace("%con%", target.getCON());
		html.replace("%int%", target.getINT());
		html.replace("%wit%", target.getWIT());
		html.replace("%men%", target.getMEN());
		html.replace("%atkElemType%", Elementals.getElementName(target.getAttackElement()));
		html.replace("%atkElemVal%", target.getAttackElementValue(target.getAttackElement()));
		html.replace("%fireDef%", target.getDefenseElementValue((byte) 0));
		html.replace("%waterDef%", target.getDefenseElementValue((byte) 1));
		html.replace("%windDef%", target.getDefenseElementValue((byte) 2));
		html.replace("%earthDef%", target.getDefenseElementValue((byte) 3));
		html.replace("%holyDef%", target.getDefenseElementValue((byte) 4));
		html.replace("%darkDef%", target.getDefenseElementValue((byte) 5));
		html.replace("%skill%", skill.toString());
		html.replace("%details%", sb.toString());
		attacker.sendPacket(new TutorialShowHtml(html.getHtml()));
	}
	
	public static void sendItemDebug(L2PcInstance player, L2ItemInstance item, StatsSet set)
	{
		var sb = new StringBuilder();
		for (var entry : set.getSet().entrySet())
		{
			sb.append("<tr><td>" + entry.getKey() + "</td><td><font color=\"LEVEL\">" + entry.getValue() + "</font></td></tr>");
		}
		
		var html = new NpcHtmlMessage();
		html.setFile(player, "data/html/admin/itemdebug.htm");
		html.replace("%itemName%", item.getName());
		html.replace("%itemSlot%", getBodyPart(item.getItem().getBodyPart()));
		html.replace("%itemType%", item.isArmor() ? "Armor" : item.isWeapon() ? "Weapon" : "Etc");
		html.replace("%enchantLevel%", item.getEnchantLevel());
		html.replace("%isMagicWeapon%", item.getItem().isMagicWeapon());
		html.replace("%item%", item.toString());
		html.replace("%details%", sb.toString());
		player.sendPacket(new TutorialShowHtml(html.getHtml()));
	}
	
	private static String getBodyPart(int bodyPart)
	{
		for (var entry : ItemTable.SLOTS.entrySet())
		{
			if ((entry.getValue() & bodyPart) == bodyPart)
			{
				return entry.getKey();
			}
		}
		return "Unknown";
	}
}
