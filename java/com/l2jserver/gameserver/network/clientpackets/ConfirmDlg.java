/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.configuration.config.AdminConfig;
import com.l2jserver.gameserver.configuration.config.events.WeddingConfig;
import com.l2jserver.gameserver.data.xml.impl.AdminData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.PlayerAction;
import com.l2jserver.gameserver.handler.AdminCommandHandler;
import com.l2jserver.gameserver.model.eventengine.GameEventManager;
import com.l2jserver.gameserver.model.events.EventDispatcher;
import com.l2jserver.gameserver.model.events.impl.character.player.OnPlayerDlgAnswer;
import com.l2jserver.gameserver.model.events.returns.TerminateReturn;
import com.l2jserver.gameserver.model.holders.DoorRequestHolder;
import com.l2jserver.gameserver.model.holders.SummonRequestHolder;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.LoggingUtils;

/**
 * @author Dezmond_snz
 */
public final class ConfirmDlg extends L2GameClientPacket
{
	private static final String _C__C6_CONFIRMDLG = "[C] C6 ConfirmDlg";
	
	private static final Logger LOG_GM_AUDIT = LoggerFactory.getLogger("gmaudit");
	
	private int _messageId;
	private int _answer;
	private int _requesterId;
	
	@Override
	protected void readImpl()
	{
		_messageId = readD();
		_answer = readD();
		_requesterId = readD();
	}
	
	@Override
	public void runImpl()
	{
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		
		var term = EventDispatcher.getInstance().notifyEvent(new OnPlayerDlgAnswer(player, _messageId, _answer, _requesterId), player, TerminateReturn.class);
		if ((term != null) && term.terminate())
		{
			return;
		}
		
		if (_messageId == SystemMessageId.S1.getId())
		{
			if (player.removeAction(PlayerAction.USER_ENGAGE))
			{
				if (WeddingConfig.ALLOW_WEDDING)
				{
					player.engageAnswer(_answer);
				}
			}
			else if (player.removeAction(PlayerAction.ADMIN_COMMAND))
			{
				var cmd = player.getAdminConfirmCmd();
				player.setAdminConfirmCmd(null);
				if (_answer == 0)
				{
					return;
				}
				var command = cmd.split(" ")[0];
				var ach = AdminCommandHandler.getInstance().getHandler(command);
				if (AdminData.getInstance().hasAccess(command, player.getAccessLevel()))
				{
					if (AdminConfig.GMAUDIT)
					{
						LoggingUtils.logGMAudit(LOG_GM_AUDIT, player.getName() + " [" + player.getObjectId() + "]", cmd, (player.getTarget() != null ? player.getTarget().getName() : "no-target"));
					}
					ach.useAdminCommand(cmd, player);
				}
			}
			else if (player.removeAction(PlayerAction.EVENTS))
			{
				if (GameEventManager.getInstance().getEvent().isParticipating())
				{
					if ((player.getObjectId() == _requesterId) && (_answer == 1))
					{
						GameEventManager.getInstance().getEvent().register(player);
					}
					else if ((player.getObjectId() == _requesterId) && (_answer == 0))
					{
						player.sendMessage(MessagesData.getInstance().getMessage(player, "events_answer_no"));
					}
				}
			}
		}
		else if ((_messageId == SystemMessageId.RESURRECTION_REQUEST_BY_C1_FOR_S2_XP.getId()) || (_messageId == SystemMessageId.RESURRECT_USING_CHARM_OF_COURAGE.getId()))
		{
			player.reviveAnswer(_answer);
		}
		else if (_messageId == SystemMessageId.C1_WISHES_TO_SUMMON_YOU_FROM_S2_DO_YOU_ACCEPT.getId())
		{
			var holder = player.removeScript(SummonRequestHolder.class);
			if ((_answer == 1) && (holder != null) && holder.getRequester().canSummonTarget(player) && (holder.getRequester().getObjectId() == _requesterId))
			{
				if ((holder.getItemId() != 0) && (holder.getItemCount() != 0))
				{
					if (player.getInventory().getInventoryItemCount(holder.getItemId(), 0) < holder.getItemCount())
					{
						var sm = SystemMessage.getSystemMessage(SystemMessageId.S1_REQUIRED_FOR_SUMMONING);
						sm.addItemName(holder.getItemId());
						player.sendPacket(sm);
						return;
					}
					player.getInventory().destroyItemByItemId("Consume", holder.getItemId(), holder.getItemCount(), player, player);
					var sm = SystemMessage.getSystemMessage(SystemMessageId.S1_DISAPPEARED);
					sm.addItemName(holder.getItemId());
					player.sendPacket(sm);
				}
				player.teleToLocation(holder.getRequester().getLocation(), true);
			}
		}
		else if (_messageId == SystemMessageId.WOULD_YOU_LIKE_TO_OPEN_THE_GATE.getId())
		{
			var holder = player.removeScript(DoorRequestHolder.class);
			if ((holder != null) && (holder.getDoor() == player.getTarget()) && (_answer == 1))
			{
				holder.getDoor().openMe();
			}
		}
		else if (_messageId == SystemMessageId.WOULD_YOU_LIKE_TO_CLOSE_THE_GATE.getId())
		{
			var holder = player.removeScript(DoorRequestHolder.class);
			if ((holder != null) && (holder.getDoor() == player.getTarget()) && (_answer == 1))
			{
				holder.getDoor().closeMe();
			}
		}
	}
	
	@Override
	public String getType()
	{
		return _C__C6_CONFIRMDLG;
	}
}
