/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.l2jserver.gameserver.AutoRestartServer;
import com.l2jserver.gameserver.GameTimeController;
import com.l2jserver.gameserver.LoginServerThread;
import com.l2jserver.gameserver.SevenSigns;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.AdminConfig;
import com.l2jserver.gameserver.configuration.config.CharacterConfig;
import com.l2jserver.gameserver.configuration.config.DeveloperConfig;
import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.configuration.config.RatesConfig;
import com.l2jserver.gameserver.configuration.config.ServerConfig;
import com.l2jserver.gameserver.configuration.config.custom.CustomConfig;
import com.l2jserver.gameserver.configuration.config.events.PcCafeConfig;
import com.l2jserver.gameserver.configuration.config.events.WeddingConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.data.sql.impl.AnnouncementsTable;
import com.l2jserver.gameserver.data.xml.impl.AdminData;
import com.l2jserver.gameserver.data.xml.impl.DimensionalRiftData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.SkillData;
import com.l2jserver.gameserver.data.xml.impl.SkillTreesData;
import com.l2jserver.gameserver.enums.PcCafeType;
import com.l2jserver.gameserver.enums.PunishmentAffect;
import com.l2jserver.gameserver.enums.PunishmentType;
import com.l2jserver.gameserver.enums.TeleportWhereType;
import com.l2jserver.gameserver.enums.ZoneId;
import com.l2jserver.gameserver.enums.network.GameClientState;
import com.l2jserver.gameserver.enums.skills.CommonSkill;
import com.l2jserver.gameserver.instancemanager.CHSiegeManager;
import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.instancemanager.ClanHallManager;
import com.l2jserver.gameserver.instancemanager.CoupleManager;
import com.l2jserver.gameserver.instancemanager.CursedWeaponsManager;
import com.l2jserver.gameserver.instancemanager.FortManager;
import com.l2jserver.gameserver.instancemanager.FortSiegeManager;
import com.l2jserver.gameserver.instancemanager.InstanceManager;
import com.l2jserver.gameserver.instancemanager.MailManager;
import com.l2jserver.gameserver.instancemanager.PetitionManager;
import com.l2jserver.gameserver.instancemanager.PunishmentManager;
import com.l2jserver.gameserver.instancemanager.SiegeManager;
import com.l2jserver.gameserver.instancemanager.TerritoryWarManager;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.PcCondOverride;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.custom.Extension;
import com.l2jserver.gameserver.model.eventengine.GameEventManager;
import com.l2jserver.gameserver.model.holders.ClientHardwareInfoHolder;
import com.l2jserver.gameserver.model.variables.AccountVariables;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ActionFailed;
import com.l2jserver.gameserver.network.serverpackets.Die;
import com.l2jserver.gameserver.network.serverpackets.EtcStatusUpdate;
import com.l2jserver.gameserver.network.serverpackets.ExBasicActionList;
import com.l2jserver.gameserver.network.serverpackets.ExGetBookMarkInfoPacket;
import com.l2jserver.gameserver.network.serverpackets.ExNoticePostArrived;
import com.l2jserver.gameserver.network.serverpackets.ExNotifyPremiumItem;
import com.l2jserver.gameserver.network.serverpackets.ExPCCafePointInfo;
import com.l2jserver.gameserver.network.serverpackets.ExReceiveShowPostFriend;
import com.l2jserver.gameserver.network.serverpackets.ExShowScreenMessage;
import com.l2jserver.gameserver.network.serverpackets.ExStorageMaxCount;
import com.l2jserver.gameserver.network.serverpackets.ExUISetting;
import com.l2jserver.gameserver.network.serverpackets.ExVoteSystemInfo;
import com.l2jserver.gameserver.network.serverpackets.HennaInfo;
import com.l2jserver.gameserver.network.serverpackets.ItemList;
import com.l2jserver.gameserver.network.serverpackets.L2FriendList;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.network.serverpackets.PledgeShowMemberListAll;
import com.l2jserver.gameserver.network.serverpackets.PledgeShowMemberListUpdate;
import com.l2jserver.gameserver.network.serverpackets.PledgeSkillList;
import com.l2jserver.gameserver.network.serverpackets.PledgeStatusChanged;
import com.l2jserver.gameserver.network.serverpackets.QuestList;
import com.l2jserver.gameserver.network.serverpackets.ShortCutInit;
import com.l2jserver.gameserver.network.serverpackets.SkillCoolTime;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

public class EnterWorld extends L2GameClientPacket
{
	private static final String _C__11_ENTERWORLD = "[C] 11 EnterWorld";
	
	private static final double MIN_HP = 0.5;
	
	private static final int COMBAT_FLAG = 9819;
	
	private static final Map<String, ClientHardwareInfoHolder> TRACE_HWINFO = new ConcurrentHashMap<>();
	
	private final int[][] _tracert = new int[5][4];
	
	@Override
	protected void readImpl()
	{
		readB(new byte[32]); // Unknown Byte Array
		readD(); // Unknown Value
		readD(); // Unknown Value
		readD(); // Unknown Value
		readD(); // Unknown Value
		readB(new byte[32]); // Unknown Byte Array
		readD(); // Unknown Value
		for (var i = 0; i < 5; i++)
		{
			for (var o = 0; o < 4; o++)
			{
				_tracert[i][o] = readC();
			}
		}
	}
	
	@Override
	protected void runImpl()
	{
		var player = getActiveChar();
		if (player == null)
		{
			LOG.warn("EnterWorld failed! activeChar returned 'null'.");
			getClient().closeNow();
			return;
		}
		
		var address = new String[5];
		for (var i = 0; i < 5; i++)
		{
			address[i] = _tracert[i][0] + "." + _tracert[i][1] + "." + _tracert[i][2] + "." + _tracert[i][3];
		}
		
		LoginServerThread.getInstance().sendClientTracert(player.getAccountName(), address);
		
		getClient().setClientTracert(_tracert);
		
		// Restore to instanced area if enabled
		if (GeneralConfig.RESTORE_PLAYER_INSTANCE)
		{
			player.setInstanceId(InstanceManager.getInstance().getPlayerInstance(player.getObjectId()));
		}
		else
		{
			var instanceId = InstanceManager.getInstance().getPlayerInstance(player.getObjectId());
			if (instanceId > 0)
			{
				InstanceManager.getInstance().getInstance(instanceId).removePlayer(player.getObjectId());
			}
		}
		
		if (DeveloperConfig.DEBUG)
		{
			if (L2World.getInstance().getObject(player.getObjectId()) != null)
			{
				LOG.warn("User already exists in Object ID map! User {} is a character clone.", player.getName());
			}
		}
		
		getClient().setState(GameClientState.IN_GAME);
		
		// Apply special GM properties to the GM when entering
		if (player.isGM())
		{
			if (AdminConfig.GM_STARTUP_INVULNERABLE && AdminData.getInstance().hasAccess("admin_invul", player.getAccessLevel()))
			{
				player.setIsInvul(true);
			}
			
			if (AdminConfig.GM_STARTUP_INVISIBLE && AdminData.getInstance().hasAccess("admin_invisible", player.getAccessLevel()))
			{
				player.setInvisible(true);
			}
			
			if (AdminConfig.GM_STARTUP_SILENCE && AdminData.getInstance().hasAccess("admin_silence", player.getAccessLevel()))
			{
				player.setSilenceMode(true);
			}
			
			if (AdminConfig.GM_STARTUP_DIET_MODE && AdminData.getInstance().hasAccess("admin_diet", player.getAccessLevel()))
			{
				player.setDietMode(true);
				player.refreshOverloaded();
			}
			
			if (AdminConfig.GM_STARTUP_AUTO_LIST && AdminData.getInstance().hasAccess("admin_gmliston", player.getAccessLevel()))
			{
				AdminData.getInstance().addGm(player, false);
			}
			else
			{
				AdminData.getInstance().addGm(player, true);
			}
			
			if (AdminConfig.GM_GIVE_SPECIAL_SKILLS)
			{
				SkillTreesData.getInstance().addSkills(player, false);
			}
			
			if (AdminConfig.GM_GIVE_SPECIAL_AURA_SKILLS)
			{
				SkillTreesData.getInstance().addSkills(player, true);
			}
		}
		
		// Set dead status if applies
		if (player.getCurrentHp() < MIN_HP)
		{
			player.setIsDead(true);
		}
		
		var showClanNotice = false;
		
		// Clan related checks are here
		var clan = player.getClan();
		if (clan != null)
		{
			player.sendPacket(new PledgeSkillList(clan));
			
			notifyClanMembers(player);
			
			notifySponsorOrApprentice(player);
			
			var clanHall = ClanHallManager.getInstance().getClanHallByOwner(clan);
			if (clanHall != null)
			{
				if (!clanHall.getPaid())
				{
					player.sendPacket(SystemMessageId.PAYMENT_FOR_YOUR_CLAN_HALL_HAS_NOT_BEEN_MADE_PLEASE_MAKE_PAYMENT_TO_YOUR_CLAN_WAREHOUSE_BY_S1_TOMORROW);
				}
			}
			
			for (var siege : SiegeManager.getInstance().getSieges())
			{
				if (!siege.isInProgress())
				{
					continue;
				}
				
				if (siege.checkIsAttacker(clan))
				{
					player.setSiegeState((byte) 1);
					player.setSiegeSide(siege.getCastle().getResidenceId());
				}
				else if (siege.checkIsDefender(clan))
				{
					player.setSiegeState((byte) 2);
					player.setSiegeSide(siege.getCastle().getResidenceId());
				}
			}
			
			for (var siege : FortSiegeManager.getInstance().getSieges())
			{
				if (!siege.isInProgress())
				{
					continue;
				}
				
				if (siege.checkIsAttacker(clan))
				{
					player.setSiegeState((byte) 1);
					player.setSiegeSide(siege.getFort().getResidenceId());
				}
				else if (siege.checkIsDefender(clan))
				{
					player.setSiegeState((byte) 2);
					player.setSiegeSide(siege.getFort().getResidenceId());
				}
			}
			
			for (var hall : CHSiegeManager.getInstance().getConquerableHalls().values())
			{
				if (!hall.isInSiege())
				{
					continue;
				}
				
				if (hall.isRegistered(clan))
				{
					player.setSiegeState((byte) 1);
					player.setSiegeSide(hall.getId());
					player.setIsInHideoutSiege(true);
				}
			}
			
			sendPacket(new PledgeShowMemberListAll(clan, player));
			sendPacket(new PledgeStatusChanged(clan));
			
			// Residential skills support
			if (clan.getCastleId() > 0)
			{
				CastleManager.getInstance().getCastleByOwner(clan).giveResidentialSkills(player);
			}
			
			if (clan.getFortId() > 0)
			{
				FortManager.getInstance().getFortByOwner(clan).giveResidentialSkills(player);
			}
			
			showClanNotice = clan.isNoticeEnabled();
		}
		
		if (TerritoryWarManager.getInstance().getRegisteredTerritoryId(player) > 0)
		{
			if (TerritoryWarManager.getInstance().isTWInProgress())
			{
				player.setSiegeState((byte) 1);
			}
			player.setSiegeSide(TerritoryWarManager.getInstance().getRegisteredTerritoryId(player));
		}
		
		// Updating Seal of Strife Buff/Debuff
		if (SevenSigns.getInstance().isSealValidationPeriod() && (SevenSigns.getInstance().getSealOwner(SevenSigns.SEAL_STRIFE) != SevenSigns.CABAL_NULL))
		{
			var cabal = SevenSigns.getInstance().getPlayerCabal(player.getObjectId());
			if (cabal != SevenSigns.CABAL_NULL)
			{
				if (cabal == SevenSigns.getInstance().getSealOwner(SevenSigns.SEAL_STRIFE))
				{
					player.addSkill(CommonSkill.THE_VICTOR_OF_WAR.getSkill());
				}
				else
				{
					player.addSkill(CommonSkill.THE_VANQUISHED_OF_WAR.getSkill());
				}
			}
		}
		else
		{
			player.removeSkill(CommonSkill.THE_VICTOR_OF_WAR.getSkill());
			player.removeSkill(CommonSkill.THE_VANQUISHED_OF_WAR.getSkill());
		}
		
		if (CharacterConfig.ENABLE_VITALITY && CharacterConfig.RECOVER_VITALITY_ON_RECONNECT)
		{
			var points = (RatesConfig.RATE_RECOVERY_ON_RECONNECT * (System.currentTimeMillis() - player.getLastAccess())) / 60000;
			if (points > 0)
			{
				player.updateVitalityPoints(points, false, true);
			}
		}
		
		if (PcCafeConfig.ALT_PCBANG_POINTS_ENABLED)
		{
			player.getPcCafeSystem().startGiveTask();
		}
		
		player.broadcastUserInfo();
		
		// Send keybinds
		if (CharacterConfig.STORE_UI_SETTINGS)
		{
			player.sendPacket(new ExUISetting(player));
		}
		
		// Send Macro List
		player.getMacros().sendUpdate();
		
		// Send Item List
		sendPacket(new ItemList(player, false));
		
		// Send GG check
		player.queryGameGuard();
		
		// Send Teleport Bookmark List
		sendPacket(new ExGetBookMarkInfoPacket(player));
		
		// Send Shortcuts
		sendPacket(new ShortCutInit(player));
		
		// Send Action list
		player.sendPacket(ExBasicActionList.STATIC_PACKET);
		
		// Send Skill list
		player.sendSkillList();
		
		// Apply Dye
		player.recalcHennaStats();
		
		// Send Dye Information
		player.sendPacket(new HennaInfo(player));
		
		DAOFactory.getInstance().getQuestDAO().playerEnter(player);
		player.sendPacket(new QuestList());
		
		if (CharacterConfig.PLAYER_SPAWN_PROTECTION > 0)
		{
			player.setProtection(true);
		}
		
		player.spawnMe(player.getX(), player.getY(), player.getZ());
		
		player.getInventory().applyItemSkills();
		
		// Wedding Checks
		if (WeddingConfig.ALLOW_WEDDING)
		{
			engage(player);
			notifyPartner(player, player.getPartnerId());
		}
		
		if (player.isCursedWeaponEquipped())
		{
			CursedWeaponsManager.getInstance().getCursedWeapon(player.getCursedWeaponEquippedId()).cursedOnLogin();
		}
		
		player.updateEffectIcons();
		
		player.sendPacket(new EtcStatusUpdate(player));
		
		// Expand Skill
		player.sendPacket(new ExStorageMaxCount(player));
		
		sendPacket(new L2FriendList(player));
		
		var sm = SystemMessage.getSystemMessage(SystemMessageId.FRIEND_S1_HAS_LOGGED_IN);
		sm.addCharName(player);
		
		if (player.hasFriends())
		{
			for (var id : player.getFriends())
			{
				var obj = L2World.getInstance().getObject(id);
				if (obj != null)
				{
					obj.sendPacket(sm);
				}
			}
		}
		
		player.sendPacket(SystemMessageId.WELCOME_TO_THE_WORLD_OF_LINEAGE_II);
		
		SevenSigns.getInstance().sendCurrentPeriodMsg(player);
		AnnouncementsTable.getInstance().showAnnouncements(player);
		
		if (showClanNotice)
		{
			var html = new NpcHtmlMessage();
			html.setFile(player, "data/html/clanNotice.htm");
			html.replace("%clan_name%", player.getClan().getName());
			html.replace("%notice_text%", player.getClan().getNotice());
			html.disableValidation();
			sendPacket(html);
		}
		else if (GeneralConfig.SERVER_NEWS)
		{
			var html = new NpcHtmlMessage();
			html.setFile(player, "data/html/servnews.htm");
			html.replace("%player_name%", player.getName());
			sendPacket(html);
		}
		
		if (CharacterConfig.PETITIONING_ALLOWED)
		{
			PetitionManager.getInstance().checkPetitionMessages(player);
		}
		
		if (player.isAlikeDead()) // dead or fake dead
		{
			// no broadcast needed since the player will already spawn dead to others
			sendPacket(new Die(player));
		}
		
		player.onPlayerEnter();
		
		sendPacket(new SkillCoolTime(player));
		sendPacket(new ExVoteSystemInfo(player));
		sendPacket(new ExPCCafePointInfo(player.getPcCafeSystem().getPoints(), 0, 1, PcCafeType.NORMAL, 12));
		sendPacket(new ExReceiveShowPostFriend(player));
		
		for (var item : player.getInventory().getItems())
		{
			if (item.isTimeLimitedItem())
			{
				item.scheduleLifeTimeTask();
			}
			if (item.isShadowItem() && item.isEquipped())
			{
				item.decreaseMana(false);
			}
		}
		
		for (var whItem : player.getWarehouse().getItems())
		{
			if (whItem.isTimeLimitedItem())
			{
				whItem.scheduleLifeTimeTask();
			}
		}
		
		if (DimensionalRiftData.getInstance().checkIfInRiftZone(player.getX(), player.getY(), player.getZ(), false))
		{
			DimensionalRiftData.getInstance().teleportToWaitingRoom(player);
		}
		
		if (player.getClanJoinExpiryTime() > System.currentTimeMillis())
		{
			player.sendPacket(SystemMessageId.CLAN_MEMBERSHIP_TERMINATED);
		}
		
		// remove combat flag before teleporting
		var combatFlag = player.getInventory().getItemByItemId(COMBAT_FLAG);
		if (combatFlag != null)
		{
			var fort = FortManager.getInstance().getFort(player);
			if (fort != null)
			{
				FortSiegeManager.getInstance().dropCombatFlag(player, fort.getResidenceId());
			}
			else
			{
				var slot = player.getInventory().getSlotFromItem(combatFlag);
				player.getInventory().unEquipItemInBodySlot(slot);
				player.destroyItem("CombatFlag", combatFlag, null, true);
			}
		}
		
		// Attacker or spectator logging in to a siege zone.
		// Actually should be checked for inside castle only?
		if (!player.canOverrideCond(PcCondOverride.ZONE_CONDITIONS) && player.isInsideZone(ZoneId.SIEGE) && (!player.isInSiege() || (player.getSiegeState() < 2)))
		{
			player.teleToLocation(TeleportWhereType.TOWN);
		}
		
		if (GeneralConfig.ALLOW_MAIL)
		{
			if (MailManager.getInstance().hasUnreadPost(player))
			{
				sendPacket(ExNoticePostArrived.valueOf(false));
			}
		}
		
		GameEventManager.getInstance().getEvent().onLogin(player);
		
		// Auto Loot Extension.
		Extension.onLogin(player);
		
		if (ServerConfig.AUTO_GAMESERVER_RESTART_ENABLE)
		{
			AutoRestartServer.getInstance().getLoginMessage(player);
		}
		
		if (CustomConfig.WELCOME_MESSAGE_ENABLED)
		{
			player.sendPacket(new ExShowScreenMessage(MessagesData.getInstance().getMessage(player, "welcome_message_text"), CustomConfig.WELCOME_MESSAGE_TIME));
		}
		
		var birthday = player.checkBirthDay();
		if (birthday == 0)
		{
			player.sendPacket(SystemMessageId.YOUR_BIRTHDAY_GIFT_HAS_ARRIVED);
			// activeChar.sendPacket(new ExBirthdayPopup()); Removed in H5?
		}
		else if (birthday != -1)
		{
			var sm1 = SystemMessage.getSystemMessage(SystemMessageId.THERE_ARE_S1_DAYS_UNTIL_YOUR_CHARACTERS_BIRTHDAY);
			sm1.addInt(birthday);
			player.sendPacket(sm1);
		}
		
		if (!player.getPremiumItemList().isEmpty())
		{
			player.sendPacket(ExNotifyPremiumItem.STATIC_PACKET);
		}
		
		if (player.getRace().ordinal() == 2)
		{
			var skill = SkillData.getInstance().getSkill(294, 1); // Shadow Sense level 1
			if ((skill != null) && (player.getSkillLevel(294) == 1))
			{
				if (GameTimeController.getInstance().isNight())
				{
					player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.IT_IS_NOW_MIDNIGHT_AND_THE_EFFECT_OF_S1_CAN_BE_FELT).addSkillName(skill));
					player.updateAndBroadcastStatus(2);
				}
				else
				{
					player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.IT_IS_DOWN_AND_THE_EFFECT_OF_S1_WILL_NOW_DISAPPEAR).addSkillName(skill));
					player.updateAndBroadcastStatus(2);
				}
			}
		}
		
		checkHardwareInfo(player);
		
		// Unstuck players that had client open when server crashed.
		player.sendPacket(ActionFailed.STATIC_PACKET);
	}
	
	private void engage(L2PcInstance player)
	{
		var playerId = player.getObjectId();
		for (var cl : CoupleManager.getInstance().getCouples())
		{
			if ((cl.getPlayer1Id() == playerId) || (cl.getPlayer2Id() == playerId))
			{
				if (cl.getMaried())
				{
					player.setMarried(true);
				}
				
				player.setCoupleId(cl.getId());
				
				if (cl.getPlayer1Id() == playerId)
				{
					player.setPartnerId(cl.getPlayer2Id());
				}
				else
				{
					player.setPartnerId(cl.getPlayer1Id());
				}
			}
		}
	}
	
	private void notifyPartner(L2PcInstance player, int partnerId)
	{
		var partner = L2World.getInstance().getPlayer(player.getPartnerId());
		if (partner != null)
		{
			partner.sendMessage(MessagesData.getInstance().getMessage(partner, "enter_with_partner"));
		}
	}
	
	private void notifyClanMembers(L2PcInstance player)
	{
		var clan = player.getClan();
		if (clan != null)
		{
			clan.getClanMember(player.getObjectId()).setPlayerInstance(player);
			
			var msg = SystemMessage.getSystemMessage(SystemMessageId.CLAN_MEMBER_S1_LOGGED_IN);
			msg.addString(player.getName());
			clan.broadcastToOtherOnlineMembers(msg, player);
			clan.broadcastToOtherOnlineMembers(new PledgeShowMemberListUpdate(player), player);
		}
	}
	
	private void notifySponsorOrApprentice(L2PcInstance player)
	{
		if (player.getSponsor() != 0)
		{
			var sponsor = L2World.getInstance().getPlayer(player.getSponsor());
			if (sponsor != null)
			{
				var msg = SystemMessage.getSystemMessage(SystemMessageId.YOUR_APPRENTICE_S1_HAS_LOGGED_IN);
				msg.addString(player.getName());
				sponsor.sendPacket(msg);
			}
		}
		else if (player.getApprentice() != 0)
		{
			var apprentice = L2World.getInstance().getPlayer(player.getApprentice());
			if (apprentice != null)
			{
				var msg = SystemMessage.getSystemMessage(SystemMessageId.YOUR_SPONSOR_C1_HAS_LOGGED_IN);
				msg.addString(player.getName());
				apprentice.sendPacket(msg);
			}
		}
	}
	
	private void checkHardwareInfo(L2PcInstance player)
	{
		// Delayed HWID checks.
		if (ServerConfig.HARDWARE_INFO_ENABLED)
		{
			ThreadPoolManager.getInstance().scheduleGeneral(() ->
			{
				// Generate trace string.
				var sb = new StringBuilder();
				for (int[] i : _tracert)
				{
					for (int j : i)
					{
						sb.append(j);
						sb.append(".");
					}
				}
				var trace = sb.toString();
				
				// Get hardware info from client.
				var hwInfo = getClient().getHardwareInfo();
				if (hwInfo != null)
				{
					hwInfo.store(player);
					TRACE_HWINFO.put(trace, hwInfo);
				}
				else
				{
					// Get hardware info from stored tracert map.
					hwInfo = TRACE_HWINFO.get(trace);
					if (hwInfo != null)
					{
						hwInfo.store(player);
						getClient().setHardwareInfo(hwInfo);
					}
					// Get hardware info from account variables.
					else
					{
						var storedInfo = player.getAccountVariables().getString(AccountVariables.HWID, "");
						if (!storedInfo.isEmpty())
						{
							hwInfo = new ClientHardwareInfoHolder(storedInfo);
							TRACE_HWINFO.put(trace, hwInfo);
							getClient().setHardwareInfo(hwInfo);
						}
					}
				}
				
				// Banned?
				if ((hwInfo != null) && PunishmentManager.getInstance().hasPunishment(hwInfo.getMacAddress(), PunishmentAffect.HWID, PunishmentType.BAN))
				{
					getClient().closeNow();
					return;
				}
				
				// Check max players.
				if (ServerConfig.KICK_MISSING_HWID && (hwInfo == null))
				{
					getClient().closeNow();
				}
				else if (ServerConfig.MAX_PLAYERS_PER_HWID > 0)
				{
					var count = 0;
					for (var plr : L2World.getInstance().getPlayers())
					{
						if (plr.isOnlineInt() == 1)
						{
							var hwi = plr.getClient().getHardwareInfo();
							if ((hwi != null) && hwi.equals(hwInfo))
							{
								count++;
							}
						}
					}
					if (count >= ServerConfig.MAX_PLAYERS_PER_HWID)
					{
						getClient().closeNow();
					}
				}
			}, 5000);
		}
	}
	
	@Override
	public String getType()
	{
		return _C__11_ENTERWORLD;
	}
	
	@Override
	protected boolean triggersOnActionRequest()
	{
		return false;
	}
}
