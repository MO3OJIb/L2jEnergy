/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.data.xml.impl.SkillData;
import com.l2jserver.gameserver.data.xml.impl.SkillTreesData;
import com.l2jserver.gameserver.enums.ClanPrivilege;
import com.l2jserver.gameserver.enums.skills.AcquireSkillType;
import com.l2jserver.gameserver.model.actor.instance.L2NpcInstance;
import com.l2jserver.gameserver.network.serverpackets.AcquireSkillInfo;

/**
 * Request Acquire Skill Info client packet implementation.
 * @author Zoey76
 */
public final class RequestAcquireSkillInfo extends L2GameClientPacket
{
	private static final String _C__73_REQUESTACQUIRESKILLINFO = "[C] 73 RequestAcquireSkillInfo";
	
	private int _id;
	private int _level;
	private AcquireSkillType _skillType;
	
	@Override
	protected void readImpl()
	{
		_id = readD();
		_level = readD();
		_skillType = AcquireSkillType.getAcquireSkillType(readD());
	}
	
	@Override
	protected void runImpl()
	{
		if ((_id <= 0) || (_level <= 0))
		{
			LOG.warn("{}: Invalid Id:{} or level: {}!", RequestAcquireSkillInfo.class.getSimpleName(), _id, _level);
			return;
		}
		
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		
		var trainer = player.getLastFolkNPC();
		if (!(trainer instanceof L2NpcInstance))
		{
			return;
		}
		
		if (!trainer.canInteract(player) && !player.isGM())
		{
			return;
		}
		
		var skill = SkillData.getInstance().getSkill(_id, _level);
		if (skill == null)
		{
			LOG.warn("{}: Skill Id: {} level: {} is undefined {} failed.", RequestAcquireSkillInfo.class.getSimpleName(), _id, _level, RequestAcquireSkillInfo.class.getName());
			return;
		}
		
		// Hack check. Doesn't apply to all Skill Types
		var prevSkillLevel = player.getSkillLevel(_id);
		if ((prevSkillLevel > 0) && !((_skillType == AcquireSkillType.TRANSFER) || (_skillType == AcquireSkillType.SUBPLEDGE)))
		{
			if (prevSkillLevel == _level)
			{
				LOG.warn("{}: Player {} is trequesting info for a skill that already knows, Id: {} level: {}!", RequestAcquireSkillInfo.class.getSimpleName(), player.getName(), _id, _level);
			}
			else if (prevSkillLevel != (_level - 1))
			{
				LOG.warn("{}: Player {} is requesting info for skill Id: {} level {} without knowing it's previous level!", RequestAcquireSkillInfo.class.getSimpleName(), player.getName(), _id, _level);
			}
		}
		
		var s = SkillTreesData.getInstance().getSkillLearn(_skillType, _id, _level, player);
		if (s == null)
		{
			return;
		}
		
		switch (_skillType)
		{
			case TRANSFORM:
			case FISHING:
			case SUBCLASS:
			case COLLECT:
			case TRANSFER:
			{
				sendPacket(new AcquireSkillInfo(_skillType, s));
				break;
			}
			case CLASS:
			{
				if (trainer.getTemplate().canTeach(player.getLearningClass()))
				{
					var customSp = s.getCalculatedLevelUpSp(player.getClassId(), player.getLearningClass());
					sendPacket(new AcquireSkillInfo(_skillType, s, customSp));
				}
				break;
			}
			case PLEDGE:
			{
				if (!player.isClanLeader())
				{
					return;
				}
				sendPacket(new AcquireSkillInfo(_skillType, s));
				break;
			}
			case SUBPLEDGE:
			{
				if (!player.isClanLeader() || !player.hasClanPrivilege(ClanPrivilege.CL_TROOPS_FAME))
				{
					return;
				}
				sendPacket(new AcquireSkillInfo(_skillType, s));
				break;
			}
		}
	}
	
	@Override
	public String getType()
	{
		return _C__73_REQUESTACQUIRESKILLINFO;
	}
}
