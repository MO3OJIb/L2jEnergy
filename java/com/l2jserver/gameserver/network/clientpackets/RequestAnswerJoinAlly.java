/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.network.SystemMessageId;

public final class RequestAnswerJoinAlly extends L2GameClientPacket
{
	private static final String _C__8D_REQUESTANSWERJOINALLY = "[C] 8D RequestAnswerJoinAlly";
	
	private int _response;
	
	@Override
	protected void readImpl()
	{
		_response = readD();
	}
	
	@Override
	protected void runImpl()
	{
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		
		var requestor = player.getRequest().getPartner();
		if (requestor == null)
		{
			return;
		}
		
		if (_response == 0)
		{
			player.sendPacket(SystemMessageId.YOU_DID_NOT_RESPOND_TO_ALLY_INVITATION);
			requestor.sendPacket(SystemMessageId.NO_RESPONSE_TO_ALLY_INVITATION);
		}
		else
		{
			if (!(requestor.getRequest().getRequestPacket() instanceof RequestJoinAlly))
			{
				return; // hax
			}
			
			var clan = requestor.getClan();
			// we must double check this cause of hack
			if (clan.checkAllyJoinCondition(requestor, player))
			{
				requestor.sendPacket(SystemMessageId.YOU_HAVE_INVITED_SOMEONE_TO_YOUR_ALLIANCE);
				player.sendPacket(SystemMessageId.YOU_HAVE_ACCEPTED_THE_ALLIANCE);
				
				player.getClan().setAllyId(clan.getAllyId());
				player.getClan().setAllyName(clan.getAllyName());
				player.getClan().setAllyPenaltyExpiryTime(0, 0);
				player.getClan().changeAllyCrest(clan.getAllyCrestId(), true);
				DAOFactory.getInstance().getClanDAO().updateClan(player.getClan().getLeaderId(), player.getClan().getAllyId(), player.getClan().getAllyName(), player.getClan().getReputationScore(), player.getClan().getAllyPenaltyExpiryTime(), player.getClan().getAllyPenaltyType(), player.getClan().getCharPenaltyExpiryTime(), player.getClan().getDissolvingExpiryTime(), player.getClan().getNewLeaderId(), player.getClan().getId());
			}
		}
		
		player.getRequest().onRequestResponse();
	}
	
	@Override
	public String getType()
	{
		return _C__8D_REQUESTANSWERJOINALLY;
	}
}
