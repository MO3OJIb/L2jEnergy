/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.configuration.config.GeneralConfig;

public class RequestBR_LectureMark extends L2GameClientPacket
{
	private static final String _C__D0_90_REQUESTBRLECTUREMARK = "[C] D0:90 RequestBR_LectureMark";
	
	private int _mark;
	
	@Override
	protected void readImpl()
	{
		_mark = readC();
	}
	
	@Override
	protected void runImpl()
	{
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		if (!GeneralConfig.EX_LECTURE_MARK)
		{
			player.sendAdminMessage("Lecture Not Support, Check Server");
			return;
		}
		player.setLectureMark(_mark, true);
	}
	
	@Override
	public String getType()
	{
		return _C__D0_90_REQUESTBRLECTUREMARK;
	}
}
