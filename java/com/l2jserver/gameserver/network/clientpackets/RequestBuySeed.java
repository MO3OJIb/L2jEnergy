/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import static com.l2jserver.gameserver.model.itemcontainer.Inventory.MAX_ADENA;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.l2jserver.gameserver.configuration.config.protection.BaseProtectionConfig;
import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.enums.FloodAction;
import com.l2jserver.gameserver.instancemanager.CastleManager;
import com.l2jserver.gameserver.instancemanager.CastleManorManager;
import com.l2jserver.gameserver.model.SeedProduction;
import com.l2jserver.gameserver.model.actor.instance.L2MerchantInstance;
import com.l2jserver.gameserver.model.holders.IntLongHolder;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.FloodProtectors;
import com.l2jserver.gameserver.util.Util;

/**
 * @author l3x
 */
public class RequestBuySeed extends L2GameClientPacket
{
	private static final int BATCH_LENGTH = 12; // length of the one item
	private int _manorId;
	private List<IntLongHolder> _items = null;
	
	@Override
	protected final void readImpl()
	{
		_manorId = readD();
		var count = readD();
		if ((count <= 0) || (count > MAX_ITEM_IN_PACKET) || ((count * BATCH_LENGTH) != _buf.remaining()))
		{
			return;
		}
		
		_items = new ArrayList<>(count);
		for (var i = 0; i < count; i++)
		{
			var itemId = readD();
			var cnt = readQ();
			if ((cnt < 1) || (itemId < 1))
			{
				_items = null;
				return;
			}
			_items.add(new IntLongHolder(itemId, cnt));
		}
	}
	
	@Override
	protected final void runImpl()
	{
		if (!FloodProtectors.performAction(getClient(), FloodAction.MANOR))
		{
			return;
		}
		
		var player = getActiveChar();
		if (player == null)
		{
			return;
		}
		else if (_items == null)
		{
			sendActionFailed();
			return;
		}
		
		var manor = CastleManorManager.getInstance();
		if (manor.isUnderMaintenance())
		{
			sendActionFailed();
			return;
		}
		
		var castle = CastleManager.getInstance().getCastleById(_manorId);
		if (castle == null)
		{
			sendActionFailed();
			return;
		}
		
		var manager = player.getLastFolkNPC();
		if (!(manager instanceof L2MerchantInstance) || !manager.canInteract(player) || (manager.getTemplate().getParameters().getInt("manor_id", -1) != _manorId))
		{
			sendActionFailed();
			return;
		}
		
		var totalPrice = 0;
		var slots = 0;
		var totalWeight = 0;
		
		final Map<Integer, SeedProduction> _productInfo = new HashMap<>();
		for (var ih : _items)
		{
			var sp = manor.getSeedProduct(_manorId, ih.getId(), false);
			if ((sp == null) || (sp.getPrice() <= 0) || (sp.getAmount() < ih.getValue()) || ((MAX_ADENA / ih.getValue()) < sp.getPrice()))
			{
				sendActionFailed();
				return;
			}
			
			// Calculate price
			totalPrice += (sp.getPrice() * ih.getValue());
			if (totalPrice > MAX_ADENA)
			{
				Util.handleIllegalPlayerAction(player, "Warning!! Character " + player.getName() + " of account " + player.getAccountName() + " tried to purchase over " + MAX_ADENA + " adena worth of goods.", BaseProtectionConfig.DEFAULT_PUNISH);
				sendActionFailed();
				return;
			}
			
			// Calculate weight
			var template = ItemTable.getInstance().getTemplate(ih.getId());
			totalWeight += ih.getValue() * template.getWeight();
			
			// Calculate slots
			if (!template.isStackable())
			{
				slots += ih.getValue();
			}
			else if (player.getInventory().getItemByItemId(ih.getId()) == null)
			{
				slots++;
			}
			_productInfo.put(ih.getId(), sp);
		}
		
		if (!player.getInventory().validateWeight(totalWeight))
		{
			player.sendPacket(SystemMessageId.WEIGHT_LIMIT_EXCEEDED);
			return;
		}
		else if (!player.getInventory().validateCapacity(slots))
		{
			player.sendPacket(SystemMessageId.YOUR_INVENTORY_IS_FULL);
			return;
		}
		else if ((totalPrice < 0) || (player.getAdena() < totalPrice))
		{
			player.sendPacket(SystemMessageId.YOU_NOT_ENOUGH_ADENA);
			return;
		}
		
		// Proceed the purchase
		for (var i : _items)
		{
			var sp = _productInfo.get(i.getId());
			var price = sp.getPrice() * i.getValue();
			
			// Take Adena and decrease seed amount
			if (!sp.decreaseAmount(i.getValue()) || !player.reduceAdena("Buy", price, player, false))
			{
				// failed buy, reduce total price
				totalPrice -= price;
				continue;
			}
			
			// Add item to player's inventory
			player.addItem("Buy", i.getId(), i.getValue(), manager, true);
		}
		
		// Adding to treasury for Manor Castle
		if (totalPrice > 0)
		{
			castle.addToTreasuryNoTax(totalPrice);
			player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.S1_DISAPPEARED_ADENA).addLong(totalPrice));
		}
	}
	
	@Override
	public String getType()
	{
		return "[C] C5 RequestBuySeed";
	}
}