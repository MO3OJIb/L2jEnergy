/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.ai.CtrlIntention;
import com.l2jserver.gameserver.configuration.config.AdminConfig;
import com.l2jserver.gameserver.configuration.config.ManorConfig;
import com.l2jserver.gameserver.data.xml.impl.AdminData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.FloodAction;
import com.l2jserver.gameserver.enums.InstanceType;
import com.l2jserver.gameserver.enums.PlayerAction;
import com.l2jserver.gameserver.handler.AdminCommandHandler;
import com.l2jserver.gameserver.handler.BypassHandler;
import com.l2jserver.gameserver.handler.CommunityBoardHandler;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.entity.Hero;
import com.l2jserver.gameserver.model.events.EventDispatcher;
import com.l2jserver.gameserver.model.events.impl.character.npc.OnNpcManorBypass;
import com.l2jserver.gameserver.model.events.impl.character.player.OnPlayerBypass;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ActionFailed;
import com.l2jserver.gameserver.network.serverpackets.ConfirmDlg;
import com.l2jserver.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jserver.gameserver.util.FloodProtectors;
import com.l2jserver.gameserver.util.LoggingUtils;
import com.l2jserver.gameserver.util.Util;

/**
 * RequestBypassToServer client packet implementation.
 * @author HorridoJoho
 */
public final class RequestBypassToServer extends L2GameClientPacket
{
	private static final String _C__23_REQUESTBYPASSTOSERVER = "[C] 23 RequestBypassToServer";
	
	private static final Logger LOG_GM_AUDIT = LoggerFactory.getLogger("gmaudit");
	
	// FIXME: This is for compatibility, will be changed when bypass functionality got an overhaul by NosBit
	private static final String[] _possibleNonHtmlCommands =
	{
		"_bbs",
		"bbs",
		"_mail",
		"_friend",
		"_match",
		"_diary",
		"_olympiad?command",
		"manor_menu_select"
	};
	
	// S
	private String _command;
	
	@Override
	protected void readImpl()
	{
		_command = readS();
	}
	
	@Override
	protected void runImpl()
	{
		if (!FloodProtectors.performAction(getClient(), FloodAction.SERVER_BYPASS))
		{
			return;
		}
		
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		
		if (_command.isEmpty())
		{
			LOG.warn("Player {} sent empty bypass!", player.getName());
			player.logout();
			return;
		}
		
		var requiresBypassValidation = true;
		for (var possibleNonHtmlCommand : _possibleNonHtmlCommands)
		{
			if (_command.startsWith(possibleNonHtmlCommand))
			{
				requiresBypassValidation = false;
				break;
			}
		}
		
		var bypassOriginId = 0;
		if (requiresBypassValidation)
		{
			bypassOriginId = player.validateHtmlAction(_command);
			if (bypassOriginId == -1)
			{
				LOG.warn("Player {} sent non cached bypass: '{}'", player.getName(), _command);
				return;
			}
			
			if ((bypassOriginId > 0) && !Util.isInsideRangeOfObjectId(player, bypassOriginId, L2Npc.INTERACTION_DISTANCE))
			{
				// No logging here, this could be a common case where the player has the html still open and run too far away and then clicks a html action
				return;
			}
		}
		
		try
		{
			if (_command.startsWith("admin_"))
			{
				var command = _command.split(" ")[0];
				
				var ach = AdminCommandHandler.getInstance().getHandler(command);
				
				if (ach == null)
				{
					if (player.isGM())
					{
						player.sendMessage(MessagesData.getInstance().getMessage(player, "gm_command_not_exist").replace("%s%", command.substring(6) + ""));
					}
					LOG.warn("{} requested not registered admin command '{}'", player, command);
					return;
				}
				
				if (!AdminData.getInstance().hasAccess(command, player.getAccessLevel()))
				{
					player.sendMessage(MessagesData.getInstance().getMessage(player, "gm_command_not_access"));
					LOG.warn("Character {} tried to use admin command {}, without proper access level!", player.getName(), command);
					return;
				}
				
				if (AdminData.getInstance().requireConfirm(command))
				{
					player.setAdminConfirmCmd(_command);
					var dlg = new ConfirmDlg(SystemMessageId.S1);
					dlg.addString("Are you sure you want execute command " + _command.substring(6) + " ?");
					player.addAction(PlayerAction.ADMIN_COMMAND);
					player.sendPacket(dlg);
				}
				else
				{
					if (AdminConfig.GMAUDIT)
					{
						LoggingUtils.logGMAudit(LOG_GM_AUDIT, player.getName() + " [" + player.getObjectId() + "]", _command, (player.getTarget() != null ? player.getTarget().getName() : "no-target"));
					}
					
					ach.useAdminCommand(_command, player);
				}
			}
			else if (CommunityBoardHandler.getInstance().isCommunityBoardCommand(_command))
			{
				CommunityBoardHandler.getInstance().handleParseCommand(_command, player);
			}
			else if (_command.equals("come_here") && player.isGM())
			{
				comeHere(player);
			}
			else if (_command.startsWith("npc_"))
			{
				var endOfId = _command.indexOf('_', 5);
				String id;
				if (endOfId > 0)
				{
					id = _command.substring(4, endOfId);
				}
				else
				{
					id = _command.substring(4);
				}
				if (StringUtil.isDigit(id))
				{
					var object = L2World.getInstance().getObject(Integer.parseInt(id));
					
					if ((object != null) && object.isNpc() && (endOfId > 0) && player.isInsideRadius(object, L2Npc.INTERACTION_DISTANCE, false, false))
					{
						((L2Npc) object).onBypassFeedback(player, _command.substring(endOfId + 1));
					}
				}
				
				player.sendPacket(ActionFailed.STATIC_PACKET);
			}
			else if (_command.startsWith("item_"))
			{
				var endOfId = _command.indexOf('_', 5);
				String id;
				if (endOfId > 0)
				{
					id = _command.substring(5, endOfId);
				}
				else
				{
					id = _command.substring(5);
				}
				try
				{
					var item = player.getInventory().getItemByObjectId(Integer.parseInt(id));
					if ((item != null) && (endOfId > 0))
					{
						item.onBypassFeedback(player, _command.substring(endOfId + 1));
					}
					
					player.sendPacket(ActionFailed.STATIC_PACKET);
				}
				catch (NumberFormatException nfe)
				{
					LOG.warn("NFE for command [{}]", _command, nfe);
				}
			}
			else if (_command.startsWith("_match"))
			{
				var params = _command.substring(_command.indexOf("?") + 1);
				var st = new StringTokenizer(params, "&");
				var heroclass = Integer.parseInt(st.nextToken().split("=")[1]);
				var heropage = Integer.parseInt(st.nextToken().split("=")[1]);
				var heroid = Hero.getInstance().getHeroByClass(heroclass);
				if (heroid > 0)
				{
					Hero.getInstance().showHeroFights(player, heroclass, heroid, heropage);
				}
			}
			else if (_command.startsWith("_diary"))
			{
				var params = _command.substring(_command.indexOf("?") + 1);
				var st = new StringTokenizer(params, "&");
				var heroclass = Integer.parseInt(st.nextToken().split("=")[1]);
				var heropage = Integer.parseInt(st.nextToken().split("=")[1]);
				var heroid = Hero.getInstance().getHeroByClass(heroclass);
				if (heroid > 0)
				{
					Hero.getInstance().showHeroDiary(player, heroclass, heroid, heropage);
				}
			}
			else if (_command.startsWith("_olympiad?command"))
			{
				var arenaId = Integer.parseInt(_command.split("=")[2]);
				var handler = BypassHandler.getInstance().getHandler("arenachange");
				if (handler != null)
				{
					handler.useBypass("arenachange " + (arenaId - 1), player, null);
				}
			}
			else if (_command.startsWith("manor_menu_select"))
			{
				var lastNpc = player.getLastFolkNPC();
				if (ManorConfig.ALLOW_MANOR && (lastNpc != null) && lastNpc.canInteract(player))
				{
					var split = _command.substring(_command.indexOf("?") + 1).split("&");
					var ask = Integer.parseInt(split[0].split("=")[1]);
					var state = Integer.parseInt(split[1].split("=")[1]);
					var time = split[2].split("=")[1].equals("1");
					EventDispatcher.getInstance().notifyEventAsync(new OnNpcManorBypass(player, lastNpc, ask, state, time), lastNpc);
				}
			}
			else
			{
				var handler = BypassHandler.getInstance().getHandler(_command);
				if (handler != null)
				{
					if (bypassOriginId > 0)
					{
						var bypassOrigin = player.getKnownList().getKnownObjects().get(bypassOriginId);
						if ((bypassOrigin != null) && bypassOrigin.isInstanceTypes(InstanceType.L2Character))
						{
							handler.useBypass(_command, player, (L2Character) bypassOrigin);
						}
						else
						{
							handler.useBypass(_command, player, null);
						}
					}
					else
					{
						handler.useBypass(_command, player, null);
					}
				}
				else
				{
					LOG.warn("{} sent not handled RequestBypassToServer: [{}]", getClient(), _command);
				}
			}
		}
		catch (Exception ex)
		{
			LOG.warn("Exception processing bypass from player {}: {}", player.getName(), _command, ex);
			
			if (player.isGM())
			{
				var sb = new StringBuilder(200);
				sb.append("<html><body>");
				sb.append("Bypass error: " + ex + "<br1>");
				sb.append("Bypass command: " + _command + "<br1>");
				sb.append("StackTrace:<br1>");
				for (var ste : ex.getStackTrace())
				{
					sb.append(ste.toString() + "<br1>");
				}
				sb.append("</body></html>");
				// item html
				var html = new NpcHtmlMessage(0, 1, sb.toString());
				html.disableValidation();
				player.sendPacket(html);
			}
		}
		
		EventDispatcher.getInstance().notifyEventAsync(new OnPlayerBypass(player, _command), player);
	}
	
	/**
	 * @param player
	 */
	private static void comeHere(L2PcInstance player)
	{
		var obj = player.getTarget();
		if (obj == null)
		{
			return;
		}
		if (obj instanceof L2Npc npc)
		{
			npc.setTarget(player);
			npc.getAI().setIntention(CtrlIntention.AI_INTENTION_MOVE_TO, player.getLocation());
		}
	}
	
	@Override
	public String getType()
	{
		return _C__23_REQUESTBYPASSTOSERVER;
	}
}
