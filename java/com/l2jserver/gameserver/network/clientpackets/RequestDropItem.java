/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.configuration.config.CharacterConfig;
import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.configuration.config.protection.BaseProtectionConfig;
import com.l2jserver.gameserver.data.xml.impl.AdminData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.FloodAction;
import com.l2jserver.gameserver.enums.PrivateStoreType;
import com.l2jserver.gameserver.enums.ZoneId;
import com.l2jserver.gameserver.enums.items.EtcItemType;
import com.l2jserver.gameserver.enums.items.ItemType2;
import com.l2jserver.gameserver.model.PcCondOverride;
import com.l2jserver.gameserver.model.itemcontainer.Inventory;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.InventoryUpdate;
import com.l2jserver.gameserver.network.serverpackets.ItemList;
import com.l2jserver.gameserver.util.FloodProtectors;
import com.l2jserver.gameserver.util.LoggingUtils;
import com.l2jserver.gameserver.util.Util;

public final class RequestDropItem extends L2GameClientPacket
{
	private static final String _C__17_REQUESTDROPITEM = "[C] 17 RequestDropItem";
	
	private static final Logger LOG_GM_AUDIT = LoggerFactory.getLogger("gmaudit");
	
	private int _objectId;
	private long _count;
	private int _x;
	private int _y;
	private int _z;
	
	@Override
	protected void readImpl()
	{
		_objectId = readD();
		_count = readQ();
		_x = readD();
		_y = readD();
		_z = readD();
	}
	
	@Override
	protected void runImpl()
	{
		var player = getClient().getActiveChar();
		if ((player == null) || player.isDead())
		{
			return;
		}
		// Flood protect drop to avoid packet lag
		if (!FloodProtectors.performAction(getClient(), FloodAction.DROP_ITEM))
		{
			return;
		}
		
		var item = player.getInventory().getItemByObjectId(_objectId);
		
		if ((item == null) || (_count == 0) || !player.validateItemManipulation(_objectId, "drop") || (!GeneralConfig.ALLOW_DISCARDITEM && !player.canOverrideCond(PcCondOverride.DROP_ALL_ITEMS)) || (!item.isDroppable() && !player.canOverrideCond(PcCondOverride.DROP_ALL_ITEMS))
			|| ((item.getItemType() == EtcItemType.PET_COLLAR) && player.havePetInvItems()) || player.isInsideZone(ZoneId.NO_ITEM_DROP))
		{
			player.sendPacket(SystemMessageId.THIS_ITEM_CANNOT_BE_DISCARDED);
			return;
		}
		if (item.isQuestItem() && !player.canOverrideCond(PcCondOverride.DROP_ALL_ITEMS))
		{
			return;
		}
		
		if (_count > item.getCount())
		{
			player.sendPacket(SystemMessageId.THIS_ITEM_CANNOT_BE_DISCARDED);
			return;
		}
		
		if ((CharacterConfig.PLAYER_SPAWN_PROTECTION > 0) && player.isInvul() && !player.isGM())
		{
			player.sendPacket(SystemMessageId.THIS_ITEM_CANNOT_BE_DISCARDED);
			return;
		}
		
		if (_count < 0)
		{
			Util.handleIllegalPlayerAction(player, "[RequestDropItem] Character " + player.getName() + " of account " + player.getAccountName() + " tried to drop item with oid " + _objectId + " but has count < 0!", BaseProtectionConfig.DEFAULT_PUNISH);
			return;
		}
		
		if (!item.isStackable() && (_count > 1))
		{
			Util.handleIllegalPlayerAction(player, "[RequestDropItem] Character " + player.getName() + " of account " + player.getAccountName() + " tried to drop non-stackable item with oid " + _objectId + " but has count > 1!", BaseProtectionConfig.DEFAULT_PUNISH);
			return;
		}
		
		if (BaseProtectionConfig.JAIL_DISABLE_TRANSACTION && player.isJailed())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "no_drop_in_jail"));
			return;
		}
		
		if (!player.getAccessLevel().allowTransaction())
		{
			player.sendPacket(SystemMessageId.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT);
			return;
		}
		
		if (player.isProcessingTransaction() || (player.getPrivateStoreType() != PrivateStoreType.NONE))
		{
			player.sendPacket(SystemMessageId.CANNOT_TRADE_DISCARD_DROP_ITEM_WHILE_IN_SHOPMODE);
			return;
		}
		if (player.isFishing())
		{
			// You can't mount, dismount, break and drop items while fishing
			player.sendPacket(SystemMessageId.CANNOT_DO_WHILE_FISHING_2);
			return;
		}
		if (player.isFlying())
		{
			return;
		}
		
		// Cannot discard item that the skill is consuming
		if (player.isCastingNow())
		{
			if ((player.getCurrentSkill() != null) && (player.getCurrentSkill().getSkill().getItemConsumeId() == item.getId()))
			{
				player.sendPacket(SystemMessageId.THIS_ITEM_CANNOT_BE_DISCARDED);
				return;
			}
		}
		
		// Cannot discard item that the skill is consuming
		if (player.isCastingSimultaneouslyNow())
		{
			if ((player.getLastSimultaneousSkillCast() != null) && (player.getLastSimultaneousSkillCast().getItemConsumeId() == item.getId()))
			{
				player.sendPacket(SystemMessageId.THIS_ITEM_CANNOT_BE_DISCARDED);
				return;
			}
		}
		
		if ((ItemType2.QUEST == item.getItem().getType2()) && !player.canOverrideCond(PcCondOverride.DROP_ALL_ITEMS))
		{
			player.sendPacket(SystemMessageId.CANNOT_DISCARD_EXCHANGE_ITEM);
			return;
		}
		
		if (!player.isInsideRadius(_x, _y, 0, 150, false, false) || (Math.abs(_z - player.getZ()) > 50))
		{
			player.sendPacket(SystemMessageId.CANNOT_DISCARD_DISTANCE_TOO_FAR);
			return;
		}
		
		if (!player.getInventory().canManipulateWithItemId(item.getId()))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "item_cannot_use"));
			return;
		}
		
		if (item.isEquipped())
		{
			var unequiped = player.getInventory().unEquipItemInSlotAndRecord(item.getLocationSlot());
			var iu = new InventoryUpdate();
			for (var itm : unequiped)
			{
				itm.unChargeAllShots();
				iu.addModifiedItem(itm);
			}
			player.sendPacket(iu);
			player.broadcastUserInfo();
			
			var il = new ItemList(player, true);
			player.sendPacket(il);
		}
		
		var dropedItem = player.dropItem("Drop", _objectId, _count, _x, _y, _z, null, false, false);
		// activeChar.broadcastUserInfo();
		
		if (player.isGM())
		{
			var target = (player.getTarget() != null ? player.getTarget().getName() : "no-target");
			LoggingUtils.logGMAudit(LOG_GM_AUDIT, player.getName() + " [" + player.getObjectId() + "]", "Drop", target, "(id: " + dropedItem.getId() + " name: " + dropedItem.getItemName() + " objId: " + dropedItem.getObjectId() + " x: " + player.getX() + " y: " + player.getY() + " z: "
				+ player.getZ() + ")");
		}
		
		if ((dropedItem != null) && (dropedItem.getId() == Inventory.ADENA_ID) && (dropedItem.getCount() >= 1000000))
		{
			var msg = "Character (" + player.getName() + ") has dropped (" + dropedItem.getCount() + ")adena at (" + _x + "," + _y + "," + _z + ")";
			LOG.warn(msg);
			AdminData.getInstance().broadcastMessageToGMs(msg);
		}
	}
	
	@Override
	public String getType()
	{
		return _C__17_REQUESTDROPITEM;
	}
	
	@Override
	protected boolean triggersOnActionRequest()
	{
		return false;
	}
}
