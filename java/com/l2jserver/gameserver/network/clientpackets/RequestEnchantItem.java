/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.configuration.config.AdminConfig;
import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.configuration.config.protection.BaseProtectionConfig;
import com.l2jserver.gameserver.data.xml.impl.EnchantItemData;
import com.l2jserver.gameserver.enums.FloodAction;
import com.l2jserver.gameserver.enums.skills.CommonSkill;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.items.enchant.EnchantSupportItem;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.EnchantResult;
import com.l2jserver.gameserver.network.serverpackets.InventoryUpdate;
import com.l2jserver.gameserver.network.serverpackets.ItemList;
import com.l2jserver.gameserver.network.serverpackets.MagicSkillUse;
import com.l2jserver.gameserver.network.serverpackets.StatusUpdate;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;
import com.l2jserver.gameserver.util.FloodProtectors;
import com.l2jserver.gameserver.util.LoggingUtils;
import com.l2jserver.gameserver.util.Util;

public final class RequestEnchantItem extends L2GameClientPacket
{
	protected static final Logger LOG_ENCHANT = LoggerFactory.getLogger("item");
	
	private static final String _C__5F_REQUESTENCHANTITEM = "[C] 5F RequestEnchantItem";
	
	private int _objectId;
	private int _supportId;
	
	@Override
	protected void readImpl()
	{
		_objectId = readD();
		_supportId = readD();
	}
	
	@Override
	protected void runImpl()
	{
		if (!FloodProtectors.performAction(getClient(), FloodAction.ENCHANT_ITEM))
		{
			return;
		}
		
		var player = getClient().getActiveChar();
		if ((player == null) || (_objectId == 0))
		{
			return;
		}
		
		if (!player.isOnline() || getClient().isDetached())
		{
			player.setActiveEnchantItemId(L2PcInstance.ID_NONE);
			return;
		}
		
		if (player.isProcessingTransaction() || player.isInStoreMode())
		{
			player.sendPacket(SystemMessageId.CANNOT_ENCHANT_WHILE_STORE);
			player.setActiveEnchantItemId(L2PcInstance.ID_NONE);
			return;
		}
		
		var item = player.getInventory().getItemByObjectId(_objectId);
		var scroll = player.getInventory().getItemByObjectId(player.getActiveEnchantItemId());
		var support = player.getInventory().getItemByObjectId(player.getActiveEnchantSupportItemId());
		
		if ((item == null) || (scroll == null))
		{
			player.setActiveEnchantItemId(L2PcInstance.ID_NONE);
			return;
		}
		
		// template for scroll
		var scrollTemplate = EnchantItemData.getInstance().getEnchantScroll(scroll);
		
		// scroll not found in list
		if (scrollTemplate == null)
		{
			return;
		}
		
		// template for support item, if exist
		EnchantSupportItem supportTemplate = null;
		if (support != null)
		{
			if (support.getObjectId() != _supportId)
			{
				player.setActiveEnchantItemId(L2PcInstance.ID_NONE);
				return;
			}
			supportTemplate = EnchantItemData.getInstance().getSupportItem(support);
		}
		
		// first validation check
		if (!scrollTemplate.isValid(item, supportTemplate))
		{
			player.sendPacket(SystemMessageId.INAPPROPRIATE_ENCHANT_CONDITION);
			player.setActiveEnchantItemId(L2PcInstance.ID_NONE);
			player.sendPacket(new EnchantResult(2, 0, 0));
			return;
		}
		
		// fast auto-enchant cheat check
		if ((player.getActiveEnchantTimestamp() == 0) || ((System.currentTimeMillis() - player.getActiveEnchantTimestamp()) < 2000))
		{
			Util.handleIllegalPlayerAction(player, "Player " + player.getName() + " use autoenchant program ", BaseProtectionConfig.DEFAULT_PUNISH);
			player.setActiveEnchantItemId(L2PcInstance.ID_NONE);
			player.sendPacket(new EnchantResult(2, 0, 0));
			return;
		}
		
		// attempting to destroy scroll
		scroll = player.getInventory().destroyItem("Enchant", scroll.getObjectId(), 1, player, item);
		if (scroll == null)
		{
			player.sendPacket(SystemMessageId.NOT_ENOUGH_ITEMS);
			Util.handleIllegalPlayerAction(player, "Player " + player.getName() + " tried to enchant with a scroll he doesn't have", BaseProtectionConfig.DEFAULT_PUNISH);
			player.setActiveEnchantItemId(L2PcInstance.ID_NONE);
			player.sendPacket(new EnchantResult(2, 0, 0));
			return;
		}
		
		// attempting to destroy support if exist
		if (support != null)
		{
			support = player.getInventory().destroyItem("Enchant", support.getObjectId(), 1, player, item);
			if (support == null)
			{
				player.sendPacket(SystemMessageId.NOT_ENOUGH_ITEMS);
				Util.handleIllegalPlayerAction(player, "Player " + player.getName() + " tried to enchant with a support item he doesn't have", BaseProtectionConfig.DEFAULT_PUNISH);
				player.setActiveEnchantItemId(L2PcInstance.ID_NONE);
				player.sendPacket(new EnchantResult(2, 0, 0));
				return;
			}
		}
		
		var iu = new InventoryUpdate();
		synchronized (item)
		{
			// last validation check
			if ((item.getOwnerId() != player.getObjectId()) || (item.isEnchantable() == 0))
			{
				player.sendPacket(SystemMessageId.INAPPROPRIATE_ENCHANT_CONDITION);
				player.setActiveEnchantItemId(L2PcInstance.ID_NONE);
				player.sendPacket(new EnchantResult(2, 0, 0));
				return;
			}
			
			var resultType = scrollTemplate.calculateSuccess(player, item, supportTemplate);
			switch (resultType)
			{
				case ERROR:
				{
					player.sendPacket(SystemMessageId.INAPPROPRIATE_ENCHANT_CONDITION);
					player.setActiveEnchantItemId(L2PcInstance.ID_NONE);
					player.sendPacket(new EnchantResult(2, 0, 0));
					break;
				}
				case SUCCESS:
				{
					Skill enchant4Skill = null;
					var it = item.getItem();
					// Increase enchant level only if scroll's base template has chance, some armors can success over +20 but they shouldn't have increased.
					if (scrollTemplate.getChance(player, item) > 0)
					{
						item.setEnchantLevel(item.getEnchantLevel() + 1);
						item.updateDatabase();
					}
					player.sendPacket(new EnchantResult(0, 0, 0));
					
					if (AdminConfig.LOG_ITEM_ENCHANTS)
					{
						LoggingUtils.logEnchantItem(LOG_ENCHANT, player, "Success", item, scroll, support);
					}
					
					// announce the success
					var minEnchantAnnounce = item.isArmor() ? 6 : 7;
					var maxEnchantAnnounce = item.isArmor() ? 0 : 15;
					if ((item.getEnchantLevel() == minEnchantAnnounce) || (item.getEnchantLevel() == maxEnchantAnnounce))
					{
						var sm = SystemMessage.getSystemMessage(SystemMessageId.C1_SUCCESSFULY_ENCHANTED_A_S2_S3);
						sm.addCharName(player);
						sm.addInt(item.getEnchantLevel());
						sm.addItemName(item);
						player.broadcastPacket(sm);
						
						var skill = CommonSkill.FIREWORK.getSkill();
						if (skill != null)
						{
							player.broadcastPacket(new MagicSkillUse(player, player, skill.getId(), skill.getLevel(), skill.getHitTime(), skill.getReuseDelay()));
						}
					}
					
					if ((item.isArmor()) && (item.getEnchantLevel() == 4) && item.isEquipped())
					{
						enchant4Skill = it.getEnchant4Skill();
						if (enchant4Skill != null)
						{
							// add skills bestowed from +4 armor
							player.addSkill(enchant4Skill, false);
							player.sendSkillList();
						}
					}
					break;
				}
				case FAILURE:
				{
					if (scrollTemplate.isSafe())
					{
						// safe enchant - remain old value
						player.sendPacket(SystemMessageId.SAFE_ENCHANT_FAILED);
						player.sendPacket(new EnchantResult(5, 0, 0));
						
						if (AdminConfig.LOG_ITEM_ENCHANTS)
						{
							LoggingUtils.logEnchantItem(LOG_ENCHANT, player, "Safe Fail", item, scroll, support);
						}
					}
					else
					{
						// unequip item on enchant failure to avoid item skills stack
						if (item.isEquipped())
						{
							if (item.getEnchantLevel() > 0)
							{
								var sm = SystemMessage.getSystemMessage(SystemMessageId.EQUIPMENT_S1_S2_REMOVED);
								sm.addInt(item.getEnchantLevel());
								sm.addItemName(item);
								player.sendPacket(sm);
							}
							else
							{
								var sm = SystemMessage.getSystemMessage(SystemMessageId.S1_DISARMED);
								sm.addItemName(item);
								player.sendPacket(sm);
							}
							
							var unequiped = player.getInventory().unEquipItemInSlotAndRecord(item.getLocationSlot());
							for (var itm : unequiped)
							{
								iu.addModifiedItem(itm);
							}
							
							player.sendPacket(iu);
							player.broadcastUserInfo();
						}
						
						if (scrollTemplate.isBlessed())
						{
							// blessed enchant - clear enchant value
							player.sendPacket(SystemMessageId.BLESSED_ENCHANT_FAILED);
							
							item.setEnchantLevel(0);
							item.updateDatabase();
							player.sendPacket(new EnchantResult(3, 0, 0));
							
							if (AdminConfig.LOG_ITEM_ENCHANTS)
							{
								LoggingUtils.logEnchantItem(LOG_ENCHANT, player, "Blessed Fail", item, scroll, support);
							}
						}
						else
						{
							// enchant failed, destroy item
							item = player.getInventory().destroyItem("Enchant", item, player, null);
							if (item == null)
							{
								// unable to destroy item, cheater ?
								Util.handleIllegalPlayerAction(player, "Unable to delete item on enchant failure from player " + player.getName() + ", possible cheater !", BaseProtectionConfig.DEFAULT_PUNISH);
								player.setActiveEnchantItemId(L2PcInstance.ID_NONE);
								player.sendPacket(new EnchantResult(2, 0, 0));
								
								if (AdminConfig.LOG_ITEM_ENCHANTS)
								{
									LoggingUtils.logEnchantItem(LOG_ENCHANT, player, "Unable to destroy", item, scroll, support);
								}
								return;
							}
							
							L2World.getInstance().removeObject(item);
							
							var crystalId = item.getItem().getCrystalItemId();
							if ((crystalId != 0) && item.getItem().isCrystallizable())
							{
								var count = item.getCrystalCount() - ((item.getItem().getCrystalCount() + 1) / 2);
								count = count < 1 ? 1 : count;
								player.getInventory().addItem("Enchant", crystalId, count, player, item);
								
								var sm = SystemMessage.getSystemMessage(SystemMessageId.YOU_HAVE_EARNED_S2_S1S);
								sm.addItemName(crystalId);
								sm.addLong(count);
								player.sendPacket(sm);
								player.sendPacket(new EnchantResult(1, crystalId, count));
							}
							else
							{
								player.sendPacket(new EnchantResult(4, 0, 0));
							}
							
							if (AdminConfig.LOG_ITEM_ENCHANTS)
							{
								LoggingUtils.logEnchantItem(LOG_ENCHANT, player, "Fail", item, scroll, support);
							}
						}
					}
					break;
				}
			}
			
			var su = new StatusUpdate(player);
			su.addAttribute(StatusUpdate.CUR_LOAD, player.getCurrentLoad());
			player.sendPacket(su);
			if (!GeneralConfig.FORCE_INVENTORY_UPDATE)
			{
				if (scroll.getCount() == 0)
				{
					iu.addRemovedItem(scroll);
				}
				else
				{
					iu.addModifiedItem(scroll);
				}
				
				if (item.getCount() == 0)
				{
					iu.addRemovedItem(item);
				}
				else
				{
					iu.addModifiedItem(item);
				}
				
				if (support != null)
				{
					if (support.getCount() == 0)
					{
						iu.addRemovedItem(support);
					}
					else
					{
						iu.addModifiedItem(support);
					}
				}
				
				player.sendPacket(iu);
			}
			else
			{
				player.sendPacket(new ItemList(player, true));
			}
			
			player.broadcastUserInfo();
			player.setActiveEnchantItemId(L2PcInstance.ID_NONE);
		}
	}
	
	@Override
	public String getType()
	{
		return _C__5F_REQUESTENCHANTITEM;
	}
}
