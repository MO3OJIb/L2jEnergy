/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.configuration.config.LimitsConfig;
import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

public final class RequestFriendAddReply extends L2GameClientPacket
{
	private static final String _C__78_REQUESTFRIENDADDREPLY = "[C] 78 RequestFriendAddReply";
	
	private int _response;
	
	@Override
	protected void readImpl()
	{
		_response = _buf.hasRemaining() ? readD() : 0;
	}
	
	@Override
	protected void runImpl()
	{
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		
		var requestor = player.getActiveRequester();
		if (requestor == null)
		{
			return;
		}
		
		if (player.isFriend(requestor.getObjectId()) || requestor.isFriend(player.getObjectId()))
		{
			requestor.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.S1_ALREADY_IN_FRIENDS_LIST).addCharName(player));
			return;
		}
		
		if (player.getFriends().size() >= LimitsConfig.FRIEND_LIST_LIMIT)
		{
			player.sendPacket(SystemMessageId.YOU_CAN_ONLY_ENTER_UP_128_NAMES_IN_YOUR_FRIENDS_LIST);
			return;
		}
		
		if (requestor.getFriends().size() >= LimitsConfig.FRIEND_LIST_LIMIT)
		{
			requestor.sendPacket(SystemMessageId.THE_FRIENDS_LIST_OF_THE_PERSON_YOU_ARE_TRYING_TO_ADD_IS_FULL_SO_REGISTRATION_IS_NOT_POSSIBLE);
			return;
		}
		
		if (_response == 1)
		{
			DAOFactory.getInstance().getFriendDAO().addFriends(requestor, player);
		}
		else
		{
			requestor.sendPacket(SystemMessageId.FAILED_TO_INVITE_A_FRIEND);
		}
		
		player.setActiveRequester(null);
		requestor.onTransactionResponse();
	}
	
	@Override
	public String getType()
	{
		return _C__78_REQUESTFRIENDADDREPLY;
	}
}
