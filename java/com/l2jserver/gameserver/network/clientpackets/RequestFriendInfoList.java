/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.data.sql.impl.CharNameTable;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

public final class RequestFriendInfoList extends L2GameClientPacket
{
	private static final String _C__6A_REQUESTFRIENDINFOLIST = "[C] 6A RequestFriendInfoList";
	
	@Override
	protected void readImpl()
	{
		// trigger
	}
	
	@Override
	protected void runImpl()
	{
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		
		player.sendPacket(SystemMessageId.FRIEND_LIST_HEADER);
		if (player.hasFriends())
		{
			for (var id : player.getFriends())
			{
				var friendName = CharNameTable.getInstance().getNameById(id);
				if (friendName == null)
				{
					continue;
				}
				
				var friend = L2World.getInstance().getPlayer(friendName);
				var sm = SystemMessage.getSystemMessage((friend == null) || !friend.isOnline() ? SystemMessageId.S1_OFFLINE : SystemMessageId.S1_ONLINE);
				sm.addString(friendName);
				player.sendPacket(sm);
			}
		}
		player.sendPacket(SystemMessageId.FRIEND_LIST_FOOTER);
	}
	
	@Override
	public String getType()
	{
		return _C__6A_REQUESTFRIENDINFOLIST;
	}
}
