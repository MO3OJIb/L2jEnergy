/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.gameserver.enums.ClanPrivilege;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

public class RequestGiveNickName extends L2GameClientPacket
{
	private static final String _C__0B_REQUESTGIVENICKNAME = "[C] 0B RequestGiveNickName";
	
	private String _name;
	private String _title;
	
	@Override
	protected void readImpl()
	{
		_name = readS();
		_title = readS();
	}
	
	@Override
	protected void runImpl()
	{
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		
		if (!StringUtil.isValidString(_title, "^[a-zA-Z0-9 !@#$&()\\-`.+,/\"]*{0,16}$"))
		{
			player.sendPacket(SystemMessageId.NOT_WORKING_PLEASE_TRY_AGAIN_LATER);
			return;
		}
		
		// Noblesse can bestow a title to themselves
		if (player.isNoble() && _name.equalsIgnoreCase(player.getName()))
		{
			player.setTitle(_title);
			player.sendPacket(SystemMessageId.TITLE_CHANGED);
			player.broadcastTitleInfo();
		}
		else
		{
			// Can the player change/give a title?
			if (!player.hasClanPrivilege(ClanPrivilege.CL_GIVE_TITLE))
			{
				player.sendPacket(SystemMessageId.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT);
				return;
			}
			
			if (player.getClan().getLevel() < 3)
			{
				player.sendPacket(SystemMessageId.CLAN_LVL_3_NEEDED_TO_ENDOWE_TITLE);
				return;
			}
			
			var member = player.getClan().getClanMember(_name);
			if (member != null)
			{
				var playerMember = member.getPlayerInstance();
				if (playerMember != null)
				{
					// is target from the same clan?
					playerMember.setTitle(_title);
					playerMember.sendPacket(SystemMessageId.TITLE_CHANGED);
					if (player != playerMember)
					{
						player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.CLAN_MEMBER_S1_TITLE_CHANGED_TO_S2).addCharName(playerMember).addString(_title));
					}
					playerMember.broadcastTitleInfo();
				}
				else
				{
					player.sendPacket(SystemMessageId.THAT_PLAYER_IS_NOT_ONLINE);
				}
			}
			else
			{
				player.sendPacket(SystemMessageId.TARGET_MUST_BE_IN_CLAN);
			}
		}
	}
	
	@Override
	public String getType()
	{
		return _C__0B_REQUESTGIVENICKNAME;
	}
}
