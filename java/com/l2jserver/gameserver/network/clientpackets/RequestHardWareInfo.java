/*
 * Copyright (C) 2004-2022 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.model.holders.ClientHardwareInfoHolder;

public class RequestHardWareInfo extends L2GameClientPacket
{
	private static final String _C__D0_96_REQUESTHARDWAREINFO = "[C] D0:96 RequestHardWareInfo";
	
	private String _macAddress;
	private int _windowsPlatformId;
	private int _windowsMajorVersion;
	private int _windowsMinorVersion;
	private int _windowsBuildNumber;
	private int _directxVersion;
	private int _directxRevision;
	private String _cpuName;
	private int _cpuSpeed;
	private int _cpuCoreCount;
	private int _vgaCount;
	private int _vgaPcxSpeed;
	private int _physMemorySlot1;
	private int _physMemorySlot2;
	private int _physMemorySlot3;
	private int _videoMemory;
	private int _vgaVersion;
	private String _vgaName;
	private String _vgaDriverVersion;
	
	@Override
	protected void readImpl()
	{
		_macAddress = readS();
		_windowsPlatformId = readD();
		_windowsMajorVersion = readD();
		_windowsMinorVersion = readD();
		_windowsBuildNumber = readD();
		_directxVersion = readD();
		_directxRevision = readD();
		readB(new byte[16]);
		_cpuName = readS();
		_cpuSpeed = readD();
		_cpuCoreCount = readC();
		readD();
		_vgaCount = readD();
		_vgaPcxSpeed = readD();
		_physMemorySlot1 = readD();
		_physMemorySlot2 = readD();
		_physMemorySlot3 = readD();
		readC();
		_videoMemory = readD();
		readD();
		_vgaVersion = readH();
		_vgaName = readS();
		_vgaDriverVersion = readS();
		return;
	}
	
	@Override
	protected void runImpl()
	{
		var client = getClient();
		client.setHardwareInfo(new ClientHardwareInfoHolder(_macAddress, _windowsPlatformId, _windowsMajorVersion, _windowsMinorVersion, _windowsBuildNumber, _directxVersion, _directxRevision, _cpuName, _cpuSpeed, _cpuCoreCount, _vgaCount, _vgaPcxSpeed, _physMemorySlot1, _physMemorySlot2, _physMemorySlot3, _videoMemory, _vgaVersion, _vgaName, _vgaDriverVersion));
	}
	
	@Override
	public String getType()
	{
		return _C__D0_96_REQUESTHARDWAREINFO;
	}
}
