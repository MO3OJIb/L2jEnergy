/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.configuration.config.protection.BaseProtectionConfig;
import com.l2jserver.gameserver.data.xml.impl.HennaData;
import com.l2jserver.gameserver.enums.FloodAction;
import com.l2jserver.gameserver.model.PcCondOverride;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.InventoryUpdate;
import com.l2jserver.gameserver.util.FloodProtectors;
import com.l2jserver.gameserver.util.Util;

/**
 * @author Zoey76
 */
public final class RequestHennaEquip extends L2GameClientPacket
{
	private static final String _C__6F_REQUESTHENNAEQUIP = "[C] 6F RequestHennaEquip";
	private int _symbolId;
	
	@Override
	protected void readImpl()
	{
		_symbolId = readD();
	}
	
	@Override
	protected void runImpl()
	{
		var player = getActiveChar();
		if (player == null)
		{
			return;
		}
		
		if (!FloodProtectors.performAction(getClient(), FloodAction.TRANSACTION))
		{
			return;
		}
		
		if (player.getHennaEmptySlots() == 0)
		{
			player.sendPacket(SystemMessageId.SYMBOLS_FULL);
			sendActionFailed();
			return;
		}
		
		var henna = HennaData.getInstance().getHenna(_symbolId);
		if (henna == null)
		{
			LOG.warn("{}: Invalid Henna Id: {} from player {}", getClass().getName(), _symbolId, player);
			sendActionFailed();
			return;
		}
		
		var _count = player.getInventory().getInventoryItemCount(henna.getDyeItemId(), -1);
		if (henna.isAllowedClass(player.getClassId()) && (_count >= henna.getWearCount()) && (player.getAdena() >= henna.getWearFee()) && player.addHenna(henna))
		{
			player.destroyItemByItemId("Henna", henna.getDyeItemId(), henna.getWearCount(), player, true);
			player.getInventory().reduceAdena("Henna", henna.getWearFee(), player, player.getLastFolkNPC());
			var iu = new InventoryUpdate();
			iu.addModifiedItem(player.getInventory().getAdenaInstance());
			player.sendPacket(iu);
			player.sendPacket(SystemMessageId.SYMBOL_ADDED);
		}
		else
		{
			player.sendPacket(SystemMessageId.CANT_DRAW_SYMBOL);
			if (!player.canOverrideCond(PcCondOverride.ITEM_CONDITIONS) && !henna.isAllowedClass(player.getClassId()))
			{
				Util.handleIllegalPlayerAction(player, "Exploit attempt: Character " + player.getName() + " of account " + player.getAccountName() + " tryed to add a forbidden henna.", BaseProtectionConfig.DEFAULT_PUNISH);
			}
			sendActionFailed();
		}
	}
	
	@Override
	public String getType()
	{
		return _C__6F_REQUESTHENNAEQUIP;
	}
}
