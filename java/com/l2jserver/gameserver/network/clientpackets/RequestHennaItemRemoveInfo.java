/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.data.xml.impl.HennaData;
import com.l2jserver.gameserver.network.serverpackets.HennaItemRemoveInfo;

/**
 * @author Zoey76
 */
public final class RequestHennaItemRemoveInfo extends L2GameClientPacket
{
	private static final String _C__71_REQUESTHENNAITEMREMOVEINFO = "[C] 71 RequestHennaItemRemoveInfo";
	
	private int _symbolId;
	
	@Override
	protected void readImpl()
	{
		_symbolId = readD();
	}
	
	@Override
	protected void runImpl()
	{
		var player = getClient().getActiveChar();
		if ((player == null) || (_symbolId == 0))
		{
			return;
		}
		
		var henna = HennaData.getInstance().getHenna(_symbolId);
		if (henna == null)
		{
			LOG.warn("{}: Invalid Henna Id: {} from player {}", getClass().getName(), _symbolId, player);
			sendActionFailed();
			return;
		}
		player.sendPacket(new HennaItemRemoveInfo(henna, player));
	}
	
	@Override
	public String getType()
	{
		return _C__71_REQUESTHENNAITEMREMOVEINFO;
	}
}
