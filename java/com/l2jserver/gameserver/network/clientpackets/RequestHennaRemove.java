/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.enums.FloodAction;
import com.l2jserver.gameserver.model.items.L2Henna;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.util.FloodProtectors;

/**
 * @author Zoey76
 */
public final class RequestHennaRemove extends L2GameClientPacket
{
	private static final String _C__72_REQUESTHENNAREMOVE = "[C] 72 RequestHennaRemove";
	private int _symbolId;
	
	@Override
	protected void readImpl()
	{
		_symbolId = readD();
	}
	
	@Override
	protected void runImpl()
	{
		var player = getActiveChar();
		if (player == null)
		{
			return;
		}
		
		if (!FloodProtectors.performAction(getClient(), FloodAction.TRANSACTION))
		{
			sendActionFailed();
			return;
		}
		
		L2Henna henna;
		var found = false;
		for (var i = 1; i <= 3; i++)
		{
			henna = player.getHenna(i);
			if ((henna != null) && (henna.getDyeId() == _symbolId))
			{
				if (player.getAdena() >= henna.getCancelFee())
				{
					player.removeHenna(i);
				}
				else
				{
					player.sendPacket(SystemMessageId.YOU_NOT_ENOUGH_ADENA);
					sendActionFailed();
				}
				found = true;
				break;
			}
		}
		// TODO: Test.
		if (!found)
		{
			LOG.warn("Player {} requested Henna Draw remove without any henna.", player);
			sendActionFailed();
		}
	}
	
	@Override
	public String getType()
	{
		return _C__72_REQUESTHENNAREMOVE;
	}
}
