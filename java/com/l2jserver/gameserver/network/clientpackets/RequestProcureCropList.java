/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import java.util.ArrayList;
import java.util.List;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.instancemanager.CastleManorManager;
import com.l2jserver.gameserver.model.actor.instance.L2MerchantInstance;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.CropHolder;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.SystemMessage;

/**
 * @author l3x
 */
public class RequestProcureCropList extends L2GameClientPacket
{
	private static final int BATCH_LENGTH = 20; // length of the one item
	
	private List<CropHolder> _items = null;
	
	@Override
	protected final void readImpl()
	{
		var count = readD();
		if ((count <= 0) || (count > MAX_ITEM_IN_PACKET) || ((count * BATCH_LENGTH) != _buf.remaining()))
		{
			return;
		}
		
		_items = new ArrayList<>(count);
		for (var i = 0; i < count; i++)
		{
			var objId = readD();
			var itemId = readD();
			var manorId = readD();
			var cnt = readQ();
			if ((objId < 1) || (itemId < 1) || (manorId < 0) || (cnt < 0))
			{
				_items = null;
				return;
			}
			_items.add(new CropHolder(objId, itemId, cnt, manorId));
		}
	}
	
	@Override
	protected final void runImpl()
	{
		if (_items == null)
		{
			return;
		}
		
		var player = getActiveChar();
		if (player == null)
		{
			return;
		}
		
		var manor = CastleManorManager.getInstance();
		if (manor.isUnderMaintenance())
		{
			sendActionFailed();
			return;
		}
		
		var manager = player.getLastFolkNPC();
		if (!(manager instanceof L2MerchantInstance) || !manager.canInteract(player))
		{
			sendActionFailed();
			return;
		}
		
		final int castleId = manager.getCastle().getResidenceId();
		if (manager.getTemplate().getParameters().getInt("manor_id", -1) != castleId)
		{
			sendActionFailed();
			return;
		}
		
		int slots = 0, weight = 0;
		for (var i : _items)
		{
			var item = player.getInventory().getItemByObjectId(i.getObjectId());
			if ((item == null) || (item.getCount() < i.getValue()) || (item.getId() != i.getId()))
			{
				sendActionFailed();
				return;
			}
			
			var cp = i.getCropProcure();
			if ((cp == null) || (cp.getAmount() < i.getValue()))
			{
				sendActionFailed();
				return;
			}
			
			var template = ItemTable.getInstance().getTemplate(i.getRewardId());
			weight += (i.getValue() * template.getWeight());
			
			if (!template.isStackable())
			{
				slots += i.getValue();
			}
			else if (player.getInventory().getItemByItemId(i.getRewardId()) == null)
			{
				slots++;
			}
		}
		
		if (!player.getInventory().validateWeight(weight))
		{
			player.sendPacket(SystemMessageId.WEIGHT_LIMIT_EXCEEDED);
			return;
		}
		else if (!player.getInventory().validateCapacity(slots))
		{
			player.sendPacket(SystemMessageId.YOUR_INVENTORY_IS_FULL);
			return;
		}
		
		// Proceed the purchase
		for (var i : _items)
		{
			var rewardPrice = ItemTable.getInstance().getTemplate(i.getRewardId()).getReferencePrice();
			if (rewardPrice == 0)
			{
				continue;
			}
			
			var rewardItemCount = i.getPrice() / rewardPrice;
			if (rewardItemCount < 1)
			{
				player.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.FAILED_IN_TRADING_S2_OF_CROP_S1).addItemName(i.getId()).addLong(i.getValue()));
				tradeFailureNotification(player, i);
				continue;
			}
			
			// Fee for selling to other manors
			var fee = (castleId == i.getManorId()) ? 0 : ((long) (i.getPrice() * 0.05));
			if ((fee != 0) && (player.getAdena() < fee))
			{
				sendPacket(SystemMessage.getSystemMessage(SystemMessageId.FAILED_IN_TRADING_S2_OF_CROP_S1).addItemName(i.getId()).addLong(i.getValue()));
				sendPacket(SystemMessage.getSystemMessage(SystemMessageId.YOU_NOT_ENOUGH_ADENA));
				player.sendMessage(MessagesData.getInstance().getMessage(player, "you_need_adena_pay_foreign_manor_trading_fee").replace("%s%", fee + ""));
				continue;
			}
			
			var cp = i.getCropProcure();
			if (!cp.decreaseAmount(i.getValue()) || ((fee > 0) && !player.reduceAdena("Manor", fee, manager, true)) || !player.destroyItem("Manor", i.getObjectId(), i.getValue(), manager, true))
			{
				continue;
			}
			player.addItem("Manor", i.getRewardId(), rewardItemCount, manager, true);
		}
	}
	
	private void tradeFailureNotification(L2PcInstance player, CropHolder crop)
	{
		var rewardName = ItemTable.getInstance().getTemplate(crop.getRewardId()).getName();
		var cropName = ItemTable.getInstance().getTemplate(crop.getId()).getName();
		player.sendMessage(MessagesData.getInstance().getMessage(player, "not_enough_offered_trade_for_unit").replace("%s%", rewardName + "").replace("%i%", cropName + ""));
	}
	
	@Override
	public String getType()
	{
		return "[C] D0:02 RequestProcureCropList";
	}
}