/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

/**
 * @author ShanSoft
 * @structure chdSdS
 */
public final class RequestSaveBookMarkSlot extends L2GameClientPacket
{
	private static final String _C__D0_51_01_REQUESTSAVEBOOKMARKSLOT = "[C] D0:51:01 RequestSaveBookMarkSlot";
	
	private int icon;
	private String name, tag;
	
	@Override
	protected void readImpl()
	{
		name = readS();
		icon = readD();
		tag = readS();
	}
	
	@Override
	protected void runImpl()
	{
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		player.teleportBookmarkAdd(player.getX(), player.getY(), player.getZ(), icon, tag, name);
	}
	
	@Override
	public String getType()
	{
		return _C__D0_51_01_REQUESTSAVEBOOKMARKSLOT;
	}
}
