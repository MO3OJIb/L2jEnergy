/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.l2jserver.gameserver.configuration.config.CharacterConfig;
import com.l2jserver.gameserver.data.xml.impl.UIData;
import com.l2jserver.gameserver.enums.network.GameClientState;
import com.l2jserver.gameserver.model.ActionKey;

/**
 * Request Save Key Mapping client packet.
 * @author mrTJO, Zoey76
 */
public class RequestSaveKeyMapping extends L2GameClientPacket
{
	private static final String _C__D0_22_REQUESTSAVEKEYMAPPING = "[C] D0:22 RequestSaveKeyMapping";
	
	private final Map<Integer, List<ActionKey>> _keyMap = new HashMap<>();
	private final Map<Integer, List<Integer>> _catMap = new HashMap<>();
	
	@Override
	protected void readImpl()
	{
		var category = 0;
		
		readD(); // Unknown
		readD(); // Unknown
		var _tabNum = readD();
		for (var i = 0; i < _tabNum; i++)
		{
			var cmd1Size = readC();
			for (var j = 0; j < cmd1Size; j++)
			{
				UIData.addCategory(_catMap, category, readC());
			}
			category++;
			
			var cmd2Size = readC();
			for (var j = 0; j < cmd2Size; j++)
			{
				UIData.addCategory(_catMap, category, readC());
			}
			category++;
			
			var cmdSize = readD();
			for (var j = 0; j < cmdSize; j++)
			{
				var cmd = readD();
				var key = readD();
				var tgKey1 = readD();
				var tgKey2 = readD();
				var show = readD();
				UIData.addKey(_keyMap, i, new ActionKey(i, cmd, key, tgKey1, tgKey2, show));
			}
		}
		readD();
		readD();
	}
	
	@Override
	protected void runImpl()
	{
		var player = getActiveChar();
		if (!CharacterConfig.STORE_UI_SETTINGS || (player == null) || (getClient().getState() != GameClientState.IN_GAME))
		{
			return;
		}
		player.getUISettings().storeAll(_catMap, _keyMap);
	}
	
	@Override
	public String getType()
	{
		return _C__D0_22_REQUESTSAVEKEYMAPPING;
	}
}
