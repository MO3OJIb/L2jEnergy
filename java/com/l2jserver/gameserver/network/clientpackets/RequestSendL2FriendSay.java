/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.configuration.config.AdminConfig;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.L2FriendSay;
import com.l2jserver.gameserver.util.LoggingUtils;

public final class RequestSendL2FriendSay extends L2GameClientPacket
{
	private static final String _C__6B_REQUESTSENDL2FRIENDSAY = "[C] 6B RequestSendL2FriendSay";
	
	private static Logger LOG_CHAT = LoggerFactory.getLogger("chat");
	
	private String _message;
	private String _reciever;
	
	@Override
	protected void readImpl()
	{
		_message = readS();
		_reciever = readS();
	}
	
	@Override
	protected void runImpl()
	{
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		
		if ((_message == null) || _message.isEmpty() || (_message.length() > 300))
		{
			return;
		}
		
		var target = L2World.getInstance().getPlayer(_reciever);
		if ((target == null) || !target.isFriend(player.getObjectId()))
		{
			player.sendPacket(SystemMessageId.THAT_PLAYER_IS_NOT_ONLINE);
			return;
		}
		
		if (AdminConfig.LOG_CHAT)
		{
			LoggingUtils.logChat(LOG_CHAT, player.getName(), _reciever, _message);
		}
		target.sendPacket(new L2FriendSay(player.getName(), _reciever, _message));
	}
	
	@Override
	public String getType()
	{
		return _C__6B_REQUESTSENDL2FRIENDSAY;
	}
}