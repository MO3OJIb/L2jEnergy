/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.dao.factory.impl.DAOFactory;
import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.enums.PrivateStoreType;
import com.l2jserver.gameserver.network.serverpackets.ExGoodsInventoryInfo;
import com.l2jserver.gameserver.network.serverpackets.ExGoodsInventoryResult;

public class RequestUseGoodsInventoryItem extends L2GameClientPacket
{
	private static final String _C__DO_92_REQUESTUSEGOODSINVENTORYITEM = "[C] D0:92 RequestUseGoodsInventoryItem";
	
	private long _itemId;
	private int _goodsType;
	private long _itemCount;
	
	@Override
	protected void readImpl()
	{
		_goodsType = readC();
		_itemId = readQ();
		if (_goodsType != 1)
		{
			_itemCount = readQ();
		}
	}
	
	@Override
	protected void runImpl()
	{
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		
		if (player.isProcessingRequest())
		{
			player.sendPacket(ExGoodsInventoryResult.PREVIOS_REQUEST_IS_NOT_COMPLETE);
		}
		
		if (player.getPrivateStoreType() != PrivateStoreType.NONE)
		{
			player.sendPacket(ExGoodsInventoryResult.CANT_USE_AT_TRADE_OR_PRIVATE_SHOP);
			return;
		}
		
		if (player.getPremiumItemList().isEmpty())
		{
			player.sendPacket(ExGoodsInventoryResult.NOT_EXISTS);
			return;
		}
		
		if (player.getInventory().getSize(false) >= (player.getInventoryLimit() * 0.8))
		{
			player.sendPacket(ExGoodsInventoryResult.INVENTORY_FULL);
			return;
		}
		
		// TODO: need fix
		// if (LoginServerThread.getInstance().isShutdown())
		// {
		// activeChar.sendPacket(new ExGoodsInventoryResult(ExGoodsInventoryResult.NOT_CONNECT_TO_PRODUCT_SERVER));
		// }
		
		var _premiumItem = player.getPremiumItemList().get((int) _itemId);
		
		if (_premiumItem == null)
		{
			return;
		}
		
		var stackable = ItemTable.getInstance().getTemplate(_premiumItem.getItemId()).isStackable();
		
		if ((_itemCount != 0) && (_premiumItem.getCount() < _itemCount))
		{
			return;
		}
		
		if (!stackable)
		{
			for (var i = 0; i < _itemCount; i++)
			{
				player.addItem("premiumItem", _premiumItem.getItemId(), _itemCount, player.getTarget(), true);
			}
		}
		else
		{
			player.addItem("premiumItem", _premiumItem.getItemId(), _itemCount, player.getTarget(), true);
		}
		
		var itemsLeft = _premiumItem.getCount() - _itemCount;
		
		if (_itemCount < _premiumItem.getCount())
		{
			_premiumItem.updateCount(itemsLeft);
			DAOFactory.getInstance().getPremiumItemDAO().update(player, (int) _itemId, _premiumItem.getCount() - _itemCount);
		}
		else
		{
			DAOFactory.getInstance().getPremiumItemDAO().delete(player, (int) _itemId);
		}
		player.sendPacket(new ExGoodsInventoryInfo(player.getPremiumItemList()));
	}
	
	@Override
	public String getType()
	{
		return _C__DO_92_REQUESTUSEGOODSINVENTORYITEM;
	}
}
