/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.model.PartyMatchRoomList;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.network.serverpackets.ExClosePartyRoom;

/**
 * @author Gnacik
 */
public final class RequestWithdrawPartyRoom extends L2GameClientPacket
{
	private static final String _C__D0_0B_REQUESTWITHDRAWPARTYROOM = "[C] D0:0B RequestWithdrawPartyRoom";
	
	private int _roomid;
	@SuppressWarnings("unused")
	private int _unk1;
	
	@Override
	protected void readImpl()
	{
		_roomid = readD();
		_unk1 = readD();
	}
	
	@Override
	protected void runImpl()
	{
		var player = getClient().getActiveChar();
		
		if (player == null)
		{
			return;
		}
		
		var _room = PartyMatchRoomList.getInstance().getRoom(_roomid);
		if (_room == null)
		{
			return;
		}
		
		if ((player.isInParty() && _room.getOwner().isInParty()) && (player.getParty().getLeaderObjectId() == _room.getOwner().getParty().getLeaderObjectId()))
		{
			// If user is in party with Room Owner
			// is not removed from Room
			
			// _activeChar.setPartyMatching(0);
			player.broadcastUserInfo();
		}
		else
		{
			_room.deleteMember(player);
			
			player.setPartyRoom(0);
			// _activeChar.setPartyMatching(0);
			
			player.sendPacket(new ExClosePartyRoom());
			player.sendPacket(SystemMessageId.PARTY_ROOM_EXITED);
		}
	}
	
	@Override
	public String getType()
	{
		return _C__D0_0B_REQUESTWITHDRAWPARTYROOM;
	}
}
