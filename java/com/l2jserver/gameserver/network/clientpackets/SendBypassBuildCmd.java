/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.configuration.config.AdminConfig;
import com.l2jserver.gameserver.data.xml.impl.AdminData;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.handler.AdminCommandHandler;
import com.l2jserver.gameserver.util.LoggingUtils;

public final class SendBypassBuildCmd extends L2GameClientPacket
{
	private static final Logger LOG_GM_AUDIT = LoggerFactory.getLogger("gmaudit");
	
	private static final String _C__74_SENDBYPASSBUILDCMD = "[C] 74 SendBypassBuildCmd";
	
	public static final int GM_MESSAGE = 9;
	public static final int ANNOUNCEMENT = 10;
	
	private String _command;
	
	@Override
	protected void readImpl()
	{
		_command = readS();
		if (_command != null)
		{
			_command = _command.trim();
		}
	}
	
	@Override
	protected void runImpl()
	{
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		
		var command = "admin_" + _command.split(" ")[0];
		
		var ach = AdminCommandHandler.getInstance().getHandler(command);
		
		if (ach == null)
		{
			if (player.isGM())
			{
				player.sendMessage(MessagesData.getInstance().getMessage(player, "gm_command_not_exist").replace("%s%", command.substring(6) + ""));
			}
			LOG.warn("No handler registered for admin command '{}'", command);
			return;
		}
		
		if (!AdminData.getInstance().hasAccess(command, player.getAccessLevel()))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "gm_command_not_access"));
			LOG.warn("Character {} tryed to use admin command {}, but have no access to it!", player.getName(), command);
			return;
		}
		
		if (AdminConfig.GMAUDIT)
		{
			LoggingUtils.logGMAudit(LOG_GM_AUDIT, player.getName() + " [" + player.getObjectId() + "]", _command, (player.getTarget() != null ? player.getTarget().getName() : "no-target"));
		}
		
		ach.useAdminCommand("admin_" + _command, player);
	}
	
	@Override
	public String getType()
	{
		return _C__74_SENDBYPASSBUILDCMD;
	}
}
