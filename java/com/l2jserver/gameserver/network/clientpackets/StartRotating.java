/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.network.serverpackets.StartRotation;

public final class StartRotating extends L2GameClientPacket
{
	private static final String _C__5B_STARTROTATING = "[C] 5B StartRotating";
	
	private int _degree;
	private int _side;
	
	@Override
	protected void readImpl()
	{
		_degree = readD();
		_side = readD();
	}
	
	@Override
	protected void runImpl()
	{
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		
		if (player.isInAirShip() && player.getAirShip().isCaptain(player))
		{
			var br = new StartRotation(player.getAirShip().getObjectId(), _degree, _side, 0);
			player.getAirShip().broadcastPacket(br);
		}
		else
		{
			var br = new StartRotation(player.getObjectId(), _degree, _side, 0);
			player.broadcastPacket(br);
		}
	}
	
	@Override
	public String getType()
	{
		return _C__5B_STARTROTATING;
	}
}
