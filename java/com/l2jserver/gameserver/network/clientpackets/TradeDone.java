/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.enums.FloodAction;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.network.SystemMessageId;
import com.l2jserver.gameserver.util.FloodProtectors;

/**
 * This packet manages the trade response.
 */
public final class TradeDone extends L2GameClientPacket
{
	private static final String _C__1C_TRADEDONE = "[C] 1C TradeDone";
	
	private int _response;
	
	@Override
	protected void readImpl()
	{
		_response = readD();
	}
	
	@Override
	protected void runImpl()
	{
		var player = getActiveChar();
		if (player == null)
		{
			return;
		}
		
		if (!FloodProtectors.performAction(getClient(), FloodAction.TRANSACTION))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "trading_too_fast"));
			return;
		}
		
		var trade = player.getActiveTradeList();
		if (trade == null)
		{
			return;
		}
		
		if (trade.isLocked())
		{
			return;
		}
		
		if (_response != 1)
		{
			player.cancelActiveTrade();
			return;
		}
		
		// Trade owner not found, or owner is different of packet sender.
		var owner = trade.getOwner();
		if ((owner == null) || !owner.equals(player))
		{
			return;
		}
		
		// Trade partner not found, cancel trade
		var partner = trade.getPartner();
		if ((partner == null) || (L2World.getInstance().getPlayer(partner.getObjectId()) == null))
		{
			player.sendPacket(SystemMessageId.THAT_PLAYER_IS_NOT_ONLINE);
			player.cancelActiveTrade();
			return;
		}
		
		if (!player.getAccessLevel().allowTransaction())
		{
			player.sendPacket(SystemMessageId.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT);
			player.cancelActiveTrade();
			return;
		}
		
		if ((owner.getActiveEnchantItemId() != L2PcInstance.ID_NONE) || (partner.getActiveEnchantItemId() != L2PcInstance.ID_NONE))
		{
			return;
		}
		
		if ((player.getInstanceId() != trade.getPartner().getInstanceId()) && (player.getInstanceId() != -1))
		{
			player.cancelActiveTrade();
			return;
		}
		
		if (player.calculateDistance(trade.getPartner(), true, false) > 150)
		{
			player.cancelActiveTrade();
			return;
		}
		trade.confirm();
	}
	
	@Override
	public String getType()
	{
		return _C__1C_TRADEDONE;
	}
}
