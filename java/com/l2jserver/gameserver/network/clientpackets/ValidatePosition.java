/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.clientpackets;

import com.l2jserver.gameserver.configuration.config.DeveloperConfig;
import com.l2jserver.gameserver.configuration.config.GeoDataConfig;
import com.l2jserver.gameserver.enums.ZoneId;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.network.serverpackets.GetOnVehicle;
import com.l2jserver.gameserver.network.serverpackets.ValidateLocation;

public class ValidatePosition extends L2GameClientPacket
{
	private static final String _C__59_VALIDATEPOSITION = "[C] 59 ValidatePosition";
	
	private int _x;
	private int _y;
	private int _z;
	private int _heading;
	private int _data; // vehicle id
	
	@Override
	protected void readImpl()
	{
		_x = readD();
		_y = readD();
		_z = readD();
		_heading = readD();
		_data = readD();
	}
	
	@Override
	protected void runImpl()
	{
		var player = getClient().getActiveChar();
		if ((player == null) || player.isTeleporting() || player.inObserverMode())
		{
			return;
		}
		
		player.updatePosition();
		
		var realX = player.getX();
		var realY = player.getY();
		var realZ = player.getZ();
		
		var realHeading = player.getHeading();
		var realVehicle = player.getVehicle() != null ? player.getVehicle().getObjectId() : 0;
		
		if (DeveloperConfig.DEVELOPER)
		{
			LOG.debug("ValidatePos[x={}, y={}, z={}, heading={}, vehicle={}], serverX={}, serverY={}, serverZ={}, serverHeading={}, serverVehicle={}", _x, _y, _z, _heading, _data, realX, realY, realZ, realHeading, realVehicle);
		}
		
		if ((_x == 0) && (_y == 0))
		{
			if (realX != 0)
			{
				return;
			}
		}
		
		int dx, dy, dz;
		double diffSq;
		
		if (player.isInBoat())
		{
			if (GeoDataConfig.COORD_SYNCHRONIZE == 2)
			{
				dx = _x - player.getInVehiclePosition().getX();
				dy = _y - player.getInVehiclePosition().getY();
				// dz = _z - activeChar.getInVehiclePosition().getZ();
				diffSq = ((dx * dx) + (dy * dy));
				if (diffSq > 250000)
				{
					sendPacket(new GetOnVehicle(player.getObjectId(), _data, player.getInVehiclePosition()));
				}
			}
			return;
		}
		else if (player.isInAirShip())
		{
			// Zoey76: TODO: Implement or cleanup.
			// if (Config.COORD_SYNCHRONIZE == 2)
			// {
			// dx = _x - activeChar.getInVehiclePosition().getX();
			// dy = _y - activeChar.getInVehiclePosition().getY();
			// dz = _z - activeChar.getInVehiclePosition().getZ();
			// diffSq = ((dx * dx) + (dy * dy));
			// if (diffSq > 250000)
			// {
			// sendPacket(new GetOnVehicle(activeChar.getObjectId(), _data, activeChar.getInBoatPosition()));
			// }
			// }
			return;
		}
		else if (player.isFalling(_z))
		{
			return; // disable validations during fall to avoid "jumping"
		}
		
		dx = _x - realX;
		dy = _y - realY;
		dz = _z - realZ;
		diffSq = ((dx * dx) + (dy * dy));
		
		// Zoey76: TODO: Implement or cleanup.
		// L2Party party = activeChar.getParty();
		// if ((party != null) && (activeChar.getLastPartyPositionDistance(_x, _y, _z) > 150))
		// {
		// activeChar.setLastPartyPosition(_x, _y, _z);
		// party.broadcastToPartyMembers(activeChar, new PartyMemberPosition(activeChar));
		// }
		
		// Don't allow flying transformations outside gracia area!
		if (player.isFlyingMounted() && (_x > L2World.GRACIA_MAX_X))
		{
			player.untransform();
		}
		
		if (player.isFlying() || player.isInsideZone(ZoneId.WATER))
		{
			player.setXYZ(realX, realY, _z);
			if (diffSq > 90000)
			{
				player.sendPacket(new ValidateLocation(player));
			}
		}
		else if (diffSq < 360000) // if too large, messes observation
		{
			if (GeoDataConfig.COORD_SYNCHRONIZE == -1) // Only Z coordinate synched to server,
			// mainly used when no geodata but can be used also with geodata
			{
				player.setXYZ(realX, realY, _z);
				return;
			}
			if (GeoDataConfig.COORD_SYNCHRONIZE == 1) // Trusting also client x,y coordinates (should not be used with geodata)
			{
				if (!player.isMoving() || !player.validateMovementHeading(_heading)) // Heading changed on client = possible obstacle
				{
					// character is not moving, take coordinates from client
					if (diffSq < 2500)
					{
						player.setXYZ(realX, realY, _z);
					}
					else
					{
						player.setXYZ(_x, _y, _z);
					}
				}
				else
				{
					player.setXYZ(realX, realY, _z);
				}
				
				player.setHeading(_heading);
				return;
			}
			// Sync 2 (or other),
			// intended for geodata. Sends a validation packet to client
			// when too far from server calculated true coordinate.
			// Due to geodata/zone errors, some Z axis checks are made. (maybe a temporary solution)
			// Important: this code part must work together with L2Character.updatePosition
			if ((diffSq > 250000) || (Math.abs(dz) > 200))
			{
				// if ((_z - activeChar.getClientZ()) < 200 && Math.abs(activeChar.getLastServerPosition().getZ()-realZ) > 70)
				
				if ((Math.abs(dz) > 200) && (Math.abs(dz) < 1500) && (Math.abs(_z - player.getClientZ()) < 800))
				{
					player.setXYZ(realX, realY, _z);
					realZ = _z;
				}
				else
				{
					if (DeveloperConfig.DEVELOPER)
					{
						LOG.debug("{}: Synchronizing position Server --> Client", player.getName());
					}
					player.sendPacket(new ValidateLocation(player));
				}
			}
		}
		
		player.setClientX(_x);
		player.setClientY(_y);
		player.setClientZ(_z);
		player.setClientHeading(_heading); // No real need to validate heading.
		player.setLastServerPosition(realX, realY, realZ);
	}
	
	@Override
	public String getType()
	{
		return _C__59_VALIDATEPOSITION;
	}
}
