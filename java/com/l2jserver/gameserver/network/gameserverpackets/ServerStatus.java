/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.gameserverpackets;

import java.util.ArrayList;
import java.util.List;

import com.l2jserver.commons.network.AttributeType;
import com.l2jserver.commons.network.BaseSendablePacket;
import com.l2jserver.gameserver.model.holders.IntLongHolder;

/**
 * @author -Wooden-
 */
public class ServerStatus extends BaseSendablePacket
{
	private final List<IntLongHolder> _attributes;
	
	public static final int ON = 0x01;
	public static final int OFF = 0x00;
	
	public ServerStatus()
	{
		_attributes = new ArrayList<>();
	}
	
	public void addAttribute(AttributeType type, int value)
	{
		_attributes.add(new IntLongHolder(type.getId(), value));
	}
	
	public void addAttribute(AttributeType type, boolean onOrOff)
	{
		addAttribute(type, (onOrOff) ? ON : OFF);
	}
	
	@Override
	public byte[] getContent()
	{
		writeC(0x06);
		writeD(_attributes.size());
		for (var temp : _attributes)
		{
			writeD(temp.getId());
			writeD((int) temp.getValue());
		}
		
		return getBytes();
	}
}