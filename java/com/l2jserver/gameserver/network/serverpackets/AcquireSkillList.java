/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.serverpackets;

import java.util.ArrayList;
import java.util.List;

import com.l2jserver.gameserver.enums.skills.AcquireSkillType;
import com.l2jserver.gameserver.model.holders.AcquireSkilInfoHolder;

/**
 * Acquire Skill List server packet implementation.
 */
public final class AcquireSkillList extends L2GameServerPacket
{
	private final List<AcquireSkilInfoHolder> _skills;
	private final AcquireSkillType _skillType;
	
	public AcquireSkillList(AcquireSkillType type)
	{
		_skillType = type;
		_skills = new ArrayList<>();
	}
	
	public void addSkill(int id, int nextLevel, int maxLevel, int spCost, int requirements)
	{
		_skills.add(new AcquireSkilInfoHolder(id, nextLevel, maxLevel, spCost, requirements));
	}
	
	@Override
	protected void writeImpl()
	{
		if (_skills.isEmpty())
		{
			return;
		}
		
		writeC(0x90);
		writeD(_skillType.ordinal());
		writeD(_skills.size());
		
		for (var temp : _skills)
		{
			writeD(temp.getId());
			writeD(temp.getNextLevel());
			writeD(temp.getMaxLevel());
			writeD(temp.getSpCost());
			writeD(temp.getRequirements());
			if (_skillType == AcquireSkillType.SUBPLEDGE)
			{
				writeD(0); // TODO: ?
			}
		}
	}
}