/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.serverpackets;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.l2jserver.gameserver.model.holders.PartHolder;

/**
 * This class is made to create packets with any format
 * @author Maktakien
 */
public class AdminForgePacket extends L2GameServerPacket
{
	private final List<PartHolder> _parts = new ArrayList<>();
	
	public AdminForgePacket()
	{
		
	}
	
	@Override
	protected void writeImpl()
	{
		for (var p : _parts)
		{
			generate(p.getByte(), p.getString());
		}
	}
	
	/**
	 * @param b
	 * @param string
	 * @return
	 */
	public boolean generate(byte b, String string)
	{
		if ((b == 'C') || (b == 'c'))
		{
			writeC(Integer.decode(string));
			return true;
		}
		else if ((b == 'D') || (b == 'd'))
		{
			writeD(Integer.decode(string));
			return true;
		}
		else if ((b == 'H') || (b == 'h'))
		{
			writeH(Integer.decode(string));
			return true;
		}
		else if ((b == 'F') || (b == 'f'))
		{
			writeF(Double.parseDouble(string));
			return true;
		}
		else if ((b == 'S') || (b == 's'))
		{
			writeS(string);
			return true;
		}
		else if ((b == 'B') || (b == 'b') || (b == 'X') || (b == 'x'))
		{
			writeB(new BigInteger(string).toByteArray());
			return true;
		}
		else if ((b == 'Q') || (b == 'q'))
		{
			writeQ(Long.decode(string));
			return true;
		}
		return false;
	}
	
	public void addPart(byte b, String string)
	{
		_parts.add(new PartHolder(b, string));
	}
}