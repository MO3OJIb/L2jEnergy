/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.serverpackets;

import com.l2jserver.gameserver.data.xml.impl.ProductItemData;
import com.l2jserver.gameserver.model.primeshop.PrimeShopGroup;

public class ExBR_ProductInfo extends L2GameServerPacket
{
	private final PrimeShopGroup _item;
	
	public ExBR_ProductInfo(final int item)
	{
		_item = ProductItemData.getInstance().getProduct(item);
	}
	
	@Override
	protected void writeImpl()
	{
		if (_item == null)
		{
			return;
		}
		
		writeC(0xFE);
		writeH(0xD7);
		
		writeD(_item.getProductId());
		writeD(_item.getPrice());
		writeD(_item.getItems().size());
		
		for (var item : _item.getItems())
		{
			writeD(item.getId());
			writeD((int) item.getValue());
			writeD(item.getWeight());
			writeD(item.isTradable());
		}
	}
}
