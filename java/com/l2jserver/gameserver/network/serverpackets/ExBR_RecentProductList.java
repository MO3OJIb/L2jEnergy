/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.serverpackets;

import java.util.ArrayList;
import java.util.List;

import com.l2jserver.gameserver.model.primeshop.PrimeShopGroup;

public class ExBR_RecentProductList extends L2GameServerPacket
{
	private List<PrimeShopGroup> _list = new ArrayList<>();
	
	public ExBR_RecentProductList(List<PrimeShopGroup> list)
	{
		_list = list;
	}
	
	@Override
	protected void writeImpl()
	{
		writeC(0xFE);
		writeH(0xDC);
		writeD(_list.size());
		
		for (var template : _list)
		{
			writeD(template.getProductId());
			writeH(template.getCategory());
			writeD(template.getPrice());
			writeD(template.getEventFlag().ordinal());// show tab 2-th group 1 - event 2 - best 3 - event & best
			writeD((int) template.getStartSaleDate()); // start sale
			writeD((int) template.getEndSaleDate()); // end sale
			writeC(template.getDayOfWeek()); // day week
			writeC(template.getStartSaleHour());
			writeC(template.getStartSaleMin());
			writeC(template.getEndSaleHour());
			writeC(template.getEndSaleMin());
			writeD(template.getCurrentStock()); // current stock
			writeD(template.getMaxStock()); // max stock
		}
	}
}
