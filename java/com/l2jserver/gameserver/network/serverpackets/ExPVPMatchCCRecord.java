/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.serverpackets;

import java.util.Map;

import com.l2jserver.gameserver.enums.events.PvPMatchRecordAction;
import com.l2jserver.gameserver.instancemanager.games.KrateisCubeManager;

public class ExPVPMatchCCRecord extends L2GameServerPacket
{
	private final PvPMatchRecordAction _state;
	private final Map<String, Integer> _playerPoints;
	
	public ExPVPMatchCCRecord(PvPMatchRecordAction state, Map<String, Integer> playerPoints)
	{
		_state = state;
		_playerPoints = playerPoints;
	}
	
	public ExPVPMatchCCRecord(PvPMatchRecordAction state)
	{
		_state = state;
		_playerPoints = KrateisCubeManager.getParticipantsMatch();
	}
	
	@Override
	public void writeImpl()
	{
		writeC(0xFE);
		writeH(0x89);
		writeD(_state.ordinal());
		writeD(_playerPoints.size());
		
		for (var player : _playerPoints.entrySet())
		{
			if (player != null)
			{
				writeS(player.getKey());
				writeD(player.getValue());
			}
		}
	}
}
