/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.serverpackets;

import java.util.List;

import com.l2jserver.gameserver.enums.events.Team;
import com.l2jserver.gameserver.model.eventengine.GameEventPlayer;

public class ExPVPMatchRecord extends L2GameServerPacket
{
	public static final int START = 0;
	public static final int UPDATE = 1;
	public static final int FINISH = 2;
	
	private final int _type;
	private final Team _winnerTeam;
	private final int _blueKills;
	private final int _redKills;
	private final List<GameEventPlayer> _blueList;
	private final List<GameEventPlayer> _redList;
	
	public ExPVPMatchRecord(int type, Team winnerTeam)
	{
		// TODO: fix
		_type = type;
		_winnerTeam = winnerTeam;
		_blueKills = 0;
		_redKills = 0;
		_blueList = null;
		_redList = null;
	}
	
	public ExPVPMatchRecord(int type, Team winnerTeam, int blueKills, int redKills, List<GameEventPlayer> blueTeam, List<GameEventPlayer> redTeam)
	{
		_type = type;
		_winnerTeam = winnerTeam;
		_blueKills = blueKills;
		_redKills = redKills;
		_blueList = blueTeam;
		_redList = redTeam;
	}
	
	@Override
	protected void writeImpl()
	{
		writeC(0xFE);
		writeH(0x7E);
		writeD(_type);
		writeD(_winnerTeam.ordinal());
		writeD(_winnerTeam.getId());
		writeD(_blueKills);
		writeD(_redKills);
		writeD(_blueList.size());
		
		for (var member : _blueList)
		{
			writeS(member.getName());
			writeD(member.getKills());
			writeD(member.getDeaths());
		}
		
		writeD(_redList.size());
		
		for (var member : _redList)
		{
			writeS(member.getName());
			writeD(member.getKills());
			writeD(member.getDeaths());
		}
	}
}