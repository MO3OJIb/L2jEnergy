/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.serverpackets;

import java.util.ArrayList;
import java.util.List;

import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.items.L2Henna;

/**
 * This server packet sends the player's henna information.
 * @author Zoey76
 */
public final class HennaInfo extends L2GameServerPacket
{
	private final L2PcInstance _player;
	private final List<L2Henna> _hennas = new ArrayList<>();
	
	public HennaInfo(L2PcInstance player)
	{
		_player = player;
		for (var henna : _player.getHennaList())
		{
			if (henna != null)
			{
				_hennas.add(henna);
			}
		}
	}
	
	@Override
	protected void writeImpl()
	{
		writeC(0xE5);
		writeC(_player.getHennaStatINT()); // equip INT
		writeC(_player.getHennaStatSTR()); // equip STR
		writeC(_player.getHennaStatCON()); // equip CON
		writeC(_player.getHennaStatMEN()); // equip MEN
		writeC(_player.getHennaStatDEX()); // equip DEX
		writeC(_player.getHennaStatWIT()); // equip WIT
		writeD(3); // Slots
		writeD(_hennas.size()); // Size
		for (var henna : _hennas)
		{
			writeD(henna.getDyeId());
			writeD(0x01);
		}
	}
}
