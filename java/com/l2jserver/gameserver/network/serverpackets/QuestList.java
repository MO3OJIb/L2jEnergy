/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.serverpackets;

import java.util.List;

import com.l2jserver.gameserver.model.quest.Quest;

public class QuestList extends L2GameServerPacket
{
	@Override
	protected final void writeImpl()
	{
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		
		final List<Quest> quests = player.getAllActiveQuests();
		
		writeC(0x86);
		writeH(quests.size());
		for (var q : quests)
		{
			writeD(q.getId());
			var qs = player.getQuestState(q.getName());
			if (qs == null)
			{
				writeD(0);
				continue;
			}
			
			var states = qs.getInt("__compltdStateFlags");
			if (states > 0)
			{
				writeD(states);
			}
			else
			{
				writeD(qs.getInt("cond"));
			}
		}
		writeB(new byte[128]);
	}
}
