/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.serverpackets;

import java.util.Calendar;

import com.l2jserver.gameserver.configuration.config.FeatureConfig;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.instancemanager.CHSiegeManager;
import com.l2jserver.gameserver.model.entity.Castle;
import com.l2jserver.gameserver.model.entity.ClanHall;

/**
 * Shows the Siege Info
 * @author KenM
 */
public class SiegeInfo extends L2GameServerPacket
{
	private Castle _castle;
	private ClanHall _hall;
	
	public SiegeInfo(Castle castle)
	{
		_castle = castle;
	}
	
	public SiegeInfo(ClanHall hall)
	{
		_hall = hall;
	}
	
	@Override
	protected final void writeImpl()
	{
		var player = getClient().getActiveChar();
		if (player == null)
		{
			return;
		}
		
		writeC(0xc9);
		if (_castle != null)
		{
			writeD(_castle.getResidenceId());
			
			var ownerId = _castle.getOwnerId();
			
			writeD(((ownerId == player.getClanId()) && (player.isClanLeader())) ? 0x01 : 0x00);
			writeD(ownerId);
			if (ownerId > 0)
			{
				var owner = ClanTable.getInstance().getClan(ownerId);
				if (owner != null)
				{
					writeS(owner.getName()); // Clan Name
					writeS(owner.getLeaderName()); // Clan Leader Name
					writeD(owner.getAllyId()); // Ally ID
					writeS(owner.getAllyName()); // Ally Name
				}
				else
				{
					LOG.warn("Null owner for castle: {}", _castle.getName());
				}
			}
			else
			{
				writeS(""); // Clan Name
				writeS(""); // Clan Leader Name
				writeD(0); // Ally ID
				writeS(""); // Ally Name
			}
			
			writeD((int) (System.currentTimeMillis() / 1000));
			if (!_castle.getIsTimeRegistrationOver() && player.isClanLeader() && (player.getClanId() == _castle.getOwnerId()))
			{
				var cal = Calendar.getInstance();
				cal.setTimeInMillis(_castle.getSiegeDate().getTimeInMillis());
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				
				writeD(0x00);
				writeD(FeatureConfig.SIEGE_HOUR_LIST.size());
				for (var hour : FeatureConfig.SIEGE_HOUR_LIST)
				{
					cal.set(Calendar.HOUR_OF_DAY, hour);
					writeD((int) (cal.getTimeInMillis() / 1000));
				}
			}
			else
			{
				writeD((int) (_castle.getSiegeDate().getTimeInMillis() / 1000));
				writeD(0x00);
			}
		}
		else
		{
			writeD(_hall.getId());
			
			var ownerId = _hall.getOwnerId();
			
			writeD(((ownerId == player.getClanId()) && (player.isClanLeader())) ? 0x01 : 0x00);
			writeD(ownerId);
			if (ownerId > 0)
			{
				var owner = ClanTable.getInstance().getClan(ownerId);
				if (owner != null)
				{
					writeS(owner.getName()); // Clan Name
					writeS(owner.getLeaderName()); // Clan Leader Name
					writeD(owner.getAllyId()); // Ally ID
					writeS(owner.getAllyName()); // Ally Name
				}
				else
				{
					LOG.warn("Null owner for siegable hall: {}", _hall.getName());
				}
			}
			else
			{
				writeS(""); // Clan Name
				writeS(""); // Clan Leader Name
				writeD(0); // Ally ID
				writeS(""); // Ally Name
			}
			
			writeD((int) (Calendar.getInstance().getTimeInMillis() / 1000));
			writeD((int) ((CHSiegeManager.getInstance().getSiegableHall(_hall.getId()).getNextSiegeTime()) / 1000));
			writeD(0x00); // number of choices?
		}
	}
}
