/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.serverpackets;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.l2jserver.gameserver.model.TimeStamp;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

/**
 * Skill Cool Time server packet implementation.
 * @author KenM
 * @author Zoey76
 */
public class SkillCoolTime extends L2GameServerPacket
{
	private List<TimeStamp> _skillReuseTimeStamps = new ArrayList<>();
	
	public SkillCoolTime(L2PcInstance player)
	{
		if (player.getSkillReuseTimeStamps() != null)
		{
			_skillReuseTimeStamps = player.getSkillReuseTimeStamps().values().stream().filter(ts -> !ts.hasNotPassed()).collect(Collectors.toList());
		}
		else
		{
			_skillReuseTimeStamps = new ArrayList<>(); // или другое действие, соответствующее вашим потребностям
		}
	}
	
	@Override
	protected void writeImpl()
	{
		writeC(0xC7);
		writeD(_skillReuseTimeStamps.size());
		for (var ts : _skillReuseTimeStamps)
		{
			writeD(ts.getSharedReuseGroup() > 0 ? ts.getSharedReuseGroup() : ts.getSkillId());
			writeD(ts.getSkillLvl());
			writeD((int) (ts.getReuse() > 0 ? ts.getReuse() : ts.getRemaining()) / 1000);
			writeD((int) ts.getRemaining() / 1000);
		}
	}
}
