/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.serverpackets;

import java.util.ArrayList;
import java.util.List;

import com.l2jserver.gameserver.model.holders.SkillInfoHolder;

public final class SkillList extends L2GameServerPacket
{
	private final List<SkillInfoHolder> _skills = new ArrayList<>();
	
	public void addSkill(int id, int level, boolean passive, boolean disabled, boolean enchanted)
	{
		_skills.add(new SkillInfoHolder(id, level, passive, disabled, enchanted));
	}
	
	@Override
	protected final void writeImpl()
	{
		writeC(0x5F);
		writeD(_skills.size());
		
		for (var temp : _skills)
		{
			writeD(temp.getPassive() ? 1 : 0);
			writeD(temp.getLevel());
			writeD(temp.getId());
			writeC(temp.getDisabled() ? 1 : 0);
			writeC(temp.getEnchanted() ? 1 : 0);
		}
	}
}
