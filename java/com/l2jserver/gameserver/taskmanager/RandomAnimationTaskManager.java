/*
 * Copyright (C) 2004-2022 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.taskmanager;

import static com.l2jserver.gameserver.ai.CtrlIntention.AI_INTENTION_ACTIVE;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.ThreadPoolManager;
import com.l2jserver.gameserver.configuration.config.GeneralConfig;
import com.l2jserver.gameserver.model.actor.L2Npc;

public final class RandomAnimationTaskManager implements Runnable
{
	private final Map<L2Npc, Long> _characters = new ConcurrentHashMap<>();
	
	protected RandomAnimationTaskManager()
	{
		// Run task each second.
		ThreadPoolManager.getInstance().scheduleAiAtFixedRate(this, 1000, 1000);
	}
	
	public final void add(L2Npc character, int interval)
	{
		_characters.put(character, System.currentTimeMillis() + (interval * 1000));
	}
	
	@Override
	public final void run()
	{
		// List is empty, skip.
		if (_characters.isEmpty())
		{
			return;
		}
		
		// Get current time.
		final long time = System.currentTimeMillis();
		
		// Loop all characters.
		for (Map.Entry<L2Npc, Long> entry : _characters.entrySet())
		{
			// Time hasn't passed yet, skip.
			if (time < entry.getValue())
			{
				continue;
			}
			
			final L2Npc character = entry.getKey();
			
			// Cancels timer on specific cases.
			if (character.isMob())
			{
				// Cancel further animation timers until intention is changed to ACTIVE again.
				if (character.getAI().getIntention() != AI_INTENTION_ACTIVE)
				{
					_characters.remove(character);
					continue;
				}
			}
			else
			{
				if (!character.isInActiveRegion()) // NPCs in inactive region don't run this task
				{
					_characters.remove(character);
					continue;
				}
			}
			
			if (!(character.isDead() || character.isStunned() || character.isSleeping()))
			{
				character.onRandomAnimation(Rnd.get(2, 3));
			}
			
			// Renew the timer.
			final int timer = (character.isMob()) ? Rnd.get(GeneralConfig.MIN_MONSTER_ANIMATION, GeneralConfig.MAX_MONSTER_ANIMATION) : Rnd.get(GeneralConfig.MIN_NPC_ANIMATION, GeneralConfig.MAX_NPC_ANIMATION);
			add(character, timer);
		}
	}
	
	public static final RandomAnimationTaskManager getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static final class SingletonHolder
	{
		protected static final RandomAnimationTaskManager INSTANCE = new RandomAnimationTaskManager();
	}
}
