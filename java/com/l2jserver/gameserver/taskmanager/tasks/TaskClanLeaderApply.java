/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.taskmanager.tasks;

import java.time.DayOfWeek;
import java.time.LocalDate;

import com.l2jserver.gameserver.configuration.config.ClanConfig;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.enums.TaskType;
import com.l2jserver.gameserver.taskmanager.Task;
import com.l2jserver.gameserver.taskmanager.TaskManager;
import com.l2jserver.gameserver.taskmanager.TaskManager.ExecutedTask;

/**
 * @author UnAfraid
 */
public class TaskClanLeaderApply extends Task
{
	private static final String NAME = "clanleaderapply";
	
	@Override
	public String getName()
	{
		return NAME;
	}
	
	@Override
	public void onTimeElapsed(ExecutedTask task)
	{
		LocalDate currentDate = LocalDate.now();
		DayOfWeek currentDayOfWeek = currentDate.getDayOfWeek();
		
		if (currentDayOfWeek == DayOfWeek.of(ClanConfig.ALT_CLAN_LEADER_DATE_CHANGE))
		{
			for (var clan : ClanTable.getInstance().getClans())
			{
				if (clan.getNewLeaderId() != 0)
				{
					var member = clan.getClanMember(clan.getNewLeaderId());
					if (member == null)
					{
						clan.setNewLeader(member);
					}
				}
			}
			LOG.info("Information of clan leader updated.");
		}
	}
	
	@Override
	public void initializate()
	{
		TaskManager.addUniqueTask(NAME, TaskType.TYPE_GLOBAL_TASK, "1", ClanConfig.ALT_CLAN_LEADER_HOUR_CHANGE, "");
	}
}
