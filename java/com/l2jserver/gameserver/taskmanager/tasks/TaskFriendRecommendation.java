/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.taskmanager.tasks;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.enums.TaskType;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.variables.AccountVariables;
import com.l2jserver.gameserver.model.variables.PlayerVariables;
import com.l2jserver.gameserver.taskmanager.Task;
import com.l2jserver.gameserver.taskmanager.TaskManager;
import com.l2jserver.gameserver.taskmanager.TaskManager.ExecutedTask;

/**
 * @author Мо3олЬ
 */
public class TaskFriendRecommendation extends Task
{
	private static final String NAME = "friend_recommendation";
	
	@Override
	public void onTimeElapsed(ExecutedTask task)
	{
		// Update data for offline players.
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps1 = con.prepareStatement("DELETE FROM account_gsdata WHERE var = ?");
			var ps2 = con.prepareStatement("DELETE FROM character_variables WHERE var = ?"))
		{
			ps1.setString(1, AccountVariables.PC_FRIEND_RECOMMENDATION_PROOF_TODAY);
			ps1.executeUpdate();
			
			ps2.setString(1, PlayerVariables.USED_PC_FRIEND_RECOMMENDATION_PROOF);
			ps2.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not reset friend recommendation daily bonus!", ex);
		}
		
		// Update data for online players.
		L2World.getInstance().getPlayers().stream().forEach(player ->
		{
			player.getAccountVariables().remove(AccountVariables.PC_FRIEND_RECOMMENDATION_PROOF_TODAY);
			player.getAccountVariables().storeMe();
			
			player.getVariables().remove(PlayerVariables.USED_PC_FRIEND_RECOMMENDATION_PROOF);
			player.getVariables().storeMe();
		});
		
		LOG.info("Friend recommendation daily bonus has been resetted.");
	}
	
	@Override
	public void initializate()
	{
		super.initializate();
		TaskManager.addUniqueTask(NAME, TaskType.TYPE_GLOBAL_TASK, "1", "06:30:00", "");
	}
	
	@Override
	public String getName()
	{
		return NAME;
	}
}
