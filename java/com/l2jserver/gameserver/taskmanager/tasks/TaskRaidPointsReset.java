/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.taskmanager.tasks;

import java.time.DayOfWeek;
import java.time.LocalDate;

import com.l2jserver.gameserver.configuration.config.FeatureConfig;
import com.l2jserver.gameserver.data.sql.impl.ClanTable;
import com.l2jserver.gameserver.enums.TaskType;
import com.l2jserver.gameserver.instancemanager.RaidBossPointsManager;
import com.l2jserver.gameserver.taskmanager.Task;
import com.l2jserver.gameserver.taskmanager.TaskManager;
import com.l2jserver.gameserver.taskmanager.TaskManager.ExecutedTask;

public class TaskRaidPointsReset extends Task
{
	public static final String NAME = "raid_points_reset";
	
	@Override
	public String getName()
	{
		return NAME;
	}
	
	@Override
	public void onTimeElapsed(ExecutedTask task)
	{
		LocalDate currentDate = LocalDate.now();
		DayOfWeek currentDayOfWeek = currentDate.getDayOfWeek();
		
		if (currentDayOfWeek == DayOfWeek.MONDAY)
		{
			// reward clan reputation points
			var rankList = RaidBossPointsManager.getInstance().getRankList();
			for (var c : ClanTable.getInstance().getClans())
			{
				for (var entry : rankList.entrySet())
				{
					if ((entry.getValue() <= 100) && c.isMember(entry.getKey()))
					{
						int reputation = switch (entry.getValue())
						{
							case 1 -> FeatureConfig.RAID_RANKING_1ST;
							case 2 -> FeatureConfig.RAID_RANKING_2ND;
							case 3 -> FeatureConfig.RAID_RANKING_3RD;
							case 4 -> FeatureConfig.RAID_RANKING_4TH;
							case 5 -> FeatureConfig.RAID_RANKING_5TH;
							case 6 -> FeatureConfig.RAID_RANKING_6TH;
							case 7 -> FeatureConfig.RAID_RANKING_7TH;
							case 8 -> FeatureConfig.RAID_RANKING_8TH;
							case 9 -> FeatureConfig.RAID_RANKING_9TH;
							case 10 -> FeatureConfig.RAID_RANKING_10TH;
							default -> entry.getValue() <= 50 ? FeatureConfig.RAID_RANKING_UP_TO_50TH : FeatureConfig.RAID_RANKING_UP_TO_100TH;
						};
						c.incReputation(reputation, true);
					}
				}
			}
			
			RaidBossPointsManager.getInstance().cleanUp();
			LOG.info("Raid points has been resetted.");
		}
	}
	
	@Override
	public void initializate()
	{
		super.initializate();
		TaskManager.addUniqueTask(NAME, TaskType.TYPE_GLOBAL_TASK, "1", "00:10:00", "");
	}
}
