/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.util;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.configuration.config.AdminConfig;

/**
 * Thread to check for deadlocked threads.
 * @author -Nemesiss-
 */
public class DeadLockDetector extends Thread
{
	private static final Logger LOG = LoggerFactory.getLogger(DeadLockDetector.class);
	
	private final ThreadMXBean tmx;
	
	public DeadLockDetector()
	{
		super("DeadLockDetector");
		tmx = ManagementFactory.getThreadMXBean();
	}
	
	@Override
	public final void run()
	{
		var deadlock = false;
		while (!deadlock)
		{
			try
			{
				var ids = tmx.findDeadlockedThreads();
				
				if (ids != null)
				{
					deadlock = true;
					var tis = tmx.getThreadInfo(ids, true, true);
					var sb = new StringBuilder();
					sb.append("DeadLock Found!");
					sb.append(System.lineSeparator());
					for (var ti : tis)
					{
						sb.append(ti.toString());
					}
					
					for (var ti : tis)
					{
						var locks = ti.getLockedSynchronizers();
						var monitors = ti.getLockedMonitors();
						if ((locks.length == 0) && (monitors.length == 0))
						{
							continue;
						}
						
						var dl = ti;
						sb.append("Java-level deadlock:");
						sb.append(System.lineSeparator());
						sb.append('\t');
						sb.append(dl.getThreadName());
						sb.append(" is waiting to lock ");
						sb.append(dl.getLockInfo().toString());
						sb.append(" which is held by ");
						sb.append(dl.getLockOwnerName());
						sb.append(System.lineSeparator());
						while ((dl = tmx.getThreadInfo(new long[]
						{
							dl.getLockOwnerId()
						}, true, true)[0]).getThreadId() != ti.getThreadId())
						{
							sb.append('\t');
							sb.append(dl.getThreadName());
							sb.append(" is waiting to lock ");
							sb.append(dl.getLockInfo().toString());
							sb.append(" which is held by ");
							sb.append(dl.getLockOwnerName());
							sb.append(System.lineSeparator());
						}
					}
					LOG.warn(sb.toString());
					
					if (AdminConfig.RESTART_ON_DEADLOCK)
					{
						Broadcast.announceToOnlinePlayers("announce_server_has_stability_issues_restarting", false);
					}
					
				}
				Thread.sleep(AdminConfig.DEADLOCK_CHECK_INTERVAL);
			}
			catch (Exception ex)
			{
				LOG.error("DeadLockDetector: ", ex);
			}
		}
	}
}
