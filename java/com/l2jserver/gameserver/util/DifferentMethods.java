/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.util;

import java.util.HashSet;
import java.util.concurrent.TimeUnit;

import com.l2jserver.commons.lang.StringUtil;
import com.l2jserver.commons.random.Rnd;
import com.l2jserver.gameserver.GameServer;
import com.l2jserver.gameserver.configuration.config.custom.PvPRewardConfig;
import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.enums.ZoneId;
import com.l2jserver.gameserver.enums.events.Team;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.olympiad.OlympiadManager;
import com.l2jserver.gameserver.network.SystemMessageId;

public class DifferentMethods
{
	public static int getPlayersCount(String type)
	{
		switch (type)
		{
			case "ALL" ->
			{
				return L2World.getInstance().getPlayers().size();
			}
			case "OFF_TRADE" ->
			{
				var offlineCount = 0;
				
				for (var player : L2World.getInstance().getPlayers())
				{
					if ((player.getClient() == null) || player.getClient().isDetached())
					{
						offlineCount++;
					}
				}
				return offlineCount;
			}
			case "GM" ->
			{
				int onlineGMcount = 0;
				for (var player : L2World.getInstance().getAllGMs())
				{
					if ((player != null) && player.isOnline() && (player.getClient() != null) && !player.getClient().isDetached())
					{
						onlineGMcount++;
					}
				}
				return onlineGMcount;
			}
			case "ALL_REAL" ->
			{
				var realPlayers = new HashSet<>();
				
				for (var player : L2World.getInstance().getPlayers())
				{
					if (((player != null) && (player.getClient() != null)) && !player.getClient().isDetached())
					{
						realPlayers.add(player.getIPAddress());
					}
				}
				return realPlayers.size();
			}
		}
		return 0;
	}
	
	public static String getServerUpTime()
	{
		var time = System.currentTimeMillis() - GameServer.dateTimeServerStarted.getTimeInMillis();
		
		var days = TimeUnit.MILLISECONDS.toDays(time);
		time -= TimeUnit.DAYS.toMillis(days);
		var hours = TimeUnit.MILLISECONDS.toHours(time);
		time -= TimeUnit.HOURS.toMillis(hours);
		var minutes = TimeUnit.MILLISECONDS.toMinutes(time);
		
		return days + " Days, " + hours + " Hours, " + minutes + " Minutes";
	}
	
	public static String getItemName(int itemId)
	{
		return ItemTable.getInstance().getTemplate(itemId).getName();
	}
	
	public static String formatPay(final L2PcInstance player, final long count, final int item)
	{
		if (count > 0L)
		{
			return StringUtil.formatNumber(count) + " " + getItemName(item);
		}
		return MessagesData.getInstance().getMessage(player, "community_board_price_free");
	}
	
	public static String className(L2PcInstance player, int classId)
	{
		if (((classId < 0) || (classId > 136)) || ((classId > 118) && (classId < 123)) || ((classId > 57) && (classId < 88)))
		{
			return MessagesData.getInstance().getMessage(player, "community_board_utils_classId_name_default");
		}
		return MessagesData.getInstance().getMessage(player, "community_board_utils_classId_name_" + classId + "");
	}
	
	public static boolean getPay(L2PcInstance player, int itemid, long count)
	{
		if (player == null)
		{
			return false;
		}
		
		if ((player.getInventory().getItemByItemId(itemid) == null) || (player.getInventory().getItemByItemId(itemid).getCount() < count))
		{
			player.sendPacket(SystemMessageId.NOT_ENOUGH_ITEMS);
			return false;
		}
		player.destroyItemByItemId("BBS", itemid, count, player, true);
		return true;
	}
	
	public static boolean checkFirstConditions(L2PcInstance player)
	{
		if (player == null)
		{
			return false;
		}
		
		if (player.isInSiege() || (player.getSiegeState() != 0) || player.isCombatFlagEquipped() || (player.getInstanceId() > 0) || player.isTransformed() || player.isFishing() || (player.getPvpFlag() != 0) || player.isStunned() || player.isDead() || player.isAlikeDead() || player.isFakeDeath()
			|| player.isInWater() || player.isInBoat() || player.isInsideZone(ZoneId.NO_BOOKMARK) || player.isCastingNow() || player.isInCombat() || player.isAttackingNow() || player.isJailed() || player.isFlying() || player.isFlyingMounted() || (player.getKarma() > 0) || player.isInDuel())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled_players_now"));
			return false;
		}
		
		if (player.getTeam() != Team.NONE)
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled_players_events"));
			return false;
		}
		
		if (player.isCursedWeaponEquipped())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled_players_cursed_weapon"));
			return false;
		}
		
		if (player.isInStoreMode() || player.getTradeRefusal() || player.isInOfflineMode())
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled_players_private_store"));
			return false;
		}
		
		if (player.isInOlympiadMode() || OlympiadManager.getInstance().isRegistered(player))
		{
			player.sendMessage(MessagesData.getInstance().getMessage(player, "community_board_services_disabled_players_olympiad"));
			return false;
		}
		return true;
	}
	
	public static double prune(double in, int pow)
	{
		return ((int) (in * Math.pow(10, pow))) / Math.pow(10, pow);
	}
	
	public static String getPlayerTime(L2PcInstance player, int time)
	{
		var timeResult = "";
		if (time >= 86400)
		{
			timeResult = Integer.toString(time / 86400) + " " + MessagesData.getInstance().getMessage(player, "html_day") + " " + Integer.toString((time % 86400) / 3600) + "" + MessagesData.getInstance().getMessage(player, "html_hourse") + "";
		}
		else
		{
			timeResult = Integer.toString(time / 3600) + "" + MessagesData.getInstance().getMessage(player, "html_hourse") + "" + Integer.toString((time % 3600) / 60) + "" + MessagesData.getInstance().getMessage(player, "html_minute") + "";
		}
		return timeResult;
	}
	
	public static String getAdenas(int adena)
	{
		var adenas = "";
		if (adena >= 1000000000)
		{
			adenas = Integer.toString(adena / 1000000000) + " Billion " + Integer.toString((adena % 1000000000) / 1000000) + " million";
		}
		else
		{
			adenas = Integer.toString(adena / 1000000) + " Million " + Integer.toString((adena % 1000000) / 1000) + " k";
		}
		return adenas;
	}
	
	public static String button(String value, String bypass, int width, int height, String back, String fore)
	{
		var sb = new StringBuilder();
		sb.append("<button value=\"" + value + "\" action=\"bypass " + bypass + "\" width=" + width + " height=" + height + " back=\"" + back + "\" fore=\"" + fore + "\">");
		return sb.toString();
	}
	
	public static String buttonTD(String value, String bypass, int width, int height, String back, String fore)
	{
		var sb = new StringBuilder();
		sb.append("<td>");
		sb.append(button(value, bypass, width, height, back, fore));
		sb.append("</td>");
		return sb.toString();
	}
	
	public static String buttonTR(String value, String bypass, int width, int height, String back, String fore)
	{
		var sb = new StringBuilder();
		sb.append("<tr>");
		sb.append(buttonTD(value, bypass, width, height, back, fore));
		sb.append("</tr>");
		return sb.toString();
	}
	
	public static int doCaptcha(boolean n1, boolean n2)
	{
		var captcha = 0;
		if (n1)
		{
			captcha = Rnd.get(1, 499);
		}
		if (n2)
		{
			captcha = Rnd.get(1, 499);
		}
		return captcha;
	}
	
	public static String RGBtoBGR(String color)
	{
		var colorOut = "";
		if ((color != null) && (color.length() == 6))
		{
			colorOut = color.substring(4, 6) + color.substring(2, 4) + color.substring(0, 2);
		}
		return colorOut;
	}
	
	public static boolean basicCheck(L2PcInstance killer, L2Character killed)
	{
		if (killer == null)
		{
			return false;
		}
		
		var targetPlayer = killed.getActingPlayer();
		if (targetPlayer == null)
		{
			return false;
		}
		
		if (targetPlayer.getLevel() < PvPRewardConfig.PVP_REWARD_MIN_LEVEL)
		{
			killer.sendMessage(MessagesData.getInstance().getMessage(killer, "pvp_bonus_minimal_player_level").replace("%i%", PvPRewardConfig.PVP_REWARD_MIN_LEVEL + ""));
			return false;
		}
		
		if (targetPlayer.getClassId().level() < PvPRewardConfig.PVP_REWARD_MIN_PROFF)
		{
			killer.sendMessage(MessagesData.getInstance().getMessage(killer, "pvp_bonus_minimal_job_level").replace("%i%", (PvPRewardConfig.PVP_REWARD_MIN_PROFF - 1) + ""));
			return false;
		}
		
		if ((System.currentTimeMillis() - killer.getLastAccess()) < (PvPRewardConfig.PVP_REWARD_MIN_UPTIME_MINUTE * 60000))
		{
			killer.sendMessage(MessagesData.getInstance().getMessage(killer, "pvp_bonus_minimal_ingame_time").replace("%i%", PvPRewardConfig.PVP_REWARD_MIN_UPTIME_MINUTE + ""));
			return false;
		}
		
		if ((System.currentTimeMillis() - targetPlayer.getLastAccess()) < (PvPRewardConfig.PVP_REWARD_MIN_UPTIME_MINUTE * 60000))
		{
			killer.sendMessage(MessagesData.getInstance().getMessage(killer, "pvp_bonus_minimal_ingame_time").replace("%i%", PvPRewardConfig.PVP_REWARD_MIN_UPTIME_MINUTE + ""));
			return false;
		}
		
		if (!PvPRewardConfig.PVP_REWARD_PK_GIVE && (killer.getKarma() > 0))
		{
			killer.sendMessage(MessagesData.getInstance().getMessage(killer, "pvp_bonus_pk"));
			return false;
		}
		
		if (killer.getTeam() != Team.NONE)
		{
			killer.sendMessage(MessagesData.getInstance().getMessage(killer, "pvp_bonus_event"));
			return false;
		}
		
		if ((killer.getParty() != null) && (killer.getParty() == targetPlayer.getParty()) && ((killer.getParty().getCommandChannel() == null) || (killer.getParty().getCommandChannel() == targetPlayer.getParty().getCommandChannel())))
		{
			killer.sendMessage(MessagesData.getInstance().getMessage(killer, "pvp_bonus_party"));
			return false;
		}
		
		if ((killer.getClan() != null) && (killer.getClan() == targetPlayer.getClan()))
		{
			killer.sendMessage(MessagesData.getInstance().getMessage(killer, "pvp_bonus_clan"));
			return false;
		}
		
		if ((killer.getAllyId() > 0) && (killer.getAllyId() == targetPlayer.getAllyId()))
		{
			killer.sendMessage(MessagesData.getInstance().getMessage(killer, "pvp_bonus_alliance"));
			return false;
		}
		
		if (PvPRewardConfig.PVP_REWARD_SPECIAL_ANTI_TWINK_TIMER && ((System.currentTimeMillis() - targetPlayer.getActingPlayer().getUptime()) < (PvPRewardConfig.PVP_REWARD_HR_NEW_CHAR_BEFORE_GET_ITEM * 60000 * 60)))
		{
			killer.sendMessage(MessagesData.getInstance().getMessage(killer, "pvp_bonus_created_short_time").replace("%i%", PvPRewardConfig.PVP_REWARD_HR_NEW_CHAR_BEFORE_GET_ITEM + ""));
			return false;
		}
		
		if (PvPRewardConfig.PVP_REWARD_ONLY_NOBLE_GIVE && !targetPlayer.getActingPlayer().isNoble())
		{
			killer.sendMessage(MessagesData.getInstance().getMessage(killer, "pvp_bonus_noblesse"));
			return false;
		}
		
		if (PvPRewardConfig.PVP_REWARD_CHECK_EQUIP && !checkEquip(targetPlayer))
		{
			killer.sendMessage(MessagesData.getInstance().getMessage(killer, "pvp_bonus_equip"));
			return false;
		}
		return true;
	}
	
	private static boolean checkEquip(L2Character killed)
	{
		if ((killed.getActingPlayer().getExpertiseWeaponPenalty() > 0) || (killed.getActingPlayer().getExpertiseArmorPenalty() > 0))
		{
			return false;
		}
		
		var weapon = killed.getActingPlayer().getActiveWeaponInstance();
		if (weapon == null)
		{
			return false;
		}
		
		if (weapon.getCrystalCount() < PvPRewardConfig.PVP_REWARD_WEAPON_GRADE_TO_CHECK)
		{
			return false;
		}
		return true;
	}
}
