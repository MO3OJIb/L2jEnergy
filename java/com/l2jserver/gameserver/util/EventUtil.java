/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.gameserver.data.xml.impl.MessagesData;
import com.l2jserver.gameserver.data.xml.impl.NpcData;
import com.l2jserver.gameserver.data.xml.impl.SpawnData;
import com.l2jserver.gameserver.datatables.ItemTable;
import com.l2jserver.gameserver.enums.ZoneId;
import com.l2jserver.gameserver.enums.events.MessageType;
import com.l2jserver.gameserver.enums.network.ChatType;
import com.l2jserver.gameserver.instancemanager.QuestManager;
import com.l2jserver.gameserver.model.L2Spawn;
import com.l2jserver.gameserver.model.L2World;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.eventengine.GameEventManager;
import com.l2jserver.gameserver.model.olympiad.OlympiadManager;
import com.l2jserver.gameserver.network.serverpackets.CreatureSay;
import com.l2jserver.gameserver.network.serverpackets.ExEventMatchMessage;

public class EventUtil
{
	private static final Logger LOG = LoggerFactory.getLogger(EventUtil.class);
	
	private static L2Spawn _flagSpawn = null;
	private static L2Npc _lastFlagSpawn = null;
	
	public static boolean checkPlayer(L2PcInstance player, boolean firstCheck)
	{
		if (firstCheck && GameEventManager.getInstance().getEvent().getConfigs().EVENT_PARTICIPATION_FEE)
		{
			if (player.getInventory().getInventoryItemCount(GameEventManager.getInstance().getEvent().getConfigs().EVENT_TAKE_ITEM_ID, -1) < GameEventManager.getInstance().getEvent().getConfigs().EVENT_TAKE_COUNT)
			{
				showMessage(player, "events_you_dont_have_enough_item_participate", ItemTable.getInstance().getTemplate(GameEventManager.getInstance().getEvent().getConfigs().EVENT_TAKE_ITEM_ID).getName(), MessageType.CUSTOM);
				return false;
			}
		}
		
		if (firstCheck && !GameEventManager.getInstance().getEvent().isParticipating())
		{
			showMessage(player, "events_you_registration_canceled_process_inactive", MessageType.CUSTOM);
			return false;
		}
		
		if (firstCheck && GameEventManager.getInstance().getEvent().isParticipant(player.getObjectId()))
		{
			showMessage(player, "event_you_alredy_registred", MessageType.CUSTOM);
			return false;
		}
		
		if (player.isTeleporting())
		{
			showMessage(player, "events_you_registration_canceled_cannot_participate_during_teleportation", MessageType.CUSTOM);
			return false;
		}
		
		if (!player.isOnline())
		{
			showMessage(player, "events_you_have_online_registration", MessageType.CUSTOM);
			return false;
		}
		
		if (player.isMounted())
		{
			showMessage(player, "events_you_registration_canceled_while_sitting_pet", MessageType.CUSTOM);
			return false;
		}
		
		if (player.isInDuel())
		{
			showMessage(player, "events_you_registration_canceled_cannot_participate_during_duel", MessageType.CUSTOM);
			return false;
		}
		
		if (player.isJailed())
		{
			showMessage(player, "events_you_registration_canceled_cannot_register_because_jail", MessageType.CUSTOM);
			return false;
		}
		
		if (firstCheck && (player.isOnEvent() || player.isCombatFlagEquipped()))
		{
			showMessage(player, "events_you_registration_canceled_cannot_participate_several_events_same_time", MessageType.CUSTOM);
			return false;
		}
		
		if (player.getBlockCheckerArena() >= 0)
		{
			showMessage(player, "events_you_registration_canceled_cannot_register_because_participating_handy_block_event", MessageType.CUSTOM);
			return false;
		}
		
		if (player.isInOlympiadMode() || OlympiadManager.getInstance().isRegistered(player))
		{
			showMessage(player, "events_you_registration_canceled_cannot_participate_olympiad_events", MessageType.CUSTOM);
			return false;
		}
		
		if (player.isInParty() && player.getParty().isInDimensionalRift())
		{
			showMessage(player, "events_you_registration_canceled_cannot_participate_dimensional_rift_delusion_chambler", MessageType.CUSTOM);
			return false;
		}
		
		if (player.isCursedWeaponEquipped())
		{
			showMessage(player, "events_you_registration_canceled_cannot_participate_cursed_weapon", MessageType.CUSTOM);
			return false;
		}
		
		if (player.isDead() || player.isAlikeDead() || player.isFakeDeath())
		{
			showMessage(player, "events_you_registration_canceled_cannot_participate_events_while_dead", MessageType.CUSTOM);
			return false;
		}
		
		if (player.inObserverMode())
		{
			showMessage(player, "events_you_registration_canceled_cannot_participate_events_while_overview_mode", MessageType.CUSTOM);
			return false;
		}
		
		if (player.getInstanceId() > 0)
		{
			showMessage(player, "events_you_registration_canceled_cannot_participate_events_while_instance_zone", MessageType.CUSTOM);
			return false;
		}
		
		if (player.getKarma() > 0)
		{
			showMessage(player, "events_you_registration_canceled_cannot_participate_events_while_pk", MessageType.CUSTOM);
			return false;
		}
		
		if ((player.getLevel() < GameEventManager.getInstance().getEvent().getConfigs().EVENT_MIN_LVL) || (player.getLevel() > GameEventManager.getInstance().getEvent().getConfigs().EVENT_MAX_LVL))
		{
			showMessage(player, "events_you_registration_canceled_cannot_participate_events_while_level", MessageType.CUSTOM);
			return false;
		}
		
		if (firstCheck && (GameEventManager.getInstance().getEvent().getCountPlayers() >= GameEventManager.getInstance().getEvent().getConfigs().EVENT_MAX_PLAYERS_IN_TEAMS))
		{
			showMessage(player, "events_you_registration_canceled_cannot_participate_events_while_maximum_player", MessageType.CUSTOM);
			return false;
		}
		
		var Q00505_BloodOffering = 505;
		var q505 = QuestManager.getInstance().getQuest(Q00505_BloodOffering);
		if (player.hasQuestState(q505.getName()))
		{
			showMessage(player, "events_you_registration_canceled_cannot_participate_events_while_sevensigns", MessageType.CUSTOM);
			return false;
		}
		
		if (player.isFishing())
		{
			showMessage(player, "events_you_cannot_register_while_fishing", MessageType.CUSTOM);
			return false;
		}
		
		if (player.isInSiege() || player.isInsideZone(ZoneId.SIEGE) || (player.getSiegeState() != 0))
		{
			showMessage(player, "events_you_cannot_register_while_siege", MessageType.CUSTOM);
			return false;
		}
		
		if ((player.getWeightPenalty() != 0))
		{
			showMessage(player, "events_you_cannot_register_invetory_weight", MessageType.CUSTOM);
			return false;
		}
		
		if (!player.isInventoryUnder90(false))
		{
			showMessage(player, "events_you_cannot_register_invetory_items", MessageType.CUSTOM);
			return false;
		}
		
		if (player.isFlying() || player.isFlyingMounted())
		{
			showMessage(player, "events_you_cannot_register_while_flying", MessageType.CUSTOM);
			return false;
		}
		
		if (player.isTransformed())
		{
			showMessage(player, "events_you_cannot_register_while_transformed", MessageType.CUSTOM);
			return false;
		}
		
		if (player.isInStoreMode() || player.getTradeRefusal() || player.isInOfflineMode())
		{
			showMessage(player, "events_you_cannot_register_private_store", MessageType.CUSTOM);
			return false;
		}
		return true;
	}
	
	/**
	 * Показывает сообщение всем игрокам в онлайне
	 */
	public static void showAnnounceEvents(String text, String string, String string2, String string3, MessageType type)
	{
		var players = L2World.getInstance().getPlayers();
		
		if (type == MessageType.CUSTOM)
		{
			for (var player : players)
			{
				var textId = MessagesData.getInstance().getMessage(player, text).replace("%i%", string + "").replace("%s%", string2 + "").replace("%y%", string3 + "");
				player.sendPacket(new CreatureSay(1, ChatType.CRITICAL_ANNOUNCE, "", textId));
			}
		}
		else
		{
			for (var player : players)
			{
				player.sendPacket(new CreatureSay(0, ChatType.CRITICAL_ANNOUNCE, "", String.format(text, string)));
			}
		}
	}
	
	public static void showAnnounceEvents(String text, String string, String string2, MessageType type)
	{
		showAnnounceEvents(text, string, string2, null, type);
	}
	
	public static void showAnnounceEvents(String text, String string, MessageType type)
	{
		showAnnounceEvents(text, string, null, null, type);
	}
	
	public static void showAnnounceEvents(String text, MessageType type)
	{
		showAnnounceEvents(text, null, null, null, type);
	}
	
	/**
	 * Показывает сообщение на экране игроку
	 */
	public static void showEventMessage(L2PcInstance player, String text, String string, MessageType type)
	{
		var textId = MessagesData.getInstance().getMessage(player, text).replace("%i%", string + "");
		
		if (player == null)
		{
			return;
		}
		
		if (type == MessageType.CUSTOM)
		{
			player.sendPacket(new ExEventMatchMessage(0, textId));
		}
		else
		{
			player.sendPacket(new ExEventMatchMessage(type.getType(), String.format(text, string)));
		}
	}
	
	/**
	 * Показывает сообщение игроку
	 */
	public static void showMessage(L2PcInstance player, String text, String string, String string2, MessageType type)
	{
		var textId = MessagesData.getInstance().getMessage(player, text).replace("%i%", string + "").replace("%s%", string2 + "");
		
		if (player == null)
		{
			return;
		}
		
		if (type == MessageType.CUSTOM)
		{
			player.sendMessage(textId);
		}
		else
		{
			player.sendMessage(String.format(text, string));
		}
	}
	
	public static void showMessage(L2PcInstance player, String text, String string, MessageType type)
	{
		showMessage(player, text, string, null, type);
	}
	
	public static void showMessage(L2PcInstance player, String text, MessageType type)
	{
		showMessage(player, text, null, null, type);
	}
	
	// TODO:
	public static void giveFame(L2PcInstance player, int count)
	{
		if ((player != null) && (count > 0) && (player.getLevel() >= 40))
		{
			player.setFame(player.getFame() + count);
		}
	}
	
	public static void givePCCafePoint(L2PcInstance player, int count)
	{
		if ((player != null) && (count > 0))
		{
			player.getPcCafeSystem().addPoints(count, false);
		}
	}
	
	public static void giveCRP(L2PcInstance player, int count)
	{
		if ((player != null) && (count > 0) && (player.getClan() != null) && (player.getClan().getLevel() > 4))
		{
			player.getClan().incReputation(count, true);
		}
	}
	
	public static L2Spawn spawnSingle(int npcId, Location loc, int instanceId)
	{
		// Spawn the flags
		try
		{
			var tmpl = NpcData.getInstance().getTemplate(npcId);
			_flagSpawn = new L2Spawn(tmpl);
			_flagSpawn.setXYZ(loc);
			_flagSpawn.setHeading(Util.calculateHeadingFrom(new Location(loc.getX(), loc.getY(), loc.getZ(), loc.getHeading()), new Location(loc.getX(), loc.getY(), loc.getZ(), loc.getHeading())));
			_flagSpawn.setInstanceId(instanceId);
			_flagSpawn.setRespawnDelay(1);
			_flagSpawn.setAmount(1);
			SpawnData.getInstance().addNewSpawn(_flagSpawn, false);
			_flagSpawn.init();
			_lastFlagSpawn = _flagSpawn.getLastSpawn();
			_lastFlagSpawn.setCurrentHp(_lastFlagSpawn.getMaxHp());
			_lastFlagSpawn.isAggressive();
			_lastFlagSpawn.decayMe();
			_lastFlagSpawn.spawnMe(_flagSpawn.getLastSpawn().getX(), _flagSpawn.getLastSpawn().getY(), _flagSpawn.getLastSpawn().getZ());
			_lastFlagSpawn.setIsInvul(true);
			_lastFlagSpawn.setTarget(_lastFlagSpawn);
		}
		catch (Exception ex)
		{
			LOG.warn("", ex);
		}
		return _flagSpawn;
	}
}
