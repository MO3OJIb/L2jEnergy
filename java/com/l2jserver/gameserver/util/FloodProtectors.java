/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.util;

import com.l2jserver.gameserver.enums.FloodAction;
import com.l2jserver.gameserver.model.PcCondOverride;
import com.l2jserver.gameserver.network.L2GameClient;

public final class FloodProtectors
{
	public static boolean performAction(final L2GameClient client, FloodAction action)
	{
		if ((client.getActiveChar() != null) && client.getActiveChar().canOverrideCond(PcCondOverride.FLOOD_CONDITIONS))
		{
			return true;
		}
		
		var reuseDelay = action.getReuseDelay();
		if (reuseDelay == 0)
		{
			return true;
		}
		
		var value = client.getFloodProtectors();
		
		synchronized (value)
		{
			if (value[action.ordinal()] > System.currentTimeMillis())
			{
				return false;
			}
			
			value[action.ordinal()] = System.currentTimeMillis() + reuseDelay;
			return true;
		}
	}
}
