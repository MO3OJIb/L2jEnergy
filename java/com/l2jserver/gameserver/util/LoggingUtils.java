/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.util;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;

import com.l2jserver.commons.util.TimeUtils;
import com.l2jserver.commons.util.Util;
import com.l2jserver.gameserver.enums.IllegalActionPunishmentType;
import com.l2jserver.gameserver.enums.OlympiadType;
import com.l2jserver.gameserver.enums.Tops;
import com.l2jserver.gameserver.enums.items.EtcItemType;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.gameserver.model.items.type.ItemType;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.network.L2GameClient;

public class LoggingUtils
{
	private static final Set<ItemType> EXCLUDED_ITEM_TYPES = new HashSet<>();
	
	static
	{
		EXCLUDED_ITEM_TYPES.add(EtcItemType.ARROW);
		EXCLUDED_ITEM_TYPES.add(EtcItemType.SHOT);
	}
	
	public static void logItem(Logger itemLogger, String processPrefix, String process, L2ItemInstance item, String ownerName, Object reference)
	{
		var sb = new StringBuilder();
		
		// Don't log arrows, shots and herbs.
		if (!EXCLUDED_ITEM_TYPES.contains(item.getItemType()))
		{
			return;
		}
		
		sb.append(processPrefix);
		sb.append(process);
		
		sb.append(", owner '");
		sb.append(ownerName);
		sb.append('\'');
		
		sb.append(", item object id '");
		sb.append(item.getObjectId());
		sb.append('\'');
		
		sb.append(", item name '");
		sb.append(item.getItem().getName());
		sb.append('\'');
		
		sb.append(", item count '");
		sb.append(item.getCount());
		sb.append('\'');
		
		if (item.getEnchantLevel() > 0)
		{
			sb.append(", item enchant level '");
			sb.append('+');
			sb.append(item.getEnchantLevel());
			sb.append('\'');
		}
		sb.append(", reference '");
		sb.append(reference.toString());
		sb.append('.');
		itemLogger.info(sb.toString());
	}
	
	public static void logChat(Logger chatLogger, String senderName, String receiverName, String message)
	{
		var sb = new StringBuilder();
		
		sb.append('[');
		sb.append(senderName);
		sb.append("] ");
		
		if (receiverName != null)
		{
			sb.append(" to ");
			
			sb.append('\'');
			sb.append(receiverName);
			sb.append('\'');
		}
		
		sb.append(", message: \"");
		sb.append(message);
		sb.append('"');
		
		sb.append('.');
		
		chatLogger.info(sb.toString());
	}
	
	public static void logAccounting(Logger accountingLogger, String message, L2GameClient client)
	{
		var sb = new StringBuilder();
		
		if (client != null)
		{
			sb.append("Character ");
			sb.append(client.getActiveChar().getName());
			sb.append('(');
			sb.append(String.valueOf(client.getActiveChar().getObjectId()));
			sb.append(")");
		}
		
		sb.append(": \"");
		sb.append(message);
		sb.append('"');
		sb.append('.');
		accountingLogger.info(sb.toString());
	}
	
	public static void logAudit(Logger auditLogger, String message, L2PcInstance player, IllegalActionPunishmentType punishment)
	{
		var sb = new StringBuilder();
		
		sb.append(message);
		if (player != null)
		{
			sb.append(' ');
			sb.append(player.getName());
		}
		if (punishment != null)
		{
			sb.append(' ');
			sb.append(punishment.toString());
		}
		auditLogger.info(sb.toString());
	}
	
	public static void logAudit(Logger auditLogger, String message, L2PcInstance player, String punishment)
	{
		logAudit(auditLogger, message, player, punishment);
	}
	
	public static void logVoteSystemBonus(Logger voteLogger, String message, Tops link, L2PcInstance player, int id, int count)
	{
		var sb = new StringBuilder();
		sb.append(message);
		sb.append(" for ");
		if (link != null)
		{
			sb.append(link.getLink());
		}
		else
		{
			sb.append("unknown link");
		}
		sb.append(" from Character");
		if (player != null)
		{
			sb.append(' ');
			sb.append(player.getName());
			sb.append('(');
			sb.append(String.valueOf(player.getObjectId()));
			sb.append(")");
		}
		else
		{
			sb.append(" unknown player");
		}
		sb.append(" ==> itemId: ");
		sb.append(id);
		sb.append(", count: ");
		sb.append(count);
		sb.append(".");
		voteLogger.info(sb.toString());
	}
	
	public static void logItemMall(Logger itemMallLogger, String message, L2PcInstance player, int id, long count)
	{
		var sb = new StringBuilder();
		sb.append(message);
		sb.append(" from Character");
		if (player != null)
		{
			sb.append(' ');
			sb.append(player.getName());
			sb.append('(');
			sb.append(String.valueOf(player.getObjectId()));
			sb.append(")");
		}
		else
		{
			sb.append(" unknown player");
		}
		sb.append(" ==> itemId: ");
		sb.append(id);
		sb.append(", count: ");
		sb.append(count);
		sb.append(".");
		itemMallLogger.info(sb.toString());
	}
	
	public static void logGMAudit(Logger gmAuditLogger, String gmName, String action, String target)
	{
		logGMAudit(gmAuditLogger, gmName, action, target, "");
	}
	
	public static void logGMAudit(Logger gmAuditLogger, String gmName, String action, String target, String params)
	{
		var sb = new StringBuilder();
		
		var date = TimeUtils.DATE_TIME_FORMATTER_FIX.format(new Date());
		
		var name = Util.replaceIllegalCharacters(gmName);
		if (!Util.isValidFileName(name))
		{
			sb.append(name = "INVALID_GM_NAME_" + date);
		}
		
		sb.append(date);
		sb.append(">");
		sb.append(name);
		sb.append(">");
		sb.append(action);
		sb.append(">");
		if (target != null)
		{
			sb.append(target);
		}
		
		sb.append(">");
		sb.append(params);
		sb.append(' ');
		sb.append(System.lineSeparator());
		sb.append('"');
		sb.append('.');
		gmAuditLogger.info(sb.toString());
	}
	
	public static void logOlympiad(Logger olympiadLogger, String message, int charId, int classId)
	{
		logOlympiad(olympiadLogger, message, charId, classId, 0, 0);
	}
	
	public static void logOlympiad(Logger olympiadLogger, String message, int charId, int classId, int compDone, int points)
	{
		var sb = new StringBuilder();
		sb.append("Message: \"");
		sb.append(message);
		sb.append(' ');
		sb.append('[');
		sb.append(charId);
		sb.append("] ");
		sb.append(classId);
		sb.append(' ');
		sb.append(compDone);
		sb.append(' ');
		sb.append(" points:");
		sb.append('(');
		sb.append(points);
		sb.append(")");
		sb.append('.');
		olympiadLogger.info(sb.toString());
	}
	
	public static void logOlympiad(Logger olympiadLogger, String message, String charName, String charName2, double playerOneHp, double playerTwoHp, int damageP1, int damageP2, int pointDiff, OlympiadType type)
	{
		var sb = new StringBuilder();
		
		sb.append('[');
		sb.append(charName);
		sb.append("] ");
		sb.append(" vs ");
		sb.append('\'');
		sb.append(charName2);
		sb.append('\'');
		sb.append(", message: \"");
		sb.append(message);
		sb.append(' ');
		sb.append(playerOneHp);
		sb.append(' ');
		sb.append(playerTwoHp);
		sb.append(' ');
		sb.append(damageP1);
		sb.append(' ');
		sb.append(damageP2);
		sb.append(' ');
		sb.append(pointDiff);
		sb.append(' ');
		sb.append(type.toString());
		sb.append('"');
		sb.append('.');
		olympiadLogger.info(sb.toString());
	}
	
	public static void logEnchantItem(Logger enchantItemLogger, L2PcInstance player, String message, L2ItemInstance item, L2ItemInstance scroll, L2ItemInstance support)
	{
		var stringBuilder = new StringBuilder();
		stringBuilder.append(player);
		stringBuilder.append(message);
		stringBuilder.append(item);
		stringBuilder.append(scroll);
		stringBuilder.append(support);
		enchantItemLogger.info(stringBuilder.toString());
	}
	
	public static void logEnchantsSkill(Logger enchantSkillLogger, L2PcInstance player, String message, Skill skill, L2ItemInstance item, int rate)
	{
		var stringBuilder = new StringBuilder();
		stringBuilder.append(player);
		stringBuilder.append(message);
		stringBuilder.append(skill);
		stringBuilder.append(item);
		stringBuilder.append(rate);
		enchantSkillLogger.info(stringBuilder.toString());
	}
	
	public static void logEnchantsSkill(Logger enchantSkillLogger, L2PcInstance player, String message, Skill skill, L2ItemInstance item)
	{
		logEnchantsSkill(enchantSkillLogger, player, message, skill, item, 0);
	}
}
