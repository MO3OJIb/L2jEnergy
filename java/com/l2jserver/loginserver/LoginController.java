/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.RSAKeyGenParameterSpec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.crypto.Cipher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.random.Rnd;
import com.l2jserver.loginserver.configuration.config.LoginConfig;
import com.l2jserver.loginserver.dao.factory.impl.DAOFactory;
import com.l2jserver.loginserver.enums.AccountKickedReason;
import com.l2jserver.loginserver.enums.LoginClientState;
import com.l2jserver.loginserver.enums.LoginFailReason;
import com.l2jserver.loginserver.model.GameServerInfo;
import com.l2jserver.loginserver.model.data.AccountInfo;
import com.l2jserver.loginserver.network.L2LoginClient;
import com.l2jserver.loginserver.network.gameserverpackets.ServerStatus;
import com.l2jserver.loginserver.network.serverpackets.AccountKicked;
import com.l2jserver.loginserver.network.serverpackets.LoginOk;
import com.l2jserver.loginserver.network.serverpackets.ServerList;
import com.l2jserver.loginserver.security.ScrambledKeyPair;

/**
 * Login Controller.
 * @author Zoey76
 */
public class LoginController
{
	protected static final Logger LOG = LoggerFactory.getLogger(LoginController.class);
	
	/** Time before kicking the client if he didn't logged yet */
	public static final int LOGIN_TIMEOUT = 60 * 1000;
	
	/** Authed Clients on LoginServer */
	protected final Map<String, L2LoginClient> _clients = new ConcurrentHashMap<>();
	
	private final Map<InetAddress, Integer> _failedLoginAttemps = new HashMap<>();
	private final Map<InetAddress, Long> _bannedIps = new ConcurrentHashMap<>();
	
	protected final ScrambledKeyPair[] _keyPairs;
	
	protected byte[][] _blowfishKeys;
	private static final int BLOWFISH_KEYS = 20;
	
	// SQL Queries
	private static final String ACCOUNT_IPAUTH_SELECT = "SELECT * FROM accounts_ipauth WHERE login = ?";
	
	protected LoginController()
	{
		LOG.info("Loading LoginController...");
		
		_keyPairs = new ScrambledKeyPair[10];
		
		try
		{
			final KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA");
			final RSAKeyGenParameterSpec spec = new RSAKeyGenParameterSpec(1024, RSAKeyGenParameterSpec.F4);
			keygen.initialize(spec);
			for (int i = 0; i < 10; i++)
			{
				_keyPairs[i] = new ScrambledKeyPair(keygen.generateKeyPair());
			}
			
			testCipher((RSAPrivateKey) _keyPairs[0].getPair().getPrivate());
			
			LOG.info("Cached 10 KeyPairs for RSA communication.");
		}
		catch (Exception ex)
		{
			LOG.error("There has been an error loading the key pairs!", ex);
		}
		
		// Store keys for blowfish communication
		generateBlowFishKeys();
		
		Thread purge = new PurgeThread();
		purge.setDaemon(true);
		purge.start();
	}
	
	private void testCipher(RSAPrivateKey key) throws Exception
	{
		// avoid worst-case execution, KenM
		Cipher rsaCipher = Cipher.getInstance("RSA/ECB/nopadding");
		rsaCipher.init(Cipher.DECRYPT_MODE, key);
	}
	
	private void generateBlowFishKeys()
	{
		_blowfishKeys = new byte[BLOWFISH_KEYS][16];
		for (int i = 0; i < BLOWFISH_KEYS; i++)
		{
			for (int j = 0; j < _blowfishKeys[i].length; j++)
			{
				_blowfishKeys[i][j] = (byte) (Rnd.nextInt(255) + 1);
			}
		}
		LOG.info("Stored {} keys for Blowfish communication", _blowfishKeys.length);
	}
	
	public byte[] getBlowfishKey()
	{
		return _blowfishKeys[(int) (Math.random() * BLOWFISH_KEYS)];
	}
	
	public void removeAuthedLoginClient(String account)
	{
		if (account == null)
		{
			return;
		}
		_clients.remove(account);
	}
	
	public L2LoginClient getAuthedClient(String account)
	{
		return _clients.get(account);
	}
	
	private void recordFailedLoginAttemp(InetAddress address)
	{
		final Integer failedLoginAttemps = _failedLoginAttemps.getOrDefault(address, 0) + 1;
		_failedLoginAttemps.put(address, failedLoginAttemps);
		
		if (failedLoginAttemps >= LoginConfig.LOGIN_TRY_BEFORE_BAN)
		{
			addBanForAddress(address, LoginConfig.LOGIN_BLOCK_AFTER_BAN * 1000);
			// we need to clear the failed login attempts here, so after the ip ban is over the client has another 5 attempts
			clearFailedLoginAttemps(address);
			LOG.warn("Added banned address {}, too many login attemps!", address.getHostAddress());
		}
	}
	
	private void clearFailedLoginAttemps(InetAddress addr)
	{
		_failedLoginAttemps.remove(addr);
	}
	
	public void retriveAccountInfo(L2LoginClient client, String login, String password)
	{
		InetAddress address = client.getConnection().getInetAddress();
		// Retrieve or create (if auto-create is on) an Account based on given login and password.
		AccountInfo account = DAOFactory.getInstance().getAccountsDAO().setAccount(login);
		
		try
		{
			MessageDigest md = MessageDigest.getInstance("SHA");
			byte[] raw = password.getBytes(StandardCharsets.UTF_8);
			String hashBase64 = Base64.getEncoder().encodeToString(md.digest(raw));
			
			if (account == null)
			{
				// Auto-create is off, add a failed attempt.
				if (!LoginConfig.AUTO_CREATE_ACCOUNTS)
				{
					// account does not exist and auto create account is not desired
					recordFailedLoginAttemp(address);
					client.close(LoginFailReason.REASON_USER_OR_PASS_WRONG);
					return;
				}
				
				// Generate an Account and feed variable.
				account = DAOFactory.getInstance().getAccountsDAO().setCreateAccount(login, hashBase64, address.getHostAddress());
				if (account == null)
				{
					client.close(LoginFailReason.REASON_ACCESS_FAILED);
					return;
				}
				LOG.info("Auto created account '{}'.", login);
			}
			else
			{
				if (!account.checkPassHash(hashBase64))
				{
					// wrong password
					recordFailedLoginAttemp(address);
					client.close(LoginFailReason.REASON_USER_OR_PASS_WRONG);
					return;
				}
				// Clear all failed login attempts.
				clearFailedLoginAttemps(address);
				
				// Refresh current time of the account.
				if (!DAOFactory.getInstance().getAccountsDAO().setAccountLastTime(address.getHostAddress(), account.getLogin()))
				{
					client.close(LoginFailReason.REASON_ACCESS_FAILED);
					return;
				}
			}
			
			// Account is banned, return.
			if (account.getAccessLevel() < 0)
			{
				client.close(new AccountKicked(AccountKickedReason.REASON_PERMANENTLY_BANNED));
				return;
			}
			
			// TODO: Test
			String allowedIP = DAOFactory.getInstance().getAccountsDAO().getAllowedIP(account.getLogin());
			if (!allowedIP.contains("*") && (client.getIpAddress().compareTo(allowedIP) != 0))
			{
				client.close(LoginFailReason.REASON_SYSTEM_ERROR_LOGIN_LATER);
				return;
			}
			
			if (canCheckin(client, address, account))
			{
				// Account is already set on ls, return.
				if (isAccountInAnyGameServer(login))
				{
					final GameServerInfo gsi = LoginController.getInstance().getAccountOnGameServer(login);
					if (gsi != null)
					{
						client.close(LoginFailReason.REASON_ACCOUNT_IN_USE);
						
						if (gsi.isAuthed())
						{
							gsi.getGameServerThread().kickPlayer(login);
						}
					}
					return;
				}
			}
			// Account is already set on gs, close the previous client.
			if (_clients.putIfAbsent(login, client) != null)
			{
				final L2LoginClient oldClient = LoginController.getInstance().getAuthedClient(login);
				if (oldClient != null)
				{
					oldClient.close(LoginFailReason.REASON_ACCOUNT_IN_USE);
					LoginController.getInstance().removeAuthedLoginClient(login);
				}
				client.close(LoginFailReason.REASON_ACCOUNT_IN_USE);
				return;
			}
			
			DAOFactory.getInstance().getAccountsDAO().setLogAccount((int) client.getConnectionStartTime(), account.getLogin(), address.getHostAddress()); // TODO
			
			client.setAccount(account.getLogin());
			client.setState(LoginClientState.AUTHED_LOGIN);
			client.setSessionKey(new SessionKey(Rnd.nextInt(), Rnd.nextInt(), Rnd.nextInt(), Rnd.nextInt()));
			client.sendPacket((LoginConfig.SHOW_LICENCE) ? new LoginOk(client.getSessionKey()) : new ServerList(client));
		}
		catch (Exception ex)
		{
			LOG.warn("Exception while retriving account info for '{}'!", login, ex);
			return;
		}
	}
	
	public SessionKey getKeyForAccount(String account)
	{
		L2LoginClient client = _clients.get(account);
		return (client == null) ? null : client.getSessionKey();
	}
	
	public String getHostForAccount(String account)
	{
		L2LoginClient client = getAuthedClient(account);
		return (client != null) ? client.getIpAddress() : "-1";
	}
	
	public boolean isAccountInAnyGameServer(String account)
	{
		Collection<GameServerInfo> serverList = GameServerTable.getInstance().getRegisteredGameServers().values();
		for (GameServerInfo gsi : serverList)
		{
			GameServerThread gst = gsi.getGameServerThread();
			if ((gst != null) && gst.hasAccountOnGameServer(account))
			{
				return true;
			}
		}
		return false;
	}
	
	public GameServerInfo getAccountOnGameServer(String account)
	{
		Collection<GameServerInfo> serverList = GameServerTable.getInstance().getRegisteredGameServers().values();
		for (GameServerInfo gsi : serverList)
		{
			GameServerThread gst = gsi.getGameServerThread();
			if ((gst != null) && gst.hasAccountOnGameServer(account))
			{
				return gsi;
			}
		}
		return null;
	}
	
	public void addBanForAddress(String address, long expiration) throws UnknownHostException
	{
		_bannedIps.putIfAbsent(InetAddress.getByName(address), expiration);
	}
	
	public void addBanForAddress(InetAddress address, long duration)
	{
		_bannedIps.putIfAbsent(address, System.currentTimeMillis() + duration);
	}
	
	public boolean isBannedAddress(InetAddress address) throws UnknownHostException
	{
		String[] parts = address.getHostAddress().split("\\.");
		Long bi = _bannedIps.get(address);
		if (bi == null)
		{
			bi = _bannedIps.get(InetAddress.getByName(parts[0] + "." + parts[1] + "." + parts[2] + ".0"));
		}
		if (bi == null)
		{
			bi = _bannedIps.get(InetAddress.getByName(parts[0] + "." + parts[1] + ".0.0"));
		}
		if (bi == null)
		{
			bi = _bannedIps.get(InetAddress.getByName(parts[0] + ".0.0.0"));
		}
		if (bi != null)
		{
			if ((bi > 0) && (bi < System.currentTimeMillis()))
			{
				_bannedIps.remove(address);
				LOG.info("Removed expired ip address ban {}.", address.getHostAddress());
				return false;
			}
			return true;
		}
		return false;
	}
	
	public Map<InetAddress, Long> getBannedIps()
	{
		return _bannedIps;
	}
	
	public boolean removeBanForAddress(InetAddress address)
	{
		return _bannedIps.remove(address) != null;
	}
	
	public boolean removeBanForAddress(String address)
	{
		try
		{
			return removeBanForAddress(InetAddress.getByName(address));
		}
		catch (Exception ex)
		{
			return false;
		}
	}
	
	public boolean isLoginPossible(L2LoginClient client, int serverId)
	{
		GameServerInfo gsi = GameServerTable.getInstance().getRegisteredGameServerById(serverId);
		int access = client.getAccessLevel();
		if ((gsi != null) && gsi.isAuthed())
		{
			boolean loginOk = ((gsi.getCurrentPlayerCount() < gsi.getMaxPlayers()) && (gsi.getStatus() != ServerStatus.STATUS_GM_ONLY)) || (access > 0);
			
			if (loginOk && (client.getLastServer() != serverId))
			{
				DAOFactory.getInstance().getAccountsDAO().setAccountLastServer(serverId, client.getAccount());
			}
			return loginOk;
		}
		return false;
	}
	
	public void setCharactersOnServer(String account, int charsNum, long[] timeToDel, int serverId)
	{
		L2LoginClient client = _clients.get(account);
		
		if (client == null)
		{
			return;
		}
		
		if (charsNum > 0)
		{
			client.setCharsOnServ(serverId, charsNum);
		}
		
		if (timeToDel.length > 0)
		{
			client.serCharsWaitingDelOnServ(serverId, timeToDel);
		}
	}
	
	public ScrambledKeyPair getScrambledRSAKeyPair()
	{
		return _keyPairs[Rnd.nextInt(10)];
	}
	
	public boolean canCheckin(L2LoginClient client, InetAddress address, AccountInfo info)
	{
		try
		{
			List<InetAddress> ipWhiteList = new ArrayList<>();
			List<InetAddress> ipBlackList = new ArrayList<>();
			try (Connection con = ConnectionFactory.getInstance().getConnection();
				PreparedStatement ps = con.prepareStatement(ACCOUNT_IPAUTH_SELECT))
			{
				ps.setString(1, info.getLogin());
				try (ResultSet rset = ps.executeQuery())
				{
					String ip, type;
					while (rset.next())
					{
						ip = rset.getString("ip");
						type = rset.getString("type");
						
						if (!isValidIPAddress(ip))
						{
							continue;
						}
						else if (type.equals("allow"))
						{
							ipWhiteList.add(InetAddress.getByName(ip));
						}
						else if (type.equals("deny"))
						{
							ipBlackList.add(InetAddress.getByName(ip));
						}
					}
				}
			}
			
			// Check IP
			if (!ipWhiteList.isEmpty() || !ipBlackList.isEmpty())
			{
				if (!ipWhiteList.isEmpty() && !ipWhiteList.contains(address))
				{
					LOG.warn("Account checkin attemp from address({}) not present on whitelist for account '{}'.", address.getHostAddress(), info.getLogin());
					return false;
				}
				
				if (!ipBlackList.isEmpty() && ipBlackList.contains(address))
				{
					LOG.warn("Account checkin attemp from address({}) on blacklist for account '{}'.", address.getHostAddress(), info.getLogin());
					return false;
				}
			}
			
			client.setAccessLevel(info.getAccessLevel());
			client.setLastServer(info.getLastServer());
			DAOFactory.getInstance().getAccountsDAO().setAccountLastTime(address.getHostAddress(), info.getLogin());
			return true;
		}
		catch (Exception e)
		{
			LOG.warn("Could not finish login process!", e);
			return false;
		}
	}
	
	public boolean isValidIPAddress(String ipAddress)
	{
		String[] parts = ipAddress.split("\\.");
		if (parts.length != 4)
		{
			return false;
		}
		
		for (String s : parts)
		{
			int i = Integer.parseInt(s);
			if ((i < 0) || (i > 255))
			{
				return false;
			}
		}
		return true;
	}
	
	class PurgeThread extends Thread
	{
		public PurgeThread()
		{
			setName("PurgeThread");
		}
		
		@Override
		public void run()
		{
			while (!isInterrupted())
			{
				for (L2LoginClient client : _clients.values())
				{
					if (client == null)
					{
						continue;
					}
					if ((client.getConnectionStartTime() + LOGIN_TIMEOUT) < System.currentTimeMillis())
					{
						client.close(LoginFailReason.REASON_ACCESS_FAILED);
					}
				}
				
				try
				{
					Thread.sleep(LOGIN_TIMEOUT / 2);
				}
				catch (InterruptedException e)
				{
					return;
				}
			}
		}
	}
	
	public static LoginController getInstance()
	{
		return SingletonHolder.INSTANCE;
	}
	
	private static class SingletonHolder
	{
		protected static final LoginController INSTANCE = new LoginController();
	}
}
