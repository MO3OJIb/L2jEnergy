/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver.dao;

import com.l2jserver.loginserver.model.data.AccountInfo;

/**
 * @author Мо3олЬ
 */
public interface AccountsDAO
{
	AccountInfo setAccount(String login);
	
	AccountInfo setCreateAccount(String login, String hashed, String addr);
	
	boolean setAccountLastTime(String address, String info);
	
	void setAccountLastServer(int serverId, String login);
	
	void setAccountAccessLevel(String login, int level);
	
	void setAccountLastTracert(String login, String pcIp, String hop1, String hop2, String hop3, String hop4);
	
	void setChangeAllowedHwid(String hwid, String account);
	
	void setChangeAllowedIP(String account, String ip);
	
	String getAllowedIP(String login);
	
	String getHwidForAccount(String account);
	
	void getBanAccount(String account, long banTime, String address, long expiration);
	
	void setLogAccount(int access, String login, String addr);
}
