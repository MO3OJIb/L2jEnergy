/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver.dao.factory.impl;

import com.l2jserver.loginserver.dao.AccountsDAO;
import com.l2jserver.loginserver.dao.MailDAO;
import com.l2jserver.loginserver.dao.ServerRegisterDAO;
import com.l2jserver.loginserver.dao.factory.IDAOFactory;
import com.l2jserver.loginserver.dao.factory.impl.mysql.AccountsDAOMySQLImpl;
import com.l2jserver.loginserver.dao.factory.impl.mysql.MailDAOMySQLImpl;
import com.l2jserver.loginserver.dao.factory.impl.mysql.ServerRegisterDAOMySQLImpl;

/**
 * @author Мо3олЬ
 */
public enum DAOFactory implements IDAOFactory
{
	INSTANCE;
	
	private final AccountsDAO accountsDAO = new AccountsDAOMySQLImpl();
	private final MailDAO mailDAO = new MailDAOMySQLImpl();
	private final ServerRegisterDAO serverRegisterDAO = new ServerRegisterDAOMySQLImpl();
	
	public static DAOFactory getInstance()
	{
		return INSTANCE;
	}
	
	@Override
	public AccountsDAO getAccountsDAO()
	{
		return accountsDAO;
	}
	
	@Override
	public MailDAO getMailDAO()
	{
		return mailDAO;
	}
	
	@Override
	public ServerRegisterDAO getServerRegisterDAO()
	{
		return serverRegisterDAO;
	}
}
