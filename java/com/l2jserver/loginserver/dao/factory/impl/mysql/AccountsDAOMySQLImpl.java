/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver.dao.factory.impl.mysql;

import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.loginserver.LoginController;
import com.l2jserver.loginserver.dao.AccountsDAO;
import com.l2jserver.loginserver.model.data.AccountInfo;

/**
 * @author Мо3олЬ
 */
public class AccountsDAOMySQLImpl implements AccountsDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(AccountsDAOMySQLImpl.class);
	
	private static final String USER_INFO_SELECT = "SELECT login, password, IF(? > value OR value IS NULL, accessLevel, -1) AS accessLevel, lastServer FROM accounts LEFT JOIN (account_data) ON (account_data.account_name=accounts.login AND account_data.var=\"ban_temp\") WHERE login=?";
	private static final String AUTOCREATE_ACCOUNTS_INSERT = "INSERT INTO accounts (login, password, lastactive, accessLevel, lastIP) values (?, ?, ?, ?, ?)";
	private static final String ACCOUNT_LAST_SERVER_UPDATE = "UPDATE accounts SET lastServer = ? WHERE login = ?";
	private static final String ACCOUNT_ACCESS_LEVEL_UPDATE = "UPDATE accounts SET accessLevel = ? WHERE login = ?";
	private static final String ACCOUNT_IPS_UPDATE = "UPDATE accounts SET pcIp = ?, hop1 = ?, hop2 = ?, hop3 = ?, hop4 = ? WHERE login = ?";
	private static final String ACCOUNT_INFO_UPDATE = "UPDATE accounts SET lastactive = ?, lastIP = ? WHERE login = ?";
	private static final String ACCOUNT_HWID_UPDATE = "UPDATE accounts SET allowed_hwid=? WHERE login=?";
	private static final String ACCOUNT_IP_UPDATE = "UPDATE accounts SET allowed_ip=? WHERE login=?";
	private static final String SELECT_IP_UPDATE = "SELECT * FROM accounts WHERE login=?";
	private static final String SELECT_HWID_UPDATE = "SELECT * FROM accounts WHERE login=?";
	
	@Override
	public AccountInfo setAccount(String login)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(USER_INFO_SELECT))
		{
			ps.setString(1, Long.toString(System.currentTimeMillis()));
			ps.setString(2, login);
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					return new AccountInfo(login, rs.getString("password"), rs.getInt("accessLevel"), rs.getInt("lastServer"));
				}
			}
		}
		catch (Exception ex)
		{
			LOG.error("Exception retrieving account infos.", ex);
		}
		return null;
	}
	
	@Override
	public AccountInfo setCreateAccount(String login, String hashed, String addr)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(AUTOCREATE_ACCOUNTS_INSERT))
		{
			ps.setString(1, login);
			ps.setString(2, hashed);
			ps.setLong(3, System.currentTimeMillis());
			ps.setInt(4, 0);
			ps.setString(5, addr);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Exception while auto creating account for '{}'!", login, ex);
			return null;
		}
		return new AccountInfo(login, hashed, 0, 1);
	}
	
	@Override
	public boolean setAccountLastTime(String address, String login)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(ACCOUNT_INFO_UPDATE))
		{
			ps.setLong(1, System.currentTimeMillis());
			ps.setString(2, address);
			ps.setString(3, login);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.error("Exception auto creating account for {}.", login, ex);
			return false;
		}
		return true;
	}
	
	@Override
	public void setAccountAccessLevel(String login, int level)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(ACCOUNT_ACCESS_LEVEL_UPDATE))
		{
			ps.setInt(1, level);
			ps.setString(2, login);
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.warn("Couldn't set access level {} for {}.", level, login, ex);
		}
	}
	
	@Override
	public void setAccountLastServer(int serverId, String login)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(ACCOUNT_LAST_SERVER_UPDATE))
		{
			ps.setInt(1, serverId);
			ps.setString(2, login);
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not set lastServer!", ex);
		}
	}
	
	@Override
	public void setAccountLastTracert(String login, String pcIp, String hop1, String hop2, String hop3, String hop4)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(ACCOUNT_IPS_UPDATE))
		{
			ps.setString(1, pcIp);
			ps.setString(2, hop1);
			ps.setString(3, hop2);
			ps.setString(4, hop3);
			ps.setString(5, hop4);
			ps.setString(6, login);
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not set last tracert!", ex);
		}
	}
	
	@Override
	public void setChangeAllowedHwid(String hwid, String account)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(ACCOUNT_HWID_UPDATE))
		{
			
			ps.setString(1, hwid);
			ps.setString(2, account);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.error("Could not write data!", ex);
		}
	}
	
	@Override
	public void setChangeAllowedIP(String account, String ip)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(ACCOUNT_IP_UPDATE))
		{
			ps.setString(1, ip);
			ps.setString(2, account);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.error("Could not write data!", ex);
		}
	}
	
	@Override
	public String getAllowedIP(String account)
	{
		String iphost = "*";
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_IP_UPDATE))
		{
			ps.setString(1, account);
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					iphost = rs.getString("allowed_ip");
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could not load allowedIp:", e);
		}
		return iphost;
	}
	
	@Override
	public String getHwidForAccount(String account)
	{
		String hwid = "*";
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(SELECT_HWID_UPDATE))
		{
			ps.setString(1, account);
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					hwid = rs.getString("allowed_hwid");
				}
			}
		}
		catch (Exception e)
		{
			LOG.error("Could not load allowed hwid:", e);
		}
		return hwid;
	}
	
	@Override
	public void getBanAccount(String account, long banTime, String address, long expiration)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("INSERT INTO account_data VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE value=?"))
		{
			ps.setString(1, account);
			ps.setString(2, "ban_temp");
			ps.setString(3, Long.toString(banTime));
			ps.setString(4, Long.toString(banTime));
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.warn("Could not ", ex);
		}
		
		try
		{
			LoginController.getInstance().addBanForAddress(address, expiration);
		}
		catch (UnknownHostException e)
		{
			
		}
	}
	
	@Override
	public void setLogAccount(int time, String login, String addr)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement("INSERT INTO account_log (time, login, ip) VALUES(?,?,?)"))
		{
			ps.setInt(1, time);
			ps.setString(2, login);
			ps.setString(3, addr);
			ps.execute();
		}
		catch (Exception ex)
		{
			LOG.error("", ex);
		}
	}
}
