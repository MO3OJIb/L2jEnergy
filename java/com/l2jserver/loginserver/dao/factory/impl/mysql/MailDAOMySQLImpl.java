/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver.dao.factory.impl.mysql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.loginserver.configuration.config.EmailConfig;
import com.l2jserver.loginserver.dao.MailDAO;

/**
 * @author Мо3олЬ
 */
public class MailDAOMySQLImpl implements MailDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(MailDAOMySQLImpl.class);
	
	@Override
	public String getUserMail(String username)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(EmailConfig.EMAIL_SYS_SELECTQUERY))
		{
			ps.setString(1, username);
			try (var rs = ps.executeQuery())
			{
				if (rs.next())
				{
					String mail = rs.getString(EmailConfig.EMAIL_SYS_DBFIELD);
					return mail;
				}
			}
		}
		catch (Exception e)
		{
			LOG.warn("Cannot select user mail!", e);
		}
		return null;
	}
}
