/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver.dao.factory.impl.mysql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.loginserver.dao.ServerRegisterDAO;

/**
 * @author Мо3олЬ
 */
public class ServerRegisterDAOMySQLImpl implements ServerRegisterDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(ServerRegisterDAOMySQLImpl.class);
	
	private static final String DELETE_SERVERS = "DELETE FROM gameservers";
	private static final String DELETE_SERVER = "DELETE FROM gameservers WHERE server_id=?";
	
	@Override
	public void setDeleteServers()
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_SERVERS))
		{
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.info("Error while cleaning registered servers!", ex);
		}
	}
	
	@Override
	public void setDelete(int id)
	{
		try (var con = ConnectionFactory.getInstance().getConnection();
			var ps = con.prepareStatement(DELETE_SERVER))
		{
			ps.setInt(1, id);
			ps.executeUpdate();
		}
		catch (Exception ex)
		{
			LOG.info("Error while cleaning registered server!", ex);
		}
	}
}
