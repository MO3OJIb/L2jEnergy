/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver.gsregistering;

import java.math.BigInteger;
import java.util.Map;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.dao.ServerNameDAO;
import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.commons.util.Util;
import com.l2jserver.loginserver.GameServerTable;
import com.l2jserver.loginserver.configuration.config.LoginConfig;
import com.l2jserver.loginserver.configuration.loader.ConfigLoader;
import com.l2jserver.loginserver.dao.factory.impl.DAOFactory;
import com.l2jserver.loginserver.model.GameServerInfo;

public class GameServerRegister
{
	private static final Logger LOG = LoggerFactory.getLogger(GameServerRegister.class);
	
	private static String _choice;
	
	public static void main(String[] args)
	{
		ConfigLoader.loading();
		// Database
		DAOFactory.getInstance();
		// Prepare Database
		ConnectionFactory.builder() //
			.withUrl(LoginConfig.DATABASE_URL) //
			.withUser(LoginConfig.DATABASE_LOGIN) //
			.withPassword(LoginConfig.DATABASE_PASSWORD) //
			.withMaxPoolSize(LoginConfig.DATABASE_MAX_CONNECTIONS) //
			.build();
		
		try (Scanner _scn = new Scanner(System.in))
		{
			var gameservers = GameServerTable.getInstance();
			LOG.info("");
			LOG.info("");
			LOG.info("        Welcome to L2jEnergy gameserver registering.");
			LOG.info("        ____________________________________________");
			LOG.info("");
			LOG.info("OPTIONS : a number : register a server ID, if available and existing on list.");
			LOG.info("          list : get a list of IDs. A '*' means the id is already used.");
			LOG.info("          clean : unregister a specified gameserver.");
			LOG.info("          cleanall : unregister all gameservers.");
			LOG.info("          exit : exit the program.");
			
			while (true)
			{
				LOG.info("");
				LOG.info("Your choice? ");
				_choice = _scn.next();
				
				if (_choice.equalsIgnoreCase("list"))
				{
					LOG.info("");
					for (Map.Entry<Integer, String> entry : ServerNameDAO.getServers().entrySet())
					{
						LOG.info(entry.getKey() + ": " + entry.getValue() + " " + (gameservers.getRegisteredGameServers().containsKey(entry.getKey()) ? "*" : ""));
					}
				}
				else if (_choice.equalsIgnoreCase("clean"))
				{
					LOG.info("");
					
					if (ServerNameDAO.getServers().isEmpty())
					{
						LOG.info("No server names available, be sure 'serverNames.xml' is in the LoginServer directory.");
					}
					else
					{
						LOG.info("UNREGISTER a specific server. Here's the current list :");
						for (GameServerInfo entry : gameservers.getRegisteredGameServers().values())
						{
							LOG.info(entry.getId() + ": " + ServerNameDAO.getServers().get(entry.getId()));
						}
						
						LOG.info("");
						LOG.info("Your choice? ");
						
						_choice = _scn.next();
						try
						{
							final int id = Integer.parseInt(_choice);
							
							if (!gameservers.getRegisteredGameServers().containsKey(id))
							{
								LOG.info("This server id isn't used.");
							}
							else
							{
								DAOFactory.getInstance().getServerRegisterDAO().setDelete(id);
								gameservers.getRegisteredGameServers().remove(id);
								
								LOG.info("You successfully dropped gameserver #" + id + ".");
							}
						}
						catch (NumberFormatException nfe)
						{
							LOG.info("Type a valid server id.");
						}
					}
				}
				else if (_choice.equalsIgnoreCase("cleanall"))
				{
					LOG.info("");
					LOG.info("UNREGISTER ALL servers. Are you sure? (y/n) ");
					
					_choice = _scn.next();
					
					if (_choice.equals("y"))
					{
						DAOFactory.getInstance().getServerRegisterDAO().setDeleteServers();
						gameservers.getRegisteredGameServers().clear();
						
						LOG.info("You successfully dropped all registered gameservers.");
					}
					else
					{
						LOG.info("'cleanall' processus has been aborted.");
					}
				}
				else if (_choice.equalsIgnoreCase("exit"))
				{
					System.exit(0);
				}
				else
				{
					try
					{
						LOG.info("");
						if (ServerNameDAO.getServers().isEmpty())
						{
							LOG.info("No server names available, be sure 'serverNames.xml' is in the LoginServer directory.");
						}
						else
						{
							final int id = Integer.parseInt(_choice);
							
							if (ServerNameDAO.getServers().get(id) == null)
							{
								LOG.info("No name for server id: " + id + ".");
							}
							else if (gameservers.getRegisteredGameServers().containsKey(id))
							{
								LOG.info("This server id is already used.");
							}
							else
							{
								byte[] hexId = Util.generateHex(16);
								
								gameservers.getRegisteredGameServers().put(id, new GameServerInfo(id, hexId));
								gameservers.registerServerOnDB(hexId, id, "");
								Util.saveHexid(id, new BigInteger(hexId).toString(16), "hexid(server " + id + ").txt");
								
								LOG.info("Server registered under 'hexid(server " + id + ").txt'.");
								LOG.info("Put this file in /configuration gameserver folder and rename it 'hexid.txt'.");
							}
						}
					}
					catch (NumberFormatException nfe)
					{
						LOG.info("Type a number or list|clean|cleanall commands.");
					}
				}
			}
		}
	}
}