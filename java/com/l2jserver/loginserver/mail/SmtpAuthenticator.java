/*
 * Copyright (C) 2004-2022 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

import com.l2jserver.loginserver.configuration.config.EmailConfig;

/**
 * @author Мо3олЬ
 */
public class SmtpAuthenticator extends Authenticator
{
	private final PasswordAuthentication _auth;
	
	public SmtpAuthenticator()
	{
		_auth = new PasswordAuthentication(EmailConfig.EMAIL_SYS_USERNAME, EmailConfig.EMAIL_SYS_PASSWORD);
	}
	
	@Override
	public PasswordAuthentication getPasswordAuthentication()
	{
		return _auth;
	}
}
