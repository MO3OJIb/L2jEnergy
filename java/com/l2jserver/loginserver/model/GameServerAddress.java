/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver.model;

import java.net.UnknownHostException;

import com.l2jserver.loginserver.util.IPSubnet;

public class GameServerAddress extends IPSubnet
{
	private final String _serverAddress;
	
	public GameServerAddress(String subnet, String address) throws UnknownHostException
	{
		super(subnet);
		_serverAddress = address;
	}
	
	public String getServerAddress()
	{
		return _serverAddress;
	}
	
	@Override
	public String toString()
	{
		return _serverAddress + super.toString();
	}
}
