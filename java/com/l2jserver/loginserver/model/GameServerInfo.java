/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver.model;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import com.l2jserver.commons.dao.ServerNameDAO;
import com.l2jserver.loginserver.GameServerThread;
import com.l2jserver.loginserver.network.gameserverpackets.ServerStatus;

public class GameServerInfo
{
	// auth
	private int _id;
	private final byte[] _hexId;
	private boolean _isAuthed;
	// status
	private GameServerThread _gst;
	private int _status;
	// network
	private final ArrayList<GameServerAddress> _addrs = new ArrayList<>(5);
	private int _port;
	// config
	private final boolean _isPvp = true;
	private int _serverType;
	private int _ageLimit;
	private boolean _isShowingBrackets;
	private int _maxPlayers;
	
	public GameServerInfo(int id, byte[] hexId, GameServerThread gst)
	{
		_id = id;
		_hexId = hexId;
		_gst = gst;
		_status = ServerStatus.STATUS_DOWN;
	}
	
	public GameServerInfo(int id, byte[] hexId)
	{
		this(id, hexId, null);
	}
	
	public void setId(int id)
	{
		_id = id;
	}
	
	public int getId()
	{
		return _id;
	}
	
	public byte[] getHexId()
	{
		return _hexId;
	}
	
	public String getName()
	{
		return ServerNameDAO.getServer(_id);
	}
	
	public void setAuthed(boolean isAuthed)
	{
		_isAuthed = isAuthed;
	}
	
	public boolean isAuthed()
	{
		return _isAuthed;
	}
	
	public void setGameServerThread(GameServerThread gst)
	{
		_gst = gst;
	}
	
	public GameServerThread getGameServerThread()
	{
		return _gst;
	}
	
	public void setStatus(int status)
	{
		_status = status;
	}
	
	public int getStatus()
	{
		return _status;
	}
	
	public String getStatusName()
	{
		switch (_status)
		{
			case 0:
				return "Auto";
			case 1:
				return "Good";
			case 2:
				return "Normal";
			case 3:
				return "Full";
			case 4:
				return "Down";
			case 5:
				return "GM Only";
			default:
				return "Unknown";
		}
	}
	
	public int getCurrentPlayerCount()
	{
		if (_gst == null)
		{
			return 0;
		}
		return _gst.getPlayerCount();
	}
	
	public String getExternalHost()
	{
		try
		{
			return getServerAddress(InetAddress.getByName("0.0.0.0"));
		}
		catch (Exception e)
		{
			
		}
		return null;
	}
	
	public int getPort()
	{
		return _port;
	}
	
	public void setPort(int port)
	{
		_port = port;
	}
	
	public void setMaxPlayers(int maxPlayers)
	{
		_maxPlayers = maxPlayers;
	}
	
	public int getMaxPlayers()
	{
		return _maxPlayers;
	}
	
	public boolean isPvp()
	{
		return _isPvp;
	}
	
	public void setAgeLimit(int val)
	{
		_ageLimit = val;
	}
	
	public int getAgeLimit()
	{
		return _ageLimit;
	}
	
	public void setServerType(int val)
	{
		_serverType = val;
	}
	
	public int getServerType()
	{
		return _serverType;
	}
	
	public void setShowingBrackets(boolean val)
	{
		_isShowingBrackets = val;
	}
	
	public boolean isShowingBrackets()
	{
		return _isShowingBrackets;
	}
	
	public void setDown()
	{
		setAuthed(false);
		setPort(0);
		setGameServerThread(null);
		setStatus(ServerStatus.STATUS_DOWN);
	}
	
	public void addServerAddress(String subnet, String addr) throws UnknownHostException
	{
		_addrs.add(new GameServerAddress(subnet, addr));
	}
	
	public String getServerAddress(InetAddress addr)
	{
		for (var a : _addrs)
		{
			if (a.equals(addr))
			{
				return a.getServerAddress();
			}
		}
		return null; // should not happens
	}
	
	public String[] getServerAddresses()
	{
		var result = new String[_addrs.size()];
		for (var i = 0; i < result.length; i++)
		{
			result[i] = _addrs.get(i).toString();
		}
		
		return result;
	}
	
	public void clearServerAddresses()
	{
		_addrs.clear();
	}
}
