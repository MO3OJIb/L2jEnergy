/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver.model;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.loginserver.network.L2LoginClient;
import com.l2jserver.loginserver.network.gameserverpackets.ServerStatus;

public class ServerData
{
	protected static final Logger LOG = LoggerFactory.getLogger(ServerData.class);
	
	private byte[] _ip;
	private final int _port;
	private final int _ageLimit;
	private final boolean _pvp;
	private final int _currentPlayers;
	private final int _maxPlayers;
	private final boolean _brackets;
	private final int _status;
	private final int _serverId;
	private final int _serverType;
	
	public ServerData(L2LoginClient client, GameServerInfo gsi)
	{
		try
		{
			_ip = InetAddress.getByName(gsi.getServerAddress(client.getConnection().getInetAddress())).getAddress();
		}
		catch (UnknownHostException e)
		{
			LOG.warn("{}: ", getClass().getSimpleName(), e);
			_ip = new byte[4];
			_ip[0] = 127;
			_ip[1] = 0;
			_ip[2] = 0;
			_ip[3] = 1;
		}
		
		_port = gsi.getPort();
		_pvp = gsi.isPvp();
		_serverType = gsi.getServerType();
		_currentPlayers = gsi.getCurrentPlayerCount();
		_maxPlayers = gsi.getMaxPlayers();
		_ageLimit = gsi.getAgeLimit();
		_brackets = gsi.isShowingBrackets();
		// If server GM-only - show status only to GMs
		_status = gsi.getStatus() != ServerStatus.STATUS_GM_ONLY ? gsi.getStatus() : client.getAccessLevel() > 0 ? gsi.getStatus() : ServerStatus.STATUS_DOWN;
		_serverId = gsi.getId();
	}
	
	public byte[] getIp()
	{
		return _ip;
	}
	
	public int getPort()
	{
		return _port;
	}
	
	public int getAgeLimit()
	{
		return _ageLimit;
	}
	
	public boolean getPvP()
	{
		return _pvp;
	}
	
	public int getCurrentPlayers()
	{
		return _currentPlayers;
	}
	
	public int getMaxPlayers()
	{
		return _maxPlayers;
	}
	
	public boolean getBrackers()
	{
		return _brackets;
	}
	
	public int getStatus()
	{
		return _status;
	}
	
	public int getServerId()
	{
		return _serverId;
	}
	
	public int getServerType()
	{
		return _serverType;
	}
}
