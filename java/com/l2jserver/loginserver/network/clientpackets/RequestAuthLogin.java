/*
 * Copyright (C) 2004-2021 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.loginserver.network.clientpackets;

import java.security.GeneralSecurityException;

import javax.crypto.Cipher;

import com.l2jserver.loginserver.LoginController;
import com.l2jserver.loginserver.enums.LoginFailReason;
import com.l2jserver.loginserver.network.L2LoginClient;

public class RequestAuthLogin extends L2LoginClientPacket
{
	private final byte[] _raw = new byte[128];
	
	public int _ncotp;
	
	@Override
	public boolean readImpl()
	{
		if (super._buf.remaining() >= 128)
		{
			readB(_raw);
			return true;
		}
		return false;
	}
	
	@Override
	public void run()
	{
		byte[] decrypted = null;
		final L2LoginClient client = getClient();
		try
		{
			final Cipher rsaCipher = Cipher.getInstance("RSA/ECB/nopadding");
			rsaCipher.init(Cipher.DECRYPT_MODE, client.getRSAPrivateKey());
			decrypted = rsaCipher.doFinal(_raw, 0x00, 0x80);
		}
		catch (GeneralSecurityException ex)
		{
			LOG.error("Failed to generate a cipher.", ex);
			client.close(LoginFailReason.REASON_ACCESS_FAILED);
			return;
		}
		
		try
		{
			final String user = new String(decrypted, 0x5E, 14).trim().toLowerCase();
			final String password = new String(decrypted, 0x6C, 16).trim();
			_ncotp = decrypted[0x7c];
			_ncotp |= decrypted[0x7d] << 8;
			_ncotp |= decrypted[0x7e] << 16;
			_ncotp |= decrypted[0x7f] << 24;
			
			LoginController.getInstance().retriveAccountInfo(client, user, password);
		}
		catch (Exception ex)
		{
			LOG.error("Failed to decrypt user/password.", ex);
			client.close(LoginFailReason.REASON_ACCESS_FAILED);
			return;
		}
	}
}
